/*

  gpReadMouseTest.c

  Test piped communication and mouse readback with gnuplot.
  This code is published under the GNU Public License.

  This example demonstrates how to wait for a mouse press and get the
  mouse button number and mouse position, from a graph in gnuplot.

*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>

#define GPFIFO "/tmp/gpio" /* any unique name */

int main(int argc, char *argv[])
{
  FILE  *gp, *gpin;
  float mx = 0.0, my = 0.0;
  int   mb = 0;
  int   n;
  char  buf[80];

/* Create a FIFO we later use for communication gnuplot => our program. */
  if(mkfifo(GPFIFO, 0600))
  {
    if(errno != EEXIST)
    {
      perror(GPFIFO);
      unlink(GPFIFO);

      return(1);
    }
  }

  if((gp = popen("gnuplot", "w")) == NULL)
  {
    perror("gnuplot");
    pclose(gp);

    return(1);
  }

  puts("Connected to gnuplot.\n");

/* Init mouse and redirect all gnuplot printings to our FIFO */
  fprintf(gp, "set mouse; set print \"%s\"\n", GPFIFO);
  fflush(gp);

  fprintf(gp, "set terminal x11\n");
  fflush(gp);

/* Open the FIFO (where gnuplot is writing to) for reading. */
  if((gpin = fopen(GPFIFO,"r")) == NULL)
  {
    perror(GPFIFO);
    pclose(gp);

    return(1);
  }

  puts("FIFO open for reading.\n");

/* Now do the work. Make a sin() plot */

  fprintf(gp, "plot sin(x)\n");
  fflush(gp);

/* Now wait for mouse clicks; Do it 5 times. */
  for(n=0;n<5;n++)
  {
    printf("%i/5. -- Click anywhere in the graph by mouse button 1.\n", n+1);
    fprintf(gp, "pause mouse\n");
    fflush(gp);

    /* Let gnuplot write to coordinates values (to the FIFO). */
    fprintf(gp, "print MOUSE_X, MOUSE_Y, MOUSE_BUTTON\n");
    fflush(gp);

    /* Read from the FIFO. */
    fscanf(gpin, "%f %f %d", &mx, &my, &mb);
    printf("You pressed mouse button: %d @ x=%f y=%f\n", mb, mx, my);
  }

  fclose(gpin);
  pclose(gp);
  unlink (GPFIFO);

  return(0);
}
