/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <math.h>
#include <signal.h>
#include <time.h>
#include <termios.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>

#include "observing_par.h"
#include "cactus.h"
#include "sex.h"
#include "global.h"
#include "gaussian.h"
#include "shell.h"
#include "window.h"
#include "moment.h"
#include "image.h"
#include "flux.h"
#include "accum.h"
#include "focus.h"
#include "object.h"
#include "lp_fft.h"
#include "strings_and_things.h"
#include "histogram.h"
#include "tsys.h"
#include "scan_sum.h"
#include "loglib.h"

#define DEFINE_ANSI_COLORS_STRINGS
#include "ansi_colors.h"

#include "caclib_proto.h"
#include "linuxlib_proto.h"
#include "proto.h"

#define TRUE  1
#define FALSE 0

#define LAST_IMAGE_INFO       1
#define LAST_FLUX_INFO        2
#define LAST_ACCUM_RESULTS    3
#define LAST_FLUX_HISTORY     4
#define LAST_CONTINUUM_PLAIN  5
#define LAST_CONTINUUM_SPTIP  6
#define LAST_OBJECT_HISTORY   7
#define LAST_FFT_INFO         8
#define LAST_HISTO_INFO       9
#define LAST_TSYS_INFO       10
#define LAST_SSS_INFO        11
#define LAST_FOCUS_HISTORY   12
#define LAST_MOMENTS_STRUCT  13

#define PLOT                 1
#define PRINT                2

#define SINGLE               1
#define MULTI                2

#define SAVE_DEM_FILE        1

#define NEW_MAKE 1

extern int computeXaxis(struct LP_IMAGE_INFO *image, int logit);

char *firstArg = "H0"; /* Need to line ../condar/image.o */

char *kp_sideband[] = { "H-LSB", "H-USB", "V-LSB", "V-USB" };
char *mg_sideband[] = { "H-LSB", "H-USB", "V-LSB", "V-USB" };
char *sideband[]    = { "LSB",   "USB",   "LSB",   "USB" };

char *prompt    = "Xgraphics >>"; /* Required for linking */
char *papersize = "10, 7.5";
char fsize[256], psize[256];
char *mouse_str     = "print \"MOUSE: \", MOUSE_X, \" \", MOUSE_X2, \" \", MOUSE_Y, \" \", MOUSE_Y2";
char *map_ang_str   = "print \"MAP_ANGLE: \", GPVAL_VIEW_ROT_X, \" \", GPVAL_VIEW_ROT_Z";
char *get_range_str = "print \"PLOT_RANGE: \", GPVAL_X_MIN, \" \", GPVAL_X_MAX, \" \", GPVAL_Y_MIN, \" \", GPVAL_Y_MAX";

char  session[80];

char  *focus_axis_str[] = { "AX", "NS", "EW" };

struct SOCK *master;
int gnuplot_pid;
int lastPlot = LAST_IMAGE_INFO;
int displayW = 1920;
int displayH = 1080;
int startx   = 1920-800;

double plot_interval;

FILE   *errPrint_fp = NULL;

struct LP_IMAGE_INFO          last_image;
struct ACCUM_RESULTS_SEND     ars;
struct FLUX_INFO              last_flux;
struct FLUX_HISTORY           fh;
struct CONTINUUM_PLAIN        last_plain_send;
struct CONTINUUM_SPTIP        last_sptip_send;
struct OBJECT                 last_obj;
struct FFT_INFO               last_fft;
struct HISTO_INFO             last_histo;
struct STRINGS_AND_THINGS     sat;
struct TSYS_SEND              last_tsys;
struct SCAN_SUM_SEND          sss;
struct XGRAPHIC_SEND          xsend;
struct FOCUS_HISTORY_SEND     focus_send;
struct SHELL_GUESS            shell_guess;
struct SPECTRAL_MOMENT_STRUCT sms;

#define MAX_BUF_SIZE (sizeof(last_image)+256)

char *plotStyleStrs[] = { "histeps",
                          "lines",
                          "points",
			  "linespoints"
};

FILE  *gpout, *gpin;

 /* The following are Required for linking to ../lib/liblinux.a*/
int stquiet;
int recursiveLevel;
int verboseLevel;
int lp_logging;

static float sinEl[] = {2.6, 2.3, 2.0, 1.7, 1.4, 1.1, 1.4, 1.7, 2.0, 2.3, 2.6};

char osbStr[32];

/* Redefine the bind keys so if I type in the plot window, 
   it doesn't do stupid things */
char *gnuplot_inits[] = {
	"bind m \"x = 1\"",
	"bind a \"x = 1\"",
	"bind g \"x = 1\"",
	"bind h \"x = 1\"",
	"bind l \"x = 1\"",
	"bind L \"x = 1\"",
	"bind 1 \"x = 1\"",
	"bind 2 \"x = 1\"",
	"bind 3 \"x = 1\"",
	"bind 4 \"x = 1\"",
	"bind 5 \"x = 1\"",
	"bind 6 \"x = 1\"",
	"bind 7 \"x = 1\"",
	NULL
};
#define MAX_GNUPLOT_INITS 13


int setColor(c)
int c;
{
  return(0);
}

/*

  Code stolen from: gpReadMouseTest.c

  Test piped communication and mouse readback with gnuplot.
  This code is published under the GNU Public License.

  This example demonstrates how to wait for a mouse press and get the
  mouse button number and mouse position, from a graph in gnuplot.

*/

char gpfifo[256];

int gnuplot_init(term)
char *term;
{
  int i;
  char pbuf[256], sysbuf[256];
  char *string, line[256], *cp;
  FILE *fp;

/* Create a FIFO we later use for communication gnuplot => our program. */
  if(mkfifo(gpfifo, 0600))
  {
    if(errno != EEXIST)
    {
      log_perror(gpfifo);
      unlink(gpfifo);

      return(1);
    }
  }

  cp = getenv("LINUXPOPS_GNUPLOT");
  if(cp)
  {
    sprintf(pbuf, "%s -title GNUPLOT_%s", cp, session);
  }
  else
  {
    sprintf(pbuf, "gnuplot -title GNUPLOT_%s", session);
  }

  if((gpout = popen(pbuf, "w")) == NULL)
  {
    log_perror(pbuf);

    exit(1);
  }

  log_msg("# Connected to gnuplot on FIFO: %s", gpfifo);

/* Init mouse and redirect all gnuplot printings to our FIFO */
  fprintf(gpout, "set mouse; set print \"%s\"\n", gpfifo);
  fflush(gpout);

  fprintf(gpout, "set terminal %s enhanced font \"%s\"\n", term, fsize);
  fflush(gpout);

  /* Redifine gnuplot bindings */
  for(i=0;i<MAX_GNUPLOT_INITS;i++)
  {
    fprintf(gpout, "%s\n", gnuplot_inits[i]);
    fflush(gpout);

    usleep(100000);
  }

/* Open the FIFO (where gnuplot is writing to) for reading. */
  if((gpin = fopen(gpfifo,"r")) == NULL)
  {
    log_perror(gpfifo);
    pclose(gpout);

    return(1);
  }

  /* Now go find the PID of the underlying gnuplot process */
  sprintf(sysbuf, 
    "/bin/ps auxww | /bin/grep -i 'GNUPLOT_%s' | /bin/grep 'gnuplot ' | /bin/grep -v grep | /bin/awk '{print $2}'", 
	session);

  if((fp = popen(sysbuf, "r")) == NULL)
  {
    log_perror(sysbuf);
    pclose(gpin);
    pclose(gpout);

    exit(1);
  }

  while((string = fgets(line, sizeof(line), fp)) != NULL)
  {
    if(strlen(line) > 1)
    {
      gnuplot_pid = atoi(line);
      break;
    }
  }

  pclose(fp);

  log_msg("< Got Gnuplot PID %d", gnuplot_pid);

  log_msg("# FIFO open for reading.");

  return(0);
}


int closeGnuplot()
{
  if(gpin)
  {
    fclose(gpin);
  }

  writeGnuplot("exit", 0);

  sleep(1);

  if(gpout)
  {
    pclose(gpout);
  }

  unlink(gpfifo);

  return(0);
}

int writeGnuplot(buf, delay)
char *buf;
int delay;
{
  log_msg("> Sending to gnuplot: %s", buf);

  fprintf(gpout, "%s\n", buf);
  fflush(gpout);

  if(delay > 0)
  {
    log_msg("Sleeping for %.3f seconds", (double)delay / 1.0e6);

    usleep(delay);
  }

  return(0);
}


int readGnuplot(buf)
char *buf;
{
  int len;
  char inBuf[1024], *cp;
  static int  cnt = 0;

  cp = fgets(inBuf, sizeof(inBuf), gpin);

  if(cp == NULL)
  {
    log_msg("Nothing returned from fscanf()");
    cnt++;

    if(cnt > 100)
    {
      log_msg("Runaway detected; Exiting");
      sock_send(master, "xgraphic_quitting");

      exit(1);
    }

    return(1);
  }
  else
  {
    len = strlen(inBuf);

    if(len > 2)
    {
      inBuf[len-1] = '\0';
    }

    log_msg("< Got from gnuplot: %s", inBuf);

    strcpy(buf, inBuf);

    cnt = 0;
  }

  return(0);
}


int isBadChan(image, bad)
struct LP_IMAGE_INFO *image;
int bad;
{
  int i;

  for(i=0;i<MAX_BAD_CHANS;i++)
  {
    if(image->badChans[i] == bad)
    {
      return(1);
    }
  }

  return(0);
}

char *makeDate(d)
double d;
{
  double year, mon, day;
  double f;
  static char tbuf[256];

  /* Example date: 2016.1130 */

//  log_msg("Date: %f", d);

  year = floor(d);
//  log_msg("Date: %f", year);

  f = d - year;                 /* f = 0000.1130 */
//  log_msg("Date: %f", f);

  f  *= 100.0;                  /* f = 11.30 */
//  log_msg("Date: %f", f);

  mon = floor(f);
//  log_msg("Date: %f", mon);

  f = f - floor(f);             /* f = 0.30 */
//  log_msg("Date: %f", f);

  f  *= 100.0;                  /* f = 30.00 */
//  log_msg("Date: %f", f);

  day  = f;
//  log_msg("Date: %f", day);

  sprintf(tbuf, "%02.0f-%02.0f-%4.0f", mon, day, year);

//  log_msg("Date: %s", tbuf);

  return(tbuf);
}


char *makeFrStr(freqres)
double freqres;
{
  static char tbuf[256];
  double fr;

  fr = fabs(freqres * 1e3);

  if(fr < 100.0)
  {
    sprintf(tbuf, "FR=%4.2f", fr);
  }
  else
  {
    sprintf(tbuf, "FR=%4.0f", fr);
  }

  return(tbuf);
}


int makeLineHeader(image, buf)
struct LP_IMAGE_INFO *image;
char *buf;
{
//  int l1, l2, l3;
  char tbuf[256], raStr[80], decStr[80], timeStr[80], scanStr[80], dateStr[256];
  struct HEADER *h;

  h = &image->head;

  sexagesimal(h->epocra / 15.0, raStr, DD_MM_SS);
  sexagesimal(h->epocdec, decStr, DD_MM_SS);
  sexagesimal(h->inttime / 3600.0, timeStr, DD_MM_SS);

  if(image->ainfo.bscan != 0.0 && image->ainfo.escan != 0.0)
  {
    sprintf(scanStr, "(%3d) Scans: %7.2f-%7.2f", 
			image->ainfo.scanct, image->ainfo.bscan, image->ainfo.escan);
  }
  else
  {
    sprintf(scanStr, "%7.2f                    ", h->scan);
  }

  strcpy(dateStr, makeDate(h->utdate));

  sprintf(tbuf, "%16.16s %s INT=%s   DATE: %s\\n", h->object, scanStr, timeStr, dateStr);

//  l1 = strlen(tbuf);

  strcpy(buf, tbuf);

  sprintf(tbuf, "%8.8s  %s  %s                      CAL= %6.1f  TS=%6.1f\\n",
	h->coordcd, raStr, decStr, h->tcal, h->stsys);
//  l2 = strlen(tbuf);

  strcat(buf, tbuf);

  sprintf(tbuf, "FREQ= %9.2f  SYN= %8.6f  VEL= %8.1f  DV=  %5.2f  %s  SB=%d",
	h->restfreq, h->synfreq/1000.0, h->velocity, h->deltax, makeFrStr(h->freqres), (int)h->sideband);
//  l3 = strlen(tbuf);

  strcat(buf, tbuf);

//  log_msg("# Header Lenght: %d, %d, %d", l1, l2, l3);

  return(0);
}


int makeFFTHeader(fft, buf)
struct FFT_INFO *fft;
char *buf;
{
  char tbuf[256], raStr[80], decStr[80], timeStr[80], scanStr[80], dateStr[256];
  struct HEADER *h;

  h = &fft->head;

  sexagesimal(h->xsource / 15.0, raStr, DD_MM_SS);
  sexagesimal(h->ysource, decStr, DD_MM_SS);
  sexagesimal(h->inttime / 3600.0, timeStr, DD_MM_SS);

  sprintf(scanStr, "%7.2f                    ", h->scan);

  strcpy(dateStr, makeDate(h->utdate));

  sprintf(tbuf, "%12.12s %s INT=%s    DATE: %s\\n", h->object, scanStr, timeStr, dateStr);

//  log_msg("# Header Line-1 len: %d", strlen(tbuf));

  strcpy(buf, tbuf);

  sprintf(tbuf, "%8.8s  %s  %s                      CAL= %6.1f  TS= %5.0f\\n",
	h->coordcd, raStr, decStr, h->tcal, h->stsys);

//  log_msg("# Header Line-2 len: %d", strlen(tbuf));

  strcat(buf, tbuf);


  sprintf(tbuf, "FREQ= %9.2f  SYN= %8.6f  VEL= %8.1f  DV=  %5.2f  %s  SB=%d",
	h->restfreq, h->synfreq/1000.0, h->velocity, h->deltax, makeFrStr(h->freqres), (int)h->sideband);

//  log_msg("# Header Line-3 len: %d", strlen(tbuf));

  strcat(buf, tbuf);

  return(0);
}


int makeSSSHeader(ss, buf)
struct SCAN_SUM_SEND *ss;
char *buf;
{
  char tbuf[256], raStr[80], decStr[80], timeStr[80], scanStr[80], dateStr[256];
  struct HEADER *h;

  h = &ss->head;

  sexagesimal(h->xsource / 15.0, raStr, DD_MM_SS);
  sexagesimal(h->ysource, decStr, DD_MM_SS);
  sexagesimal(h->inttime / 3600.0, timeStr, DD_MM_SS);

  sprintf(scanStr, "%7.2f                    ", h->scan);

  strcpy(dateStr, makeDate(h->utdate));

  sprintf(tbuf, "%12.12s %s INT=%s    DATE: %s\\n", h->object, scanStr, timeStr, dateStr);

//  log_msg("# Header Line-1 len: %d", strlen(tbuf));

  strcpy(buf, tbuf);

  sprintf(tbuf, "%8.8s  %s  %s                      CAL= %6.1f  TS= %5.0f\\n",
	h->coordcd, raStr, decStr, h->tcal, h->stsys);

//  log_msg("# Header Line-2 len: %d", strlen(tbuf));

  strcat(buf, tbuf);

  sprintf(tbuf, "FREQ= %9.2f  SYN= %8.6f  VEL= %8.1f  DV=  %5.2f  %s  SB=%d",
	h->restfreq, h->synfreq/1000.0, h->velocity, h->deltax, makeFrStr(h->freqres), (int)h->sideband);

//  log_msg("# Header Line-3 len: %d", strlen(tbuf));

  strcat(buf, tbuf);

  return(0);
}


int makeHistoHeader(histo, buf)
struct HISTO_INFO *histo;
char *buf;
{
  char tbuf[256], raStr[80], decStr[80], timeStr[80], scanStr[80], dateStr[256];
  struct HEADER *h;

  h = &histo->head;

  sexagesimal(h->xsource / 15.0, raStr, DD_MM_SS);
  sexagesimal(h->ysource, decStr, DD_MM_SS);
  sexagesimal(h->inttime / 3600.0, timeStr, DD_MM_SS);

  sprintf(scanStr, "%7.2f                    ", h->scan);

  strcpy(dateStr, makeDate(h->utdate));

  sprintf(tbuf, "%12.12s %s INT=%s    DATE: %s\\n", h->object, scanStr, timeStr, dateStr);

//  log_msg("# Header Line-1 len: %d", strlen(tbuf));

  strcpy(buf, tbuf);

  sprintf(tbuf, "%8.8s  %s  %s                      CAL= %6.1f  TS= %5.0f\\n",
	h->coordcd, raStr, decStr, h->tcal, h->stsys);

//  log_msg("# Header Line-2 len: %d", strlen(tbuf));

  strcat(buf, tbuf);

  sprintf(tbuf, "FREQ= %9.2f  SYN= %8.6f  VEL= %8.1f  DV=  %5.2f  %s  SB=%d",
	h->restfreq, h->synfreq/1000.0, h->velocity, h->deltax, makeFrStr(h->freqres), (int)h->sideband);

//  log_msg("# Header Line-3 len: %d", strlen(tbuf));

  strcat(buf, tbuf);

  return(0);
}

int makeContHeader(h, a, buf)
struct HEADER *h;
struct ACCUM_RESULTS_SEND *a;
char *buf;
{
  char tbuf[256], raStr[80], decStr[80], timeStr[80], scanStr[80], dateStr[256];

  if(a)
  {
    setPaperSize(a->paperw, a->paperh, psize, fsize);
  }
  else
  {
    setPaperSize(0.0, 0.0, psize, fsize);
  }

  sexagesimal(h->xsource / 15.0, raStr, DD_MM_SS);
  sexagesimal(h->ysource, decStr, DD_MM_SS);
  sexagesimal(h->inttime / 3600.0, timeStr, DD_MM_SS);

  if(a && (a->bscan != 0 && a->escan != 0))
  {
    sprintf(scanStr, "(%3d) Scans: %7.2f-%7.2f", a->scanct, a->bscan, a->escan);
  }
  else
  {
    sprintf(scanStr, "%7.2f                    ", h->scan);
  }

  strcpy(dateStr, makeDate(h->utdate));

  sprintf(tbuf, "%12.12s %s INT=%s   DATE: %s\\n", h->object, scanStr, timeStr, dateStr);

//  log_msg("# Header Line-1 len: %d", strlen(tbuf));
  strcpy(buf, tbuf);

  sprintf(tbuf, "%8.8s  %s  %s                      CAL= %6.1f  TS= %5.0f\\n",
	h->coordcd, raStr, decStr, h->tcal, h->stsys);
//  log_msg("# Header Line-2 len: %d", strlen(tbuf));
  strcat(buf, tbuf);

  sprintf(tbuf, "FREQ= %9.2f  SYN= %10.8f  VEL= %8.1f                       SB=%d",
	h->obsfreq, h->synfreq/1000.0, h->velocity, (int)h->sideband);
//  log_msg("# Header Line-3 len: %d", strlen(tbuf));
  strcat(buf, tbuf);

  return(0);
}

  /* Draw the plot */
int displayFFT(fft, how)
struct FFT_INFO *fft;
int how;
{
  int   i;
  FILE *fp;
  char  datName[256], demName[256], 
        sname[256], headBuf[512], gpBuf[256];

  /* Write out datafile first */
  sprintf(datName, "/tmp/%s/scan.dat", session);

  if((fp = fopen(datName, "w")) == NULL)
  {
    log_msg("Unable to open %s for writting", datName);

    return(0);
  }

  /* Write Results */
  for(i=0;i<fft->num;i++)
  {
    fprintf(fp, "%e %e\n", fft->xdata[i], fabs(fft->ydata[i]));
  }

  fclose(fp);

  /* Now Write out gnuplot command file */
  sprintf(demName, "/tmp/%s/scan.dem", session);

  if((fp = fopen(demName, "w")) == NULL)
  {
    log_msg("Unable to open %s for writting", demName);

    return(0);
  }

  strncpy(sname, fft->head.object, 16);
  sname[16] = '\0';

  makeFFTHeader(fft, headBuf);

  fprintf(fp, "reset\n");
  fprintf(fp, "set size 1.0, 1.0\n");

  fprintf(fp, "set title \"%s\" font \"%s\" noenhanced\n", headBuf, fsize);

  fprintf(fp, "set xlabel \"Inverse Frequency (MHz^{-1})\" tc rgb \"red\"\n");
  fprintf(fp, "set ylabel \" \" tc rgb \"red\"\n");

  if(fft->xset)
  {
    fprintf(fp, "set xrange [%e:%e]\n", fft->xb, fft->xe);
  }

  fprintf(fp, "set grid\n");

  fprintf(fp, "set key off\n");

  fprintf(fp, "set terminal x11 enhanced %d font \"%s\" position %d,%d\n", fft->plotwin, fsize, startx, fft->plotwin*10);

  fprintf(fp, "plot '%s' with histeps lc rgb \"red\" \n", datName);

  fclose(fp);

  sprintf(gpBuf, "load \"/tmp/%s/scan.dem\"", session);
  writeGnuplot(gpBuf, 0);

  bcopy((char *)fft, (char *)&last_fft, sizeof(last_fft));

  return(0);
}


  /* Draw the plot */
int displayHisto(histo, how)
struct HISTO_INFO *histo;
int how;
{
  int   i;
  FILE *fp;
  char  datName[256], demName[256], gname[256],
        sname[256], headBuf[512], gpBuf[256];

  /* Write out datafile first */
  sprintf(datName, "/tmp/%s/scan.dat", session);

  if((fp = fopen(datName, "w")) == NULL)
  {
    log_msg("Unable to open %s for writting", datName);

    return(0);
  }

  /* Write Results */
  for(i=0;i<histo->num;i++)
  {
    fprintf(fp, "%e %e\n", histo->xdata[i], histo->ydata[i]);
  }

  fclose(fp);

  if(histo->gset)
  {
    /* Write out gaussian data */
    sprintf(gname, "/tmp/%s/gauss.dat", session);

    if((fp = fopen(gname, "w")) == NULL)
    {
      log_msg("Unable to open %s for writting", gname);

      return(0);
    }

    /* Write Results */
    for(i=0;i<histo->num;i++)
    {
      fprintf(fp, "%e %e\n", histo->xdata[i], histo->gdata[i]);
    }

    fclose(fp);
  }

  /* Now Write out gnuplot command file */
  sprintf(demName, "/tmp/%s/scan.dem", session);

  if((fp = fopen(demName, "w")) == NULL)
  {
    log_msg("Unable to open %s for writting", demName);

    return(0);
  }

  strncpy(sname, histo->head.object, 16);
  sname[16] = '\0';

  makeHistoHeader(histo, headBuf);

  fprintf(fp, "reset\n");
  fprintf(fp, "set size 1.0, 1.0\n");

  fprintf(fp, "set title \"%s\" font \"%s\" noenhanced\n", headBuf, fsize);

  fprintf(fp, "set xlabel \"Intensity\" tc rgb \"red\"\n");
  fprintf(fp, "set ylabel \" Quantity\" tc rgb \"red\"\n");

  fprintf(fp, "set grid\n");

  fprintf(fp, "set key off\n");

  fprintf(fp, "set terminal x11 enhanced %d font \"%s\" position %d,%d\n", histo->plotwin, fsize, startx, histo->plotwin*10);

  fprintf(fp, "plot '%s' with histeps lc rgb \"red\" ", datName);

  if(histo->gset)
  {
    fprintf(fp, ", '%s' with lines lc rgb \"dark-magenta\"", gname);
  }

  fprintf(fp, "\n");

  fclose(fp);

  sprintf(gpBuf, "load \"/tmp/%s/scan.dem\"", session);
  writeGnuplot(gpBuf, 0);

  bcopy((char *)histo, (char *)&last_histo, sizeof(last_histo));

  return(0);
}


int logImageInfo(image)
struct LP_IMAGE_INFO *image;
{
  int i;
  char bstr[4096], tbuf[256];

  log_msg("# ");
  log_msg("# struct LP_IMAGE_INFO image");

  log_msg("# Magic      = %s",   image->magic);
  log_msg("# Name       = %s",   image->name);
  log_msg("# Mode       = %d",   image->mode);
  log_msg("# Peak       = %d",   image->peakChan);
  log_msg("# Plotwin    = %d",   image->plotwin);
  log_msg("# NArray     = %d",   image->narray);
  log_msg("# nSize      = %d",   image->nsize);
  log_msg("# Datamin    = %f",   image->datamin);
  log_msg("# Datamax    = %f",   image->datamax);

  log_msg("# struct ACCUM_INFO ainfo");

  log_msg("# ----> ScanCt     = %d",   image->ainfo.scanct); 
  log_msg("# ----> Bscan      = %.2f", image->ainfo.bscan); 
  log_msg("# ----> Escan      = %.2f", image->ainfo.escan); 

  log_msg("# struct BASELINE_INFO binfo");

  strcpy(bstr, "Baselines  = ");

  for(i=0;i<MAX_BASELINES;i++)
  {
    if(image->binfo.baselines[i][0] > 0)
    {
      sprintf(tbuf, "%d ", image->binfo.baselines[i][0]);
      strcat(bstr, tbuf);
    }

    if(image->binfo.baselines[i][1] > 0)
    {
      sprintf(tbuf, "%d ", image->binfo.baselines[i][1]);
      strcat(bstr, tbuf);
    }
  }

  log_msg("# ----> DoBaseline = %d",   image->binfo.doBaseLine); 
  log_msg("# ----> %s", bstr);
  log_msg("# ----> a          = %f",   image->binfo.a); 
  log_msg("# ----> b          = %f",   image->binfo.b); 
  log_msg("# ----> rms        = %f",   image->binfo.rms); 

  log_msg("# struct PLOT_INFO pinfo");

  log_msg("# ----> Markers    = %d",   image->pinfo.markers); 
  log_msg("# ----> Plotstyle  = %d",   image->pinfo.plotStyle); 
  log_msg("# ----> X1Mode     = %d",   image->pinfo.x1mode); 
  log_msg("# ----> X2Mode     = %d",   image->pinfo.x2mode); 
  log_msg("# ----> Gridmode   = %d",   image->pinfo.gridMode); 
  log_msg("# ----> Zline      = %d",   image->pinfo.zline); 
  log_msg("# ----> Badchshow  = %d",   image->pinfo.badchshow); 
  log_msg("# ----> Bmark      = %d",   image->pinfo.bmark); 
  log_msg("# ----> Bshow      = %d",   image->pinfo.bshow); 
  log_msg("# ----> Mshow      = %d",   image->pinfo.mshow); 
  log_msg("# ----> Bdrop      = %d",   image->pinfo.bdrop); 
  log_msg("# ----> Edrop      = %d",   image->pinfo.edrop); 
  log_msg("# ----> DoBadChan  = %d",   image->pinfo.dobadch); 
  log_msg("# ----> Yscale     = %d",   image->pinfo.yscale); 
  log_msg("# ----> Plot_noint = %d",   image->pinfo.plot_noint); 
  log_msg("# ----> Bplot      = %d",   image->pinfo.bplot); 
  log_msg("# ----> Splot      = %d",   image->pinfo.splot); 
  log_msg("# ----> YPlotMode  = %d",   image->pinfo.yplotmode); 

  log_msg("# ----> PlotW      = %d",   image->pinfo.paperw); 
  log_msg("# ----> PlotH      = %d",   image->pinfo.paperh); 

  log_msg("# ----> Yupper     = %f",   image->pinfo.yupper); 
  log_msg("# ----> Ylower     = %f",   image->pinfo.ylower); 
  log_msg("# ----> PlotMin    = %f",   image->pinfo.plotmin); 
  log_msg("# ----> PlotMax    = %f",   image->pinfo.plotmax); 

  log_msg("# struct GAUSSIAN ginfo");

  log_msg("# ----> NFits      = %d",   image->ginfo.nfits); 

  for(i=0;i<image->ginfo.nfits;i++)
  {
    log_msg("# ----> [%d] Peak      = %f",   i, image->ginfo.fits[i].peak); 

    switch(image->ginfo.fits[i].xmode)
    {
	case XMODE_VEL:
    	  log_msg("# ----> [%d] Peak Vel  = %f",   i, image->ginfo.fits[i].center); 
	  break;

	case XMODE_CHAN:
    	  log_msg("# ----> [%d] Peak Chan = %f",   i, image->ginfo.fits[i].center); 
	  break;

	case XMODE_FREQ:
    	  log_msg("# ----> [%d] Peak Freq = %f",   i, image->ginfo.fits[i].center); 
	  break;
    }

    log_msg("# ----> [%d] Peak HP   = %f",   i, image->ginfo.fits[i].hp); 
    log_msg("# ----> [%d] Sum Gauss = %f",   i, image->ginfo.fits[i].sumK); 

  }

  log_msg("# ");

  return(0);
}


int makeOSB(h)
struct HEADER *h;
{
  if(fabs(h->openpar[0] - 54321.12345) < 0.001)
  {
    strcpy(osbStr, "(OSB)");
  }
  else
  {
    strcpy(osbStr, " ");
  }

  return(0);
}



int placeGauss(image, fp, print)
struct LP_IMAGE_INFO *image;
FILE *fp;
int print;
{
  int i;
  char lbuf[256], lcr[256], *font;
  struct IMAGE_GAUSS *p;

  if(image->satActive)
  {
    p = &sat.gauss;

    if(!p->used)
    {
      return(0);
    }

    font = "5x8";

    for(i=0;i<MAX_IMAGE_GAUSS_RESULTS;i++)
    {
      if(p->results[i].used)
      {
        sprintf(lbuf, "%s", p->results[i].string);
	strcpy(lcr, "left");

        fprintf(fp, "set label \"%s\" at %.6f, %.6f %s tc rgb \"%s\" font \"%s\"\n", 
				lbuf, 
				p->results[i].x, 
				p->results[i].y,
				lcr,
				ansi_colors[p->color], font);

      }
    }
  }


  return(0);
}



int placeLabels(image, fp, print)
struct LP_IMAGE_INFO *image;
FILE *fp;
int print;
{
  int i, hlabel = 0;
  float y, diff;
  char lbuf[256], lcr[256], *font;
  struct IMAGE_LABELS *p;

  if(image->satActive)
  {
    p = &sat.labels[0];

    for(i=0;i<MAX_IMAGE_LABELS;i++, p++)
    {
      if(p->used)
      {
        hlabel = 0;

	if(strstr(p->label, "Helvet"))
	{
	  font = " ";
	}
	else
	{
	  font = "font \"Times-Roman,10\"";
	}

        if(p->direct == LABEL_LEFT)
        {
          sprintf(lbuf, "%s {/Symbol \256}", p->label); /* --> */
	  strcpy(lcr, "right");
        }
        else
        if(p->direct == LABEL_RIGHT)
        {
          sprintf(lbuf, "{/Symbol \254} %s", p->label); /* <-- */
	  strcpy(lcr, "left");
        }
        else
        if(p->direct == LABEL_DOWN)
        {
          sprintf(lbuf, "{/Symbol \254} %s", p->label); /* <-- */
	  strcpy(lcr, "rotate by 90");
        }
        else
        {
          sprintf(lbuf, "{/Symbol \254}");
	  strcpy(lcr, "rotate by 90");
	  hlabel = 1;
        }

	/* Overwrite if no head */
        if(p->nohead == TRUE)
        {
          sprintf(lbuf, "%s", p->label);
	  strcpy(lcr, "left");
	  hlabel = 0;
        }

        fprintf(fp, "set label \"%s\" at %.6f, %.6f %s tc rgb \"%s\" %s\n", 
				lbuf, 
				p->x, 
				p->y,
				lcr,
				ansi_colors[p->color], font);

        if(hlabel)
        {

	  if(image->pinfo.yscale == Y_AUTO_SCALE) 	/* Use datamax - datamin */
          {
	    diff = image->datamax - image->datamin;
          }
	  else						/* Use yupper - ulower */
          {
	    diff = image->pinfo.yupper - image->pinfo.ylower;
	  }

	  y = p->y + diff * 0.04;

	  log_msg("Placing label %s", p->label);

	  if(strstr(p->label, "\\n"))  			/* Check for double high label */
	  {
	    log_msg("BUMPING LABEL");

	    y += diff * 0.04; 				/* Raise it some more */
	  }

	  strcpy(lcr, "center");
          fprintf(fp, "set label \"%s\" at %.6f, %.6f %s tc rgb \"%s\" %s\n", 
				p->label, 
				p->x, 
				y,
				lcr,
				ansi_colors[p->color], font);
        }
      }
    }
  }

  return(0);
}


int placeTBlock(image, fp, print)
struct LP_IMAGE_INFO *image;
FILE *fp;
int print;
{
  int i;
//  float y, diff;
  char lbuf[256], lcr[256], *font;
  struct TITLE_BLOCK  *t;
  struct IMAGE_LABELS *p;

  t = &sat.tblock;
  p = &t->labels[0];

  if(image->satActive && t->used)
  {
    for(i=0;i<MAX_TBLOCK_LABELS;i++, p++)
    {
      if(p->used)
      {
	font = "Times-Roman,10";

        sprintf(lbuf, "%s", p->label);
	strcpy(lcr, "left");

        fprintf(fp, "set label \"%s\" at %.6f, %.6f %s tc rgb \"%s\" font \"%s\"\n", 
				lbuf, 
				p->x, 
				p->y,
				lcr,
				ansi_colors[p->color], font);

      }
    }
  }

  return(0);
}


/* Mark any bad channels */
int placeBadChans(image, fp)
struct LP_IMAGE_INFO *image;
FILE *fp;
{
  int i;
  float delta;

  if(image->pinfo.badchshow)
  {
    delta = image->datamax - image->datamin;
    delta *= 0.05;

    for(i=1;i<(image->head.noint-1);i++)
    {
      if(isBadChan(image, i))
      {
        fprintf(fp, "set arrow from  %.3f, %.3f to  %.3f, %.3f front nohead lw 0.1 lc rgb \"green\"\n", 
			image->x1data[i], image->yplot1[i] - delta, 
			image->x1data[i], image->yplot1[i] + delta);
      }
    }
    log_msg("# Turning On BadChan Markers");
  }

  return(0);
}


int placeChanMarkers(image, fp)
struct LP_IMAGE_INFO *image;
FILE *fp;
{
  int i;

  if(image->pinfo.mshow && image->pinfo.markers)
  {
    for(i=image->pinfo.bdrop;i<(image->head.noint-image->pinfo.edrop);i++)
    {
      if(i > 0 && !(i%image->pinfo.markers))
      {
        fprintf(fp, "set arrow from  %.3f, %.3f to  %.3f, %.3f nohead lc rgb \"green\"\n", 
			image->x1data[i], image->pinfo.plotmin, 
			image->x1data[i], image->pinfo.plotmax);
      }
    }
    log_msg("# Turning On Markers");
  }

  return(0);
}



int placeMarkers(image, fp)
struct LP_IMAGE_INFO *image;
FILE *fp;
{
  int i;

  if(image->satActive)
  {
    for(i=0;i<MAX_IMAGE_MARKERS;i++)
    {
      if(sat.markers[i].used)
      {
        fprintf(fp, "set arrow from  %.6f, %.6f to  %.6f, %.6f front nohead lw 0.3 lc rgb \"%s\"\n", 
		sat.markers[i].x, sat.markers[i].y, 
		sat.markers[i].x, sat.markers[i].y2,
		ansi_colors[sat.markers[i].color]);
      }
    }
  }

  return(0);
}


int placeHlines(image, fp)
struct LP_IMAGE_INFO *image;
FILE *fp;
{
  int i, n;

  n = image->head.noint - 1;
  if(image->satActive)
  {
    for(i=0;i<MAX_IMAGE_HLINES;i++)
    {
      if(sat.hlines[i].used)
      {
        fprintf(fp, "set arrow from  %.6f, %.6f to  %.6f, %.6f front nohead lw 0.1 lc rgb \"%s\"\n", 
		image->x1data[0], sat.hlines[i].value, 
		image->x1data[n], sat.hlines[i].value,
		ansi_colors[sat.hlines[i].color]);
      }
    }
  }

  return(0);
}


int placeForks(image, fp, how)
struct LP_IMAGE_INFO *image;
FILE *fp;
int how;
{
  int i, j;
  float x, y, midpoint;
  char lbuf[256], lcr[80], *font;

  if(image->satActive)
  {
    for(i=0;i<MAX_IMAGE_FORKS;i++)
    {
      if(sat.forks[i].used)
      {
	/* Draw Tines */
        for(j=0;j<sat.forks[i].ntines;j++)
        {
          fprintf(fp, "set arrow from  %.6f, %.6f to  %.6f, %.6f front nohead lw 0.2 lc rgb \"%s\"\n", 
		sat.forks[i].tines[j].x, sat.forks[i].tines[j].y, 
		sat.forks[i].tines[j].x, sat.forks[i].crossT,
		ansi_colors[sat.forks[i].color]);
        }

	/* Draw Cross-Bar */
        fprintf(fp, "set arrow from  %.6f, %.6f to  %.6f, %.6f front nohead lw 0.2 lc rgb \"%s\"\n", 
		sat.forks[i].tines[0].x,                     sat.forks[i].crossT, 
		sat.forks[i].tines[sat.forks[i].ntines-1].x, sat.forks[i].crossT,
		ansi_colors[sat.forks[i].color]);

	/* Draw Fork Handle */
        midpoint = (sat.forks[i].tines[0].x + sat.forks[i].tines[sat.forks[i].ntines-1].x) / 2.0;

        fprintf(fp, "set arrow from  %.6f, %.6f to  %.6f, %.6f front nohead lw 0.2 lc rgb \"%s\"\n", 
		midpoint, sat.forks[i].crossT, 
		midpoint, sat.forks[i].topT,
		ansi_colors[sat.forks[i].color]);

	font = "Times-Roman,10";

	/* Draw Label */
        sprintf(lbuf, "%s", sat.forks[i].label);
        strcpy(lcr, "center");

//        len = strlen(lbuf);
        x = midpoint; /* Goes with 'center' above */

	/* Need more cleaver calc here */
        y = sat.forks[i].topT + (sat.forks[i].topT - sat.forks[i].crossT) * 0.5;

        fprintf(fp, "set label \"%s\" at %.6f, %.6f %s tc rgb \"%s\" font \"%s\"\n", 
				lbuf, 
				x, 
				y,
				lcr,
				ansi_colors[sat.forks[i].color],
				font);

      }
    }
  }

  return(0);
}


int placeDate(image, fp)
struct LP_IMAGE_INFO *image;
FILE *fp;
{
  char lcr[80];

  if(image->satActive)
  {
    if(sat.date.used)
    {
      strcpy(lcr, "left");

      log_msg("Placing Date, %s @ %f %f", sat.date.date, sat.date.x, sat.date.y);

      fprintf(fp, "set label \"%s\" at %.6f, %.6f %s tc rgb \"%s\" font \"fixed\"\n", 
				sat.date.date, 
				sat.date.x, 
				sat.date.y,
				lcr,
				ansi_colors[sat.date.color]);
    }
  }

  return(0);
}


int getSize(s)
int s;
{
  int size = 12;

  if(s == 1)
  {
    size -= 8;
  }
  else
  if(s == 2)
  {
    size -= 6;
  }
  else
  if(s == 3)
  {
    size -= 4;
  }
  else
  if(s == 4)
  {
    size -= 2;
  }
  else
  if(s == 5)
  {
    ;
  }
  else
  if(s == 6)
  {
    size += 2;
  }
  else
  if(s == 7)
  {
    size += 4;
  }
  else
  if(s == 8)
  {
    size += 6;
  }
  else
  if(s == 9)
  {
    size += 8;
  }

  return(size);
}



int placePstamps(image, fp)
struct LP_IMAGE_INFO *image;
FILE *fp;
{
  int    i, k=50;
  char   tbuf[256];
  struct STAMP *p;
  char *cp;

  if(image->pinfo.nightmode == ANSI_BLACK)
  {
    cp = "gray90";
  }
  else
  {
    cp = "gray40";
  }

  if(sat.pstamp.nstamps)
  {
    p = &sat.pstamp.stamps[0];
    for(i=0;i<MAX_PSTAMPS;i++,p++)
    {
      if(p->used)
      {
        fprintf(fp, "set object %d rect from %.6f,%.6f to %.6f,%.6f back fs empty border rgb \"%s\"\n", k+i,
		p->stampULx, p->stampULy, 
		p->stampLRx, p->stampLRy, cp);

        log_msg("# Drawing Postage Stamp: %d", i);

        fprintf(fp, "set label \"%s\" at %f, %f center tc rgb \"%s\"\n", 
				p->label, 
				(p->stampULx +  p->stampLRx) / 2.0, 
				 p->stampULy + (p->stampULy - p->stampLRy) / 15.0,
				 ansi_colors[p->color]);

	sprintf(tbuf, "%5.3fK", p->ymax);

        fprintf(fp, "set label \"%s\" at %f, %f left tc rgb \"%s\"\n", tbuf, p->stampLRx, p->stampULy, ansi_colors[p->color]);
      }
    }
  }

  return(0);
}


int placeBmark(image, fp)
struct LP_IMAGE_INFO *image;
FILE *fp;
{
  int i, k;
  double diff, y1, y2;
  char *cp;

  if(image->pinfo.nightmode == ANSI_BLACK)
  {
    cp = "gray90";
  }
  else
  {
    cp = "gray40";
  }

  if(image->pinfo.bmark)
  {
    diff = (image->pinfo.plotmax - image->pinfo.plotmin) / 7.0;

    if(image->pinfo.bmark > 0) /* +/- about zero */
    {
      y1 = +diff * 0.2;
      y2 = -diff * 0.2;
    }
    else			/* Place near bottom of plot */
    {

      y1 = image->pinfo.plotmin + diff * 0.2;
      y2 = image->pinfo.plotmin;
    }

    log_msg("Plot_interval = %f", diff);
    log_msg("Y1            = %f", y1);
    log_msg("Y2            = %f", y2);

    for(i=0,k=1;i<MAX_BASELINES;i++,k++)
    {
      if(image->binfo.baselines[i][0] == 0 || image->binfo.baselines[i][1] == 0)
      {
        continue;
      }

      fprintf(fp, "set object %d rect from %.6f,%.6f to %.6f,%.6f back fs empty border rgb \"%s\"\n", k,
		image->x1data[image->binfo.baselines[i][0]], y1, 
		image->x1data[image->binfo.baselines[i][1]], y2, cp);
    }

    log_msg("# Turning On Basline Markers");
  }

  return(0);
}


int placeWindows(image, fp)
struct LP_IMAGE_INFO *image;
FILE *fp;
{
  int i, k;
  char *cp;

  if(image->pinfo.nightmode == ANSI_BLACK)
  {
    cp = "green";
  }
  else
  {
    cp = "magenta";
  }

  if(image->winfo.nwindows)
  {
    for(i=0,k=100;i<MAX_WINDOWS;i++,k++)
    {
      if(image->winfo.windows[i].used && image->winfo.windows[i].active)
      {
        fprintf(fp, "set object %d rect from %.6f,%.6f to %.6f,%.6f back fs empty border rgb \"%s\"\n", k,
		image->winfo.windows[i].xmin, 
		image->winfo.windows[i].ymin, 
		image->winfo.windows[i].xmax, 
		image->winfo.windows[i].ymax, cp);
      }
    }

    log_msg("# Turning On Window");
  }

  return(0);
}


int placeShellLabels(image, fp)
struct LP_IMAGE_INFO *image;
FILE *fp;
{
  int i, k;
  double x1, x2, x, y;
  char *cp, lcr[80], label[256];
  struct SHELL_FIT *p;

  cp = "green";

  p = &image->sinfo.fits[0];
  for(i=0,k=200;i<MAX_SHELL_FITS;i++,k++,p++)
  {
    if(image->sinfo.fits[i].used && image->sinfo.fits[i].active)
    {
      x1 = image->x1data[p->left_edge_chan];
      x2 = image->x1data[p->right_edge_chan];
       x = (x1+x2)        / 2.0;
       y = p->temperature / 2.0;

	  strcpy(lcr, "center");
          sprintf(label, "{/Helvetica=14 %d }", i);
          fprintf(fp, "set label \"%s\" at %.6f, %.6f %s tc rgb \"%s\"\n", 
				label, 
				x, y,
				lcr, cp);
    }
  }

  return(0);
}


/*
  default char *papersize = "10, 7.5";
 */
int setPaperSize(w, h, psize, fsize)
double  w, h;
char *psize, *fsize;
{
  log_msg("setPaperSize(): PlotW = %f, PlotH = %f", w, h);

  if(w > 0.0 && h > 0.0)
  {
    sprintf(psize, "%.1f, %.1f", w, h);

    if(w > 24)
    {
      sprintf(fsize, "Courier,20");
    }
    else
    if(w > 18)
    {
      sprintf(fsize, "Courier,18");
    }
    else
    if(w > 14)
    {
      sprintf(fsize, "Courier,14");
    }
    else
    {
      sprintf(fsize, "Courier,12");
    }

    log_msg("setPaperSize(): Papersize: %s, Font: %s", psize, fsize);

    return(1);
  }
  else
  {
    strcpy(psize, papersize);
    strcpy(fsize, "9x15");
  }

  log_msg("setPaperSize(): Papersize: %s, Font: %s", psize, fsize);

  return(0);
}


int makeGfile(image, gname, fp)
struct LP_IMAGE_INFO *image;
char *gname;
FILE *fp;
{
  int i, k;
  double xd, a[4], dyda[4], y;

  sprintf(gname, "/tmp/%s/gline.dat", session);

  if((fp = fopen(gname, "w")) == NULL)
  {
    log_msg("Unable to open %s for writting", gname);

    return(1);
  }

  bzero((char *)&image->gplot[0], sizeof(float) * MAX_CHANS); /* Clear the gplot array */

  log_msg("# Creating G-Line-Line File");

  for(k=0;k<MAX_GAUSSIAN_FITS;k++)
  {
    if(image->ginfo.fits[k].active)
    {
      a[0] = 0.0;
      a[1] = image->ginfo.fits[k].peak;
      a[2] = image->ginfo.fits[k].center;
      a[3] = image->ginfo.fits[k].hp;

      for(i=0;i<image->head.noint;i++)
      {
        xd = (double)image->x1data[i];

        fgauss(xd, &a[0], &y, &dyda[0]);

        image->gplot[i] += y;
      }
    }
  }

  for(i=0;i<image->head.noint;i++)
  {
    fprintf(fp, "%e %e\n", image->x1data[i], image->gplot[i]);
  }

  fclose(fp);

  return(0);
}

/*
        evaluate least squares polynomial
*/
double leasev(double *c, int degree, double x)
{
  double temp;
  int i;

    /*
     * Commented out - refer to "Numerical Methods in C" for the
     * reason, section 5.2! The revolution has begun.
    temp = 0.0;
    for (i = 0; i <= degree; i++) {
        if ((i == 0) && (x == 0.0)) {
            temp = temp + c[i];
        } else {
            temp = temp + c[i] * pow(x, (double) (i));
        }
    } */

  temp = c[degree];

  for(i=degree-1;i>=0;i--)
  {
    temp = c[i] + temp*x;
  }

  return (temp);
}



int makeSfile(image, sname, fp)
struct LP_IMAGE_INFO *image;
char *sname;
FILE *fp;
{
  int i, j;
  struct SHELL_FIT *p;

  bzero((char *)&last_image.splot[0], sizeof(last_image.splot));

  sprintf(sname, "/tmp/%s/sline.dat", session);

  if((fp = fopen(sname, "w")) == NULL)
  {
    log_msg("Unable to open %s for writting", sname);

    return(1);
  }

  log_msg("# Creating S-Line-Line File");

  p = &image->sinfo.fits[0];
  for(i=0;i<MAX_SHELL_FITS;i++,p++)
  {
    if(p->used && p->active)
    {
      for(j=p->peak_left_chan-10;j<p->peak_right_chan+10;j++)
      {
        if(j >= (p->left_edge_chan) && j <= (p->right_edge_chan))
        {
          last_image.splot[j] += leasev(p->coeff, 2, image->x1data[j]);
        }
        else
        {
          last_image.splot[j] += 0.0;
        }
      }
    }
  }

  for(i=0;i<image->head.noint;i++)
  {
    fprintf(fp, "%e %e\n", image->x1data[i], last_image.splot[i]);
  }

  fclose(fp);

  return(0);
}


int makeSGfile(sgname, fp)
char *sgname;
FILE *fp;
{
  int i;

  sprintf(sgname, "/tmp/%s/sgline.dat", session);

  if((fp = fopen(sgname, "w")) == NULL)
  {
    log_msg("Unable to open %s for writting", sgname);

    return(1);
  }

  log_msg("# Creating Shell Guess File");

  for(i=0;i<shell_guess.npoints;i++)
  {
    fprintf(fp, "%e %e\n", shell_guess.xd[i], shell_guess.yd[i]);
  }

  fclose(fp);

  return(0);
}

int makeSFfile(sfname, fp)
char *sfname;
FILE *fp;
{
  int i;

  sprintf(sfname, "/tmp/%s/sfline.dat", session);

  if((fp = fopen(sfname, "w")) == NULL)
  {
    log_msg("Unable to open %s for writting", sfname);

    return(1);
  }

  log_msg("# Creating Shell Guess Fit  File");

  for(i=0;i<shell_guess.npoints;i++)
  {
    fprintf(fp, "%e %e\n", shell_guess.xd[i], shell_guess.fit[i]);
  }

  fclose(fp);

  return(0);
}


int makeSBfile()
{
  int i;
  char fname[256];
  FILE *fp;

  sprintf(fname, "/tmp/%s/sbline.dat", session);

  if((fp = fopen(fname, "w")) == NULL)
  {
    log_msg("Unable to open %s for writting", fname);

    return(1);
  }

  log_msg("# Creating Full Shell Guess Fit  File");

  for(i=0;i<shell_guess.npoints;i++)
  {
    fprintf(fp, "%e %e %e\n", shell_guess.xd[i], shell_guess.yd[i], shell_guess.fit[i]);
  }

  fclose(fp);

  return(0);
}


int makeBfile(image, bshape, fp)
struct LP_IMAGE_INFO *image;
char *bshape;
FILE *fp;
{
  int i;

  sprintf(bshape, "/tmp/%s/bsplot.dat", session);

  if((fp = fopen(bshape, "w")) == NULL)
  {
    log_msg("Unable to open %s for writting", bshape);

    return(1);
  }

  log_msg("# Creating Baseline-Shape File");

  for(i=0;i<image->head.noint;i++)
  {
    fprintf(fp, "%e %e\n", image->x1data[i], image->bplot[i]);
  }

  fclose(fp);

  return(0);
}


int makeBshow(image, bname)
struct LP_IMAGE_INFO *image;
char *bname;
{
  int i;
  FILE *fp;

  sprintf(bname, "/tmp/%s/bline.dat", session);

  if((fp = fopen(bname, "w")) == NULL)
  {
    log_msg("Unable to open %s for writting", bname);

    return(0);
  }

  log_msg("# Creating Baseline-Line File");

  for(i=0;i<image->head.noint;i++)
  {
    fprintf(fp, "%e %e\n", 
		image->x1data[i], 
		(float)i * image->binfo.a + image->binfo.b);
  }

  fclose(fp);

  return(0);
}


int makeZline(image, zname)
struct LP_IMAGE_INFO *image;
char *zname;
{
  int i;
  FILE *fp;

  sprintf(zname, "/tmp/%s/zline.dat", session);

  if((fp = fopen(zname, "w")) == NULL)
  {
    log_msg("Unable to open %s for writting", zname);

    return(0);
  }

  log_msg("# Creating Z-Line File");

  for(i=0;i<image->head.noint;i++)
  {
    fprintf(fp, "%e 0.0\n", image->x1data[i]);
  }

  fclose(fp);

  return(0);
}



int makeHeaderAndLabels(image, fp, mode, how)
struct LP_IMAGE_INFO *image;
FILE *fp;
int mode, how;
{
  char sname[256], headBuf[1024];

  strncpy(sname, image->head.object, 16);
  sname[16] = '\0';

  makeLineHeader(image, headBuf);

  fprintf(fp, "reset\n");
  fprintf(fp, "set size 1.0, 1.0\n");

  setPaperSize(image->pinfo.paperw, image->pinfo.paperh, psize, fsize);

  if(mode == SINGLE)
  {
    fprintf(fp, "set title \"%s\" font \"%s\" noenhanced\n", headBuf, fsize);
  }
  else /* MULTI */
  {
    if(image->pinfo.xstart || image->pinfo.ystart)
    {
      fprintf(fp, "set terminal x11 enhanced %d font \"%s\" position %d,%d\n", image->plotwin, psize, image->pinfo.xstart, image->pinfo.ystart);
    }
    else
    {
      fprintf(fp, "set terminal x11 enhanced %d font \"%s\" position %d,%d\n", image->plotwin, psize, startx, image->plotwin*10);
    }
  
    fprintf(fp, "set multiplot title \"%s\" layout 1,2 \n", headBuf);
  }

  if(image->xaxisotfoff == 1)
  {
    fprintf(fp, "set xlabel \"R.A. Offset (arcsec)\" tc rgb \"red\"\n");
  }
  else
  if(image->xaxisotfoff == 2)
  {
    fprintf(fp, "set xlabel \"Dec Offset (arcsec)\" tc rgb \"red\"\n");
  }
  else
  if(image->pinfo.x1mode == XMODE_FREQ)
  {
    fprintf(fp, "set xlabel \"%sFreq Offset (MHz)\" tc rgb \"red\"\n", osbStr);
  }
  else
  if(image->pinfo.x1mode == XMODE_VEL)
  {
    fprintf(fp, "set xlabel \"%sVelocity (%8.8s)\" tc rgb \"red\"\n", osbStr, image->head.veldef);
  }
  else
  if(image->pinfo.x1mode == XMODE_CHAN)
  {
    fprintf(fp, "set xlabel \"%sChannel Number\" tc rgb \"red\"\n", osbStr);
  }

  if(image->xaxisotfoff == 1)
  {
    fprintf(fp, "set x2label \"R.A. Offset (arcsec)\" tc rgb \"red\"\n");
  }
  else
  if(image->xaxisotfoff == 2)
  {
    fprintf(fp, "set x2label \"Dec Offset (arcsec)\" tc rgb \"red\"\n");
  }
  else
  if(image->pinfo.x2mode == XMODE_FREQ)
  {
    fprintf(fp, "set x2label \"%sFreq Offset (MHz)\" tc rgb \"red\"\n", osbStr);
  }
  else
  if(image->pinfo.x2mode == XMODE_VEL)
  {
    fprintf(fp, "set x2label \"%sVelocity (%8.8s)\" tc rgb \"red\"\n", osbStr, image->head.veldef);
  }
  else
  if(image->pinfo.x2mode == XMODE_CHAN)
  {
    fprintf(fp, "set x2label \"%sChannel Number\" tc rgb \"red\"\n", osbStr);
  }

  if(image->pinfo.yplotmode == LOG_PLOT)
  {
    if(!image->issmt && image->head.utdate < 2013.07) /* After new 12M telescope and removal of T / etafss */
    {
      fprintf(fp, "set ylabel \"log({/Symbol T}_{R} (K))\" tc rgb \"red\"\n");
    }
    else
    {
      fprintf(fp, "set ylabel \"log({/Symbol T}_{/Symbol A}* (K))\" tc rgb \"red\"\n");
    }
  }
  else
  {
    if(strlen(image->title))
    {
      fprintf(fp, "set ylabel \"%s\" tc rgb \"red\"\n", image->title);
    }
    else
    if(!image->issmt && image->head.utdate < 2013.07) /* After new 12M telescope and removal of T / etafss */
    {
      fprintf(fp, "set ylabel \"{/Symbol T}_{R} (K)\" tc rgb \"red\"\n");
    }
    else
    {
      fprintf(fp, "set ylabel \"{/Symbol T}_{/Symbol A}* (K)\" tc rgb \"red\"\n");
    }
  }

  if(image->pinfo.gridMode == GRID_MODE_NONE)
  {
    fprintf(fp, "unset grid\n");
  }
  else
  if(image->pinfo.gridMode == GRID_MODE_X_ONLY)
  {
    fprintf(fp, "set grid noytics xtics\n");
  }
  else
  if(image->pinfo.gridMode == GRID_MODE_Y_ONLY)
  {
    fprintf(fp, "set grid ytics noxtics\n");
  }
  else
  {
    fprintf(fp, "set grid\n");
  }

  fprintf(fp, "set key off\n");
  fprintf(fp, "set xrange [%e:%e]\n", 
		image->x1data[image->pinfo.bdrop], 
		image->x1data[(int)image->head.noint-1-image->pinfo.edrop]);

  fprintf(fp, "set x2tics out\n");
  fprintf(fp, "set x2range [%e:%e]\n", 
		image->x2data[image->pinfo.bdrop], 
		image->x2data[(int)image->head.noint-1-image->pinfo.edrop]);

  fprintf(fp, "set yrange [%e:%e]\n", image->pinfo.plotmin, image->pinfo.plotmax);

  sendPlotRange( image->x1data[image->pinfo.bdrop],
		 image->x1data[(int)image->head.noint-1-image->pinfo.edrop],
		 image->pinfo.plotmin,
		 image->pinfo.plotmax);

  return(0);
}



  /* Draw the plot */
int linePlot_1(image, how)
struct LP_IMAGE_INFO *image;
int how;
{
  int   i, j, k;
  FILE *fp;
  double x, xscale, xmax, xmin, xstart;
  double y, yscale, ymax, ymin, yoffset;

  char  datName[256], 
	demName[256], 
	bname[256], 
	zname[256], 
	gname[256], 
	sname[256], 
	sgname[256], 
	sfname[256], 
	gpBuf[256], 
	bshape[256],
	psname[256],
	*cp;

  struct STAMP *p;

  makeOSB(&image->head);

  /* Go fill in x-axis values */
  computeXaxis(image, 1);

  /* Write out datafile first */
  sprintf(datName, "/tmp/%s/scan.dat", session);

  if((fp = fopen(datName, "w")) == NULL)
  {
    log_msg("Unable to open %s for writting", datName);

    return(1);
  }

  /* Write Results */
  for(i=0;i<image->head.noint;i++)
  {
    fprintf(fp, "%e %e\n", image->x1data[i], image->yplot1[i]);
  }

  fclose(fp);

  if(sat.pstamp.nstamps)
  {
    p = &sat.pstamp.stamps[0];

    for(j=0;j<MAX_PSTAMPS;j++, p++)
    {
      if(p->used)
      {
        sprintf(psname, "/tmp/%s/pstamp-%d.dat", session, j);

        if((fp = fopen(psname, "w")) == NULL)
        {
          log_msg("Unable to open %s for writting", psname);

          return(1);
        }

	xmax = -999999.9;
	xmin =  999999.9;
	ymax = -999999.9;
	ymin =  999999.9;

	/* find max value for this segment */
        for(i=0;i<image->head.noint;i++)
        {
 		/* Only consider Values inside the stamp */
	  if((image->x1data[i] >= p->linexmin && image->x1data[i] <= p->linexmax) || 
             (image->x1data[i] <= p->linexmin && image->x1data[i] >= p->linexmax))
	  {
	    if(image->x1data[i] > xmax)
            {
              xmax = image->x1data[i];
            }

	    if(image->x1data[i] < xmin)
            {
              xmin = image->x1data[i];
            }

	    if(image->yplot1[i] > ymax)
            {
              ymax = image->yplot1[i];
            }

	    if(image->yplot1[i] < ymin)
            {
              ymin = image->yplot1[i];
            }

	  }
	}

        if(p->linexmin < p->linexmax) /* Increasing xaxis */
        {
	  xscale = (p->stampULx-p->stampLRx) / (xmax-xmin);
	  xstart = p->stampLRx;
        }
        else
        {
	  xscale = (p->stampULx-p->stampLRx) / (xmin-xmax);
	  xstart = p->stampULx;
        }

        xscale *= 0.75;

	log_msg("(%2d): xmin = %9.3f, xmax = %9.3f, xstart = %9.3f, stampUx = %9.3f, stampLx = %9.3f,  xscale = %9.3f", j,
			xmin, xmax, xstart, p->stampULx, p->stampLRx, xscale);

	yscale = (p->stampULy-p->stampLRy) / (ymax-ymin);
        yscale *= 0.98;

	if(ymin < 0.0)
        {
	  yoffset = fabs(ymin);
        }
        else
        {
	  yoffset = -ymin;
        }

        yoffset *= yscale;

	log_msg("(%2d): ymin = %9.3f, ymax = %9.3f, yoffst = %9.3f, stampUy = %9.3f, stampLy = %9.3f,  yscale = %9.3f", j,
					ymin, ymax, yoffset, p->stampULy, p->stampLRy, yscale);

        p->ymax = ymax;

	  /* Write Results */
        k = 0;
        for(i=0;i<image->head.noint;i++)
        {
          if(p->linexmin < p->linexmax) /* Increaing xaxis */
	  {
	    if(image->x1data[i] >= p->linexmin && image->x1data[i] <= p->linexmax)
	    {
	      x = xstart - (double)k * image->head.deltax * xscale;

	      y = yoffset + p->stampLRy + (image->yplot1[i] * yscale);

              k++;

              fprintf(fp, "%e %e\n", x, y);
	    }
	  }
	  else
	  {
	    if(image->x1data[i] >= p->linexmax && image->x1data[i] <= p->linexmin)
	    {
	      x = xstart + (double)k * image->head.deltax * xscale;

	      y = yoffset + p->stampLRy + (image->yplot1[i] * yscale);

              k++;

              fprintf(fp, "%e %e\n", x, y);
	    }
	  }
        }

        fclose(fp);
      }
    }
  }

  /* compute min and max for ploting */
  find_ticks(image);

  if(image->pinfo.zline)
  {
    if(makeZline(image, zname))
    {
      return(1);
    }
  }

  if(image->pinfo.bshow)
  {
    if(makeBshow(image, bname))
    {
      return(1);
    }
  }

  if(image->pinfo.bplot)
  {
    if(makeBfile(image, bshape, fp))
    {
      return(1);
    }
  }

  if(image->sinfo.nshells)
  {
    if(makeSfile(image, sname, fp))
    {
      return(1);
    }
  }

  if(shell_guess.npoints)
  {
    if(makeSGfile(sgname, fp))
    {
      return(1);
    }

    if(makeSFfile(sfname, fp))
    {
      return(1);
    }

    makeSBfile();
  }

  if(image->ginfo.nfits)
  {
    if(makeGfile(image, gname, fp))
    {
      return(1);
    }
  }

  /* Now Write out gnuplot command file */
  sprintf(demName, "/tmp/%s/scan.dem", session);

  if((fp = fopen(demName, "w")) == NULL)
  {
    log_msg("Unable to open %s for writting", demName);

    return(1);
  }

  makeHeaderAndLabels(image, fp, SINGLE, how);

  placeBmark(image, fp);
  placeWindows(image, fp);
  placeChanMarkers(image, fp);
  placeBadChans(image, fp);
  placeLabels(image, fp, how);
  placeTBlock(image, fp, how);
  placeGauss(image, fp, how);
  placeMarkers(image, fp);
  placeHlines(image, fp);
  placeForks(image, fp, how);
  placeDate(image, fp);
  placePstamps(image, fp);

  if(image->sinfo.nshells && image->sinfo.label)
  {
    placeShellLabels(image, fp);
  }

  setPaperSize(image->pinfo.paperw, image->pinfo.paperh, psize, fsize);

  if(image->pinfo.xstart || image->pinfo.ystart)
  {
    fprintf(fp, "set terminal x11 enhanced %d font \"%s\" position %d,%d\n", image->plotwin, fsize, image->pinfo.xstart, image->pinfo.ystart);
  }
  else
  {
    fprintf(fp, "set terminal x11 enhanced %d font \"%s\" position %d,%d\n", image->plotwin, fsize, startx, image->plotwin*10);
  }

  fprintf(fp, "plot '%s' with %s lc rgb \"red\"", datName, plotStyleStrs[image->pinfo.plotStyle]);

  if(image->pinfo.bshow)
  {
    fprintf(fp, ", '%s' with lines lc rgb \"blue\"", bname);
  }

  if(image->pinfo.bplot)
  {
    fprintf(fp, ", '%s' with lines lc rgb \"magenta\"", bshape);
  }

  if(image->pinfo.nightmode == ANSI_BLACK)
  {
    cp = "green";
  }
  else
  {
    cp = "magenta";
  }

  if(image->pinfo.zline)
  {
    fprintf(fp, ", '%s' with lines lc rgb \"blue\"", zname);
  }

  if(image->sinfo.nshells)					/* Plot this after zline so the complete line shows */
  {
/* linespoints ps 5 pt 4 */
    fprintf(fp, ", '%s' with lines lc rgb \"%s\"", sname, cp);
  }

  if(shell_guess.npoints)					/* Plot the shell guess if any */
  {
/* linespoints ps 5 pt 4 */
    fprintf(fp, ", '%s' with linespoints dt 2 ps 5 pt 4 lc rgb \"yellow\"", sgname);
    fprintf(fp, ", '%s' with lines lc rgb \"%s\"", sfname, cp);
  }

  if(image->ginfo.nfits)
  {
    fprintf(fp, ", '%s' with lines lc rgb \"dark-magenta\"", gname);
  }

  if(sat.pstamp.nstamps)
  {
    p = &sat.pstamp.stamps[0];

    for(j=0;j<MAX_PSTAMPS;j++, p++)
    {
      if(p->used)
      {
        fprintf(fp, ", '/tmp/%s/pstamp-%d.dat' with lines lc rgb \"red\"", session, j);
      }
    }
  }

  fprintf(fp, "\n"); /* Close it out */

  fclose(fp);

  sprintf(gpBuf, "load \"/tmp/%s/scan.dem\"", session);
  writeGnuplot(gpBuf, image->xdelay);

#ifdef SAVE_DEM_FILE
  /* Now save the .dem file for diagnostics */

  if(1)
  {
    int  pid;
    char sysbuf[256];

    pid = rand();
    pid = pid%32767;

    sprintf(sysbuf, "cp /tmp/%s/scan.dem /tmp/%s/scan-%d.dem", session, session, pid);

    system(sysbuf);
  }
#endif

  bcopy((char *)image, (char *)&last_image, sizeof(last_image));

  logImageInfo(image);

  return(0);
} /* linePlot_1 */



  /* Draw a parallel plot */
int linePlot_2(image, how)
struct LP_IMAGE_INFO *image;
int how;
{
  int   i;
  FILE *fp;
  char  datName1[256], datName2[256], demName[256], bname[256], zname[256], gpBuf[256];

  makeOSB(&image->head);

  /* Go fill in x-axis values */
  computeXaxis(image, 1);

  /* Write out First datafile first */
  sprintf(datName1, "/tmp/%s/scan1.dat", session);

  if((fp = fopen(datName1, "w")) == NULL)
  {
    log_msg("Unable to open %s for writting", datName1);

    return(0);
  }

  log_msg("Writing Data file #1: %s", datName1);

  /* Write Results */
  for(i=0;i<image->head.noint;i++)
  {
    fprintf(fp, "%e %e\n", image->x1data[i], image->yplot1[i]);
  }

  fclose(fp);

  /* Write out Second datafile first */
  sprintf(datName2, "/tmp/%s/scan2.dat", session);

  if((fp = fopen(datName2, "w")) == NULL)
  {
    log_msg("Unable to open %s for writting", datName2);

    return(0);
  }

  log_msg("Writing Data file #2: %s", datName2);

  /* Write Results */
  for(i=0;i<image->head.noint;i++)
  {
    fprintf(fp, "%e %e\n", image->x1data[i], image->yplot2[i]);
  }

  fclose(fp);


  /* compute min and max for ploting */
  find_ticks(image);

  if(image->pinfo.zline)
  {
    if(makeZline(image, zname))
    {
      return(1);
    }
  }

  if(image->pinfo.bshow)
  {
    if(makeBshow(image, bname))
    {
      return(1);
    }
  }

  /* Now Write out gnuplot command file */
  sprintf(demName, "/tmp/%s/scan.dem", session);

  if((fp = fopen(demName, "w")) == NULL)
  {
    log_msg("Unable to open %s for writting", demName);

    return(0);
  }

  makeHeaderAndLabels(image, fp, MULTI, how);

  placeBmark(image, fp);
  placeWindows(image, fp);
  placeChanMarkers(image, fp);
  placeBadChans(image, fp);
  placeLabels(image, fp, how);
  placeTBlock(image, fp, how);
  placeGauss(image, fp, how);
  placeMarkers(image, fp);
  placeHlines(image, fp);
  placeForks(image, fp, how);
  placeDate(image, fp);

  fprintf(fp, "plot '%s' with %s lc rgb \"red\"", datName1, plotStyleStrs[image->pinfo.plotStyle]);

  if(image->pinfo.bshow)
  {
    fprintf(fp, ", '%s' with lines lc rgb \"blue\"", bname);
  }

  if(image->pinfo.zline)
  {
    fprintf(fp, ", '%s' with lines lc rgb \"blue\"", zname);
  }

  fprintf(fp, "\n");

  fprintf(fp, "plot '%s' with %s lc rgb \"red\"", datName2, plotStyleStrs[image->pinfo.plotStyle]);

  if(image->pinfo.bshow)
  {
    fprintf(fp, ", '%s' with lines lc rgb \"blue\"", bname);
  }

  if(image->pinfo.zline)
  {
    fprintf(fp, ", '%s' with lines lc rgb \"blue\"", zname);
  }

  fprintf(fp, "\n");

  fprintf(fp, "unset multiplot\n");

  fclose(fp);

  sprintf(gpBuf, "load \"/tmp/%s/scan.dem\"", session);

  writeGnuplot(gpBuf, image->xdelay);

  bcopy((char *)image, (char *)&last_image, sizeof(last_image));

  logImageInfo(image);

  return(0);
} /* linePlot_2 */



  /* Draw a quad plot */
int linePlot_4(image, how)
struct LP_IMAGE_INFO *image;
int how;
{
  int   i, j;
  char  datName1[256], datName2[256], *cp, datName3[256], datName4[256], 
	demName[256], sname[256], headBuf[512], gpBuf[256], zname[256];
  float *f;
  FILE *fp;

  makeOSB(&image->head);

  /* Write out the datafiles */
  for(j=0;j<4;j++)
  {
    switch(j)
    {
      case 0: cp = datName1; f = image->yplot1; break;
      case 1: cp = datName2; f = image->yplot2; break;
      case 2: cp = datName3; f = image->yplot3; break;
      case 3: cp = datName4; f = image->yplot4; break;
    }

    sprintf(cp, "/tmp/%s/scan%d.dat", session, j+1);

    if((fp = fopen(cp, "w")) == NULL)
    {
      log_msg("Unable to open %s for writting", cp);

      return(0);
    }

    log_msg("Writing Data file #%d: %s", j+1, cp);

    /* Go fill in x-axis values using the proper values */
    image->head.scan      = image->mhp[j].scan;
    image->head.restfreq  = image->mhp[j].restfreq;
    image->head.stsys     = image->mhp[j].stsys;
    image->head.tcal      = image->mhp[j].tcal;
    image->head.freqres   = image->mhp[j].freqres;
    image->head.sideband  = image->mhp[j].sideband;
    image->head.refpt     = image->mhp[j].refpt;
    image->head.refpt_vel = image->mhp[j].refpt_vel;
    image->head.deltax    = image->mhp[j].deltax;

    computeXaxis(image, 1);

    for(i=0;i<image->head.noint;i++,f++)
    {
      fprintf(fp, "%e %e\n", image->x1data[i], *f);
    }

    fclose(fp);
  }

  /* Write Results */

  /* compute min and max for ploting */
  find_ticks(image);

  if(image->pinfo.zline)
  {
    if(makeZline(image, zname))
    {
      return(1);
    }
  }

  /* Now Write out gnuplot command file */
  sprintf(demName, "/tmp/%s/scan.dem", session);

  if((fp = fopen(demName, "w")) == NULL)
  {
    log_msg("Unable to open %s for writting", demName);

    return(0);
  }

  strncpy(sname, image->head.object, 16);
  sname[16] = '\0';

  /* Put everything back */
  image->head.scan      = image->mhp[0].scan;
  image->head.restfreq  = image->mhp[0].restfreq;
  image->head.stsys     = image->mhp[0].stsys;
  image->head.tcal      = image->mhp[0].tcal;
  image->head.freqres   = image->mhp[0].freqres;
  image->head.sideband  = image->mhp[0].sideband;
  image->head.refpt     = image->mhp[0].refpt;
  image->head.refpt_vel = image->mhp[0].refpt_vel;
  image->head.deltax    = image->mhp[0].deltax;

  makeLineHeader(image, headBuf);

  fprintf(fp, "reset\n");
  fprintf(fp, "set size 1.0, 1.0\n");

  fprintf(fp, "set terminal x11 enhanced %d font \"%s\" position %d,%d\n", image->plotwin, fsize, startx, image->plotwin*10);

  fprintf(fp, "set multiplot title \"%s\" layout 2,2\n", headBuf);
//  fprintf(fp, "set multiplot title \"%s\" noenhanced layout 2,2\n", headBuf);
//  fprintf(fp, "set title \"%s\" font \"%s\"\n", headBuf, fsize);

  if(image->pinfo.x1mode == XMODE_FREQ)
  {
    fprintf(fp, "set xlabel \"%sFreq Offset (MHz)\" tc rgb \"red\"\n", osbStr);
  }
  else
  if(image->pinfo.x1mode == XMODE_VEL)
  {
    fprintf(fp, "set xlabel \"%sVelocity (%8.8s)\" tc rgb \"red\"\n", osbStr, image->head.veldef);
  }
  else
  if(image->pinfo.x1mode == XMODE_CHAN)
  {
    fprintf(fp, "set xlabel \"%sChannel Number\" tc rgb \"red\"\n", osbStr);
  }

  if(image->pinfo.x2mode == XMODE_FREQ)
  {
    fprintf(fp, "set x2label \"%sFreq Offset (MHz)\" tc rgb \"red\"\n", osbStr);
  }
  else
  if(image->pinfo.x2mode == XMODE_VEL)
  {
    fprintf(fp, "set x2label \"%sVelocity (%8.8s)\" tc rgb \"red\"\n", osbStr, image->head.veldef);
  }
  else
  if(image->pinfo.x2mode == XMODE_CHAN)
  {
    fprintf(fp, "set x2label \"%sChannel Number\" tc rgb \"red\"\n", osbStr);
  }

  if(image->pinfo.yplotmode == LOG_PLOT)
  {
    fprintf(fp, "set ylabel \"log(Temp (K))\" tc rgb \"red\"\n");
  }
  else
  {
    fprintf(fp, "set ylabel \"Temp (K)\" tc rgb \"red\"\n");
  }

  if(image->pinfo.gridMode == GRID_MODE_NONE)
  {
    fprintf(fp, "unset grid\n");
  }
  else
  if(image->pinfo.gridMode == GRID_MODE_X_ONLY)
  {
    fprintf(fp, "set grid noytics xtics\n");
  }
  else
  if(image->pinfo.gridMode == GRID_MODE_Y_ONLY)
  {
    fprintf(fp, "set grid ytics noxtics\n");
  }
  else
  {
    fprintf(fp, "set grid\n");
  }

  fprintf(fp, "set key off\n");
  fprintf(fp, "set x2tics out\n");
  fprintf(fp, "set yrange [%e:%e]\n", image->pinfo.plotmin, image->pinfo.plotmax);

  sendPlotRange( image->x1data[image->pinfo.bdrop],
		 image->x1data[(int)image->head.noint-1-image->pinfo.edrop],
		 image->pinfo.plotmin,
		 image->pinfo.plotmax);

  placeBmark(image, fp);
  placeWindows(image, fp);

  for(j=0;j<4;j++)
  {
    switch(j)
    {
      case 0: cp = datName1; break;
      case 1: cp = datName2; break;
      case 2: cp = datName3; break;
      case 3: cp = datName4; break;
    }

    /* Go fill in x-axis values using the proper values */
    image->head.scan      = image->mhp[j].scan;
    image->head.restfreq  = image->mhp[j].restfreq;
    image->head.stsys     = image->mhp[j].stsys;
    image->head.tcal      = image->mhp[j].tcal;
    image->head.freqres   = image->mhp[j].freqres;
    image->head.sideband  = image->mhp[j].sideband;
    image->head.refpt     = image->mhp[j].refpt;
    image->head.refpt_vel = image->mhp[j].refpt_vel;
    image->head.deltax    = image->mhp[j].deltax;

    computeXaxis(image, 1);

    fprintf(fp, "set xrange [%e:%e]\n", 
		image->x1data[image->pinfo.bdrop], 
		image->x1data[(int)image->head.noint-1-image->pinfo.edrop]);
  
    fprintf(fp, "set x2range [%e:%e]\n", 
		image->x2data[image->pinfo.bdrop], 
		image->x2data[(int)image->head.noint-1-image->pinfo.edrop]);

    fprintf(fp, "plot '%s' with %s lc rgb \"red\"", cp, plotStyleStrs[image->pinfo.plotStyle]);

    if(image->pinfo.zline)
    {
      fprintf(fp, ", '%s' with lines lc rgb \"blue\"", zname);
    }

    fprintf(fp, "\n");
  }

  fprintf(fp, "unset multiplot\n");

  fclose(fp);

  sprintf(gpBuf, "load \"/tmp/%s/scan.dem\"", session);
  writeGnuplot(gpBuf, image->xdelay);

  bcopy((char *)image, (char *)&last_image, sizeof(last_image));

  logImageInfo(image);

  return(0);
} /* linePlot_4 */


  /* Draw a Cross Correlation plot */
int plotXcor(image, how)
struct LP_IMAGE_INFO *image;
int how;
{
  int   i, j, dlen;
  FILE *fp;
  char  datName1[256], datName2[256], *cp,
	datName3[256], datName4[256],
	demName[256], sname[256], headBuf[512], gpBuf[256];
  float *f;

  dlen = image->nsize / sizeof(float);

  /* Write out the datafiles */
  for(j=0;j<4;j++)
  {
    switch(j)
    {
      case 0: cp = datName1; f = image->yplot1; break;
      case 1: cp = datName2; f = image->yplot2; break;
      case 2: cp = datName3; f = image->yplot3; break;
      case 3: cp = datName4; f = image->yplot4; break;
    }

    sprintf(cp, "/tmp/%s/scan%d.dat", session, j+1);

    if((fp = fopen(cp, "w")) == NULL)
    {
      log_msg("Unable to open %s for writting", cp);

      return(0);
    }

    log_msg("Writing Data file #%d: %s; len = %d", j+1, cp, dlen);

  /* Write Results; NOTE Xaxis is in the bplot array */
    for(i=0;i<dlen;i++,f++)
    {
      if(image->bplot[i] != 0.0)
      {
        fprintf(fp, "%e %e\n", image->bplot[i], *f);
      }
    }

    fclose(fp);
  }

  /* compute min and max for ploting */
  find_ticks(image);

  /* Now Write out gnuplot command file */
  sprintf(demName, "/tmp/%s/scan.dem", session);

  if((fp = fopen(demName, "w")) == NULL)
  {
    log_msg("Unable to open %s for writting", demName);

    return(0);
  }

  strncpy(sname, image->head.object, 16);
  sname[16] = '\0';

  makeLineHeader(image, headBuf);

  fprintf(fp, "reset\n");
  fprintf(fp, "set size 1.0, 1.0\n");

  fprintf(fp, "set terminal x11 enhanced %d font \"%s\" position %d,%d\n", image->plotwin, fsize, startx, image->plotwin*10);

  fprintf(fp, "set title \"%s\"font \"%s\" noenhanced\n", headBuf, fsize);

  fprintf(fp, "set xlabel \"IF #1 (Kelvins)\" tc rgb \"red\"\n");
  fprintf(fp, "set ylabel \"IF #2 (Kelvins)\" tc rgb \"red\"\n");

  fprintf(fp, "set key off\n");

  for(j=0;j<4;j++)
  {
    switch(j)
    {
      case 0: 
        fprintf(fp, "plot '%s' with %s lc rgb \"red\", ", datName1, "lp");
	break;
      case 1: 
        fprintf(fp, "'%s' with %s lc rgb \"red\", ", datName2, "lines");
	break;
      case 2: 
        fprintf(fp, "'%s' with %s lc rgb \"red\", ", datName3, "lines");
	break;
      case 3: 
        fprintf(fp, "'%s' with %s lc rgb \"red\"\n", datName4, "lines");
	break;
    }
  }

  fclose(fp);

  sprintf(gpBuf, "load \"/tmp/%s/scan.dem\"", session);
  writeGnuplot(gpBuf, image->xdelay);

  bcopy((char *)image, (char *)&last_image, sizeof(last_image));

  logImageInfo(image);

  return(0);
}


int find_ticks(image)
struct LP_IMAGE_INFO *image;
{
  double min, max, tickint, diff;
  double log10();
  int i, ticks;
  int order;
  float *f;

  f = image->yplot1;

  max = -1.0e127; /* there are probably standard macros, but this works */
  min = 9.99e127;

  for(i=image->pinfo.bdrop;i<image->head.noint-image->pinfo.edrop;i++)  /* compute min and max */
  {
    if(f[i] > max)
    {
      max = f[i];
      image->peakChan = i;
    }
    if(f[i] < min)
    {
      min = f[i];
    }
  }

  image->datamin = min;
  image->datamax = max;

  log_msg("# Max = %f, Min = %f", image->datamax, image->datamin);

  diff = max-min;
  if(diff > 0.0)
  {
    order = (int)rint(log10(diff))-1;
  }
  else
  {
    image->pinfo.plotmax = max;
    image->pinfo.plotmin =  min;

    if(image->pinfo.yscale == Y_FIXED_SCALE)
    {
      image->pinfo.plotmin = image->pinfo.ylower;
      image->pinfo.plotmax = image->pinfo.yupper;
    }

    plot_interval = (image->pinfo.plotmax - image->pinfo.plotmin) / 8.0;

    return(1);
  }

  tickint = pow(10.0, (double)order);
  ticks = (int)rint(diff/tickint);

  log_msg("# Tickint = %f, ticks = %d", tickint, ticks);

  if( ticks >= 10 )
  {
    if( ticks >= 20 )
    {
      ticks /= 5;
      tickint *= 5.0;
    }
    else
    {
      ticks /= 2;
      tickint *= 2.0;
    }
  }
  else
  if( ticks < 3 )
  {
    ticks *= 5;
    tickint /= 5.0;
  }

  log_msg("# Tickint = %f, ticks = %d", tickint, ticks);

  image->pinfo.plotmax = ((int)floor((max + tickint) / tickint)) * tickint;
  image->pinfo.plotmin = image->pinfo.plotmax - (ticks +2) * tickint;

  plot_interval = tickint;

  log_msg("# Max = %f, Min = %f", image->pinfo.plotmax, image->pinfo.plotmin);

  if(image->pinfo.yscale == Y_FIXED_SCALE)
  {
    image->pinfo.plotmin = image->pinfo.ylower;
    image->pinfo.plotmax = image->pinfo.yupper;
  }

  return(ticks);
}


int fluxPlot(flux, how)
struct FLUX_INFO *flux;
int how;
{
  int i;
  char datName[256], demName[256], gpBuf[256];
  FILE *fp;

 /* Write out datafile first */
  sprintf(datName, "/tmp/%s/scan.dat", session);

  if((fp = fopen(datName, "w")) == NULL)
  {
    log_msg("Unable to open %s for writting", datName);

    return(0);
  }

  for(i=0;i<flux->num;i++)
  {
    if(flux->values[i].freq > 0.0)
    {
      fprintf(fp, "%e %e\n", flux->values[i].freq, flux->values[i].tb);
    }
  }

  fclose(fp);


 /* Now Write out gnuplot command file */
  sprintf(demName, "/tmp/%s/scan.dem", session);

  if((fp = fopen(demName, "w")) == NULL)
  {
    log_msg("Unable to open %s for writting", demName);

    return(0);
  }

  fprintf(fp, "reset\n");
  fprintf(fp, "set size 1.0, 1.0\n");
  fprintf(fp, "set title \"%s Brightness Temperature\"\n", flux->name);
  fprintf(fp, "set xlabel \"Freq (GHz)\"\n");
  fprintf(fp, "set ylabel \"Brightness Temperature (K)\"\n");

  if(flux->manFreq)
  {
    fprintf(fp, "set xrange [%e:%e]\n", flux->bfreq, flux->efreq);
  }
  else
  {
    fprintf(fp, "set xrange [65:840]\n");
  }

  fprintf(fp, "set grid\n");

  fprintf(fp, "set terminal x11 enhanced %d font \"%s\" position %d,%d\n", flux->plotwin, fsize, startx, flux->plotwin*10);

  fprintf(fp, "plot \'%s' with lines\n", datName);

  fclose(fp);

  sprintf(gpBuf, "load \"/tmp/%s/scan.dem\"", session);
  writeGnuplot(gpBuf, 0);

  return(0);
}


int accumPlot(a, how)
struct ACCUM_RESULTS_SEND *a;
int how;
{
  int i, j, k, l, n, first=1, wh, bfound=0;
  char datName[256], demName[256], xlabel[256],
       ylabel[256], gpBuf[256], sbname[80], headBuf[512], coeffStr[256], tbuf[256];
  float x, y, *f, yy, center;
  float xmin=9999.0, xmax = -99999,  ymin = 99999.0, ymax = -99999.0, ydiff;
  FILE *fp;

 /* Write out datafile first */

  for(j=0;j<MAX_IFS;j++)
  {
    if(a->ar[j].num)
    {
      if(a->issmt)
      {
        strcpy(sbname, mg_sideband[j]);
      }
      else
      {
        strcpy(sbname, kp_sideband[j]);
      }

      sprintf(datName, "/tmp/%s/%s.dat", session, sbname);

      if((fp = fopen(datName, "w")) == NULL)
      {
        log_msg("Unable to open %s for writting", datName);

        return(0);
      }

      for(i=0;i<a->ar[j].num;i++)
      {
        x = 0.0;
	y = 0.0;

        switch(a->plot_select1)
        {
	  case ACCUM_PLOT_X_EL:   
		x = a->ar[j].results[i].el;
		sprintf(xlabel, "El (Degrees)");
		break;

	  case ACCUM_PLOT_X_UT:   
		x = a->ar[j].results[i].ut;
		sprintf(xlabel, "Time (UT)");
		break;

	  case ACCUM_PLOT_X_SCAN: 
		x = a->ar[j].results[i].scan;
		sprintf(xlabel, "Scan Number");
		break;

	  default: 
		x = (double)i; 
		sprintf(xlabel, "Sample Number");
		break;
        }

        if(x < xmin)
        {
          xmin = x;
        }

        if(x > xmax)
        {
          xmax = x;
        }


        switch(a->plot_select2)
        {
	  case ACCUM_PLOT_Y_SIG:    
		y = a->ar[j].results[i].sig;     
		sprintf(ylabel, "Signal Temperature (K)");
		break;

	  case ACCUM_PLOT_Y_SKY:    
		y = a->ar[j].results[i].sky;     
		sprintf(ylabel, "Sky Temperature (K)");
		break;

	  case ACCUM_PLOT_Y_SMROR:  
		y = a->ar[j].results[i].smror;   
		sprintf(ylabel, "Ratio: (Sig - Sky) / Sky");
		break;

	  case ACCUM_PLOT_Y_TSYS:   
		y = a->ar[j].results[i].tsys;    
		sprintf(ylabel, "System Temperature (K)");
		break;

	  case ACCUM_PLOT_Y_TASTAR: 
		y = a->ar[j].results[i].tastar;  
		sprintf(ylabel, "Temperature (Ta*) (K)");
		break;

	  case ACCUM_PLOT_Y_TR:     
		y = a->ar[j].results[i].tr;      
		sprintf(ylabel, "Temperature (Tr) (K)");
		break;

	  case ACCUM_PLOT_Y_JANSKY: 
		y = a->ar[j].results[i].janskys; 
		sprintf(ylabel, "Janskys");
		break;

	  case ACCUM_PLOT_Y_NORMAL: 
		y = a->ar[j].results[i].normalized; 
		sprintf(ylabel, "Normalized");
		break;

	  default:                  
		y = a->ar[j].results[i].janskys; 
		sprintf(ylabel, "Janskys");
		break;
        }

        if(y < ymin)
        {
          ymin = y;
        }

        if(y > ymax)
        {
          ymax = y;
        }

        fprintf(fp, "%e %e\n", x, y);
      }

      fclose(fp);
    }
  }

  if(a->plotExpected)
  {
    for(j=2;j<4;j++) /* Only plot last two */
    {
      sprintf(datName, "/tmp/%s/expected-%s.dat", session, sideband[j]);

      if((fp = fopen(datName, "w")) == NULL)
      {
        log_msg("Unable to open %s for writting", datName);

        return(0);
      }

      n = a->ar[j].num;

      for(i=0;i<n;i++)
      {
        switch(a->plot_select1)
        {
	  case ACCUM_PLOT_X_EL:   
		x = a->ar[j].results[i].el;
		break;

	  case ACCUM_PLOT_X_UT:   
		x = a->ar[j].results[i].ut;
		break;

	  case ACCUM_PLOT_X_SCAN: 
		x = a->ar[j].results[i].scan;
		break;

	  default: 
		x = (double)i; 
		break;
        }

        switch(a->plot_select2)
        {
	  case ACCUM_PLOT_Y_SIG:    
		y = 0.0;
		break;

	  case ACCUM_PLOT_Y_SKY:    
		y = 0.0;
		break;

	  case ACCUM_PLOT_Y_SMROR:  
		y = 0.0;
		break;

	  case ACCUM_PLOT_Y_TSYS:   
		y = 0.0;
		break;

	  case ACCUM_PLOT_Y_TASTAR: 
		y = a->expectedTaStar[j];
		break;

	  case ACCUM_PLOT_Y_TR:     
		y = a->expectedTr[j];
		break;

	  case ACCUM_PLOT_Y_JANSKY: 
		y = a->expectedJanskys[j];
		break;

	  case ACCUM_PLOT_Y_NORMAL: 
		y = 1.0;
		break;

	  default:                  
		y = a->expectedTaStar[j];
		break;
        }

        fprintf(fp, "%e %e\n", x, y);
      }

      fclose(fp);
    }
  }

  if(a->bplot)			/* Are there fitted curves to plot */
  {
    for(j=0;j<MAX_IFS;j++)
    {
      n = a->ar[j].num;

      if(n)
      {
        if(a->issmt)
        {
          sprintf(datName, "/tmp/%s/bplot-%s.dat", session, mg_sideband[j]);
        }
	else
	{
          sprintf(datName, "/tmp/%s/bplot-%s.dat", session, kp_sideband[j]);
	}

        if((fp = fopen(datName, "w")) == NULL)
        {
          log_msg("Unable to open %s for writting", datName);

          return(0);
        }


        f = &a->fitcurve[j][0];

        for(i=0;i<n;i++,f++)
        {
          switch(a->plot_select1)
          {
	    case ACCUM_PLOT_X_EL:   
		x = a->ar[j].results[i].el;
		break;

	    case ACCUM_PLOT_X_UT:   
		x = a->ar[j].results[i].ut;
		break;

	    case ACCUM_PLOT_X_SCAN: 
		x = a->ar[j].results[i].scan;
		break;

	    default: 
		x = (double)i; 
		break;
          }

          y = *f;

          fprintf(fp, "%e %e\n", x, y);
        }

        fclose(fp);

        bfound++;
      }
    }
  }

 /* Now Write out gnuplot command file */
  sprintf(demName, "/tmp/%s/scan.dem", session);

  if((fp = fopen(demName, "w")) == NULL)
  {
    log_msg("Unable to open %s for writting", demName);

    return(0);
  }

  log_msg("Valid = 0x%x", a->valid);

  wh = -1;
  if(a->valid)
  {
    for(k=0;k<MAX_IFS;k++)
    {
      if(a->valid & 1<<k)
      {
        wh = k;
        break;
      }
    }

    if(wh == -1)
    {
      log_msg("No Valid Headers");
      return(0);
    }

    makeContHeader(&a->head[wh], a, headBuf);
  }
  else
  {
    log_msg("No Valid Headers");
    return(0);
  }


  fprintf(fp, "reset\n");
  fprintf(fp, "set size 1.0, 1.0\n");

  fprintf(fp, "set title \"%s\" font \"%s\" noenhanced\n", headBuf, fsize);

  fprintf(fp, "set xlabel \"%s\"\n", xlabel);
  fprintf(fp, "set ylabel \"%s\"\n", ylabel);
  fprintf(fp, "set pointsize 2\n");
  fprintf(fp, "set grid\n");

	/* Place labels */
  if(a->ncoeff)
  {
    ydiff  = (ymax - ymin) / 10.0;
    center = (xmax - xmin);
    yy     = ymin  + ydiff*5.0;
    l      = 0;

    for(j=0;j<MAX_IFS;j++)
    {
      if(a->ar[j].num)			/* Valid data here ? */
      {
        if(a->issmt)
        {
          sprintf(coeffStr, "Coeff for %s ", mg_sideband[j]);
        }
	else
        {
          sprintf(coeffStr, "Coeff for %s ", kp_sideband[j]);
        }

        for(k=0;k<a->ncoeff+1;k++)
        {
          sprintf(tbuf, "a%d=%.3e ", k, a->coeff[j][k]);
	  strcat(coeffStr, tbuf);
        }

        log_msg("accumPlot(): %s", coeffStr);

	fprintf(fp, "set label %d \"%s\" at %f, %f tc lt %d font \"fixed\"\n", j+11, coeffStr, center * 0.60, yy, 3+l);

        yy -= ydiff;

        l++;

      }
    }
  }

  fprintf(fp, "set terminal x11 enhanced %d font \"%s\" position %d,%d\n", a->plotwin, fsize, startx, a->plotwin*10);

  fprintf(fp, "plot ");

  if(a->plotExpected)
  {
    for(j=2;j<MAX_IFS;j++)
    {
      if(!first)
      {
        fprintf(fp, ", ");
      }

      sprintf(datName, "/tmp/%s/expected-%s.dat", session, sideband[j]);

      fprintf(fp, "\'%s' with lp", datName);

      first = 0;
    }
  }

  for(j=0;j<MAX_IFS;j++)
  {
    if(a->ar[j].num)
    {
      if(!first)
      {
        fprintf(fp, ", ");
      }

      if(a->issmt)
      {
        strcpy(sbname, mg_sideband[j]);
      }
      else
      {
        strcpy(sbname, kp_sideband[j]);
      }

      sprintf(datName, "/tmp/%s/%s.dat", session, sbname);

      fprintf(fp, "\'%s' with lp", datName);

      first = 0;
    }
  }

  if(a->bplot && bfound)
  {
    for(j=0;j<MAX_IFS;j++)
    {
      if(a->ar[j].num)
      {
        if(!first)
        {
          fprintf(fp, ", ");
        }

        if(a->issmt)
        {
          strcpy(sbname, mg_sideband[j]);
        }
        else
        {
          strcpy(sbname, kp_sideband[j]);
        }

        sprintf(datName, "/tmp/%s/bplot-%s.dat", session, sbname);

        fprintf(fp, "\'%s' with lines", datName);

        first = 0;
      }
    }
  }

  fprintf(fp, "\n");

  fclose(fp);

  sprintf(gpBuf, "load \"/tmp/%s/scan.dem\"", session);
  writeGnuplot(gpBuf, 0);

  return(0);
}



int contPlot(cp, how)
struct CONTINUUM_PLAIN *cp;
int how;
{
  int i;
  char datName[256], demName[256], gpBuf[256], headBuf[512],
       gname[256], zname[256];
  FILE *fp;
  double max = -99999999.0, yy = 0.0;

  log_msg("Inside contPlot(): %d Data Points", cp->dlen);

  /* Write out datafile first */
  sprintf(datName, "/tmp/%s/scan.dat", session);

  if((fp = fopen(datName, "w")) == NULL)
  {
    log_msg("Unable to open %s for writting", datName);
    return(1);
  }

  for(i=0;i<cp->dlen;i++)
  {
    fprintf(fp, "%e %e\n", cp->xdata[i], cp->ydata[i]);
  }

  log_msg("Got %d datapoints & %d Gaussian points", cp->dlen, cp->ngauss);

  fclose(fp);

  if(cp->zline)
  {
    /* Write out datafile first */
    sprintf(zname, "/tmp/%s/zline.dat", session);

    if((fp = fopen(zname, "w")) == NULL)
    {
      log_msg("Unable to open %s for writting", zname);
      return(1);
    }

    for(i=0;i<cp->dlen;i++)
    {
      fprintf(fp, "%e %e\n", cp->xdata[i], 0.0);
    }

    fclose(fp);
  }

  if(cp->ngauss) /* Write out gaussian data */
  {
    log_msg("Inside contPlot(): %d Gaussian Data Points", cp->ngauss);

    sprintf(gname, "/tmp/%s/gauss.dat", session);

    if((fp = fopen(gname, "w")) == NULL)
    {
      st_fail("Unable to open %s for writting", gname);

      return(0);
    }

    for(i=0;i<cp->ngauss;i++)
    {
      fprintf(fp, "%e %e\n", cp->xdata[i], cp->gdata[i]);

      if(cp->gdata[i] > max)
      {
        max = cp->gdata[i];
      }
    }

    fclose(fp);
  }

  /* Now Write out gnuplot command file */
  sprintf(demName, "/tmp/%s/scan.dem", session);

  if((fp = fopen(demName, "w")) == NULL)
  {
    log_msg("Unable to open %s for writting", demName);

    return(1);
  }

  makeContHeader(&cp->head, NULL, headBuf);

  fprintf(fp, "reset\n");
  fprintf(fp, "set size 1.0, 1.0\n");

  fprintf(fp, "set title \"%s\" font \"%s\" noenhanced\n", headBuf, fsize);

  fprintf(fp, "set xlabel \"%s\"\n", cp->xlabel);
  fprintf(fp, "set ylabel \"%s\"\n", cp->ylabel);

  if(cp->gridMode == GRID_MODE_NONE)
  {
    fprintf(fp, "unset grid\n");
  }
  else
  if(cp->gridMode == GRID_MODE_X_ONLY)
  {
    fprintf(fp, "set grid noytics xtics\n");
  }
  else
  if(cp->gridMode == GRID_MODE_Y_ONLY)
  {
    fprintf(fp, "set grid ytics noxtics\n");
  }
  else
  {
    fprintf(fp, "set grid\n");
  }

  fprintf(fp, "set key off\n");

  fprintf(fp, "set xrange [%e:%e]\n", cp->xdata[0], cp->xdata[cp->dlen-1]);

  if(cp->autoscale == Y_FIXED_SCALE)
  {
    fprintf(fp, "set yrange [%e:%e]\n", cp->yplotmin, cp->yplotmax);
  }

  if(cp->xstart || cp->ystart)
  {
    fprintf(fp, "set terminal x11 enhanced %d font \"%s\" position %d,%d\n", cp->plotwin, fsize, cp->xstart, cp->ystart);
  }
  else
  {
    fprintf(fp, "set terminal x11 enhanced %d font \"%s\" position %d,%d\n", cp->plotwin, fsize, startx, cp->plotwin*10);
  }

  if(cp->ngauss && cp->yaxis_scale != LOG_PLOT) /* Labels are useless for LOG_PLOT */
  {
    yy = 0.97;
    for(i=0;i<MAX_GLABELS;i++)
    {
      if(strlen(cp->glabels[i]) > 2)
      {
        fprintf(fp, "set label %d \"%s\" at %f, %f tc rgb \"blue\" font \"fixed\"\n", i+11, cp->glabels[i], cp->xdata[1], max * yy);

	yy -= 0.05;
      }
    }
  }

  if(cp->yaxis_scale == LOG_PLOT)
  {
    fprintf(fp, "plot '%s' with lp lc rgb \"red\"", datName);
  }
  else
  {
    fprintf(fp, "plot '%s' with %s lc rgb \"red\"", datName, plotStyleStrs[cp->plotStyle]);
  }

  if(cp->ngauss)
  {
    fprintf(fp, ", '%s' with lines lc rgb \"blue\"", gname);
  }

  if(cp->zline)
  {
    fprintf(fp, ", '%s' with lines lc rgb \"blue\"", zname);
  }

  fprintf(fp, "\n");

  fclose(fp);

  sprintf(gpBuf, "load \"/tmp/%s/scan.dem\"", session);
  writeGnuplot(gpBuf, 0);

  return(0);
}


int plotFluxHistory(f, how)
struct FLUX_HISTORY *f;
int how;
{
  int i;
  FILE *fp;
  char datName[256], demName[256], gpBuf[256];

  log_msg("Plotting ALMA Calibrator %s", f->name);

  /* Write out datafile first */
  sprintf(datName, "/tmp/%s/scan.dat", session);

  if((fp = fopen(datName, "w")) == NULL)
  {
    st_fail("Unable to open %s for writting", datName);

    return(0);
  }

  for(i=0;i<f->num;i++)
  {
    fprintf(fp, "%s %e\n", f->history[i].date, f->history[i].flux);
  }

  fclose(fp);

  /* Now Write out gnuplot command file */
  sprintf(demName, "/tmp/%s/scan.dem", session);

  if((fp = fopen(demName, "w")) == NULL)
  {
    st_fail("Unable to open %s for writting", demName);

    return(0);
  }

  fprintf(fp, "reset\n");
  fprintf(fp, "set size 1.0, 1.0\n");

  fprintf(fp, "set title \"%s Flux Density History\"\n", f->name);
  fprintf(fp, "set xlabel \"Date\"\n");
  fprintf(fp, "set xdata time\n");
  fprintf(fp, "set timefmt \"%%Y/%%m/%%d\"\n");
  fprintf(fp, "set ylabel \"Flux Density (Jk)\"\n");
  fprintf(fp, "set grid\n");
  fprintf(fp, "set pointsize 2\n");
  fprintf(fp, "set key off\n");

  fprintf(fp, "set terminal x11 enhanced %d font \"%s\" position %d,%d\n", f->plotwin, fsize, startx, f->plotwin*10);

  fprintf(fp, "plot \'%s' using 1:2 with lp\n", datName);

  fclose(fp);

  log_msg("Plotting ALMA Calibrator %s", f->name);
  log_msg("Plotting %d ALMA calibrator measurements", f->num);
  log_msg("Plotting data: %s", f->datestr);

  sprintf(gpBuf, "load \"/tmp/%s/scan.dem\"", session);
  writeGnuplot(gpBuf, 0);

  return(0);
}


int plotFocusHistory(f, how)
struct FOCUS_HISTORY_SEND *f;
int how;
{
  int i, j, l;
  FILE *fp;
  char datName[256], bplotName[256], demName[256], gpBuf[256];
  double xx, yy, err, ymin=999999.9, ymax=-999999.9, ydiff;

  log_msg("plotFocusHistory(): Plotting Focus History");

  /* Write out datafile first */
  sprintf(datName, "/tmp/%s/focus-results.dat", session);

  if((fp = fopen(datName, "w")) == NULL)
  {
    st_fail("plotFocusHistory(): Unable to open %s for writting", datName);

    return(0);
  }

  for(i=0;i<f->nfocs;i++)
  {
    yy  = f->focuses[i].fit;
    err = f->focuses[i].cnterr;

    if(f->sort_order == FOCUS_SORTED_TAMB)
    {
      xx = f->focuses[i].tamb;
    }
    else
    {
      xx = f->focuses[i].el;
    }

    switch(f->scale)
    {
      case FOCUS_SCALE_ADD:
        yy  += f->focuses[i].scaling;
	break;

      case FOCUS_SCALE_SUB:
        yy  -= f->focuses[i].scaling;
	break;

      case FOCUS_SCALE_MULTI:
        yy  *= f->focuses[i].scaling;
        err *= f->focuses[i].scaling;
	break;

      case FOCUS_SCALE_DIVIDE:
        yy  /= f->focuses[i].scaling;
        err /= f->focuses[i].scaling;
	break;

      case FOCUS_SCALE_NONE:		/* Do nothing */
	break;
    }

    fprintf(fp, "%e %e %e\n", xx, yy, err);

    if(yy < ymin)
    {
      ymin = yy;
    }

    if(yy > ymax)
    {
      ymax = yy;
    }
  }

  fclose(fp);

  if(f->bfit)
  {
    sprintf(bplotName, "/tmp/%s/focus-fit.dat", session);

    if((fp = fopen(bplotName, "w")) == NULL)
    {
      st_fail("plotFocusHistory(): Unable to open %s for writting", bplotName);

      return(0);
    }

    for(i=0;i<f->nfocs;i++)
    {
      yy = f->bplot[i];

      if(f->sort_order == FOCUS_SORTED_TAMB)
      {
        xx = f->focuses[i].tamb;
      }
      else
      {
        xx = f->focuses[i].el;
      }

      switch(f->scale)
      {
        case FOCUS_SCALE_ADD:
          yy += f->focuses[i].scaling;
	  break;

        case FOCUS_SCALE_SUB:
          yy -= f->focuses[i].scaling;
	  break;

        case FOCUS_SCALE_MULTI:
          yy *= f->focuses[i].scaling;
	  break;

        case FOCUS_SCALE_DIVIDE:
          yy /= f->focuses[i].scaling;
	  break;

        case FOCUS_SCALE_NONE:		/* Do nothing */
	  break;
      }

      fprintf(fp, "%e %e\n", xx, yy);
    }

    fclose(fp);
  }

  /* Now Write out gnuplot command file */
  sprintf(demName, "/tmp/%s/focus.dem", session);

  if((fp = fopen(demName, "w")) == NULL)
  {
    st_fail("plotFocusHistory(): Unable to open %s for writting", demName);

    return(0);
  }

  fprintf(fp, "reset\n");
  fprintf(fp, "set size 1.0, 1.0\n");

  fprintf(fp, "set title \"%s Focus History\"\n", focus_axis_str[f->axis]);

  if(f->sort_order == FOCUS_SORTED_TAMB)
  {
    fprintf(fp, "set xlabel \"Tamb\"\n");
  }
  else
  {
    fprintf(fp, "set xlabel \"El\"\n");
  }

//  fprintf(fp, "set xrange [0:90]\n");

  fprintf(fp, "set ylabel \"Focus Fit (mm)\"\n");
  fprintf(fp, "set grid\n");

        /* Place labels, if any */
  ydiff     = (    ymax -     ymin) / 20.0;

  if(f->xlabel != 0.0)
  {
    xx = f->xlabel;
  }
  else
  {
    xx = 25.0;
  }

  if(f->ylabel != 0.0)
  {
     yy = f->ylabel;
  }
  else
  {
     yy = ymax;
  }

  l = 0;

  for(j=0;j<MAX_FOCUS_LABELS;j++)
  {
    if(strlen(f->blabels[j]) > 2)
    {
        /* These most likely is for focus curves so assume the plot os 0 - 90 degrees */
      fprintf(fp, "set label %d \"%s\" at %f, %f tc lt %d font \"6x10\"\n", j+11, f->blabels[j], xx, yy, 3/*+l*/);

      yy -= ydiff;

      l++;
    }
  }

//  fprintf(fp, "set key off\n");

  fprintf(fp, "set terminal x11 enhanced %d font \"%s\" position %d,%d\n", f->plotwin, fsize, startx, f->plotwin*10);

  if(f->lines)
  {
    fprintf(fp, "plot \'%s' with yerrorlines", datName);
  }
  else
  {
    fprintf(fp, "plot \'%s' with yerror", datName);
  }

  if(f->bfit)
  {
    fprintf(fp, ", \'%s' with lines", bplotName);
  }

  fprintf(fp, "\n");	/* Close it out */

  fclose(fp);

  log_msg("Plotting Focus History");
  log_msg("Plotting %d Focus measurements", f->nfocs);

  if(f->bfit)
  {
    log_msg("And, Plotting %d bplot values", f->nfocs);
  }

  sprintf(gpBuf, "load \"%s\"", demName);
  writeGnuplot(gpBuf, 0);

  return(0);
}


int contSptipPlot(cs, how)
struct CONTINUUM_SPTIP *cs;
int how;
{
  int chan, i, k;
  float *f;
  float sinel[11], delta_airmass, upper_airmass, lower_airmass, gfit0;
  char fitName[2][256], datName[2][256], demName[256], gpBuf[256], sname[80];
  char headBuf[512], *ftype;
  FILE *fp;

  log_msg("Inside contSptipPlot()");

                  /* does the header contain the stop and start airmasses ??? */
                                                           /* if so, use them */
  if(cs->head1.sptip_start > 0.0 && cs->head1.sptip_stop > 0.0)
  {
    lower_airmass = 1.0 / sin(cs->head1.sptip_start * DEG_TO_RAD);
    upper_airmass = 1.0 / sin(cs->head1.sptip_stop  * DEG_TO_RAD);

    delta_airmass  = (upper_airmass - lower_airmass) / 5.0;

    sinel[0]  = sinel[10] = lower_airmass;
    sinel[1]  = sinel[ 9] = lower_airmass + delta_airmass * 1.0;
    sinel[2]  = sinel[ 8] = lower_airmass + delta_airmass * 2.0;
    sinel[3]  = sinel[ 7] = lower_airmass + delta_airmass * 3.0;
    sinel[4]  = sinel[ 6] = lower_airmass + delta_airmass * 4.0;
    sinel[5]  =             upper_airmass;

  }
  else                       /* if not, it's an old sptip and use the default */
  {
    for(k=0;k<11;k++)
    {
      sinel[k] = sinEl[k];
    }
  }

  for(chan=0;chan<2;chan++)
  {
    /* Write out datafile first */
    sprintf(datName[chan], "/tmp/%s/scan%d.dat", session, chan);

    if((fp = fopen(datName[chan], "w")) == NULL)
    {
      log_msg("Unable to open %s for writting", datName[chan]);

      return(0);
    }

    if(chan == 0)
    {
      f = &cs->ydata1[0];
    }
    else
    {
      f = &cs->ydata2[0];
    }

    for(i=0;i<cs->dlen;i++,f++)
    {
      fprintf(fp, "%2d %e\n", i+1, *f);
    }

    fclose(fp);

    log_msg("Wrote Channel %d datafile %s", chan+1, datName[chan]);

    /* Write out fit file second */
    sprintf(fitName[chan], "/tmp/%s/tipfit%d.dat", session, chan);

    if((fp = fopen(fitName[chan], "w")) == NULL)
    {
      log_msg("Unable to open %s for writting", fitName[chan]);

      return(0);
    }

    for(i=0;i<cs->dlen;i++)
    {
      gfit0 = cs->results[chan].a + ( -cs->results[chan].b * sinel[i]);
      fprintf(fp, "%2d %e\n", i+1, gfit0);
    }

    fclose(fp);

    log_msg("Wrote Channel %d fitfile %s", chan+1, fitName[chan]);

    /* Now Write out gnuplot command file */
    sprintf(demName, "/tmp/%s/scan.dem", session);

    if(chan == 0)
    {
      ftype = "w";
    }
    else
    {
      ftype = "a";
    }

    if((fp = fopen(demName, ftype)) == NULL)
    {
      log_msg("Unable to open %s for writting", demName);

      return(0);
    }

    strncpy(sname, cs->head1.object, 16);
    sname[16] = '\0';

// TOM

    if(chan == 0)
    {
      makeContHeader(&cs->head1, NULL, headBuf);

      fprintf(fp, "reset\n");
      fprintf(fp, "set size 1.0, 1.0\n");
      fprintf(fp, "set title \"%s\" font \"%s\" noenhanced\n", headBuf, fsize);

      fprintf(fp, "set terminal x11 enhanced %d font \"%s\" position %d,%d\n", cs->plotwin, fsize, startx, cs->plotwin*10);

      fprintf(fp, "set size 1.0, 1.0\n");
      fprintf(fp, "set origin 0.0, 0.0\n");
      fprintf(fp, "set grid\n");
      fprintf(fp, "unset key\n");
      fprintf(fp, "set multiplot\n");
      fprintf(fp, "set xlabel \"Sample Number\"\n");
      fprintf(fp, "set ylabel \"Intensity\"\n");
      fprintf(fp, "set xrange [1:11]\n");
      fprintf(fp, "set size 1.0, 0.5\n");
      fprintf(fp, "set origin 0.0,0.5\n");

      fprintf(fp, "set label 11 \"Tau0     %.3f\" at 5.5, %.3f tc rgb \"red\" font \"fixed\"\n", 
					cs->results[chan].tau0, cs->ydata1[2]);
      fprintf(fp, "set label 12 \"Siga +/- %.3f\" at 5.5, %.3f tc rgb \"red\" font \"fixed\"\n", 
					cs->results[chan].sigmaA, cs->ydata1[1]);
      fprintf(fp, "set label 13 \"Sigb +/- %.3f\" at 5.5, %.3f tc rgb \"red\" font \"fixed\"\n", 
					cs->results[chan].sigmaB, cs->ydata1[0]);
    }
    else
    {
      makeContHeader(&cs->head2, NULL, headBuf);

      fprintf(fp, "set title \"%s\" font \"%s\" noenhanced\n", headBuf, fsize);
      fprintf(fp, "set origin 0.0,0.0\n");

      fprintf(fp, "set label 11 \"Tau0     %.3f\" at 5.5, %.3f tc rgb \"red\" font \"fixed\"\n", 
					cs->results[chan].tau0, cs->ydata2[2]);
      fprintf(fp, "set label 12 \"Siga +/- %.3f\" at 5.5, %.3f tc rgb \"red\" font \"fixed\"\n", 
					cs->results[chan].sigmaA, cs->ydata2[1]);
      fprintf(fp, "set label 13 \"Sigb +/- %.3f\" at 5.5, %.3f tc rgb \"red\" font \"fixed\"\n", 
					cs->results[chan].sigmaB, cs->ydata2[0]);
    }



    fprintf(fp, "plot \'%s' with histeps, \'%s' with lines\n", datName[chan], fitName[chan]);

    if(chan == 1)
    {
      fprintf(fp, "unset multiplot\n");
    }

    log_msg("Wrote %s in %s mode", demName, ftype);

    fclose(fp);
  }

  sprintf(gpBuf, "load \"/tmp/%s/scan.dem\"", session);
  writeGnuplot(gpBuf, 0);

  return(0);
}


int displayObjHistory(obj, how)
struct OBJECT *obj;
int how;
{
  int    i, j, k, l, used[MAX_IFS], first=1, ymode, xmode, lines;
  float  x1, y1, s1, n1, xx, yy;
  float  xmin=9999.0, xmax = -99999,  ymin = 99999.0, ymax = -99999.0, ydiff;
  float  dataymin = 99999.0, dataymax = -99999.0, dataydiff;
  double effSigma;
  char   datName[256], demName[256], gpBuf[256];
  char   coeffStr[256], tbuf[256];
  FILE   *fp;

  log_msg("Plotting Continuum Source %s", obj->source);
  log_msg("Object %s has %d Measurements", obj->source, obj->numMeas);

  setPaperSize(obj->paperw, obj->paperh, psize, fsize); /* Borrow the last_image for now */

  xmode = obj->xmode;
  ymode = obj->ymode;
  lines = obj->lines;

  /* Write out datafile first */
  for(j=0;j<MAX_IFS;j++)
  {
    if(obj->measurements[0].results[j].measuredFlux > 0.0)
    {
      used[j] = 1;
      sprintf(datName, "/tmp/%s/flux-%s.dat", session, mg_sideband[j]);

      if((fp = fopen(datName, "w")) == NULL)
      {
        st_fail("Unable to open %s for writting", datName);

        return(0);
      }

      n1 = obj->normalize[j];

      for(i=0;i<obj->numMeas;i++)
      {
	if(ymode == MODE_EFF)
        {
          effSigma = obj->measurements[i].results[j].measuredTmbSigma / 
                     obj->measurements[i].results[j].measuredTmb;

	  effSigma *= obj->measurements[i].results[j].beam_eff;

          if(xmode == MODE_NUM)
          {
            x1 = (float)(i+1);
          }
          else
          {
            x1 = obj->measurements[i].averageEl;
          }

	  y1 = obj->measurements[i].results[j].beam_eff;
	  s1 = effSigma;

	  if(n1 != 0.0) /* Has it been normalized */
	  {
	    y1 *= n1;
	    s1 *= n1;
	  }

	  if(y1 > dataymax)
	  {
	    dataymax = y1;
	  }

	  if(y1 < dataymin)
	  {
	    dataymin = y1;
	  }

          fprintf(fp, "%e %e %e\n", x1, y1, s1);
        }
	else
	if(ymode == MODE_APP)
        {
          effSigma = obj->measurements[i].results[j].measuredFluxSigma / 
                     obj->measurements[i].results[j].measuredFlux;

	  effSigma *= obj->measurements[i].results[j].app_eff;

          if(xmode == MODE_NUM)
          {
            x1 = (float)(i+1);
          }
          else
          {
            x1 = obj->measurements[i].averageEl;
          }

          y1 = obj->measurements[i].results[j].app_eff;
          s1 = effSigma;

	  if(n1 != 0.0) /* Has it been normalized */
	  {
	    y1 *= n1;
	    s1 *= n1;
	  }

	  if(y1 > dataymax)
	  {
	    dataymax = y1;
	  }

	  if(y1 < dataymin)
	  {
	    dataymin = y1;
	  }

          fprintf(fp, "%e %e %e\n", x1, y1, s1);
        }
        else
	if(ymode == MODE_JANSKYS)
        {
          if(xmode == MODE_NUM)
          {
            x1 = (float)(i+1);
          }
          else
          {
            x1 = obj->measurements[i].averageEl;
          }

          y1 = obj->measurements[i].results[j].measuredFlux;
          s1 = obj->measurements[i].results[j].measuredFluxSigma;

	  if(n1 != 0.0) /* Has it been normalized */
	  {
	    y1 *= n1;
	    s1 *= n1;
	  }

	  if(y1 > dataymax)
	  {
	    dataymax = y1;
	  }

	  if(y1 < dataymin)
	  {
	    dataymin = y1;
	  }

          fprintf(fp, "%e %e %e\n", x1, y1, s1);
        }
        else
	if(ymode == MODE_TMB)
        {

          if(xmode == MODE_NUM)
          {
            x1 = (float)(i+1);
          }
          else
          {
            x1 = obj->measurements[i].averageEl;
          }

          y1 = obj->measurements[i].results[j].measuredTmb;
	  s1 = obj->measurements[i].results[j].measuredTmbSigma;
	
	  if(n1 != 0.0) /* Has it been normalized */
	  {
	    y1 *= n1;
	    s1 *= n1;
	  }

	  if(y1 > dataymax)
	  {
	    dataymax = y1;
	  }

	  if(y1 < dataymin)
	  {
	    dataymin = y1;
	  }

          fprintf(fp, "%e %e %e\n", x1, y1, s1);
        }
      }

      fclose(fp);
    }
    else
    {
      used[j] = 0;
    }
  }

	/* Are there baselines curves to plot ?? */
  if(obj->bplot)
  {
    for(j=0;j<MAX_IFS;j++)
    {
      if(used[j])
      {
        sprintf(datName, "/tmp/%s/bplot-%s.dat", session, mg_sideband[j]);

        if((fp = fopen(datName, "w")) == NULL)
        {
          st_fail("Unable to open %s for writting", datName);

          return(0);
        }

        n1 = obj->normalize[j];

	if(obj->fit_scaled)	/* linuxpops has already scaled the fit curves */
        {
	  n1 = 0.0;
	}

        for(i=0;i<obj->numMeas;i++)
        {
          if(xmode == MODE_NUM)
          {
            x1 = (float)(i+1);
          }
          else
          {
            x1 = obj->measurements[i].averageEl;
          }

          if(x1 < xmin)
          {
            xmin = y1;
          }

          if(x1 > xmax)
          {
            xmax = x1;
          }


          y1 = obj->fitcurves[j][i];

	  if(n1 != 0.0)
	  {
	    y1 *= n1;
	  }

          if(y1 < ymin)
          {
            ymin = y1;
          }

          if(y1 > ymax)
          {
            ymax = y1;
          }

          fprintf(fp, "%e %e\n", x1, y1);
        }

        fclose(fp);
      }
    }
  }

  /* Now Write out gnuplot command file */
  sprintf(demName, "/tmp/%s/scan.dem", session);

  if((fp = fopen(demName, "w")) == NULL)
  {
    st_fail("Unable to open %s for writting", demName);

    return(0);
  }

  fprintf(fp, "reset\n");
  fprintf(fp, "set size 1.0, 1.0\n");

  if(obj->fit_scaled)	/* This is normalized data */
  {
    fprintf(fp, "set title \"%s Beam Efficiency History\"\n", obj->source);
    fprintf(fp, "set ylabel \"Normalized\"\n");
  }
  else
  if(ymode == MODE_EFF)
  {
    fprintf(fp, "set title \"%s Beam Efficiency History\"\n", obj->source);
    fprintf(fp, "set ylabel \"Beam Efficiency\"\n");
  }
  else
  if(ymode == MODE_APP)
  {
    fprintf(fp, "set title \"%s Apperature Efficiency History\"\n", obj->source);
    fprintf(fp, "set ylabel \"Apperture Efficiency\"\n");
  }
  else
  if(ymode == MODE_JANSKYS)
  {
    fprintf(fp, "set title \"%s Flux Density History\"\n", obj->source);
    fprintf(fp, "set ylabel \"Flux Density (Jk)\"\n");
  }
  else
  if(ymode == MODE_TMB)
  {
    fprintf(fp, "set title \"%s Main Beam Temperature History\"\n", obj->source);
    fprintf(fp, "set ylabel \"Tmb (K)\"\n");
  }

  if(xmode == MODE_NUM)
  {
    fprintf(fp, "set xlabel \"Measurement\"\n");
    fprintf(fp, "set xrange [0:%d]\n", obj->numMeas+1);
  }
  else
  {
    fprintf(fp, "set xlabel \"Elevation\"\n");

    if(obj->xmin || obj->xmax)
    {
      fprintf(fp, "set xrange [%.2f:%.2f]\n", obj->xmin, obj->xmax);
    }
    else
    {
      fprintf(fp, "set xrange [%.2f:%.2f]\n", 0.0, 90.0);
    }
  }

        /* Place labels */
  if(obj->ncoeff)
  {
    dataydiff = (dataymax - dataymin) / 10.0;
    ydiff     = (    ymax -     ymin) / 10.0;

    if(dataydiff > ydiff)
    {
      ydiff = dataydiff;
    }

    if(obj->xlabel != 0.0)
    {
      xx = obj->xlabel;
    }
    else
    {
      xx = 20.0;
    }

    if(obj->ylabel != 0.0)
    {
       yy = obj->ylabel;
    }
    else
    {
       yy = ymin;
    }

    l      = 0;

    for(j=0;j<MAX_IFS;j++)
    {
      if(used[j])                  /* Valid data here ? */
      {
        sprintf(coeffStr, "Coeff for %s ", mg_sideband[j]);

        for(k=0;k<obj->ncoeff+1;k++)
        {
          sprintf(tbuf, "a%d=%.3e ", k, obj->coeff[j][k]);
          strcat(coeffStr, tbuf);
        }

        log_msg("accumPlot(): %s", coeffStr);

	/* These most likely is for el gain curves so assume the plot os 0 - 90 degrees */
        fprintf(fp, "set label %d \"%s\" at %f, %f tc lt %d font \"6x10\"\n", j+11, coeffStr, xx, yy, 3+l);

        yy -= ydiff;

        l++;

      }
    }
  }

  fprintf(fp, "set grid\n");
  fprintf(fp, "set pointsize 2\n");

  fprintf(fp, "set terminal x11 enhanced %d font \"%s\" position %d,%d\n", obj->plotwin, fsize, startx, obj->plotwin*10);

  first = 1;
  for(j=0;j<MAX_IFS;j++)
  {
    if(used[j])
    {
      sprintf(datName, "/tmp/%s/flux-%s.dat", session, mg_sideband[j]);

      if(first)
      {
        if(lines == MODE_LINES)
        {
          fprintf(fp, "plot \'%s' with yerrorlines", datName);
        }
        else
        {
          fprintf(fp, "plot \'%s' with yerror", datName);
        }

        first = 0;
      }
      else
      {
        if(lines == MODE_LINES)
        {
          fprintf(fp, ", \'%s' with yerrorlines", datName);
        }
        else
        {
          fprintf(fp, ", \'%s' with yerror", datName);
        }
      }
    }
  }

  if(obj->bplot)
  {
    for(j=0;j<MAX_IFS;j++)
    {
      if(used[j])
      {
        fprintf(fp, ", '/tmp/%s/bplot-%s.dat' with lines", session, mg_sideband[j]);
      }
    }
  }

  fprintf(fp, "\n"); /* Finish the line */

  fclose(fp);

  sprintf(gpBuf, "load \"/tmp/%s/scan.dem\"", session);
  writeGnuplot(gpBuf, 0);

  return(0);
}



int displayMoments(s, how)
struct SPECTRAL_MOMENT_STRUCT *s;
int how;
{
  int   i, k, indx;
  FILE *fp;
  double x, y;
  struct SPECTRAL_MOMENT_ELEMENT *p;
  char  fname[256], demName[256], sbname[256], gpBuf[256], title[256];
  char xlabel[256], ylabel[256];

  log_msg("Displaying Moments Info");

  /* Write out datafile first */

  for(k=0;k<MAX_MOMENT_IFS;k++)
  {
    if(s->measurements[0][k].scan == 0.0)		/* Any data ?? */
    {
      log_msg("Skipping IF %d", k+1);
      continue;
    }

    if(s->issmt)
    {
      strcpy(sbname, mg_sideband[k]);
    }
    else
    {
      strcpy(sbname, kp_sideband[k]);
    }

    sprintf(fname, "/tmp/%s/moments-%s.dat", session, sbname);

    if((fp = fopen(fname, "w")) == NULL)
    {
      log_msg("Unable to open %s for writting", fname);

      return(1);
    }


    for(i=0;i<s->n;i++)
    {
      p = &s->measurements[i][k];

      if(s->plotx == MOM_X_AXIS_EL)
      {
	x = p->el;
      }
      else
      {
        x = p->scan;
      }

      switch(s->ploty)
      {
	case MOM_Y_AXIS_AVG:  y = p->avg;       break;
	case MOM_Y_AXIS_AREA: y = p->area_kmhz; break;
	case MOM_Y_AXIS_KKMS: y = p->kkms;      break;
	case MOM_Y_AXIS_SUM:  y = p->sum;       break;
	case MOM_Y_AXIS_TSYS: y = p->tsys;      break;
      }

      if(x != 0.0)
      {
        fprintf(fp, "%.0f %e\n", x, y);
      }
    }

    fclose(fp);
  }

  /* Now Write out gnuplot command file */
  sprintf(demName, "/tmp/%s/moments.dem", session);

  if((fp = fopen(demName, "w")) == NULL)
  {
    log_msg("Unable to open %s for writting", demName);

    return(1);
  }

  if(s->plotx == MOM_X_AXIS_EL)
  {
    strcpy(xlabel, "Elevation");
  }
  else
  {
    strcpy(xlabel, "Scan Number");
  }

  switch(s->ploty)
  {
    case MOM_Y_AXIS_AVG:  strcpy(ylabel, "Average");     break;
    case MOM_Y_AXIS_AREA: strcpy(ylabel, "Area.mHz");    break;
    case MOM_Y_AXIS_KKMS: strcpy(ylabel, "Kelvin km/s"); break;
    case MOM_Y_AXIS_SUM:  strcpy(ylabel, "Sum");         break;
    case MOM_Y_AXIS_TSYS: strcpy(ylabel, "Tsys");        break;
  }

  fprintf(fp, "reset\n");
  fprintf(fp, "set size 1.0, 1.0\n");
  fprintf(fp, "set title \"Moments Info\"\n");
  fprintf(fp, "set grid\n");
  fprintf(fp, "set xlabel \"%s\" tc rgb \"red\"\n", xlabel);
  fprintf(fp, "set ylabel \"%s\" tc rgb \"red\"\n", ylabel);

  /* Borrow the last_image for now */
  setPaperSize(last_image.pinfo.paperw, last_image.pinfo.paperh, psize, fsize); 

  indx = 0;
  for(k=0;k<MAX_MOMENT_IFS;k++)
  {
    if(s->measurements[0][k].scan == 0.0)		/* Any data ?? */
    {
      log_msg("Skipping IF %d", k+1);
      continue;
    }

    if(s->issmt)
    {
      strcpy(sbname, mg_sideband[k]);
    }
    else
    {
      strcpy(sbname, kp_sideband[k]);
    }

    sprintf(fname, "/tmp/%s/moments-%s.dat", session, sbname);
    sprintf(title, "%s", sbname);

    if(indx == 0)
    {
      fprintf(fp, "plot '%s' with linespoints title \"%s\"", fname, title);
    }
    else
    {
      fprintf(fp, ", '%s' with linespoints title \"%s\"", fname, title);
    }

    indx++;

  }

  log_msg("Plotted %d Sidebands", indx);

  fprintf(fp, "\n"); /* Close it out */

  fclose(fp);

  sprintf(gpBuf, "load \"/tmp/%s/moments.dem\"", session);
  writeGnuplot(gpBuf, 10000);
    
  return(0);
}


int displayTsys(tsys, how)
struct TSYS_SEND *tsys;
int how;
{
  int   i, indx, el;
  FILE *fp;
  char  fname[256], demName[256], gpBuf[256], title[256];

  /* Write out datafile first */

  for(i=0;i<tsys->num;i++)
  {
    if(!tsys->freqs[i].used)
    {
      continue;
    }

    sprintf(fname, "/tmp/%s/tsys-%03d.dat", session, tsys->freqs[i].freq);

    if((fp = fopen(fname, "w")) == NULL)
    {
      log_msg("Unable to open %s for writting", fname);

      return(1);
    }

    /* Write Results */
    for(el=0;el<90;el++)
    {
      if(tsys->freqs[i].elev[el] > 0)
      {
        fprintf(fp, "%d %d\n", el, tsys->freqs[i].elev[el]);
      }
    }

    fclose(fp);
  }

  /* Now Write out gnuplot command file */
  sprintf(demName, "/tmp/%s/tsys.dem", session);

  if((fp = fopen(demName, "w")) == NULL)
  {
    log_msg("Unable to open %s for writting", demName);

    return(1);
  }

  fprintf(fp, "reset\n");
  fprintf(fp, "set size 1.0, 1.0\n");
  fprintf(fp, "set title \"Receiver Tsys\"\n");
  fprintf(fp, "set grid\n");
  fprintf(fp, "set xlabel \"Elevation\" tc rgb \"red\"\n");
  fprintf(fp, "set ylabel \"Tsys\" tc rgb \"red\"\n");

  setPaperSize(last_image.pinfo.paperw, last_image.pinfo.paperh, psize, fsize); /* Borrow the last_image for now */

  indx = 0;
  for(i=0;i<tsys->num;i++)
  {
    if(!tsys->freqs[i].used)
    {
      continue;
    }

    sprintf(fname, "/tmp/%s/tsys-%03d.dat", session, tsys->freqs[i].freq);
    sprintf(title, "%03d GHz", tsys->freqs[i].freq);

    if(indx == 0)
    {
      fprintf(fp, "plot '%s' with lines  lc rgb \"red\" title \"%s\"", fname, title);
    }
    else
    {
      fprintf(fp, ", '%s' with lines  lc rgb \"red\" title \"%s\"", fname, title);
    }

    indx++;
  }

  log_msg("Plotted %d freqs", indx);

  fprintf(fp, "\n"); /* Close it out */

  fclose(fp);

  sprintf(gpBuf, "load \"/tmp/%s/tsys.dem\"", session);
  writeGnuplot(gpBuf, tsys->xdelay);

  return(0);
}


int displayScanSum(ss, how)
struct SCAN_SUM_SEND *ss;
int how;
{
  int   i, j, indx;
  double x, y;
  FILE *fp;
  char  fname[256], demName[256], gpBuf[256], headBuf[1024];
  char xbuf[256], ybuf[256];

  log_msg("SSS: scans:   %d", ss->nscans);
  log_msg("SSS: X-Axis:  %d", ss->xaxis);
  log_msg("SSS: Y-Axis:  %d", ss->yaxis);
  log_msg("SSS: Plotwin: %d", ss->plotwin);
  log_msg("SSS: XDelay:  %d", ss->xdelay);

  /* Write out datafile first */
  for(i=0;i<MAX_IFS;i++)
  {
    if(ss->scans[0].scan[i] < 1.0)
    {
      continue;
    }

    sprintf(fname, "/tmp/%s/sss-%d.dat", session, i);

    if((fp = fopen(fname, "w")) == NULL)
    {
      log_msg("Unable to open %s for writting", fname);

      return(1);
    }

    /* Write Results */
    for(j=0;j<ss->nscans;j++)
    {
      switch(ss->xaxis)
      {
        case SCAN_SUM_X_UT:  x = ss->scans[j].ut[i];   break;
        case SCAN_SUM_X_LST: x = ss->scans[j].lst[i];  break;
        case SCAN_SUM_X_AZ:  x = ss->scans[j].az[i];   break;
        case SCAN_SUM_X_EL:  x = ss->scans[j].el[i];   break;
        case SCAN_SUM_X_NUM: x =   (double)j;          break;
        default:             x = ss->scans[j].scan[i]; break;
      }

      switch(ss->yaxis)
      {
	case SCAN_SUM_Y_RMS:  y = ss->scans[j].rms[i]*1000.0;  break;
	case SCAN_SUM_Y_TSYS: y = ss->scans[j].tsys[i];        break;
	case SCAN_SUM_Y_TCAL: y = ss->scans[j].tcal[i];        break;
	default:              y = ss->scans[j].peak[i];        break;
      }

      fprintf(fp, "%e %e\n", x, y);
    }

    fclose(fp);

    log_msg("Wrote %d elements to %s", j, fname);
  }


  /* Now Write out gnuplot command file */
  sprintf(demName, "/tmp/%s/sss.dem", session);

  if((fp = fopen(demName, "w")) == NULL)
  {
    log_msg("Unable to open %s for writting", demName);

    return(1);
  }

  fprintf(fp, "reset\n");
  fprintf(fp, "set size 1.0, 1.0\n");

  makeSSSHeader(ss, headBuf);

  fprintf(fp, "set title \"%s\"\n", headBuf);
  fprintf(fp, "set grid\n");

  switch(ss->xaxis)
  {
    case SCAN_SUM_X_UT:  strcpy(xbuf, "UT");   break;
    case SCAN_SUM_X_LST: strcpy(xbuf, "LST");  break;
    case SCAN_SUM_X_AZ:  strcpy(xbuf, "AZ");   break;
    case SCAN_SUM_X_EL:  strcpy(xbuf, "EL");   break;
    case SCAN_SUM_X_NUM: strcpy(xbuf, "Num");  break;
    default:             strcpy(xbuf, "Scan"); break;
  }

  switch(ss->yaxis)
  {
    case SCAN_SUM_Y_RMS:  strcpy(ybuf, "rms");  break;
    case SCAN_SUM_Y_TSYS: strcpy(ybuf, "Tsys");  break;
    case SCAN_SUM_Y_TCAL: strcpy(ybuf, "Tcal");  break;
    default:              strcpy(ybuf, "(K)");  break;
  }

  fprintf(fp, "set xlabel \"%s\" tc rgb \"red\"\n", xbuf);
  fprintf(fp, "set ylabel \"%s\" tc rgb \"red\"\n", ybuf);

  if(ss->yscale)
  {
    fprintf(fp, "set yrange [%e:%e]\n", ss->ylower, ss->yupper);
  }

  setPaperSize(last_image.pinfo.paperw, last_image.pinfo.paperh, psize, fsize); /* Borrow the last_image for now */

  fprintf(fp, "set terminal x11 enhanced %d font \"%s\" position %d,%d\n", ss->plotwin, fsize, startx, ss->plotwin*10);

  indx = 0;
  for(i=0;i<MAX_IFS;i++)
  {
    if(ss->scans[0].scan[i] < 1.0)
    {
      continue;
    }

    sprintf(fname, "/tmp/%s/sss-%d.dat", session, i);

    if(indx == 0)
    {
      fprintf(fp, "plot '%s' with lp lc rgb \"red\" ", fname);
    }
    else
    {
      fprintf(fp, ", '%s' with lp  lc rgb \"red\" ", fname);
    }

    indx++;
  }

  fprintf(fp, "\n"); /* Close it out */

  fclose(fp);

  sprintf(gpBuf, "load \"/tmp/%s/sss.dem\"", session);
  writeGnuplot(gpBuf, ss->xdelay);

  return(0);
}


int startGnuPlot()
{
  int gfd;
  char tbuf[256];

  sprintf(tbuf, "x11 size 800,600 position %d,0", startx);

  if(gnuplot_init(tbuf))
  {
    log_msg("Failed to open connection to gnuplot");
    exit(1);
  }

  gfd = fileno(gpin);

  log_msg("# Gnuplot gfd = %d", gfd);

  return(gfd);
}


int sendPlotRange(xmin, xmax, ymin, ymax)
double xmin, xmax, ymin, ymax;
{
  bzero((char *)&xsend, sizeof(xsend));

  strcpy(xsend.magic, "XGRAPHICS");

  xsend.plot_used = 1;
  xsend.xplotmin  = xmin;
  xsend.xplotmax  = xmax;
  xsend.yplotmin  = ymin;
  xsend.yplotmax  = ymax;

  sock_write(master, (char *)&xsend, sizeof(xsend));

  return(0);
}


int sendMousePosition(x, y, x2, y2)
double x, y, x2, y2;
{
  bzero((char *)&xsend, sizeof(xsend));

  strcpy(xsend.magic, "XGRAPHICS");
  xsend.mouse_used = 1;

  xsend.last_mouse_x  = x;
  xsend.last_mouse_y  = y;
  xsend.last_mouse_x2 = x2;
  xsend.last_mouse_y2 = x2;

  log_msg("sendMousePosition(): Sending structure");

  sock_write(master, (char *)&xsend, sizeof(xsend));

  return(0);
}


int sendMapAngle(ang1, ang2)
double ang1, ang2;
{
  bzero((char *)&xsend, sizeof(xsend));

  strcpy(xsend.magic, "XGRAPHICS");

  xsend.map_angle_used = 1;
  xsend.map_angle_1    = ang1;
  xsend.map_angle_2    = ang2;

  sock_write(master, (char *)&xsend, sizeof(xsend));

  return(0);
}


/* 
MOUSE: 94.4722850678733 -143.047007918552 0.0230744018354638 0.0230744018354638
 */

int parseGnuplot(buf)
char *buf;
{
  int n;
  double x, y, x2, y2;
  double ang1, ang2;
  double xmin, xmax, ymin, ymax;

  log_msg("parseGnuplot(1): Got %s", buf);

  if(buf && strstr(buf, "MOUSE"))
  {
    if(!strncmp(buf, "MOUSE: MOUSE: ", 14))
    {
      n = sscanf(buf, "MOUSE: MOUSE: %lf %lf %lf %lf", &x, &x2, &y, &y2);
    }
    else
    {
      n = sscanf(buf, "MOUSE: %lf %lf %lf %lf", &x, &x2, &y, &y2);
    }

    if(n == 4)
    {
      log_msg("parseGnuplot(2): Got %d args", n);
      sendMousePosition(x, y, x2, y2);
    }
    else
    {
      log_msg("parseGnuplot(2): Wrong number of args: %d", n);
      log_msg("parseGnuplot(2): Got %s", buf);
    }
  }
  else
  if(buf && strstr(buf, "MAP_ANGLE"))
  {
    n = sscanf(buf, "MAP_ANGLE: %lf %lf", &ang1, &ang2);

    if(n == 2)
    {
      log_msg("parseGnuplot(3): Got %d Args", n);
      sendMapAngle(ang1, ang2);
    }
  }
  else
  if(buf && strstr(buf, "PLOT_RANGE"))
  {
    n = sscanf(buf, "PLOT_RANGE: %lf %lf %lf %lf", &xmin, &xmax, &ymin, &ymax);

    if(n == 4)
    {
      log_msg("parseGnuplot(4): Got %d Args", n);
      sendPlotRange(xmin, xmax, ymin, ymax);
    }
  }

  return(0);
}


/* Make a postscript output file of previously plotted data */
int make()
{
  char cmdStr[256], plotfname[256];

  sprintf(cmdStr, "set terminal postscript font \"%s\" color solid enhanced landscape size %s", fsize, psize);
  writeGnuplot(cmdStr, 100000);

    
  sprintf(plotfname, "/tmp/%s/ccal_plot.ps", session);
  sprintf(cmdStr, "set out \"%s\"", plotfname);
  writeGnuplot(cmdStr, 100000);

  sprintf(cmdStr, "replot");
  writeGnuplot(cmdStr, 100000);

  sprintf(cmdStr, "set terminal x11 enhanced");
  writeGnuplot(cmdStr, 100000);

  sprintf(cmdStr, "replot");
  writeGnuplot(cmdStr, 100000);

  return(0);
}


int main(argc, argv)
int argc;
char *argv[];
{
  int lastCmdCancel = 0;
  int gfd, sel, len;
  int n, xw, xy;
  int expectedSize = -1, nexpected = 0, curExpected=0, bplotExpected = 0, splotExpected=0;
  char message[MAX_BUF_SIZE], gpBuf[512], logdir[256], *cp, sysbuf[256];

  setCactusEnvironment();

  if(argc > 1)
  {
    n = sscanf(argv[1], "%dx%d", &xw, &xy);

    if(n == 2)
    {
      displayW = xw;
      displayH = xy;
    }
  }

  cp = getenv("LPSESSION");

  if(cp)
  {
    strcpy(session, cp);
  }
  else
  {
    strcpy(session, "12345");
  }

  sprintf(gpfifo, "/tmp/%s/gpio", session);
  sprintf(logdir, "LOGDIR=/tmp/%s/", session);

  putenv(logdir);

  smart_log_open("xgraphic", 3);

  sock_bind("LINUXPOPS_XGRAPHIC");

  log_eventdEnable(LOG_TO_LOGGER);

  master = sock_connect("LINUXPOPS_MASTER");

  sock_bufct(MAX_BUF_SIZE);

  log_msg("Using Display Width: %d, Height: %d", displayW, displayH);

  startx = displayW - 800;

  strcpy(fsize, "9x15");
  strcpy(psize, papersize);

  gfd = startGnuPlot();

// Look for background 
// xrdb -query | grep gnuplot.background

  while(1)
  {
    sel = sock_sel(message, &len, &gfd, 1, 60, 0);

    log_msg("< Select Returned: %d", sel);

    if(sel == -1)
    {
      log_msg("# Awake");
    }
    else
    if(sel == gfd)
    {
      readGnuplot(gpBuf);
      if(!lastCmdCancel)
      {
	parseGnuplot(gpBuf);
      }
    }
    else
    if(sel > 0)
    {
      log_msg("-");
      log_msg("< Got %d bytes on fd = %d", len, sel);
      lastCmdCancel = 0;

      if(len == expectedSize)
      {
	log_msg("Len %d matches Expected %d", len, expectedSize);

        if(curExpected != nexpected)
        {
          switch(curExpected)
          {
	    case 0:
              bcopy(message, (char *)&last_image.yplot1, last_image.nsize);
	      curExpected++;
	      break;

	    case 1:
              bcopy(message, (char *)&last_image.yplot2, last_image.nsize);
	      curExpected++;
	      break;

	    case 2:
              bcopy(message, (char *)&last_image.yplot3, last_image.nsize);
	      curExpected++;
	      break;

	    case 3:
              bcopy(message, (char *)&last_image.yplot4, last_image.nsize);
	      curExpected++;
	      break;
          }
	   /* Are we still waiting for more?? */
          if(curExpected != nexpected)
          {
            continue;
          }
        }
        else
        if(bplotExpected)
        {
          bcopy(message, (char *)&last_image.bplot, last_image.nsize);
          log_msg("Received my Baseline Plot Array");

          bplotExpected = 0;
        }
        else
        if(splotExpected)
        {
          bcopy(message, (char *)&last_image.splot, last_image.nsize);
          log_msg("Received my Shell Plot Array");

          splotExpected = 0;
        }

        if(curExpected == nexpected && !bplotExpected && !splotExpected)
        {
	  switch(last_image.mode)
	  {
	    case MODE_SPEC:         
	    case MODE_OTF:          
	      if(last_image.narray == 1)
              {
	        linePlot_1(&last_image, PLOT); 
              }
              else
	      if(last_image.narray == 2)
              {
	        linePlot_2(&last_image, PLOT); 
              }
              else
	      if(last_image.narray == 4)
              {
	        linePlot_4(&last_image, PLOT); 
              }

	      break;

	    case MODE_CONT:         
	      log_msg("CONT mode not implemented yet"); 
	      break;

	    case MODE_XCOR:          
	      log_msg("XCOR: Plotting"); 
	      plotXcor(&last_image, PLOT); 
	      break;
	  }

	  lastPlot = LAST_IMAGE_INFO;

          expectedSize = -1;
	  nexpected    = 0;
          curExpected  = 0;
        }
      }
      else
      if(len == SHORT_IMAGE_SIZE)
      {
        if(!strncmp(message, "LP_IMAGE_INFO", 13))
        {
          log_msg("< Got SHORT_IMAGE_INFO struct");

          bzero((char *)&last_image, sizeof(last_image));
          bcopy(message, (char *)&last_image, SHORT_IMAGE_SIZE);

	  /* Remember how many arrays and how big, to follow */
	  nexpected     = last_image.narray;
          expectedSize  = last_image.nsize;
	  bplotExpected = last_image.pinfo.bplot;	/* Baseline expected */
	  splotExpected = last_image.pinfo.splot;	/* Shell array expected */

          log_msg("Expecting %d Arrays of %d size", nexpected, expectedSize);

          if(bplotExpected)
          {
            log_msg("Also Expecting a Baseline Plot Array");
          }
	  else
          {
            log_msg("NOT Expecting a Baseline Plot Array");
          }

          if(splotExpected)
          {
            log_msg("Also Expecting a Shell Plot Array");
          }
	  else
          {
            log_msg("NOT Expecting a Shell Plot Array");
          }

          log_msg("SAT Struct Active = %d", last_image.satActive);
        }

        continue;
      }
      else
      if(len == sizeof(struct FLUX_INFO))
      {
        if(!strncmp(message, "FLUX_INFO", 9))
        {
          log_msg("< Got FLUX_INFO struct");

          bcopy(message, (char *)&last_flux, sizeof(last_flux));

          fluxPlot(&last_flux, PLOT);

	  lastPlot = LAST_FLUX_INFO;
        }

        continue;
      }
      else
      if(len == sizeof(struct ACCUM_RESULTS_SEND))
      {
        if(!strncmp(message, "ACCUM_INFO", 9))
        {
          log_msg("< Got ACCUM_RESULTS_SEND struct: %d", len);

          bcopy(message, (char *)&ars, sizeof(ars));

          accumPlot(&ars, PLOT);

	  lastPlot = LAST_ACCUM_RESULTS;
        }

        continue;
      }
      else
      if(len == sizeof(struct CONTINUUM_PLAIN))
      {
        if(!strncmp(message, "CONTINUUM_PLAIN", 15))
        {
          log_msg("< Got CONTINUUM_PLAIN struct: %d", len);

          bcopy(message, (char *)&last_plain_send, sizeof(last_plain_send));

          contPlot(&last_plain_send, PLOT);

	  lastPlot = LAST_CONTINUUM_PLAIN;
        }

        continue;
      }
      else
      if(len == sizeof(struct CONTINUUM_SPTIP))
      {
        if(!strncmp(message, "CONTINUUM_SPTIP", 15))
        {
          log_msg("< Got CONTINUUM_SPTIP struct: %d", len);

          bcopy(message, (char *)&last_sptip_send, sizeof(last_sptip_send));

          contSptipPlot(&last_sptip_send, PLOT);

	  lastPlot = LAST_CONTINUUM_SPTIP;
        }

        continue;
      }
      else
      if(len == sizeof(struct FLUX_HISTORY))
      {
        if(!strncmp(message, "FLUX_HISTORY", 9))
        {
          log_msg("< Got FLUX_HISTORY struct: %d", len);

          bcopy(message, (char *)&fh, sizeof(fh));

	  plotFluxHistory(&fh, PLOT);

	  lastPlot = LAST_FLUX_HISTORY;
        }

        continue;
      }
      else
      if(len == sizeof(struct FOCUS_HISTORY_SEND))
      {
        if(!strncmp(message, "FOCUS_SEND", 10))
        {
          log_msg("< Got FOCUS_HISTORY_SEND struct: %d", len);

          bcopy(message, (char *)&focus_send, sizeof(focus_send));

	  plotFocusHistory(&focus_send, PLOT);

	  lastPlot = LAST_FOCUS_HISTORY;
        }

        continue;
      }
      else
      if(len == sizeof(struct OBJECT))
      {
        if(!strncmp(message, "OBJECT_MAGIC", 12))
        {
          log_msg("< Got OBJECT_MAGIC struct: %d", len);

          bcopy(message, (char *)&last_obj, sizeof(last_obj));

	  displayObjHistory(&last_obj, PLOT);

	  lastPlot = LAST_OBJECT_HISTORY;
        }
        else
        {
          log_msg("ObjectDisplay(): message is not OBJECT_MAGIC");
        }

        continue;
      }
      else
      if(len == sizeof(struct FFT_INFO))
      {
        if(!strncmp(message, "FFT_INFO", 8))
        {
          log_msg("< Got FFT_INFO struct: %d", len);

          bcopy(message, (char *)&last_fft, sizeof(last_fft));

	  displayFFT(&last_fft, PLOT);

	  lastPlot = LAST_FFT_INFO;
        }
        else
        {
          log_msg("main(): message is not FFT_INFO");
        }

        continue;
      }
      else
      if(len == sizeof(struct HISTO_INFO))
      {
        if(!strncmp(message, "HISTOGRAM", 9))
        {
          log_msg("< Got HISTO_INFO struct: %d", len);

          bcopy(message, (char *)&last_histo, sizeof(last_histo));

	  displayHisto(&last_histo, PLOT);

	  lastPlot = LAST_HISTO_INFO;
        }
        else
        {
          log_msg("main(): message is not HISTO_INFO");
        }

        continue;
      }
      else
      if(len == sizeof(struct STRINGS_AND_THINGS))
      {
        if(!strncmp(message, "STRINGS", 7))
        {
          log_msg("< Got STRINGS_AND_THINGS struct: %d", len);

          bcopy(message, (char *)&sat, sizeof(sat));

        }
        else
        {
          log_msg("main(): message is not STRINGS_AND_THINGS");
        }

        continue;
      }
      else
      if(len == sizeof(struct TSYS_SEND))
      {
        if(!strncmp(message, "TSYS_SEND", 9))
        {
          log_msg("< Got TSYS_SEND struct: %d", len);

          bcopy(message, (char *)&last_tsys, sizeof(last_tsys));

	  displayTsys(&last_tsys, PLOT);

	  lastPlot = LAST_TSYS_INFO;
        }
        else
        {
          log_msg("main(): message is not TSYS_SEND");
        }

        continue;
      }
      else
      if(len == sizeof(struct SCAN_SUM_SEND))
      {
        if(!strncmp(message, "SCAN_SUM_SEND", 13))
        {
          log_msg("< Got SCAN_SUM_SEND struct: %d", len);

          bcopy(message, (char *)&sss, sizeof(sss));

	  displayScanSum(&sss, PLOT);

	  lastPlot = LAST_SSS_INFO;
        }
        else
        {
          log_msg("main(): message is not SCAN_SUM_SEND; %s", message);
        }

        continue;
      }
      else
      if(len == SPEC_MOM_STRUCT_SIZE)
      {
        if(!strncmp(message, "MOMENTS_STRUCT", 14))
        {
          log_msg("< Got SPECTRAL_MOMENT_STRUCT struct: %d", len);

          bcopy(message, (char *)&sms, sizeof(sms));

	  /* Plot it here */

	  displayMoments(&sms, PLOT);

	  lastPlot = LAST_MOMENTS_STRUCT;
        }
        else
        {
          log_msg("main(): message is right size, but not SPECTRAL_MOMENT_STRUCT; %s", message);
        }

        continue;
      }
      else
      if(len == SHELL_GUESS_SIZE)
      {
        if(!strncmp(message, "SHELL_GUESS", 11))
        {
          log_msg("< Got SHELL_GUESS struct: %d", len);

          bcopy(message, (char *)&shell_guess, sizeof(shell_guess));
        }
        else
        {
          log_msg("main(): message is not SHELL_GUESS; %s", message);
        }

        continue;
      }

	/* Check for commands */
      if(!strncmp(message, "exit", 4))
      {
        log_msg("< Got command to exit()");

        log_msg("lp_exit"); /* Trigger the logger to exit too */

        closeGnuplot();

        exit(0);
      }
      else
      if(!strncmp(message, "ccur", 4))
      {
        log_msg("< Got 'ccur'");
        writeGnuplot("pause mouse button1", 0);
        writeGnuplot(mouse_str, 0);
      }
      else
      if(!strncmp(message, "tcur", 4))
      {
        log_msg("< Got 'tcur'");
        writeGnuplot("pause mouse button1", 0);
        writeGnuplot(mouse_str, 0);
      }
      else
      if(!strncmp(message, "cancel", 4))
      {
        log_msg("< Got 'cancel'");
        log_msg("> Sending SIGINT to gnuplot");
	sprintf(sysbuf, "/bin/kill -INT %d", gnuplot_pid);
	log_msg(sysbuf);
        system(sysbuf);
	lastCmdCancel = 1;
      }
      else
      if(!strncmp(message, "restart", 7))
      {
        log_msg("< Got 'restart'");
        log_msg("> Sending SIGHUP to gnuplot");

        closeGnuplot();

        gfd = startGnuPlot();
      }
      else
      if(!strncmp(message, "page", 4))
      {
        log_msg("< Got 'page'");
        writeGnuplot("clear", 0);
      }
      else
      if(!strncmp(message, "show_view", 9))
      {
        log_msg("< Got 'show_view'");
        writeGnuplot(map_ang_str, 0);
      }
      else
      if(!strncmp(message, "get_range", 9))
      {
        log_msg("< Got 'get_range'");
        writeGnuplot(get_range_str, 0);
      }
      else
      if(!strncmp(message, "make", 4))
      {
        log_msg("< Got 'make'");
	make();
      }
      else
      if(!strncmp(message, "refresh", 7))
      {
        log_msg("< Got 'refresh'");
        switch(lastPlot)
        {
	  case LAST_IMAGE_INFO:
	      if(last_image.mode == MODE_XCOR)
              {
	        plotXcor(&last_image, PLOT); 
	      }
	      else
	      if(last_image.narray == 1)
              {
	        linePlot_1(&last_image, PLOT); 
              }
              else
	      if(last_image.narray == 2)
              {
	        linePlot_2(&last_image, PLOT); 
              }
              else
	      if(last_image.narray == 4)
              {
	        linePlot_4(&last_image, PLOT); 
              }

              break;

	  case LAST_FLUX_INFO:       fluxPlot(&last_flux, PLOT);          break;
	  case LAST_ACCUM_RESULTS:   accumPlot(&ars, PLOT);               break;
	  case LAST_FLUX_HISTORY:    plotFluxHistory(&fh, PLOT);          break;
          case LAST_CONTINUUM_PLAIN: contPlot(&last_plain_send, PLOT);    break;
          case LAST_OBJECT_HISTORY:  displayObjHistory(&last_obj, PLOT);  break;
          case LAST_FFT_INFO:        displayFFT(&last_fft, PLOT);         break;
          case LAST_HISTO_INFO:      displayHisto(&last_histo, PLOT);     break;
          case LAST_TSYS_INFO:       displayTsys(&last_tsys, PLOT);       break;
          case LAST_SSS_INFO:        displayScanSum(&sss, PLOT);          break;
          case LAST_FOCUS_HISTORY:   plotFocusHistory(&focus_send, PLOT); break;
          case LAST_MOMENTS_STRUCT:  displayMoments(&sms, PLOT);          break;
        }
      }
      else
      if(!strncmp(message, "load", 4))
      {
        log_msg("< Got 'load'");
        sprintf(gpBuf, "load \"/tmp/%s/scan.dem\"", session);
        writeGnuplot(gpBuf, 0);
      }
      else
      if(!strncmp(message, "xgraphic", 8))
      {
        log_msg("< Got '%s'", message);
        sprintf(gpBuf, "%s", message+8);
        writeGnuplot(gpBuf, 0);
      }
    }
    else
    {
      log_msg("* sock_sel() returned %d", sel);
    }
  }

  return(0);
}
