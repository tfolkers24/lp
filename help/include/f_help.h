﻿/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */

  { "f", 
      {
	  "Show First IF",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: scan_number f [options]",
	  "",
	  "where: 'scan_number' is the root of the scan; i.e. 1234; no fraction",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: s",
	  "",
	  NULL
      } 
  },

  { "fboffset", 
      {
	  "Apply Temperature Offset to block of channels",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: fboffset [seg] [match seg] [bmatch seg1 seg2] [options]",
	  "",
	  "where: 'match' means to match 'seg' to others",
	  "where: 'bmatch' means to match 'seg' to other baseline average",
	  "where: 'seg' by itself will require mouse clicks",
	  "",
	  "options are:",
	  "",
	  "/a  Auto set bad blocks of channels",
	  "/l  List current segment offsets",
	  "/c  clear out all defined fb-offsets",
	  "/h  help",
	  "",
	  "NOTE: the internal variable 'markers' must be set to match block boundaries",
	  "NOTE: You must have done a 'avg /b /s' prior to using 'bmatch'",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "fc", 
      {
	  "Set Plot X-Axis to Freq over Channel",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: fc [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  cc, cf, cv, ff, fv, vc, vf, vv",
	  "",
	  NULL
      } 
  },

  { "fcur", 
      {
	  "Get Cursor Frequency Value",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: fcur [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "feed", 
      {
	  "Select IF to Analyze",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: feed [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  backend, bkres, bknameres",
	  "",
	  NULL
      } 
  },

  { "ff", 
      {
	  "Set Plot X-Axis to Freq over Freq",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: ff [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  cc, cf, cv, fc, fv, vc, vf, vv",
	  "",
	  NULL
      } 
  },

  { "ffb", 
      {
	  "Print FFB Filter Diagnostics [SMT Only]",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: ffb [options] [chan]",
	  "",
	  "where: [chan] is the channels to query.",
	  "       if no [chan] passed; you will be queried",
	  "       to select one using mouse.",
	  "",
	  "options are:",
	  "",
	  "/h  Help",
	  "/a  List all 2560 channel assignments",
	  "/b  List crate and board info for bad channels",
	  "/t  List table of channels/board info",
	  "/r  Reload table of channels/board info",
	  "",
	  "See Also:  fboffset",
	  "",
	  NULL
      } 
  },

  { "fft", 
      {
	  "FFT Sub-System",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: fft [xb xe] [options]",
	  "",
	  "options are:",
	  "",
	  "/a  'xb 'xe'; set fft x-axis to 'xb' thru 'xe'",
	  "/c  Perform a 'cut' of a range of frequencies to remove",
	  "/p  Pick a number if individual frequencies to remove",
	  "/r  Reset fft x-axis to defaults",
	  "",
	  "/h  help",
	  "",
	  "NOTE: A baseline must be removed before performing a 'fft'",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "filename", 
      {
	  "Load SDD file",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: filename [fname] [options]",
	  "",
	  "options are:",
	  "",
	  "where: 'fname' is the sdd data file to open",
	  "",
	  "       with no argument, a GUI will open to allow browsing",
	  "       for a data file",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "files", 
      {
	  "Show Current Data and Scratch Files",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: files [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "fillpat", 
      {
	  "Generate sin-wave at Freq",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: fillpatter freq [options]",
	  "",
	  "where: 'freq' is frequency in MHz",
	  "",
	  "options are:",
	  "",
	  "/t tan wave clamped at +/- 10.0",
	  "/c cos wave",
	  "/s sin wave",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "focus", 
      {
	  "Focus Analysis Functions",
	  "",
	  "Mode: CONT",
	  "",
	  "Usage: focus [options]",
	  "",
	  "options are:",
	  "",
	  "/c Calibrate by applying a numbered formula to data",
	  "   /n    'n' the number of the formula to apply",
	  "   /add  add the formula 'n' to raw values",
	  "   /sub  subtract the formula 'n' from raw values",
	  "   /mul  multiply the raw values by formula 'n'",
	  "   /div  divide the raw values by formula 'n'",
	  "/d  Re-Display Focus Array",
	  "    /n  Display with no Lines; can be used with all actions)",
	  "/e  Erase any fit data",
	  "/f  Find and List Available Focus Scans",
	  "/g  Perform a 'nfit' LSQ fit to the Focus Array",
	  "    /a  Save the resulting fit to formula[nfit]",
	  "/l  List Loaded Focus Fits",
	  "/m  Use Mouse to place Fit Labels",
	  "/r  Read in and Analyze All 'Selected' Axial Focus Scans",
	  "    /a  Read in and Analyze All 'Selected' Regardless of Source",
	  "    /e  Read in and Analyze All 'Selected' EWFC Focus Scans",
	  "    /n  Read in and Analyze All 'Selected' NSFC Focus Scans",
	  "    /x  Dont bother displaying the individual scans",
	  "/s  Sort the focus Data (Default: increasing elevation)",
	  "    /i  Use Scan Number for sorting",
	  "    /t  Use Ambient Temperature for sorting",
	  "",
	  "/h  help",
	  "",
	  "CAUTION: This command uses over-loaded flags; The left-most flags must be first",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "findseq", 
      {
	  "Popup a window listing of all Sequences scan in all available versions",
	  "",
	  "Mode: CONT",
	  "",
	  "Usage: findseq [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "flags", 
      {
	  "Help for Command Flags",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: flags [options]",
	  "",
	  "options are:",
	  "",
	  "Most commands have modifier flags that change the behaviour of the command.",
	  "Just type the 'command /h' for a listing of available flags for that command.",
	  "",
	  "All commands respect the following",
	  "/h  help",
	  "",
	  "Most commands respect the following",
	  "/q  quiet   (reduced   output)",
	  "/v  verbose (increased output)",
	  "",
	  "See Also: shortcuts",
	  "",
	  NULL
      } 
  },

  { "fork", 
      {
	  "Create a Hyper-Fine Label Marker",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: fork /i n nt 'label text' [options]  (initialize fork #n, of nt tines)",
          "Usage: fork /a n 'label text'               (create a fork based on gaussian fits)",
          "Usage: fork /d n                            (deletes fork #n)",
          "Usage: fork /r filename                     (read 'filename' containing predefined forks)",
          "Usage: fork /s filename                     (save the current fork list to 'filename')",
          "Usage: fork /c                              (clears all forks)",
          "Usage: fork /l                              (list currently defined forks)",
          "",
          "Where:\tn  = fork number; 0 - 31",
          "Where:\tnt = number of tines; Max of 8 tines",
          "\t'label text' is the label without quote marks",
          "\t'filename' is the file to save/load fork to/from",
          "",
          "options are:",
	  "/i  initialize",
	  "/a  create a fork based on gaussian fits",
	  "/d  delete",
	  "/s  save",
	  "/r  read",
	  "/l  list",
	  "/c  clear",
	  "/h  help",
          "",
          "/Cbl blue    fork",
          "/Crd red     fork",
          "/Cgr green   fork",
          "/Ccy cyan    fork",
          "/Cmg magenta fork",
          "/Cyl yellow  fork",
          "/Cwt white   fork",
          "",
	  "See Also: label, hline, marker",
	  "",
	  NULL
      } 
  },

  { "formula", 
      {
	  "Formula Functions",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: formula math_syntax [filename] [options]",
	  "",
	  "where: 'math_syntax' is the formula",
	  "       'filename'    is the file to read or write",
	  "       '/1, ... /24'; is the formula designator",
	  "",
	  "options are:",
	  "",
	  "/c  /[n] Clear out formula 'n' ",
	  "/d  /[n] Define formula 'n' ",
	  "/e  Erase all formulas",
	  "/l  List all formulas",
	  "/r  'filename' Read formulas  from 'filename'",
	  "    /i  Ignore errors if 'filename' doesn't exist",
	  "/w  'filename' Write formulas to   'filename'",
	  "/x  /[n] Evaluate formula 'n'",
	  "    /v verbose listing while executing",
	  "",
	  "/h  help",
	  "",
	  "See Also: ",
	  "",
	  NULL
      } 
  },

  { "freex", 
      {
	  "Set X-Axis Range to Auto Scale",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: freex [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  autoscale, xset, xrange, yset, yrange, freey",
	  "",
	  NULL
      } 
  },

  { "freey", 
      {
	  "Set Y-Axis Range to Auto Scale",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: freey [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  autoscale, xset, xrange, yset, yrange, freex",
	  "",
	  NULL
      } 
  },

  { "freq", 
      {
	  "Select Frequency to Analyze",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: freq xxx.xxxxx [options]",
	  "",
	  "where: 'xxx.xxxxx' is the desired freq in GHz",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "fv", 
      {
	  "Set Plot X-Axis to Freq over Velocity",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: fv [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  cc, cf, cv, fc, ff, vc, vf, vv",
	  "",
	  NULL
      } 
  },

  { "fver", 
      {
	  "Change the Data File Version",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: fver [version] [options]",
	  "",
	  "options are:",
	  "",
	  "where: 'version' is the version number of the desired data-file",
	  "        with no args, a GUI will pop up for selection",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "fwhm", 
      {
	  "Utility to Compute Full-Width-Half-Max",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: fwhm [options]",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  "Variables: dish_size, taper, freq",
	  "",
	  NULL
      } 
  },
