﻿/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */

  { "g1", 
      {
	  "Display the Last IF 1 Gains",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: g1 [options]",
	  "",
	  "options are:",
	  "",
	  "/c  Show the Cold Array (if present)",
	  "/s  Show the Sky  Array",
	  "/v  Show the Vane Array",
	  "/h  help",
	  "",
	  "See Also:  g1, g2, g3, g4, g12, g13",
	  "",
	  NULL
      } 
  },

  { "g2", 
      {
	  "Display the Last IF 2 Gains",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: g2 [options]",
	  "",
	  "options are:",
	  "",
	  "/c  Show the Cold Array (if present)",
	  "/s  Show the Sky  Array",
	  "/v  Show the Vane Array",
	  "/h  help",
	  "",
	  "See Also:  g1, g2, g3, g4, g12, g13",
	  "",
	  NULL
      } 
  },

  { "g3", 
      {
	  "Display the Last IF 3 Gains",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: g3 [options]",
	  "",
	  "options are:",
	  "",
	  "/c  Show the Cold Array (if present)",
	  "/s  Show the Sky  Array",
	  "/v  Show the Vane Array",
	  "/h  help",
	  "",
	  "See Also:  g1, g2, g3, g4, g12, g13",
	  "",
	  NULL
      } 
  },

  { "g4", 
      {
	  "Display the Last IF 4 Gains",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: g4 [options]",
	  "",
	  "options are:",
	  "",
	  "/c  Show the Cold Array (if present)",
	  "/s  Show the Sky  Array",
	  "/v  Show the Vane Array",
	  "/h  help",
	  "",
	  "See Also:  g1, g2, g3, g4, g12, g13",
	  "",
	  NULL
      } 
  },

  { "g11", 
      {
	  "Display the Last IF 11 Gains",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: g11 [options]",
	  "",
	  "options are:",
	  "",
	  "/c  Show the Cold Array (if present)",
	  "/s  Show the Sky  Array",
	  "/v  Show the Vane Array",
	  "/h  help",
	  "",
	  "See Also:  g1, g2, g3, g4, g12, g13",
	  "",
	  NULL
      } 
  },

  { "g12", 
      {
	  "Display the Last IF 12 Gains",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: g12 [options]",
	  "",
	  "options are:",
	  "",
	  "/c  Show the Cold Array (if present)",
	  "/s  Show the Sky  Array",
	  "/v  Show the Vane Array",
	  "/h  help",
	  "",
	  "See Also:  g1, g2, g3, g4, g12, g13",
	  "",
	  NULL
      } 
  },

  { "gauss", 
      {
	  "Fit a Spectral or Continuum Gaussian",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: gauss [n] [linename] [options]",
	  "",
	  "options are:",
	  "",
          "/a Compute (1-23) gaussian plot based on peak channel (Spectral)",
          "  [/1] Gaussian curve  (Spectral)",
          "   /2  Gaussian curves (Spectral)",
          "   ...",
          "   /23 Gaussian curves (Spectral)",
          "/c to clear all previous Gaussian curves (Spectral)",
          "/f Turn OFF all or (1-23) Gaussian fit(s)",
          "  [/1] Gaussian curve (Spectral)",
          "   /2  Gaussian curve (Spectral)",
          "   ...",
          "   /23 Gaussian curve (Spectral)",
          "/m Compute (1) gaussian plot based on middle channel (Spectral)",
          "/n n, Enter a linename for the Gaussian fit(n)",
          "/o Turn ON all or (1-23) Gaussian fit(s)",
          "  [/1] Gaussian curve (Spectral)",
          "   /2  Gaussian curve (Spectral)",
          "   ...",
          "   /23 Gaussian curve (Spectral)",
          "/l List all Fitted Gaussian Curves (Spectral)",
          "/p plot the data + Gaussian curve (Continuum)",
          "/s save the data + Gaussian curve (Continuum)",
          "/s Do multiple Splat/Lovas line searchs of each Gaussian fit (Spectral)",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "get", 
      {
	  "Fetch Scan",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: get [xxxx.xx | stackscan] [options]",
	  "",
	  "where: 'xxxx.xx' is the desired scan to fetch",
	  "       ‘stackscan’ is the value in variable:stackscan",
	  "",
	  "options are:",
	  "",
	  "/r  If in Spec mode, Remove any bad channels after the get",
	  "/h  help",
	  "",
	  "See Also: stack, variable:autobad, variable:stackscan",
	  "",
	  NULL
      } 
  },

  { "gfilename", 
      {
	  "Set Gains file name",
	  "",
	  "Mode: SPEC/OTF",
	  "",
	  "Usage: gfilename [gname] [options]",
	  "",
	  "options are:",
	  "",
	  "where: 'gname' is the desired gains file",
	  "       with no arg, a GUI will pop up for selection",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "gget", 
      {
	  "Fetch Gains Scan",
	  "",
	  "Usage: gget gscan [options]",
	  "",
	  "options are:",
	  "",
	  "where: 'gscan' is the desired gains scan  to fetch",
	  "",
	  "/h  help",
	  "",
	  "See Also: g1, g2, g3, g4, gset, gfilename",
	  "",
	  NULL
      } 
  },

  { "glabel", 
      {
	  "Place Gaussian Info Block on the Plot",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: glabel [options]",
	  "",
	  "options are:",
	  "",
          "/Cbl blue    date",
          "/Crd red     date",
          "/Cgr green   date",
          "/Ccy cyan    date",
          "/Cmg magenta date",
          "/Cyl yellow  date",
          "/Cwt white   date",
	  "",
	  "/c  Clear out the Title Block Info",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "glist", 
      {
	  "Dump a Listing of Gain Scans",
	  "",
	  "Usage: glist [options]",
	  "",
	  "options are:",
	  "",
          "/c  List only Cold Cals",
          "/g  grep for source and freq (if specified)",
          "/l  Run output through 'less'",
	  "/h  help",
	  "",
	  "See Also:  dumpscans",
	  "",
	  NULL
      } 
  },

  { "gnuplot", 
      {
	  "Help for GnuPlot Plot Windows",
	  "",
	  "The following are short-cuts when using a Gnuplot plot window",
	  "",
	  "Some of the standard Gnuplot short-cuts are removed to prevent conflict",
	  "with Linuxpops internal, such as Grid On/Off, Log Scale, mouse labeling",
	  "",
	  " <B1> doubleclick           send mouse coordinates to clipboard (pm win wxt x11)",
	  " <B2>                       annotate the graph using `mouseformat` (see keys '1', '2')",
	  "                            or draw labels if `set mouse labels is on`",
	  " <Ctrl-B2>                  remove label close to pointer if `set mouse labels` is on",
	  " <B3>                       mark zoom region (only for 2d-plots and maps)",
	  " <B1-Motion>                change view (rotation); use <Ctrl> to rotate the axes only",
	  " <B2-Motion>                change view (scaling); use <Ctrl> to scale the axes only",
	  " <Shift-B2-Motion>          vertical motion -- change xyplane",
	  " <wheel-up>                 scroll up (in +Y direction)",
	  " <wheel-down>               scroll down",
	  " <shift-wheel-up>           scroll left (in -X direction)",
	  " <shift-wheel-down>         scroll right",
	  " <Control-WheelUp>          zoom in on mouse position",
	  " <Control-WheelDown>        zoom out on mouse position",
	  " <Shift-Control-WheelUp>    pinch on x",
	  " <Shift-Control-WheelDown>  expand on x",

	  " Space          raise gnuplot console window",
	  " q            * close this plot window",
	  "",
	  " b              `builtin-toggle-border`",
	  " e              `builtin-replot`",
	  " i              `builtin-invert-plot-visibilities`",
	  " r              `builtin-toggle-ruler`",
	  " V              `builtin-set-plots-invisible`",
	  " v              `builtin-set-plots-visible`",
	  " n              `builtin-zoom-next` go to next zoom in the zoom stack",
	  " p              `builtin-zoom-previous` go to previous zoom in the zoom stack",
	  " u              `builtin-unzoom`",
	  " +              `builtin-zoom-in` zoom in",
	  " =              `builtin-zoom-in` zoom in",
	  " -              `builtin-zoom-out` zoom out",
	  " Right          `scroll right in 2d, rotate right in 3d`; <Shift> faster",
	  " Up             `scroll up in 2d, rotate up in 3d`; <Shift> faster",
	  " Left           `scroll left in 2d, rotate left in 3d`; <Shift> faster",
	  " Down           `scroll down in 2d, rotate down in 3d`; <Shift> faster",
	  " Escape         `builtin-cancel-zoom` cancel zoom region",
	  "",
	  "See Also:  xgraphic",
	  "",
	  NULL
      } 
  },

  { "gracefit", 
      {
	  "Fit a 'nfit' LSQ-fit to data",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: gracefit [options]",
	  "",
	  "options are:",
	  "",
	  "/o Perform fitting to OBJECT Array",
	  "   /s  Save Resulting fit term in elgain_term[x]",
	  "/v Be Verbose",
	  "/h help",
	  "",
	  "NOTE: 'nfit' must be 0 - 19",
	  "",
	  "See Also: object, baseline, bshape, bspline",
	  "See Variable: nfit",
	  "",
	  NULL
      } 
  },

  { "greview", 
      {
	  "Display a GUI of Scan Plots",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: greview [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: review, preview",
	  "",
	  NULL
      } 
  },

  { "grid", 
      {
	  "Set Plot Grid Mode",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: grid [options]",
	  "",
	  "options are:",
	  "",
	  "/o  grid off",
	  "/x  x-ticks only",
	  "/y  y-ticks only",
	  "/b  both x-ticks and y-ticks",
	  "/h  help",
	  "",
	  "See Variable:  zline",
	  "",
	  NULL
      } 
  },

  { "gmap", 
      {
	  "Grid Map Frontend",
	  "",
	  "Mode: CONT",
	  "",
	  "Usage: gmap* [options]",
	  "",
	  "options are:",
	  "",
	  "/b Call: gmapmapbase, Remove Baseline from Gridded Map",
	  "/c Call: gmapslice,   Cut a slice of map and display",
	  "/d Call: gmapdive,    Plot Grid Map with varing dB's",
	  "/e Call: gmapclear,   Clear out internal Gridmap data",
	  "/f Call: gmapgauss,   Fit Gaussian to a Gridded Map Cente",
	  "/g Call: gmapgrid,    Accumulate Selected CONTMAP Scans",
	  "/l Call: gmaplimits,  Set / Reset Plotting Limits",
	  "/p Call: gmapplot,    Plot 3D Gaussian Gridded Map",
	  "/r Call: gmaprotate,  Rotate Gridmap and Snap images",
	  "/s Call: gmapstats,   Display Stats of Grid Map",
	  "/x Call: gmapgrab,    Force Gridmap Window to be Active",
	  "",
	  "/h  help",
	  "",
	  "gmap is a frontend to multiple grid mapping functions:",
	  "",
	  "See individual command help for their use.",
	  "",
	  "CAUTION: This command uses over-loaded flags; The above flags must be first",
	  "",
	  "See Also: gmapclear,  gmapdive,    gmapgauss,  gmapgrab,   gmapgrid,",
	  "          gmaplimits, gmapmapbase, gmaprotate, gmappgauss, gmapplot,",
	  "          gmapslice,  gmapstats",
	  "",
	  NULL
      } 
  },

  { "gmapclear", 
      {
	  "Delete Internal Grid Map",
	  "",
	  "Mode: CONT",
	  "",
	  "Usage: gmapclear [options]",
	  "",
	  "options are:",
	  "",
	  "/y  Assume 'yes' for answer",
	  "",
	  "/h  help",
	  "",
	  "See Also: gmap",
	  "",
	  NULL
      } 
  },

  { "gmapmapbase", 
      {
	  "Remove Baselines from Gridded Map",
	  "",
	  "Mode: CONT",
	  "",
	  "Usage: gmapmapbase [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: mbase, gmap",
	  "See variable: mapbaselines",
	  "",
	  NULL
      } 
  },

  { "gmapdive", 
      {
	  "Dive Through Grid Map Display in dB's",
	  "",
	  "Mode: CONT",
	  "",
	  "Usage: gmaprotate maxdB [options]",
	  "",
	  "where: maxdB is the lower limit of Dive: Example: -30",
	  "",
	  "options are:",
	  "",
	  "/r  Record and composite into movie",
	  "  /k  Keep Intermediate Frames in /tmp",
	  "/v  Verbose output",
	  "/h  help",
	  "",
	  "See Also: gmap",
	  "",
	  NULL
      } 
  },

  { "gmapgrab", 
      {
	  "Force Gridmap Window to be Active",
	  "",
	  "Mode: CONT",
	  "",
	  "Usage: gmapgrab [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: gmap",
	  "",
	  NULL
      } 
  },

  { "gmapgrid", 
      {
	  "Accumulate Selected CONTMAP Scans",
	  "",
	  "Mode: CONT",
	  "",
	  "Usage: gmapgrid [options]",
	  "",
	  "options are:",
	  "",
	  "/a  Accum additional map into grid",
	  "/n  Skip removal of baselines from Map; (Or, set 'mapbaselines' to 0",
	  "",
	  "/h  help",
	  "",
	  "See Also: gmap",
	  "See Variables: mapbaselines",
	  "",
	  NULL
      } 
  },

  { "gmaplimits", 
      {
	  "Set / Reset Gridmap Plot Extents",
	  "",
	  "Mode: CONT",
	  "",
	  "Usage: gmapgrid [xb xe yb ye] [options]",
	  "",
	  "Where: xb, xe are the beginning and ending coluums",
	  "       yb, ye are the beginning and ending rows",
	  "",
	  "NOTE: All 4 arguments must be passed when setting",
	  "",
	  "options are:",
	  "",
	  "/r  Reset to defaults",
	  "/s  Show current limits",
	  "/h  help",
	  "",
	  "See Also: gmap",
	  "",
	  NULL
      } 
  },

  { "gmapgauss", 
      {
	  "Fit Gaussian to a Gridded Map",
	  "",
	  "Mode: CONT",
	  "",
	  "Usage: gmapgauss [row] [options]",
	  "",
	  "where: 'row' is the requested row and not the computed center",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: gmap",
	  "",
	  NULL
      } 
  },

  { "gmapplot", 
      {
	  "Plot 3D Gaussian Gridded Map",
	  "",
	  "Mode: CONT",
	  "",
	  "Usage: gmapplot [options] [file_name]",
	  "",
	  "Where: file_name is the name of the csv file create with the /c option",
	  "",
	  "options are:",
	  "",
	  "/0  Set Map angle to  60.0 &  30.0; Orthogonal",
	  "/1  Set Map angle to 180.0 &  90.0; Bottom View",
	  "/2  Set Map angle to  90.0 &   0.0; Side Neg Edge View",
	  "/3  Set Map angle to   0.0 &   0.0; Top View, vertical",
	  "/4  Set Map angle to   0.0 &  90.0; Top View",
	  "/5  Set Map angle to  90.0 & 270.0; Back Side Edge View",
	  "/6  Set Map angle to  90.0 & 180.0; Side Pos Edge View",
	  "/7  Set Map angle to  60.0 & 220.0; Orthogonal Reversed",
	  "/8  Set Map angle to  90.0 &  90.0; Front Edge View",
	  "/9  Set Map angle to 270.0 & 270.0; Back Edge View (Upside Down)",
	  "",
	  "/a  Square the Aspect Ratio",
	  "/b  include base plane; can't have /b & /w",
	  "  /b  Reverse surface plotting vs base plane",
	  "/c  make a beam_map.csv file of map",
	  "  /a  Create an automatic file_name for map",
	  "/d  Scale to dB's",
	  "/f  Fixed Map Angle",
	  "/g  Use Gray Scale Color Table",
	  "/i  When drawing a surface, interpolate 2,2",
	  "  /i  multiple instances raises it ( up to 10,10)",
	  "/l  include contours",
	  "/p  make a PS file of plot",
	  "  /p  Twice: make a PNG file of plot",
	  "/s  Place contours on Surface",
	  "  /s  Twice: Both Surface and Base Contours",
	  "",
	  "/r  Use RGB Color Table",
	  "  /r  Use Next Color Table",
	  "    /r  Use Next Color Table",
	  "",
	  "/v  Verbose output",
	  "/w  Wire-Frame View; Can't have /w & /b; negates /i",
	  "",
	  "",
	  "/h  help",
	  "",
	  "See Also: map_angle, gmap",
	  "",
	  NULL
      } 
  },

  { "gmappgauss", 
      {
	  "2D Gaussian Gridded Map Center Row",
	  "",
	  "Mode: CONT",
	  "",
	  "Usage: gmappgauss [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: gmap",
	  "",
	  NULL
      } 
  },

  { "gmaprotate", 
      {
	  "Rotate Grid Map Display 360 degrees",
	  "",
	  "Mode: CONT",
	  "",
	  "Usage: gmaprotate [options]",
	  "",
	  "options are:",
	  "",
	  "/r  Record and composite into movie",
	  "  /k  Keep Intermediate Frames in /tmp",
	  "/v  Verbose output",
	  "/h  help",
	  "",
	  "See Also: gmap",
	  "",
	  NULL
      } 
  },

  { "gmapslice", 
      {
	  "Display a slice of Grid Map",
	  "",
	  "Mode: CONT",
	  "",
	  "Usage: gmapslice [row# | col#] [options]",
	  "",
	  "options are:",
	  "",
	  "/y  Show columns [default = rows]",
	  "/h  help",
	  "",
	  "See Also: gmap",
	  "",
	  NULL
      } 
  },

  { "gmapstats", 
      {
	  "Display statistics of a Grid Map",
	  "",
	  "Mode: CONT",
	  "",
	  "Usage: gmapstats [filename] [options]",
	  "",
	  "options are:",
	  "",
	  "/i  Display Integrated Time for each Cell",
	  "/p  Display Processed Values for each Cell",
	  "/r  Display Raw Values for each Cell",
	  "/s  Display Scan Counts for each Cell",
	  "/t  Display Average Tsys for each Cell",
	  "/w  filename, write stats to 'filename'",
	  "/h  help",
	  "",
	  "See Also: gmap",
	  "",
	  NULL
      } 
  },

  { "gset", 
      {
	  "Set the Gains File to Match Primary Data File",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: gset [options]",
	  "",
	  "options are:",
	  "",
	  "NOTE: Gainsfile must be located and named the same as the datafile with the",
	  "      exception of the [g] in the name; i.e. gsdd.sys_001 vs. sdd.sys_001",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "gver", 
      {
	  "Change the Gains File Version",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: gver [gversion] [options]",
	  "",
	  "where: 'gversion' is the desired gains file version",
	  "       if no args, then GUI will pop up for selection",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },
