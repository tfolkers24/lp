﻿/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */
/* S Help */

  { "s", 
      {
	  "Show Second IF",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: xxxx s [options]",
	  "",
	  "where: 'xxxx' is the root integer scan number",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: f",
	  "",
	  NULL
      } 
  },

  { "sat", 
      {
	  "Strings And Things sub-system",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: sat [options]",
	  "",
	  "options are:",
	  "",
	  "/c  Clear out all Strings And Things",
	  "/l  List out all Strings And Things",
	  "/f  Turn Strings And Things OFF",
	  "/o  Turn Strings And Things ON",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "save", 
      {
	  "Save H? Image to nsave in Savefile",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: save fname [options]",
	  "",
	  "where: 'fname' is the file to save the image to",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: variable:nsave",
	  "",
	  NULL
      } 
  },

  { "saveall", 
      {
	  "Write All Configurations to File",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: saveall fname [options]",
	  "",
	  "options are:",
	  "",
	  "where: 'fname' is the file to save the images to",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "scansum", 
      {
	  "Scan Summary Sub-System",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: scansum [options]",
	  "",
	  "options are:",
	  "",
	  "/i  Initialize the Scan Summary Structure",
	  "/r  Record Current Scan",
	  "/s  Send To XGraphics Program for Plotting",
	  "",
	  "Additional Flags for Send",
	  "",
	  "  /u  Use UT  as the X-Axis (default is Scan Number)",
	  "  /l  Use LST as the X-Axis",
	  "  /a  Use Az  as the X-Axis",
	  "  /e  Use El  as the X-Axis",
	  "",
	  "  /m  Plot RMS  as the Y-Axis",
	  "  /t  Plot Tsys as the Y-Axis",
	  "  /c  Plot Tcal as the Y-Axis",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "savedata", 
      {
	  "Write Data Table to File",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: savedata fname [options]",
	  "",
	  "where: 'fname' is the file to save the data to",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "saveenv", 
      {
	  "Save the Current Environment to Stack",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: saveenv [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: restoreenv",
	  "",
	  NULL
      } 
  },

  { "sfilename", 
      {
	  "Set SAVE filename",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: sfilename fname [options]",
	  "",
	  "where: 'fname' the name of the save file",
	  "",
	  "options are:",
	  "",
	  "/e  Erase Existing File First",
	  "/c  Create File Boot-Strap Block",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "sfocus", 
      {
	  "Analyze a Spectral Line Focus",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: sfocus scan.xx [options]",
	  "",
	  "where: 'scan.xx' is the scan number of the spectral focus",
	  "",
	  "options are:",
	  "",
	  "/2 Use the Scans that are spaced by 2; i.e. xxx.01 & xxx.03",
	  "/p save the intermediate plots in .png format",
	  "   /t send results to MONITOR (SMT & 12M only)",
	  "/h  help",
	  "",
	  "See Also: spec5",
	  "See Variables: sf_fitcenter, sf_start, sf_end, sf_bchans, sf_fitmhz",
	  "",
	  NULL
      } 
  },

  { "say", 
      {
	  "Say Something in the Terminal",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: say 'something to say' [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: pause, delay",
	  "",
	  NULL
      } 
  },

  { "scale", 
      {
	  "Set the Calibration Scale Factor",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: scale x.xx [options]",
	  "",
	  "where: 'x.xx' is the calibration scaling factor",
	  "",
	  "        Spectral line mode; scale plot: data[n] / x.xx",
	  "",
	  "NOTE:   In Spectral Line, action is only executed once and lost",
	  "        on the next accum.  Tsys and Tcal are scaled too",
	  "",
	  "        In Continuum, data is scaled continuously while in 'calmode: scale'",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  calmode",
	  "",
	  NULL
      } 
  },

  { "selfrej", 
      {
	  "Auto Reject the current scan if 'ratio' > rms/trms",
	  "",
	  "Usage: selfrej ratio [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "selprint", 
      {
	  "Print the Current X Selection",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: selprint [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
          "/l for landscape",
          "/b for 'b' size paper",
          "cpixx where xx = point-size",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "set", 
      {
	  "Assign a value to a variable",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: set var=xx.x [options]",
	  "",
	  "where: set 'var' variable to 'xx.x'",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: let",
	  "",
	  NULL
      } 
  },

  { "sex", 
      {
	  "Convert number to/from Sexagesimal",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: sex dd:mm:ss.ss [options]",
	  "       sex xxx.xxxxxx [options]",
	  "",
	  "where: dd:mm:ss.ss is a sexagesimal number to convert to decimal",
	  "       xxx.xxxxxx is a decimal number to convert to sexagesimal",
	  "",
	  "options are:",
	  "",
	  "/exp print results in scientific notion",
	  "/t   passed value is in TIME",
	  "/h   help",
	  "",
	  "See Also: ",
	  "",
	  NULL
      } 
  },

  { "sdd", 
      {
	  "Convert Motorola sdd file to Intel format",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: sdd infile outfile [options]",
	  "",
	  "where: 'infile'  is the circa <2005 Motorola sdd data file",
	  "       'outfile' is the output sdd data file",
	  "",
	  "options are:",
	  "",
	  "/c  convert file",
	  "/l  load file once complete",
	  "/h  help",
	  "",
	  "See Also:  class, pdfl",
	  "",
	  NULL
      } 
  },

  { "shell", 
      {
	  "Fit a Spectral Circumstellar Shell",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: shell [options] [filename] [linename]",
	  "",
	  "NOTE: Shells are numbered 0-23",
	  "",
	  "options are:",
	  "",
          "/c Clear All or a single shell curve(s)",
	  "   /0 - /23, clear only that shell",
	  "   /g, clear out the 'guess' plot",
	  "",
          "/d Define (1-24) shells based (2) mouse clicks",
          "  /1 - /24 Shell curve(s)",
	  "",
	  "/f Turn all or a single shell's visibility OFF",
          "  /0 - /23 Shell curve",
	  "",
          "/i Highly interactive shell define based on multiple mouse clicks",
	  "",
          "/m Remove any shell identifing number labels",
          "   /m Mark shells with identifing number labels",
	  "",
          "/n Enter a linename for the Shell fit(n)",
	  "   /0 - /23, name that shell",
	  "",
	  "/o Turn All or a single shell's visibility ON",
          "  /0 - /23 Shell curve",
	  "",
          "/l List all defined Shell Curves",
	  "",
          "/r Read Shells from 'filename'",
	  "",
          "/w Write all Shells to 'filename'",
	  "",
	  "/h  help",
	  "",
	  "See Also: Gauss",
	  "",
	  "CAUTION: This command uses over-loaded flags; The action flags must be first",
	  "",
	  NULL
      } 
  },

  { "show", 
      {
	  "Process and Show Selected Scan",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: show [options]",
	  "",
	  "options are:",
	  "",
	  "/b  Remove a Continuum baseline; QK5 & SEQ only",
	  "/f  If the scan contains multiple bandpasses, show full",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "showbad", 
      {
	  "Show Bad Channels to be entered in Rambo",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: show offset [options]",
	  "",
	  "where: offset is the starting channel of the filter band segment",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "shift", 
      {
	  "Shift Spectra ASHIFT channels",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: shift [options]",
	  "",
	  "options are:",
	  "",
	  "/d  Drop ASHIFT channels from beginning or end",
	  "/h  help",
	  "",
	  "See Also:     autoshift",
	  "See Variable: ashift",
	  "",
	  NULL
      } 
  },

  { "shortcuts", 
      {
	  "Predefined 'Hot-Keys'",
	  "",
	  "Mode: Varies",
	  "",
	  "Usage: hotkeys /h [options]",
	  "",
	  "The Following are Predefined Hot Keys:",
	  "",
	  "NOTE: Not all Hot Keys work in all Modes",
	  "",
	  "Control-a:	Set Auto Scale",
	  "Control-b:	Remove Baseline",
	  "Control-c:	Exit internal loops; repeat withing 2 secs to exit program",
	  "Control-e:	Print Environments",
	  "Control-g:	Fix a Gaussian to strongest line in current view",
	  "Control-l:	Print listing of scans",
	  "Control-p:	Print Environment Parameters",
	  "Control-r:	Redo Last Undo",
	  "Control-t:	Print Table of Scans in File",
	  "Control-w:	Define next available window based on current XGraphics Zoom",
	  "Control-u:	Undo Last Action",
	  "Control-x:	Set plot extents based on current XGraphics Zoom",
	  "",
	  "/h  help",
	  "",
	  "See Also: hotkeys",
	  "",
	  NULL
      } 
  },

  { "sleep", 
      {
	  "Pause execution time uSecs",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: sleep secs [options]",
	  "",
	  "where: 'secs' is how long to sleep in uSeconds",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: pause, delay",
	  "",
	  NULL
      } 
  },

  { "source", 
      {
	  "Select Source to Analyze",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: source source_name [*] [options]",
	  "",
	  "where: source_name is source to analyze.",
	  "       If no source provided, a GUI will pop up showing all available sources.",
	  "",
          "Use '*' to set to 'I don't care'"
	  "",
	  "options are:",
	  "",
	  "/a  Use the current HEADER object name as source",
	  "/r  reverse the list in GUI",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "spec5", 
      {
	  "Analyze Spectral Line 5 Points",
	  "",
	  "Mode: SPEC or OTF",
	  "",
	  "Usage: spec5 center_scan_number [options]",
	  "",
	  "where: 'center_scan_number' is the Center Scan Number.",
	  "",
	  "options are:",
	  "",
	  "/c  Clear Results array",
	  "/l  List  Results array",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "spike", 
      {
	  "Remove Spikes in Data",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: spike [ratio] [options]",
	  "",
	  "options are:",
	  "",
	  "where: 'ratio' is the ratio of chan val / adjacent baseline val",
	  "        Default ratio = 200; accepts values 0.1 - 1000",
	  "",
	  "/r  Remove any found spikes",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "splat", 
      {
	  "Look up Molecular line within a Gaussian curve",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: splat [freq del-freq] [search terms] [options]",
	  "",
	  "options are:",
	  "",
	  "/s  arg1 arg2 argn; search the database for 'args'",
	  "/h  help",
	  "",
	  "NOTE: Perform a Gauss on the line of interest first or pass two args",
	  "",
	  "See Also: gauss molecule",
	  "",
	  NULL
      } 
  },

  { "splines", 
      {
	  "List Possible Line Poining Freqs",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: splines [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: ",
	  "",
	  NULL
      } 
  },

  { "split", 
      {
	  "Set 5-point Split Phases Flag (experimental)",
	  "",
	  "Mode: CONT",
	  "",
	  "Usage: split [0 | 1] [options]",
	  "",
	  "options are:",
	  "",
	  "0 turn off",
	  "1 turn on",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "sptip", 
      {
	  "Process SPTIP Data",
	  "",
	  "Mode: CONT",
	  "",
	  "Usage: sptip n [options]",
	  "",
	  "where: n is integer scan number to process",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "stack", 
      {
	  "Stack Routines",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: stack [options]",
	  "",
	  "options are:",
	  "",
	  "/e empty 	(empty the stack)",
	  "/f fill 	(fill the stack with criteria meeting scans)",
	  "/g       (get the scan pointed to by variable:stackscan)",
	  "/l list	(list contents of stack)",
	  "/i inc	(increment the stack pointer)",
	  "/r reset	(reset stack pointer to 0)",
	  "",
	  "/h  help",
	  "",
	  "NOTE: once the stack is full you can do a ‘get stackscan’ to fetch",
	  "      the scan pointed to by the variable:stackscan value",

	  "",
	  "See Also: get, variable:stackscan",
	  "",
	  NULL
      } 
  },

  { "stop", 
      {
	  "Send 'stop' to sub-processes",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: stop [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: done, end",
	  "",
	  NULL
      } 
  },

  { "subtractimg", 
      {
	  "Subtract Hx from Hy; Results left in H0",
	  "",
	  "Mode: SPEC",
	  "",
	  "alias for image /d Hx Hy",
	  "",
	  "Usage: subtractime Hx Hy [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: image",
	  "",
	  NULL
      } 
  },

  { "sum", 
      {
	  "Sum Hx to Hy; Results left in H0",
	  "",
	  "Mode: SPEC",
	  "",
	  "alias for image /s Hx Hy",
	  "",
	  "Usage: sum x y [options]",
	  "",
	  "where: x and y are the numbers of the 'H' holders to accum",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: image",
	  "",
	  NULL
      } 
  },

  { "summary", 
      {
	  "Print a Summary Table of Online Data",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: summary [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "survey", 
      {
	  "Write out a File in Spectral Line Survey Format",
	  "",
	  "Usage: survey filename [options]",
	  "",
	  "where: 'filename' is the desired file to create",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "switchenv", 
      {
	  "Switch to Environment n",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: switchenv n [options]",
	  "",
	  "where: 'n' is the desired environment",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "system", 
      {
	  "Do system command; alias for ':'",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: system 'command and args to execute' [options]",
	  "",
	  "where: all text after the 'system' command are passed",
	  "       to the /bin/sh shell to be executed",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "NOTE: Be vary careful not to attempt execution of commands that",
	  "      will require a <control-c> to terminate, because the Linuxpops",
	  "      interpreter will intercept that and exit Linuxpops; probably not",
	  "      the action you wanted:  I’m still working on this problem",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

