﻿/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */

  { "id", 
      {
	  "Print ID info",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: id [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "if", 
      {
	  "IF statement",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: if ( 'test' ) do_something; [else do_something_else;] endif [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "NOTE: So far this syntax only works when all commands are on the same line",
	  "",
	  "See Also: else, endif, for, endfor",
	  "",
	  NULL
      } 
  },

  { "ignore", 
      {
	  "Add scan(s) to ignored list",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: ignore scan1 scan2 scan3 ... [options]",
	  "",
	  "where: 'scan[x]' are scans numbers to ignore; include fractional part ",
	  "        use a -scan.xx to un-ignore",
	  "",
	  "options are:",
	  "",
	  "/c  clear all ignored scans",
	  "/l  list ignored scans",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "igfile", 
      {
	  "Populate ignored list from file",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: igfile ifile [options]",
	  "",
	  "where: 'ifile' contains white-space delimited scan numbers",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "image", 
      {
	  "Image (Holders) Sub-System",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: image [Title] [Hs Hd] [Hx Hy] [H1 H2] [options]",
	  "",
	  "options are:",
	  "",
	  "/c  Copy  image Hs, source, to Hd, destination",
	  "/d  Subtract image H2, from H1, place results in H0",
	  "/p  Print image",
	  "/l  List  images",
	  "/s  Summ Hx and Hy, leave results in H0",
	  "/y  Divide Hx by Hy, leave results in H0",
	  "/t  Set title",
	  "         where: 'Title' is the title up to 256 characters",
	  "        (Spaces Allowed) (Quotes Optional, but will appear if used)",
	  "/z  Zero image; Add /a to zero all of them",
	  "   /a  zero them all",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "invert", 
      {
	  "Invert Cont Phases or Spectra",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: invert [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "int", 
      {
	  "Create a Integer Variable",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: int int_name [options]",
	  "",
	  "where: 'int_name' is name of new integer variable",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: double, dlist",
	  "",
	  NULL
      } 
  },
