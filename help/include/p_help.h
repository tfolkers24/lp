﻿/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */
/* P Help */

  { "p", 
      {
	  "Alias for Print; a Dynamic or Header Variable",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: p variable [options]",
	  "",
	  "where: 'variable' is either a dynamic, internal or header variable",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "params", 
      {
	  "Show Selected Current Scan Search Criteria",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: params [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "paramgui", 
      {
	  "Open a GUI allowing Parameter Manipulation (Experimental)",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: paramgui [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "page", 
      {
	  "Clear the plot window",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: page [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "papersize",
      {
	  "Set the Plot Printout Page Size",
          "",
	  "Mode: ALL",
	  "",
          "Usage: papersize width height",
          "Usage: papersize default",
          "Usage: papersize *",
          "Usage: papersize 0",
          "",
          "where: 'width' and 'height' are in inches",
          "where: [*] resets to 10.0\" x 7.5\"",
          "where: [0] resets to 0.0\" x 0.0\"",
	  "/h  help",
          "",
	  "See Also:",
	  "",
          NULL
      }
  },

  { "pause", 
      {
	  "Pause execution until Keyboard hit",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: pause [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "pboot", 
      {
	  "Print the Last SDD Boot Block",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: pboot [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "pdfl", 
      {
	  "Convert Motorola(SunOS) pdfl file to Intel format sdd-file",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: pdfl /c [/l] pdfl-infile sdd-outfile [options]",
	  "",
	  "where: 'infile'  is the circa <2000 Motorola pdfl data file",
	  "       'outfile' is the output sdd data file",
	  "",
	  "options are:",
	  "",
	  "/c  convert file",
	  "/l  load file once complete",
	  "/h  help",
	  "",
	  "See Also:  class, sdd",
	  "",
	  NULL
      } 
  },

  { "peak", 
      {
	  "Show Peak Temperature in Current Image Holder",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: peak [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "peaks", 
      {
	  "Show Peak Temperature in each Image Holder",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: peaks [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "pflux", 
      {
	  "Show a Plot of Planet Flux",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: pflux planet [options]",
	  "",
	  "where: 'planet' is one of the 7 planets",
	  "       Mercury, Venus, Mars, Jupiter, Saturn, Neptune, Uranus",
	  "       Sorry Pluto...",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  plan, planets",
	  "",
	  NULL
      } 
  },

  { "pgauss", 
      {
	  "Print Results of Last Gaussian Fit",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: pgauss [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "phases", 
      {
	  "Show Phases of current Scan",
	  "",
	  "Mode: CONT",
	  "",
	  "Usage: phases [options]",
	  "",
	  "options are:",
	  "",
	  "/s  Perform a 'switch' on the phases",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "planets", 
      {
	  "Internal Planet info",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: plan [date] [freq] [options]",
	  "",
	  "options are:",
	  "",
	  "/d  Use the 'date' from the environment",
	  "",
	  "/f  User passed 'freq' in GHz",
	  "    NOTE: If passing both '/u date' & '/f freq', ",
	  "    'date' must be first and 'freq' must be second",
	  "",
	  "/n  Now (use current time for ephemeris)",
	  "",
	  "/u  User passed 'date' in the form: yyyy.mmdd",
	  "    In the range of: 2012.0101 to 2025.1231, for now...",
	  "",
	  "/r  Reload (reload planet ephemeris)",
	  "",
	  "/h  help",
	  "",
	  "NOTE: By default, uses the 'date' from the loaded scan and 'freq' from the",
	  "      environment and should be set before using this function otherwise ",
	  "      'now' will be used for date and you will get errors in regards to",
	  "      un-set frequency",
	  "",
	  "      Set variable dish_size or taper to be used instead of defaults in",
	  "      planet convoluted beam size calculations",

	  "See Also:  pflux, planets, variable:dish_size, variable:taper",
	  "",
	  NULL
      } 
  },

  { "plotseq", 
      {
	  "Plot the Results of SEQ",
	  "",
	  "Mode: CONT",
	  "",
	  "Usage: plotseq [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "pname", 
      {
	  "Generate and filename suggestion based on loaded scans",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: pname [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "points", 
      {
	  "Set plotting format to Points",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: points [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  lines, histo, linepts",
	  "",
	  NULL
      } 
  },

  { "popbase", 
      {
	  "Pop the saved baselines from the stack of (1)",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: popbase [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  bset, nbaseset, baseline, pushbase",
	  "",
	  NULL
      } 
  },

  { "preview", 
      {
	  "Review Stack Scans in Parallel",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: preview [options]",
	  "",
	  "options are:",
	  "",
	  "/f  Fix scale to present values",
	  "/n  Don't ask, just delay(loopdelay2) and move on to next",
	  "/h  help",
	  "",
	  "See Also: review, greview",
	  "",
	  NULL
      } 
  },

  { "print", 
      {
	  "Print a Dynamic or Header Variable",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: print variable1, variable2, ... [options]",
	  "",
	  "where: 'variable' is either a dynamic, environmental or header variable",
	  "        Also, various structures: header, weather, source, telescope, map, status",
	  "",
	  "options are:",
	  "",
	  "/exp print results in exponential",
	  "/hex print results in hexadecimal",
	  "/n   Don't print variable names",
	  "/s   Strip all white-space before printing",
	  "/w   Write output to userfile instead of stdout",
	  "/1   /2 /3 ... /9;  Precision for doubles; applies to all variables; (Sorry)",
	  "/1   /2 /3 ... /13; When printing the 'header', print only these classes",
	  "/h   help",
	  "",
	  "NOTE: Use: @Space, @Tick or @Quote to add those characters to printout",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "prune", 
      {
	  "Create a Criteria based SDD file",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: prune filename [options]",
	  "",
	  "where: 'filename' is the new file name",
	  "",
	  "options are:",
	  "",
	  "/r  Remove any existing file first",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "pshow", 
      {
	  "Show Parallel Scans side by side",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: pshow scanno [options]",
	  "",
	  "where: 'scanno' is the first scan to display. The second scan",
	  "        will be scanno + 0.01",
	  "",
	  "options are:",
	  "",
	  "/i  invert spectra after accum",
	  "/b  remove baseline after accum",
	  "/h  help",
	  "",
	  "See Also: qshow",
	  "",
	  NULL
      } 
  },

  { "psc12", 
      {
	  "Show Parallel Accum for IF1 & IF2",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: psc12 [options]",
	  "",
	  "options are:",
	  "",
	  "/b  remove baselines",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "psc13", 
      {
	  "Show Parallel Accum for IF1 & IF3",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: psc13 [options]",
	  "",
	  "options are:",
	  "",
	  "/b  remove baselines",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "psc24", 
      {
	  "Show Parallel Accum for IF2 & IF4",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: psc24 [options]",
	  "",
	  "options are:",
	  "",
	  "/b  remove baselines",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "psc34", 
      {
	  "Show Parallel Accum for IF3 & IF4",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: psc34 [options]",
	  "",
	  "options are:",
	  "",
	  "/b  remove baselines",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "pstamp", 
      {
	  "Place a Postage Stamp Window on Spectral Plot",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: [n] [fname] [label] [options]",
	  "",
	  "options are:",
	  "",
	  "/e  n, Erase Postage Stamp 'n'",
	  "    /a Erase All Postage Stamps",
	  "/l  List Postage Stamps",
	  "/c  n 'label text', Create Postage Stamp 'n', with label 'label txt'",
	  "/r  fname, Read in file of Postage Stamps",
	  "/w  fname, Write out file of Postage Stamps",
	  "",
          "/Cbl blue    pstamp",
          "/Crd red     pstamp",
          "/Cgr green   pstamp",
          "/Ccy cyan    pstamp",
          "/Cmg magenta pstamp",
          "/Cyl yellow  pstamp",
          "/Cwt white   pstamp",
          "",
          "/1  4 point Helvetica font",
          "/2  6 point Helvetica font",
          "/3  8 point Helvetica font",
          "/4 10 point Helvetica font",
          "/5 12 point Helvetica font (default)",
          "/6 14 point Helvetica font",
          "/7 16 point Helvetica font",
          "/8 18 point Helvetica font",
          "/9 20 point Helvetica font",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },


  { "ptime", 
      {
	  "Set Time for Planet Flux Calculations",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: ptime xx.x [*] [options]",
	  "",
	  "where: xx.x is the UT time in hours",
	  "",
          "Use '*' to set to 'I don't care'"
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "pushbase", 
      {
	  "Push the baselines into the stack of (1)",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: pushbase [options]",
	  "",
	  "options are:",
	  "",
	  "/c  Clear out the currently defined baslines",
	  "/h  help",
	  "",
	  "See Also:  bset, nbaseset, baseline, popbase",
	  "",
	  NULL
      } 
  },
