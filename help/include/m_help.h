﻿/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */
/* M Help */

  { "make", 
      {
	  "Send Last Plot to Printer or Capture-file",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: make [capture] [gui] [filename] [options]",
	  "",
	  "where: 'capture' means to make a .pdf file instead of sending to printer",
	  "       'filename' is the name of the resulting file when using capture",
	  "       'gui' will popup a GtkLP GUI to allow refined printer options",
	  "",
	  "options are:",
	  "",
	  "/b  Print to 'B' paper",
	  "/c  capture plot to PDF/PS file",
	  "/d  Force plot to fit Documentaion format",
	  "/p  create a PS file, not PDF",
	  "  or ",
	  "/j  create PDF and a JPEG file",
	  "/v  Pop up a window showing the PDF file",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "makehelp", 
      {
	  "Make Help Files (Sys Admin Only)",
	  "",
	  "Usage: makehelp [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "mapangle", 
      {
	  "Set Display Angle of a Gridded Map",
	  "",
	  "Mode: CONT",
	  "",
	  "Usage: mapangle [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "marker",
      {
	  "Place Vertical Markers on Plot",
          "",
	  "Mode: SPEC",
	  "",
          "Usage: marker /c n [options]",
          "Usage: marker /p n x-axis-value [/a] [ /m y1 y2]",
          "Usage: marker /e n",
          "Usage: marker /d",
          "Usage: marker /l",
          "Usage: marker /s filename",
          "Usage: marker /r filename",
          "",
          "Where:\tn = marker number [0 - 32]",
          "",
          "options are:",
          "",
          "/c create marker n",
          "/p place  marker n",
          "  /a Auto placement of y locations using plot scale",
          "  /l Auto placement of y locations using line intensities",
          "  /m passed args  placement of y locations",
          "/e erase  marker n",
          "/d delete all markers",
          "/l list  all markers",
          "/s save all markers to filename",
          "/r read & create markers from filename",
          "/h help",
          "",
          "/Cbl blue    marker",
          "/Crd red     marker",
          "/Cgr green   marker",
          "/Ccy cyan    marker",
          "/Cmg magenta marker",
          "/Cyl yellow  marker",
          "/Cwt white   marker",
          "",
	  "See Also:  fork, label, hline, datestring",
	  "",
          NULL
      }
  },

  { "mbase", 
      {
	  "Set # of Samples for Map Baselines",
	  "",
	  "Mode: CONT",
	  "",
	  "Usage: mbase [-]x [options]",
	  "",
	  "where: 'x' is the number of samples on both ends of map row",
	  "       '-x' use middle 'x' samples of the map row",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: gridmapbase",
	  "See variable: mapbaselines",
	  "",
	  NULL
      } 
  },

  { "measure", 
      {
	  "Measure Dist Between Spec Features",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: measure [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "midrow", 
      {
	  "Show the Middle Row of Map",
	  "",
	  "Mode: CONT",
	  "",
	  "Usage: midrow [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "mode", 
      {
	  "Set the Mode; CONT | SPEC | OTF",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: mode [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "molecule", 
      {
	  "Manually Select a Line for ID",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: molecule [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: splat",
	  "",
	  NULL
      } 
  },

  { "moment", 
      {
	  "Calculate the 'moment' of a region of Spectra",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: moment (Interactive Use)",
	  "Usage: moment bchan echan",
	  "Usage: moment [action_flag] [filename]",
	  "",
	  "options are:",
	  "",
	  "   Where 'action_flags are:",
	  "",
	  "/c Clear out the Moments database",
	  "/i Ingest all Scans and compute Moments",
	  "   /a Append the scans to database",
	  "   /v Verbose output and plotting",
	  "/l List the current Moments Database",
	  "/p Plot the current Moments Database",
	  "   /e Plot Elevation on X-Axis",
	  "   /a Plot Area.mhz on Y-Axis",
	  "      /a Plot Average on Y-Axis",
	  "   /k Plot Kelvin km/s on Y-Axis",
	  "   /t Plot Tsys on Y-Axis",
	  "/s Save the Current Moment measurement in the Database",
	  "/w Write the Current Moment measurements to 'filename'",
	  "",
	  "/h  help",
	  "",
	  "See Also: shell gauss",
	  "",
	  "NOTE: One of the X-Axis need to be Freq Offset",
	  "      Baselines need to be removed first",
	  "      /i ingest mode requires 'moment_bchan', moment_echan',",
	  "         and 'nfit'; to be set; and baseline regions to be defined",
	  "",
	  "Variables: moment_bchan, moment_echan, nfit, baselines",
	  "",
	  NULL
      } 
  },

  { "mv", 
      {
	  "Execute a system 'mv' command",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: mv old_file_name new_file_name [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: vi, cp",
	  "",
	  NULL
      } 
  },
