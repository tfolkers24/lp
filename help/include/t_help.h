﻿/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */
/* T Help */

  { "table", 
      {
	  "Printout Spec/[non-switched] Continuum Data",
	  "",
	   "Mode: ALL",
	  "",
	  "Usage: table [options]",
	  "",
	  "options are:",
	  "",
	  "/exp print results in scientific notion",
	  "/h   help",
	  "",
	  "See Also: data",
	  "",
	  NULL
      } 
  },

  { "taccept", 
      {
	  "Use Latest Sptip Result as Current Tau0",
	  "",
	  "Mode: CONT",
	  "",
	  "Usage: taccept [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "tau0", 
      {
	  "Set the Manual Zenith Tau0",
	  "",
	  "Mode: CONT",
	  "",
	  "Usage: tau0 manual_val [*] [options]",
	  "",
	  "where: 'manual_val' is the desired tau0 value",
	  "",
          "Use '*' to set to 'I don't care'"
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "tcur", 
      {
	  "Use Mouse to Get Cursor Temperature",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: tcur [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "tell", 
      {
	  "List Selected Scans, or category of scans",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: tell [options]",
	  "",
	  "options are one of:",
	  "",
	  "acc		Show Sequence that Meet Criteria",
	  "env		Show a Listing of the Environments",
	  "foc		Show Focus Scans that Meet Criteria",
	  "ignored	\tShow Ignored Scans",
	  "qk5		Show Five-Points that Meet Criteria",
	  "map		Show Map Scans that Meet Criteria",
	  "ewf		Show  East-West  Focus Scans that Meet Criteria",
	  "nsf		Show North-South Focus Scans that Meet Criteria",
	  "ondata	\tShow all the Scans in the Current Data File",
	  "seq		Show Sequence that Meet Criteria",
	  "sfocus	\tShow Spec Focus scans that Meet Criteria",
	  "spec5	\tShow Spec Five scans that Meet Criteria",
	  "tip		Show SPTIP Scans that Meet Criteria",
	  "",
	  "/a  list all scans in the selected file",
	  "    pass an arg to, 'list /a arg', and only scans with 'arg' will list",
	  "    will match 'Source', 'Backend' or 'Obsmode'",
	  "",
	  "/n  list Selected scans using the NSAVE value instead of scan number",
	  "/p  list only planets regardless of source name selected; all other criteria apply",
	  "/s  list all scans in the save-file",
	  "/t  print a summary of the selected file",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "tips", 
      {
	  "Show All SPTIP's [if any]",
	  "",
	  "Mode: CONT",
	  "",
	  "Usage: tips [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "title", 
      {
	  "Set Title of Current Environment",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: title 'Title Name' [options]",
	  "",
	  "where: 'Title Name' is title string; Spaces allowed; 32 Chars Limit",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: imagetitle, name",
	  "",
	  NULL
      } 
  },

  { "titleblock", 
      {
	  "Place a Title Block on the Plot",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: titleblock Title [options]",
	  "",
	  "where: 'Title can be multiple word title",
	  "",
	  "options are:",
	  "",
	  "/a   Place All Available Info (No Gaussian yet)",
	  "/b   Include Baseline Info",
	  "/g   Include Last Gaussian Fit Info; (Not implemented yet)",
	  "/p   Include Peak Info",
	  "/r   Include rms Info",
	  "",
          "/Cbl blue    date",
          "/Crd red     date",
          "/Cgr green   date",
          "/Ccy cyan    date",
          "/Cmg magenta date",
          "/Cyl yellow  date",
          "/Cwt white   date",
	  "",
	  "/c   Clear out the Title Block Info",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "tcsh", 
      {
	  "Invoke a 'tcsh'",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: tcsh [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "NOTE: Use <control-d> to exit shell; this is probably a better way",
	  "      to execute a external command because it’s not effected by a",
	  "      <control-c>",
	  "",
	  "See Also: ‘:’, system",
	  "",
	  NULL
      } 
  },

  { "tsys", 
      {
	  "Tsys Sub-System",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: tsys [multiple options]",
	  "",
	  "Options:",
	  "",
	  "tsys elmin xx.x; Set lower plot Elevations to xx.x",
	  "tsys elmax xx.x; Set upper plot Elevations to xx.x",
	  "",
	  "tsys fmin xx.x; Set lower plot Frequency to xx.x",
	  "tsys fmax xx.x; Set upper plot Frequency to xx.x",
	  "",
	  "tsys tmax xx.x; Set upper plot Tsys to xx.x",
	  "",
	  "tsys query; Query the Plot parameters",
	  "",
	  "/a Show all freqs without using popup selector",
	  "/i ingest; Load all 'backend' matching scans into the tsys structure",
	  "/l list:   Prints a list of Freqs, Active-State and #Data-Points",
	  "/p plot",
	  "	Plot the Tsys vs Elev values",
	  "	Note: a selection pop-up will appear first unless the /a flag is passed",
	  "/3 Plot 3D Plot",
	  "	Displays a 3D version of the Freq vs Elev plot",
	  "/s select",
	  "	Select which frequencies to plot.",
	  "	Must use 'tsys /p' first",
	  "/h help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "trec", 
      {
	  "Display a Cold Cal T-Receiver Array",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: trec scan_number.xx [options]",
	  "",
	  "where: 'scan_number.xx is the cold-cal scan number + fraction",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  g1, g2, g3, g4, g12, g13",
	  "",
	  NULL
      } 
  },

