﻿/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */
/* L Help */

  { "label",
      {
	  "Create or Delete a screen Label",
          "",
	  "Mode: SPEC",
	  "",
          "Usage: label create n 'label text' [options]",
          "Usage: label set n x.xx y.yy c d 'label text' [options]",
          "Usage: label move n [options] (relocate a single label, n)",
          "Usage: label delete n         (deletes a single label, n)",
          "",
          "Where:\tn = label number",
          "\tx = xaxis placement",
          "\ty = yaxis placement",
          "\tc = color [0=blk,1=red,2=grn,3=yel,4=blu,5=mag,6=cyn,7=wht]",
          "\td = label direction [ 0=c,1=w,2=e,3=d]",
          "\t'label text' is the label without quote marks",
          "\t'filename' is the file to save/load label to/from",
          "",
          "options are:",
          "",
	  "/c  clear out all labels",
	  "/l  List labels",
	  "/p  Display Help for PostScript Enhanced Options",
	  "/r  filenmae, Load labels from 'filename'",
	  "/s  filenmae, Save labels to 'filename'",
	  "/h  help",
          "",
          "/Cbl blue    label",
          "/Crd red     label",
          "/Cgr green   label",
          "/Ccy cyan    label",
          "/Cmg magenta label",
          "/Cyl yellow  label",
          "/Cwt white   label",
          "",
          "default:   Placement: (Label is Horizontal, Centered Above  Mouse Click)",
          "/e East    Placement: (Label is Horizontal, to the Right of Mouse Click)",
          "/w West    Placement: (Label is Horizontal, to the Left  of Mouse Click)",
          "/d Down    Placement: (Label is Vertical, Above Mouse Click)",
          "/n         No Arrow",
          "",
          "/1  4 point Helvetica font",
          "/2  6 point Helvetica font",
          "/3  8 point Helvetica font",
          "/4 10 point Helvetica font",
          "/5 12 point Helvetica font (default)",
          "/6 14 point Helvetica font",
          "/7 16 point Helvetica font",
          "/8 18 point Helvetica font",
          "/9 20 point Helvetica font",
          "",
          "",
	  "See Also: hline, marker, forks",
	  "",
          NULL
      }
  },

  { "let", 
      {
	  "Assign a value to a defined variable",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: let [options]",
	  "",
	  "Example:  let noint = 1024",
	  "          shorthand, just: noint = 1024",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  eval",
	  "",
	  NULL
      } 
  },

  { "lines", 
      {
	  "Set plotting format to Lines",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: lines [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  histo, points, linepts",
	  "",
	  NULL
      } 
  },

  { "linepts", 
      {
	  "Set plotting format to Lines+Points",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: linepts [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: lines, points, histo",
	  "",
	  NULL
      } 
  },

  { "list", 
      {
	  "List Selected Scans, or [type]",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: list [type_scans] [options]",
	  "",
	  "where:",
	  "[type_scan] one of [acc|env|foc|ignored|qk5|map|ewf|nsf|ondata|seq|tip|weather] ",
	  "",
	  "acc		Show Sequence that Meet Criteria",
	  "env		Show a Listing of the Environments",
	  "foc		Show Focus Scans that Meet Criteria",
	  "ignored	\tShow Ignored Scans",
	  "qk5		Show Five-Points that Meet Criteria",
	  "map		Show Map Scans that Meet Criteria",
	  "ewf		Show  East-West  Focus Scans that Meet Criteria",
	  "nsf		Show North-South Focus Scans that Meet Criteria",
	  "ondata	\tShow all the Scans in the Current Data File",
	  "seq		Show Sequence that Meet Criteria",
	  "sfocus	\tShow Spec Focus scans that Meet Criteria",
	  "spec5	\tShow Spec Five scans that Meet Criteria",
	  "tip		Show SPTIP Scans that Meet Criteria",
	  "weather	Show Weather Info for Scans that Meet Criteria",
	  "",
	  "options are:",
	  "",
	  "/a  list all scans in the selected file",
	  "    pass an arg to, 'list /a arg', and only scans with 'arg' will list",
	  "    will match 'Source', 'Backend' or 'Obsmode'",
	  "",
	  "/e  list Enviroments, not scans",
	  "/n  list Selected scans using the NSAVE value instead of scan number",
	  "/p  list only planets regardless of source name selected; all other criteria apply",
	  "/s  list all scans in the save-file",
	  "/t  print a summary of the selected file",
	  "/h  help",
	  "",
	  "See Also:  get, nget",
	  "",
	  NULL
      } 
  },

  { "load", 
      {
	  "Read in file of valid Commands",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: load [options]",
	  "",
	  "options are:",
	  "",
	  "/i  Attempt to continue passed parse errors",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "logplot", 
      {
	  "Set Y-Axis to 10*log10(y/pk)",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: logplot [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: linearplot",
	  "",
	  NULL
      } 
  },

  { "logging", 
      {
	  "Turn Logging On/Off",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: logginh [options]",
	  "",
	  "options are:",
	  "",
	  "/f  Turn Loggin Off",
	  "/o  Turn Loggin On",
	  "/h  help",
	  "",
	  "See Also: files",
	  "",
	  NULL
      } 
  },

  { "logwatch", 
      {
	  "Start a xterm with a tail of the session log",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: logwatch [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: xwatch",
	  "",
	  NULL
      } 
  },

  { "linearplot", 
      {
	  "Set Y-Axis to Linear Scale",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: linearplot [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: logplot",
	  "",
	  NULL
      } 
  },
