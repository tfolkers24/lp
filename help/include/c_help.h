/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */

/* C Help */

  { "c1",
      {
          "Accumulate IF 1",

	  "",
	  "Mode: ALL",
	  "",
	  "Usage: c1 [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  c1, c2, c3, c4, c12, c13, c24, c34, cb",
	  "",
	  NULL
      } 
  },

  { "c12", 
      {
	  "Accumulate IF 1 & 2",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: c12 [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  c1, c2, c3, c4, c12, c13, c24, c34, cb",
	  "",
	  NULL
      } 
  },

  { "c13", 
      {
	  "Accumulate IF 1 & 3",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: c13 [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  c1, c2, c3, c4, c12, c13, c24, c34, cb",
	  "",
	  NULL
      } 
  },

  { "c2", 
      {
	  "Accumulate IF 2",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: c2 [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  c1, c2, c3, c4, c12, c13, c24, c34, cb",
	  "",
	  NULL
      } 
  },

  { "c24", 
      {
	  "Accumulate IF 2 & 4",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: c24 [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  c1, c2, c3, c4, c12, c13, c24, c34, cb",
	  "",
	  NULL
      } 
  },

  { "c3", 
      {
	  "Accumulate IF 3",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: c3 [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  c1, c2, c3, c4, c12, c13, c24, c34, cb",
	  "",
	  NULL
      } 
  },

  { "c34", 
      {
	  "Accumulate IF 3 & 4",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: c34 [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  c1, c2, c3, c4, c12, c13, c24, c34, cb",
	  "",
	  NULL
      } 
  },

  { "c4", 
      {
	  "Accumulate IF 4",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: c4 [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  c1, c2, c3, c4, c12, c13, c24, c34, cb",
	  "",
	  NULL
      } 
  },

  { "cancel", 
      {
	  "Send a Cancel to XGraphics Program",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: cancel [options]",
	  "",
	  "options:",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: ",
	  "",
	  NULL
      } 
  },

  { "cb", 
      {
	  "Accumulate IF's 1 & 2",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: cb [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  c1, c2, c3, c4, c12, c13, c24, c34, cb",
	  "",
	  NULL
      } 
  },

  { "cc", 
      {
	  "Set Plot X-Axis to Channel over Channel",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: cc [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  cf, cv, fc, ff, fv, vc, vf, vv",
	  "",
	  NULL
      } 
  },

  { "cf", 
      {
	  "Set Plot X-Axis to Channel over Freq",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: cf [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  cc, cv, fc, ff, fv, vc, vf, vv",
	  "",
	  NULL
      } 
  },

  { "cv", 
      {
	  "Set Plot X-Axis to Channel over Velocity",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: cv [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  cc, cf, fc, ff, fv, vc, vf, vv",
	  "",
	  NULL
      } 
  },

  { "calmode", 
      {
	  "Set the Calibration Mode",
	  "",
	  "Mode: CONT",
	  "",
	  "Usage: calmode mode [options]",
	  "",
	  "where: 'mode' is one of: [none | scale | beam | eff]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  autocal",
	  "",
	  NULL
      } 
  },

  { "ccur", 
      {
	  "Get Cursor Channel Number",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: ccur [options]",
	  "",
	  "options:",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  fcur, tcur, vcur",
	  "",
	  NULL
      } 
  },

  { "centerall", 
      {
	  "Analyzes Lots of Maps Rows",
	  "",
	  "Mode: CONT",
	  "",
	  "Usage: centerall [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "centerrow", 
      {
	  "Analyzes A Map Center Row",
	  "",
	  "Mode: CONT",
	  "",
	  "Usage: centerrow [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "changes", 
      {
	  "Display File of Linuxpops Versions and Revisions",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: changes [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  id, about",
	  "",
	  NULL
      } 
  },

  { "chngres", 
      {
	  "Change Spectral Resolution",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: chngres [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "variable 'newres' must be set to the new resolution first",
	  "and must be > then the current resolution",
	  "",
	  "See Variable: newres",
	  "",
	  NULL
      } 
  },

  { "checkfile", 
      {
	  "Dump a listing of scans in data file",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: checkfile [options]",
	  "",
	  "options are: [ | less] or/and [ | grep xxxx]",
	  "",
	  "/s  Show savefile scans",
	  "/h  help",
	  "",
	  "See Also:  check_sdd, dumpscans",
	  "",
	  NULL
      } 
  },

  { "check_sdd", 
      {
	  "Dump Raw Info for Scan From File",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: check_sdd [options]",
	  "",
	  "options are: [ | less] or/and [ | grep xxxx]",
	  "",
	  "/h  help",
	  "",
	  "See Also:  checkfile, dumpscans",
	  "",
	  NULL
      } 
  },

  { "class", 
      {
	  "Convert Class file to sdd file",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: class /c infile outfile [/l] [options]",
	  "Usage: class /s class-file_out [/i] [options]",
	  "",
	  "where: 'infile'  is the class data file",
	  "       'outfile' is the output sdd data file",
	  "",
	  "options are:",
	  "",
	  "/c  convert a 'class-file' to sdd format",
	  "    add /l to load resulting file once complete",
	  "",
	  "/s  Save / append the current image to a 'class-file'",
	  "    add the /i flag to overwrite 'class-file'",
	  "",
	  "/h  help",
	  "",
	  "See Also: sdd, pdfl",
	  "",
	  NULL
      } 
  },

  { "clearall", 
      {
	  "Clear ALL Dynamic Variables",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: clearall [options]",
	  "",
	  "options:",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  double, int, variable=xx.x, dlist",
	  "",
	  NULL
      } 
  },

  { "clearenv", 
      {
	  "Completely Clear an Environment",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: clearenv env# [options]",
	  "",
	  "where: env# is the number of the environment to clear",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: copyenv",
	  "",
	  NULL
      } 
  },

  { "clearvar", 
      {
	  "Clear a Dynamic Variable",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: clearvar variable_name [options]",
	  "",
	  "where: variable_name is the variable to clear",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  double, int, dlist",
	  "",
	  NULL
      } 
  },

  { "codeview", 
      {
	  "Show the Code Segment for Command",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: codeview command [options]",
	  "",
	  "where: command is the code function to view",
	  "",
	  "NOTE: Only works with local instal + code tree",
	  "",
	  "options are:",
	  "",
	  "/n  Don't actually execute vi command; just list",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "color", 
      {
	  "Set Terminal Text Color to 'color'",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: color [color_name] [options]",
	  "",
	  "where: color_name can be: black, red, green, yellow, blue, magenta, cyan, white, reset",
	  "       or pass the color flags",
	  "",
	  "options are:",
	  "",
          "/Cbk for black   color",
          "/Crd for red     color",
          "/Cgr for green   color",
          "/Cyl for yellow  color",
          "/Cbl for blue    color",
          "/Cmg for magenta color",
          "/Ccy for cyan    color",
          "/Cwt for white   color",
	  "/r  Reset to default",
	  "/h  help",
	  "",
	  "NOTE: Most commands that draw on the plot window also take the color option flags",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "copyenv", 
      {
	  "Copies Environment: src -> dest",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: copyenv env#1 env#2 [options]",
	  "",
	  "where: env#1 is copied to env#2",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  clearenv",
	  "",
	  NULL
      } 
  },

  { "copyraw", 
      {
	  "Copies the latest plot raw data to a named file",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: copyraw [filename]",
	  "",
	  "where: filename is a user supplied name which will get over-written",
	  "",
	  "options are:",
	  "",
	  "/a  Automatically generate a filename",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "cp", 
      {
	  "Copies src_file dest_file",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: cp [cp_options] src_file dest_file [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  mv, vi",
	  "",
	  NULL
      } 
  },

  { "cread", 
      {
	  "Read Configuration File",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: cread filename [options]",
	  "",
	  "where: filename is file containing valid commands",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  @",
	  "",
	  NULL
      } 
  },

  { "crisscross", 
      {
	  "Process Criss Cross Map",
	  "",
	  "Mode: CONT",
	  "",
	  "Usage: crisscross [options]",
	  "",
	  "options are:",
	  "",
	  "/b  Remove a balseline from plots",
	  "/d  Delay between plots",
	  "/g  Fit a Gaussian",
	  "/j  Save a JPEG/ONG of the plot",
	  "/p  Plot the Gaussian fit",
	  "/r  Record the Results",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "cursor", 
      {
	  "Place a crosshair and labels on plot",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: cursor [options]",
	  "",
	  "options are:",
	  "",
	  "/c  clear all cross-hairs",
	  "/h  list all cross-hairs",
	  "/h  help",
	  "",
	  "See Also: fork, label, marker, hline",
	  "",
	  NULL
      } 
  },

  { "cwrite", 
      {
	  "Write Environmental Configuration to File",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: cwrite filename [options]",
	  "",
	  "where: 'filename' is file to write config to",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: cread, saveall",
	  "",
	  NULL
      } 
  },

