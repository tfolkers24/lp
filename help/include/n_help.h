﻿/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */
/* N Help */

  { "nbaseset", 
      {
	  "Set All or Some Baselines at Once",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: nbaseset seg start end [options]",
	  "",
	  "where: 'seg'   is the baseline segment identifier",
	  "       'start' is the first channel number of the segment",
	  "       'end'   is the last channel number of the segment",
	  "",
	  "options are:",
	  "",
	  "/l  list current baseline segments",
	  "/h  help",
	  "",
	  "See Also:  baseline, bset, writebase",
	  "",
	  NULL
      } 
  },

  { "name", 
      {
	  "Set Title of Current Environment",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: name title [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  title",
	  "",
	  NULL
      } 
  },

  { "nbset", 
      {
	  "Set Baseline Negative Regions",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: nbset [start end] [options]",
	  "",
	  "Where: start & end are the desired beginning and end of the area of interest",
	  "       in units of lower plot scale; i.e. channels, velocity, freq",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "nget", 
      {
	  "Fetch 'NSAVE' Scan",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: nget [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Variable:  nsave",
	  "",
	  NULL
      } 
  },

  { "nightmode", 
      {
	  "Switch to Dark Color Display",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: nightmode [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: daymode",
	  "",
	  "NOTE: gnuplot will restart during this command",
	  "",
	  NULL
      } 
  },

  { "normalize", 
      {
	  "Normalize the Contiumn Data Array to 1.0",
	  "",
	  "Mode: CONT",
	  "",
	  "Usage: normalize [options]",
	  "",
	  "NOTE: You must fit a baseline curve to the data first.",
	  "      This is meant for use on a series of sequences",
	  "      scans plotted vs. elevation; Good for Elev Gain calc",
	  "",
	  "options are:",
	  "",
	  "/s  Set elgain terms from results",
	  "/h  help",
	  "",
	  "See Also:      gracefit",
	  "See Variables: nfit, elgain, elgain_term* ",
	  "",
	  NULL
      } 
  },

  { "nsac", 
      {
	  "Sum up a number of nsave scans",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: nsac [n nsave1 nsave2 nsave3 ...] [options]",
	  "",
	  "options are:",
	  "",
	  "/c  Clear out all images arrays first",
	  "/h  help",
	  "",
	  "See Also: ",
	  "",
	  NULL
      } 
  },

  { "nutator", 
      {
	  "Analyze Nutator Seperation",
	  "",
	  "Mode: CONT",
	  "",
	  "Usage: nutator scan_num [options]",
	  "",
	  "options are:",
	  "",
	  "/b  Remove baselines",
	  "    /b  Remove baselines using Center too",
	  "/g  Fit a Guassian to both beams",
	  "/h  help",
	  "",
	  "NOTE: Set 'mapbaselines' to something reasonable for baseline removal",
	  "",
	  "See Also: ",
	  "",
	  NULL
      } 
  },

  { "nyquist", 
      {
	  "Computes the Nyquist size for the current freq & dish_size",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: nyquist [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: ",
	  "",
	  NULL
      } 
  },

