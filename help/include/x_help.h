﻿/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */
/* X Help */

  { "xcorrelate", 
      {
	  "Cross Correlate H1 & H2 Arrays",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: xcorrelate [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "xgrab", 
      {
	  "Grabs the Current Plot Range of the Graphics Window",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: xgrab [options]",
	  "",
	  "options are:",
	  "",
	  "/c  Clear the plot range varaibles and return",
	  "/s  Set the plot range varaibles based on current view",
	  "/h  help",
	  "",
	  "See Also:",
	  "See Variables: xplotmax, xplotmin, yplotmax, yplotmin",
	  "",
	  NULL
      } 
  },

  { "xgraphic", 
      {
	  "Send Arbitrary Command to the Xgraphic Program",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: xgraphic string_to_send_to_xgraphic [options]",
	  "",
	  "where: 'string_to_send_to_xgraphic' is the string to send",
	  "",
	  "options are:",
	  "",
	  "/x  Don't send the 'xgraphic' preamble",
	  "/h  help",
	  "",
	  "See Also: ",
	  "",
	  NULL
      } 
  },

  { "xx", 
      {
	  "Alias for replot",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: xx [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  page, show, replot",
	  "",
	  NULL
      } 
  },

  { "xwatch", 
      {
	  "Open Xterm 'tailing' log files from Xgraphics Engine",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: xwatch [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: logwatch",
	  "",
	  NULL
      } 
  },

  { "xrange", 
      {
	  "Set the X-Axis Range to x1, x2",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: xrange x1 x2 [options]",
	  "",
	  "where: 'x1' is the left extent and 'x2' is the right extent of plot",
	  "       'x1' & 'x2' must be in units of the lower x-axis",
	  "",
	  "options are:",
	  "",
	  "/i ignore 'No scans Loaded Yet' error condition",
	  "/h  help",
	  "",
	  "See Also:  xset, xrange, yset, yrange, freex, freey",
	  "",
	  NULL
      } 
  },

  { "xset", 
      {
	  "Use Mouse to Set the X-Range",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: xset [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  xset, xrange, yset, yrange, freex, freey",
	  "",
	  NULL
      } 
  },

