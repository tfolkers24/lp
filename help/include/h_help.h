﻿/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */
/* H Help */

  { "h0", 
      {
	  "Set Active Working Array to H0",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: h0 [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  h1, h2, h3, h4, h5, h6, h7, h8, h9",
	  "",
	  NULL
      } 
  },

  { "h1", 
      {
	  "Set Active Working Array to H1",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: h1 [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  h0, h2, h3, h4, h5, h6, h7, h8, h9",
	  "",
	  NULL
      } 
  },

  { "h2", 
      {
	  "Set Active Working Array to H2",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: h2 [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  h0, h1, h3, h4, h5, h6, h7, h8, h9",
	  "",
	  NULL
      } 
  },

  { "h3", 
      {
	  "Set Active Working Array to H3",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: h3 [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  h0, h1, h2, h4, h5, h6, h7, h8, h9",
	  "",
	  NULL
      } 
  },

  { "h4", 
      {
	  "Set Active Working Array to H4",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: h4 [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  h0, h1, h2, h3, h5, h6, h7, h8, h9",
	  "",
	  NULL
      } 
  },

  { "h5", 
      {
	  "Set Active Working Array to H5",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: h5 [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  h0, h1, h2, h3, h4, h6, h7, h8, h9",
	  "",
	  NULL
      } 
  },

  { "h6", 
      {
	  "Set Active Working Array to H6",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: h6 [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  h0, h1, h2, h3, h4, h5, h7, h8, h9",
	  "",
	  NULL
      } 
  },

  { "h7", 
      {
	  "Set Active Working Array to H7",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: h7 [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  h0, h1, h2, h3, h4, h5, h6, h8, h9",
	  "",
	  NULL
      } 
  },

  { "h8", 
      {
	  "Set Active Working Array to H8",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: h8 [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  h0, h1, h2, h3, h4, h5, h6, h7, h9",
	  "",
	  NULL
      } 
  },

  { "h9", 
      {
	  "Set Active Working Array to H9",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: h9 [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  h0, h1, h2, h3, h4, h5, h6, h7, h8",
	  "",
	  NULL
      } 
  },

  { "hard", 
      {
	  "Alias for make",
	  "",
	  "Mode: ALL",
	  "",
	  "See Also: make",
	  "",
	  NULL
      } 
  },

  { "halves", 
      {
	  "Sum Parallel Plots together",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: halves [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "hanning", 
      {
	  "Perform Hanning Smoothing on data",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: hanning [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "histo", 
      {
	  "Set plotting format to Histograph",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: histo [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: lines, points, linepts",
	  "",
	  NULL
      } 
  },

  { "histogram", 
      {
	  "Plot a Histogram of Spectral Data",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: histogram num_of_bins [options]",
	  "",
	  "where:",
	  "",
	  "num_of_bins must be > 10 and < 1024",
	  "",
	  "options are:",
	  "",
	  "/g  Compute and show a Gaussian fit",
	  "/w  write out a local copy of the results in 'histo.dat'",
	  "/h  help",
	  "",
	  "See Also: ",
	  "",
	  NULL
      } 
  },

  { "history", 
      {
	  "Show Command Line History",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: history [options]",
	  "",
	  "options are:",
	  "",
	  "/f  flush current history to disk",
	  "/t  toggle multi-history On / Off",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  "NOTE: Set 'use_multi_hist = 1' to allow for",
	  "      separate histories for each environment",
	  "      Default is '0', off",
	  "",
	  NULL
      } 
  },

  { "hline",
      {
	  "Add a Horizontal Line to Plot",
          "",
	  "Mode: SPEC",
	  "",
          "Usage: hline n val [options]",
          "Usage: hline n [options]",
          "",
          "Where:\tn = hline number",
          "",
          "options are:",
          "",
          "/c create n'th hline",
          "/d delete n'th hline",
          "/e erase all hlines",
          "/l list hlines",
          "/s save all hlines to filename",
          "/r read all hlines from filename",
          "",
          "/Cbl blue    hline",
          "/Ccy cyan    hline",
          "/Crd red     hline",
          "/Cgr green   hline",
          "/Cmg magenta hline",
          "/Cyl yellow  hline",
          "/Cwt white   hline",
          "/h help",
          "",
	  "See Also:  label, marker, fork, datestring",
	  "",
          NULL
      }
  },

  { "header", 
      {
	  "Display Header for Last Scan",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: header [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  'print header'",
	  "",
	  NULL
      } 
  },

  { "help", 
      {
	  "Show Help",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: help",
	  "Usage: help /a",
	  "Usage: help 'command'",
	  "Usage: command /h",
	  "Usage: a?",
	  "",
	  "",
	  "There are many types of help available",
          "",
          "Simply typing 'help' will provide help for 'help'",
	  "",
	  "Typing 'help /a' will give a listing of all available commands for the mode Linuxpops is in.",
	  "",
	  "Typing 'help /k keyword' will give a listing of all available commands containing 'keyword'.",
	  "",
	  "Typing 'help command' will give help on an individual command.",
	  "",
	  "Typing 'command /h' will give a brief description of that command and any args and options required",
	  "	NOTE: the '/h' needs to be the first flag for this to work",
	  "",
	  "Typing a single letter+? will give a listing of all mode-valid commands starting with that letter",
	  "",
	  "options are:",
	  "",
	  "/a  list all help topics",
	  "/k  list commands with keyword",
	  "/h  help",
	  "",
	  "See Also:  demo, dlist",
	  "",
	  NULL
      } 
  },

  { "helperwait", 
      {
	  "Wait on response from 'helper' process",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: helperwait keyword delay [options]",
	  "",
	  "where: 'keyword' is the expected response",
	  "       'delay' is the wait time in seconds",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: pause",
	  "",
	  NULL
      } 
  },

  { "hload", 
      {
	  "Load image_file into H* array",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: hload fname [options]",
	  "",
	  "where: 'fname' is name of image-file",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: hsave",
	  "",
	  NULL
      } 
  },

  { "hotkeys", 
      {
	  "Predefined 'Hot-Keys'",
	  "",
	  "Mode: Varies",
	  "",
	  "Usage: hotkeys /h [options]",
	  "",
	  "The Following are Predefined Hot Keys:",
	  "",
	  "NOTE: Not all Hot Keys work in all Modes",
	  "",
	  "Control-a:	Set Auto Scale",
	  "Control-b:	Remove Baseline",
	  "Control-c:	Exit internal loops; repeat withing 2 secs to exit program",
	  "Control-e:	Print Environments",
	  "Control-g:	Fix a Gaussian to strongest line in current view",
	  "Control-l:	Print listing of scans",
	  "Control-p:	Print Environment Parameters",
	  "Control-r:	Redo Last Undo",
	  "Control-t:	Print Table of Scans in File",
	  "Control-w:	Define next available window based on current XGraphics Zoom",
	  "Control-u:	Undo Last Action",
	  "Control-x:	Set plot extents based on current XGraphics Zoom",
	  "",
	  "/h  help",
	  "",
	  "See Also: shortcuts",
	  "",
	  NULL
      } 
  },

  { "hsave", 
      {
	  "Save all H* images to image_file",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: hsave [options]",
	  "",
	  "where: 'fname' is name of image-file",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: hload",
	  "",
	  NULL
      } 
  },
