﻿/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */
/* O Help */

  { "osb", 
      {
	  "Change Display to Other Side Band",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: osb [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "object", 
      {
	  "Object Handling Routine",
	  "",
	  "Mode: CONT",
	  "",
	  "Usage: object [options] [args]",
	  "",
	  "options are:",
	  "",
	  "/i  Initialize the entire object structure.",
	  "",
	  "/c  obj_name  Clear out all entries for the named object.",
	  "    Note: The planets and a few Quasars have predetermined indexes.",
	  "    See 'object /l' for a list of named objects.",
	  "",
	  "/d  [x y] [nolines]",
	  "    Display the results using x & y axis with error-bars",
	  "    Where x is:",
	  "      num  = observation number",
	  "      el   = elevation of each grouping.",
	  "      date = show date for x-axis. (not implemented)",
	  "",
	  "    Where y is:",
	  "        Flux & janskys = Janskys",
	  "        tmb            = Main Beam Temperature",
	  "        app & eff      = Aperture/Beam Efficiency (See 'object save')",
	  "",
	  "    [nolines] To suppress the plotting of connecting lines.",
	  "",
	  "    NOTE: with no args just redisplay based on last call",
	  "",
	  "/l  List the Currently Defined Objects.",
	  "",
	  "/m  Use Mouse to Place Fit Labels.",
	  "",
	  "/n  n Set the current working object index to n, [0 based][so, MAX = 31]",
	  "",
	  "/r  Record the current reduction set to the next available slot within the currently selected object.",
	  "    Note: If the freq, source and scans numbers are the same, the system will reuse the same slot.",
	  "    Note: You should set the expected flux and tmb before issuing this command.  Necessary for eff and app plots.",
	  "",
	  "/s  Print out a recap of all object slots for the currently selected object.",
	  "    /s  Sort the Array of Measurements by Increasing Elevation",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  "NOTE: There are  32 Allowed Discrete Objects",
	  "      There are 128 Allowed sets of Measurements Per Object",
	  "",
	  NULL
      } 
  },

  { "ons", 
      {
	  "Turn 'ons' off or on",
	  "",
	  "Mode: CONT",
	  "",
	  "Usage: ons 0|1 [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "otf", 
      {
	  "OTF sub-system",
	  "",
	  "Mode: OTF",
	  "",
	  "Usage: otf [args] [options]",
	  "",
	  "options are:",
	  "",
	  "/a  Configure internal parameter based on loaded scan",
	  "/b  Send just the current baseline info to the OTF gridder",
	  "/c  Send a command directly to the OTF gridder",
	  "/d  scan_num spec-number [num-average]; plot Scan Spectra",
	  "    where: 'scan_num' is the scan number to plot",
	  "           'spec-number' which 100mS spectra to plot",
	  "           'num-average' Optional: Number of 100mS spectra to average",
	  "/e  Exit the OTF Gridder",
	  "    /y  Assume Yes",
	  "/f  Load the current file of OTF scans",
	  "    /f  Do not repaint during loading (faster)",
	  "    /a  Auto-config Bscan & Escan (source name must be set)",
	  "/g  [no args] Grab last Spectral Plot and Display",
	  "    [  0   0] to Grab the strongest cell",
	  "    [col row] to Grab a particular col, row",
	  "    /g  Grab an integrated plot of the sum of all cells",
	  "/i  Init OTF Gridder based in internal values",
	  "    /y  Assume Yes",
	  "/l  List all internal OTF Parameters",
	  "/m  Display a Mosaic of Velocities",
	  "/o  Display a 3d Profile Plot",
	  "    /2 /3 /4 /5 Interpolation level (Really slows down plotting)",
	  "/p  Push Internal Parameter to OTF Gridder without re-initializing",
	  "    /k  Quick mode; Send only params that don't take a lot of time",
	  "/r  Revert Gridder Plot to a Regular Intensity map",
	  "/s  Start the OTF Gridder",
	  "/t  Change OTF gridder plot to Tsys",
	  "    /t  Change OTF gridder plot to rms",
	  "    /t  Change OTF gridder plot to integration time",
	  "    /t  Change OTF gridder plot to hires Interpolation",
	  "/v  Perform Velocity Movie based on Internal Parameter",
	  "    /r  Record individual frames",
	  "    /i  Interpolate individual frames",
	  "/w  Whoa: Send a 'done' command to the Gridder",
	  "/3  Write a 3D Data Cube file to be used in CloudCompare",
	  "",
	  "/h  help",
	  "",
	  "CAUTION: This command uses over-loaded flags; The above flags must be first",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "otfdirect", 
      {
	  "Send a command directly to the OTF gridder",
	  "",
	  "Mode: OTF",
	  "",
	  "Usage: otfdirect 'command to send' [options]",
	  "",
	  "where: 'command to send' is a valid gridder command",
	  "       Try: 'otfdirect help' for a listing of available commands",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "otffix", 
      {
	  "Tell dataserv to repair missing cells; ( cosmetic; use sparingly )",
	  "",
	  "Mode: OTF",
	  "",
	  "Usage: otffix [options]",
	  "",
	  "options are:",
	  "",
	  "/i  Perform a Deep Repair over larger missing cell gaps; limited to MAX_GAP:5",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "otfdisplay", 
      {
	  "Show one or more 100 mS spectra from a OTF scan",
	  "",
	  "Mode: OTF",
	  "",
	  "Usage: otfdisplay scan_number spectra_number[navg] [options]",
	  "",
	  "where: 'scan_number' is the OTF scan to plot",
	  "       'spectra_number' the individual spectral plot desired",
	  "       'navg' the number of spectra to average and plot",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "otfhelp", 
      {
	  "Shortcut to OTF help",
	  "",
	  "Mode: OTF",
	  "",
	  "Usage: otfhelp [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: otf /h",
	  "",
	  NULL
      } 
  },

  { "otfslice", 
      {
	  "Extract a slice through an OTF map and return to LinuxPops",
	  "",
	  "Mode: OTF",
	  "",
	  "Usage: otfslice num [options]",
	  "",
	  "where: 'num' is which row or col",
	  "",
	  "options are:",
	  "",
	  "/c  Extract a col 'num'",
	  "/r  Extract a row 'num'",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },
