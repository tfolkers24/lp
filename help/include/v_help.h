﻿/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */
/* V Help */

  { "variables", 
      {
	  "Information on Variables (Alias for 'dlist')",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: variables [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: int, double, dlist",
	  "",
	  NULL
      } 
  },

  { "vc", 
      {
	  "Set Plot X-Axis to Velocity over Channel",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: vc [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  cc, cf, cv, fc, ff, fv, vf, vv",
	  "",
	  NULL
      } 
  },

  { "vcur", 
      {
	  "Use Mouse to Get Cursor Velocity Value",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: vcur [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "vf", 
      {
	  "Set Plot X-Axis to Velocity over Freq",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: vf [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  cc, cf, cv, fc, ff, fv, vc, vv",
	  "",
	  NULL
      } 
  },

  { "vv", 
      {
	  "Set Plot X-Axis to Velocity over Velocity",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: vv [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  cc, cf, cv, fc, ff, fv, vc, vf",
	  "",
	  NULL
      } 
  },

  { "verbose", 
      {
	  "Set the Verbose Level",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: verbose n [options]",
	  "",
	  "where: n is a mask in powers of 2",
	  "",
	  "currently defined levels:",
	  "",
	  "     0 Verbose               OFF",
	  "     1 Verbose for Parsing   Info",
	  "     2 Verbose for Auto Tau  Calculations",
	  "     4 Verbose for Criteria  Selection",
	  "     8 Verbose for Xgraphic  Activity",
	  "    16 Verbose for FBOFFSET  Routines",
	  "    32 Verbose for BASELINE  Routines",
	  "    64 Verbose for STATS     Routines",
	  "   128 Verbose for RECURSIVE Info",
	  "   256 Verbose for Batchfile Info",
	  "   512 Verbose for Alias     Info",
	  "  1024 Verbose for Fileops   Info",
	  "  2048 Verbose for Gridmap   Routines",
	  "  4096 Verbose for Flag      Checking",
	  "  8192 Verbose for Atm       Routines",
	  "",
	  "multiple levels can be active at once",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "versions", 
      {
	  "Show List of Available Data Files",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: versions [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  chngver, fver",
	  "",
	  NULL
      } 
  },

  { "vi",
      {
	  "Invoke a 'vi' Editor",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: vi filename [options]",
	  "",
	  "options are:",
	  "",
	  "where: filename is the file to edit",
	  "",
	  "/h  help",
	  "",
	  "See Also: mv, cp",
	  "",
	  NULL
      } 
  },

