﻿/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */
/* R Help */

  { "rap", 
      {
	  "Reduce Freq-Switch Data",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: rap [options]",
	  "",
	  "options are:",
	  "",
	  "/a  Fetch Freq-Ref & Freq-Sig from header; otherwise set 'fs' & 'fr' variables",
	  "",
	  "/h  help",
	  "",
	  "See Also: variable:fs, variable:fr",
	  "",
	  NULL
      } 
  },

  { "read", 
      {
	  "Read in file of Valid Commands",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: read fname [options]",
	  "",
	  "where: 'fname' is the file to read",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "readbadch", 
      {
	  "Read in bad channel file",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: readbadch fname [options]",
	  "",
	  "where: 'fname' is the file to read",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  badchan, showbad",
	  "",
	  NULL
      } 
  },

  { "recall", 
      {
	  "Recall the 'nsave' index entry from SDD file",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: recall [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: get",
	  "",
	  NULL
      } 
  },

  { "record", 
      {
	  "Record commands to filename",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: record on|off fname [options]",
	  "",
	  "where: 'on' turns on recording and 'off' closes out the file",
	  "       'fname' is the file to write to;",
	  "        CAUTION: fname will be over-written",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "redo", 
      {
	  "Redo Previous Command",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: redo [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: redo",
	  "",
	  NULL
      } 
  },

  { "region", 
      {
	  "Map Region Sub System",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: region [region_file] [options]",
	  "",
	  "where: 'region_file' is the region file to load",
	  "",
	  "options are:",
	  "",
	  "/e  Erase all loaded regions",
	  "/l  List loaded regions",
	  "/r  Read/Load in a region file",
	  "    /a  Automatically set OTF map extents based on loaded regions",
	  "    /d  Tell dataserv to also load the file of regions",
	  " ",
	  "    NOTE: Adjust 'otf_size_factor' to add extra padding to map",
	  " ",
	  "/h  help",
	  "",
	  "See Also: otf",
	  "",
	  NULL
      } 
  },

  { "rejection", 
      {
	  "Set Receiver Rejections",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: rejection rej1 rej2 [rej3 rej4] [options]",
	  "",
	  "where: 'rejx' is the rejection for that IF",
	  "       use '-' to skip that IF",
	  "Example: 'rejection 21.0 18.0 - 18', would set only the",
	  "         1st, 2nd and 4th rej's and leave the 3rd one untouched",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "resetenv", 
      {
	  "Reset All Criteria",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: resetenv [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "refresh", 
      {
	  "Repaint the Plot Window",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: refresh [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  xx, page, show",
	  "",
	  NULL
      } 
  },

  { "restoreenv", 
      {
	  "Restore a Saved Environment from Stack",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: restoreenv [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:  read",
	  "",
	  NULL
      } 
  },

  { "report", 
      {
	  "Print out various scan information",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: report [delete] [options]",
	  "",
	  "where: 'delete' means to delete the 'report.log' file",
	  "",
	  "options are:",
	  "",
	  "/o Print Object Name",
	  "/s Print Scan Number",
	  "/d Print Date",
	  "/u Print UT",
	  "/l Print LST",
	  "/a Print Azimuth (Degrees)",
	  "/e Print Elevation (Degrees)",
	  "/p Print Peak Intensity (K)",
	  "/r Print Baseline RMS (mK)",
	  "/t Print Tsys (K)",
	  "/i Print Scan Integration Time (Sec)",
	  "/w Write Report to file 'report.log' instead of stdout",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "review", 
      {
	  "Review 'stack' and possibly Ignore Scans",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: review [options]",
	  "",
	  "options are:",
	  "",
	  "/i  REview Ignored scans and possibly restore",
	  "/f  Fix scale to present values",
	  "/n  Don't ask, just delay(loopdelay1) and move on to next",
	  "/s  If in CONT mode, show the sequences ON/OFFS split",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "rms", 
      {
	  "Print out the rms of the baseline regions",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: rms [options]",
	  "",
	  "options are:",
	  "",
	  "/b  blocksize Compute and plot rms in 'blocksize' pieces",
	  "/p  print the Peak-2-Peak value too",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  "NOTE: hwdc should be set to the known hardware duty cycle.",
	  "      'rms' ignores channels specified by 'bdrop' & 'edrop'",
	  "      baseline regions must be set",
	  "",
	  NULL
      } 
  },

  { "rmbase", 
      {
	  "Remove Computed Baseline",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: rmbase [options]",
	  "",
	  "options are:",
	  "",
	  "/a  Autoscale when done",
	  "/f  Retain the fixed scale range about zero",
	  "/h  help",
	  "",
	  "See Also:  bshape, gracefit, bspline",
	  "",
	  NULL
      } 
  },

  { "run", 
      {
	  "Run a file of valid commands",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: run fname [options]",
	  "",
	  "where: 'fname' is the file with valid commands",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also: load, @ ",
	  "",
	  NULL
      } 
  },

  { "rungauss", 
      {
	  "Run through All scans in the stack and fit a Gaussian; then plot",
	  "",
	  "Mode: SPEC",
	  "",
	  "Usage: rungauss [options]",
	  "",
	  "options are:",
	  "",
	  "/d  Just display last run",
	  "    /e  Sort plot by Elevation: [default sort: scan number]",
	  "    /s  Plot Sum under the Gaussian Fit; otherwise Peak",
	  "/v  Verbose",
	  "    /v  Be a bit more verbose",
	  "        /v  Really be more verbose",
	  "/h  help",
	  "",
	  "NOTE: If there are multiple lines, set the 'xrange' of the region ",
	  "      to fit the Gaussian before calling this command; Up to 2048 scans",
	  "",
	  "See Also: ",
	  "",
	  NULL
      } 
  },
