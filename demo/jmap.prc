#
# DEMO: Process a beam map of Jupiter
#
#
say "Setting up Some Variables"
delay 1000000
#
switch 14
saveenv
title JMAP Tests
mode cont
calmode none
file jmap.sdd
bscan  1494
escan  1524
freq 115.280000
delfreq 0.500000
source Jupiter
feed 1
date 2014.1119
ptime 0.00
tau0  0.000
mbase  4
mapangle 180 90
#
# Grid the map
#
say " "
say "Ready to Grid the Map"
delay 1000000
#
gmapgrid
#
# Compute the gaussian profiles
#
say " "
say "Ready to Compute Map Gaussian Profiles"
delay 1000000
gmapgauss
#
# Display the map
#
say " "
say "Ready to Display Map"
delay 1000000
#
plotwin=20
#
gmapplot
#
restoreenv
#
end
