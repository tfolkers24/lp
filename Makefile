
top_srcdir = .

#
# Requires sourcing one of the env.sh or env.csh files before 'make install'
#

SUBDIRS = lib ptlib slalib cactus/lib dscans astro/atm astro class dataserv demo help helper lines linuxpops greview/src gridder logger lovas otf procedures render share splat xgraphic unipops utils bin

.PHONY: subdirs $(SUBDIRS)

# paragui/src

subdirs: $(SUBDIRS)

$(SUBDIRS): check-env
	$(MAKE) -C $@

check-env:
ifndef LINUXPOPS_HOME
	$(error LINUXPOPS_HOME is undefined)
endif

depend:
	for dir in $(SUBDIRS); do \
	  $(MAKE) -C $$dir depend; \
	done

proto:
	for dir in $(SUBDIRS); do \
	  $(MAKE) -C $$dir proto; \
	done

makefiles:
	./imake.csh
	(cd bin; make links)
 
install: check-env
	for dir in $(SUBDIRS) profiles; do \
	  $(MAKE) -C $$dir install; \
	done

clean:
	for dir in $(SUBDIRS); do \
	  $(MAKE) -C $$dir clean; \
	done

realclean:
	for dir in $(SUBDIRS); do \
	  $(MAKE) -C $$dir realclean; \
	done

dox::
	doxygen dox/doxygen.conf > dox/doxygen.output

install_dox: check-env
	(cd dox; $(MAKE) install)
