#include <math.h>

int main()
{
  int i;
  double el, sinel;
  double tau0, tresult0;

  el   = 90.0;

  while(el> 1.0)
  {
    sinel = sin(el / 57.296);

    printf("%.0f ", el);

    tau0 = 0.05;
    for(i=0;i<10;i++)
    {
      tresult0 = exp(-tau0 * 1.0/sinel);

      printf("%.4f ", tresult0);

      tau0 += 0.05;
    }

    printf("\n");


    el -= 1.0;

  }

  return(0);
}
