#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "sdd.h"
#include "header.h"
#include "observing_par.h"

#include "caclib_proto.h"

#define MAX_VERS 512

struct VERSIONS {
	char name[256];
	int  nscans;
};

struct VERSIONS versions[MAX_VERS];


int doZenity(cmd, rst)
char *cmd, *rst;
{
  FILE *fp;
  char *string, line[256];
  static char cmdBuf[4*4096];

//  st_report("doZenity(): cmd = %s", cmd);

  sprintf(cmdBuf, "/usr/bin/zenity %s", cmd);

  if( (fp = popen(cmdBuf, "r") ) <= (FILE *)0 )
  {
    fprintf(stderr, "Unable to open %s\n", cmdBuf);

    return(1);
  }

  bzero(line, sizeof(line));

  while((string = fgets(line, sizeof(line), fp)) != NULL)
  {
    deNewLineIt(line);
    strcpy(rst, line);
  }

  pclose(fp);

  return(0);
}


int findSize(fname, all)
char *fname;
char *all;
{
  int fd, i, n, rt=0;
  struct BOOTSTRAP  boot;
  struct DIRECTORY dir;

  if((fd = open(fname, O_RDONLY))<0)
  {
    fprintf(stderr, "Unable to open %s\n", fname);
    return(-1);
  }

  if( read( fd, (char *)&boot, sizeof(struct BOOTSTRAP))<0)
  {
    perror("read boot");
    close(fd);

    return(-1);
  }

  if(boot.version != 1)
  {
//    printf("File: %s, is old format\n", fname);
    close(fd);

    return(0);
  }

  n = boot.num_entries_used+1;

//  printf("File %s, has %d sccans\n", fname, n);

  if(all)
  {
    for(i=0;i<n;i++)
    {
      if( read( fd, &dir, sizeof(struct DIRECTORY)) <0 ) 
      {
        perror("read dir");
        exit(1);
      }

      if((dir.obsmode & 0x7f) == SEQUENCE)
      {
        rt++;
      }
    }
  }
  else
  {
    rt = n;
  }

  close(fd);

  return(rt);
}


/*
  take a filename like:

  /home/data/yls/sdd_hc.yls_054

  And find all other versions available in:

  /home/data/yls

  Just print the version numer and # of scans 
 */
int main(argc, argv)
int argc;
char *argv[];
{
  int  i, len, j=0, tscans;
  char tbuf[256], sysBuf[512];
  char inBuf[256], *string;
  char bigAssBuf[4*4096], rst[256];
  char *all = NULL;
  struct VERSIONS *p;
  FILE *fp;

  if(argc < 2)
  {
    fprintf(stderr, "Usage: %s path_to_data\n", argv[0]);
    exit(1);
  }

//  fprintf(stdout, "%s %s\n", argv[0], argv[1]);
  

  if(argc == 3)
  {
    if(!strncmp(argv[2], "seq", 3))
    {
      all = "CONTSEQ ";
    }
  }

  strcpy(tbuf, argv[1]);

  len = strlen(tbuf);

  if(len > 6)
  {
    tbuf[len-3] = '?';
    tbuf[len-2] = '?';
    tbuf[len-1] = '?';

    sprintf(sysBuf, "ls -1rL %s", tbuf); /* 'L', In case it's a link */

    if((fp = popen(sysBuf, "r") ) <= (FILE *)0)
    {
      fprintf(stderr, "Unable to open %s\n", sysBuf);
      return(0);
    }

    p = &versions[0];

    while((string = fgets(inBuf, sizeof(inBuf), fp)) != NULL)
    {
      if(strlen(inBuf) > 6)
      {
        inBuf[strlen(inBuf)-1] = '\0'; /* Overwrite newline */
        strcpy(p->name, inBuf);
      }

      j++;

      if(j>=MAX_VERS)
      {
        fprintf(stderr, "Too many files\n");
        break;
      }
      p++;
    }
  }

  tscans = 0;
  p = &versions[0];
  for(i=0;i<j;i++,p++)
  {
    printf("LOADING\n");
    fflush(stdout);
    p->nscans = findSize(p->name, all);

    tscans += p->nscans;
  }

  if(tscans == 0)
  {
    return(1);
  }

  printf("DONE\n");
  fflush(stdout);

  sprintf(bigAssBuf, "--list --height=400 --width=600 --column='Filename' --column='Number of Scans' --multiple --print-column='1' --text='Select a Data File' ");

  p = &versions[0];
  for(i=0;i<j;i++,p++)
  {
    if(p->nscans > 0)
    {
      sprintf(tbuf, "'%s' '%d' ", p->name, p->nscans);
      strcat(bigAssBuf, tbuf);
    }
  }

  doZenity(bigAssBuf, rst);

  strcmprs(rst);

  printf("%s\n", rst);

  return(0);
}
