#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>
#include <math.h>
#include <string.h>
#include <sys/types.h>
#include <sys/time.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "cactus.h"
#include "observing_par.h"
#include "cont.h"
#include "caclib_proto.h"
#include "contlib_proto.h"

static float sinEl[] = {2.6, 2.3, 2.0, 1.7, 1.4, 1.1, 1.4, 1.7, 2.0, 2.3, 2.6};

char errBuf[256];

static float datamax, datamin;


static float sqrarg;

#define SQR(a) (sqrarg=(a),sqrarg*sqrarg)

int sptip_lsqfit(x, y, bdata, edata, sig, mwt, a, b, siga, sigb, chi2, q)
float x[], y[], sig[], *a, *b, *siga, *sigb, *chi2, *q;
int bdata, edata, mwt;
{
  int i;
  float wt, t, sxoss, sx=0.0, sy=0.0,
        st2 = 0.0, ss, sigdat;

  *b = 0.0;

#ifdef TESTING
  printf("bdata = %d, edata =  %d\n", bdata, edata);
  printf("bdata = %.3f %.3f\n", x[bdata], y[bdata]);
  printf("edata = %.3f %.3f\n", x[edata], y[edata]);
#endif

  if(mwt)
  {
    ss=0.0;

    for(i=bdata;i<edata;i++)
    {
      wt  =  1.0 / SQR(sig[i]);
      ss += wt;
      sx += x[i] * wt;
      sy += y[i] * wt;
    }
  }
  else
  {
    for(i=bdata;i<edata;i++)
    {
      sx += x[i];
      sy += y[i];
    }

    ss=edata-bdata;
  }

  sxoss=sx/ss;

  if(mwt)
  {
    for(i=bdata;i<edata;i++)
    {
      t=(x[i]-sxoss)/sig[i];
      st2 += t*t;
      *b += t*y[i]/sig[i];
    }
  }
  else
  {
    for(i=bdata;i<edata;i++)
    {
      t = x[i] - sxoss;
      st2 += t*t;
      *b  += t *y[i];
    }
  }

  *b /= st2;
  *a = (sy - sx * (*b)) / ss;

  *siga = sqrt((1.0 + sx*sx / (ss * st2)) / ss);
  *sigb = sqrt( 1.0 / st2);

  *chi2 = 0.0;

  if(mwt == 0)
  {
    for(i=bdata;i<edata;i++)
    {
      *chi2 += SQR(y[i]-(*a)-(*b)*x[i]);
    }

    *q=1.0;

    sigdat=sqrt((*chi2)/(edata-bdata-2));

    *siga *= sigdat;
    *sigb *= sigdat;
  }

  return(0);
}

#undef SQR



int zero_check(who, dl, data)
char *who;
int dl;
float *data;
{
  int i;

  for(i=0; i < dl; i++)
  {
    if(!data[i])
    {
      sprintf(errBuf, "MSG_ERR %s data contains ZERO'S, ignoring.", who);
      log_msg(errBuf);

      return(0);
    }
  }

  return(0);
}


int sptip_min_max(ydata, dl, scan)
float *ydata;
int dl;
float scan;
{
  char buf[128];
  int i;

  if(ydata == NULL)
  {
    return(1);
  }
                                                   /* redetermine min and max */
  datamax = -9999999999.9;
  datamin = 9999999999.0;

  for(i=0;i<dl;i++)
  {
    if(ydata[i] > datamax)
    {
      datamax = ydata[i];
    }

    if(ydata[i] < datamin)
    {
      datamin = ydata[i];
    }
  }

  if(fabs(datamax - datamin) < DATASPREAD)
  {
    sprintf(buf,"Scan #%7.2f is all %.4f", scan, datamin);
    log_msg(buf);
    datamin -= 1.0;
    datamax += 1.0;

    return(1);
  }

#ifdef TESTING
  printf("Data Min %f, Data Max %f\n", datamin, datamax);
#endif

  return(0);
}



int go_malloc(who, xdata, ydata, sig, dl)
char *who;
float **xdata, **ydata, **sig;
int dl;
{
  char b[80];
  char *m = "Error in ( malloc() )";

  if((*xdata = (float *)malloc((unsigned)(dl*sizeof(float)))) == (float *)NULL)
  {
    sprintf(b,"%s %s", who, m);
    log_msg(b);

    return(1);
  }

  if((*ydata = (float *)malloc((unsigned)(dl*sizeof(float)))) == (float *)NULL)
  {
    sprintf(b,"%s %s", who, m);
    log_msg(b);

    return(1);
  }

  if((*sig   = (float *)malloc((unsigned)(dl*sizeof(float)))) == (float *)NULL)
  {
    sprintf(b,"%s %s", who, m);
    log_msg(b);

    return(1);
  }

  return(0);
}



int freeData(x, y, z)
float *x, *y, *z;
{
  free((char *)x);
  free((char *)y);
  free((char *)z);

  return(0);
}


int sptip_bin_data(ydata, dl, data, head)
float *ydata;
int dl;
float *data;
struct HEADER *head;
{
  int i,j;
  extern struct CONTINUUM *cont;

  if(cont->issmt)
  {
    for(i=0,j=0;i<dl;i+=2)
    {
      ydata[j] = (data[i] + data[i+1]) / 2.0;
      j++;
    }
  }
  else
  {
    for(i=0,j=0;i<dl;i+=4)
    {
      ydata[j] = (data[i] + data[i+1] + data[i+2] + data[i+3]) / 4.0;
      j++;
    }
  }

  return(j);
}




int sptip_display_sptip(tau0, siga, sigb, data, head, plot, chan)
float *tau0, *siga, *sigb;
float *data;
struct HEADER *head;
int plot, chan;
{
  float	*ydata, *xdata, *sig,
	a1,b1,siga1,sigb1,chi2_1,q1,
	a2,b2,siga2,sigb2,chi2_2,q2;
  int   i, j, k, datalen, nlen;
  float sinel[11], delta_airmass, upper_airmass, lower_airmass, gfit;
  char fitName[256], datName[256], demName[256], sysBuf[256], plotfname[256], sname[80];
  FILE *fp;
  extern int validate();
  char *ftype;

  datalen = (int)head->datalen/sizeof(float);

  zero_check("Sptip", datalen, data);

  if(go_malloc("Sptip", &xdata, &ydata, &sig, datalen))
  {
    *tau0 = 0.0;
    *siga = 0.0;
    *sigb = 0.0;
    return(0);
  }


  nlen = sptip_bin_data(ydata, datalen, data, head);

#ifdef TESTING
  printf("Datalen = %d, NoPhases = %d\n", datalen, (int)head->nophase);

  printf("New Datalen: %d\n", nlen);
  printf("Ydata after binning:     ");
  for(i=0,j=0;i<nlen;i++)
  {
    printf("%.2e ", ydata[i]);
  }
  printf("\n");
#endif

  j = 0;
  for(i=0;i<nlen;i+=2)		  /* subtract vane-out from vane-in's */
  {
    ydata[j] = ydata[i] - ydata[i+1];
    j++;
  }

  nlen /= 2;

  if(nlen != 11)
  {
    printf("Problems: datalen != 11: %d\n", nlen);

    *tau0 = 0.0;
    *siga = 0.0;
    *sigb = 0.0;
    freeData(xdata,ydata,sig);

    return(0);
  }

#ifdef TESTING
  printf("New Datalen: %d\n", nlen);
  printf("Ydata After Subtraction: ");
  for(i=0;i<nlen;i++)
  {
    printf("%.0f ", ydata[i]);
  }
  printf("\n");
#endif

  for(i=0;i<nlen;i++)
  {
    xdata[i] = (float)(i) + 1.0;
    ydata[i] = log(ydata[i]);
    sig[i]   = 1.0;
  }

 
  if(sptip_min_max(ydata, nlen, head->scan))
  {
    *tau0 = 0.0;
    *siga = 0.0;
    *sigb = 0.0;
    freeData(xdata,ydata,sig);

    return(0);
  }

                  /* does the header contain the stop and start airmasses ??? */
                                                           /* if so, use them */

  if(head->sptip_start > 0.0 && head->sptip_stop > 0.0)
  {
#ifdef TESTING
    printf("Sptip Lower El: %.1f;  Sptip Upper El: %.1f\n", 
           head->sptip_start, head->sptip_stop);
#endif

    lower_airmass = 1.0 / sin(head->sptip_start * DEG_TO_RAD);
    upper_airmass = 1.0 / sin(head->sptip_stop  * DEG_TO_RAD);

    delta_airmass  = (upper_airmass - lower_airmass) / 5.0;

    sinel[0]  = sinel[10] = lower_airmass;
    sinel[1]  = sinel[ 9] = lower_airmass + delta_airmass * 1.0;
    sinel[2]  = sinel[ 8] = lower_airmass + delta_airmass * 2.0;
    sinel[3]  = sinel[ 7] = lower_airmass + delta_airmass * 3.0;
    sinel[4]  = sinel[ 6] = lower_airmass + delta_airmass * 4.0;
    sinel[5]  =             upper_airmass;

#ifdef TESTING
    printf("Using Custom AirMass:\n");
#endif
  }
  else                       /* if not, it's an old sptip and use the default */
  {
#ifdef TESTING
    printf("Sptip Lower El: 22.62;  Sptip Upper El: 65.38\n");
#endif

    for(k=0;k<11;k++)
    {
      sinel[k] = sinEl[k];
    }
#ifdef TESTING
    printf("Using Default AirMass:\n");
#endif
  }

#ifdef TESTING
  printf("AirMass:  %7.3f %7.3f %7.3f %7.3f %7.3f %7.3f %7.3f %7.3f %7.3f %7.3f %7.3f\n",
                                    sinel[0], sinel[1], sinel[2], 
                                    sinel[3], sinel[4], sinel[5],
				    sinel[4], sinel[3], sinel[2],
				    sinel[1], sinel[0]);

  printf("Log-Data: ");
  for(i=0;i<nlen;i++)
  {
    printf("%7.3f ", ydata[i]);
  }
  printf("\n");
#endif

  sptip_lsqfit( sinel, ydata, 0,  5, sig, 0, &a1, &b1, &siga1, &sigb1, &chi2_1, &q1);
#ifdef TESTING
  printf("pre1-Tau0 = %5.3f %5.4f %f %f\n", a1, b1*-1.0, siga1, sigb1);
#endif

  sptip_lsqfit( sinel, ydata, 5, 10, sig, 0, &a2, &b2, &siga2, &sigb2, &chi2_2, &q2);
#ifdef TESTING
  printf("pre2-Tau0 = %5.3f %5.4f %f %f\n", a2, b2*-1.0, siga2, sigb2);
#endif

  a1 = (a1 + a2) /  2.0;
  b1 = (b1 + b2) / -2.0;

  *tau0 = b1;
  *siga = (siga1 + siga2) / 2.0;
  *sigb = (sigb1 + sigb2) / 2.0;

  printf("%.2f: %5.4f  %f  %f\n", head->scan, *tau0, *siga, *sigb);
#ifdef TESTING
  printf("\n");
#endif

  /* Write out datafile first */
  sprintf(datName, "/tmp/scan.dat");

  if((fp = fopen(datName, "w")) == NULL)
  {
    printf("\nUnable to open %s for writting\n\n", datName);
    freeData(xdata,ydata,sig);

    return(0);
  }

  for(i=0;i<nlen;i++)
  {
    fprintf(fp, "%2d %.3f\n", i+1, ydata[i]);
  }

  fclose(fp);

  /* Write out fit file second */
  sprintf(fitName, "/tmp/tipfit.dat");

  if((fp = fopen(fitName, "w")) == NULL)
  {
    printf("\nUnable to open %s for writting\n\n", fitName);
    freeData(xdata,ydata,sig);

    return(0);
  }

  for(i=0;i<nlen;i++)
  {
    gfit = a1 + ( -b1 * sinel[i]);
    fprintf(fp, "%2d %.3f\n", i+1, gfit);
  }

  fclose(fp);

  /* Now Write out gnuplot command file */
  sprintf(demName, "/tmp/scan.dem");

  if(chan == 0)
  {
    ftype = "w";
  }
  else
  {
    ftype = "a";
  }

  if((fp = fopen(demName, ftype)) == NULL)
  {
    printf("\nUnable to open %s for writting\n\n", demName);
    freeData(xdata,ydata,sig);

    return(0);
  }

  strncpy(sname, head->object, 16);
  sname[16] = '\0';


  if(plot == GNUPLOT)
  {
    fprintf(fp, "set terminal x11 size 640,480 position 3100, 0\n");
    fprintf(fp, "plot \'%s' with histeps\n", datName);
    fprintf(fp, "pause -1 \"Hit Return When Finished\"\n");
  }
  else
  if(plot == PSPLOT)
  {
    sprintf(plotfname, "/tmp/ccal_plot.ps");

    if(chan == 0)
    {
      fprintf(fp, "reset\n");
      fprintf(fp, "set terminal postscript landscape size 10, 8\n");
      fprintf(fp, "set out \"%s\"\n", plotfname);
      fprintf(fp, "set size 1.0, 1.0\n");
      fprintf(fp, "set origin 0.0, 0.0\n");
      fprintf(fp, "set grid\n");
      fprintf(fp, "unset key\n");
      fprintf(fp, "set multiplot\n");
      fprintf(fp, "set xlabel \"Sample Number\"\n");
      fprintf(fp, "set ylabel \"Intensity\"\n");
      fprintf(fp, "set xrange [1:11]\n");
      fprintf(fp, "set size 1.0, 0.5\n");
      fprintf(fp, "set origin 0.0,0.5\n");
    }
    else
    {
      fprintf(fp, "set origin 0.0,0.0\n");
    }

    fprintf(fp, "plot \'%s' with histeps, \'%s' with lines\n", datName, fitName);
  }
  else
  {
    printf("\n Unknown Plot request %d\n\n", plot);
    fclose(fp);
    return(0);
  }

  if(chan == 1)
  {
    fprintf(fp, "unset multiplot\n");
  }

  fclose(fp);

  if(chan == 1)
  {
    sprintf(sysBuf, "/usr/bin/gnuplot %s", demName);
    system(sysBuf);
  }

  freeData(xdata, ydata, sig);

  validate(SP_TIP);

  return(1);
}
