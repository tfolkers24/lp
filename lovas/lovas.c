#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <math.h>

#include "alma.h"
#include "lovas.h"
#include "ansi_colors.h"

#include "caclib_proto.h"
#include "proto.h"

int quiet = 0;

struct LOVAS_CATALOG sc;

struct BANDS {
	char *filename;
	int pad;
	double min;
	double max;
};

struct BANDS bands[] = {
        { "lovas-band2.dat", 0, ALMA_BAND_2_LOWER, ALMA_BAND_2_UPPER },
        { "lovas-band3.dat", 0, ALMA_BAND_3_LOWER, ALMA_BAND_3_UPPER },
        { "lovas-band4.dat", 0, ALMA_BAND_4_LOWER, ALMA_BAND_4_UPPER },
        { "lovas-band5.dat", 0, ALMA_BAND_5_LOWER, ALMA_BAND_5_UPPER },
        { "lovas-band6.dat", 0, ALMA_BAND_6_LOWER, ALMA_BAND_6_UPPER },
        { "lovas-band7.dat", 0, ALMA_BAND_7_LOWER, ALMA_BAND_7_UPPER },
        { "lovas-band8.dat", 0, ALMA_BAND_8_LOWER, ALMA_BAND_8_UPPER },
        { "lovas-band9.dat", 0, ALMA_BAND_9_LOWER, ALMA_BAND_9_UPPER }
};

#define MAX_BANDS 8

char tok[80][80];

int findBand(f)
double f;
{
  int i;
  struct BANDS *p;

  p = &bands[0];
  for(i=0;i<MAX_BANDS;i++,p++)
  {
    if(f >= p->min && f <= p->max)
    {
      return(i);
    }
  }

  return(-1);
}



int get_tok(edit)
char *edit;
{
  int c = 0;
  char *token;

  bzero((char *)&tok, sizeof(tok));

  token = strtok(edit, ":");
  while(token != (char *)NULL)
  {
    sprintf(tok[c++], "%s", token);
    token = strtok((char *)NULL, ":");
  }

  return(c);
}




int loadBand(name)
char *name;
{
  int    j = 0, first = 1;
  char   line[512], *string, *cp, fname[256];
  FILE  *fp;
  struct LOVAS_ENTRY *p;
  char   freq[80], species[80], elcm[80], quantum[80], inten[80];

  cp = getenv("LINUXPOPS_HOME");

  if(!cp)
  {
    printf("LOVAS(): Unable to load %s\n", name);
  }

  sprintf(fname, "%s/share/%s", cp, name);

  if((fp = fopen(fname, "r") ) <= (FILE *)0)
  {
    printf("LOVAS(): Unable to open file %s\n", fname);
    return(1);
  }

  if(!quiet)
  {
    printf("LOVAS(): Loading file: %s\n", fname);
  }

  bzero((char *)&sc, sizeof(sc));

  strcpy(sc.catalog, name);

  p = &sc.entries[0];
  while((string = fgets(line, sizeof(line), fp)) != NULL)
  {
    if(line[0] == '#')
    {
      continue;
    }
/*                        Elcm                                Inten
         1         2         3         4         5         6 
12345678901234567890123456789012345678901234567890123456789012345678
            |           |         |                      |   |     |
  210832.438 NH2CHO        231.161   21( 6,15)- 22( 5,18)      0.000
  210832.438 NH2CHO        231.161   21( 6,15)- 22( 5,18)      0.000
  210836.547 HC7N     V0   654.201  187       -186             2.866
  210836.719 28Si34S  V1   773.006   12       - 11             3.805
  210857.313 CH3CHO         67.518   10( 5, 5)- 11( 4, 8)  E   5.423
  210885.609 H13COOH        17.258    6( 2, 5)-  6( 1, 6)      5.779
  210910.656 HCCCN           0.000   23,1e    - 22,1e 3V7      3.131
  210941.375 DC5N     V0   288.524   83       - 82             2.992
  210948.031 H12COOD        32.164   10( 0,10)-  9( 0, 9)      3.998
  210953.797 H2CCHCN        73.075   20( 3,17)- 20( 2,18)      4.325
 */

    strcpy(p->name,  "         ");

    strncpy(freq, line, 13);
    freq[13] = '\0';
    strcmprs(freq);
    p->freq = atof(freq);

    strncpy(species, line+13, 12);
    species[12] = '\0';
    strcmprs(species);
    strcpy(p->species, species);

    strncpy(elcm, line+26, 9);
    elcm[9] = '\0';
    p->Elcm      = atof(elcm);

    strncpy(inten, line+62, 7);
    inten[7] = '\0';
    p->intensity = atof(inten);

    if(first)
    {
      sc.freqmin = p->freq;
      first = 0;
    }

    sc.freqmax = p->freq;

    strncpy(quantum, line+36, 23);
    quantum[23] = '\0';
    strcmprs(quantum);
    strcpy(p->quantum,  quantum);

    p++;
    j++;

    if(j >= MAX_LOVAS_ENTRIES)
    {
      printf("LOVAS(): Maximum number Entries Loaded\n");
      sc.num = j;
      fclose(fp);

      return(0);
    }
  }

  fclose(fp);

  if(!quiet)
  {
    printf("LOVAS(): # Loaded %d Lovas Lines\n", j);
  }

  sc.num = j;

  if(j > 100)
  {
    return(0);
  }
  else
  {
    return(1);
  }
}


int printHeader()
{
  printf("   Freq       Delta             Species                    Quantum Numbers      Intensity    Elcm-1\n\n");

  return(0);
}


int setColor(c)
int c;
{
  static int oldColor=-1;

  if(c == oldColor)
  {
    return(0);
  }

  switch(c)
  {
    case ANSI_BLACK:   printf("%c[0m",     0x1b); break;
    case ANSI_RED:     printf("%c[31m",    0x1b); break;
    case ANSI_GREEN:   printf("%c[32;1m",  0x1b); break;
    case ANSI_YELLOW:  printf("%c[33;1m",  0x1b); break;
    case ANSI_BLUE:    printf("%c[34;1m",  0x1b); break;
    case ANSI_MAGENTA: printf("%c[35;1m",  0x1b); break;
    case ANSI_CYAN:    printf("%c[36;1m",  0x1b); break;
    case ANSI_WHITE:   printf("%c[37;40m", 0x1b); break;
    case ANSI_RESERVE: printf("%c[0m",     0x1b); break;
    case ANSI_RESET:   printf("%c[39m",    0x1b); break;
  }

  oldColor = c;

  return(0);
}


int printFreq(p, f, delta, hl)
struct LOVAS_ENTRY *p;
double f, delta;
int hl;
{
  if(!quiet)
  {
    if(hl)
    {
      setColor(ANSI_RED);
    }
    else
    {
      setColor(ANSI_BLACK);
    }
  }

  if(!quiet)
  {
    printf("%10.6f (%11.6f) %20.20s %32.32s %10.4f %10.4f\n", 
		p->freq/1e3, delta/1e3, p->species, p->quantum,
		p->intensity, p->Elcm);
  }
  else
  {
    if(hl)
    {
      printf("%20.20s", p->species);
      printf("\n");
    }
  }

  return(0);
}


int findFreq(f, d)
double f, d;
{
  int i, n, first=1, dindx = -1;
  double flo, fhi;
  double delta, dmin = 9999999999.9;
  struct LOVAS_ENTRY *p;

  flo = f - d;
  fhi = f + d;
  n   = sc.num;

  /* First find the closest entry */
  p = &sc.entries[0];
  for(i=0;i<n;i++,p++)
  {
//    log_msg("Comparing %f < %f > %f", flo, p->freq, fhi);

    if(p->freq > flo && p->freq < fhi)
    {
      delta = p->freq - f;

      if(fabs(delta) < dmin)
      {
        dmin = fabs(delta);
        dindx = i;
      }
    }
  }

  p = &sc.entries[0];
  for(i=0;i<n;i++,p++)
  {
    if(p->freq > flo && p->freq < fhi)
    {
      if(first)
      {
        if(!quiet)
	{
          printHeader();
	}

        first = 0;
      }

      delta = p->freq - f;

      if(i == dindx)
      {
        printFreq(p, f, delta, 1);
      }
      else
      {
        printFreq(p, f, delta, 0);
      }
    }
  }

  return(0);
}


int main(argc, argv)
int argc;
char *argv[];
{
  int ret;
  double freq, delta;

  if(strstr(argv[0], "slovas"))
  {
    quiet = 1;
  }

  if(argc < 3)
  {
    printf("Usage: %s freq delta-freq\n", argv[0]);
    exit(1);
  }

  freq  = atof(argv[1]);
  delta = atof(argv[2]);
  
  if((ret = findBand(freq)) >= 0)
  {
    ret = loadBand(bands[ret].filename);

    if(!ret)
    {
      if(!quiet)
      {
	printf("LOVAS(): Searching bands for freq: %f +/- %f\n", freq, delta);
      }

      findFreq(freq*1e3, delta*1e3);
    }
    else
    {
      printf("LOVAS(): Failed to load anought lines\n");
    }
  }
  else
  {
    printf("LOVAS(): No catalog covering freq = %f\n", freq);
    printf("LOVAS(): %f Min to %f Max\n", ALMA_BAND_2_LOWER, ALMA_BAND_9_UPPER);
  }

  if(!quiet)
  {
    setColor(ANSI_BLACK);
  }

  return(0);
}
