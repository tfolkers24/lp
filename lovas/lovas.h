#ifndef LOVAS_H
#define LOVAS_H

#define MAX_LOVAS_ENTRIES  8192

struct LOVAS_ENTRY {
	char   species[80];
	char   name[80];
	char   quantum[80];
	float  freq;
	float  intensity;
	float  Elcm;
	float  ElK;
};

struct LOVAS_CATALOG {
	char   catalog[16];	/* Lovas catalog */
	int    band;		/* ALMA Band number */
	int    num;		/* Number of entries loaded */
	double freqmin;		/* Search min freq */
	double freqmax;		/* Search max freq */

	struct LOVAS_ENTRY entries[MAX_LOVAS_ENTRIES];
};

#endif
