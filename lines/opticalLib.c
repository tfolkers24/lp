#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include "site.h"
#include "sex.h"
#include "sourcedata.h"
#include "caclib_proto.h"
#include "opticalLib_proto.h"

#define SIN626	0.8878153851
#define COS626	0.4601997848

int gregCalc(sd)
struct SOURCE_DATA *sd;
{
  double year, month, day;

  sd->tn   = time(NULL);
  sd->tn  += 25200;
/*
  sd->now  = localtime(&sd->tn);
 */
  sd->now  = localtime((time_t *)&sd->tn);
  year     = sd->now->tm_year + 2000;
  month    = sd->now->tm_mon +1;
  day      = sd->now->tm_mday;
  sd->greg = 365 *year +day;

  if(month <= 2.0)
  {
    sd->greg = sd->greg +((month -1) *31)
                      +(int)((year -1) / 4)
                      -(int)(((int)((year -1) / 100 +1)) *0.75);
  }
  else
  {
    sd->greg = sd->greg +((month -1) *31)
                      -(int)(month *0.4 +2.3)
                      +(int)(year / 4)
                      -(int)((int)((year / 100) +1) *0.75);
  }

  sd->greg -= 722895.5;

  return(0);
}



int calcLst(sd, utset)
struct SOURCE_DATA *sd;
int utset;
{
  double tc = 0.065711,
         gc = 11.927485;


  if(!utset)
  {
    sd->tn = time(NULL);
    sd->tn += 25200;
/*
    sd->now = localtime(&sd->tn);
 */

    sd->now = localtime((time_t *)&sd->tn);
    sd->ut = sd->now->tm_hour
           + sd->now->tm_min / 60.0
           + sd->now->tm_sec / 3600.0 ;
  }

  sd->lst  = tc * sd->greg
          + gc + ((sd->ut / 24.0) *tc)
          + sd->ut + OBSLONGU;

  if(sd->lst > 24.0)
  {
    do
    {
      sd->lst -= 24.0;
    }while(sd->lst > 24.0);
  }
  if(sd->lst < -24.0)
  {
    do
    {
      sd->lst += 24.0;
    }while(sd->lst < 0.0);
  }

  return(0);
}


int calcAzEl(sd)
struct SOURCE_DATA *sd;
{
  sd->hour_angle = sd->lst -sd->ra;

  if(sd->hour_angle < 0.0)
  {
    sd->hour_angle += 24.0 ;
  }

  sd->hour_angle = sd->hour_angle * 15.0 *CTR;	/* cvrt hour_angle to degs */
						/* convert to radians */
						/* calculate elevaton angle */
  sd->el = asin( sd->sindec * sd->sinlat 
	       + sd->cosdec * cos(sd->hour_angle) * sd->coslat);
						   /* calculate azimuth angle */
  sd->az = (1.0 / cos(sd->el))
        * (sd->sindec *sd->coslat -sd->cosdec *cos(sd->hour_angle) *sd->sinlat);

  if(fabs(sd->az) > 1.0)
  {
    if( sd->az >  1.0)
    {
      sd->az = 1.0;
    }
    else
    {
      sd->az = -1.0;
    }
  }

  sd->az = acos(sd->az);
  sd->az *= CTD;                                        /* convert to degrees */

  if(sin(sd->hour_angle) > 0.0)
  {
    sd->az = 360.0 -sd->az;
  }

  sd->el = sd->el *CTD;                                 /* convert to degrees */

  return(0);
}

int calcRaDec(sd)
struct SOURCE_DATA *sd;
{
  double sinE, cosE, tanP, cosA, cosDec, tanDec;

  sinE = sin(sd->el * CTR);
  cosE = cos(sd->el * CTR);
  cosA = cos(sd->az * CTR);

  sd->dec = asin(sd->sinlat * sinE + sd->coslat * cosE * cosA );
  cosDec = cos( sd->dec * CTR);

  tanP = tan(LAT);
  tanDec = tan(sd->dec);
  sd->hour_angle = acos( (sinE / sd->coslat / cosDec ) - tanP * tanDec);

  sd->hour_angle *= CTD;
  sd->hour_angle /= 15.0;

  sd->ra = sd->lst - sd->hour_angle;

  if(sd->ra < 0.0)
  {
    sd->ra += 24.0 ;
  }

  if(sd->ra > 24.0)
  {
    sd->ra -= 24.0 ;
  }

  return(0);
}



/*
A2345+67M50  23 45 30.244   67 31 44.15  5.0 A0      format of new catalog
 */

int parseStarSource(sd)
struct SOURCE_DATA *sd;
{
  char    *cp, buf[256], field_hold[80][80];
  int     f=1;
  double  rahr, ramin, rasec, decdeg, decmin, decsec;

  strcpy(buf, sd->source);
  cp = strtok(buf, ": ");

  while(cp)
  {
    strcpy(field_hold[f++],cp);
    cp = strtok(NULL, ": \n\t");
  }

  strcpy(sd->name,field_hold[1]);

  rahr  = atof(field_hold[2]);
  ramin = atof(field_hold[3]);
  rasec = atof(field_hold[4]);
  sd->ra = rahr +ramin / 60.0 +rasec / 3600.0;

  decdeg = atof(field_hold[5]);
  decmin = atof(field_hold[6]);
  decsec = atof(field_hold[7]);
  sd->dec = fabs(decdeg) +decmin / 60.0 +decsec / 3600.0;

  sd->mag = atof(field_hold[8]);
  strcpy(sd->spec_type,field_hold[10]);

                       /* now check for '-', i.e. ascii 45, in declination,
                     field_hold[5] holds declination degrees in string form */
  if(strchr(field_hold[5],45))
    sd->dec *= -1.0;

  sd->coslat = COSLAT;
  sd->sinlat = SINLAT;

  sd->cosdec = cos(sd->dec * M_PI / 180.0);
  sd->sindec = sin(sd->dec * M_PI / 180.0);

  return(0);
}


int sourceUp(sd)
struct SOURCE_DATA *sd;
{
  if( sd->el > 5.0 && sd->el < 85.0)
  {
    return(1);
  }

  return(0);
}

/*   09:45:13.14  13:30:39.99  J2000.0  cwleo(-1,0)       -26.0 LSR RAD */
/*    0  1  2      3  4  5       6        7                 8    9  10  */

int parseNraoSource(sd)
struct SOURCE_DATA *sd;
{
  char    *cp, buf[256], tbuf[256], field_hold[80][80];
  int     f=0;
  double  rahr, ramin, rasec, decdeg, decmin, decsec;

  bzero(field_hold, sizeof(field_hold));

  strcpy(tbuf, sd->source);
  strupr(tbuf);
  if(strstr(tbuf, "GALACTIC"))
  {
    parseGalacticSource(sd);
    return(0);
  }

  strcpy(buf, sd->source);

  cp = strtok(buf, ": ,\n\t");
  while(cp)
  {
    strcpy(field_hold[f++], cp);
    cp = strtok(NULL, ": ,\n\t");
  }

  rahr   = atof(field_hold[0]);
  ramin  = atof(field_hold[1]);
  rasec  = atof(field_hold[2]);
  sd->ra = rahr +ramin / 60.0 +rasec / 3600.0;

  decdeg  = atof(field_hold[3]);
  decmin  = atof(field_hold[4]);
  decsec  = atof(field_hold[5]);
  sd->dec = fabs(decdeg) +decmin / 60.0 +decsec / 3600.0;

  strcpy(sd->epochstr, field_hold[6]);

  bzero(sd->name, sizeof(sd->name));
  strcpy(sd->name,     field_hold[7]);

  sd->vel = atof(      field_hold[8]);
  strcpy(sd->velstr,   field_hold[8]);
  strcpy(sd->lsrstr,   field_hold[9]);
  strcpy(sd->typestr,  field_hold[10]);

                       /* now check for '-', i.e. ascii 45, in declination,
                     field_hold[5] holds declination degrees in string form */
  if(strchr(field_hold[3],45))
  {
    sd->dec *= -1.0;
  }

  sexagesimal(sd->ra,  sd->rastr,  DDD_MM_SS_S100);
  sexagesimal(sd->dec, sd->decstr, DDD_MM_SS_S100);
  sd->coslat = COSLAT;
  sd->sinlat = SINLAT;
  sd->cosdec = cos(sd->dec * M_PI / 180.0);
  sd->sindec = sin(sd->dec * M_PI / 180.0);

  return(0);
}



/*   76.2000        1.1500  GALACTIC GSMA5             -60.000 LSR RAD */
/*    0			1	2      3		   4	5   6  */

int parseGalacticSource(sd)
struct SOURCE_DATA *sd;
{
  char    *cp, buf[256], field_hold[80][80];
  int     f=0;
  double l,b;

  strcpy(buf, sd->source);

  cp = strtok(buf, " ,\n\t");
  while(cp)
  {
    strcpy(field_hold[f++],cp);
    cp = strtok(NULL, " ,\n\t");
  }

  l = atof(field_hold[0]);
  b = atof(field_hold[1]);
  galtoeq(l, b, &sd->ra, &sd->dec);

  strcpy(sd->epochstr, field_hold[2]);
  strcpy(sd->name,     field_hold[3]);
  sd->vel = atof(      field_hold[4]);
  strcpy(sd->velstr,   field_hold[4]);
  strcpy(sd->lsrstr,   field_hold[5]);
  strcpy(sd->typestr,  field_hold[6]);

  sexagesimal(sd->ra,  sd->rastr,  DDD_MM_SS_S100);
  sexagesimal(sd->dec, sd->decstr, DDD_MM_SS_S100);
  sd->coslat = COSLAT;
  sd->sinlat = SINLAT;
  sd->cosdec = cos(sd->dec * M_PI / 180.0);
  sd->sindec = sin(sd->dec * M_PI / 180.0);

  return(0);
}



/*
 * Galactic to equatorial coordinates; twf 901002. tested and working jeff.
 */

int galtoeq(l, b, j2000ra, j2000dec )
double l, b, *j2000ra, *j2000dec;
{
  double x,y,z,x0,y0,z0;
 
  x0 = cos(b*CTR) * cos((l - 33.0)*CTR);
  y0 = cos(b*CTR) * sin((l - 33.0)*CTR);
  z0 = sin(b*CTR);
  y = x0;
  x = -COS626 * y0 + SIN626 * z0;
  z =  SIN626 * y0 + COS626 * z0;
  *j2000dec = asin(z)*CTD;
  *j2000ra  = atan2(y,x)*CTD + 192.25;
  if( *j2000ra < 0.0)
    *j2000ra += 360.0;

  *j2000ra /= 15.0;

  return(0);
}

