
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "header.h"
#include "defines.h"
#include "gaussian.h"
#include "shell.h"
#include "window.h"
#include "image.h"
#include "global.h"
#include "sourcedata.h"

#include "linuxlib_proto.h"
#include "caclib_proto.h"
#include "opticalLib_proto.h"

#define MAX_LINES 4096
#define USE_SD       1

#define LINE_FILE "lines.dat"

struct LINES {
	double freq;
	char   name[80];
	double kelvins;
	double newfirstif;
        int    centered;
#ifdef USE_SD
	struct SOURCE_DATA sd;
#endif
};

struct LINES lines[MAX_LINES];
struct LINES results[MAX_LINES];

char tok[40][40];
int nlines, debugging = 1;

int get_tok(edit)
char *edit;
{
  int c = 0;
  char *token;

  token = strtok(edit, " \n\t");
  while(token != NULL)
  {
    sprintf(tok[c++], "%s", token);
    token = strtok(NULL, " \n\t");
  }

  return(c);
}


int parseLine(buf)
char *buf;
{
  int n;
  struct LINES *p;

  if(!strncmp(buf, "#", 1))
  {
    return(1);
  }

  n = get_tok(buf);

  if(n == 9)
  {
    p = &lines[nlines];

    p->freq = atof(tok[0]);
    strcpy(p->name, tok[1]);
    p->kelvins = atof(tok[2]);

#ifdef USE_SD
                            /* Create a standard NRAO catalog source from info */
    sprintf(p->sd.source, "%s %s %s %12s %6s LSR RAD", 
                             tok[4], tok[5], tok[6], tok[3], tok[7]);

    parseNraoSource(&p->sd);

    gregCalc(&p->sd);
    calcLst(&p->sd, 0);
    calcAzEl(&p->sd);

    strcpy(p->sd.spec_type, tok[8]);                     /* really the BSP or PS */
    strcpy(p->sd.catalog_filename, LINE_FILE);
#endif

    nlines++;

    if(nlines >= MAX_LINES)
    {
      log_msg("Too many Lines %d", nlines);
      exit(1);
    }
  }
  else
  {
    log_msg("Incorrect Number of Tokens in Line %d", n);
    return(1);
  }

  return(0);
}

int loadLines(fname)
char *fname;
{  
  FILE *fp;
  char line[256];

  if( (fp = fopen(fname, "r") ) <= (FILE *)0 )
  {
    log_msg("FINDLINES: unable to open %s", fname);
    return(1);
  }

  while(fgets(line, sizeof(line), fp))
  {
    parseLine(line);
  }

  fclose(fp);

  log_msg("Loaded %d lines from %s", nlines, fname);

  return(0);
}

int main(argc, argv)
int argc;
char *argv[];
{
  int i, nresults;
  char   fname[256], *cp;
  double freq, firstif;
  double newfreq_hi, newfreq_lo, newfirstif_hi, newfirstif_lo;
  double if_range_lo, if_range_hi;
  double if_range_up, if_range_dn;
  struct LINES *p;

  setCactusEnvironment();

  log_open("findlines", 3);

  if(argc != 3)
  {
    log_msg("Usage: %s freq if_freq", argv[0]);
    exit(3);
  }

  cp = getenv("LINUXPOPS_HOME");

  if(cp)
  {
    sprintf(fname, "%s/share/%s", cp, LINE_FILE);

    if(loadLines(fname))
    {
      exit(1);
    }
  }
  else
  {
    log_msg("Env: LINUXPOPS_HOME, not defined");
    exit(1);
  }

  freq = atof(argv[1]);

  firstif = atof(argv[2]);

  if_range_lo = firstif-1.0;
  if_range_hi = firstif+1.0;

  if_range_up = if_range_hi - firstif;
  if_range_dn = firstif - if_range_lo;

  log_msg("Freq = %f", freq);
  log_msg("FrstIF = %f", firstif);
  log_msg("LO Range: %f Lower and %f Upper", if_range_lo, if_range_hi);
  log_msg("Movable Range: %f up and %f Down", if_range_up, if_range_dn);

  newfreq_hi = freq + if_range_up;
  newfirstif_hi = firstif + if_range_up;
  log_msg("Upper Freq = %f with new IF of %f", newfreq_hi, newfirstif_hi);

  newfreq_lo = freq - if_range_dn;
  newfirstif_lo = firstif - if_range_dn;
  log_msg("Lower Freq = %f with new IF of %f", newfreq_lo, newfirstif_lo);

  nresults = 0;
  p = &lines[0];

  for(i=0;i<nlines;i++,p++)
  {
    if(p->freq >= newfreq_lo && p->freq <= newfreq_hi)
    {
      results[nresults].freq = p->freq;
      strcpy(results[nresults].name, p->name);
      results[nresults].kelvins = p->kelvins;

      results[nresults].newfirstif = ((p->freq - freq) + firstif) * 1000.0;

      results[nresults].centered = 1;

#ifdef USE_SD
      strcpy(results[nresults].sd.name, p->sd.name);
#endif

      nresults++;
    }
    else     /* Check to see if a line is still in the bandpass even if we add
                or subtract 400 MHz to account for the extra Filter Bandwidth */
    if(p->freq >= (newfreq_lo - 0.400) && p->freq <= newfreq_hi)
    {
      results[nresults].freq = newfreq_lo;
      strcpy(results[nresults].name, p->name);
      results[nresults].kelvins = p->kelvins;
      results[nresults].newfirstif = newfirstif_lo * 1000.0;
      results[nresults].centered = 0;

#ifdef USE_SD
      strcpy(results[nresults].sd.name, p->sd.name);
#endif

      nresults++;
    }
    else
    if(p->freq >= newfreq_lo && p->freq <= (newfreq_hi + 0.400))
    {
      results[nresults].freq = newfreq_hi;
      strcpy(results[nresults].name, p->name);
      results[nresults].kelvins = p->kelvins;
      results[nresults].newfirstif = newfirstif_hi * 1000.0;
      results[nresults].centered = 0;

#ifdef USE_SD
      strcpy(results[nresults].sd.name, p->sd.name);
#endif

      nresults++;
    }
  }

  if(nresults)
  {
    log_msg(" ");

    if(nresults>1)
      log_msg("Found %d results", nresults);
    else
      log_msg("Found %d result", nresults);

    log_msg("  Freq      LineName   Kelvins  1stIF   Cent   Source");

    for(i=0;i<nresults;i++)
    {
//      log_msg("FirstIF was %f", results[i].newfirstif);
      log_msg("%8.4f %12.12s    %4.1f   %.1f   %s  %s", 
	results[i].freq, results[i].name, 
        results[i].kelvins, 
        results[i].newfirstif - fmod(results[i].newfirstif, 5.0),
	results[i].centered ? "Yes" : " No",

#ifdef USE_SD
        results[i].sd.name);
#else
	" ");
#endif
    }
  }
  else
  {
    log_msg("No Lines Found for This Freq %f", freq);
  }

  return(0);
}
