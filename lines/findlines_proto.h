 
/* findlines.c */
int get_tok(char *edit);
int parseLine(char *buf);
int loadLines(char *fname);
int main(int argc, char *argv[]);
/* opticalLib.c */
int gregCalc(struct SOURCE_DATA *sd);
int calcLst(struct SOURCE_DATA *sd, int utset);
int calcAzEl(struct SOURCE_DATA *sd);
int calcRaDec(struct SOURCE_DATA *sd);
int parseStarSource(struct SOURCE_DATA *sd);
int sourceUp(struct SOURCE_DATA *sd);
int parseNraoSource(struct SOURCE_DATA *sd);
int parseGalacticSource(struct SOURCE_DATA *sd);
int galtoeq(double l, double b, double *j2000ra, double *j2000dec);
 
