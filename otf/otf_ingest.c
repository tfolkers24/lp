#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#ifndef __DARWIN__
#include <malloc.h>
#endif
#include <math.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>

#include "header.h"
#include "sdd.h"

#include "otf_global.h"

#include "caclib_proto.h"
#include "otf_ingest_proto.h"

#define MAX_RECORD 0x3fffff

struct OTF_GLOBAL *OG;

struct BOOTSTRAP  boot;
struct DIRECTORY  dir;
struct HEADER     head;
float             *data = NULL;
int               bytperrec;

typedef enum {
  OTHER_SCAN=0,
  OFF_SCAN,
  ON_SCAN
} scanType;


struct SCANS {
  int      indx;
  int      entry;
  long     start_rec;
  scanType scantype;
  double   scan;
  double   off;
  char     scanmode[10];
};

int sanity(int fd, struct BOOTSTRAP *, struct DIRECTORY *);

struct SCANS *scanArray=NULL;
int   numOfScans=0, bad_scans=0;

int first=1, last;
float scan=0.0, firstscan;




int findSize(filename )
char *filename;
{
  int fd, n;
  struct BOOTSTRAP  boot;

  if( (fd = pdopen(filename))<0)
    return(-1);

  if( read( fd, (char *)&boot, sizeof(struct BOOTSTRAP))<0 ) {
    perror("read boot");
    (void)close(fd);
    return(-1);
  }
  close(fd);

  n = boot.num_entries_used+1;
  if( (scanArray = (struct SCANS *)calloc(n, sizeof(struct SCANS))) == NULL)
  {
    log_msg("otffixoffs: Error in findSize( malloc() )");
    return(-1);
  }

  return(0);
}



int findScans(filename)
char *filename;
{
  int fd, dirl, bytesinindex, bytesused, num;
  int sizeofDIR;
  struct BOOTSTRAP  boot;
  struct DIRECTORY  dir;
 
  sizeofDIR = sizeof(struct DIRECTORY);
 
  if((fd = pdopen(filename))<0)
  {
    return(-1);
  }
 
  if(read( fd, (char *)&boot, sizeof(struct BOOTSTRAP))<0)
  {
    perror("read boot");
    close(fd);

    return(-1);
  }
 
  /* check bootstrap block */
 
  if(sizeof(struct DIRECTORY) != boot.bytperent)
  {
    log_msg("directory sizes dont match");
    close(fd);

    return(-1);
  }
 
  bytperrec = boot.bytperrec;
  bytesinindex = boot.num_index_rec * boot.bytperrec;
  bytesused = boot.num_entries_used * boot.bytperent;
 
  if(bytesinindex < bytesused+sizeof(struct DIRECTORY))
  {
    log_msg("Directory structure of %s is full.", filename );
//    close(fd);
//
//    return(-1);
  }
 
  if(boot.typesdd || !boot.version)
  {
    log_msg("SDD version or type of %s is invalid.", filename );
    close(fd);

    return(-1);
  }
 
  if(sizeof(struct DIRECTORY) != boot.bytperent)
  {
    log_msg("File %s has faulty direcory size",filename );
    close(fd);

    return(-1);
  }
 
  if(boot.num_entries_used <= 0 )
  {
    log_msg("File %s is empty", filename);
    close(fd);

    return(-1);
  }

  num = 0; bad_scans = 0;

  while(num+bad_scans < boot.num_entries_used)
  {
    dirl = (num+bad_scans) * boot.bytperent + boot.bytperrec;

    lseek( fd, (off_t)dirl, SEEK_SET );

    if(read(fd, (char *)&dir, sizeofDIR ) != sizeofDIR)
    {
      perror("readdir");
      close(fd);

      return(-1);
    }

    if(sanity(fd, &boot, &dir))
    {
      scanArray[num].scan = head.scan;
      scanArray[num].start_rec = dir.start_rec;
      scanArray[num].entry = num + bad_scans;
      num++;
    }
    else
    {
      bad_scans++;			/* Count the insane scans */
    }
  }

  log_msg("DUMPSCANS: File %s Contains %d scans.", filename, num);

  if(bad_scans)
  {
    log_msg("(Excluding the %d erroneous scans above)", bad_scans);
  }

  close(fd);

  return(num);
}
 



int read_scan(scan, filename, start_rec)
float scan;
char *filename;
long start_rec;
{
  int fd, l, i, datalen;
  long max;

  if(data)
  {
    free((char *)data);
    data = NULL;
  }

  if((fd = pdopen(filename))<0)
  {
    return(-1);
  }

  max = bytperrec * (start_rec-1);

  lseek(fd, (off_t)max, SEEK_SET ); 

  if(read(fd, (char *)&head, sizeof(struct HEADER)) < 0)
  {
    perror("read header");
    close(fd);

    return(-1);
  }

  datalen = (int)head.datalen;
  data = (float *)malloc( (unsigned)datalen );
  bzero( (char *)data, datalen );

  if(read( fd, (char *)data, datalen) <0)
  {
    perror("read data");
    log_msg("Error reading scan %7.2f data", scan);
    close(fd);

    return(-1);
  }

                                /* now check for abnormal float in data array */
  l = datalen / sizeof(float);
  for(i=0;i<l;i++)
  {
    if(isnan( (double)data[i]))
    {
      data[i] = 0.0;
    }
  }

  close(fd);

  return((int)head.scan);
}



int pdopen(name)
char *name;
{
  int fd;

  if( (fd = open(name, O_RDONLY, 0666 ))<0) {
    log_msg("Unable to open %s", name );
    return(-1);
  }

  return(fd);
}

/* Perform a sanity check on this scan */
/* Return zero if HEADER is inconsistent with DIRECTORY */

int sanity (int fd, struct BOOTSTRAP *boot, struct DIRECTORY *dir) 
{

  int ret = 1, max, nchan, nchanmax;

  if (dir->start_rec-1 > MAX_RECORD) 
  {
    log_msg("Start record %ud for scan %.2f extends beyond byte 0x7ffffff", dir->start_rec, dir->scan);
    return(0);
  }

  if (dir->end_rec-1 > MAX_RECORD) 
  {
    log_msg("End record %ud for scan %.2f extends beyond byte 0x7ffffff", dir->end_rec, dir->scan);
    return(0);
  }

  max = boot->bytperrec * (dir->start_rec-1);
  (void)lseek( fd, (off_t)max, SEEK_SET ); 

  if(read(fd, (char *)&head, sizeof(struct HEADER) )<0) 
  {
    perror("read header");
    (void)close(fd);
    log_msg("Could not read header for scan %.2f", dir->scan);

    return(0);
  }

  if ((float)head.scan != dir->scan) 
  {
    log_msg("HEADER scan # %.2f doesn't match DIRECTORY: %.2f", head.scan,dir->scan);

    return(0);
  }

  nchan = head.datalen/sizeof(float);
  nchanmax = (boot->bytperrec * (dir->end_rec - dir->start_rec + 1) - sizeof(struct HEADER))/sizeof(float);

  if(nchanmax-nchan < 0 || (nchanmax-nchan)*sizeof(float) >= boot->bytperrec) 
  {
    log_msg("Scan %.2f no. of channels %d greater than max from record size %d", dir->scan, nchan, nchanmax);
    ret = 0;
  }

  if(strncmp(head.object, dir->source, 16)) 
  {
    head.object[16] = 0;
    dir->source[16] = 0;
    log_msg("Scan %.2f header object name %s doesn't match directory's: %s", dir->scan, head.object, dir->source);
    ret = 0;
  }

  return(ret);
}


int loadScans(datafile)
char *datafile;
{
  int  i, ret;
  struct OTF_FILE *of;
  struct OTF_SCAN_RECORD *osr;

  if( findSize(datafile))
  {
    log_msg("otf_ingest: File %s is empty", datafile);
    return(1);
  }
  if( (numOfScans = findScans(datafile)) < 0)
  {
    log_msg("otf_ingest: File %s is empty", datafile);
    return(1);
  }

  of = &OG->files[OG->nfiles];
  bzero((char *)of, sizeof(struct OTF_FILE));

  strcpy(of->filename, datafile);

  osr = &of->scans[0];

  for(i=0;i<numOfScans;i++)
  {
    if((ret = read_scan(head.scan, datafile, scanArray[i].start_rec)) > 0.0)
    {
      if(!strncmp(head.obsmode, "LINEOTF", 7))
      {
	strncpy(osr->source, head.object, 16);
	osr->source[15] = '\0';
	strcmprs(osr->source);

        osr->scanno   = (float)head.scan;
	osr->offscan  = (float)head.offscan;
	osr->gainscan = (float)head.gains;
	osr->freq     = (float)head.restfreq;
	osr->tsys     = (float)head.stsys;
	osr->tcal     = (float)head.tcal;
	osr->ra       = (float)head.epocra;
	osr->dec      = (float)head.epocdec;
	osr->az       = (float)head.az;
	osr->el       = (float)head.el;
	osr->freqres  = (float)head.freqres;
	osr->ut       = (float)head.ut;
	osr->lst      = (float)head.lst;
	osr->sb       = (float)head.sideband;
	osr->vel      = (float)head.velocity;
	osr->deltaxr  = (float)head.deltaxr;
	osr->deltayr  = (float)head.deltayr;

	osr->col      = (int)head.xcell0;
	osr->row      = (int)head.ycell0;
	osr->noint    = (int)head.noint;

	of->nscans++;

	if(of->nscans > MAX_OTF_SCANS)
	{
          log_msg("Too many Scans in file: %s", datafile);

    	  OG->nfiles++;

          return(1);
        }

	osr++;
      }
    }
  }

  if(of->nscans)
  {
    OG->nfiles++;
  }
  

  return(0);
}


int main(argc, argv)
int argc;
char *argv[];
{
  char initials[80], backend[80], pbuf[256], datafile[256];
  char *string, line[256];
  FILE *fp;

  smart_log_open("otf_ingest", 3);

  if(argc < 3)
  {
    log_msg("Usage: %s initials [aos | aro | ffb ]", argv[0]);
    exit(1);
  }

  OG = gm_map("./OTF_GLOBAL", 1);

  strcpy(initials, argv[1]);
  strcpy(backend, argv[2]);

  sprintf(pbuf, "find /home/data/%3.3s -name \"sdd_%3.3s.%3.3s_???\" -print | sort -n", initials, backend, initials);

  fp = popen(pbuf, "r");

  if(!fp)
  {
    log_msg("Unable to execute: %s", pbuf);
    exit(1);
  }

  while((string = fgets(line, sizeof(line), fp)) != NULL)
  {
    deNewLineIt(line);
    strcmprs(line);

    log_msg("Found sdd file: %s", line);

    strcpy(datafile, line);

    loadScans(datafile);
  }

  pclose(fp);

  return(0);
}

