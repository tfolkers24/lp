
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/file.h>
#include <sys/fcntl.h>
#include <sys/mman.h>

#include "header.h"
#include "sdd.h"

#include "otf_global.h"

#include "caclib_proto.h"
#include "otf_ingest_proto.h"

extern struct OTF_GLOBAL *OG;

struct OTF_GLOBAL *gm_map(name, iswrite)
char *name;
int iswrite;
{
  int fd = -1, sz, isrd, iswr=0;
  char zero = 1;
  struct OTF_GLOBAL *map;

  sz = 0;

  /* Try to open the file */

  if(access(name, F_OK))   /* access failed, attempt to create it */
  {
    if(!iswrite)
    {
      log_msg("gm_map can't create a file when called with no write access");
      return(0);
    }

    if(( fd = open( name, O_CREAT | O_RDWR, 0664 )) < 0)
    {
      log_msg("gm_map(): File creation failed");
      return(0);
    }

    /* fill new file with zeros with unix lseek trick */
    lseek(fd, sizeof(struct OTF_GLOBAL)-1, SEEK_SET );
    zero = 0;
    write(fd, &zero, 1 );
  }
  else /* File already exists */
  {
    isrd = access(name, R_OK)==0;
    iswr = iswrite && access(name, W_OK)==0;

    if(iswr && isrd) 
    {
      fd = open(name, O_RDWR,0);
    }
    else
    if(isrd)
    {
      fd = open(name, O_RDONLY,0);
    }

    if(fd < 0)
    {
      log_msg("gm_map could not open file %s",name);
      return(0);
    }

    sz = lseek(fd, 0l, SEEK_END );

    /* Sanity check.  The file ought to be the expected size. */
    if( sz > 0 && sz != sizeof(struct OTF_GLOBAL))
    {

      char *pwd;

      if( !(pwd = getenv("PWD")))
      {
	pwd = "no pwd";
      }

      log_msg("gm_map: %s (pwd %s) has %d bytes but expected %d", 
	name, pwd, sz, sizeof(struct OTF_GLOBAL));

      return(0);
    }
  }

  /* Map to the file */

  map = (struct OTF_GLOBAL *)mmap(0, sizeof(struct OTF_GLOBAL), PROT_READ | (iswr?PROT_WRITE:0), MAP_SHARED, fd, 0);

  close(fd);

  if(map == NULL)
  {
    log_msg("gm_map could not map to %s", name);
    perror("mmap");

    return 0;
  }

  /* Result is also saved in a static pointer */

  OG = map;

  return(map);
}
