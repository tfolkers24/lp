#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#ifndef __DARWIN__
#include <malloc.h>
#endif
#include <math.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <getopt.h>

#include "otf_global.h"

#include "caclib_proto.h"
#include "otf_ingest_proto.h"


#define OPTION_SOURCE   0
#define OPTION_FREQ     1
#define OPTION_FREQRES  2
#define OPTION_SIDEBAND 3
#define OPTION_IF       4


struct option long_options[] = {
        {"source",      required_argument, 0,  0 },
        {"freq",        required_argument, 0,  0 },
        {"freqres",     required_argument, 0,  0 },
        {"sideband",    required_argument, 0,  0 },
        {"if",          required_argument, 0,  0 },

        {0,             0,                 0,  0 }
};      

#define MAX_OPTIONS ((sizeof(long_options) / sizeof(struct option)) -1)

struct OTF_GLOBAL *OG;


int usage(prg)
char *prg;
{
  int i;

  fprintf(stderr, "Usage: %s ", prg);

  for(i=0;i<MAX_OPTIONS;i++)
  {
    fprintf(stderr, "[ --%s arg ] ", long_options[i].name);
  }

  fprintf(stderr, "\n");

  exit(1);
}


int matchSource(source, osr)
char *source;
struct OTF_SCAN_RECORD *osr;
{
  char src[80];

  strcpy(src, osr->source);
  strcmprs(src);

  if(!strcmp(source, src))
  {
    return(1); 
  }

  return(0);
}


int matchFreq(freq, osr)
double freq;
struct OTF_SCAN_RECORD *osr;
{
  if(fabs(freq - osr->freq) < 0.1)
  {
    return(1); 
  }

  return(0);
}


int matchFreqRes(fr, osr)
double fr;
struct OTF_SCAN_RECORD *osr;
{
  if(fabs(fr - fabs(osr->freqres)) < 0.001)
  {
    return(1); 
  }

  return(0);
}


int matchSideBand(sb, osr)
int sb;
struct OTF_SCAN_RECORD *osr;
{
  if(sb == osr->sb)
  {
    return(1); 
  }

  return(0);
}


int getFeed(scan)
double scan;
{ 
  int feed;
  double tmp;
    
  tmp = scan - floor(scan);
    
  if(tmp > 0.1) /* is this the MAC data; .11 .12 */
  { 
    tmp -= 0.1;
  } 
  
  tmp *= 100.0;
  feed = (int)(tmp + 0.25);
      
  return(feed);
} 


int matchIF(whichIF, osr)
int whichIF;
struct OTF_SCAN_RECORD *osr;
{
  int IF;

  IF = getFeed(osr->scanno);

  if(whichIF == IF)
  {
    return(1); 
  }

  return(0);
}




int main(argc, argv)
int argc;
char *argv[];
{
  int    c, i, j, sideband, whichIF, found;
  int    useSource, useFreq, useFreqRes, useSideBand, useIF;
  char   source[80];
  double restfreq, freqres;
  struct OTF_FILE *of;
  struct OTF_SCAN_RECORD *osr;
  
  smart_log_open("otf_fetch", 3);

  if(argc < 3)
  {
    usage(argv[0]);
  }
  
  /* Set these so they can't possibly match */
  strcpy(source, "NO_SOURCE");
  restfreq = 0.0;
  freqres  = 0.0;
  sideband =   9;
  whichIF  =  -1;

  useSource   = 0;
  useFreq     = 0;
  useFreqRes  = 0;
  useSideBand = 0;
  useIF       = 0;

  while(1)
  {
    int option_index = 0;

    c = getopt_long(argc, argv, "", long_options, &option_index);

    if (c == -1)
    {
      break;
    }

    switch(c)
    {
      case 0:
	switch(option_index)
	{
	  case OPTION_SOURCE:
		log_msg("Setting sourcename to: %s", optarg);
		strcpy(source, optarg);
		useSource = 1;
		break;
	  case OPTION_FREQ:
		log_msg("Setting Rest Freq to: %s", optarg);
		restfreq = atof(optarg);
		useFreq = 1;
		break;
	  case OPTION_FREQRES:
		log_msg("Setting Resolution Freq to: %s", optarg);
		freqres = atof(optarg);
		useFreqRes = 1;
		break;
	  case OPTION_SIDEBAND:
		log_msg("Setting Sideband to: %s", optarg);
		sideband = atoi(optarg);
		useSideBand = 1;
		break;
	  case OPTION_IF:
		log_msg("Setting IF to: %s", optarg);
		whichIF = atoi(optarg);
		useIF = 1;
		break;
	  default:
		usage(argv[0]);
		break;
	}

        break;

      case '?':
        printf("CASE '?': Got '?'");
        break;

      default:
        printf("?? getopt returned character code 0%o ??\n", c);
    }
  }

  if(!useSource && !useFreq && !useFreqRes && !useSideBand && !useIF)
  {
    usage(argv[0]);
  }

  OG = gm_map("./OTF_GLOBAL", 0);

  found = 0;

  for(i=0;i<OG->nfiles;i++)
  {
    of = &OG->files[i];

    osr = &of->scans[0];
    for(j=0;j<of->nscans;j++,osr++)
    {
      if(useSource)
      {
        if(!matchSource(source, osr))
        {
	  continue;
        }
      }

      if(useFreq)
      {
        if(!matchFreq(restfreq, osr))
        {
	  continue;
        }
      }

      if(useFreqRes)
      {
        if(!matchFreqRes(freqres, osr))
        {
	  continue;
        }
      }

      if(useSideBand)
      {
        if(!matchSideBand(sideband, osr))
        {
	  continue;
        }
      }

      if(useIF)
      {
        if(!matchIF(whichIF, osr))
        {
	  continue;
        }
      }

      /* we made it this far; print */

      printf("Found Source: %s, Scan Number: %8.2f, File: %s\n", osr->source, osr->scanno, of->filename);

      found++;
    }
  }

  printf("Found %d Scans\n", found);

  return(0);
}

