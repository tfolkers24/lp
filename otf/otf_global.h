#ifndef OTF_GLOBAL_H
#define OTF_GLOBAL_H

#define MAX_OTF_SCANS 4096
#define MAX_OTF_FILES  512

struct OTF_SCAN_RECORD {
        char   source[16];

        int    col;
        int    row;
        int    noint;
        int    ignore;

        float scanno;
        float ut;
        float lst;
        float offscan;
        float gainscan;
        float freq;
        float sb;
        float vel;
        float ra;
        float dec;
        float az;
        float el;
        float tsys;
        float tcal;
        float freqres;
        float deltaxr;
        float deltayr;
};


struct OTF_FILE {
        char filename[256];
        char initials[8];

        int  nscans;
        int  version;

        struct OTF_SCAN_RECORD scans[MAX_OTF_SCANS];
};


struct OTF_GLOBAL {
        int initialized;
        int nfiles;

        struct OTF_FILE files[MAX_OTF_FILES];
};

#endif
