#ifndef CALCHECK_H
#define CALCHECK_H

#define CC_ZEROS  1
#define CC_CALIN  2
#define CC_CALOUT 3

struct CAL_SEND_STRUCT {
        int mode;
        int counts;
        int nchan;
        int fake;
        double channels[512];
};

//#ifdef DEFINE_CC_MODE_STRS
//char *cc_mode_strs[] = { "None ", "Zeros", "Vane ", " Sky " };
//#endif

#endif
