#ifndef _FV_PROTO
#define _FV_PROTO

/* block.c */
int block_for_event(int display);
int fvset_sock_callback(int (*call)(char *, int));
int fvset_timer(void (*call)(void), int sec, void *param);
int dq_tim(void (*call)(void));
int fvset_internal_timer(void (*call)(void), void *param);
int fvclear_internal_timer(void);
int fvadd_fdblock(int fd);
int fvclear_fdblock(int fd);
int fv_setmaxmsg(int d);

/* button.c */
struct BUTTON *fvcreate_button(struct FRAME *f, int x, int y, char *text, void (*callback)(void));
int fvdraw_button(struct BUTTON *b);
int fv_draw_shadow_circle(Display *dpy, Window win, GC gc, int x, int y, unsigned int w, unsigned int h, int tpix, int bpix, int fpix, int lt);
int press_button(XButtonEvent *e);
int release_button(XButtonEvent *e);
int releaseif_button(struct BUTTON *b);
int fvstickylist_button(struct BUTTON *b, struct LIST *l);
struct BUTTON *fvcreate_shape_button(struct FRAME *f, int x, int y, int w, int h, int justify, int shape, char *text, void (*callback)(void));
int fvset_button_help(struct BUTTON *b, char *text);
int fv_draw_shadow_box(Display *dpy, Window win, GC gc, int x, int y, unsigned int w, unsigned int h, int tpix, int bpix, int fpix, int lt);
int fvset_button_mask(struct BUTTON *b, int bg);
int fvcreate_done_but(struct FRAME *ff, int x, int y);

/* check.c */
struct CHECKBOX *fvcreate_checkbox(struct FRAME *f, int x, int y, char *text, void (*callback)(void));
int fvdraw_checkbox(struct CHECKBOX *c);
int press_checkbox(XButtonEvent *e, struct CHECKBOX *c);
int fvset_checkbox(struct CHECKBOX *c, int state);

/* clear_map.c */
int main(int argc, char *argv[]);

/* color.c */
unsigned long fv_getColor(struct FRAME *ff, char *color, int flag, int red, int green, int blue);
int fvfree_color(struct FRAME *ff, unsigned long *c, int n, int p);
int fv_set_color_set(void);
int fvinit_colors(struct FRAME *ff);
int fvfind_close_color(Display *dpy, Colormap cmap, XColor *exact_color, char *color);

/* fonts.c */
int fv_initFonts(struct FRAME *ff);
int fv_setCanvasFont(struct FRAME *ff, GC gc, int cW);

/* frame.c */
struct FRAME *fvcreate_frame(char *display_name, int x, int y, int w, int h, int argc, char *argv[]);
struct FRAME *fvcreate_subframe(struct FRAME *f, int x, int y, int w, int h, int map);
int fvdraw_frame(struct FRAME *f);
int fvdraw_label(struct LABEL *l);
int fvconfig_frame(struct FRAME *f, XEvent *e);
struct LABEL *fvcreate_label(struct FRAME *f, int x, int y, char *text, int isbold);
int fvclear_label(struct LABEL *l);
int fvset_label(struct LABEL *l, char *s);
int fvset_labelcallback(struct LABEL *l, void (*call)(void));
int fvset_colorcallback(struct FRAME *f, void (*call)(void));
struct GRAPHIC *fvcreate_graphic(struct FRAME *f, int x1, int y1, int x2, int y2, int pixel);
int fvdraw_stoplt(struct FRAME *f, struct LABEL *l);
int fvset_local_predraw(struct FRAME *f, int (*p)(void));
int fvset_local_redraw(struct FRAME *f, int (*p)(void));
int fvset_local_config(struct FRAME *f, int (*p)(void));
int fvset_local_map(struct FRAME *f, int (*p)(void));
int fvset_frame_label(struct FRAME *f, char *frame_name);
int fvset_frame_prop(struct FRAME *f, char *frame_name, char *icon_name);
int fvset_help_msg(struct FRAME *f, char *str, int clear);
int fvset_diag(struct FRAME *f, char *diag);
int fvframe_death(void);
int fvnew_color(struct FASTOBJ *f, int pix);
int fvset_sync(struct FRAME *f);
int fvset_frame_focus(struct FRAME *f, int (*v)(void));
int fvset_frame_button(struct FRAME *f, int (*v)(void));
int fvset_pix(struct FASTOBJ *f, int p);
int create_config_frame(struct FRAME *f);
int fvset_color_button(struct FRAME *f, int ix);
int create_print_frame(struct FRAME *f);
int e_printPrint(struct BUTTON *b);
int e_printDismiss(struct BUTTON *b);
int fv_print_frame(struct FRAME *f, struct PRINT_OPTIONS *po);
int framePrintPS(struct FRAME *f);
int e_printOptionsTog(struct TOGGLE *t);
int _checkSizeOfLabel(struct LABEL *l);
int _checkSizeOfText(struct TEXT *t);
int _checkSizeOfPlot(struct FRAME *ff);
int _checkSizeOfGauge(struct FRAME *ff);
int _checkSizeOfLines(struct FRAME *ff);
int _checkSizeOfTherm(struct FRAME *ff);
int fv_window_fit(struct FRAME *ff, int *w, int *h);
int fvget_printBackgroundTog(void);

/* gauge.c */
struct GAUGE *fvcreate_gauge(struct FRAME *f, int x, int y, int r, int ang, struct XTICK *tick, void (*callback)(void));
int fvdraw_gauges(struct FRAME *f);
int fvdraw_gauge(struct GAUGE *g);
int fvdraw_gset_point(struct GAUGE *g);
int fvdraw_gpointer(struct GAUGE *g);
int fvset_gauge(struct GAUGE *g, double value);
int fvset_gauge_startangle(struct GAUGE *g, int a);
int fvset_gauge_labels(struct GAUGE *g, int bool);

/* lib.c */
int fvmain_loop(void);
int fvdraw(Window w);
int fvconfig(XEvent *e);
struct FASTOBJ *find_dude(Window win);
int add_dude(struct FASTOBJ *dude);
int dude_stats(void);
int dude_walk(void (*goof(struct FASTOBJ *)));
int load_font(struct FRAME *f);
int fvset_id(struct FASTOBJ *a, int key, void *value);
void *fvget_id(struct FASTOBJ *a, int key);
int fvset_fds(fd_set *pset);
int fvset_map(struct FASTOBJ *b, int m);
int fvstartup_config(int key, void *v);
int rm_dude(struct FASTOBJ *dude);
int _fventerButton(struct BUTTON *b);
int _fvleaveButton(void);
int fv_Q_but_help(struct BUTTON *b);
int fvdraw_help_screen(Window w);
int but_help_callback(struct BUTTON *b);
int fvcreate_help_window(struct FRAME *f);

/* list.c */
struct LIST *fvcreate_list(struct FRAME *f, int x, int y, int w, int h, char **head, void (*callback)(void), int mapped);
int fvdraw_list(struct LIST *l);
int list_motion(XMotionEvent *ev, struct LIST *l);
int list_release(XEvent *ev, struct LIST *l);
int press_list(XEvent *ev, struct LIST *l);
int fvmap_list(struct LIST *l);
int fvraise_list(struct LIST *l);
int fvnew_list(struct LIST *l, char **name);
int fvset_list(struct LIST *l, int pos);

/* menu.c */
struct MENU *fvcreate_internal_menu(struct FRAME *f, char *label);
struct MENU *fvcreate_menu(struct FRAME *f, char *label);
struct FASTOBJ *include_menu(struct FRAME *f, struct MENU *m);
int fvdraw_menu(struct MENU *m);
int menu_up(XEvent *ev, struct MENU *m);
int menu_down(XEvent *ev, struct FRAME *f);
int menu_leave(XEvent *ev);
int menu_motion(XEvent *ev);
int fvadd_menulist(char *name, int type, void (*call)(void), int append);
int fvdestroy_menu(struct MENU *m);

/* options.c */
int fvset_prog_prefer(char *fg, char *bg, char *font, char *bfont, char *ffont, char *bffont, char *bfd, char *bfm, char *bfh, char *bfs, char *olwc);
int fv_pre_options(int x, int y, int w, int h, int argc, char *argv[]);
int fv_post_options(struct FRAME *f, int argc, char *argv[]);
int fvset_backing_store(struct FASTOBJ *o);

/* plot.c */
struct PLOT *fvcreate_plot(struct FRAME *f, int x, int y, int w, int h, double *xdata, double *ydata, int n, int auto_ticks);
int fvset_plot_labels(struct PLOT *p, char *title, char *xlabel, char *ylabel);
int fvadd_plot(struct PLOT *p, double *xdata, double *ydata, int n, int color);
int fvdraw_plot(struct FRAME *f, struct PLOT *p);
struct XTICK *tick_int(struct XTICK *t, double min, double max);
int fvset_plot_axis(int which, struct PLOT *p, struct XTICK *paxis);

/* scroll.c */
struct SCROLL *fvcreate_scrollreg(struct FRAME *f, int x, int y, int w, int h, int lines, int chrs, int (*callback)(void));
int fvdraw_scroll(struct SCROLL *s);
int scroll_motion(XEvent *ev, struct SCROLL *s);
int press_scroll(XEvent *ev, struct SCROLL *s);
int fvmap_scroll(struct SCROLL *s);
int fvset_scroll(struct SCROLL *s, int pos);
int fvaddline_scroll(struct SCROLL *s, char *line, int fg, int bg);
int fvclear_scroll(struct SCROLL *s);
int fvsetend_scroll(struct SCROLL *s);

/* speed.c */
struct SPEEDBAR *fvcreate_speed(struct FRAME *f, int x, int y, void (*callback)(void), struct MENU *menu, double *value, double range);
int fvdraw_speed(struct SPEEDBAR *b);
int press_speed(XButtonEvent *ev, struct SPEEDBAR *b);
int speed_release(XButtonEvent *ev, struct SPEEDBAR *b);
int fvspeed_menu(struct MENU *m);
int fvset_speed_value(struct SPEEDBAR *s, double v);
int fvset_speed_pointer(struct SPEEDBAR *s, double *p);
int fvset_speed_colors(struct SPEEDBAR *s, int t, int b);

/* text.c */
struct TEXT *fvcreate_text(struct FRAME *f, int x, int y, int w, int max, void (*callback)(void));
int fvdraw_text(struct TEXT *t);
int draw_focus(struct TEXT *t, int pixel);
int process_key(XKeyEvent *e);
int fvmove_focus(XButtonEvent *e, struct FRAME *f);
char *fvget_text(struct TEXT *t);
char fvset_text(struct TEXT *t, char *s, int ign);
int fvchange_focus(struct TEXT *t);

/* therm.c */
struct THERM *fvcreate_therm(struct FRAME *f, int x, int y, int w, int h, int mode, struct XTICK *tick, void (*callback)(void));
int fvdraw_thermos(struct FRAME *f);
int fvdraw_therm(struct THERM *t);
int fvset_therm(struct THERM *t, double value);

/* toggle.c */
struct TOGGLE *fvcreate_toggle(struct FRAME *f, int x, int y, char *text[], int textlen, void (*callback)(void), int isvert);
int fvdraw_toggle(struct TOGGLE *b);
int press_toggle(XButtonEvent *e, struct TOGGLE *b);
int fvset_toggle(struct TOGGLE *b, int which);

/* trig.c */
int deg_icos(int a);
int deg_isin(int a);

/* writePS.c */
int fv_writePS(Display *dpy, Drawable win, int scr, int x, int y, int dx, int dy, int land, int rev, char *label, char *outfile);

/* xargs.c */
int InitXOption(char *sbOpt, char **psb, char *sbDef, char *sbHelp, char *sbRm, char *sbName, char *sbClass);
Display *ParseXArgs(char *sbProgName, char *sbProgClass, int *pcsb, char *rgsb[]);
int PrintOptions(void);
 
#endif /* _FV_PROTO */
