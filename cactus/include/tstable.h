#include "irigSmt.h"
#include "site.h"
#include "receiver.h"
#include "ts_defines.h"
#include "time_obs.h"
#include "dome_struct.h"

/* This file has many definitions including numeric constants and the 
   TS_TABLE struct which describes the shared global memory area used by 
   various control programs to communicate.

   This file also has the latitude and longitude of the 12M observatory.
*/

struct TS_TABLE {
	int    new;        				/* set by tscom 1 for new command else 0 */
	int    type;       				/* see macros */
	int    offtab;     				/* Offset table in use */
	int    soutab;     				/* Source/ephemeris table in use */
	int    offlag;     				/* 0 -> ON, 1 -> OFF, -1 -> Absolute OFF */
	int    rotator_wd; 				/* rotator watchdog flag */
	int    rxbay;        				/* Receiver bay number */
	int    drive;        				/* Drive state mask: 0=OFF, 1=ON; bit 0 is AZ, 1 is EL */
	int    sync;
	int    noff[MAXOFFSET];       			/* Number of offsets in table */
	int    lastoff[MAXOFFSET];    			/* Last offset refd in table */
	int    newServo; 				/* Flag used to alert servo. that a move to a new obj. in on. */
	int    trk_sambit;
	int    tracking; 				/* 1= AZ tracking (bit 0), 2= EL tracking (bit 1). */
	int    azState, elState; 			/* Indicates when resetting encoder. */ 

	int    domeRecIndx;				/* Index into DOME_SERVO array; Receiver & Transmitter */
	int    domeTrxIndx;

	int    spare_ints[64];

	int    force_tol; 				/* If not 0, tol. bit set "off src" pending new coeff */
	int    userForceWrap;
	int    intstatus;   				/* Time between Status messages (ms) */
	int    spare_longs[64];

	double time;    				/* Time used by tracker (mjd) */
	double Lst;     				/* lst at mjd above time  (name changed from 'lst' 29apr02) */ 
	double azSun; 					/* last computed sun Azimuth in deg. */
	double elSun; 					/* last computed sun elev. */
	double xSun;  					/* Sun position as X Y Z unit vector. xSun, ySun, zSun MUST */
	double ySun;  					/* remain together and in this order! */
	double zSun;
	double raSun;
	double decSun;
	double latitudeShift;  				/* Polar wandering corr. In degrees. */
	double longitudeShift; 				/* Polar wandering corr. In degrees. */
	double pt_ref;    				/* Refraction coefficient */
	double CenW;        				/* The sin(2.0*az) terms. */ 
	double CenN;
	double w2lr;
	double n2lr;
	double w2fb;
	double n2fb;
	double sfcz;        				/* thermal effects. Usually 0.0 */
	double third_refract;
	double elMinLimitDeg; 				/* minimum allowed elev. limit in units of degrees.*/
	double focusCorrection;  			/* Reading of North/South focus position in microns. See traksub.c */
	double focusFactor[MAXRECEIVER]; 		/* converts focusCorrection to arcsec */

	double spare_doubles[64];

	float  refIndex; 				/* Computed by ptmodel() in traksub.c */
	float  sunDistance; 				/* Computed by checkSunProximity. */
	float  raoff[MAXOFFENTRY][MAXOFFSET];   	/* RA offset          */
	float  decoff[MAXOFFENTRY][MAXOFFSET];  	/* Dec offset         */
	float  azoff[MAXOFFENTRY][MAXOFFSET];   	/* Az offset          */
	float  aztype[MAXOFFENTRY][MAXOFFSET];  	/* Az offset type     */
	float  eloff[MAXOFFENTRY][MAXOFFSET];   	/* El offset          */
	float  rarate[MAXOFFENTRY][MAXOFFSET];  	/* RA rate            */
	float  decrate[MAXOFFENTRY][MAXOFFSET]; 	/* Dec rate           */
	float  azrate[MAXOFFENTRY][MAXOFFSET];  	/* Az rate            */
	float  elrate[MAXOFFENTRY][MAXOFFSET];  	/* El rate            */
	float  spare_floats[64];

	char   pt_model[8]; 				/* Pointing model name */
	char   spare_chars[256];

	struct DOME_SERVO dome_status[MAX_DOME_SERVO];	/* Telemetry from the dome */
	struct DOME_PID   dome_pid;			/* Dome control PID terms*/
	struct TIME_OBS time_obs;			/* Struct to hold various TIME specific variables */
	struct COEF_CMD newPoly; 			/* Contains the latest polynomial from tracker */
	struct TSCACTUS cactus;     			/* those vars sent to cactus by respond */
	struct PTCOEFF pt_coeff[MAXRECEIVER];
	struct SOURCE objects[MAXSOURCE];
	struct EPHENTRY eph[MAXEPHENTRY]; 		/* entries for all ephem sources  */
};
