struct OPTICAL_SEND_STRUCT {
	char   magic[16];
	char   source[32];
	double az;
	double el;
	double azcorr;
	double elcorr;
	double refract;
};
