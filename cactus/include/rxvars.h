
struct RXVARS {
  double restfreq; /* 2nd channel rx variables */
  double skyfreq;
  double harmonic;
  double sideband;
  double lk_if;
  double sig_fs; /* freq switching offsets */
  double ref_fs;
  double synfreq;
  double img_sig_gain;                  /* Image/signal SB ratio */
  double yfactor;
  double rejection;
};
