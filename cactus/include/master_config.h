#define MASTER_MAGIC_STR   "MASTER_CONFIG"

// Structure that defines the receiver, IF and spectrometer configuration
struct MASTER_CONFIG {
  char                          magic[MAGIC_SIZE];

  char                                rx_name[24];
  char                  line_name[N_IF_CHANS][24];
  char      ubackend[MAX_BACKENDS][N_IF_CHANS][8]; // Unipops backend name
  char     cbackend[MAX_BACKENDS][N_IF_CHANS][12]; // Class backend name

  char                           spare_chars[256];

   
  double                     restfreq[N_IF_CHANS];
  double                      skyfreq[N_IF_CHANS];
  double                    imagefreq[N_IF_CHANS];
  double                       IFfreq[N_IF_CHANS];
  double                       LOfreq[N_IF_CHANS];
  double                     velocity[N_IF_CHANS];

  double                 sideband_fac[N_IF_CHANS];

                          /* Beam squint offsets; could be RA/DEC or LAT/LONG */
  double                   lat_offset[N_IF_CHANS];
  double                  long_offset[N_IF_CHANS];

  double                     yfactors[N_IF_CHANS];

  double         offset[MAX_BACKENDS][N_IF_CHANS];
  double           refpt[MAX_BACKENDS][MAX_PARTS];
  double       refpt_vel[MAX_BACKENDS][MAX_PARTS];
  double  scan_fractions[MAX_BACKENDS][MAX_PARTS];

  int        config_indx[MAX_BACKENDS][MAX_PARTS];
  int                        sideband[N_IF_CHANS];

  time_t                               configTime; // Date of this config
  int                         pleaseLevelNextTime; // Level the IF next CAL
  int                                 noAutoLevel; // turn off auto-level

  int                    otf_nchans[MAX_BACKENDS];

  float                  rejections[MAX_BACKENDS];

  int                             spares_ints[54];

  struct SPEC_CONFIG        configs[MAX_BACKENDS];
};
