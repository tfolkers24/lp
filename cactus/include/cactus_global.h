/*

    Cactus File %W%
      SID %I%
      Date %G%

    This file contains the definition of global memory in CACTUS
    It is also used to create the gs_init.c file and there are
    certian directives embedded in comment lines that are important.
    e.g. 
      When a field is declared like

      char  obj_name_ref[24];    [* Reference name RAMBO LOG *]

      The comment following the name is important. The keywords RAMBO and LOG 
      are passed along to gs_init.c to indicate that RAMBO wants this variable.
      Also LOG incicates that the executive should mail a log message to MONITOR
      when this variable changes. The comment with directives must be contained
      only on the line of the declatation. When the comment is started after the
      declaration on the same line but the comment is extended beyond that line,
      the directives will be ignored.
*/

#include "header.h"
#include "tscactus.h"
#include "therm.h"
#include "alarm.h"
#include "totalpower.h"
#include "processTime.h"
#include "receiver_global.h"

#define MAX_OFF       2  /* Maximum number of OFF positions in an observation */
#define MAX_RECEIVER  5
#define MAX_FB_BANKS  2
#define MAX_HS_IFS    8
#define HC_NBIN       8
#define MAX_BEAMS     8
#define N_IF          2
#define N_SYN         2

#define CHARSTAR 1 /* follows ramexec */
#define INTEGER  2
#define DOUBLE   3
#define FLOAT    4

#define RAMBO 1
#define LOG   1

extern struct cactus_global *cpr;
extern int NGS;


/* CACTUS Global Section Definition */

struct cactus_global {

    /* formerly field structure */
    /* struct coords catpos_inp;  Catalog position as input */

    double epocra;       /* catpos_equ RAMBO */
    double epocdec;      /* RAMBO */
    float  epoch;        /* RAMBO */
    char   coordcd[24];  /* RAMBO */
    
    double gallong;    /* catpos_gal RAMBO */
    double gallat;     /* RAMBO */

    double otf_ra;          /* RAMBO */
    double otf_dec;         /* RAMBO */
    double cen_ra;          /* RAMBO */
    double cen_dec;         /* RAMBO */

    char  obj_name[24];        /* Source name  RAMBO */
    char  obj_name_ref[24];    /* Reference name. (used for APS) RAMBO */
    char  service_name[24];    /* Service name  RAMBO */

  /* struct coords catposref_inp; Catalog ref. position as input */

    double epocra_ref;  /* catposref_equ RAMBO */
    double epocdec_ref; /* RAMBO */
    float  epoc_ref;    /* RAMBO */

    double gallong_ref; /* catposref_gal RAMBO */
    double gallat_ref;  /* RAMBO */

    double cur_ra_ref;  /* curposref_equ RAMBO */
    double cur_dec_ref; /* RAMBO */

    double dop_ra;      /* used in doppler calculation RAMBO */
    double dop_dec;     /* RAMBO */
    double dop_ref;     /* RAMBO */

    int  cur_type_ref;  /* RAMBO */

  /* OFFLAG defines whether the following 
     offsets are applied to the object or 
     the reference position */

    float onpos_raoff;  /* onpos RAMBO */
    float onpos_decoff; /* RAMBO */
    float onpos_azoff;  /* RAMBO */
    int   onpos_aztyp;  /* RAMBO */
    float onpos_eloff;  /* RAMBO */

    float onpos_glatoff; /* RAMBO */
    float onpos_glngoff; /* RAMBO */

    float onpos_dradt;  /* RAMBO */
    float onpos_ddecdt;  /* RAMBO */
    float onpos_dazdt;  /* RAMBO */
    float onpos_deldt;  /* RAMBO */

    float offpos_raoff[MAX_OFF]; /* offpos RAMBO */
    float offpos_decoff[MAX_OFF];/* RAMBO */

    float offpos_glatoff[MAX_OFF]; /* RAMBO */
    float offpos_glngoff[MAX_OFF]; /* RAMBO */

    float offpos_azoff[MAX_OFF]; /* RAMBO */
    int   offpos_aztyp[MAX_OFF]; /* RAMBO */
    float offpos_eloff[MAX_OFF]; /* RAMBO */
    float corr_astep;            /* RAMBO */

    int    n_offpos;            /* Number of offpos offsets in use RAMBO */
    int    ts_src_local;        /* RAMBO */
    double fixedaz;             /* catalog screen RAMBO */
    double fixedel;             /* catalog screen RAMBO */
    int    ref_f;               /* catalog screen RAMBO */

   /* pt_model things in global mem */

    float pt_ref;               /* RAMBO */
    int   pt_rxbay;             /* RAMBO */
  
    float fit_az;    /* Azimuth from fit to 5-point RAMBO */
    float fit_el;    /* Elevation from fit to 5-point RAMBO */
    float center_az; /* Azimuth from eightbeam center to 5-point RAMBO */
    float center_el; /* Elevation from eightbeam center to 5-point RAMBO */
    float fit_ax0;   /* Fits to focalize RAMBO */
    float fit_ns0;   /* RAMBO */
    float fit_ew0;   /* RAMBO */
    float fit_tau0;  /* Fit to SPTIP RAMBO */

    int   veltype;     /* Velocity type RAMBO */
    int   velframe;    /* Velocity frame RAMBO */
    float vel_offset;  /* Velocity offset RAMBO */
    float vel_tot;     /* Object velocity + offset RAMBO */

    /* observation */

    char  obsid[24];    /* Observer name. RAMBO */
    char  oprid[24];    /* Operator name. RAMBO */

/* Backend(s) in use: [ any combination of MAC + DIGBE + FILTERS ] */
    int   backends;  /* RAMBO */

    double xzero;    /* X coordinate of grid point (0,0) RAMBO */
    double yzero;    /* Y coordinate of grid point (0,0) RAMBO */
    int   frame;    /* Coord. system frame code (c.f. struct coords) RAMBO */
    char  frame_ptr[24];  /* Coord. system frame name RAMBO */
    double scanang;  /* Position angle in "frame" of map Y axis (deg.) RAMBO */
    double deltax;   /* X spacing of grid points RAMBO */
    double deltay;   /* Y spacing of grid points RAMBO */
    double deltax_st;   /* X spacing of grid points for status RAMBO */
    double deltay_st;   /* Y spacing of grid points for status RAMBO */
    int   n_xpts;   /* Number of x points RAMBO */
    int   n_ypts;   /* Number of y points RAMBO */
    int   xcell0;  /* Starting x grid cell number RAMBO */
    int   ycell0;  /* Starting y grid cell number RAMBO */
    int   x_cell;   /* Current x grid cell number RAMBO */
    int   y_cell;   /* Current y grid cell number RAMBO */
    float disp_dx;  /* Status display delta x RAMBO */
    float disp_dy;  /* Status display delta y RAMBO */
    int   obs_nextseq;  /* Next obs_seqno */

    /* filterbank */

    int    obsmode_fb;       /* Observation mode  RAMBO */
    char   caltype_fb[24];       /* Calibration type VANE or NOCAL RAMBO */
    float  caltype_const[12];    /* tsys for nocal mode RAMBO */
    
    int    mask;        /* SAM bus bit mask for invalid data bit(s) RAMBO */

    float   t_integ_fb;  /* Observation (total) integration time RAMBO */
    int     n_sample_fb; /* Number of samples per integration desired RAMBO */
    float   t_sample_fb; /* Length of each sample (seconds).  RAMBO */

            /* Rate for subreflector nutation("INT5",
             * "INT2.5", "INT1.25", or "EXT" for 5 Hz,
             * 2.5 Hz, 1.25 Hz, or External */
    char    nutrate_fb[24]; /* RAMBO */
    char    fsrate_fb[24];   /* Ditto for frequency switch rate RAMBO */
    char    fb_config1[24];  /* Filterbank 1 configuration name (e.g. PAR) RAMBO */
    float   freqres_fb1;     /* Channel width (MHz) RAMBO */
    int     config_fb1;      /* Filter bank mode [ SERIES | PARALLEL ] RAMBO */
    int     fb1_tog_val;     /* toggle widget value RAMBO */
    int     firstchan_fb1;   /* Index (1-512) of first channel in FBMUX data RAMBO */
    int     lastchan_fb1;    /* Index (1-512) of last  channel in FBMUX data RAMBO */
    float   freqres_fb2;     /* Channel width (MHz) RAMBO */
    int     config_fb2;      /* Filter bank mode [ SERIES | PARALLEL ] RAMBO */
    int     fb2_tog_val;     /* toggle widget value RAMBO */
    int     firstchan_fb2;   /* Index (1-512) of first channel in FBMUX data RAMBO */
    int     lastchan_fb2;    /* Index (1-512) of last  channel in FBMUX data RAMBO */
    int     bad_chan_fb[512]; /* Non-zero if the channel is bad RAMBO */
    char    bad_str_fb[512];  /* List of bad channel numbers in ASCII RAMBO */
    int     badfirst_fb;      /* RAMBO */
    int     badlast_fb;       /* RAMBO */
    char    fb_config2[24];   /* Filterbank 2 config name (e.g. PAR) RAMBO */
    double  ifskyosc_fb;     /* sky offset freq ( normally 0 MHz) RAMBO */

    /* dbe */

    int     obsmode_dbe;        /* Observation mode (Position switch, etc.) RAMBO */
    char    caltype_dbe[24];    /* Calibration type - On Off Vane - RAMBO */
    float   bw_dbe;             /* Bandwidth of DBE observations (MHz) RAMBO */
    int     intime_dbe;         /* DBE integration time (units: 64 us). RAMBO */
    int     ndump_dbe;          /* Number of dumps (Data Bus transfers) to be done. RAMBO */
    int     nchan_dbe;          /* Number of receiver channels. RAMBO */
    int     nphase_dbe;         /* Number of phases (2, 4, 6, or 8). RAMBO */
    int     calphase_dbe;       /* Phases requiring noise tube on (mask). RAMBO */
    int     bltime_dbe;         /* Blanking time between phases (units: 4 us). RAMBO */

                                /* Time it takes for the subreflector to begin 
                                   to move once it is commanded (units .25 us) */
    int     predelay_dbe;       /* RAMBO */
    float   dbe_ut;             /* UT as reported by the DBE backend RAMBO */
    int     dbe_doy;            /* DOY as reported by the DBE backend RAMBO */
    float   fit_az1;            /* Azimuth from fit to 5-point RAMBO */
    float   fit_az2;            /* Azimuth from fit to 5-point RAMBO */
    float   fit_azavg;          /* Azimuth from fit to 5-point RAMBO */
    float   fit_el1;            /* Elevation from fit to 5-point RAMBO */
    float   fit_el2;            /* Elevation from fit to 5-point RAMBO */
    float   fit_elavg;          /* Elevation from fit to 5-point RAMBO */
    float   fit_ax01;           /* Fits to focalize RAMBO */
    float   fit_ax02;           /* Fits to focalize RAMBO */
    float   fit_ax0avg;         /* Fits to focalize RAMBO */
    float   fit_ns01;           /* RAMBO */
    float   fit_ns02;           /* RAMBO */
    float   fit_ns0avg;         /* RAMBO */
    float   fit_tau01;          /* Fit to SPTIP RAMBO */
    float   fit_tau02;          /* Fit to SPTIP RAMBO */
    float   fit_tau0avg;        /* Fit to SPTIP RAMBO */


    /* MAC */
    int     obsmode_hs;      /* Observation mode (Position switch, etc.) RAMBO */
    double  begin_hs;        /* Time the current mode was begun (Days since Jan 0, 1989) */

    char    mac_mode[24];    /* most string for the MAC RAMBO */ 
    int     nifs_mac;        /* MAC RAMBO */
    float   bandwidth_mac;   /* MAC RAMBO */
    int     chanmin_mac;     /* MAC RAMBO */
    int     chanmax_mac;     /* MAC RAMBO */
    int     chanlen_mac;     /* MAC RAMBO */
    double  if_off_hs[8];    /* mac 4 if mode offset RAMBO */
                             /* cenfreq depends on restfreq if_off sideband et al*/
    double  cenfreq[8];      /* center freqs for 4 and 8 if modes RAMBO */
    double  skycenfreq[8];   /* sky center freqs for 4 and 8 if modes RAMBO */
    double  bandfreq_hs[8];  /* hyspec offsets variable RAMBO */
    double  offsign_hs[8];   /* hyspec offsets variable RAMBO */
    int     sbline_hs[8];    /* hyspec offsets variable RAMBO */
    char    vvlec_hs[24];    /* Yes or No vanvleck corr RAMBO */
    char    weight_hs[24];   /* Uniform Hanning Hamming RAMBO */
    char    export_hs[24];   /* Raw Filter RAMBO */

    /* subreflector */

    int     nutating;      /* Subreflector nutation state RAMBO */
    int     mode_foc;      /* Focus mode RAMBO RAMBO */
    float   p_beam[2];     /* Plus beam Az*-El offsets RAMBO RAMBO */
    float   m_beam[2];     /* Minus beam Az*-El offsets RAMBO RAMBO */
    float   foc_cen_ax;    /* Focus central values RAMBO */
    float   foc_cen_ns;    /* RAMBO */
    float   foc_cen_ew;    /* RAMBO */

    /* receiver */

    char    rx_name[24];  /* Receiver name. RAMBO */
    int     rot_localpara; /* Is Rambo tracking paralatic angle RAMBO */
    int     frontend;     /* Front end code number RAMBO */
    int     n_beam;       /* Number of beams RAMBO */

  /* Info. for each beam */

    float   t_rx[MAX_BEAMS];       /* HOT-COLD Receiver temperature RAMBO */
    float   t_sys[MAX_BEAMS];      /* System temperature from CAL RAMBO */
    float   t_cal[MAX_BEAMS];      /* Calibration temperature param. RAMBO */
    float   tc_on[MAX_BEAMS];      /* cal temp when noise tube is on. RAMBO */
    float   tc_off[MAX_BEAMS];     /* cal temp when noise tube is off. RAMBO */
    float   tc_vane[MAX_BEAMS];    /* cal temp when noise tube is vane. RAMBO */
    float   tc_st[8];              /* tcs displayed on status RAMBO */

    float   beam_azoff[MAX_BEAMS]; /* Az/el offsets of feed from the on-axis RAMBO */
    float   beam_eloff[MAX_BEAMS]; /*  position with the rotator at zero degrees RAMBO */
    char    line_name[24];         /* Line name. RAMBO */
    double  restfreq[2];           /* Rest frequency (GHz). RAMBO LOG */
    double  skyfreq[2];            /* Observation frequency (GHz). RAMBO */
  
    float   lk_if[2];              /* formerly intfreq RAMBO */
    float   firstif;               /* RAMBO */

    double  synfreq[2*N_SYN];      /* Synthesizer frequencies (MHz). RAMBO */
    double  synfreq1[2*N_SYN];     /* Synthesizer freq second (MHz). RAMBO */
    double  synfreq_st[2*N_SYN];   /* Synthesizer freq(MHz). for status RAMBO */
    int     harmonic[N_SYN];       /* Synthesizer multipliers. RAMBO */
    int     harmonic1[N_SYN];      /* Synthesizer mult second. RAMBO */
    int     harmonic_st[N_SYN];    /* actual value from FMS RAMBO */

   /* Desired synthesizer frequency (used when 
    * choosing "harmonic").  Set to 0 if 
    * "harmonic"'s should be used as stored */
    float   syngoal;        /* RAMBO */ 
    int     lofact;         /* LO multiplier. RAMBO */
  
    float   freqoff[2];     /* Sig and Ref freq switch offsets (MHz). RAMBO */
    float   freqoff1[2];    /* for 2nd lo RAMBO */

    float   freqoff_sav[2]; /* Frequency switching offsets saved RAMBO */
    float   freqoff1_sav[2];/* for 2nd lo RAMBO */

    int     polariz;        /* Polarization indicator RAMBO */
    float   rx_orient;      /* Receiver box orientation (degrees) RAMBO */

    double  fluke0;         /* IF processor synthesizer frequencies RAMBO */
    double  fluke1;         /* IF processor synthesizer frequencies RAMBO */
    char    rx_info[24];    /* receiver information */
    int     cmd_vane;       /* vane command     RAMBO */
    int     act_vane;       /* vane actual      RAMBO */
    int     cmd_csmir;      /* csmir command    RAMBO */
    int     act_csmir;      /* csmir actual     RAMBO */
    int     cmd_chopper;    /* chopper command  RAMBO */
    int     act_chopper;    /* chopper actual   RAMBO */

    /* rambo */

    float   lst_table[198]; /* Array of LST's for AZEL grid map RAMBO */
    int     lst_1st;        /* Index of first LST RAMBO */
    int     lst_next;       /* Index of next LST RAMBO */
    char    obs_name[24];   /* Observer name RAMBO */
    char    project[24];    /* Project name RAMBO */
    char    catalog[24];    /* Object catalog RAMBO */
    char    catalog_ref[24];/* reference catalog RAMBO */
    char    save_obj[24];   /* RAMBO */
    char    tmp_name[24];   /* RAMBO */
    char    tmp_cat[24];    /* RAMBO */
    double  tmp_ra;         /* RAMBO */
    double  tmp_dec;        /* RAMBO */
    double  tmp_vel;        /* RAMBO */
    int     tmp_vframe;     /* RAMBO */
    int     tmp_vtyp;       /* RAMBO */
    double  tmp_epoch;      /* RAMBO */
    char    tmp_obs[24];    /* RAMBO */
    char    tmp_oname[48];  /* RAMBO */
    char    tmp_proj[48];   /* RAMBO */
    char    tmp_dir[24];    /* RAMBO */

    /* spot */

    int     scans_cal;      /* Observations per cal RAMBO */
    int     repeats;        /* Number of repeats RAMBO */
    int     n_pair;         /* Number of ON-OFF pairs RAMBO */
    int     n_onoff;        /* Number of ONs + OFFs RAMBO */
    int     onsperoff;      /* Number of ONs per OFF RAMBO */
    float   samp_time;      /* Sample time (seconds) RAMBO */
    int     samp_count;     /* Sample time (0.1 sec units) RAMBO */
    int     xcenter;        /* Grid mapping center XCELL number RAMBO */
    int     ycenter;        /* Grid mapping center YCELL number RAMBO */
    int     n_center;       /* No. of observations per center observation RAMBO */
    int     center_entry;   /* Grid mapping center offset entry number RAMBO */
    int     center_table;   /* Grid mapping center offset table number RAMBO */

    float   onpos_raglob;   /* Mapping procedure global offsets for ON RAMBO */
    float   onpos_decglob;  /* RAMBO */
    float   onpos_azglob;   /* RAMBO */
    float   onpos_elglob;   /* RAMBO */
    float   offpos_raglob;  /* Mapping procedure global offsets for OFF RAMBO */
    float   offpos_decglob; /* RAMBO */
    float   offpos_azglob;  /* RAMBO */
    float   offpos_elglob;  /* RAMBO */
   
    int     arcunit;        /* RAMBO mapping modes */
    int     coselcor;       /* RAMBO mapping modes */
    char    coselind[24];   /* RAMBO mapping modes */
    int     onpos_offtab;   /* ON position offset table number RAMBO */
    int     onpos_offent;   /* ON position offset entry number RAMBO */
    int     offpos_offtab;  /* OFF position offset table number RAMBO */
    int     offpos_offent1; /* OFF position 1 offset entry number RAMBO */
    int     offpos_offent2; /* OFF position 2 offset entry number RAMBO */
    int     busy;           /* when true exec is active RAMBO */
    int     hyspec_f;       /* when true hyspec is turned on RAMBO */
    int     samples;        /* used by status for screen by executive RAMBO */
    float   sec;            /* used by status for screen by executive RAMBO */
    float   time;           /* used by status for screen by executive RAMBO */
    int     rsamples;       /* sent to montior for real-time display RAMBO */
    float   rsec;           /* from backend RAMBO                          */
    float   rtime;          /* RAMBO */

    char  obs_mode[24] ;    /* Name of observation in progress  RAMBO */
    char  scans[24];        /* Number of scans on status or AUTO RAMBO   */

/* PS */

    float secs_ps; /* RAMBO */
    int pairs_ps;  /* RAMBO */
    int scans_ps;  /* RAMBO */
    int spc_ps;    /* RAMBO */
    float cal_ps;  /* RAMBO */

/* tp */

    float secs_tp; /* RAMBO */
    int scans_tp;  /* RAMBO */

/* fs */

    float sig_fs[2]; /* RAMBO */
    float ref_fs[2]; /* RAMBO */
    float rate_fs;   /* RAMBO */
    float secs_fs;   /* RAMBO */
    int scans_fs;    /* RAMBO */
    int spc_fs;      /* RAMBO */
    int switch_fs;   /* RAMBO */
    int sw_pair_fs;  /* RAMBO */
    float focsize_fs;  /* RAMBO */
  
/* bsp */

    float secs_bsp; /* RAMBO */
    int sets_bsp;   /* RAMBO */
    int scans_bsp;  /* RAMBO */
    int spc_bsp;    /* RAMBO */
    float nutrate_bsp; /* RAMBO */
    int ignore_off_bsp; /* RAMBO */

/* continuum sequence */

    float secs_seq; /* RAMBO */
    int sets_seq;   /* RAMBO */
    int spc_seq;    /* RAMBO */
    int scans_seq;  /* RAMBO */
    int ignore_off_seq; /* RAMBO */
    float cal_seq;  /* RAMBO */
  
/* Continuum Five point */

    int ignore_off_five; /* RAMBO */
    float secs_five;     /* RAMBO */
    int pairs_five;      /* RAMBO */
    float grid_five;     /* RAMBO */
    float one_scan_five; /* RAMBO */
    float cal_five;      /* RAMBO */
    int five_f;          /* RAMBO used to load five point offsets */
    int eight_five;      /* RAMBO used for eight beam */
    int ptchan_five;     /* RAMBO used for eight beam, also on focus */
  
/* focus */

    int ignore_off_foc;  /* RAMBO */
    int settings_foc;    /* RAMBO */
    float secs_foc;      /* RAMBO */
    float cal_foc;       /* RAMBO */
    float astep_foc;     /* RAMBO */

/* skytip */

    float azimuth_sky;      /* RAMBO */
    float secs_sky;         /* RAMBO */
    float tipper_rh;        /* RAMBO */

    int tipper_force_flag;  /* RAMBO */
    int last_tip_time;      /* RAMBO */
    int using_tipper_rh;    /* RAMBO */

/* N-S focalize */

    float secs_ns;       /* RAMBO */
    int settings_ns;     /* RAMBO */
    float zero_ns;       /* RAMBO */
    float step_ns;       /* RAMBO */
    float tol_ns;        /* RAMBO */
    int mode_ns;         /* RAMBO */

/* E-W focalize */

    float secs_ew;       /* RAMBO */
    int settings_ew;     /* RAMBO */
    float zero_ew;       /* RAMBO */
    float step_ew;       /* RAMBO */

/* rectangular grid mapping run variables */

    char maptyp_wgrid[24];/* RAMBO */
    int frame_wgrid;      /* RAMBO */
    double deltax_wgrid;  /* RAMBO */
    double deltay_wgrid;  /* RAMBO */
    double secs_wgrid;    /* RAMBO */
    int pairs_wgrid;      /* RAMBO */
    int spc_wgrid;        /* RAMBO */
    int spcenter_wgrid;   /* RAMBO */
    double onsecs_wgrid;  /* RAMBO */
    double offsecs_wgrid; /* RAMBO */
    int onpoff_wgrid;     /* RAMBO */
    int offpc_wgrid;      /* RAMBO */
    int onpcenter_wgrid;  /* RAMBO */
    int rows_wgrid;       /* RAMBO */
    int cols_wgrid;       /* RAMBO */
    int srow_wgrid;       /* RAMBO */
    int scol_wgrid;       /* RAMBO */
    int erow_wgrid;       /* RAMBO */
    int ecol_wgrid;       /* RAMBO */
    int strow_wgrid;      /* RAMBO */
    int stcol_wgrid;      /* RAMBO */
    double angle_wgrid ;  /* RAMBO */

/* setup variables */

    char maptyp_grid[24];/* RAMBO */
    int frame_grid;      /* RAMBO */
    double deltax_grid;  /* RAMBO */
    double deltay_grid;  /* RAMBO */
    double secs_grid;    /* RAMBO */
    int pairs_grid;      /* RAMBO */
    int spc_grid;        /* RAMBO */
    int spcenter_grid;   /* RAMBO */
    double onsecs_grid;  /* RAMBO */
    double offsecs_grid; /* RAMBO */
    int onpoff_grid;     /* RAMBO */
    int offpc_grid;      /* RAMBO */
    int onpcenter_grid;  /* RAMBO */
    int rows_grid;       /* RAMBO */
    int cols_grid;       /* RAMBO */
    int srow_grid;       /* RAMBO */
    int scol_grid;       /* RAMBO */
    int erow_grid;       /* RAMBO */
    int ecol_grid;       /* RAMBO */
    int strow_grid;      /* RAMBO */
    int stcol_grid;      /* RAMBO */
    double angle_grid ;  /* RAMBO */

/* Spectral Five Point */

    float secs_sfive;    /* RAMBO */
    int pairs_sfive;     /* RAMBO */
    int spc_sfive;       /* RAMBO */
    float grid_sfive;    /* RAMBO */
    double onsecs_sfive; /* RAMBO */
    double offsecs_sfive;/* RAMBO */
    
/* Catalog Mapping */

    float secs_catm;     /* RAMBO */
    int pairs_catm;      /* RAMBO */
    int scans_catm;      /* RAMBO */
    int spc_catm;        /* RAMBO */
   
/* Az-El Grid Mapping */

  int mode_azel;         /* RAMBO */
  float secs_azel;       /* RAMBO */
  int spc_azel;          /* RAMBO */
  int rows_azel;         /* RAMBO */
  int cols_azel;         /* RAMBO */
  int srow_azel;         /* RAMBO */
  int scol_azel;         /* RAMBO */
  int erow_azel;         /* RAMBO */
  int ecol_azel;         /* RAMBO */
  float cal_azel;        /* RAMBO */
  char scandir_azel[24]; /* RAMBO */
  int center_azel;       /* RAMBO */
  int nplz_azel;         /* RAMBO */
  
/* Raster Mapping */

  char scandir_otf[24];  /* RAMBO */
  float rampup_otf;      /* RAMBO */
  float rate_otf;        /* RAMBO */
  float dist_otf;        /* RAMBO */
  int rows_otf;          /* RAMBO */
  int srow_otf;          /* RAMBO */
  int erow_otf;          /* RAMBO */
  int center_otf;        /* RAMBO */
  int five_otf;          /* RAMBO raster rows per 5 point */ 
  int rowspc_otf;        /* RAMBO */
  float cal_otf;         /* RAMBO */
  double xsize_otf;      /* RAMBO */
  double ysize_otf;      /* RAMBO */
  double rot8_otf;       /* RAMBO */
  double deltay_otf;     /* RAMBO */
  int    n8_otf;         /* RAMBO */
  int    nfoot8_otf;     /* RAMBO */
  int    srow8_otf;      /* RAMBO */
  int    erow8_otf;      /* RAMBO */
  int    end_off_otf;    /* Non-0 if will end OTF with a final OFF RAMBO */
  float  xmap_otf;       /* Row X offset for log RAMBO */
  float  ymap_otf;       /* Row Y offset for log RAMBO */


  
/* for rx tuning via vxworks */

  int lock_alarm_1;      /* RAMBO */
  int lock_alarm_2;      /* RAMBO */

/* for contin polarizer mode */

  double secs_plz;       /* RAMBO */
  int n_plz;            /* RAMBO */
  int sets_plz;         /* RAMBO */
  int scans_plz;        /* RAMBO */
  int spc_plz;          /* RAMBO */
  double angle_cmd_plz;    /* polarizer commanded angle RAMBO */
  double angle_act_plz;    /* polarizer actual angle RAMBO */
  double angle_offset_plz; /* user selected polarizer angle RAMBO */
  double angle_index_plz;  /* hardware zero angle for veritcal indexing RAMBO */
  double secs_cplz;        /* RAMBO */

  int n_cplz;           /* RAMBO */
  int sets_cplz;        /* RAMBO */
  int scans_cplz;       /* RAMBO */
  int spc_cplz;         /* RAMBO */

/* spectral line polarizer */

  double secs_splz;      /* RAMBO */
  int  n_splz;           /* RAMBO */
  int sets_splz;         /* RAMBO */
  int scans_splz;        /* RAMBO */
  int spc_splz;          /* RAMBO */
  char nt_splz[24];      /* RAMBO */

/* otf modes */

  char offtype_fbotf[24]; /* RAMBO */
  double offsecs_fbotf;   /* RAMBO */
  double calsecs_fbotf;   /* RAMBO */
  int rowpoff_fbotf;      /* RAMBO */
  int offpc_fbotf;        /* RAMBO */
  int rows_fbotf;         /* RAMBO */
  int srow_fbotf;         /* RAMBO */
  int erow_fbotf;         /* RAMBO */
  char scandir_fbotf[24]; /* RAMBO */
  double dist_fbotf;      /* RAMBO */
  double rate_fbotf;      /* RAMBO */
  double rampup_fbotf;    /* RAMBO */
  char center_fbotf[24];  /* RAMBO */
  double xsize_fbotf;     /* RAMBO */
  double ysize_fbotf;     /* RAMBO */
  double rot8_fbotf;      /* RAMBO */
  double deltay_fbotf;    /* RAMBO */
  double angle_fbotf;     /* RAMBO */
  int n8_fbotf;           /* RAMBO */
  int nfoot8_fbotf;       /* RAMBO */
  int srow8_fbotf;        /* RAMBO */
  int erow8_fbotf;        /* RAMBO */
  int databin_fbotf;      /* RAMBO */
  int datachan_fbotf;     /* RAMBO */
  int datasarr_fbotf;     /* RAMBO */

  int    harm_sbc;        /* RAMBO */
  int    simage_sbc;      /* RAMBO */
  int    chan_sbc;        /* RAMBO */
  double ffluke_sbc;      /* RAMBO */

/* VLBI */

  float tm_vlbi;          /* RAMBO */
  float dur_vlbi;         /* RAMBO */

/* Hyspec IF oscillator limits; beyond these, phase lock is lost */

  double if1_lo;  /* RAMBO */
  double if1_hi;  /* RAMBO */
  double if2_lo;  /* RAMBO */
  double if2_hi;  /* RAMBO */

/* optical pointing */

  int  secs_opt;

  /* end of former field structure */

  /* misc */

    int     obseq;          /* Observation number (scan number) RAMBO */
    int     sdd_version;    /* sdd version number RAMBO */
    int     running_aro;    /* Non-zero when AROWS integrating RAMBO */
    int     running_dbe;    /* Non-zero when DBE integrating RAMBO */

                            /* Frequency counter values. */
    double  twoghz[N_SYN];  /* 1.9 Ghz frequency (20 x SYNFREQ) RAMBO */
    double  sigosc;         /* Phase lock signal frequency RAMBO */
    double  refosc;         /* Phase lock reference frequency RAMBO */
    double  vlbi_sky_freq;  /* RAMBO */

    float   point_az;      /* Pointing correction in azimuth (deg.) RAMBO */
    float   point_el;      /* Pointing correction in elevation (deg.) RAMBO */

    char    sideband_name[24];  /* Receiver sideband ("USB" or "LSB") RAMBO */
    char    sideband_name2[24]; /* second channel ("USB" or "LSB")    RAMBO */

    double  epocra_st;     /* Display slots for epoch, current, and RAMBO */
    double  epocdec_st;    /* galactic coordinate RAMBO */
    double  gallong_st;    /* RAMBO */
    double  gallat_st;     /* RAMBO */
    double  cur_ra_st;     /* RAMBO */
    double  cur_dec_st;    /* RAMBO */
    char    epoch_str[24]; /* epoch string for status RAMBO */

    char  vframe_str[24];  /* String pointer for velocity frame RAMBO */
    char  vtype_str[24];   /* String pointer for velocity type RAMBO */
    char  warn_msg[128];   /* Warning message pointer */
    int   end_of_scan;       /* set to one at end of scan */
    int   ptua_on;           /* true if power source is on */ 
    int   franklin_on;
    int   generator_on;
    int   current_disk;    /* RAMBO */

/* datetime */

    char   date_str[24];   /* Date in ASCII (e.g. 30 JUN 90) RAMBO */
    double ut_date;        /* year.mmdd format                  RAMBO */

/* weather */

    float  amb_temp;       /* Ambient temperature (deg C). RAMBO */
    float  amb_pres;       /* Barometric pressure (mm Hg). RAMBO */
    float  humidity;       /* Relative humidity (%). RAMBO */
    float  dew_pt;         /* Dew point (deg C) RAMBO */
    float  mm_h2o;         /* Water partial pressure (mm Hg) RAMBO */
    float  tau_0;          /* Zenith opacity. RAMBO */
    float  vane_temp;      /* Temperature of the vane */
    float  wx_temp;        /* Weather station temperature RAMBO */
    float  wx_torr;        /* Weather station pressure RAMBO */
    float  wx_rh;          /* Weather station relative humidity RAMBO */
    float  wx_wspeed;      /* Weather station wind speed RAMBO */
    float  wx_wdirect;     /* Weather station wind direction RAMBO */
    float  wx_refract;     /* Refraction calc. from weather station data RAMBO */
    float  wx_tauz;        /* Weather station zenith tau */
    float  wx_tau;         /* Weather station tau RAMBO */
    float  wx_tausigma;    /* Weather station tau sigma */
    float  wx_mmh2o;       /* Weather station mm H2O RAMBO */
    float  wx_dp;          /* Weather station dew point */
    float  wx_rain;        /* Weather station rain detection */
    float  wx_wdr2;        /* Weather station second wind dir */
    float  wx_wspd2;       /* Weather station second wind speed */

    double total_ra;       /* R.A. including all offsets RAMBO */
    double total_dec;      /* Dec. including all offsets RAMBO */
    int    refresh_stat;   /* If 1, STATUS will refresh its screen RAMBO */
    int    refresh_net;    /* If 1, NETSTATUS will refresh its screen RAMBO */
    int    stat_debug;     /* Non-0 to display debug info on status screen */

                           /* Radial velocity of antenna due to
                            *  earth's motions.  Velocity frame and 
                            *  definition same as for object */
    float  ant_vel;        /* RAMBO */
    float  quad_x;         /* Quadrant detector EW reading RAMBO */
    float  quad_y;         /* Quadrant detector NS reading RAMBO */
    int    quad_status;    /* 1 if laser on, 0 laser off */
    int    subpos;         /* Pointing model beam selection RAMBO */

/* telescope */

    float  beam_fwhm;      /* Beam size (arcsec) RAMBO */
    float  apereff;        /* Aperature efficiency RAMBO */
    float  beameff;        /* Beam efficiency RAMBO */
    float  antgain;        /* Antenna gain RAMBO */
    float  eta_l;          /* Rear spillover and scattering effcy. RAMBO */
    float  eta_fss;        /* Foward spillover and scattering effcy. RAMBO */
    float  plate_scale;    /* Arcsec per mm for focus translation RAMBO */
    float  ns_tilt;        /* north south tilt measurement RAMBO */ 
    float  ew_tilt;        /* east west tilt measurement RAMBO */ 
    float  tilt_temp;      /* tilt meter temperature RAMBO */ 

/* SPTIP */
    float  sptip_lower_elev;	/* RAMBO */
    float  sptip_upper_elev;	/* RAMBO */
    float  sptip_lower_airmass; /* RAMBO */
    float  sptip_upper_airmass; /* RAMBO */

/* cold load, values from the cold load monitor */

    float cl_cold0;
    float cl_cold1;
    float cl_amb0;
    float cl_amb1;
    float cl_press;

    int   processTimes[MAX_PROC_TIME];
    unsigned int alarm_hash;    /* computed by init_alarm used to zap structs */
    unsigned int exec_pid;                     /* executive pid used by rambo */

    char   disp_mac_mode[24];                   /* mac mode for display RAMBO */

    char   rcvr_sideband[8];             /* Actual receiver selected SB RAMBO */
    char   spare_chars[480];

    int    widebandmode;             /* Special 1.2 GHz mac mode active RAMBO */
    int    harm_want;                    /* RAMBO which harmonic is requested */
    int    srr_test;                            /* Radar Tests RAMBO */
    int    coldcal;                             /* RAMBO */

    int    forceUserWrap;                       /* RAMBO */

    int    arows_enabled;			/* RAMBO */
    int    arows_mode;				/* RAMBO */

    int    auto_tilt;				/* RAMBO */

    int    arows_nchans;			/* RAMBO Number of channels for AROWS */
    int    arows_cf[8];				/* RAMBO AROWS center Freq in MKz */
    int    arows_hanning;			/* RAMBO AROWS Hanning smoothing enbled */

    int    auto_arows_level;			/* RAMBO if True, auto level the AROWS/DBE TP Box */

    int    time_map_coord;			/* RAMBO */
    int    time_start_telemetry;		/* RAMBO */
    int    time_start_tracking;			/* RAMBO */
    int    time_start_observing;		/* RAMBO */
    int    mac_use_other_sideband;              /* RAMBO */
    int    running_mac;          /* Non-zero when DBE integrating RAMBO */

    int    spare_ints[35]; /* was 43 */

    int    munch0_dumplags;			/* RAMBO */
    int    munch1_dumplags;			/* RAMBO */
    int    write_rx_params;
    int    monitor_process_time;

    float  yfactor1;				/* RAMBO */
    float  yfactor2;				/* RAMBO */
    float  rejection1;				/* RAMBO */
    float  rejection2;				/* RAMBO */

    float  rcvr_1st_t;				/* RAMBO */
    float  rcvr_2nd_t;				/* RAMBO */
    float  rcvr_jt_t;				/* RAMBO */
    float  rcvr_vac;				/* RAMBO */

    float  power_level_3mm;			/* RAMBO */

    float  firstif2;                            /* RAMBO */

    float  fit_ew01;                            /* RAMBO */
    float  fit_ew02;                            /* RAMBO */
    float  fit_ew0avg;                          /* RAMBO */

    float  time_map_size;			/* RAMBO */
    float  time_scan_time;			/* RAMBO */
    float  time_map_angle;			/* RAMBO */

    float  yfactor3;				/* RAMBO */
    float  yfactor4;				/* RAMBO */
    float  rejection3;				/* RAMBO */
    float  rejection4;				/* RAMBO */

    float  spare_floats[44]; /* was 51 */

    double mjd;					/* RAMBO */
    double foc_axcmd;				/* RAMBO */
    double foc_axind;				/* RAMBO */
    double foc_ax0;				/* RAMBO */

    double refpt_vel;				/* RAMBO */

    double arows_multi_freqs[8];		/* RAMBO */
    
    double spare_doubles[51];

    struct HEADER           hdr;                  /* current observation header */ 
    struct TSCACTUS         ts;                   /* copy of tracker global memory */
    struct THERMPROBE       thermist;             /* Thermistor readings */
    struct THERMPROBE       dish_temp;            /* backup structure and surface temp probes */
    struct TOTAL_POWER      tp;                   /* voltages from dbe RAMBO */
    struct ALARMGLOB        alarm[MAXALARMS];     /* alarm structures */
    struct RECEIVER_GLOBAL  last_rcvr_values[20]; /* last values for each receiver */
};
