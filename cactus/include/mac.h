/*
 * Cactus File %W%
 *         SID %I%
 *        Date %G%
 */

struct MACSTATE {
  char magic[8];    /* MAC */ 
  float macp5r;     /* positive 5 volts right */
  float macp5l;     /* positive 5 volts left */
  float macn5;      /* negative 5 volts */

  float macn2r;     /* negative 2 volts right */
  float macn2l;     /* positive 2 volts left */
  float mac24;      /* positive 24 volts */
  float mac12;      /* positive 12 volts */
  float mactemp;    /* mac temperature deg C */
  float macs5;      /* sys mon 5 volts */
  float macp15;     /* sys mon positive 15 volts */
  float macn15;     /* sys mon negative 15 volts */
};

#define BAUD 0
