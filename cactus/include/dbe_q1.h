#ifndef DBE_Q1_H
#define DBE_Q1_H

#define MAX_DBE_Q1_POINTS 1024

struct DBE_Q1 {
	char magic[16];
	int  cycles;
	int  datalen;

	float chan1[MAX_DBE_Q1_POINTS];
	float chan2[MAX_DBE_Q1_POINTS];
};

#endif
