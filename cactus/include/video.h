/*
 Cactus file @(#)video.h	1.4
        Date 9/10/92
*/

/* VIDEO.H -- Define certain video attributes for ANSI screen
 *
 * This module defines some ANSI terminal advanced video features
 * that can be used for highlighting on monochrome or color terminals.
 */

#define	BK_BLK	40	/* black */
#define	BK_RED	41	/* red */
#define	BK_GRN	42	/* green */
#define	BK_YEL	43	/* yellow */
#define	BK_BLU	44	/* blue */
#define	BK_MAG	45	/* magenta */
#define	BK_CYN	46	/* cyan */
#define	BK_WHT	47	/* white */
                 
#define	FG_BLK	30	/* black */
#define	FG_RED	31	/* red */
#define	FG_GRN	32	/* green */
#define	FG_YEL	33	/* yellow */
#define	FG_BLU	34	/* blue */
#define	FG_MAG	35	/* magenta */
#define	FG_CYN	36	/* cyan */
#define	FG_WHT	37	/* white */

char	*nrml_vid = "\033[m";
char	*bold_vid = "\033[1m";
char	*unsc_vid = "\033[4m";
char	*rvrs_vid = "\033[7m";
char	*fatal_vid = "\033[43;31;7m";		       /* red with black writing */
char	*error_vid = "\033[43;30;1m";		    /* yellow with black writing */
char	*warn_vid = "\033[42;30;1m";		    /* orange with black writing */

char	*wide_vid = "\033#6";	/* vt100 extension but we use it anyway */
char	*naro_vid = "\033#5";	/* vt100 extension */

#define WARN_CHAR '\001'
#define ERROR_CHAR '\002'
#define FATAL_CHAR '\003'
