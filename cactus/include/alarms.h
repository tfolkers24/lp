/*
 *  Cactus file @(#)alarms.h	1.7
 *        Date 9/15/94
 *
 */


/* ALARMS.H --  Defines for the operator alarm panel. */
#define SET			1
#define CLEAR			2
#define CLEAR_ALL		3

#define HORN_DELAY		15

#define	NULL_ALARM		0
#define	IF_OUT_LOCK		1
#define	RCVR_OUT_LOCK		2
#define	TORQUE_MOTOR		3
#define	FINAL_ELEV		4
#define	INCORRECT_FS_POS	5
#define	INCORRECT_VANE		6
#define	INCORRECT_SUB_POS	7
#define	INCORRECT_SUB_BEAM	8
#define	INCORRECT_FOCUS		9
#define	INCORRECT_RX_BAY	10
#define	FREQ_COUNTER		11
#define	FB_WAIT_V_DATA		12
#define	DBE_WAIT_V_DATA		13
#define	NOT_TAKING_DATA		14
#define	END_OF_CAL		15
#define	END_OF_PAIR		16
#define	END_OF_SCAN		17
#define	DATE_CHANGE		18
#define	RAIN_DETECTED		19
#define	HIGH_WIND		20
#define	HIGH_MOTOR_CURRENT	21
#define	TOO_CLOSE_2_SUN		22
#define BACKEND_UP		23
#define BACKEND_DOWN		24
#define MASER_OUT_LOCK		25
#define RCVR_WARM		26
#define HYSPEC_SYNTH_LOCK	27
#define BEARING_HEAT_DIFF	28


#define ALARM_BIT_MASK		1<<(IF_OUT_LOCK-1)		|	\
				1<<(RCVR_OUT_LOCK-1)		|	\
				1<<(TORQUE_MOTOR-1)		|	\
				1<<(FINAL_ELEV-1)		|	\
				1<<(INCORRECT_FS_POS-1)		|	\
				1<<(INCORRECT_VANE-1)		|	\
				1<<(INCORRECT_SUB_POS-1)	|	\
				1<<(INCORRECT_SUB_BEAM-1)	|	\
				1<<(INCORRECT_FOCUS-1)		|	\
				1<<(INCORRECT_RX_BAY-1)		|	\
				1<<(FREQ_COUNTER-1)		|	\
				1<<(FB_WAIT_V_DATA-1)		|	\
				1<<(DBE_WAIT_V_DATA-1)		|	\
				1<<(NOT_TAKING_DATA-1)		|	\
				1<<(END_OF_CAL-1)		|	\
				1<<(END_OF_PAIR-1)		|	\
				1<<(END_OF_SCAN-1)		|	\
				1<<(DATE_CHANGE-1)		|	\
 				1<<(RAIN_DETECTED-1)		|	\
 				1<<(HIGH_WIND-1)		|	\
 				1<<(HIGH_MOTOR_CURRENT-1)	|	\
				1<<(TOO_CLOSE_2_SUN-1)		|	\
				1<<(BACKEND_UP-1)		|	\
				1<<(BACKEND_DOWN-1)		|	\
				1<<(MASER_OUT_LOCK-1)		|	\
				1<<(RCVR_WARM-1)		|	\
				1<<(HYSPEC_SYNTH_LOCK-1)	|	\
				1<<(BEARING_HEAT_DIFF-1)

#define DEFAULT_BIT_MASK	1<<(END_OF_CAL-1)		|	\
				1<<(END_OF_PAIR-1)		|	\
				1<<(DATE_CHANGE-1)







struct ALARM {
	char *msg;
	int  set;
	int  ack;
	int  sev;
	int  mask;
	char *name;
	double time;
};


#ifdef ALLOC_ALARMS

char *alm = "busy busy",
     *eop = "touchtone.0 touchtone.0",
     *eos = "doorbell",
     *ups = "rooster",
     *dns = "splat arnold";

struct ALARM alarms[] = { 
 "",				    0, 0, 0,	    0, "NULL_ALARM",	    0.0,
 "I.F. Processer out of lock.",	    0, 0, MSG_ERR,  0, "IF_OUT_LOCK",	    0.0,
 "Receiver out of lock.",	    0, 0, MSG_ERR,  0, "RCVR_OUT_LOCK",	    0.0,
 "Torque motor temps too high.",    0, 0, MSG_WARN, 0, "TORQUE_MOTOR",	    0.0,
 "Final elevation limit.",	    0, 0, MSG_WARN, 0, "FINAL_ELEV",	    0.0,
 "Incorrect Freq. Switch Pos.",     0, 0, MSG_ERR,  0, "INCORRECT_FS_POS",  0.0,
 "Incorrect Vane-Sky Position.",    0, 0, MSG_ERR,  0, "INCORRECT_VANE",    0.0,
 "Improper Subreflector position.", 0, 0, MSG_ERR,  0, "INCORRECT_SUB_POS", 0.0,
 "Improper Subreflector BEAMS.",    0, 0, MSG_ERR,  0, "INCORRECT_SUB_BEAM",0.0,
 "Focus Possibly in Manual.",	    0, 0, MSG_WARN, 0, "INCORRECT_FOCUS",   0.0,
 "Incorrect Receiver Bay.",	    0, 0, MSG_ERR,  0, "INCORRECT_RX_BAY",  0.0,
 "Frequency Counter error.",	    0, 0, MSG_ERR,  0, "FREQ_COUNTER",      0.0,
 "FBMUX Waiting on Valid Data.",    0, 0, MSG_WARN, 0, "FB_WAIT_V_DATA",    0.0,
 "DBE Waiting on Valid Data.",	    0, 0, MSG_WARN, 0, "DBE_WAIT_V_DATA",   0.0,
 "Integration time NOT advancing.", 0, 0, MSG_WARN, 0, "NOT_TAKING_DATA",   0.0,
 "End of Calibration.",	            0, 0, MSG_INFO, 0, "END_OF_CAL", 	    0.0,
 "End of Pair.",		    0, 0, MSG_INFO, 0, "END_OF_PAIR",       0.0,
 "End of Scan.",		    0, 0, MSG_INFO, 0, "END_OF_SCAN",	    0.0,
 "Date is about to be changed.",    0, 0, MSG_INFO, 0, "DATE_CHANGE",	    0.0,
 "Rain Detector Activated.",	    0, 0, MSG_WARN, 0, "RAIN_DETECTED",	    0.0,
 "High Winds Detected.",	    0, 0, MSG_INFO, 0, "HIGH_WIND",	    0.0,
 "Torque Motor Currents Too High.", 0, 0, MSG_INFO, 0, "HIGH_MOTOR_CURRENT",0.0,
 "Tracking Too Close to Sun.",	    0, 0, MSG_INFO, 0, "TOO_CLOSE_2_SUN",   0.0,
 "VxWorks Backend Up.",		    0, 0, MSG_INFO, 0, "BACKEND_UP",	    0.0,
 "VxWorks Backend Going Down.",	    0, 0, MSG_INFO, 0, "BACKEND_DOWN",	    0.0,
 "Maser Out Of Lock.",		    0, 0, MSG_ERR,  0, "MASER_OUT_LOCK",    0.0,
 "Receiver Warming Up.",	    0, 0, MSG_ERR,  0, "RCVR_WARM",	    0.0,
 "HySpec Synth Out of Lock.",	    0, 0, MSG_ERR,  0, "HYSPEC_SYNTH_LOCK", 0.0,
 "Bearing Heater Diff Out of Tol.", 0, 0, MSG_ERR,  0, "BEARING_HEAT_DIFF", 0.0
};

#define MAXALARMS	( sizeof(alarms) / sizeof(struct ALARM) )

#endif
