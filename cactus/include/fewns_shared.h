
/* this structure is shared between apex computer and tracker */

/* this is the definition of the enum hostid */

#define APEX_FOC  1
#define APEX_EW   2
#define APEX_NS   3

/* flags also indicates this */
/* flags are a mask as defined by: */

#define FOCFLG_MAN  0x01
#define FOCFLG_BUSY 0x02
#define FOCFLG_HOME 0x04
#define FOCFLG_STAT 0x08

/* all values in network byte order - aka bigendian sun/moto */

struct FEWNS_SH {
   char magic[8]; /* always set to FEWNS\0 */
   int hostid;    /* this message is either from FOCUS EW or NS */
   int cmdPos;     
   int tech80Pos;
   int flags;  
   int potPos;
};


