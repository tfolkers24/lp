#include "site.h"

#define PI	M_PI

#define CTD	57.29577951
#define CTR	0.017453292


struct SOURCE_DATA {
  char
	name[256],
	catalog_filename[256],
	source[256],
	result_filename[256],
	rastr[80],
	decstr[80],
	epochstr[80],
	velstr[80],
	lsrstr[80],
	typestr[80],
	spec_type[80];

  double
	greg,
	ut,
	lst,
	ra,
	dec,
	mag,
	vel,
	hour_angle,
	az,
	el,
	coslat,
	sinlat,
	cosdec,
	sindec,

	para,
	pad;

  int
	plotMe,
	pointing,
	pixel_x,
	pixel_y,
	width,
	height,
	tn;

  struct tm *now;
};


struct BOX_DATA {
  int origin_x,
      origin_y,
      box_size,
      box_sqrd,
      step,
      gain,
      fgbcol,
      fgbrow,
      offset,
      offset_high,
      offset_low,
      color,
      frame_counts,
      x, y,
      num_of_peaks,
      where_peak,
      peak_x,
      peak_y,
      auto_fc,
      tol,
      auto_offset,
      sea_mag,
      sea_max,
      found_it,
      nofinds;

  double
      tolf,
      rms,
      raw_peak,
      sn,
      mazo,
      melo,
      platexx,
      plateyy;
  float
      gauss_x,
      gauss_y,
      gauss_xw,
      gauss_yw,
      gauss_xpeak,
      gauss_ypeak;
};



struct WEATHER {
  double
      refract_radio,
      refract_optic,
      refract_diff,
      temperature,
      rel_humitity,
      bar_press;
};




struct COEFF {
  char
     coeff_file[20],
     coeff_radio[4][80],
     coeff_optical[4][80],
     coeff_eight_eight[6][80];
  double
     optical0[5],
     radio0[5],
     block_880[5];
};
