
#define DBE_MAGIC_STR   "DBE_CONFIG"

// Structure that defines the receiver, IF and spectrometer configuration
struct DBE_CONFIG {
  char                     magic[24];

  char                   rx_name[24];
  char     line_name[N_IF_CHANS][24];
  char        backend[N_IF_CHANS][8]; // Unipops backend name
   
  double        restfreq[N_IF_CHANS];
  double         skyfreq[N_IF_CHANS];
  double       imagefreq[N_IF_CHANS];
  double          IFfreq[N_IF_CHANS];
  double          LOfreq[N_IF_CHANS];
  double        velocity[N_IF_CHANS];

  double    sideband_fac[N_IF_CHANS];

  double          offset[N_IF_CHANS];

  double      rejections[N_IF_CHANS];
  double        yfactors[N_IF_CHANS];

  int        config_indx[N_IF_CHANS];
  int           sideband[N_IF_CHANS];

  time_t                configTime; // Date of this config
};
