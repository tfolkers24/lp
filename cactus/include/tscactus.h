#ifndef TSTABLE_H
#define TSTABLE_H
/*
    tracker servo cactus
    This structure is filled in by various ts processes
    and sent to monitor as a status update once a second
    by respond.
*/

/* coordinate type for cur_type */

#define STRADEC      1
#define STAZEL       2
#define NOPOINTAZEL  3
#define EPRADEC      4
#define EPAZEL       5

#define NUM_OF_TRACKER_DOUBLES 89
#define NUM_OF_TRACKER_INTS    11

struct TSCACTUS {
  char magic[24];
                    /* respond */
  double ut;
  double azcmd;
  double elcmd;
  double azact;
  double elact;
  double azerr;
  double elerr;
  double toterr;

  double lst;         /* Local mean sidereal time (seconds). */
  double obj_vel;
  double comp_ref;    /* formerly refrac */
  double cur_ra;
  double cur_dec;
  double cur_hpar;
  double azcorr;
  double elcorr;

    /* Manual azimuth and elevation pointing corrections to be added to 
       the standard pointing model.  (Supplied by user) */

  double uazcorr; /* formerly user_daz */
  double uelcorr; /* formerly user_del */
  double azbeam;
  double elbeam;


  double raoff;
  double decoff;
  double azoff;
  double eloff;
  double dradt;
  double ddecdt;
  double dazdt;
  double deldt;

  double t_antsamp;
  double ant_tol;
  double t_ts_status;
 
  double dut1;
  double d_at;
  double d_app;
  double t_horizon;
  double air_mass;  /* 1/sin(elcmd) */

  double foc_axcmd;
  double foc_axind;
  double foc_ax0;
  double foc_ax1;
  double foc_axtol;
  double foc_astep;
  double foc_axoff;

  double foc_nscmd;
  double foc_nsind;
  double foc_ns0;
  double foc_ns1;
  double foc_nstol;
  double foc_nstep;
  double foc_nsoff;

  double foc_ewcmd;
  double foc_ewind; 
  double foc_ew0;
  double foc_ew1;
  double foc_ewtol;
  double foc_estep;
  double foc_ewoff;

/* motor currents and temps */

  double az_current;
  double el_current;
  double ins_az_current;
  double ins_el_current;
  double peak_az_current;
  double peak_el_current;
  double az_temp;
  double ele_temp;
  double elw_temp;
  double nutator_temp;
  double subref_pbeam;
  double subref_mbeam;

    /* Stealing these old rotator slots */

  double azcmdvel;
  double elcmdvel;
  double rot_angle;   /* last angle sent to rotator              */
  double rot_act;     /* last reply from rotator                 */
  double eightaz[8];  /* loaded from pointing.model              */
  double eightel[8];  /* loaded from pointing.model              */

/* ints at end, sparc/68030 alignment must match */

  int    rot_ispara;  /* true if tracker should track paralactic */
  int    cur_type;
  int    offlag;
  int    aztyp;
  int    off_table;
  int    off_entry;
  int    az_drv_stat;
  int    el_drv_stat;
  int    year;
  int    doy;
  int    trackint;
  char   track_name[24];
};
#endif
