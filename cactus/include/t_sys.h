/*
 *  Cactus file @(#)t_sys.h	1.2
 *        Date 07/19/04
 *
 */

#ifndef T_SYS_H
#define T_SYS_H

struct RX_T_SYS {
	char  magic[16]; /* "RX_T_SYS" */
	int   mask;      /* which ones are set */
	float t_cal[8];  /* actual t_cal's */
	float t_sys[8];  /* actual t_sys's */
	float t_rcv[8];  /* actual t_rcv's */
};

#endif
