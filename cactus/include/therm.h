/*
 * Cactus File %W%
 *         SID %I%
 *        Date %G%
 */

#define MAX_THERM 64

struct THERMPROBE {
  char   magic[16];
  float  temps[MAX_THERM];
};
