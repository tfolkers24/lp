#define MAX_PROCS	20

#define CORONA_ID 	0
#define LOCURA_ID 	1
#define BOHEMIA_ID 	2
#define BIGMAC0_ID 	3
#define BIGMAC1_ID 	4
#define MAX_P_HOSTS 	5

char *hostnames[] = { "corona", "locura", "bohemia", "bigmac0", "bigmac1", NULL
};

	/* struct for communitcation between machines and clearinghouse */

struct PROCD_STRUCT {
	int   pid;
	char  user[16];
	char  tid[80];
};

struct SOCK_PROCD {
	char   magic[16];
	int    indx;
	int    hostid;
	int    nprocs;
	struct PROCD_STRUCT procs[MAX_PROCS];
};

	/* struct for communitcation between clearinghouse and programs */

struct PROC_ELEMENT {
	time_t t;
	int  hostid;
	int  pid;
	int  type;
	char user[16];
	char dpy[64];
	char name[32];
};


struct PROC_PKG {
	char magic[16];
	int  n;
	struct PROC_ELEMENT elem[MAX_PROCS];
};
