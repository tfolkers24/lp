#define TPRECORD         1
#define READSPERMIN     15
#define MAX_TP_CHANS     8
#define MAX_TP_CHANS_ACT 4


struct TOTAL_POWER {
	char magic[16];					      /* "TOTALPOWER" */
	double chan_tp[MAX_TP_CHANS];
	double chan_sp[MAX_TP_CHANS];
};
