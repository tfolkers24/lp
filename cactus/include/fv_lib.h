/*
 * Cactus File %W%
 *         SID %I%
 *        Date %G%
 */
/* for type element in structures */

#define  x_frame    1
#define  x_scroll   2
#define  x_button   3
#define  x_list     4
#define  x_subframe 6
#define  x_menu     7
#define  x_toggle   8
#define  x_speed    9
#define  x_text    10
#define  x_label   11
#define  x_therm   12
#define  x_gauge   13
#define  x_checkbox 14

#define menu_pullright 1
#define menu_callback  2

#define SCROLLW 16
#define SCROLL_3D_OFFSET 3

#define cfg_fontname          1
#define cfg_boldname          2
#define cfg_fontfixed         3
#define cfg_boldfixed         4
#define cfg_clrbackground     5
#define cfg_clrforeground     6
#define cfg_clrmedium         7
#define cfg_clrdark           8
#define cfg_clrborderlite     9
#define cfg_clrbordershadow  10

#define fv_color_set_dkgray   1
#define fv_color_set_gray     2
#define fv_color_set_ltgray   3
#define fv_color_set_dkblue   4
#define fv_color_set_blue     5
#define fv_color_set_ltblue   6
#define fv_color_set_dkyellow 7
#define fv_color_set_yellow   8
#define fv_color_set_ltyellow 9

struct FASTOBJ {
  struct FASTOBJ *next;
  void *user;
  struct IDLIST *idlist;  
  Window win;
  struct FASTOBJ *hash;
  int map;
  int type;             
  int pix;              /* always these first 8 elements */
};

struct FRAME
{
  struct FRAME *next;
  void *user;
  struct IDLIST *idlist;  
  Window win;
  struct FASTOBJ *hash;
  int map;
  int type;
  int pix;             /* always these first 8 elements */
  struct MENU *menu;   /* don't move this MENU struct */

  Display *display;
  XFontStruct *font_info;
  XFontStruct *bold_info;
  XFontStruct *font_fixed_info;
  XFontStruct *bold_fixed_info;
  int screen_num;
  int x, y;                      /* window position */
  int width, height;             /* window size */
  unsigned int border_width;
  unsigned int display_width, display_height;
  unsigned int icon_width, icon_height;
  char window_name[80];
  char icon_name[80];
  char diag[128];
  Pixmap icon_pixmap;
  XSizeHints size_hints;
  XClassHint class_hints;
  XIconSize *size_list;
  Colormap def_cmap;
  int foreground;
  int background;
  int darkpix;
  int medpix;
  int greenpix;
  int redpix;
  int hi_lite_pix;
  int shadow_pix;
  int scroll_pix;
  int last_key_pressed;
  GC gc;
  GC bold_gc;
  GC fixed_gc;
  GC bold_fixed_gc;
  GC button_gc;
  GC focus_gc;
  Pixmap but_pix;
  Pixmap foc_pix;
  struct FRAME    *frame2bPrinted;
  struct BUTTON   *callback_button;	     /* object that caused a callback */
  struct LABEL    *labelist;
  struct TEXT     *texthead;
  struct TEXT     *texttail;
  struct TEXT     *focus;
  struct FRAME    *subframe; 
  struct GRAPHIC  *lines;
  struct THERM    *thermos;
  struct GAUGE    *gauges;
  struct PLOT     *plot;
  struct BUTTON   *last_down;			 /* saved for up stroke paint */
  struct MENU	  *last_menu;
  int tm;

  int (*local_predraw)();
  int (*local_redraw)();
  int (*local_config)();
  int (*local_map)();
  int (*focus_call)();   /* focus change callback */
  int (*button_click)(); /* generic button click callback */
  void (*local_color_call)(); /* if user changes colors then call this */
};
 

Pixmap button_stip;

struct IDLIST {
  struct IDLIST *next;
  int key;
  void *value;
}; 

#define but_norm     	1  /* button state */
#define but_pressed  	2
#define CENTER_JUSTIFY	1
#define RIGHT_JUSTIFY	2
#define LEFT_JUSTIFY	3

#define BUT_NORMAL	1
#define BUT_RECTANGLE	2
#define BUT_ROUND	3
#define BUT_R_ARROW	4
#define BUT_L_ARROW	5
#define BUT_U_ARROW	6
#define BUT_D_ARROW	7
#define BUT_RADIO  	8

#define STICKY_BUT_1	1
#define STICKY_BUT_2	2
#define STICKY_BUT_3	3
#define ANY_STICKY_BUT	4

struct BUTTON 
{
  struct BUTTON *next;
  void *user;
  struct IDLIST *idlist;  
  Window win;
  struct FASTOBJ *hash;
  int map;
  int type;
  int pix;             /* always these first 8 elements */

  struct FRAME *frame;
  struct LIST *sticky;
  int sticky_but;
  int state;     
  int radio_state;
  int shape;
  int but_face_pix;
  int text_justify;
  char *text;
  char *help_text;
  int x, y, w, h, ascent;
  XImage *ximage;
  XImage *mask;
  void (*callback)();
};


struct SBLIST {
  struct SBLIST *next;
  int fg, bg;
  char *line; 
};

struct SCROLL 
{
  struct SCROLL *next;
  void *user;
  struct IDLIST *idlist;  
  Window win;
  Display *display;
  int map;
  int type;
  int pix;             /* always these first 8 elements */

  struct FRAME *frame;
  int x, y, w, h, ascent;
  int bg;
  int start, len; /* which line is shown at top */
  int fh;         /* font height */
  int lines;      /* num lines to save */
  int chrs;       /* num of chars in a line */
  int linecount;  /* how many now */
  Pixmap pixmap;  /* for clean scrolling */
  struct SBLIST *head, *tail, *cur;
  int (*call)();
};

struct TOGGLE 
{
  struct FRAME *frame;
  void *user;
  struct IDLIST *idlist;  
  Window win;
  struct FASTOBJ *hash;
  int map;
  int type;
  int pix;             /* always these first 8 elements */

  char **text;
  int *textx;
  int *texty;
  int textlen;
  int cur;
  int isvert;
  int mask;
  int x, y, w, h, ascent;
  void (*callback)();
};


struct MENULIST {
  struct MENULIST *next;
  char *name;
  int type;          /* menu_callback or menu_pullright */
  void (*callback)();
  int x, y, w, h;
  int ix;         /* which in list */
};


struct MENU {
  struct FRAME *frame;
  void *user;
  struct IDLIST *idlist;  
  Window win;
  struct FASTOBJ *hash;
  int map;
  int type;
  int pix;             /* always these first 8 elements */

  char *label;
  int num, w, h;
  int ascent, descent;
  int last_x, last_y;
  struct MENULIST *last_index;
  struct MENULIST *list;
};


struct LABEL
{
  struct LABEL *next;
  void *user;
  struct IDLIST *idlist;  
  struct FRAME *frame;
  int isbold;
  int map;    
  int type;
  int pix;             /* always these first 8 elements */

  int x, y, w, h;
  int shadow_box;
  char *text;
  void (*callback)();
};


struct TEXT {
  struct TEXT *next;
  void *user;
  struct IDLIST *idlist;  
  struct FRAME *frame;
  int len;
  int map;
  int type;
  int pix;             /* always these first 8 elements */

  int x, y, w, h;         /* pixels */
  int ascent, descent;    /* pixels */
  int max;                /* number of characters */
  int pending;            /* redraw */
  int shadow_box;
  char *text;
  void (*callback)();
};


struct LINE {
  struct LINE *next;
  char line[132];
};


struct SPEEDBAR 
{
  struct FRAME *frame;
  void *user;
  struct IDLIST *idlist;  
  Window win;
  struct FASTOBJ *hash;
  int map;
  int type;
  int pix;             /* always these first 8 elements */
  struct MENU *menu;    /* 9th matches FRAME             */

  int x, y, w, h;
  void (*callback)();
  int pressed;
  int timer_set;
  int tpix;
  int bpix;
  double factor;
  double *value; /* pointer to data */
  double savev;  /* save during speed */
  double range;  /* increment % of this value */
};


struct GRAPHIC {
  struct GRAPHIC *next;
  int x1, y1, x2, y2;
  int pixel;
};

struct LIST {
  struct FRAME *frame;
  void *user;
  struct IDLIST *idlist;  
  Window win;
  struct FASTOBJ *hash;
  int map;
  int type;
  int pix;             /* always these first 8 elements */

  void (*call)();
  int    x, y, w, h;
  char **name;
  int    len;
  int    last_motion;
  int    map_feature;
  int    unmap;
  int    ascent, height; /* font info */
  int    sc_start, sc_len;
  struct BUTTON *sticky;
  int    nmulti;
  Pixmap pixmap;
  unsigned char *multi;
};

struct PDATA {
  struct PDATA *next;
  double *xdata;
  double *ydata;
  int len;
  int color;
  int style;     /* style of points */
  int notconn; /* is not connected with lines? */
};


				/* define axis ticks for PLOT */
struct XTICK {
  int num;       /* number of ticks */
  int num_sub;   /* number of sub ticks */
  double start;
  double end;
  double step;
  double step_sub;
  char format[16];
};


#define XLIB_PLOT_NONE  0
#define XLIB_PLOT_RECT  1
#define XLIB_PLOT_LINES 2


struct PLOT {
  struct PLOT *next;
  void *user;
  struct IDLIST *idlist;  
  Window win;
  struct FASTOBJ *hash;
  int map;
  int type;
  int pix;             /* always these first 8 elements */

  int x, y; /* upper left */
  int w, h; 
  int dobox;           /* if true draw a shadowbox */
  int grid;
  struct PDATA *pdata;
  struct XTICK xaxis; 
  char xlabel[80];

  struct XTICK yaxis; 
  char ylabel[80];
  char title[80];
};


struct THERM {
  struct THERM *next;
  void *user;
  struct IDLIST *idlist;  
  struct FRAME *frame;
  Display *display;
  int map;
  int type;
  int pix;             /* always these first 8 elements */

  int x, y, w, h;       /* pixels */
  int ascent, descent;  /* pixels */
  int mode;
  int therm_pix;
  struct XTICK *tick;
  double value;
  void (*callback)();
};


struct GAUGE {
  struct GAUGE *next;
  void *user;
  struct IDLIST *idlist;  
  Window inwin;
  struct FASTOBJ *hash;
  int map;
  int type;
  int pix;             /* always these first 8 elements */

  struct FRAME *frame;
  int x, y;
  int r;                /* radius in pixels  */
  int ang;              /* sweep range, degrees */
  int zang;             /* starting angle (X convention) degrees */
  int ascent, descent;  /* pixels */
  int show_labels;
  int ptr_pix;
  int draw_first, draw_last;
  struct XTICK *tick;
  double value;
  double set_point;
  XPoint arrow[4];
  void (*callback)();
};


struct CHECKBOX  /* just like buttons */
{
  struct BUTTON *next;
  void *user;
  struct IDLIST *idlist;  
  Window win;
  struct FASTOBJ *hash;
  int map;
  int type;
  int pix;             /* always these first 8 elements */

  struct FRAME *frame;
  int state;     
  int use_fixed;
  int shadow_box;
  char *text;
  int x, y, w, h, ascent;
  void (*callback)();
};


extern struct FRAME *frame_head;
extern int _fvdebug;


#define PRINT_MODE_LOCAL	0
#define PRINT_MODE_FILE		1
#define PRINT_MODE_REMOTE	2

struct PRINT_OPTIONS {
	int  mode;
        int  rev;
        int  land;
	char printer[256];
	char command[256];
	char dir[256];
	char file[256];
	char user[256];
	char host[256];
};


struct HELP_TEXT {
	Window window;
	int x;
	int y;
	int w;
	int h;
	int fg;
	int bg;
	char *text;
};

extern Display *fvlib_display;


#include "fv_proto.h"
