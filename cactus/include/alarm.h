
/* Defines for the operator alarm panel */
/* and include file for cactus error message handling */
/* This file superceeds alarms.h and cactus_error.h   */
/* It is included by cactus_global.h                  */

#define SET        1
#define CLEAR      2
#define CLEAR_ALL  3

#define HORN_DELAY 15
#define MAXALARMS  (100)

#define MSG_INFO	0	/* Informational message */
#define	MSG_WARN	1	/* Warning message */
#define MSG_ERR		2	/* Error message */
#define MSG_FATAL	3	/* Severe error message */
#define	MSG_DEBUG	4	/* Debugging message */
#define	MSG_RPT		5	/* Report message */

#define FATAL_LEVEL	1
#define ERROR_LEVEL	2
#define WARNING_LEVEL	4

struct ALARM {
  struct ALARMGLOB *g;
  char *msg;
  char *name;
  int sev;
};

struct ALARMGLOB {        /* global memory alarm structure */
  int set;
  int ack;
  int active;
  int mask;
  double time;
};

