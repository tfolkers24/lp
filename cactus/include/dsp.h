
/*
 * Cactus File @(#)dsp.h	1.10
 *         SID 1.10
 *        Date 05/23/07
 */

#include "rxvars.h"

#define PI 3.14159265358979323846

#ifndef NULL
#define NULL ((void *)0)
#endif

#define HANNING_WEIGHT        1
#define HAMMING_WEIGHT        2

#define DATA_VALID   1 
#define DSP_VECTOR   97
#define DSP_LEVEL    2

#define NCARDS 4
#define NCHIPS 16
#define NQUADS 2
#define NBINS 4
#define NIFS 8
#define HALFIFS 4
#define CHAN 32768  /* max channels per if, so far */

#define PARITY_NONE 0
#define PARITY_ODD 1
#define PARITY_EVEN 3

unsigned long *dram_addr();
unsigned long *sram_addr();

/* OTF communications structure - shared memory */

struct IVTP {
  unsigned long ivtp[0x40]; /* ivtp starts at 0x8000 0000 */
};

struct EIGHTAZEL { /* offsets */
  double ra; 
  double dec; 
  double az; 
  double el;
};

struct COMM {
  char magic[8];          /* COMM */
  struct RXVARS firstrx;  /* rxvars from the header */
  struct RXVARS secondrx; /* rxvars from the receiver command */
  struct EIGHTAZEL eightbeam[8];
  double eightbeam_scanang;
  double cenfreq[8];              /* spec 4 or 8 if modes */
  double skycenfreq[8];           /* spec 4 or 8 if modes */
  double ifoff[8];                /* spec 4 or 8 if modes */
  double bw;                      /* bandwidth of this mode */

  struct HEADER pops;
  long nifs;              /* number of ifs in use this mode */
  long lenif;             /* length of if from mode     */
  long nquads;            /* number of quads in use this mode */
  long mode;              /* current mode */
  long cal_mode;          /* wh->mode of the last cal */
  long nbins;             /* number of bins in use this mode */
  long issigp;            /* true if sigp desired */ 
  long iscal;             /* true if calibrate is desired (different bins) */ 
  long ispolar;           /* true if 4if polarization mode */ 
  long weight;            /* used by sigp */
  long van_vleck;         /* used by sigp */
  long baseif;            /* used by muncher, tells whether nifs 1-4 or 5-8  */

  long auto_dlen;         /* size of one dump's data seg, no sram */
  long auto_slen;         /* size of one dump's count seg, sram only */
  long auto_dump;         /* size of the whole dump */
  long isotf;             /* true if otf active */ 
  long obs_mode;          /* refers to PS, BSP etc */
  long done;              /* for timing, set by end_obs, tells rt to set clr */
  long aborted;           /* dont send done when set */
  long clr;               /* for timing, true when rt is done */
  long caltype;           /* for cals */
  long bank_time;         /* bank switch tm, 0.001 secs units */
  float dump_time;        /* bank switch tm in seconds */
  long otfct;             /* otf count */
  long vzero;             /* v0 mask for the four vata valid bits */
  long inverted;          /* 600 MHz mode inverts spectrum  */
  long safe2monitor;      /* for polling sysmon card  */
  float t_cal;            /* for calibrate */
  long chan_min;
  long chan_max;          /* channel limits, 0-lenif */
  long chan_len;          /* length of array */
  long spare;
};

#define MACOTFMAX 20

struct OTFSTATE { /* on the fly data observing state */
  int total;   
  int curr;
  int issend;
  int send_ix;
  int specnum;
  int count;
  int otf_done; /* tell send_otf thread done */
  int send_thrd; /* write thread handle */
  int fd;
  int offset;
  struct OTFMSG *otf[4][2]; /* nifs, buffers */
};

struct WHERE {
  int mode;     /* arbitrary */
  int nifs;     /* number of ifs this mode */
  int lenif;    /* number of lags in one if of this mode*/
  int wh[NCARDS*NCHIPS];
};

/* holds intermediate data for non-otf data modes */

struct ACCUM  
{
  float *local_data[NBINS];
  float sample_rate[NBINS]; /* must stay first after pointers */
  float integ_time[NBINS];
  float total_sample_rate;
  float total_integ_time;
};

struct COMPDATA {
  int flag;          /* mutex flag */
  struct ACCUM *accum[HALFIFS];
  double counts[NBINS][HALFIFS];
};
extern struct ACCUM *cal[HALFIFS];

struct WHERE *find_mode(long);

#define TIME_TO_SEC (4e-8) /* 24 mhz blanking, 100 Mhz chips */

/* 

  4096 mode and 2048 modes

  1.31072 msecs is the correlator period
  0.02176 blanked time
  1.28896 msecs is the nonblanked time per period

  1024 mode 

  1.31072 msecs is the correlator period
  0.01152 blanked time
  1.29920 msecs is the nonblanked time per period

*/

#define BANK_TIME   1.31072e-3 /* bank switch timing */

/* obs_mode types */

#define MYSTERY_OTYPE   0
#define	PS		1	/* Position Switching Mode */
#define APS		2
#define	FS		3	/* Frequency Switching Mode */
#define	BSP		4	/* Beam and Position Switching Mode */
#define	TP_ON		5	/* Total Power On Beam */
#define	TP_OFF		6	/* Total Power Off Beam */
#define ATP		7
#define	PSM		8	/* Position Switching Mapping */
#define APM		9
#define FSM		10
#define	TPM_ON		11	/* Total Power Mapping  On Beam */
#define	TPM_OFF		12	/* Total Power Mappint  Off Beam */
#define DRIFT		13
#define	PS_CAL		14	/* Position Switching Calibration */
#define	BS_CAL		15	/* Beam Switching Calibration */
#define BLANKING	16
#define SEQUENC 	17
#define FIVE_PT		18
#define OTF             29	/* On-the-fly */
#define CCAL            30	/* Cold Cal */
#define PSS1		42
#define PSPZ            46

/* for data accumulation */

#define BAD_DATA_MARKER  1.666e-20  /* indicates Bad Data */

#define REF_PS         1  /* storage order of reference for PS */
#define SIG_PS         0  /* storage order of signal for PS */

#define REF_FS         0  /* storage order of reference for FS */
#define SIG_FS         1  /* storage order of signal for FS */

#define PBEAM_ON       1  /* storage bin for positive beam on source */
#define MBEAM_ON       0  /* storage bin for minus beam on source */
#define PBEAM_OFF      3  /* storage bin for positive beam off source */
#define MBEAM_OFF      2  /* storage bin for minus beam off source */

#define CAL_SIG        0  /* storage bin for vane position during cal */
#define CAL_REF        1  /* storage bin for sky position during cal */
#define CAL_COLD       2  /* storage bin for cold load during cal */

/* resolution modes */

#define MODETEST       0
#define MODE600_512_8  1
#define MODE600_1K_8   2
#define MODE300_1K_8   3
#define MODE300_2K_8   4
#define MODE150_2K_8   5
#define MODE150_4K_8   6
#define MODE75_4K_8    7
#define MODE75_8K_8    8

#define MODE600_1K_4   9
#define MODE600_2K_4   10
#define MODE300_2K_4   11
#define MODE300_4K_4   12
#define MODE150_4K_4   13
#define MODE150_8K_4   14
#define MODE75_8K_4    15
#define MODE75_16K_4   16

#define MODE600_2K_2   17
#define MODE600_4K_2   18
#define MODE300_4K_2   19
#define MODE300_8K_2   20
#define MODE150_8K_2   21
#define MODE150_16K_2  22
#define MODE75_16K_2   23
#define MODE75_32K_2   24

#define MODE600_1K_4P  25
#define MODE300_1K_8P  26
#define MODE300_2K_4P  27
#define MODE150_2K_8P  28
#define MODE150_4K_4P  29

struct MODE {
  char name[24];
  int (*action)();
}; 

extern unsigned char *attn;
extern float *temp_storage;

