/*
 *  Cactus file @(#)timeLib.h	1.1
 *        Date 10/24/94
 *
 */
#define SUNFUDGE	0.000022222 			  /* i.e. 00:00:00.08 */
#define JAN1989		599601743
#define GREG		588829

int sexagesimal();         /* SEXAGESIMAL -- converts double to xx:xx[:xx.xx] */
int delay();  /* DELAY() -- Custom delay routine that uses select to time out */
int getDoy();                            /* getDoy() returns the day of year. */
int getYear();                                 /* getYear() returns the year. */
int getMonth();                              /* getMonth() returns the Month. */
int getDom();                           /* getDom() returns the day of Month. */
int breakmJulian();                   /* breaks mjd to yr mon day hr min secs */
char *getDate();                  /* getDate() -- returns date like '032293'. */
char *getSlashDate();      /* getSlashDate() -- returns date like '03/22/93'. */
char *getSDate();                  /* getSDate() -- returns date like '0322'. */
char *getRDate();                /* getRDate() -- returns date like '930322'. */
char *getTimeS();      /* getTimeS --  Returns a char string like '12:34:56'. */
char *getcTime();  /* getcTime --  Returns string 'Fri Feb 26 13:30:12 1993'. */
double getNraoDate();       /* getNraoDate() -- returns date like '1570.000'. */
double getTimeD();            /* getTimeD() -- Returns a floating point time. */
double getmJulDate();      /* getJulDate() -- Returns a floating Julian time. */
