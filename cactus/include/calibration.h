
#define TCOLD (81.0)   /* Standard boiling temperature for LN2 (K) */

#define CONST_CAL_VAL (300.0)

#define SPEC_CAL_NORMAL     0
#define SPEC_CAL_FIXED      1

//#define DO_FAKE_TELE_MOVE 1

// Recompile & Restart spectral/control if you change FUNKY_PS_MOVE
//#define FUNKY_PS_MOVE

/* Recompile & restart spectral/control and apex/nutate if you change this */
//#define SR_REST_OFFSET (-120.0)
#define SR_REST_OFFSET (0.0)

/* Voltage levels to set IF system */
#define LEVEL_SET_VANE  3.5
#define LEVEL_SET_SKY   1.5

