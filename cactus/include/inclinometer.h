#ifndef INCLINOMETER_H


struct INCLINOMETER {
	float ut;
	float az;
	float el;
	float tincl;
	int   actual;
	float inc[2];
};

#define INCLINOMETER_H

#endif
