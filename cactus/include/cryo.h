
#include "site.h"

#ifdef MT_GRAHAM

#define RCVR_1_3MMAL   0
#define RCVR_0_8MMSIS  1
#define RCVR_0_4MMSIS  2

#define TANK_SUPPLY_CONST_1_3MMAL   (100.0)
#define TANK_SUPPLY_CONST_0_8MMSIS  (100.0)
#define TANK_SUPPLY_CONST_0_4MMSIS  (100.0)

#define MAX_RCVR     3

#ifndef NO_CRYO_NAMES
  char *rx_names[] = { "1.3mmal",  "0.8mmsis", "0.4mmsis"   };
#endif

#else

#define RCVR_23MM    0
#define RCVR_3MM     1

#define MAX_RCVR     2

#define COMPRESSOR_1_TANK_SENSOR (100.0)
#define COMPRESSOR_3_TANK_SENSOR ( 75.0)

#ifndef NO_CRYO_NAMES
  char *rx_names[] = { "23mm", "3mm" };
#endif

#endif


struct COMP_MONITOR {
        char  magic[16];
	int   rcvr;
        float comp_supply;
        float comp_return;
        float jt_supply;
        float jt_return;
        float tank_supply;
        float jt_flow;
};


struct TEMP_RECORD {
	char  magic[16];
	int   rcvr;
	float temps[8];
        float vac;
};
