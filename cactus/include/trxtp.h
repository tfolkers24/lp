/*
 * Cactus File @(#)trxtp.h	1.5
 *         SID 1.5
 *        Date 07/07/06
 *
 * used for dbe to tuning computer receiver temps and total power
 *
 */

struct TRXTP {
  char magic[8];
  int intime;
  int tp[8];
};
