#ifndef ATM_H

#include "header.h"

#define ATM_H

#define VLIGHT (299792.5)
#define D2R (M_PI / 180.0)
#define SMT_ELEVATION (3.186) // Kilometers elevation
#define  KP_ELEVATION (1.914) // Kilometers elevation

/* Sideband specifiers */
#define LSB              1
#define USB              2



struct SEND_ATM {
	char   magic[16];
	struct HEADER head;
        int    mode;
        int    backend;
	int    part;
	float  eff;
	float  sbratio;
	float  tcold;
	float  thot;
	float  tsky;
	float  trec;
};

struct REPLY_ATM {
	char   magic[16];
	int    backend;
        int    part;
	float  trec;
	float  t_sys;
	float  tcal;
	float  tau_sig;
	float  tau_img;
	float  mmH2O;
	float  atm_t_sig;
	float  atm_t_img;
};


/*

struct DBE_ATM {
	int    dicke_mode[8];
	int    atmBitch;

	double last_mmh2o;
	double last_rh;
	double last_press;
	double last_tamb;
};
 */


#endif
