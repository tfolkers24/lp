/*
 * Cactus File @(#)cactus_error.h	1.1
 *         SID 1.1
 *        Date 12/27/93
 */

/* include file for cactus error message handling */


#define MSG_INFO	0	/* Informational message */
#define	MSG_WARN	1	/* Warning message */
#define MSG_ERR		2	/* Error message */
#define MSG_FATAL	3	/* Severe error message */
#define	MSG_DEBUG	4	/* Debugging message */
#define	MSG_RPT		5	/* Report message */

#define FATAL_LEVEL	1
#define ERROR_LEVEL	2
#define WARNING_LEVEL	4
