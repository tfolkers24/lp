/*
 * Cactus File %W%
 *         SID %I%
 *        Date %G%
 */

#ifndef SAMBITS_H

/* SAM bus signals */

#define SPEC_OTF        0x0001                   /* SPEC: OTF in progress */
#define DBE_OTF         0x0002                    /* DBE: OTF in progress */
#define NUTATOR_SR      0x0004                    /* DBE: OTF in progress */
#define VANE_POS        0x0008                    /* DBE: OTF in progress */

#define SIG_REF         0x0010                             /* Sig/Ref bit */
#define MAC_HYS         0x0020                                 /* Mac Bit */
#define NOT_USED_1      0x0040
#define TS_ACK          0x0080                     /* Tracker acknowledge */

#define NOT_USED_2      0x0100
#define RX_LOCK         0x0200                    /* Receiver out-of-lock */
#define NOT_USED_3      0x0400
#define NOT_USED_4      0x0800

#define FOCUS_BIT       0x1000                        /* Antenna in Focus */
#define OFF_SOURCE_BIT  0x2000                      /* Antenna off source */
#define VANE_ENABLE     0x4000                            /* Vane Control */
#define SAMBUS_CLOCK    0x8000      /* 100ms SAM bus clock (for latching) */

#define SAMBITS_H
#endif
