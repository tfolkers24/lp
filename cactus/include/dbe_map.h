
#ifndef DBE_MAP

#include "dbe.h"
#include "header.h"
#include "dbe_struct.h"
#include "t_sys.h"
#include "cur_pt_model.h"
#include "dbe_pos.h"
#include "dbe_status.h"
#include "dbe_config.h"

#define MAX_TP_ARRAY 256
#define MAX_POSITION_ARRAY 16384
#define MAX_DATA_ARRAY 16384

int dbe_map(int iswrite);
int cm_map(void);


struct CONTROL_MEMORY {
        int    active_chans[MAX_DBE_CHAN];
        int    TimeStamp[MAXSAMPLES];
        float  tc[MAX_DBE_CHAN];
        float  tsys[MAX_DBE_CHAN];
        float  rtsys[MAX_DBE_CHAN];
	float  volts[MAX_DBE_CHAN];
	float  cnts_per_mkelvin[MAX_DBE_CHAN]; /* Normalized VCO counts per milli-Kelvin */
	char   cur_state[MAXCOMMAND];
        char   obs_task[MAXCOMMAND];
        char   old_tele_command[MAXCOMMAND];
        char   last_command[MAXCOMMAND];
	int    which_two[2];
	int    dstar;
	float  phase_norm;	   /* Phase normalization value (to unit time)*/
	float  vane_temp;

	struct DBE_STATUS dbe;
	struct curPtMod cptm;
        struct RX_T_SYS rx_t_sys;
        struct RXVARS secondrx;           /* rxvars from the receiver command */
	struct SP spo;                    /* contains most internal variables */
        struct HEADER pop_head;
	struct DBE_CONFIG dbe_config;
	struct POSITION_INFO cur_position_info;
};

struct DATA_MEMORY {
	struct FIFO_BUFFER tpsamples[MAX_TP_ARRAY];
	struct POSITION_INFO pos_info[MAX_POSITION_ARRAY];
	struct FIFO_BUFFER datasamples[MAX_DATA_ARRAY];
};

#define DBE_MAP

#endif
