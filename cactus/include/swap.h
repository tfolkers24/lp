/*
 * Cactus File @(#)swap.h	1.3
 *         SID 1.3
 *        Date 08/17/04
 */

/* Prototypes for swapdatum.c */

double swapD(double d);

double swapd(double *d);
float swapf(float *l);
long swapl(long *l);
int swapi(int *l);
unsigned short swaps(unsigned short *s);

void in_swapus(unsigned short *s);
void in_swapf(float *f);
void in_swaps(short *s);
void in_swapd(double *d);
void in_swapl(long *ll);
void in_swapi(int *ii);
