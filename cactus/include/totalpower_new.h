/*
 * Cactus File @(#)totalpower.h	1.1
 *         SID 1.1
 *        Date 06/23/03
 */

#define TPRECORD         4
#define READSPERMIN     15
#define MAX_TP_CHANS     4


struct TOTAL_POWER {
	char magic[16];					      /* "TOTALPOWER" */
	double chan_tp[MAX_TP_CHANS];
	double chan_sp[MAX_TP_CHANS];
};
