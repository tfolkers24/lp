
#ifndef BACKENDS_H

#define BACKENDS_H

/* A backend is a collection of spectra, all the same resolution, produced by
 * the same physical spectrometer. There may be more than one backend per
 * physical spectrometer if it can generate multiple resolution spectra at the
 * same time */

/* A part is one spectrum, produced by a single receiver IF signal. */


#define MAX_BACKENDS 3
#define MAX_PARTS    16

#define MAX_VALUE_FBS    16384
#define MAX_VALUE_MAC    16384
#define MAX_VALUE_ARO    16384

#define MAX_FBS_CHANNELS    512
#define MAX_MAC_CHANNELS  32768
#define MAX_ARO_CHANNELS  32768

#define MAX_SPEC_CHANS (MAX_ARO_CHANNELS) // Set to the biggest backend

#define BACKEND_FBS        0
#define BACKEND_MAC        1
#define BACKEND_ARO        2
#define BACKEND_DBE_4GHZ   3  /* Define DBE backend here for rtdata's sake */
#define BACKEND_OPTICAL    4

//
// Min Refresh Rate per Backend in milliseconds
//
#define FBS_REFRESH_RATE_MS  200
#define MAC_REFRESH_RATE_MS  200
#define ARO_REFRESH_RATE_MS  200

//
// List of Physical Back Ends.
//
#define FBS_BE            0
#define MAC_BE            1
#define ARO_BE            2
#define MAX_PHYSICAL_BE   3
#define DBE_BE            4  /* Define DBE backend here for rtdata's sake */

/* DBE Channels */
#define N_IF_CHANS        4

#define MAX_DBE_CHAN      4
#define MAX_DBE_PHASE     2


#endif
