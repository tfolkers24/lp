#ifndef GTK_COLORS_H
#define GTK_COLORS_H

#define GTK_BLACK	    0
#define GTK_BROWN	    1
#define GTK_RED	    2
#define GTK_ORANGE	    3
#define GTK_YELLOW	    4
#define GTK_GREEN	    5
#define GTK_BLUE	    6
#define GTK_VIOLET	    7
#define GTK_GRAY	    8
#define GTK_WHITE	    9
#define GTK_MAGENTA	   10
#define GTK_CYAN	   11
#define GTK_GRAY85	   12
#define GTK_GRAY75	   13
#define GTK_GRAY65	   14
#define GTK_GRAY55	   15
#define GTK_LTYELLOW   16

#define MAX_GTK_COLORS 17

gint allocColors(GtkWidget *drawable);

#ifdef DEFINE_GTK_COLORS
GdkColor gtk_colors[MAX_GTK_COLORS];
#else
extern GdkColor gtk_colors[MAX_GTK_COLORS];
#endif

struct GTK_COLOR_DEF {
	char *name;
	int red;
	int green;
	int blue;
};

#ifdef DEFINE_GTK_COLORS
struct GTK_COLOR_DEF gtk_color_defs[] = {
	{"black", 	0, 	0, 	0},
	{"brown", 	42240, 	10752, 	10752},
	{"red", 	65535, 	0, 	0},
	{"orange", 	65535, 	42240, 	0},
	{"yellow", 	65535, 	65535, 	0},
	{"green", 	0, 	65535, 	0},
	{"blue", 	0, 	0, 	65535},
	{"violet", 	60928, 	33280, 	60928},
	{"gray", 	48640, 	48640, 	48640},
	{"white", 	65535, 	65535, 	65535},
	{"magenta", 	65535, 	0, 	65535},
	{"cyan", 	0, 	65535, 	65535},
	{"gray85", 	55552, 	55552, 	55552},
	{"gray75", 	48896, 	48896, 	48896},
	{"gray65", 	42496, 	42496, 	42496},
	{"gray55", 	35840, 	35840, 	35840},
	{"ivory1", 	65535, 	65535, 	240*256}
};
#else
extern struct GTK_COLOR_DEF gtk_color_defs[];

#endif

#endif

