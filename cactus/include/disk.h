
#define UTIL_START	 0
#define UTIL_STOP	 3
#define AIPS_START	 4
#define AIPS_STOP       13	
#define DATA_START      14	
#define DATA_STOP       22	
#define MAX_DISKS       23

char *disk_labels[] = { "obs", "cor", "boh", "bck",
                        "a1 ", "a2 ", "a3 ", "a4 ", "a5 ", 
                        "c1 ", "c2 ", "c3 ", "c4 ", "c5 ", 
			"d1 ", "d2 ", "d3 ", 
			"d4 ", "d5 ", "d6 ", 
			"d7 ", "d8 ", "d9 "

};

int  disk_gmax[] = {    26067,  1979,  1952, 1189,
                        35278, 35278, 35278, 35278,  35278,
                        35278, 35278, 35278, 35278,  35278,
			52522, 52522, 52522, 
			52522, 52522, 52522, 
			52522, 52522, 52522 
};

char *disks[]  = { "/home/obs",    "/home/corona", "/home/bohemia",
		   "/home/backup",
		   "/home/aips1",  "/home/aips2",  "/home/aips3", 
		   "/home/aips4",  "/home/aips5", 
		   "/home/class1", "/home/class2", "/home/class3", 
		   "/home/class4", "/home/class5", 
                   "/home/data1",  "/home/data2",  "/home/data3",
                   "/home/data4",  "/home/data5",  "/home/data6",
                   "/home/data7",  "/home/data8",  "/home/data9",
                   "/home/data10"
};

struct DISK_SPACE {
	float cap;
	float space;
};

struct DISK_MSG_BUF {
	char magic[12];
	struct DISK_SPACE disk[MAX_DISKS];
};
