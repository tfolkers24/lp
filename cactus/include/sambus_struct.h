#ifndef SAMBUS_STRUCT_H
#define SAMBUS_STRUCT_H

#define BNC_TO_SAMBUS 1
#define SAMBUS_TO_BNC 2

struct SAMBUS_BIT {
	int  bit;
	int  dir;
	char label[80];
	char xlabel[80];
	char define[80];
	char source[256];
};


struct SAMBUS_ARRAY {
	int valid;
	int n;
	int n_in;
	int n_out;

	struct SAMBUS_BIT bits[24];
};

#endif
