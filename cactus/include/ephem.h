struct BODY_POS {
  double RA;
  double DEC;
  double Hor_Par;
  double Geo_Dist;
  double Vrad;
};

#define NUMORB 8
struct BODY_PARS {
  char name[24];
  double orb_pars[NUMORB];		/* Orbital elements */
};

struct NEW_BODY {			/* Message from tracker servo */
  int ret;				/* Return status */
  struct BODY_PARS body_pars;		/* Input structure */
  double mjd;				/* Time */
};

struct BODY_EPHEM {			/* Message from tracker servo */
  int ret;				/* Return status */
  struct BODY_PARS body_pars;		/* Input structure */
  double mjd;				/* Time */
  struct BODY_POS body_pos;		/* Result structure */
};

/* Indices of above orb_pars[] */
enum ORB_PARS {DAILY_MOT=0, PER_PASSAGE, PER_DIST, ECCEN, SEMI_MAJOR,
  PER_ARG, ASC_NODE, INCLIN, MEAN_ANOM = PER_DIST};
