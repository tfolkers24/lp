/*
 *  Cactus file @(#)dbe_pos.h	1.1
 *        Date 11/13/02
 *
 */

#ifndef DBE_POS_H


struct POSITION_INFO {
	float ut;
	float lst;
	float raoff;
	float decoff;
	float azoff;
	float eloff;
	float az;
	float el;
};


struct SEND_POS {
	char magic[16];
	struct POSITION_INFO pi;
};


#define DBE_POS_H

#endif
