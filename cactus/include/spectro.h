
#ifndef SPECTRO_H
#define SPECTRO_H

struct SPECTRO {
	char  magic[32];

	int   nchan;
	int   sigref;
	int   sampat;
	int pad;

	float resolution[2];
	
	unsigned short dr11data[512];
};

struct SPEC_GAINS {
	char  magic[32];

	float gains[512];
};

#endif
