#define FG_WIDTH        640                            /* default board width */
#define FG_HEIGHT       480                           /* default board height */
#define FG_DEPTH        8                              /* default board depth */

struct OPTICAL {
	char magic[16];
	int nbytes;
	int bcol;
	int brow;
	int width;
	int height;
	int pad[3];
	unsigned char buf[FG_WIDTH*FG_HEIGHT];
};
