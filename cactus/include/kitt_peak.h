/* SUN_CHECK_PERIOD_MS is the time period (in milliseconds) between checks
   for sun proximity. These are done regardless whether telescope is tracking
   or not. As long as the program is running, it checks the sun proximity
   every SUN_CHECK_PERIOD_MS milliseconds.*/
#define SUN_CHECK_PERIOD_MS 2000
#define SUN_CHECK_NIGHT_MS  60000 /*!< after sunset check only every minute. */

/* Every SUN_BLOB_INTERVAL milliseconds the coords of the sun avoidance zone 
   perimeter are sent to catalog by writing a sunBlob.txt file. this allows
   catalog to draw the sun avoidance zone on it's display.*/
#define SUN_BLOB_INTERVAL (2 * 60 * 1000)  /*!< See sunBlob.h also! */
#define N_SAFE_POINTS 100 

/* These are ultimate limits in the sense that if they are exceeded, vel is 
   forced = 0.0 in speedPosLimit(). Units = arcsec. */
#define CW_LIMIT_DEG  447.0   /*!< Absolute limit is actually 450 */
#define CCW_LIMIT_DEG (-87.0) /*!< Absolute limit is actually -90. */
#define CW_LIMIT_ASEC (CW_LIMIT_DEG * 3600.0)  
#define CCW_LIMIT_ASEC (CCW_LIMIT_DEG * 3600.0)
#define CW_LIMIT_RAD (CW_LIMIT_DEG*RAD_DEG)
#define CCW_LIMIT_RAD (CCW_LIMIT_DEG*RAD_DEG)

#define EL_MAX_LIMIT_DEG 125.0  /*!< Max tracking limit. */ 
#define EL_MAX_LIMIT_ASEC ( EL_MAX_LIMIT_DEG * 3600.0)  /*!< Units are arcsec. */
#define EL_MIN_LIMIT_DEG_DEFAULT  3.0
#define EL_MIN_LIMIT_DEG  ts->elMinLimitDeg
#define EL_MIN_LIMIT_ASEC ( EL_MIN_LIMIT_DEG * 3600.0)
#define EL_MIN_HARD_LIMIT_DEG 2.0

#define ASEC_360     (360.0 * 3600.0)
#define ASEC_CW_LIM  (CW_LIMIT_ASEC - ASEC_360) /*!< Always a little < 90 deg.*/
#define ASEC_CCW_LIM (ASEC_360 + CCW_LIMIT_ASEC) /*!< A little > 270 deg. */

#define AZIMUTH    0  /*!< Code used for the speed.axis flag. */
#define ELEVATION  1  /*!< Code used for the speed.axis flag. */

#define APA_X_PLATE_SCALE 34.45
#define APA_Y_PLATE_SCALE 34.45
