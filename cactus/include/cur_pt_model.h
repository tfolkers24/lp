/*
 * Cactus File %W%
 *         SID %I%
 *        Date %G%
 */

#ifndef CUR_PT_MODEL_H

#define PTMOD_MAGIC "ptmod"
/* This structure is used for reporting the current pointing model in use. */
struct curPtMod { char magic[24]; /* normally this is "ptmod" */
                  int rxbay; /* currently selected receiver bay. */
                  char recName[16]; /* name of corresponding receiver. */
                  float ref;  /* refractive index. */
                  double NulA, ColA, Perp, IncW, IncN, NulE, EFlx, ZFlx;
                  double CenW;
                  double CenN;
                  double w2lr;
                  double n2lr;
                  double w2fb;
                  double n2fb;
                  double sfcz;
                  double third_refract;
                };

#define CUR_PT_MODEL_H

#endif
