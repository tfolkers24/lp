#ifndef SITE_H
 
#define KITT_PEAK

#define OBSLONGU   -7.440990281  /* Longitude in hours. uncorr. for wandering */ /* Verified twf 7/9/14 */
#define OBSLATU    31.95333333   /* Latitude in degrees. uncorr. for wandering*/ /* Verified twf 7/9/14 */

#define LONG       (OBSLONGU)
#define LAT        (OBSLATU) 

#define SINLAT      0.5292283644
#define COSLAT      0.8484794272

#define KP_ELEVATION (1.914) // Kilometers elevation

#define MAX_CPUS       8
#define MAX_CPU_HOSTS 12

#define CPU_HOST_BALLENA 0
#define CPU_HOST_CORONA  1
#define CPU_HOST_KING    2
#define CPU_HOST_MODELO  3
#define CPU_HOST_OPTICAL 4
#define CPU_HOST_PYTHON  5
#define CPU_HOST_TECATE  6
#define CPU_HOST_TRACKER 7
#define CPU_HOST_TSDPY   8
#define CPU_HOST_TUX     9
 
#define SITE_H
 
#endif
