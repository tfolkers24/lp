/*
 Cactus file @(#)gs_init.h	1.6
        Date 7/21/93
*/

struct GSKEY {
  void *offset;
  char *name;
  int type;   /* INTEGER FLOAT DOUBLE CHARSTAR */
  int  size;  /* 1 if not array                */
  int field;  /* active/passive field          */
  int log;    /* send log message to monitor when changes */
  int rambo;  /* include this in ramexec ?     */
};
