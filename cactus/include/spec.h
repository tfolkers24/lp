/* Spec's Global Memory Definition */

#ifndef SPEC_H
#define SPEC_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "backends.h"
#include "spec_config.h"

#ifndef TRUE
#define TRUE  1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#define MAX_RXS 6 // put this in rx.h?

#define VLIGHT (299792.5)
#define D2R (M_PI / 180.0)
#define SMT_ELEVATION (3.186) // Kilometers elevation

#define BAD_CHAN_VAL (1e-20)

/* define FAKE_DATA to test system in Tucson; Otherwise the system
 * expect to talk to real hardware backends
 *
 */

// #define FAKE_DATA 1

#ifdef FAKE_DATA
  #define HARD_ENABLE_FBS   1
  #define HARD_ENABLE_MAC   1
  #define HARD_ENABLE_ARO   1
#else
  #define HARD_ENABLE_FBS   0
  #define HARD_ENABLE_MAC   0
  #define HARD_ENABLE_ARO   0
#endif

static const int backend_channels[MAX_BACKENDS] = {
	MAX_FBS_CHANNELS,
	MAX_MAC_CHANNELS,
	MAX_ARO_CHANNELS
};


//
// See file ~cactus/spectral/include/backend_names.h
// for a character strings of above defined backends.
//

#define SIG_BIN               0
#define REF_BIN               1
#define TRASH_BIN             2
#define MAX_BINS              3

#define MAX_BAD_CHANS        32

// these are internal bad bin numbers for diagnostic use
#define NOT_ENABLED_BIN       4
#define ZERO_TIME_BIN         8
#define LOCAL_BLANK_BIN    0x80

#define MAX_ATTEN           128

#define SPEC_CONFIG_MAGIC   "SPECCONF"
#define SPEC_ATTEN_MAGIC    "SPECATTEN"
#define SPEC_MUNCH_MAGIC    "MUNCHMAGIC"


#define MAX_SPEC_PROCESS     25

/* Observing Modes */
/* See backend_names.h for a string of these */
#define OBS_MODE_ANY         -1

#define OBS_MODE_IDLE         0
#define OBS_MODE_HOT          1
#define OBS_MODE_SKY          2
#define OBS_MODE_COLD         3
#define OBS_MODE_ZERO         4
#define OBS_MODE_SIG          5
#define OBS_MODE_REF          6
#define OBS_MODE_BSP          7
#define OBS_MODE_OTF          8
#define OBS_MODE_TPON         9
#define OBS_MODE_TPOFF       10

#define OBS_MODE_MAX         11


#define NORMAL_BLTIME        270
#define NORMAL_PREDELAY       40

/* Actual Observing types */

#define OBS_IDLE	 0
#define OBS_PS		 1
#define OBS_APS		 2
#define OBS_FS		 3
#define OBS_BSP		 4
#define OBS_TP_ON	 5
#define OBS_TP_OFF	 6
#define OBS_ATP		 7
#define OBS_PSM		 8
#define OBS_APM		 9
#define OBS_FSM		10
#define OBS_TPM_ON	11
#define OBS_TPM_OFF	12
#define OBS_DRIFT	13
#define OBS_PS_CAL	14
#define OBS_BS_CAL	15
#define OBS_BLANKING	16
#define OBS_SEQUENCE	17
#define OBS_FIVE_PT	18
#define OBS_CONT_MAP	19
#define OBS_FOCALIZE	20
#define OBS_NS_FOCALIZE 21
#define OBS_TP_TIP      22
#define OBS_SP_TIP      23
#define OBS_D_ON        24
#define OBS_CALIBRATE   25
#define OBS_FS_PS       26
#define OBS_EW_FOCALIZE 27
#define OBS_ZEROS       28
#define OBS_TLPW        29      
#define OBS_FQSW        30      
#define OBS_NOCL        31      
#define OBS_PLCL        32      
#define OBS_ONOF        33      
#define OBS_BMSW        34      
#define OBS_PSSW        35      
#define OBS_DRFT        36      
#define OBS_OTF         37      
#define OBS_S_ON        38      
#define OBS_S_OF        39      
#define OBS_QK5         40
#define OBS_QK5A        41
#define OBS_PSS1        42 /* PS flip or PS-1 mode */
#define OBS_VLBI        43
#define OBS_PZC         44
#define OBS_CPZM        45
#define OBS_PSPZ        46
#define OBS_CPZ1        47
#define OBS_CPZ2        48
#define OBS_CCPZ        49
#define OBS_CCPM        50
#define OBS_TPPZ        51
#define OBS_OPT         52
#define OBS_COLDCAL     53

/* Sideband specifiers */
#define LSB              1
#define USB              2

#define CAL_VANE         0 /* Normal Vane Cal */
#define CAL_AVGAINS      1 /* Const Cal fac specified by Tsys */
#define CAL_CONST        2 /* Const Cal fac specified by config const value */

#define DICKE_SRR        0 /* (Signal-Reference/Reference mode (normal) */
#define DICKE_SR         1 /* (Signal-Reference) mode (for Comb test, etc) */

#define SIG_BIN          0
#define REF_BIN          1
#define ANY_BIN         -1

#define CFG_SSS          0 /* variaous series/parallel modes */
#define CFG_SPP          1
#define CFG_SSP          2
#define CFG_SPS          3
#define CFG_DPS          4
#define CFG_DSS          5


// ------------------------ configuration -------------------------------

// Below are the possible values for each configuration menu field

// Sideband code stored in cpr->sb and alt_sb
#define SB_NONE  0   // (secondary freq. only) disabled
#define SB_LSB   1   // LSB
#define SB_USB   2   // USB

// Polarization code stored in cpr->pols
#define POL_HV   0   // Both - normal
#define POL_VH   1   // Both - swap for testing
#define POL_H    2   // H Pol only
#define POL_V    3   // V Pol only

#define IF_A     0   // IF channel wired to filterbank IF input A
#define IF_B     1   // IF channel wired to filterbank IF input B
#define IF_C     2   // IF channel wired to filterbank IF input C & AOS A & AOS C
#define IF_D     3   // IF channel wired to filterbank IF input D & AOS B & CTS


// sent from each spectrometer to catcher for each data set
//
// NOTE: Some items will be filled later in the data pipeline.
//
struct SPEC_DATA_SUB_SET {
  char   magic[MAGIC_SIZE];	// fill with SPEC_CONFIG_MAGIC (16 bytes)
  int    which_spec;            // Which spec this data belongs to.
  int    nparts;                // number of spectra for this spectrometer
  int    nchans;                // number of channels in each spectrum
  int    depth;                 // Number of seperate spectra. (for OTF / DBE?)
  int    bin;                   // bin 0 for sig, 1 for ref, 2 trash
  int    walsh;                 // walsh function value for this dataset
  int    int_time;              // integration time in microseconds
  int    error_bits;            // zero for good, error bits defined in header
  int    mode;                  // Observing modes: SIG, HOT, SKY etc
  float  obsTime;               // Mid-Time for observation (OTF Mapping)
  float  raoff;                 // Right Assension Offset for OTF Mapping;
  float  decoff;                // Declination Offset for OTF Mapping;
  float  azoff;                 // Azinuth Offset for OTF Mapping;
  float  eloff;                 // Elevation Offset for OTF Mapping;
  int    start_time[2];         // when integration started ([0]=sec,[1]=nsec)
  int    end_time[2];           // when integration ended   ([0]=sec,[1]=nsec)
}; /* 16 + (14 * 4 ) + (2 * 8) = 88 bytes in size */

struct SPEC_DATA_SET {
	struct SPEC_DATA_SUB_SET sdss;
	int data[MAX_SPEC_CHANS];       // raw spectral data array
};

/* same struct only with floating point data */
struct SPEC_CAL_SET {
	struct SPEC_DATA_SUB_SET sdss;
	float data[MAX_SPEC_CHANS];     // Processed Cal array
};

/* same struct only with doubles */
struct SPEC_ACCUM_SET {
	struct SPEC_DATA_SUB_SET sdss;
	double data[MAX_SPEC_CHANS];    // Accumalation data array
};

// sent from catcher to each spectrometer as config command 
//struct SPEC_CONFIG {
//  char magic[MAGIC_SIZE];	// fill with SPEC_CONFIG_MAGIC
//  int spec_enable;              // This backend is usable.
//  int data_enable;              // Data acq enable flags for each backend
//  int which_spec;               // Which spec this config belongs to.
//  int nparts;                   // number of spectra for this spectrometer
//  int valid_mask;		// SAMbus blanking mask
//  int valid_bits;		// SAMbus blanking valid bits
//  int mode;			// Backend Mode; i.e. AROWS mode; FBS and MAC probably won't use.
//  int image_offset;		// Obsolete!image offset(kHz) for 4-IF backends
//  int doppler_khz[MAX_PARTS];   // doppler shift of alt. sideband per FFB part
//  int offset_khz[MAX_PARTS];    // manual offset added for each FFB part
//  double res;                   // backend resolutions in MHz
//  double center;                // and center channels in MHz from sky LO
//};

struct ALL_SPEC_CONFIG {
	char magic[MAGIC_SIZE];
        int spares[64];
	struct timespec time_stamp;   // when config sent
	struct SPEC_CONFIG configs[MAX_BACKENDS];
};


struct SPEC_ATTEN {
	char magic[MAGIC_SIZE];
	int which_spec;
	int n;
	struct timespec time_stamp;   // when atten sent
	int data[MAX_ATTEN];
};


struct MUNCH_COMMAND {
	char   magic[MAGIC_SIZE];     // MUNCHMAGIC
	int    scan_segment;          // Sequence number of scan part
        int    walsh;                 // walsh function value for this dataset
	int    mode;                  // mode segment; vane, sky, off, on, otf
	int    scanno;                // scan number for this observation
	float  secs;                  // Requested integration time (Secs)
	int    nbufs;                 // Num of buffers for this obs
	int    counts;                // Sizes of each buffer.
        int    depth;                 // Number of buffers (i.e. OTF)
	int    act_integ[2];          // Actual integration time. (uSec)
	int    start_time[2];         // when data collection started
	int    end_time[2];           // when data collection stopped
};


#endif
