
#ifndef RECEIVER_GLOBAL_H

#define RECEIVER_GLOBAL_H

struct RECEIVER_GLOBAL {

	float  last_az;                /* Current az to be saced */
	float  last_el;                /* Current el to be saced */
	float  last_ax0;               /* Current focus to be saced */
	float  last_ns0;               /* Current ns focus to be saced */
	float  last_tau0;              /* Current tau0 to be saced */

	float  last_fit_az1;           /* Azimuth chan1 fit to 5-point */
	float  last_fit_az2;           /* Azimuth chan2 fit to 5-point */
	float  last_fit_azavg;         /* Azimuth avg   fit to 5-point */

	float  last_fit_el1;           /* Elevation chan1 fit to 5-point */
	float  last_fit_el2;           /* Elevation chan2 fit to 5-point */
	float  last_fit_elavg;         /* Elevation avg   fit to 5-point */

	float  last_fit_ax01;          /* Chan1 Fits to focalize */
	float  last_fit_ax02;          /* Chan2 Fits to focalize */
	float  last_fit_ax0avg;        /* Avg  Fits to focalize */

	float  last_fit_ns01;          /* Chan 1 Fits to NS Focalize */
	float  last_fit_ns02;          /* Chan 2 Fits to NS Focalize */
	float  last_fit_ns0avg;        /* Avg  Fits to NS Focalize */

	float  last_fit_tau01;         /* Chan 1 Fit to SPTIP */
	float  last_fit_tau02;         /* Chan 2 Fit to SPTIP */
	float  last_fit_tau0avg;       /* Avg Fit to SPTIP */

	float  last_spares[64];

	double last_restfreq[2];       /* Last Rest frequency (GHz). */
	char   last_line_name[24];     /* Line name. */
	char   last_sideband_name[24]; /* Receiver sideband ("USB" | "LSB") */
	char   last_sideband_name2[24];/* second channel ("USB" | "LSB") */
};

#endif
