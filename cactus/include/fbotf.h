/*
 *  Cactus file %W%
 *        Date %G%
 *
 */

/*
   this file is used both by rtdata and fbmux
   so sparc alignment must be considered when adding elements
   also used by the DSP card and hyspec hence all ints
*/
                   /* OTF max is number of .1 sec spectra saved between barfs */
#define OTFMAX  20 /* to the data sender which sends data to rtdata           */

#define MAXFLY  10000 /* Maximum no. of on-the-fly data points */

struct LINEFLY {
  int scan;              /* scan number for this computation  */
  float ut[MAXFLY];      /* UT corresponding to data taker (hours) */
  float ra[MAXFLY];      /* Corresponding RA offsets (degrees) */
  float dec[MAXFLY];     /* Corresponding Dec. offsets */
  float az[MAXFLY];      /* Corresponding Az. offsets */
  float el[MAXFLY];      /* Corresponding El. offsets */
};


struct OTFMSG {
  int  tag[2];     /* really char[8] set to "FBOTF" or "HCOTF" */
  int  total;      /* total number of otf spectra expected     */
  int  spec_size;  /* size of a spectra (256 or 128)           */
  int  index;      /* num of spectra from start                */
  int  spec_num;   /* num of spectra in this dump              */
  int  subscan;    /* 1-4 the subscan index of this data       */
  int  baseif;     /* 0 for fb, subscan num = baseif + subscan */
  float data[1];   /* waste 4 bytes for convenience            */
};

#define OTFSIZE(x) (sizeof(struct OTFMSG)+ OTFMAX*sizeof(float)*x)

#define MAXSCANDAT	10000	/* Max. # of positions saved while scanning  */
#define MAXSCAN		2000	/* Max. # of offsets saved for OFFDATA cmd.  */

struct FLY {
  int cmd_az;    /* Commanded az of ant during last scan */
  int cmd_el;    /* Commanded el of ant during last scan */
  int act_az;    /* Az pos of antenna during last scan   */
  int act_el;    /* El pos of antenna during last scan   */
  int aedoy;     /* DOY & time(10ms) correspond to above */
  int aetime;
};

struct AZEL_FLY {
  char magic[8];
  int nfly;
  int spare; /* sparc alignment */
  struct FLY fly[MAXSCANDAT];
};

struct FLYPOS {
  double scanra;   /* Scan R.A. offsets (degrees)       */
  double scandec;  /* Scan Dec. offsets (degrees)       */
  double scanaz;   /* Scan AZ offsets (degrees)         */
  double scanel;   /* Scan EL offsets (degrees)         */
  double scanpara; /* Scan parallactic angles (degrees) */
  double scantim;  /* Corresponding UT time (hours)     */
};

struct POS_FLY {
  char magic[8];
  int npos;
  int spare; /* sparc alignment */
  struct FLYPOS pos[MAXSCAN];
};

