/*
 Cactus file %W%
        Date %G%
*/

/* 
 * This file contains symbols and macros that are generally useful to
 * the sun cactus software
 */
#ifndef CACTUS_H
#define CACTUS_H

char *cactus_home;

#define PROCESS_INFO_STATUS 0

#define	SZ_LINE		161	/* Length of typical text line. */
#define	SZ_FNAME	63	/* Maximum file name length. */
#define	SZ_PATHNAME	127	/* Maximum path name length. */
#define	EOS		'\0'	/* End of string (null). */
#define	ERR		(-1)	/* Error condition return value. */
#define	OK		0	/* Success condition return value. */
#define	YES		1	/* Boolean true. */
#define	NO		0	/* Boolean false. */

#define	BOF		0	/* Beginning-of-file. */
#define	BOFL		(-3L)
#define EOFL		(-2L)

#define MSG_INFO	0	/* Informational message to writemsg() */
#define	MSG_WARN	1	/* Warning message */
#define MSG_ERR		2	/* Error message */
#define MSG_FATAL	3	/* Severe error message */
#define	MSG_DEBUG	4	/* Debugging message */
#define MSG_UNKNOWN	5	/* for writemsg use only */

#define	MSG_NO_MAIL	32	/* add to suppress sending to monitor */
#define	MSG_BIT_NOMAIL	5
#define	MSG_NO_PRINT	64	/* add to suppress printing on screen */
#define	MSG_BIT_NOPRINT	6

#define	WRITEMSG_EF	14	/* event flag for writemsg */
#define	MAXMBLINE	2050	/* formerly of ipc.h */

/* probably not the place for this... */


/* -------------------------------------------------------------------- *
 * 		External (function prototype) definitions		*
 * -------------------------------------------------------------------- */

/* CACTUS string handling routine definitions (cacstr.c). */

extern	int	getntok ();
extern	char	*felem ();
extern	char	*strcmprs ();
extern	double	ctod ();
extern	long	ctoma ();
extern	char	*strlwr ();
extern	char	*strupr ();
extern	char	*strbin ();

/* message writing */

extern	int	writemsg ();

/* Supplementary bit functions (getbits.c) */

extern	int	getbits ();
extern	int	chkbit ();

#define	BITTEST(bitno,pattern)	(pattern >> bitno) & 1
#define BITSET(bitno,pattern)	pattern = (pattern | (1 << bitno))
#define	BITCLEAR(bitno,pattern)	pattern = (pattern & ~(1 << bitno))

/* -------------------------------------------------------------------- *
 * 		Sun-OS version 4 constants, symbols, etc.		*
 * -------------------------------------------------------------------- */

/* Assorted machine constants. */

#define	EPSILON		(1.192e-7)	/* smallest real E s.t. (1.0+E > 1.0) */
#define EPSILOND	(2.220d-16)	/* double precision epsilon */
#define	MAX_LONG	2147483647

/* Indefinite valued numbers. */

#define	INDEFS		(-32767)
#define	INDEFL		(0x80000001)
#define	INDEFI		INDEFL
#define	INDEFR		1.6e38
#define	INDEFD		1.6e38
#define	INDEF		INDEFR

#define PROTOTYPES	1      /* Use ANSI function prototypes (1=yes, 0=no)? */

/* These are handy. */

#define	min(a,b)	(((a)<(b))?(a):(b))
#define	max(a,b)	(((a)>(b))?(a):(b))
#define mod(a,b)	((a)-((a)/(b))*(b))

#define	labs		(long) abs

#ifndef	NO_EXTERNS
extern	char	*strstr ();
#endif


#ifndef DEG_TO_RAD
#define DEG_TO_RAD (M_PI/180.0)
#endif

#endif			/* End #ifndef CACTUS_H */
