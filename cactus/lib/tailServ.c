#include <stdio.h>
#include <fcntl.h>

char  buf[4096];
char *nrml_vid  = "\033[m";
char *bold_vid  = "\033[1m";
char *unsc_vid  = "\033[4m";
char *rvrs_vid  = "\033[7m";
char *fatal_vid = "\033[43;31;7m";		    /* red with black writing */
char *error_vid = "\033[43;30;1m";		 /* yellow with black writing */
char *warn_vid  = "\033[42;30;1m";		 /* orange with black writing */

main(argc, argv)
int argc;
char *argv[];
{
  int fd, offset=0;
  char *ch;

  if(argc > 2)
  {
    if(strstr(argv[2], "clip"))
    {
      offset = 25;
    }
  }

  while(1)
  {
    gets(buf);

    if(strstr(buf,"--NewLog--"))
    {
      close(0);

      sleep(4);

      if(argv[1] != NULL)
      {
	if(( fd = open(argv[1], O_APPEND | O_WRONLY,0)) >=  0)
	{
	  if( write(fd,"\n",1) != 1)
	  {
	    perror("write");
	  }

	  close(fd);
	}
	else
        {
	  perror("open");
	}
      }

      exit(0);
    }

    for(ch=buf;*ch;ch++)			       /* replace \n with ' ' */
      if(*ch == '\n' || *ch == '\r')
        *ch = ' ';

    if(strstr(buf, "MSG_WARN"))
      printf("\007%s%s%s\n",warn_vid,  buf+offset, nrml_vid);
    else
    if(strstr(buf, "MSG_ERR"))
      printf("\007%s%s%s\n",error_vid, buf+offset, nrml_vid);
    else
    if(strstr(buf, "MSG_FATAL"))
      printf("\007%s%s%s\n",fatal_vid, buf+offset, nrml_vid);
    else
      printf("%s\n", buf+offset);
  }
}
