/* FORTRAN interface to some of the standard system/Cactus routines */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/timeb.h>

/* Sleep for the time specified (floating point seconds) */

void fsleep_(float time)
{
  usleep((int)(1.e6*time));
}

/* Return the time in seconds and milliseconds */

void ftime_(long *seconds, long *milliseconds)
{
  struct timeb tm;

  ftime(&tm);

  *seconds = tm.time;
  *milliseconds = tm.millitm;

}    


/* Open the log file */

void flog_open_(char *file, int *mode, int *siz)
{
  static char msg[1000];

//  if (siz > 1000)
//    siz = 1000 - 1;		/* Too big to fit */

/* Skip trailing blanks  */
//  while (siz > 0 && file[--siz] == ' ')
//    continue;

//  if (siz == 0 && file[0] == ' ')
//    return;			/* Skip blank messages */

  strncpy(msg, file, *siz);
  msg[*siz] = 0;			/* String terminator */
  log_open(msg, mode);
}


/* Log a message */

void flog_msg_(char *text, int siz)
{
  char msg[1000];

  if (siz > 1000)
    siz = 1000 - 1;		/* Too big to fit */

/* Skip trailing blanks  */
  while (siz > 0 && text[--siz] == ' ')
    continue;

  if (siz == 0 && text[0] == ' ')
    return;			/* Skip blank messages */

  strncpy(msg, text, ++siz);
  msg[siz] = 0;			/* String terminator */
  log_msg(msg);
}


/* Rotate the log files */

void flog_newlog_(void)
{
  log_newlog();
}
