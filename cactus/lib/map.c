#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <unistd.h>
#include "cactus_global.h"

struct cactus_global *cpr = NULL;

cactus_map(iswrite)
int iswrite;
{
  int fd = -1, sz, isrd, iswr;
  char c;
  void *map;
  char *name, *getenv();
  char *cp;

  cp = getenv("HOST");

  if(iswrite >= 0) /* Allow remote access to global in read only mode */
  {
    if(cp)
    {
      if(strcmp(cp, "corona"))
      {
        fprintf(stderr, "Process Can ONLY run on corona; Not %s\n", cp);
        exit(3);
      }
    }
//    iswrite = 0;
  }

  sz = 0;
  if( (name = getenv("CACTUS_GLOBAL"))== NULL )
    name = "/home/corona/cactus/CACTUS_GLOBAL";

  /* Try to open the file */

  if( access(name, F_OK)) { /* access failed, attempt to create */

    if( !iswrite )
      return(0);

    if(( fd = open( name, O_CREAT | O_RDWR, 0664 )) < 0)
      return(0);

    /* fill new file with zeros with unix lseek trick */

    lseek(fd, sizeof(struct cactus_global)-1, SEEK_SET );
    c = 0;
    write(fd, &c, 1 );

  } else {
    isrd = access(name, R_OK)==0;
    iswr = iswrite && access(name, W_OK)==0;
    if( iswr && isrd ) 
      fd = open(name, O_RDWR,0);
    else if( isrd )
      fd = open(name, O_RDONLY,0);

    if( fd < 0 )
      return(0);

    sz = lseek(fd, 0l, SEEK_END );
  }

  map = mmap(0, sizeof(struct cactus_global),
                PROT_READ | (iswr?PROT_WRITE:0), MAP_SHARED, fd, 0);

  if (map == NULL) {
    log_msg("global memory mapper could not map to %s\n", name);
    log_perror("mmap");
    return 0;
  }
  cpr = (struct cactus_global *) map;

  if( sz > 0 && sz != sizeof(struct cactus_global) ) 
  {
    char *pwd;

    if( !(pwd = getenv("PWD")))
      pwd = "no pwd";

    log_msg( "global memory mapper: %s has %d expected %d at home %s\n", 
      name, sz, sizeof(struct cactus_global), pwd);

    exit(3);
  }

  return (int) map;
}

