/*
 Cactus file @(#)man_ephem.c	1.4
        Date 11/10/92
*/
static char SccsID[] = "@(#)man_ephem.c	1.4\t11/10/92";

/*
 * MAN_EPHEM -- Manual ephemeris entry program:
 * Allows one to manually input a comet ephemeris.  The input is put into the 
 * form expected by Tracker-Servo for an ephemeris table.  Input is either 
 * from the keyboard or a log file.  When from the keyboard, all input is also 
 * written to a log file.  When input is from a log file and the end-of-file is 
 * reached, the user is given the option to continue input from the keyboard.  
 * If continued, the keyboard input is then appended to the log file.  
 *
 * The user is prompted for the root name of the output file.  ".eph" will be
 * added to the root name specified.  The output is written to the current
 * working directory.  This ought to be the same directory when the OBS user
 * puts his catalog files:
 *   		/home/obs/<initials>
 * where <initials> are the observer's initials.
 *
 * The user is prompted for the ephemeris table number in the Tracker-Servo
 * computer where the ephemeris will be stored.  This is a number from 10 to 20.
 * (The output file and/or the log file can be later edited to change this
 * number.)  The user is next prompted for the object name (no embedded blanks),
 * the first date and date increment for the ephemeris.  The date increment is
 * expressed in days, and may be less than one.  Usually, the user is then
 * prompted for the apparent R.A. hours, minutes, and seconds, 
 * Declination degrees, minutes, and seconds, the true geocentric distance (in 
 * A.U.), and the radial velocity (km/sec) for evenly spaced time intervals 
 * beginning at the first date.  (Input one line per date, with spaces
 * separating the numbers.)  The coordinates can either be geocentric or
 * topocentric.  If topocentric, specify a large number (999) for the
 * geocentric distance to disable the parallax correction.  The radial
 * velocity should be topocentric.  (If geocentric, it will be necessary to
 * select the geocentric velocity frame manually, each time Tracker is seeks
 * the comet.)
 */

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <time.h>

main()
{
    static char name[200], line[200], month[4], *mnames[12] = {"JAN", "FEB", 
	"MAR","APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"};
    static int daycnt[] = {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 
    									334};
    static int daymon[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 999};
    static char *kind[2] = {"w", "r"}, *word[2] = {"Writing", "Reading"};
    double millenium = 4078.;				/* Mar 1, 2000 */

    char *com, answer[2], inname[200], outname[200];
    FILE *infile, *outfile, *logfile;
    int i, j, k, azel, in, table, np, mon, day, year;
    float hour;
    double par[8], ra, dec, az, el, hpar, eptime, interval, t;
    time_t seconds;
    struct tm *date;

    printf("Name a file to receive a log of your input.  Preface the name \n");
    printf("with @ to read the input from a previous log file, instead.\n");

    while (1) {					/* Do until "break" */
	clearerr(stdin);
	printf("Log file :");
	if (fgets(line, 199, stdin) == NULL || 
    					sscanf(line, "%199s", inname) != 1)
	    continue;
	in = (inname[0] == '@');
	logfile = fopen(inname + in, kind[in]);
	if (logfile == NULL) {
	    printf("Could not open %s\n",inname + in);
	    continue;
	}
	printf("%s log file %s\n", word[in], inname + in);
	if (in) {
	    infile = logfile;
	    logfile = stdout;
	} else
	    infile = stdin;
	break;
    }

    printf("Name the file to receive the ephemeris output\n");	    

    while (1) {					/* Do until "break" */
	clearerr(infile);
	printf("Output file: ");
	if (fgets(line, 199, infile) == NULL || 
    					sscanf(line, "%199s", outname) != 1)
	    if (in) {
		printf("Could not read output file name from log file\n");
		exit();
	    } else
		continue;
	strcpy(outname + strlen(outname), ".eph");
	outfile = fopen(outname, "w");
	if (outfile == NULL) {
	    printf("Could not open %s\n", outname);
	    if (in)
		exit();
	} else {
	    fprintf(logfile, "%s\n", outname);	/* Put in log file */
	    break;
	}
    }						/* Try for output file again */

    while (1) {					/* Do until "break" */
	clearerr(infile);
	printf("Ephemeris table number (10-20): ");
	if (fgets(line, 199, infile) == NULL || 
    			    		sscanf(line, "%d", &table) != 1 || 
    					table < 10 || table > 20) {
	    if (in) {
		printf("Could not read table number from log file\n");
		exit();
	    } else
		continue;
	}
	break;
    }
    fprintf(logfile, "%d\n", table);

    while (1) {					/* Do until "break" */
	clearerr(infile);
	printf("Object name: ");
	if (fgets(line, 199, infile) == NULL || 
    					sscanf(line, "%199s", name) != 1) {
	    if (in) {
		printf("Could not read object name from log file\n");
		exit();
	    } else
		continue;
	}
	break;
    }
    fprintf(logfile, "%s\n", name);

    printf("Give the date of the first entry:\n");
    printf("day-of-month  3-letter-month-name  year  hours\n");
    printf("(If omitted, hours = 0.0 and year = current year)\n");

    while (1) {					/* Do until "break" */
	year = 0;				/* Set defaults */
	hour = 0.;
	clearerr(infile);
	printf("Date: ");
	if (fgets(line, 199, infile) == NULL || 
				sscanf(line, "%d %3c %n %d %f", &day, month, 
    							&i, &year, &hour) < 2) {
	    if (in) {
		printf("Could not read first date from log file\n");
		exit();
	    } else {
		printf("Try again, please\n");
		continue;
	    }
	}

	strupr(month);			/* Convert month name to upper case */
	for (mon=0; mon<12 && strcmp(mnames[mon],month); mon++) /* Find month */
	    continue;
	if (mon != 12) 
	    break;
	else 
	    if (in) {
		printf("Unknown month (%s) in first date from log file\n",
						month);
		exit();
	    } else
		printf("Unknown month\n");
    }						/* Try again */

    fprintf(logfile, "%s", line);

    if (year == 0 && line[i] != '\n' && line[i] != ' ')
	/* Try again for year, skipping possible extra month letters */
	sscanf(line + i, "%*s %d %f", &year, &hour);

    if (year <= 0) {
	/* Get current year */
	time(&seconds);				/* Seconds since Jan 1, 1970 */
	seconds += 7.*3600.;			/* Convert to GMT */
	date = localtime(&seconds);		/* Decode time */
	year = date->tm_year;			/* Get year */
    } else if (year > 200.)			/* (Breaks in year 2100 A.D.) */
	year -= 1900.;

    /* Get days since Jan 0, 1989 */
    eptime = daycnt[mon] + day + 365*(year-89) + (year-89)/4 + hour/24.;

    /* If "year" is a leap year & mon > Feb. */
    if (!(year % 4) && mon > 1) eptime += 1.;

    if (eptime > millenium) eptime -= 1.;	/* Year 2000 is not a leap year */

    while (1) {					/* Do until "break" */
	clearerr(infile);
	printf("Interval between entries (in days): ");
	if (fgets(line, 199, infile) == NULL || 
		sscanf(line, "%lf", &interval) != 1) {
	    if (in) {
		printf("Could not read entry interval from log file\n");
		exit();
	    } else {
		printf("Try again, please\n");
		continue;
	    }
	}
	break;
    }
    fprintf(logfile, "%f\n", interval);

    while (1) {					/* Do until "break" */
	clearerr(infile);
	printf("Are the coordinates apparent azimuth & elevation? (y/n): ");
	if (fgets(line, 199, infile) == NULL) {
	    if (in) {
		printf("Could not read entry interval from log file\n");
		exit();
	    } else {
		printf("Try again, please\n");
		continue;
	    }
	}
	if (line[0] == 'N' || line[0] == 'n')
	    azel = 0;
	else if (line[0] == 'Y' || line[0] == 'y')
	    azel = 1;
	else {
	    if (in) {
		printf("Error reading az-el response from log file\n");
		exit();
	    } else {
		printf("Try again, please\n");
		continue;
	    }
	}
	break;
    }
    fprintf(logfile, "%c\n", line[0]);

    printf("Enter coordinates one line per date (or -1 if done) as:\n");
    if (azel) {
	printf("d m s d m s v\n");
	printf("where \"d m s d m s\" is the apparent azimuth and elevation\n");
	printf("(each in degrees, minutes, and seconds)\n");
    } else {
	printf("h  m  s  d  m  s  r v\n");
	printf("where \"h m s\" is the apparent R.A. hours, minutes, and seconds;\n");
	printf("\"d m s\" is the declination degrees, minutes, and seconds;\n");
	printf("\"r\" is the true geocentric distance (in AU; optional).\n");
    }
    printf("and \"v\" is the radial velocity (in km/sec; optional)\n");

    /* End-of-file will also stop the program */

    /* Read in the input coordinates */
    for ( ; ; eptime += interval) {

	/* Convert eptime to ASCII */
	if (eptime > millenium)
	    t = eptime + 1.;		/* Make 2000 A.D. a leap year after all */
	else
	    t = eptime;
	year = t/(4.*365. + 1.);	/* No. of leap year cycles. */
	t -= year*(4.*365. + 1.);
	year *= 4;
	i = t/365.;		/* Remaining years */
	t -= i*365.;		/* Day-of-year + fraction */
	year += i + 1989;		/* Year */
	day = t;
	hour = (t-day)*24.;	/* Hour */
	if (!(year % 4) && year != 2000 && day > daycnt[2])
	    daymon[1] = 29;		/* It's a leap year and it's past Mar 1 */

	for (mon=0; day > daymon[mon]; day -= daymon[mon++])   /* Get the month */
	    continue;
	daymon[1] = 28;			/* Restore February */

	printf("%d %s %d %.1f H :\n", day, mnames[mon], year, hour);

	/* Get the input line for this date */

	/* Read next line */
	if (fgets(line, 199, infile) == NULL) {
	    if (!in)
		exit();
	    else {
		printf("End of log file\n");
		printf("Continue entry from keyboard or -1 if done\n");
		printf("%d %s %d %.1f H :\n", day, mnames[mon], year, hour);
		in = 0;
		fclose(infile);
		infile = stdin;
		do {
		    if (fgets(line, 199, infile) == NULL)
			exit();
		} while (sscanf(line, "%lf", &par[0]) != 1);
		if (par[0] == -1.)
		    exit();
		logfile = fopen(inname + 1, "a");	/* Append to log file */
		if (logfile != NULL) 
		    printf("(Appending keyboard input to %s)\n", inname+1);
		else {
		    logfile = stdout;
		    printf("Append to %s failed\n", inname+1);
		}
	    }
	}
	while (1) {				/* Do until "break" */

	    /* Interpret the input line */
	    com = strchr(line,'=');			/* Search for comment */
	    if (com != NULL && *(com+1) == '>') {
		*(com++) = '\n';			/* Erase comment */
		*com = '\0';
	    }
	    for (i=0; i < 5; i++)
		par[i] = 0.;		/* Default coordinates */
	    if (azel)
		par[6] = 0.;		/* Default radial velocity */
	    else {
		par[6] = 999999;	/* Default geocentric distance */
		par[7] = 0.;		/* Default radial velocity */
	    }
	    np = sscanf(line,"%lf%n %lf%n %lf%n %lf%n %lf%n %lf%n %lf%n %lf%n", 
			    &par[0], &k, &par[1], &k, &par[2], &j, 
			    &par[3], &k, &par[4], &k, &par[5], &k, 
			    &par[6], &k, &par[7], &k);
	    if (np <= 1 && par[0] == -1.)
		exit();

	    if (np == 3)
		k = j;		/* Point k to the ending character */

	    for ( ; k<strlen(line)-1 && isspace(line[k]); k++)
		continue;		/* Find next non-white char., if any */

	    if (np == 0 || !isspace(line[k]) || np > 8 || np < 4) {
		printf("Unrecognized input: %s\n", line);
		if (in)
		    exit();
		else {
		    do {
			printf("%d %s %d %.1f H :\n", day, mnames[mon], 
								year, hour);
		    } while (fgets(line, 199, infile) == NULL);
		    continue;		/* Interpret the new input */
		}
	    }
	    break;				/* Input OK */
	}
	line[strlen(line)-1] = '\0';	/* Get rid of \n */
	fprintf(logfile, "%s => %2d %s %4d %.2f H\n", line, day, month, 
							    year, hour);

	if (azel) {
	    /* Convert azimuth and elevation into degrees */
	    az = par[0] + par[1]/60. + par[2]/3600.;
	    el = par[3] + par[4]/60. + par[5]/3600.;
	    fprintf(outfile, "EPHEM %d 0 0 D %.4f D %.4f J %.6f 0 %.2f %s\n",
				table, az, el, eptime, par[6], name);
	} else {    
	    /* Convert R.A. and Dec. into degrees */
	    ra = par[0]*15. + par[1]/4. + par[2]/240.;
	    dec = fabs(par[3]) + par[4]/60. + par[5]/3600.;
	    /* Look for negative sign beginning at character after R.A. sec. */
	    sscanf(line+j, "%1s", answer);  
	    if (answer[0] == '-')
		dec = -dec;

	    /* Convert Horizontal Parallax into arcsec */
	    hpar = 8.794148/par[6];

	    fprintf(outfile, "EPHEM %d D %.4f D %.4f J %.6f %.2f %.2f %s\n",
				table, ra, dec, eptime, hpar, par[7], name);
	}
	name[0] = '\0';			/* Name first entry only */
    }					/* Bump "eptime" and get more input */
}
