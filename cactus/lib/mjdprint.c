#include <stdio.h>
#include <string.h>

#include "timeLib.h"

char *months[] = { "Jan","Feb","Mar","Apr","May","Jun",
                   "Jul","Aug","Sep","Oct","Nov","Dec" };

main(argc,argv)
int argc;
char *argv[];
{
  char *cp;
  int yr, mo, day, hr, min, sec;
  double jd, atof();

  if(argc<2)
    return(1);

  jd = atof(argv[1]);
  breakmJulian(jd, &yr, &mo, &day, &hr, &min, &sec);
  printf("%2.2d:%2.2d:%2.2d GMT %s %2d %d\n",
                hr, min, sec, months[mo-1], day, yr+2000);
  return(0);
}
