/*
 * Cactus File @(#)gs_end.c	1.2
 *         SID 1.2
 *        Date 8/6/96
 */

};

/* Number of elements in structure gs[] */
int NGS = sizeof(gs)/sizeof(gs[0]);

void gs_init()
{
    int i;
    static int once = 0;
    extern struct cactus_global *cpr;

    if( once )
      return;
    else
      once = 1;

    /* Initialize the pointers into the Global Section */

    for (i=0; i<NGS; i++)
        gs[i].offset = (int)gs[i].offset + (char *)cpr;
    return;
}


