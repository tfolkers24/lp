/*
 * Cactus File @(#)fsock.c	1.3
 *         SID 1.3
 *        Date 10/08/07
 */
static char SccsId[] = "@(#)fsock.c	1.3\t10/08/07";

/* Layer for FORTRAN access to the socket library, socklib
 *
 * Also containes backend_codes(), a FORTRAN hook into the backend codes of
 * spec.h
 */

#include <string.h>
#define NO_MODE_TABLE

void fsock_bind_(char *name, int *ret, int len)
{
    char mbname[80+1];

    len = (len > 80) ? 80 : len;
    strncpy(mbname, name, len);
    mbname[len] = 0;
    *ret = sock_bind(mbname);
    return;
}

void fsock_connect_(char *name, int *ret, int len)
{
    char mbname[80+1];

    len = (len > 80) ? 80 : len;
    strncpy(mbname, name, len);
    mbname[len] = 0;
    *ret = sock_connect(mbname);
    return;
}

void fsock_send_(int *s, char *msg, int *ret, int len)
{
    char mbmsg[800+1];

    len = (len > 800) ? 800 : len;
    strncpy(mbmsg, msg, len);
    mbmsg[len] = 0;
    *ret = sock_send(*s, mbmsg);
    return;
}

void fsock_write_(int *s, char *msg, int *num, int *ret, int len)
{
    *ret = sock_write(*s, msg, *num);
    return;
}

void fsock_ssel_(int **ss, int *ns, char *msg, int *len, 
    		int *p, int *n, int *tim, int *rd_in, int *ret, int siz)
{
    char buffer[4096];

    if (siz < 4096) {
/* 	Put message into buffer first in case msg is too small */
	*ret = sock_ssel(ss, *ns, buffer, len, p, *n, *tim, *rd_in);
	*len = (*len > siz) ? siz : *len;
	if (*ret >= 0)
	    memcpy(msg, buffer, *len);
    } else
	*ret = sock_ssel(ss, *ns, msg, len, p, *n, *tim, *rd_in);
}

void fsock_sel_(char *msg, int *len, int *p, int *n,
    		int *tim, int *rd_in, int *ret, int siz)
{
    int zero = {0};

    fsock_ssel_(NULL, &zero, msg, len, p, n, tim, rd_in, ret, siz);
}

void fsock_only_(int *s, char *msg, int *tim, int *ret, int siz)
{
    char buffer[9000];
    int len;

    len = sock_only(*s, buffer, *tim);

    if (len > siz)		/* Limit the return to the size of the buffer */
      len = siz;

    if (len > 0)
      memcpy(msg, buffer, len);

    *ret = len;
}


void fsock_only_f_(int *s, char *msg, double *tim, int *ret, int siz)
{
    char buffer[9000];
    int len;

    len = sock_only_f(*s, buffer, *tim);

    if (len > siz)
      len = siz;

    if (len > 0)
      memcpy(msg, buffer, len);

    *ret = len;
}


void fsock_close_(char *name, int len)
{
    char buffer[80+1];

    if (len <= 0)
	sock_close(NULL);
    else {
/*	Add terminator to "name" */
	len = (len > 80) ? 80 : len;
	strncpy(buffer, name, len);
	buffer[len] = 0;
	sock_close(buffer);
    }
}

char *sock_name(int);

void fsock_name_(int *s, char *name, int len)
{
    strncpy(name, sock_name(*s), len);
}

int last_msg_(void)
{
    return last_msg();
}
