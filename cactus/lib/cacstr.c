/*
 *  Cactus file %W%
 *        Date %G%
 *
 */
#ifndef lint

static char SccsID[] = "%W%\t%G%";

#endif

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>

#define	NO_EXTERNS

#include "cactus.h"
#include "sex.h"

#include "caclib_proto.h"

struct SOCK *super;
int interactiveSession = 0;

char *cactus_home        = "/home/corona/cactus";
char *cactus_global      = "/home/corona/cactus/CACTUS_GLOBAL";
char *cactus_mailboxdefs = "/home/corona/cactus/mailboxdefs";
char *cactus_wind_memory = "/home/corona/cactus/monitor/WIND_MEMORY";
char *cactus_isc_memory  = "/home/corona/cactus/monitor/ISC_MEMORY";

/* CACSTR.C -- Assorted CACTUS string handling routines. */


/* setCactusEnvironment()
 *
 * Set the standard cactus string variables for use
 * by programs who care to use them. 
 * i.e. all of them eventually. ;-)
 *
 * Must be called before and thing else.
 *
 */
int setCactusEnvironment()
{
  char *cp;

  if((cp = getenv("CACTUSHOME")))
    cactus_home = cp;

  if((cp = getenv("CACTUS_GLOBAL")))
    cactus_global = cp;

  if((cp = getenv("MAILBOXDEFS")))
    cactus_mailboxdefs = cp;

  return(0);
}


/* Return 0 if interactive terminal session */
int getInteractive()
{
  int log_how;
  pid_t tgrp, pgrp;

  char *cp;

  cp = getenv("TERM");

  if(cp)
  {
    pgrp = getpgrp();
    tgrp = tcgetpgrp(STDOUT_FILENO);

//    fprintf(stdout, "TERM = %s\n", cp);
//    fprintf(stdout, "getpgrp = %d\n", pgrp);
//    fprintf(stdout, "tcgetpgrp = %d\n", tgrp);

    if(pgrp == tgrp)
    {
      log_how = 0;
      interactiveSession = 1;
    }
    else
    {
      log_how = 1;
      interactiveSession = 0;
    }
  }
  else
  {
    log_how = 1;
  }

  return(log_how);
}



int smart_log_open(name, offset)
char *name;
int offset;
{
  int log_how;

  log_how = getInteractive() + offset;

  log_open(name, log_how);

  log_msg("Smart Logging Set to: %d", log_how);
  log_msg("interactiveSession:   %d", interactiveSession);

  return(log_how);
}


int f_get_cactus_env__()
{
  setCactusEnvironment();

  return(0);
}
  

/* Determines if the instance of the process in owned by 'user' and
   is being displayed on 'display'. If so return True. 

   Used to determine if this program should be the one to send
   PROCESS_TIMES to the super-visor process. A lot of programs can
   be run multiply times and only one of them should report in. 
 */

int iamwho(user, display)
char *user, *display;
{
  char *cp1, *cp2;

  log_msg("iamwho(%s, %s)", user, display);

  cp1 = getenv("USER");
  cp2 = getenv("DISPLAY");
  
  log_msg("iamwho(): USER = %s", cp1);
  log_msg("iamwho(): DISPLAY = %s", cp2);

  if(cp1)
  {
    if(!strncmp(cp1, user, strlen(user)))
    {
      if(cp2)
      {
        if(!strncmp(cp2, display, strlen(display)))
        {
	  log_msg("iamwho(): Returns True");
          return(1);
        }
      }
    }
  }

  log_msg("iamwho(): Returns False");

  return(0);
}


/* GETNTOK -- Get the Nth token in a string.  Leading white space is ignored.
 * The token delimiter is specified in the call.  The number of characters in
 * the token is returned in the function value.
 *
 *		int
 *		getntok(ntok, dlm, str, token)
 *
 *		int	ntok	token number desired
 *		char	*dlm	token delimiter(s)
 *		char	*str	input string
 *		char	*token	returned token
 */

int getntok(ntok, dlm, str, token)
int     ntok;           /* token number */
char    *dlm;           /* token delimiters */
char    *str;           /* input string */
char    *token;         /* result */
{
    int     nt, pd, pnt=0;

    while(isspace(str[pnt]))
        pnt++;

    token[0] = 0;
    pd = pnt + strcspn(&str[pnt], dlm);

    for(nt=0;(nt < ntok); nt++) {     /* Find Nth delimiter */
        pnt = pd + strspn(&str[pd], dlm);
        pd = pnt + strcspn(&str[pnt], dlm);
    }

    strncpy(token, &str[pnt], pd-pnt);
    token[pd-pnt] = 0;
    return(strlen(token));
}


/* STRELEM -- Get the Nth token in a string.  Leading white space is ignored.
 * The token delimiter is specified in the call.
 *
 * This is the C language emulation of the DCL f$element lexical function.
 *
 *		char *
 *		strelem(ntok, dlm, str)
 *
 *		int	ntok	token number desired
 *		char	*dlm	token delimiter(s)
 *		char	*str	input string
 */

char *strelem(ntok, dlm, str)
int     ntok;           /* token number */
char    *dlm;           /* token delimiter(s) */
char    *str;           /* input string */
{
    int		nt, pd, pnt=0;
    static	char	token[SZ_LINE];
    
    while(isspace(str[pnt]))
        pnt++;

    token[0] = 0;
    pd = pnt + strcspn(&str[pnt], dlm);

    for(nt=0;(nt < ntok); nt++) {     /* Find Nth delimiter */
        pnt = pd + strspn(&str[pd], dlm);
        pd = pnt + strcspn(&str[pnt], dlm);
    }

    strncpy(token, &str[pnt], pd-pnt);
    token[pd-pnt] = 0;
    return(token);
}


/* STRCMPRS -- Compress all white spaces in a string to single spaces.
 * Leading and trailing white space is removed from the string.  The
 * function returns the resulting compressed string.
 *
 * This is the C emulation of the DCL f$edit(,"compress") lexical function.
 *
 *		char *
 *		strcmprs(str)
 *
 *		char	*str	input string
 */

char *strcmprs(str)
char *str;           /* input string */
{
    register char   *ip;
    register char   *op;
    int     whiteout;

    for(ip=str; isspace(*ip); ip++)    /* Remove leading white */
        ;
    whiteout=NO;

    for(op=str;(*op = *ip); ip++, op++)
        if(isspace(*ip))
        if(whiteout)
            op--;
        else {
            *op = ' ';
            whiteout = YES;
        }
        else
        whiteout = NO;

    if(whiteout)               /* Remove trailing white */
        op--;

    *op = 0;

    return(str);
}


/* deNewLineIt() Replace all newlines and returns with spaces */
int deNewLineIt(buf)
char *buf;
{
  char *cp;

  for(cp=buf;*cp;cp++)
  {
    if( *cp == '\n' || *cp == '\r')
    {
      *cp = ' ';
    }
  }

  return(0);
}


/* CTOD -- Convert ASCII character string to double, including sexagesimal.
 *
 *		double
 *		ctod(str)
 *
 *		char    *str	input string
 *
 * Sexagesimal numbers are identified as strings containing colons, and 0, 1
 * or 2 "significant" colons are permissible.  A "significant" colon is one
 * that has a placeholder string in front of or behind it.  A sexigesimal
 * number has from 1 to 3 fields, and they are presumed to accumulate from
 * the least to most significant.
 */

double ctod(str)
char    *str;           /* input string */
{
	char	strloc[SZ_LINE+1];
	
	strcpy(strloc, str);
	if(strchr(strloc, ':')) {	/* sexagesimal - parse here */
		char	*tok;
		double	fld[3], sexnum;
		int	i = -1;
		int	neg=NO;

		tok = strtok(strloc, ":");
		while(tok && ++i<3) {
			fld[i] = atof(tok);
			if(fld[i] <(double) 0)
				neg = YES;
			fld[i] = fabs(fld[i]);
			tok = strtok(NULL, ":");
		}

		switch(i) {
		case 0:
			sexnum = fld[0] /(double) 3600;
			break;
		case 1:
			sexnum = fld[0] /(double) 60 + fld[1] /(double) 3600;
			break;
		case 2:
			sexnum = fld[0] + fld[1] /(double) 60 + fld[2] /(double) 3600;
			break;
		default:
			fprintf(stderr, "ctod: invalid sexagesimal number %s\n", str);
			return((double) 0);
		}

		if(neg)
			sexnum = -sexnum;

		return((double) sexnum);

	} else				/* not sexagesimal - use atof() */

		return((double) atof(strloc));
}


#define DTOMA   3600000

/* CTOMA -- Convert ASCII character string to long integer milliarcsec.
 *
 *		long
 *		ctoma(str)
 *
 *		char    *str	input string
 */

long ctoma(str)
char    *str;
{
    double  val, ctod();

    val = ctod(str) *(double) DTOMA;

    return((long) val);
}

/* STRLWR -- Convert string to lower case.
 *
 *		char *
 *		strlwr(str)
 *
 *		char *str	input string
 */

char *strlwr(str)
char *str;
{
	register char   *ip;
	
	ip = str;
	while(*ip) {
	    if(isupper(*ip))
		*ip = tolower(*ip);
	    *ip++;
	}

	return((char *) str);
}


/* STRUPR -- Convert string to upper case.
 *
 *		char *
 *		strupr(str)
 *
 *		char *str	input string
 */

char *strupr(str)
char *str;
{
	register char   *ip;

	ip = str;	
	while(*ip) {
	    if(islower(*ip))
		*ip = toupper(*ip);
	    *ip++;
	}

	return((char *) str);
}


/* STRBIN --
 * Return a string that is the binary representation of the(long) argument
 */
char *strbin(n)
long n;
{
    static char bits[32];
    int i, j;

    for(i=0x80000000, j=0; i; i =(i >> 1) & 0x7FFFFFFF)
	bits[j++] =(n & i) ? '1' : '0';

    return(char *)&bits;
}




/* SEXAGESIMAL -- converts double to xx:xx[:xx.xx]
 */
int sexagesimal(dang,str,prec)
double dang;
char *str;
char prec;
{
  int bigi,degi,mini,
      seci,sec100,neg=0,i;
  char buf[512];

  if(dang < 0.0)
    neg = 1;

  dang = fabs(dang);

  switch(prec)
  {
    case MMMM_SS:
    case IGNORE_EXP:
    case MMM_SS_OVER:
    case DDDD_MM_SS:
//	dang += 0.0001388;				  /* add 1/2 a second */
	break;
    case DDDD_MM:
    case HH_MM:
//	dang += 0.0083334;				  /* add 1/2 a minute */
	break;
    case SSS_S100:
    case MMM_SS_S100:
    case DDDD_MM_SS_S100:
    case DDD_MM_SS_S100:
    case DD_MM_SS:
    case HH_MM_SS:
    case DDDD_MM_SS_SS100:
//	dang += 0.00001388;		    /* add 1/2 of 1 tenth of a second */
	break;
  }

  bigi = (int)(dang * 360000);		       /* convert to integer math now */
  degi = bigi / 360000;
  bigi %= 360000;
  mini = bigi / 6000;
  bigi %= 6000;
  seci = bigi / 100;
  sec100 = bigi % 100;

  switch(prec)
  {
    case SSS_S100 :					    /* seconds.sec100 */
      sprintf(buf,"%3d.%1.0f",seci,(float)sec100 / 10);
      break;
      
    case MMMM_SS :						   /* MMMM:SS */
     if(degi>0) 
       mini += degi * 60; 
     sprintf(buf,"%4d:%02d",mini,seci);
     break;

    case MMM_SS_OVER :		     /* MM:SS or 60:00 if greater thern 60:00 */
     sprintf(buf,"%3d:%02d",mini,seci);
     if(degi>0) 
       strcpy(buf," 60:00"); 
     break;

    case IGNORE_EXP :		   /* MM:SS or 'IGNORE' if greater then 60:00 */
     sprintf(buf,"%3d:%02d",mini,seci);
       if(degi>0) strcpy(buf,"IGNORE"); 
     break;

    case DDDD_MM :						   /* +DDD:MM */
      sprintf(buf,"%4d:%02d",degi,mini);
      break;
                                              
    case HH_MM :						     /* DD:MM */
      sprintf(buf,"%2d:%02d",degi,mini);
      break;
                                              
    case MMM_SS_S100 :						  /* MMM:SS.S */
      sprintf(buf,"%3d:%02d.%1.1d",mini,seci,sec100);
      break;

    case DDDD_MM_SS :						/* +DDD:MM:SS */
      sprintf(buf,"%4d:%02d:%02d",degi,mini,seci);
      break;

    case DD_MM_SS :				      /* HH:MM:SS or DD:MM:SS */
    case HH_MM_SS :				      /* HH:MM:SS or DD:MM:SS */
      sprintf(buf," %2d:%02d:%02d",degi,mini,seci);
      break;

    case DDDD_MM_SS_S100 :				      /* +DDD:MM:SS.s */
      sprintf(buf,"%4d:%02d:%02d.%d",degi,mini,seci,sec100 / 10);
      break;


    case DDD_MM_SS_S100 :				       /* +DD:MM:SS.s */
      sprintf(buf,"%3d:%02d:%02d.%d",degi,mini,seci,sec100 / 10);
      break;

    case DDDD_MM_SS_SS100 :				     /* +DDD:MM:SS.SS */
      sprintf(buf,"%4d:%02d:%02d.%02d",degi,mini,seci,sec100);
      break;
  }

  if(neg)                                           /* add negitive sign here */
  {
    int n = strlen(buf);

    for(i=0;i<n;i++)
    {
      if(isdigit(buf[i]))
      {
	if(i>0)
	{
          buf[i-1] = '-';
          sprintf(str, "%s",  buf);
          break;
	}
	else
          sprintf(str, "-%s", buf);
      }
    }
  }
  else
    sprintf(str, "%s", buf);
}


/* GET_TIME --  Returns a char string like '12:34:56'. 
 */
char *get_time()
{
  float timed;
  static char timestr[128];

  struct timeval tvv,*tp;   			  /* holds time in SUN format */
  struct tm *tmnow;
  long sec, usec;
  int hour, minutes, seconds;

  tp = &tvv;
  gettimeofday(tp,0);
  sec = tp->tv_sec + 25200; 			  /* add offset for u.t. time */
  usec = tp->tv_usec;
  tmnow = localtime(&sec);

  hour = tmnow->tm_hour;
  minutes = tmnow->tm_min;
  seconds = tmnow->tm_sec;

  timed = hour 
	+ minutes / 60.0 
	+ seconds / 3600.0 
	+ usec / 3600000000.0;
    
  sexagesimal((double)timed,timestr,DD_MM_SS);
  return(timestr);
}

/* Return non-zero if "string" only contains characters that could be a number:
 * digits, up to one decimal point, optional leading + or - sign.  
 * A single E is allowed if it is followed by an integer.  Returns 1 if 
 * string is an integer, 2 if string has a decimal point, 3 if string is an
 * E-format number.  Otherwise 0 is returned.
 * N.B. string should not contain leading or trailing blanks.
 */

int cactus_isnumber(string)
char *string;
{
  char *digits = "0123456789";
  int n, len;

  if (string[0] == '+' || string[0] == '-')
    string++;					/* Skip over the sign */

  n = strspn(string, digits);
  if (string[n] == '\0')
    return 1;					/* Only digits were found */

  if (string[n] == '.')				/* Found a decimal point */
    n += strspn(string+(++n), digits);		/* More digits can follow */

  if (string[n] == '\0')
    return 2;					/* A decimal number was found */

  if (string[n] != 'e' && string[n] != 'E')
    return 0;					/* Not a number */
  
  n++;						/* letter E was found */
  if (string[n] == '+' || string[n] == '-')
    n++;					/* Exponent has a sign */

  if (string[n] == '\0')
    return 0;					/* Bad: No exponent value */

  n += strspn(string+n, digits);
  if (string[n] == '\0')
    return 3;					/* E-format number was found */
  else
    return 0;					/* Not a number */
}


/* Returns 1 if string only contains digits or the letters a-f.
 * Returns 0 if other characters are found.
 * N.B. string should not contain leading or trailing blanks.
 */

int ishex(string)
char *string;
{
  char *hexdigits = "0123456789abcdefABCDEF";
  int n;

  n = strspn(string, hexdigits);

  return (string[n] == '\0');

}


/*
 * Convert hexadecimal string <s> to an integer
 * (If <s> is not a hexadecimal number, 0 is returned)
 */

int atox(s)
char *s;
{
  int d = 0;

  sscanf(s, "%x", &d );

  return(d);
}


int sendProcessTime(indx, t, pid)
int indx, t, pid;
{
  char tbuf[256];

  if(!indx)
  {
    return(0);
  }

  sprintf(tbuf, "PROCESS_TIME %d %d %d", indx, t, pid);

  if(!super)
  {
    super = sock_connect("SUPER");
  }

  return(sock_send(super, tbuf));
}



int checkSendProc(id, pid)
int id, pid;
{
  static time_t old_time = 0;
  time_t t;

  t = time(NULL);

  if(abs(t - old_time) > 10)
  {
    sendProcessTime(id, t, pid);

    old_time = t;

    return(1);
  }

  return(0);
}



int send_process_info_internal(indx, iNum, iBuf)
int indx, iNum;
char *iBuf;
{
  char tbuf[4096];

  if(!indx)
  {
    return(0);
  }

  snprintf(tbuf, sizeof(tbuf), "PROCESS_INFO %3d %5d %s", indx, iNum, iBuf);

  if(!super)
  {
    super = sock_connect("SUPER");
  }

  return(sock_write(super, tbuf, strlen(tbuf)));
}


int sendProcessInfo(indx, pid, type)
int indx, pid, type;
{
  FILE *fp;
  char bigAssBuf[4096], tbuf[4096];
  char filename[256];

  if(type == PROCESS_INFO_STATUS)
  {
    sprintf(filename, "cat /proc/%d/status", pid);

    if((fp = popen(filename, "r") ) <= (FILE *)0 )
    {
      log_msg("Unable to %s", filename);
      return(1);
    }

    sprintf(bigAssBuf, "/proc/%d/status\n", pid);

    while(fgets(tbuf, sizeof(tbuf), fp) != NULL)
    {
      strcat(bigAssBuf, tbuf);
    }

    pclose(fp);
  }

/* Big Ass Buffer size arounf 750 bytes */

  if(type == PROCESS_INFO_STATUS)
  {
    send_process_info_internal(indx, type, bigAssBuf);
  }

  return(0);
}

