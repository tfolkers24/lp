/*
 *  Cactus file @(#)tailtool.c	1.4
 *        Date 10/08/07
 *
 */
#ifndef lint

static char SccsID[] = "@(#)tailtool.c	1.4\t10/08/07";

#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/file.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>


FILE *fp;
struct stat statb;
int newlog=0, fileSize=0, offset=0;
char filename[512];

char *nrml_vid  = "\033[m";
char *bold_vid  = "\033[1m";
char *unsc_vid  = "\033[4m";
char *rvrs_vid  = "\033[7m";
char *fatal_vid = "\033[40;31;1m";                  /* red with black writing */
char *error_vid = "\033[40;33;1m";               /* yellow with black writing */
char *warn_vid  = "\033[40;36;1m";               /* orange with black writing */

int clip=0;

int main(argc,argv)
int argc;
char *argv[];
{
  if(argc > 1)
  {
    sprintf(filename, "/home/corona/cactus/%s/%s.log", argv[1], argv[1]);
    if(argc > 2)
      offset = 25;

    if(strcmp(argv[1], "monitor") == 0)
      clip = 1;

    openFile(filename);
    while(1)
    {
      one_second();
      sleep(1);
    }
  }

  exit(0);
}


int openFile(f)
char *f;
{
  if( (fp = fopen(f, "r") ) <= (FILE *)0 )
  {
    fprintf(stderr, "tailtool: unable to open %s\n", f);
    exit(1);
  }

  (void)fstat(fileno(fp),&statb);
  fileSize = statb.st_size;

  if(fileSize > 5000)
  {
    fileSize -= 5000;
  }
  else
  if(fileSize > 4000)
  {
    fileSize -= 4000;
  }
  else
  if(fileSize > 3000)
  {
    fileSize -= 3000;
  }
  else
  if(fileSize > 2000)
  {
    fileSize -= 2000;
  }
  else
  if(fileSize > 1000)
  {
    fileSize -= 1000;
  }
  else
  {
    fileSize = 0;
  }

  return(0);
}


int one_second()
{
  char line[512];
  int  fsize, l;
  static int first = 1;

  if(fp)
  {
    (void)fstat(fileno(fp),&statb);

    if(newlog)
    {
      newlog = 0;
      printf("\nclosing file %s\n", filename);
      fclose(fp);
      sleep(5);
      openFile(filename);
      fileSize  = 0L;
      one_second();
    }
    if(statb.st_size > fileSize)
    {
      fseek(fp, fileSize,0);
      fsize = statb.st_size - fileSize;
      while(fsize > 10)
      {  
        if(!fgets(line, 512, fp))
        { 
          fprintf(stderr, "tailtool: Error in read\n");
          return;
        }
        l = strlen(line);
        fsize -= l; 
        if(first) /* don't print first garbage line */
        {
          first = 0;
          continue;
        }
        if(clip)
          printLine(line+24, l);
	else
          printLine(line, l);
      }
      fileSize = statb.st_size;
      fflush(stdout);
    }
  }
}


int printLine(line, l)
char *line;
int l;
{
  char *cp, buf[512];

  if(strstr(line, "--NewLog--"))
  {
    newlog = 1;
  }

  for(cp=line;*cp;cp++)                                /* replace \n with ' ' */
  {
    if(*cp == '\n' || *cp == '\r')
    {
      *cp = ' ';
    }
  }

  if(strstr(line, "MSG_WARN"))
  {
    sprintf(buf, "\007%s%s%s\n",warn_vid,  line+offset, nrml_vid);
  }
  else
  if(strstr(line, "MSG_ERR"))
  {
    sprintf(buf, "\007%s%s%s\n",error_vid, line+offset, nrml_vid);
  }
  else
  if(strstr(line, "MSG_FATAL"))
  {
    sprintf(buf, "\007%s%s%s\n",fatal_vid, line+offset, nrml_vid);
  }
  else
  {
    sprintf(buf, "%s\n", line+offset);
  }

  printf(buf);

  return(0);
}
