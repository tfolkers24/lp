
/*
 * Cactus File @(#)muxwatch.c	1.5
 *         SID 1.5
 *        Date 5/11/94
 */
static char SccsId[] = "@(#)muxwatch.c	1.5\t5/11/94";

/*
 * MUXWATCH.C -- Catus Dmux monitor program. 
 */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <sys/fcntl.h>
#include <sys/termios.h>
#include <sys/termio.h>
#include <sys/types.h>
#include <sys/time.h>

/*
#include "samioctl.h"
 */


#define NORM    1
#define RED     2
#define GREEN   3
#define RED_C   4
#define GREEN_C 5
#define BLANK   6
#define YES     1
#define NO      0
#define RAW     1
#define COOKED  0

#define TEXT_Y  14
#define INMODE  1
#define OUTMODE -1


void cntrc_handler();
int set_kb(int);

int istat = 0,
    mode = INMODE,
    muxchan = 0,
    reset = 1;


struct field_names {
  char *text_str;
  char *lo_str;
  char *hi_str;
};


struct field_names in_screen[] = {
  "     NOT USED           ",	"   LOW    ",	"   HIGH   ",
  "0:   VANE POSITION SEL  ",	"          ",	"   SKY   ",
  "1:   VANE POSITION SEL  ",	"          ",	"   LOAD   ",
  "2:   FREQUENCY SWITCH   ",	"          ",	"   REF   ",
  "3:   FREQUENCY SWITCH   ",	"          ",	"   SIG   ",
  "4:   NOT USED           ",	"   LOW    ",	"   HIGH   ",
  "5:   NOT USED           ",	"   LOW    ",	"   HIGH   ",
  "6:   SUB-REFLECTOR      ",	"  LOCKED  ",	"  NUTATE  ",
  "7:   N-S TRANSLATION    ",	"  IN TOL  ",	"OUT OF TOL",
  "8:   NOT USED           ",	"   LOW    ",	"   HIGH   ",
  "9:   NOT USED           ",	"   LOW    ",	"   HIGH   ",
  "10:  NOT USED           ",	"   LOW    ",	"   HIGH   ",
  "11:  NOT USED           ",	"   LOW    ",	"   HIGH   ",
  "12:  NOT USED           ",	"   LOW    ",	"   HIGH   ",
  "13:  NOT USED           ",	"   LOW    ",	"   HIGH   ",
  "14:  NOT USED           ",	"   LOW    ",	"   HIGH   ",
  "15:  NOT USED           ",	"   LOW    ",	"   HIGH   "
};



struct field_names out_screen[] = {
  "     NOT USED           ",	"   LOW    ",	"   HIGH   ",
  "0:   VANE POSITION      ",	"   LOAD   ",	"   SKY    ",
  "1:   SUB_REF NUTATE SRC ",	"   DBE    ",	"   FBMUX  ",
  "2:   HY-SPEC SIG/REF BIN",	"   REF    ",	"   SIG   ",
  "3:   NOT USED           ",	"   LOW    ",	"   HIGH   ",
  "4:   HY-SPEC VALID DATA ",	" INVALID  ",	"   VALID  ",
  "5:   FREQ COUNTER INPUT ",	"   LOW    ",	"   HIGH   ",
  "6:   00=Sig 10=Ref 01=2GHZ",	"   LOW    ",	"   HIGH   ",
  "7:   FREQ COUNTER ENABLE",	"   LOW    ",	"   HIGH   ",
  "8:   NOT USED           ",	"   LOW    ",	"   HIGH   ",
  "9:   NOT USED           ",	"   LOW    ",	"   HIGH   ",
  "10:  NOT USED           ",	"   LOW    ",	"   HIGH   ",
  "11:  NOT USED           ",	"   LOW    ",	"   HIGH   ",
  "12:  NOT USED           ",	"   LOW    ",	"   HIGH   ",
  "13:  NOT USED           ",	"   LOW    ",	"   HIGH   ",
  "14:  NOT USED           ",	"   LOW    ",	"   HIGH   ",
  "15:  NOT USED           ",	"   LOW    ",	"   HIGH   "
};



main()
{
  char ch,buf[256];
  unsigned int out_pat,mask = 0xffff;
  int i=0,ret,width=16;
  fd_set rfd;
  struct timeval tm;


  signal(SIGINT,cntrc_handler);                   /* set up control-c catcher */

  if((istat = dxopen(&muxchan)) != 0) 
  {
    printf("Could not open MUX driver!!!. Exiting."); 
    exit(1);
  }


  set_kb(RAW);
  mode = INMODE;
  put_header();
  put_text();
  while(1)
  {
    FD_ZERO(&rfd);
    FD_SET(0,&rfd);
    tm.tv_sec = 1;
    tm.tv_usec = 0;
  
    ret = select(width,&rfd,NULL,NULL,&tm);

    if(ret < 0) 
    {
      perror("select");
      continue;
    } 
    else 
    if(ret == 0)					  /* do timeout stuff */
    {
      if(mode == INMODE)
        display_inbits();
      else
        display_outbits();
      fflush(stdout);
    }
    else
    if(FD_ISSET(0,&rfd))				   /* got a keystroke */
    {
      ch = toupper(getchar());
      if(ch == 'T')
      {
        mode *= -1;
        reset++;
        put_header();
        put_text();
      }
      else
      if((ch == 'O') && (mode == OUTMODE))
      {
        set_kb(COOKED);
        do
        {
	  gotoxy(1,23);
	  printf("Enter OUTPUT bit pattern [mask] -> ");
	  gets(buf);
	  i = sscanf(buf,"%x %x",&out_pat,&mask);

	  if(i == 1)
	    dxwrite(muxchan,0xffff,out_pat);
	  else
	  if(i == 2)
	    dxwrite(muxchan,mask,out_pat);
	  else

	    i = 0;

	  gotoxy(1,23);
	  printf("                                              ");
        }while(!i);
        set_kb(RAW);
      }
    }
  }
}





put_header()
{
  reset_vt100();
  gotoxy(1,1); printf("\033#6>>>>>>>>>>>>>> MUX WATCH <<<<<<<<<<<<<<<");
  set_scroll(3,24);          /* set scrolling area so header won't scroll off */
  gotoxy(34,2); 
  if(mode == INMODE)
    printf("-> INPUT <-");
  else
    printf("-> OUTPUT <-");
  gotoxy(38,3); printf("BITS");
  gotoxy(4,4);  printf("15   14   13   12   11   10    9   8");
  gotoxy(44,4); printf("7    6    5    4    3    2    1    0");
  gotoxy(37,5); printf("WEIGHT");
  gotoxy(4,6);  printf("8    4    2    1    8    4    2    1");
  gotoxy(44,6); printf("8    4    2    1    8    4    2    1");
  gotoxy(1,8);  printf("HI");
  gotoxy(1,10); printf("LO");
  gotoxy(54,23);printf("MUX BUS PATERN->");
  gotoxy(1,11); printf("========================================");
  gotoxy(41,11);printf("========================================");
}




int put_text()
{
  int i;
  struct field_names *screen;

  if(mode == INMODE)
    screen = in_screen;
  else
    screen = out_screen;

  gotoxy(1,TEXT_Y - 1);
  printf("BIT         PURPOSE          STATE\n");
  gotoxy(40,TEXT_Y - 1);
  printf("BIT          PURPOSE          STATE\n");

  for(i=1;i<17;i++)
  {
    if(i<9)
      gotoxy(1,TEXT_Y +i -1);
    else
      gotoxy(40,TEXT_Y +i -9);

    printf(screen[i].text_str);
  }
}





/* DISPLAY_INBITS() -- Display bit patern of Dmux bus input port. 
 *
 */
int display_inbits()
{
  unsigned short int si;
  int i;

  unsigned short int chg_patrn;
  static unsigned short int old_patrn = 0, patrn = 0;

  if(istat= dxread(muxchan,&patrn))
  {
    printf("Error reading muxbus.");
    exit(1);
  }

  if(reset)
  {
    old_patrn = ~patrn;
    reset = 0;
  }

  if(patrn != old_patrn)
  {
    gotoxy(73,23);
    printf("%X",patrn);
    chg_patrn = old_patrn ^ patrn;
    for(i=0;i<16;i++)
    {
      if(chg_patrn & (1 << i))
      {
        if(patrn & (1 << i))
	{
          set_field((79 - (i*5)),YES);
	  set_word(i+1,YES);
	}
        else
	{
          set_field((79 - (i*5)),NO);
	  set_word(i+1,NO);
	}
      }
    }
    old_patrn = patrn;
  }

  set_attr(NORM);
  old_patrn = patrn;
}




/* DISPLAY_OUTBITS() -- Display bit patern of Dmux bus output port. 
 *
 */
int display_outbits()
{
  unsigned short int si;
  int i;

  unsigned short int chg_patrn;
  static unsigned short int old_patrn = 0, patrn = 0;

  if(ioctl(muxchan,SAMDXWD,&patrn)<0) 
    perror("SAMDXWD");


  if(reset)
  {
    old_patrn = ~patrn;
    reset = 0;
  }

  if(patrn != old_patrn)
  {
    gotoxy(73,23);
    printf("%X",patrn);
    chg_patrn = old_patrn ^ patrn;
    for(i=0;i<16;i++)
    {
      if(chg_patrn & (1 << i))
      {
        if(patrn & (1 << i))
	{
          set_field((79 - (i*5)),YES);
	  set_word(i+1,YES);
	}
        else
	{
          set_field((79 - (i*5)),NO);
	  set_word(i+1,NO);
	}
      }
    }
    old_patrn = patrn;
  }

  set_attr(NORM);
  old_patrn = patrn;
}





int set_word(i,flag)
int i;
int flag;
{
  int x,y;
  struct field_names *screen;

  if(mode == INMODE)
    screen = in_screen;
  else
    screen = out_screen;

  if(i>16)
    return;
  if(i<9)
  {
    x=28;
    y= TEXT_Y + i -1;
  }
  else
  {
    x=68;
    y= TEXT_Y -9 +i;
  }
  gotoxy(x,y);


  if(flag)
  {
    set_attr(RED_C);
    printf("%s",screen[i].hi_str);
  }
  else
  {
    set_attr(GREEN_C);
    printf("%s",screen[i].lo_str);
  } 
}




int set_field(x,flag)
int x;
int flag;
{
  if(flag)
  {
    set_attr(RED);
    gotoxy(x,8);
    putchar('\32');
    set_attr(BLANK);
    gotoxy(x,10);
    printf(" ");
  }
  else
  {
    set_attr(BLANK);
    gotoxy(x,8);
    printf(" ");
    set_attr(GREEN);
    gotoxy(x,10);
    putchar('\32');
  }
}




/*
 * CNTRC_HANDLER() -- If user presses control c this routine will be called,
 *                    instead of the program just halting.
 *
 */
void cntrc_handler()
{
  set_kb(COOKED);
  reset_vt100();
  set_attr(NORM);
  set_scroll(1,24);
  exit(1);
}







/* SET_KB() --  Yo Tom,here is a routine that does what you wanted.
 *              if you call it with a 1: set_kb(1);
 *              that is the same as -> stty cbreak,stty -echo.
 *              if you call it with a 0: set_kb(0);
 *              that is the same as -> stty -cbreak,stty echo.
 *
 *              it operates on the standard input (0) which is
 *              assumed to be associated with a terminal.
 */
set_kb(on)
int on;
{
  struct termios t;

  if(ioctl(0,TCGETS,&t))
    perror("ioctl:");

  if(on) {
    t.c_lflag &= ~ICANON;
    t.c_lflag &= ~ECHO;
    t.c_cc[VMIN] = 1;
    t.c_cc[VTIME] = 0;

  } else {
    t.c_lflag |= ICANON;
    t.c_lflag |= ECHO;
  }

  if(ioctl(0,TCSETS,&t))
    perror("ioctl:");
}


              


/*******************************************************************************
*                                                                              *
*                            VIDEO DRIVERS                                     *
*                                                                              *
*******************************************************************************/

int gotoxy(x,y)                           /* set cursor to x,y on ansi screen */
int x,y;
{
  char temp[20];

  if((x<1) | (x>80) | (y<1) | (y>24)) return(0);
  printf("\033\133%1d\073%1d\146",y,x);
  return(1);
}  

int vt100_print(x,y,label)                  /* sort of point and shoot print */
int x,y; 
char *label;
{
  char temp[20];

  if(gotoxy(x,y) !=NULL)  
  {
    printf("%s",label);
  }
}


int set_scroll(t,b)                        /* sets scrollable area of screen */
int t,b;
{
  printf("\033\133%d\073%d\162",t,b);                        /* set no scroll */
}


int reset_vt100()
{
  printf("\033\133\077\064\154");                       /* select jump scroll */
  printf("\033\133\077\063\154");                    /* select 80 column mode */
  printf("\033\133\077\067\150");                        /* turn on auto-wrap */
  printf("\033\133\077\065\154");                  /* set background to black */
  printf("\033\133\110");                               /* set cursor to home */
  printf("\033\133\062\112");                                 /* clear screen */
  printf("\033\133\077\066\154");           /* set absolute screen addressing */
}


set_attr(astr)				   /* set attribute for next printing */
int astr;
{
  char *cp;

  switch(astr)
  {
    case RED_C:
      cp = "[40;31m" ;break;
    case GREEN_C:
      cp = "[40;32m" ;break;
    case RED:
      cp = "[41;31m" ;break;
    case GREEN:
      cp = "[42;32m" ;break;
     case BLANK:
      cp = "[40;30m" ;break;
    case NORM :
      cp = "[m" ;break;
   }
  printf("%s",cp);
}

