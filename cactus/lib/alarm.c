/*
 * Cactus File %W%
 *         SID %I%
 *        Date %G%
 */
static char SccsId[] = "%W%\t%G%";

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cactus_global.h"

struct ALARM alarms[] = {
{ NULL, "MRI Receiver Warming Up.",         "RCVR_WARM_MRI",        MSG_ERR  },
{ NULL, "Blue I.F. Proc Out Of Lock.",      "IF_OUT_LOCK",          MSG_ERR  },
{ NULL, "DBE Waiting On Valid Data.",       "DBE_WAIT_V_DATA",      MSG_WARN },
{ NULL, "End Of Calibration.",              "END_OF_CAL",           MSG_INFO },
{ NULL, "End Of Pair.",                     "END_OF_PAIR",          MSG_INFO },
{ NULL, "End Of Sets.",                     "END_OF_SETS",          MSG_INFO },
{ NULL, "End Of Scan.",                     "END_OF_SCAN",          MSG_INFO },
{ NULL, "FBMUX Waiting On Valid Data.",     "FB_WAIT_V_DATA",       MSG_WARN },
{ NULL, "Filter Bank Offset Out of Range.", "FILTER_OFF_OOR",       MSG_ERR  },
{ NULL, "Final Elevation Limit.",           "FINAL_ELEV",           MSG_WARN },
{ NULL, "Generator Running.",               "GENERATOR_RUN",        MSG_WARN },
{ NULL, "Focus Out Of Tolerance.",          "INCORRECT_FOCUS",      MSG_WARN },
{ NULL, "High Winds Detected.",             "HIGH_WIND",            MSG_INFO },
{ NULL, "Humidity High.",                   "HUMIDITY_HIGH",        MSG_WARN },
{ NULL, "Ignoring Focus.",                  "IGNORING_FOCUS",       MSG_WARN },
{ NULL, "Ignoring Receiver Lock.",          "IGNORING_RX_LOCK",     MSG_WARN },
{ NULL, "Improper Pointing Tol Set.",       "INCORRECT_TOL",        MSG_WARN },
{ NULL, "Improper Subreflector BEAMS.",     "INCORRECT_SUB_BEAM",   MSG_ERR  },
{ NULL, "Improper Subreflector Position.",  "INCORRECT_SUB_POS",    MSG_ERR  },
{ NULL, "Incorrect Receiver Bay.",          "INCORRECT_RX_BAY",     MSG_ERR  },
{ NULL, "Integration Time NOT Advancing.",  "NOT_TAKING_DATA",      MSG_WARN },
{ NULL, "MAC LO Synth Problem.",            "LOSYNTH_ERROR",        MSG_ERR  },
{ NULL, "MAC Synth Out Of Lock.",           "HYSPEC_SYNTH_LOCK",    MSG_ERR  },
{ NULL, "No Ref Position Specified.",       "NO_REFERENCE_POS",     MSG_ERR  },
{ NULL, "Nothing From 8CHANIF SBC.",        "NO_8CHANIF_SBC",       MSG_ERR  },
{ NULL, "Nothing From LOSYNTH SBC.",        "NO_LOSYNTH_SBC",       MSG_ERR  },
{ NULL, "Nothing From Tipper.",             "TIPPER_NO_RESPOND",    MSG_WARN },
{ NULL, "Nothing From Tracker's Respond.",  "TRACKER_NO_RESPOND",   MSG_WARN },
{ NULL, "Nothing From Wx Station.",         "WX_NO_RESPOND",        MSG_WARN },
{ NULL, "Rain Detector Activated.",         "RAIN_DETECTED",        MSG_WARN },
{ NULL, "Receiver Out Of Lock.",            "RCVR_OUT_LOCK",        MSG_ERR  },
{ NULL, "Reference Same As On Pos.",        "REF_SAME_AS_ON",       MSG_ERR  }, 
{ NULL, "Sig / Reference Too Similar.",     "SIG_REF_SAME",         MSG_ERR  }, 
{ NULL, "Source Below Elevation Limit.",    "SOURCE_BELOW_ELEV",    MSG_WARN },
{ NULL, "Subreflector Is NOT Moving.",      "SUB_REF_NOT_MOVING",   MSG_ERR  },
{ NULL, "Sun/Telescope Conflict.",          "SUN_TELE_CONFLICT",    MSG_WARN },
{ NULL, "Torque Motor Current Too High.",   "TORQUE_MOTOR_CURRENT", MSG_WARN },
{ NULL, "Torque Motor Temps Too High.",     "TORQUE_MOTOR_TEMP",    MSG_WARN },
{ NULL, "Tracking Too Close To Sun.",       "TOO_CLOSE_2_SUN",      MSG_INFO },
{ NULL, "Utility Power Off",                "UTIL_POWER_OFF",       MSG_ERR  },
{ NULL, "Velocity Offset Non-Zero.",        "VELOCITY_OFFSET",      MSG_WARN },
{ NULL, "Verify Telescope On Source.",      "VERIFY_SOURCE",        MSG_WARN },
{ NULL, "VxWorks Backend Up.",              "BACKEND_UP",           MSG_INFO },
{ NULL, "VxWorks Backend Going Down.",      "BACKEND_DOWN",         MSG_INFO },
{ NULL, "Problem with Filter Bank Zeros",   "ZERO_CHECK_ERROR",     MSG_ERR  },
{ NULL, "Nutator AUX Amp Hot",              "NUTATOR_AUX_AMP_HOT",  MSG_ERR  },
{ NULL, "Receiver/Global Sideband Mismatch","SIDEBAND_MISMATCH",    MSG_WARN },
{ NULL, "Receiver Cabin Hot",               "RXCABIN_HOT",          MSG_WARN },
{ NULL, "UPS Alarm",                        "UPS_ALARM",            MSG_WARN },
{ NULL, "Telescope 24V Supply Failure",     "24V_PS_ALARM",         MSG_ERR  },
{ NULL, "Dome Lost",                        "DOME_LOST",            MSG_ERR  },

{ NULL,  NULL,                               NULL,                  0        }
};

/* 
  This routine assumes that all programs which access the alarm structures
  will be restarted when the alarm structure changes.
  If the alarm structure changes then all alarm settings will be cleared
  when the ismonitor flag is true.
*/

int init_alarms(ismonitor)
int ismonitor;
{
  struct ALARM *a;
  struct ALARMGLOB *g;
  char *p;
  int hash;

  if( !cpr )
  {
    return(0);
  }

  g = &cpr->alarm[0];
  for( a = alarms; a->name; a++, g++ )
  {
    a->g = g;
  }
  
  if( !ismonitor )
  {
    return(0);
  }

  hash = 0;
  for( a=alarms; a->name; a++ )
  {
    p = a->name;

    while( *p )
    {
      hash += (int)*p++;
    }
  }

  if( hash != cpr->alarm_hash)
  {
    cpr->alarm_hash = hash;
    bzero( cpr->alarm, sizeof(struct ALARMGLOB) * MAXALARMS );

    if(g->time) ; /* make this program crash if you exceed global mem */
    {
      return(1);
    }
  }

  return(0);
}

