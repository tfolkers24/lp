#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "caclib_proto.h"

int main(int argc, char *argv[])
{
  char *sourcename, *destname;
  char *message;
  struct SOCK *source;
  int len, timeout;
  int quit_flag = 0;
  int quiet_flag = 0;
  int response_flag = 0;
  int slam_flag = 0;
  int ct_flag = 8192;

  setCactusEnvironment();

  sourcename = NULL;
  destname = "ANONYMOUS";
  if( argc <= 1 ) {
    printf( "\nusage: %s [-s] [-r] [-d destname] [sourcename]\n", argv[0] );
    printf( "  -d destname - the destination mailbox name\n" );
    printf( "  sourcename  - the source mailbox name\n" );
    printf( "  -r response is expected\n\n" );
    printf( "  -s  slam it down one line at a time with out looking for a response\n");
    printf( "  -ct  ct  size of buffer\n");
    printf( "  -q Quiet!  Skip \"message received\" prefix\n");
    exit(0);
  }

  while( *++argv ) {
    if( strcmp( *argv , "-d" ) == 0 )
      destname = *++argv;
    else if( strcmp( *argv , "-r" ) == 0 )
      response_flag = 1;
    else if( strcmp( *argv , "-s" ) == 0 )
      slam_flag = 1;
    else if( strcmp( *argv , "-ct" ) == 0 )
      ct_flag = atoi(*++argv);
    else if( strcmp( *argv , "-q" ) == 0 )
      quiet_flag = 1;
    else
      sourcename = *argv;
  }

  message = (char *)malloc(ct_flag);

  if( destname )
    sock_bind(destname);

  sock_bufct(ct_flag);
  if( sourcename )
    source = sock_connect(sourcename);

  if( response_flag )
    timeout = 1;
  else
    timeout = 0;

  if( slam_flag ) {
    while( fgets(message, ct_flag-1, stdin))
      sock_send( source, message );
     
  } else  {
    while(1) {
      switch(sock_sel( message, &len, NULL, 0, timeout, !quit_flag)) {
        case 0: 
          if (!sock_send( source, message) && destname && !sourcename)
	    source = 0; 
          break;
        case -1:
          if( quit_flag )
            exit(1);
        case -2:
        case -3:
          if( response_flag )
            quit_flag = 1;
          else
            exit(1);
          break;
        default:
	  if (source == 0)
	    source = (struct SOCK *)last_msg();
          if( response_flag || quiet_flag)
            printf("%s\n", message );
          else
            printf("message received %s\n", message );
          break;
      }
    }
  }

  return EXIT_SUCCESS;  /* never reached */
}

