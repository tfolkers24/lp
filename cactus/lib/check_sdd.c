/*
 *  Cactus file %W%
 *        Date %G%
 *
 */
#ifndef lint

static char SccsID[] = "%W%\t%G%";

#endif

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <math.h>

#include "sdd.h"
#include "header.h"

//#define TOMMIE 1

char *string();


int do_one(h, pheadlen, pdatalen)
struct HEADER *h;
int *pheadlen, *pdatalen;
{
/* Class 1 */

  printf("\nClass 1\n");

  printf( "headlen %f\n",  h->headlen);
  printf( "datalen %f\n", h->datalen);
  printf( "scan %f\n", h->scan);
  printf(" obsid     %s\n", string(&h->obsid,8));
  printf(" observer  %s\n", string(&h->observer,16));
  printf(" telescop  %s\n", string(&h->telescop,8));
  printf(" projid    %s\n", string(&h->projid,8));
  printf(" object    %s\n", string(&h->object,16));
  printf(" obsmode   %s\n", string(&h->obsmode,8));
  printf(" frontend  %s\n", string(&h->frontend,8));
  printf(" backend   %s\n", string(&h->backend,8));
  printf(" precis    %s\n", string(h->precis,8));

  *pheadlen = (int)h->headlen;
  *pdatalen = (int)h->datalen;
}

int do_two(h)
struct HEADER *h;
{
/* Class 2 */

  printf("\nClass 2\n");

  printf( "xpoint %f\n", h->xpoint);
  printf( "ypoint %f\n", h->ypoint);
  printf( "uxpnt %f\n", h->uxpnt);
  printf( "uypnt %f\n", h->uypnt);
  printf( "ptcon %f %f %f %f\n", h->ptcon[0], h->ptcon[1], h->ptcon[2], h->ptcon[3]);
  printf( "orient %f\n", h->orient);
  printf( "focusr %f\n", h->focusr);
  printf( "focusv %f\n", h->focusv);
  printf( "focush %f\n", h->focush);
  printf(" pt_model  %s\n", string(&h->pt_model,8));
}

int do_three(h)
struct HEADER *h;
{
/* Class 3  */

  printf("\nClass 3\n");

  printf( "utdate %f\n", h->utdate);
  printf( "ut %f\n", h->ut);
  printf( "lst %f\n", h->lst);
  printf( "norchan %f\n", h->norchan);
  printf( "noswvar %f\n", h->noswvar);
  printf( "nophase %f\n", h->nophase);
  printf( "cycllen %f\n", h->cycllen);
  printf( "samprat %f\n", h->samprat);
  printf(" cl11type  %s\n", string(&h->cl11type,8));

  return(0);
}

int do_four(h)
struct HEADER *h;
{
/* Class 4 */

  printf("\nClass 4\n");

  printf( "epoch %f\n",  h->epoch);
  printf( "xsource %f\n", h->xsource);
  printf( "ysource %f\n", h->ysource);
  printf( "xref %f\n", h->xref);
  printf( "yref %f\n", h->yref);
  printf( "epocra %f\n", h->epocra);
  printf( "epocdec %f\n", h->epocdec);
  printf( "gallong %f\n", h->gallong);
  printf( "gallat %f\n", h->gallat);
  printf( "az %f\n", h->az);
  printf( "el %f\n", h->el);
  printf( "indx %f\n", h->indx);
  printf( "indy %f\n", h->indy);
  printf( "desorg %f %f %f\n", h->desorg[0],h->desorg[1], h->desorg[2]);
  printf( "coordcd %s\n", string(&h->coordcd, 8));

  return(0);
}

int do_five(h)
struct HEADER *h;
{

/* Class 5 */

  printf("\nClass 5\n");

  printf( "tamb %f\n", h->tamb);
  printf( "pressure %f\n", h->pressure);
  printf( "humidity %f\n", h->humidity);
  printf( "refrac %f\n", h->refrac);
  printf( "dewpt %f\n", h->dewpt);
  printf( "mmh2o %f\n", h->mmh2o);

  return(0);
};

int do_six(h)
struct HEADER *h;
{
/* Class 6 */

  printf("\nClass 6\n");

  printf( "scanang %f\n", h->scanang);
  printf( "xzero %f\n", h->xzero);
  printf( "yzero %f\n", h->yzero);
  printf( "deltaxr %f\n", h->deltaxr);
  printf( "deltayr %f\n", h->deltayr);
  printf( "nopts %f\n", h->nopts);
  printf( "noxpts %f\n", h->noxpts);
  printf( "noypts %f\n", h->noypts);
  printf( "xcell0 %f\n", h->xcell0);
  printf( "ycell0 %f\n", h->ycell0);
  printf( "frame %s\n",  string(&h->frame,8));

  return(0);
}

int do_seven(h)
struct HEADER *h;
{

/* Class 7 */

  printf("\nClass 7\n");

  printf( "bfwhm %f\n", h->bfwhm);
  printf( "offscan %f\n", h->offscan);
  printf( "badchv %f\n", h->badchv);
  printf( "rvsys %f\n", h->rvsys);
  printf( "velocity %f\n", h->velocity);
  printf( "veldef %s\n",  string(&h->veldef, 8));
  printf( "typecal %s\n",  string(&h->typecal, 8));

  return(0);
}

int do_eight(h)
struct HEADER *h;
{
/* Class 8 */

  printf("\nClass 8\n");

  printf( "appeff  %f\n", h->appeff);
  printf( "beameff %f\n", h->beameff);
  printf( "antgain %f\n", h->antgain);
  printf( "etal    %f\n", h->etal);
  printf( "etafss  %f\n", h->etafss);

  return(0);
}

int do_nine(h)
struct HEADER *h;
{
/* Class 9 - Mt Graham */

  printf("\nClass 9\n");

  printf( "synfreq %f\n", h->synfreq);
  printf( "lofact %f\n", h->lofact);
  printf( "harmonic %f\n", h->harmonic);
  printf( "loif %f\n", h->loif);
  printf( "firstif %f\n", h->firstif);
  printf( "razoff %f\n", h->razoff);
  printf( "reloff %f\n", h->reloff);
  printf( "bmthrow %f\n", h->bmthrow);
  printf( "bmorent %f\n", h->bmorent);
  printf( "baseoff %f\n", h->baseoff);
  printf( "obstol %f\n", h->obstol);
  printf( "sideband %f\n", h->sideband);
  printf( "wl %f\n", h->wl);
  printf( "gains %f\n", h->gains);
  printf( "pbeam %f %f\n", h->pbeam[0], h->pbeam[1]);
  printf( "mbeam %f %f\n", h->mbeam[0], h->pbeam[1]);
  printf( "sroff %f %f %f %f\n", h->sroff[0], h->sroff[1], h->sroff[2], h->sroff[3]);
  printf( "foffsig  %f\n", h->foffsig);
  printf( "foffref1 %f\n", h->foffref1);
  printf( "foffref2 %f\n", h->foffref2);

  return(0);
}

int do_ten(h)
struct HEADER *h;
{
  int i;

 /* Class 10 */

  printf("\nClass 10\n");
  for( i=0; i<10; i++ )
    printf( "openpar[%d] %f\n", i, h->openpar[i]); 

  return(0);
}

int do_eleven(h)
struct HEADER *h;
{
  int i;
 /* Class 11 */

  printf("\nClass 11\n");

  printf( "current_disk %f\n", h->current_disk);
  printf( "widebandmode %f\n", h->widebandmode);
  printf( "sptip_start %f\n", h->sptip_start);
  printf( "sptip_stop %f\n", h->sptip_stop);
  printf( "ramp_up %f\n", h->ramp_up);
  printf( "tatms %f\n", h->tatms);
  printf( "taus %f\n", h->taus);
  printf( "taui %f\n", h->taui);
  printf( "tatmi %f\n", h->tatmi);
  printf( "tchop %f\n", h->tchop);
  printf( "tcold %f\n", h->tcold);
  printf( "gaini %f\n", h->gaini);
  printf( "count %f, %f, %f\n", h->count[0], h->count[1], h->count[2]);
  printf( "linename %s\n",  string(&h->linename, 16));
  printf( "refpt_vel %f\n", h->refpt_vel);
  printf( "tip_humid %f\n", h->tip_humid);
  printf( "tip_ref_flag %f\n", h->tip_ref_flag);
  printf( "refract_45 %f\n", h->refract_45);
  printf( "ref_correct %f\n", h->ref_correct);
  printf( "beam_num %f\n", h->beam_num);
  printf( "burn_time %f\n", h->burn_time);
  printf( "parallactic %f\n", h->parallactic);
  printf( "az_offset %f\n", h->az_offset);
  printf( "el_offset %f\n", h->el_offset);
  printf( "yfactor %f\n", h->yfactor);
  printf( "rejection %f\n", h->rejection);
  printf( "nutator_temp %f\n", h->nutator_temp);

  for(i=0;i<6;i++)
    printf( "spares05[%d] %f\n", i, h->spares05[i]);


  return(0);
}

int do_twelve(h)
struct HEADER *h;
{

 /* Class 12 */

  printf("\nClass 12\n");

  printf( "obsfreq %f\n", h->obsfreq);
  printf( "restfreq %f\n", h->restfreq);
  printf( "freqres %f\n", h->freqres);
  printf( "bw %f\n", h->bw);
  printf( "trx %f\n", h->trx);
  printf( "tcal %f\n", h->tcal);
  printf( "stsys %f\n", h->stsys);
  printf( "rtsys %f\n", h->rtsys);
  printf( "tsource %f\n", h->tsource);
  printf( "trms %f\n", h->trms);
  printf( "refpt %f\n", h->refpt);
  printf( "x0 %f\n", h->x0);
  printf( "deltax %f\n", h->deltax);
  printf( "inttime %f\n", h->inttime);
  printf( "noint %f\n", h->noint);
  printf( "spn %f\n", h->spn);
  printf( "tauh2o %f\n", h->tauh2o);
  printf( "th2o %f\n", h->th2o);
  printf( "tauo2 %f\n", h->tauo2);
  printf( "to2 %f\n", h->to2);
  printf( "polariz %s\n", string(h->polariz,8));
  printf( "effint %f\n", h->effint);
  printf( "rx_info %s\n", string(h->rx_info,16));

  return(0);
}

int do_thirteen(h)
struct HEADER *h;
{
  printf("\nClass 13\n");

  printf( "nostac %f\n", h->nostac);
  printf( "fscan %f\n", h->fscan);
  printf( "lscan %f\n", h->lscan);
  printf( "lamp %f\n", h->lamp);
  printf( "lwid %f\n", h->lwid);
  printf( "ili %f\n", h->ili);
  printf( "rms %f\n", h->rms);

  return(0);
}

char *string(s,n)
char *s;
int n;
{
  static char buf[2050];

  strncpy(buf, s, n );
  buf[n] = '\0';
  return(buf);
}


int main(argc, argv )
int argc;
char *argv[];
{
  int size, k, fd, fd2, i, max, dirno;
  struct BOOTSTRAP boot;
  struct DIRECTORY dir;
  struct HEADER head;
  int headlen, datalen;
  float *data, *data0 = NULL;
  int norchan, loop;

  if( argc < 2 ) {
    printf("%s file [scan] [all] - shows identified scan\n%s file - shows last scan\n%s file all - shows whole file\n",
      argv[0], argv[0], argv[0] );
    exit(0);
  }

  if( (fd = open(argv[1], O_RDONLY, 0666 ))<0) {
    perror("open");
    exit(1);
  }

  if( read( fd, &boot, sizeof(struct BOOTSTRAP))<0 ) {
    perror("open");
    exit(1);
  }

  printf("  num_index_rec     %d\n", boot.num_index_rec );
  printf("  num_data_rec      %d\n", boot.num_data_rec );
  printf("  bytperrec         %d\n", boot.bytperrec);
  printf("  bytperent         %d\n", boot.bytperent);
  printf("  num_entries_used  %d\n", boot.num_entries_used ); 
  printf("  counter           %d\n", boot.counter);  
  printf("  typesdd           %d\n", boot.typesdd);
  printf("  version           %d\n", boot.version);

/* print last directory entry */

  if( boot.num_entries_used <= 0 ) {
    printf("sdd file empty\n");
    exit(1);
  }

  if( argc < 3 ) {
    int dirl; 

    loop = 0;
    dirl = boot.bytperrec + boot.bytperent * (boot.num_entries_used-1);
    lseek( fd, dirl, SEEK_SET );
    if( read( fd, &dir, sizeof(struct DIRECTORY)) <0 ) {
      perror("read dir");
      exit(1);
    }
    dirno = boot.num_entries_used;
  } 
  else 
  {
    float scan = atof(argv[2]);

    if( scan <= 0 && strncmp(argv[2], "all", 3 ) == 0 ) {
      loop = boot.num_entries_used;
      dirno = 0;
    } else {
      loop = 0;
      dirno = boot.num_entries_used;
      lseek( fd, 512, SEEK_SET );
      for( i=0; i<dirno; i++ ) {
        if( read( fd, &dir, sizeof(struct DIRECTORY)) <0 ) {
          perror("read dir");
          exit(1);
        }
        if( scan == dir.scan )
          break;
      }
      if( i>=dirno ){
        printf("scan %7.2f not found\n", scan );
        exit(0);
      }
      dirno = i;
    }
  }

  while(1) {

  if( loop > 0 ) {
    if( --loop <= 0 )
       break;
    lseek( fd, boot.bytperrec + dirno * boot.bytperent, SEEK_SET );
    if( read( fd, &dir, sizeof(struct DIRECTORY)) <0 ) {
      perror("read dir");
      exit(1);
    }
  }

  printf("\ndirectory i'th entry %d\n", dirno++);
  printf(" start_rec %d\n", dir.start_rec );
  printf(" end_rec   %d\n", dir.end_rec );
  printf(" h_coord   %f\n", dir.h_coord );
  printf(" v_coord   %f\n", dir.v_coord );
  printf(" source    %s\n", string(dir.source, 16 ));
  printf(" scan      %f\n", dir.scan );
  printf(" freq_res  %f\n", dir.freq_res );
  printf(" rest_freq %f\n", dir.rest_freq );
  printf(" lst       %f\n", dir.lst       );
  printf(" ut        %f\n", dir.ut        );
  printf(" obsmode   %d\n", dir.obsmode   );
  printf(" phase_rec %d\n", dir.phase_rec );
  printf(" pos_code  %d\n", dir.pos_code );

/* read in data header of first channel of last scan */ 

  max = boot.bytperrec * (dir.start_rec-1);
  lseek( fd, max, SEEK_SET ); 
  if(read(fd, &head, sizeof(struct HEADER) )<0) {
    perror("read header");
    exit(1);
  }
  printf("\n data header:\n");
  printf(" headcls %d\n", head.headcls);
  printf(" oneptr %d\n", head.oneptr);
  printf(" twoptr %d\n", head.twoptr);
  printf(" thrptr %d\n", head.thrptr);
  printf(" fourptr %d\n", head.fourptr);
  printf(" fiveptr %d\n", head.fiveptr);
  printf(" sixptr %d\n", head.sixptr);
  printf(" sevptr %d\n", head.sevptr);
  printf(" eigptr %d\n",  head.sevptr);
  printf(" nineptr %d\n", head.nineptr);
  printf(" tenptr %d\n", head.tenptr);
  printf(" elvptr %d\n", head.elvptr);
  printf(" twlptr %d\n", head.twlptr);
  printf(" trnptr %d\n", head.trnptr);


/* print out real pdfl data structure */

  do_one(&head, &headlen, &datalen);
  do_two(&head);
  norchan = do_three(&head);
  do_four(&head);
  do_five(&head);
  do_six(&head);
  do_seven(&head);
  do_eight(&head);
  do_nine(&head);
  do_ten(&head);
  do_eleven(&head);
  do_twelve(&head);
  do_thirteen(&head);

/* data */

  max = boot.bytperrec * (dir.start_rec-1) + headlen;
  lseek( fd, max, SEEK_SET );

  if (data0 != NULL)
    free(data0);

  data = data0 = (float *)malloc( datalen );
  if (data0 == NULL) {
    printf("Malloc of %d bytes failed\n", datalen);
    exit(1);
  }

  bzero( data, datalen );

  if( read( fd, data, datalen) <0 ) {
    perror("read data");
    exit(1);
  }

//  exit(0);

  printf("\n data\n");
  i = 8;

#ifdef TOMMIE
  while( datalen > 0 ) {
    printf("%10.6e %10.6e %10.6e %10.6e %10.6e %10.6e %10.6e %10.6e\n", 
      data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7]);
    datalen -= 8 * sizeof(float);
    data += 8;
  }
#else
  size = datalen / sizeof(float);

  for(k=0;k<size;k++)
  {
    printf("%f\n", data[k]);
  }

#endif
  if( loop <= 0 )
    break; 
  } /* while(1) */
}
