/*
 *  Cactus file %W%
 *        Date %G%
 *
 */
#ifndef sparc

static char SccsID[] = "%W%\t%G%";

#endif

#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <unistd.h>
#include <ctype.h>
#include <fcntl.h>
#include <malloc.h>
#include <math.h>
#include <sys/types.h>
#include <sys/time.h>

#include "sdd.h"
#include "header.h"


extern int swapThisSdd;


int swapBoot(boot)
struct BOOTSTRAP  *boot;
{
  int i;
  long *p;

  if(boot->version != 1)
  {
    swapThisSdd = 1;
    for(i=0,p=&boot->num_index_rec;i<8;i++,p++)
      in_swapl(p);
  }
  else
    swapThisSdd = 0;

  return(0);
}


int force_swapBoot(boot)
struct BOOTSTRAP  *boot;
{
  int i;
  long *p;

  for(i=0,p=&boot->num_index_rec;i<8;i++,p++)
    in_swapl(p);

  return(0);
}



int swapDir(dir)
struct DIRECTORY *dir;
{

  in_swapl(&dir->start_rec);
  in_swapl(&dir->end_rec);

  in_swapf(&dir->h_coord);
  in_swapf(&dir->v_coord);
 
/* leave the char "source" alone */

  in_swapf(&dir->scan);
  in_swapf(&dir->freq_res);

  in_swapd(&dir->rest_freq);

  in_swapf(&dir->lst);
  in_swapf(&dir->ut);

  in_swaps(&dir->obsmode);
  in_swaps(&dir->phase_rec);
  in_swaps(&dir->pos_code);
  in_swaps(&dir->unused);

  return(0);
}


swapHeader(head)
struct HEADER *head;
{
  int j;
  double *d;
  float  *f;
  short  *s;
  int    *i;

  for(j=0,s=&head->headcls;j<16;j++,s++)
    in_swaps(s);

/* class 1 */

  in_swapd(&head->headlen);
  in_swapd(&head->datalen);
  in_swapd(&head->scan);
  /* leave the chars alone */

/* class 2 */

  for(j=0,d=&head->xpoint;j<12;j++,d++)
    in_swapd(d);
  /* leave the chars alone */

/* class 3 */

  for(j=0,d=&head->utdate;j<8;j++,d++)
    in_swapd(d);
  /* leave the chars alone */

/* class 4 */

  for(j=0,d=&head->epoch;j<16;j++,d++)
    in_swapd(d);
  /* leave the chars alone */

/* class 5 */

  for(j=0,d=&head->tamb;j<6;j++,d++)
    in_swapd(d);

/* class 6 */

  for(j=0,d=&head->scanang;j<10;j++,d++)
    in_swapd(d);
  /* leave the chars alone */

/* class 7 */

  for(j=0,d=&head->bfwhm;j<5;j++,d++)
    in_swapd(d);
  /* leave the chars alone */

/* class 8 */

  for(j=0,d=&head->appeff;j<5;j++,d++)
    in_swapd(d);

/* class 9 */

  for(j=0,d=&head->synfreq;j<25;j++,d++)
    in_swapd(d);

/* class 10 */

  for(j=0,d=head->openpar;j<10;j++,d++)
    in_swapd(d);

/* class 11 */

  for(j=0,d=&head->current_disk;j<15;j++,d++)
    in_swapd(d);
  /* leave the chars "linename" alone */
  for(j=0,d=&head->refpt_vel;j<13;j++,d++)
    in_swapd(d);
  for(j=0,d=head->spares05;j<6;j++,d++)
    in_swapd(d);

/* class 12 */

  for(j=0,d=&head->obsfreq;j<20;j++,d++)
    in_swapd(d);
  /* leave the chars "polariz" alone */
  in_swapd(&head->effint);
  /* leave the chars "rx_info" alone */

/* class 13 */

  for(j=0,d=&head->nostac;j<11;j++,d++)
    in_swapd(d);

  return(0);
}



swapData(data, n)
float *data;
int n;
{
  int i;
  float *f;

  f = data;
  for(i=0;i<n;i++,f++)
    in_swapf(f);

  return(0);
}

