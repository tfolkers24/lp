/*
 *  Cactus file @(#)mk.c	1.3
 *        Date 10/08/07
 *
 */
#ifndef lint

static char SccsID[] = "@(#)mk.c	1.3\t10/08/07";

#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "y.tab.h"

struct GSKEY {
  struct GSKEY *next;
  char *name;
  int type;   /* INTEGER FLOAT DOUBLE CHARSTAR */
  char *size; /* NULL if not array             */
  int field;  /* active/passive field          */
  int log;    /* send log message to monitor when changes */
  int rambo;  /* include this in ramexec ?     */

};

int count = 1;

struct GSKEY *head = NULL;
struct GSKEY *tail = NULL;

char *get_field(), *get_log(), *get_rambo();

int rambo_on = 0;

char sub_str[80];
char sub[80];

main(argc, argv)
int argc;
char **argv;
{
  sub_str[0] = '\0';
  sub[0] = '\0';

  while(*++argv) {
    if( strcmp(*argv, "-r" ) == 0 ) {
      rambo_on = 1;
    }
    if( strcmp(*argv, "-sub") == 0 ) {
      strcpy( sub, *++argv );
      strcpy( sub_str, *++argv );
    }
  }

  while(yyparse());
  exit(0);
}

ignore()
{ 
}


struct GSKEY *new_gs()
{
  struct GSKEY *p;

  p = (struct GSKEY *)malloc( sizeof(struct GSKEY));
  bzero( p, sizeof(struct GSKEY));

  if( !head  )
    head = p;

  if( tail )
    tail->next = p;

  tail = p;

  if( rambo_on )
    p->rambo = 1;

  return(p);
}

add_notchar(v, name)
int v;
char *name;
{
  
  struct GSKEY *p = new_gs();

  p->name = name;
  p->type = v;
  p->size = (char *)malloc(2);
  strcpy(p->size, "1");
}


add_array(v, name, sz)
int v;
char *name, *sz;
{
  struct GSKEY *p = new_gs();

  p->name = name;
  p->type = v;
  p->size = sz;
}


comment(p)
char *p;
{
  if( strstr( p, "LOG" ))
   tail->log = 1;
  if( strstr( p, "RAMBO" ))
   tail->rambo = 1;

  free(p);
}

add_char(name, sz)
char *name, *sz;
{
  struct GSKEY *p = new_gs();

  p->name = name;
  p->type = CHARSTAR;
  p->size = sz;
}

got_construct( name )
char *name;
{
  struct GSKEY *p = head;
  struct GSKEY *pp;
  int field;
  char *prefix;
  char nm[32];
  char sz[32];
  char off[32];

  if( strcmp( name, "field") == 0 )
    field = 1;
  else 
    field = 0;

  printf("\n      /* %s */\n", name );

  prefix = "";

  while(p) {
    sprintf( sz, "%s,", p->size );
    if( field )
      sprintf( off, "&ZRO->act.%s,", p->name );
    else if( strcmp( name, "HEADER" ) == 0 ) {
      sprintf( off, "&ZRO->hdr.%s,", p->name );
      prefix = "h_";
    } else if( *sub_str && strcmp( name, sub_str ) == 0 ) {
      sprintf( off, "&ZRO->%s.%s,", sub, p->name );
      prefix = "";
    } else
      sprintf( off, "&ZRO->%s,", p->name );

    sprintf( nm, "\"%s%s\",", prefix, p->name );

    switch( p->type ) {
      case INTEGER:
      case LONG:
        printf("%-30.30s %-30.30s INTEGER,  %-15.15s %-10.10s %-5.5s %-6.6s\n",
          off, nm, sz, get_field(field), get_log(p->log), get_rambo(p->rambo));
        break;
      case DOUBLE:
        printf("%-30.30s %-30.30s DOUBLE,   %-15.15s %-10.10s %-5.5s %-6.6s\n",
          off, nm, sz, get_field(field), get_log(p->log), get_rambo(p->rambo));
        break;
      case FLOAT:
        printf("%-30.30s %-30.30s FLOAT,    %-15.15s %-10.10s %-5.5s %-6.6s\n",
           off, nm, sz, get_field(field), get_log(p->log), get_rambo(p->rambo));
        break;
      case CHARSTAR:
        printf("%-30.30s %-30.30s CHARSTAR, %-15.15s %-10.10s %-5.5s %-6.6s\n",
          off, nm, sz, get_field(field), get_log(p->log), get_rambo(p->rambo));
        break;
      default:
        printf("/* skipped %s */\n", p->name );
        break;
    }
    pp = p;
    p = p->next;
    free(pp->name);
    free(pp->size);
    free(pp);
  }
  head = tail = NULL;
}

char *get_field(f)
int f;
{
  if( f )
   return("RAMBO,");
  else
   return("0,");
}

char *get_log(l)
int l;
{
  if( l )
   return("LOG,");
  else
   return("0,");
}

char *get_rambo(l)
int l;
{
  if( l )
   return("RAMBO,");
  else
   return("0,");
}
