/*
 * Cactus File @(#)swapdatum.c	1.3
 *         SID 1.3
 *        Date 08/17/04
 */

/*
 * Convert between EEEI and IEEE byte order
 * The routines are symmetrical so they do both EEEI -> IEEE and IEEE -> EEEI
 *
 * These return the converted number and don't change the original:
 * swapd(&x) 	convert a double
 * swapf(&x) 	convert a float
 * swapl(&x) 	convert a long
 * swapi(&x) 	convert an int
 * swaps(&x) 	convert an unsigned short
 *  
 * These are called with the address of the original and change it.  (No return)
 * in_swapd(&x) 	convert a double
 * in_swapf(&x) 	convert a float
 * in_swapl(&x) 	convert a long
 * in_swapi(&x) 	convert an int
 * in_swapus(&x) 	convert an unsigned short
 * in_swaps(&x) 	convert a signed short (Actually the same as unsigned)
 *
 * swapD(x)	same as swapd but takes a value not a pointer as an argument
 */

double swapd( d )
double *d;
{
  union {
    double d;
    unsigned char c[sizeof(double)];
  }u;
  unsigned char t;

  u.d = *d;

  t = u.c[0];
  u.c[0] = u.c[7];
  u.c[7] = t;

  t = u.c[1];
  u.c[1] = u.c[6];
  u.c[6] = t;

  t = u.c[2];
  u.c[2] = u.c[5];
  u.c[5] = t;

  t = u.c[3];
  u.c[3] = u.c[4];
  u.c[4] = t;

  return(u.d);
}

float swapf(f)
float *f;
{
  union {
    float f;
    unsigned char c[4];
  }u;
  unsigned char t;

  u.f = *f;
  
  t = u.c[0];
  u.c[0] = u.c[3];
  u.c[3] = t;

  t = u.c[1];
  u.c[1] = u.c[2];
  u.c[2] = t;

  return(u.f);
}

long swapl(l)
long *l;
{
  union {
    long l;
    unsigned char c[4];
  }u;
  unsigned char temp;

  u.l = *l;
  temp = u.c[0];
  u.c[0] = u.c[3];
  u.c[3] = temp;

  temp = u.c[1];
  u.c[1] = u.c[2];
  u.c[2] = temp;

  return(u.l);
}

int swapi(l)
int *l;
{
  union {
    int l;
    unsigned char c[4];
  }u;
  unsigned char temp;

  u.l = *l;
  temp = u.c[0];
  u.c[0] = u.c[3];
  u.c[3] = temp;

  temp = u.c[1];
  u.c[1] = u.c[2];
  u.c[2] = temp;

  return(u.l);
}

unsigned short swaps(s)
unsigned short *s;
{
  union {
    unsigned s;
    unsigned char c[2];
  }u;
  unsigned char temp;

  u.s = *s;
  temp = u.c[0];
  u.c[0] = u.c[1];
  u.c[1] = temp;

  return(u.s);
}

void swapus(s)
unsigned short *s;
{
  union SWAPS {
    unsigned short *s;
    unsigned char c[2];
  }*u;
  unsigned char t;

  u = (union SWAPS *)s;
  
  t = u->c[0];
  u->c[0] = u->c[1];
  u->c[1] = t;
}

void in_swapf(f)
float *f;
{
  union SWAPF {
    float f;
    unsigned char c[4];
  }*u;
  unsigned char t;

  u = (union SWAPF *)f;
  
  t = u->c[0];
  u->c[0] = u->c[3];
  u->c[3] = t;

  t = u->c[1];
  u->c[1] = u->c[2];
  u->c[2] = t;
}

void in_swaps(s)
short *s;
{
  union SWAPS {
    short s;
    unsigned char c[2];
  }*u;
  unsigned char t;

  u = (union SWAPS *)s;
  
  t = u->c[0];
  u->c[0] = u->c[1];
  u->c[1] = t;

}

void in_swapd(d)
double *d;
{
  union SWAPD {
    double d;
    unsigned char c[4];
  }*u;
  unsigned char t;

  u = (union SWAPD *)d;
  
  t = u->c[0];
  u->c[0] = u->c[7];
  u->c[7] = t;

  t = u->c[1];
  u->c[1] = u->c[6];
  u->c[6] = t;

  t = u->c[2];
  u->c[2] = u->c[5];
  u->c[5] = t;

  t = u->c[3];
  u->c[3] = u->c[4];
  u->c[4] = t;
}

void in_swapl(ll)
long *ll;
{
  union SWAPLL {
    long l;
    unsigned char c[4];
  }*u;
  unsigned char t;

  u = (union SWAPLL *)ll;
  
  t = u->c[0];
  u->c[0] = u->c[3];
  u->c[3] = t;

  t = u->c[1];
  u->c[1] = u->c[2];
  u->c[2] = t;
}

void in_swapi(ii)
int *ii;
{
  union SWAPII {
    int i;
    unsigned char c[4];
  }*u;
  unsigned char t;

  u = (union SWAPII *)ii;
  
  t = u->c[0];
  u->c[0] = u->c[3];
  u->c[3] = t;

  t = u->c[1];
  u->c[1] = u->c[2];
  u->c[2] = t;
}


double swapD( d )
double d;
{
  union {
    double d;
    unsigned char c[sizeof(double)];
  }u;
  unsigned char t;

  u.d = d;

  t = u.c[0];
  u.c[0] = u.c[7];
  u.c[7] = t;

  t = u.c[1];
  u.c[1] = u.c[6];
  u.c[6] = t;

  t = u.c[2];
  u.c[2] = u.c[5];
  u.c[5] = t;

  t = u.c[3];
  u.c[3] = u.c[4];
  u.c[4] = t;

  return(u.d);
}

