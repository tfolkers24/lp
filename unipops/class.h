/*
 * Cactus File @(#)class.h	1.1
 *         SID 1.1
 *        Date 07/15/04
 */

#define Oflag (O_RDWR|O_CREAT)
#define Mode  (S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH)

#define NEW_OK 2
#define OLD_OK 1
#define NOT_OK 0
#define MAX_CHAN 2048
#define ZERO_YEAR 2024
#define ZERO_DAY -302
#define ZEROC 273.15
#define NPHASE 2

#define DEGREES2RADIANS(x) ( (x) * M_PI / 180.0 )
