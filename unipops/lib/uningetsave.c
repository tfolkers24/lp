#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>

#include "files.h"
#include "unisdd.h"
#include "../../include/sdd.h"
#include "../../include/header.h"
#include "unipopslib_proto.h"

#define NFILES             10
#define HDR_SIZE          384
#define MAX_DATA_POINTS 16384
#define HDU_FLOAT_SIZE (HDR_SIZE*2 + MAX_DATA_POINTS)

char *sddopen_returns[] = {
	"opened ok",
	"unit = 0",
	"file can't be found/doesn't exist",
	"can't access file with desired mode",
	"attempt to open already open file with unit > 0",
	"files array is full, can't open any more files",
	"open failed, which is bizzare if it passed the access check",
	"getbs error 1, error reading or positioning file",
	"getbs error 2, bootstrap parameters are bizzare"
};

char *getascan_returns[] = {

	"everything went ok", 
	"not open", 
	"problem reading or positioning file", 
	"bootstrap block is weird", 
	"scan not found/ entry is empty", 
	"scan has a weird value or entry is out of range"
};


char *saveascan_returns[] = {
	"everything went ok",
	"file not open",
	"problems reading file or positioning file for read",
	"bootstrap info is bizzare",
	"file index is full, can't store any more",
	"improper location specified on a savebyloc",
	"overwrite is not allowed and attempted to overwrite",
	"modeid returned an error",
	"error writing to file or positioning file for write",
	"warning if overwrite happened and overwrite was allowed",
	"both 7 and 9 occured"
};


int uninget(fname, nsave, head, data)
char *fname;
int nsave;
struct HEADER *head;
float *data;
{
  int flag, unit, ier, maxsize, lrtn;
  double buf[HDU_FLOAT_SIZE];
  float location;
  char name[256], mode[16];
//  extern sddfile file[NFILES];

  if(nsave < 1 || nsave > 16000)
  {
    fprintf(stderr, "Nsave %d, out of range (1 = 16000)\n", nsave);
    return(1);
  }

  unit = 1;
  strcpy(name, fname);
  strcpy(mode, "r ");

  sddopen_(&unit, name, mode, &ier);

  fprintf(stdout, "uninget(): sddopen(): returned: %d; %s\n", ier, sddopen_returns[ier]);

  if(ier == 0)
  {
    location = (float) nsave;		/* nsave value */
    flag = 0;			/* Fetch a nsave entry */
    maxsize = HDU_FLOAT_SIZE;

    getascan_(&unit, &location, &flag, &buf[0], &maxsize, &lrtn);
    fprintf(stdout, "unitget(): getascan_(): returned: %d; %s\n", lrtn, getascan_returns[lrtn]);

    if(lrtn == 0)
    {
				/* Copy parts to my structs */
      bcopy((char *)buf, (char *)head, sizeof(struct HEADER));
      bcopy((char *)buf+sizeof(struct HEADER), (char *)data, head->datalen*sizeof(float));

      head->align3[0] = (double)nsave; /* Set the nsave value in header */
    }

    sddclose_(&unit);

    return(lrtn);
  }
  else
  {
    return(ier);
  }

  return(0);
}


int uninsave(fname, nsave, overwrite, head, data)
char *fname;
int nsave, overwrite;
struct HEADER *head;
float *data;
{
//  int i;
  int unit, flag, ow, lrtn, ier;
  double location;
  double buf[HDU_FLOAT_SIZE];
  char name[256], mode[80];

  bcopy((char *)head, (char *)buf, sizeof(struct HEADER));
  bcopy((char *)data, (char *)buf +sizeof(struct HEADER), head->datalen*sizeof(float));

  unit = 1;
  strcpy(name, fname);
  strcpy(mode, "rw");

  sddopen_(&unit, name, mode, &ier);

  fprintf(stdout, "uninsave(): sddopen(): returned: %d; %s\n", ier, sddopen_returns[ier]);

  if(ier == 0)
  {
    flag = 0;
    location = (double)nsave;
    ow = overwrite;

    saveascan_(&unit, &location, &flag, (double *)head, (double *)buf, &ow, &lrtn);

    fprintf(stdout, "uninsave(): saveascan() returned %d: %s\n", lrtn, saveascan_returns[lrtn]);

    sddclose_(&unit);
  }

  return(0);
}


int unilistsave_(fname)
char *fname;
{
  int ier, lrtn, i, maxsize, n, unit, flag, location;
  char name[256], mode[80];
  double buf[HDU_FLOAT_SIZE];
  struct HEADER head;
  float  loc;
  float scan;
  extern int printScan(struct HEADER *p, int ns);
  extern char *listHeaderN;

  n = 16000;

  unit = 1;
  strcpy(name, fname);
  strcpy(mode, "r ");

  sddopen_(&unit, name, mode, &ier);

  fprintf(stdout, "uninsave(): sddopen(): returned: %d; %s\n", ier, sddopen_returns[ier]);

  fprintf(stdout, "%s\n", listHeaderN);

  for(i=1;i<n;i++)
  {
    location = i;
    scanloc_(&unit, &location, &scan, &lrtn);

    if(scan > 0.0)
    {
      flag = 0;			/* Fetch a nsave entry */
      loc = (float)location;
      maxsize = HDU_FLOAT_SIZE;

      getascan_(&unit, &loc, &flag, &buf[0], &maxsize, &lrtn);

      if(lrtn == 0)
      {
				/* Copy parts to my structs */
        bcopy((char *)buf, (char *)&head, sizeof(struct HEADER));

        head.align3[0] = (double)i-1; /* Set the nsave value in header */

        printScan(&head, 1);
      }
    }
  }

  sddclose_(&unit);

  return(0);
}
