
/* @(#)sddclose.c	5.1 06/22/94 */

/* closes an sdd file */

#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <string.h>

#ifndef __DARWIN__
#include <malloc.h>
#endif
#include <unistd.h>

#include "files.h"
#include "unisdd.h"
#include "../../include/sdd.h"
#include "../../include/header.h"
#include "unipopslib_proto.h"

void sddclose_(unit)
int *unit;
{
  sddfile *fptr, *getslot();

  fptr = getslot(unit);
  if (fptr != 0 && fptr->fd != -1)
  {
    close(fptr->fd);
    free(fptr->index);

    fptr->index = 0;
    fptr->fd    = -1;

    if(*unit > 0)
    {
      fptr->unit = 0;
    }
  } 
}
