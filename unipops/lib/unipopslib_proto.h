/* filefns.c */
void sddinit_(void);
void closeall_(void);
sddfile *getslot(int *unit);
/* getascan.c */
void getascan_(int *unit, float *location, int *flag, double *buf, int *maxsize, int *lrtn);
/* getnindex.c */
void getnindex_(int *unit, int *entry, sdd_index_entry *index, int *nelem, int *lrtn);
/* getsddbs.c */
void getsddbs_(int *unit, int *num_index_rec, int *num_data_rec, int *bytperrec, int *bytperent, int *num_entries_used, int *ier);
/* getsddscanlist.c */
void getsddscanlist(sddfile *fptr, float *rbuf, int *actelem, int *maxelem, int *ier);
/* makesindex.c */
int makesindex(sddfile *fptr);
/* modefn.c */
void modeid_(char *field, short int *id, short int *irtn);
void modedesc_(short int *id, char *desc, short int *irtn);
void modefield_(short int *id, char *field, short int *irtn);
/* scanloc.c */
void scanloc_(int *unit, int *location, float *scan, int *lrtn);
/* sddopen.c */
void sddopen_(int *unit, char *name, char *mode, int *ier);
/* getarec.c */
void getarec_(int *unit, float *location, int *phase_no, int *rec_no, double *buf, int *maxsize, int *lrtn);
/* getbs.c */
int getbs(sddfile *fptr);
/* getnsddindex.c */
void getnsddindex(sddfile *fptr, int *entry, sdd_index_entry *index, int *nelem, int *lrtn);
/* getsddscan.c */
void getsddscan(sddfile *fptr, float *location, int *flag, double *buf, int *maxsize, int *lrtn);
/* getslist.c */
void getslist_(int *unit, float *rbuf, int *actelem, int *maxelem, int *ier);
/* saveascan.c */
void saveascan_(int *unit, double *location, int *flag, double *buf, double *obuf, int *overwrite, int *lrtn);
/* sddclose.c */
void sddclose_(int *unit);
/* swap_endian.c */
void swapdouble(double *dp, int n);
void swapfloat(float *fp, int n);
void swaplong(unsigned int *lp, int n);
void swapshort(unsigned short *sp, int n);
/* uningetsave.c */
int uninget(char *fname, int nsave, struct HEADER *head, float *data);
int uninsave(char *fname, int nsave, int overwrite, struct HEADER *head, float *data);
int unilistsave_(char *fname);
