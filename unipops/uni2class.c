#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#ifndef __DARWIN__
#include <malloc.h>
#endif
#include <math.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#define NEW 1
#define NEW_OK 2
#define NOT_OK 0

#include "structure.h"
#include "header.h"
#include "sdd.h"
#include "sex.h"
#include "caclib_proto.h"
#include "u2c_proto.h"

struct HEADER     head;
float             *data = NULL;


int main(int argc, char *argv[])
{
  char dataFile[256];
  char classFile[256];
  char *getcTime();
  double getTimeD();
  int ent, indx;
  double s,e;
  char buf[80];

  if(argc == 3)
  {
    strcpy(dataFile, argv[1]);
    strcpy(classFile, argv[2]);
  } 
  else
  if(argc == 2)
  {
    strcpy(dataFile, argv[1]);
    printf("Class filename: ");
    scanf("%s", classFile);
  } 
  else
  {
    printf("SDD filename: ");
    scanf("%s", dataFile);
    printf("Class filename: ");
    scanf("%s", classFile);
  }

  printf("CLASS_WRITER: Conversion Started  %s\n", getcTime(0));
  s = getTimeD(0);

  class_init(classFile);

  indx = ent = 0;

  while(next_scan(indx, dataFile) > 0)
  {
    ent += class_write(classFile, &head, data);
    indx++;
  }

  e = getTimeD(0);

  printf("CLASS_WRITER: Conversion Finished %s\n", getcTime(0));

  sexagesimal((e-s), buf, DD_MM_SS);

  printf("CLASS_WRITER: %d Scans Processed in a time of %s\n", ent, buf );

  return(0);
}


int next_scan(int entry, char *filename)
{
  int fd, dirl, bytesinindex, bytesused, max;
  int i,l, sizeofDIR;
  int datalen;
  struct BOOTSTRAP  boot;
  struct DIRECTORY  dir;

  sizeofDIR = sizeof(struct DIRECTORY);
  if(data) {
    free((char *)data);
    data = NULL;
  }

  if( (fd = pdopen(filename))<0)
    return(-1);

  if( read( fd, (char *)&boot, sizeof(struct BOOTSTRAP))<0 ) {
    perror("read boot");
    close(fd);
    return(-1);
  }
  
  if (entry <= 0) {
  /* check bootstrap block */

    if( sizeof(struct DIRECTORY) != boot.bytperent ) {
      fprintf(stderr,"directory sizes dont match\n");
      close(fd);
      return(-1);
    }

    bytesinindex = boot.num_index_rec * boot.bytperrec;
    bytesused = boot.num_entries_used * boot.bytperent;
  
    if( bytesinindex < bytesused+sizeof(struct DIRECTORY) ) {
      fprintf(stderr,"Directory structure of %s is full.\n", filename );
      close(fd);
      return(-1);
    }

    if( boot.typesdd || !boot.version  ) {
      fprintf(stderr, "SDD version or type of %s is invalid.\n", 
        filename );
      close(fd);
      return(-1);
    }

    if( sizeof(struct DIRECTORY) != boot.bytperent ) {
      fprintf(stderr,"File %s has faulty direcory size\n",filename );
      close(fd);
      return(-1);
    }

    if( boot.num_entries_used <= 0 ) {
      fprintf(stderr,"File %s is empty\n", filename);
      close(fd);
      return(-1);
    }
    entry = -entry;
  }
 
  if (entry < boot.num_entries_used) {
    dirl = entry * boot.bytperent + boot.bytperrec;
    lseek( fd, (off_t)dirl, SEEK_SET );
    if(read( fd, (char *)&dir, sizeofDIR ) != sizeofDIR ){
      perror("readdir");
      close(fd);
      return(-1);
    }
  } else {
/*    fprintf(stderr,"Entry %d beyond end of file\n", entry); */
    fprintf(stderr,"\nCLASS_WRITER: End of file reached\n");
    close(fd);
    return(-1);
  }

  max = boot.bytperrec * (dir.start_rec-1);
  lseek( fd, (off_t)max, SEEK_SET ); 
  if(read(fd, (char *)&head, sizeof(struct HEADER) )<0) {
    perror("read header");
    close(fd);
    return(-1);
  }

  datalen = (int)head.datalen;
  data = (float *)malloc( (unsigned)datalen );
  bzero( (char *)data, datalen );

  if( read( fd, (char *)data, datalen) <0 ) {
    perror("read data");
    fprintf(stderr,"Error reading scan %7.2f data\n", dir.scan);
    close(fd);
    return(-1);
  }

				/* now check for abnormal float in data array */
  l = datalen / sizeof(float);

  for(i=0;i<l;i++)
    if(!isnormal( (double)data[i]))
      data[i] = 0.0;
 
  close(fd);
  return((int)head.scan);
}


int pdopen(name)
char *name;
{
  int fd;

  if( (fd = open(name, O_RDONLY, 0666 ))<0) 
  {
    fprintf(stderr, "Unamle to open %s\n", name );
    return(-1);
  }

  return(fd);
}
