/* uni2class.c */
int main(int argc, char *argv[]);
int next_scan(int entry, char *filename);
int pdopen(char *name);
/* class.c */
int julda(int nyr);
void datj(int id, int im, int iy, int *jour);
int class_date(double pops_date);
void add_class_section(long *info, int num, int type, int length);
int eqtogal(double ra, double dec, double *glong, double *glat);
int class_write(char *file, struct HEADER *headp, float *datap);
int class_init(char *file);
