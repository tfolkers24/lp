/*
 * Cactus File @(#)structure.h	1.2
 *         SID 1.2
 *        Date 07/15/04
 */

/* class data structures */

#define REC_LEN 512
#define	ENT_EXT	384
#define	ENT_LEN (32*4)
#define ENT_REC (REC_LEN/ENT_LEN)
#define EXT_LEN (ENT_EXT*ENT_LEN)
#define EIII_FILE "1B  "
#define EEEI_FILE "1A  "

struct	file_index
{
	char	code[4];		/* file type			*/	
	long	next_rec,		/* next free (128 byte) record	*/
		num_ent,		/* entries per extension	*/
		num_ext,		/* active extensions		*/
		next_ent,		/* next free entry		*/
		ext_addr[251];		/* record addrs of extensions	*/
};

#define KIND_SPEC 0			/* Spectroscopy			*/
#define KIND_CONT 1			/* Continuum drift		*/
#define KIND_SKY 2			/* Skydip data			*/
#define KIND_ONOFF 3			/* Continuum on/off		*/

#define UNKNOWN 1			/* coord systems		*/
#define EQUAT 2
#define GALAC 3
#define HORIZ 4
#define PLANET 5
#define GEOCEN 6

struct	index_ent
{
	long	addr,			/* record address of scan	*/
		obs,			/* obs number			*/
		version;		/*   and version		*/
	char	source[12],		/* source, 			*/
		line[12],		/*   line,			*/
		telescope[12];		/*   and telescope names	*/
	long	date_obs,		/* observ and reduction dates	*/
		date_red;		/*   (days from 27 August 2024) */
	float	offsets[2];		/* position offsets		*/
	long	offset_type,		/* 1 Un 2 Eq 3 Ga 4 Ho 5 Pl 6 Ea */
		obs_type,		/* 0: spectrum; 1: continuum 	*/
		quality,
		scan,			/* scan number			*/
		future[12];
};

#define	SPECTRUM "2   "
#define SCAN_LEN 54
struct	scan_ent
{
	char	code[4];		/* scan ID			*/
	long	size_rec,		/* scan size in records		*/
		size,			/* scan size in (4 byte) words	*/
		unused_1,
		data_addr,		/* data offset in words		*/
		data_size,		/* data size in words		*/
		unused_2,
		num_sec,		/* number of header sects	*/
		entry,			/* entry number in index	*/
		sect_info[45];  	/* section codes[num_sec],	*/
					/* section lengths[num_sec],	*/
					/* section addrs[num_sec]	*/	
};

struct	pointer_descr	/* length of sects. if 0, sect not present. 	*/
{
	long	general,
		position,
		spectro,
		fswitch,
		drift,
		beam,
		calibration,
		skydip,
		gauss,
		hfs,
		shell,
		continuum,
		history,
		plot,
		base,
		xcoord;
};

#define	GENERAL_SECT -2
#define	GENERAL_LEN 9
struct	general_descr			/* always present		*/
{
	double	ut_time,		/* (seconds)			*/
		lst_time;
	float	azimuth,		/* (radians)			*/
		elevation,
		tau,			/* opacity			*/
		tsys,			/* system temperature (K)	*/
		integration;		/* integration time (minutes)	*/
};

#define P_NONE 0			/* Projections			*/
#define P_GNOMONIC 1			/* Radial Tangent plane		*/
#define P_ORTHO 2			/* Dixon Tangent plane		*/
#define P_AZIMUTHAL 3			/* Schmidt Tangent plane	*/
#define P_STEREO 4			/* Stereographic		*/
#define P_LAMBERT 5			/* Lambert equal area		*/
#define P_AITOFF 6			/* Aitoff equal area		*/
#define P_RADIO 7		/* Classic Single dish radio mapping	*/

#define	POSITION_SECT -3
#define	POSITION_LEN 11
struct	position_descr
{
	char	source[12];		/* source name			*/
	float	epoch;			/* coordinate epoch		*/
	double	lambda,			/* coordinates			*/
		beta;
	float	lambda_off,		/* coordinate offsets		*/
		beta_off;
	long	projection;		/* projection system		*/
};

#define VUNK 0		/* velocity references: unknown (planetary...)	*/
#define VLSR 1
#define VHEL 2				/* Heliocentric			*/
#define VOBS 3				/* Observatory (telescope)	*/
#define VEAR 4				/* Earth-Moon barycenter	*/
#define VAUT -1				/* from data			*/

#define	SPECTRO_SECT -4
#define	SPECTRO_LEN 18
struct	spectro_descr			/* spectroscopy	*/
{					/* 	spectra only		*/
	char	line[12];		/* line name			*/
	double	rest;			/* rest frequency		*/
	long	nchan;			/* number of channels		*/
	float	reference,		/* reference channel		*/
		fres,			/* frequency resolution		*/
		foff,			/* frequency offset		*/
		vres,			/* velocity resolution		*/
		voff,			/* velocity at reference chan	*/
		bad;			/* blanking value		*/
	double	image;			/* image frequency		*/
	long	velocity;		/* 0 Unkn 1 LSR 2 Hel. 3 Ear. 	*/
	double	sky;			/* sky frequency		*/
	float	vteles;			/* velocity of telescope rel	*/
};

#define	BASE_SECT -5
#define	MWIND 20
#define	BASE_LEN 4+2*MWIND
struct	base_descr			/* baseline 			*/	
{					/* 	spectra or drifts	*/
	long	degree;			/* degree of last baseline	*/
	float	sigma,			/* sigma			*/
		area;			/* area under windows		*/
	long	nwindow;		/* number of line windows	*/
	float	wind1[MWIND],		/* lower limits of windows	*/
		wind2[MWIND];		/* upper limits of windows	*/
};

#define	HISTORY_SECT -6
#define	MSEQ 100
#define	HISTORY_LEN 1+2*MSEQ
struct	history_descr			/* orig scan nums		*/
{
	long	sequence,		/* number of sequences		*/
		start[MSEQ],		/* start scan number of seq.	*/
		end[MSEQ];		/* end scan number of seq.	*/
};

#define	PLOT_SECT -7
#define	PLOT_LEN 4
struct	plot_descr			/* default plot limits		*/
{
	float	ymin,			/* min y value plotted		*/
		ymax,			/* max y value plotted		*/
		xmin,			/* min x value plotted		*/
		xmax;			/* max x value plotted		*/
};

#define MOD_FREQ 0			/* Frequency switch		*/
#define MOD_POS 1			/* Position switch		*/
#define MOD_FOLD 2			/* Folded frequency switch	*/

#define	FSWITCH_SECT -8
#define	MXPHAS 8
#define	FSWITCH_LEN 2+6*MXPHAS
struct	fswitch_descr			/* freq & pos switch		*/
{					/* 	spectra only		*/
	long	nphase;			/* number of phases		*/
	double	decalage[MXPHAS];	/* frequency offsets		*/
	float	duree[MXPHAS],		/* time per phase		*/
		poids[MXPHAS];		/* weight of each phase		*/
	long	mode;			/* switch mode (freq , pos)	*/
	float	lamoff[MXPHAS],		/* lambda offsets		*/
		betoff[MXPHAS];		/* beta offsets of each phase	*/
};

#define	CALIBRATION_SECT -14
#define	CALIBRATION_LEN 21
struct	calibration_descr
{
	float	beam_eff,		/* beam efficiency		*/
		forw_eff,		/* forward efficiency		*/
		gain_im,		/* image/signal gain ratio	*/
		h2omm,			/* mm of water vapor		*/
		pamb,			/* ambient pressure (hpa)	*/
		tamb,			/* ambient temperature (K)	*/
		tatmsig,		/* atmosphere temp. signal band	*/
		tchop,			/* chopper temperature		*/
		tcold,			/* cold load temperature	*/
		tausig,			/* opacity signal band		*/
		tauima,			/* opacity image band		*/
 		tatmima,		/* atmosphere temp. image band	*/
		trec;			/* receiver temperature		*/
	long	mode;			/* calibration mode		*/
	float	factor,			/* applied calibration factor	*/
		altitude,		/* site elevation (m)		*/
		counts[3],		/* power of atm., chopp., cold	*/
		lamoff,			/* long offset for sky measure	*/
		betoff;			/* lat offset for sky measure	*/
};


#define	SKYDIP_SECT -16
#define	MSKY 10
#define	SKYDIP_LEN (10+4*MSKY)
struct	skydip_descr			/* skydips 			*/
{					/*	no associated data	*/
	char	line[12];		/* line name			*/
	double	rest,			/* rest frequency		*/
		image;			/* image frequency		*/
	long	nsky,			/* number of points on sky	*/
		nchop,			/*    -   -    -    - chopper	*/
		ncold;			/*    -   -    -    - cold load	*/
	float	elevation[MSKY],	/* elevations			*/
		emission [MSKY],	/* power on sky			*/
		chopper  [MSKY],	/* power on chopper		*/
		cold [MSKY];		/* power on cold load		*/
};

#define	GAUSS_SECT -9
#define	MXGAUS 5
#define	GAUSS_LEN (3+6*MXGAUS)
struct	gauss_descr			/* fit results			*/
{					/* 	spectra or drifts	*/
	long	nline;			/* number of components		*/
	float	sigma_base,		/* sigma on base		*/
		sigma_raie,		/* sigma on line		*/
		fit[3*MXGAUS],		/* fit results			*/
		err[3*MXGAUS];		/* errors			*/
};

#define	SHELL_SECT -12
#define	SHELL_LEN 43
struct	shell_descr		/* "stellar shell" profile fit result	*/
{					/* 	spectra only		*/
	long	nline;			/* number of components		*/
	float	sigma_base,		/* sigma on base		*/
		sigma_raie,		/* sigma on line		*/
		fit[20],		/* fit results			*/
		err[20];		/* errors			*/
};

#define	HFS_SECT -13
#define	HFS_LEN 27
struct	hfs_descr		/* "hyperfine struct" (nh3, hcn) fit	*/
{					/* 	spectra only		*/
	long	nline;			/* number of components		*/
	float	sigma_base,		/* sigma on base		*/
		sigma_raie,		/* sigma on line		*/
		fit[12],		/* fit results			*/
		err[12];		/* errors			*/
};

#define	DRIFT_SECT -10
#define	DRIFT_LEN 16
struct	drift_descr			/* continuum drifts	*/
{					/* 	drifts only		*/
	double	rest;			/* rest frequency		*/
	float	width;			/* bandwidth			*/
	long	npoint;			/* number of data points	*/
	float	reference,		/* reference point		*/
		timoff,			/* time at reference		*/
		angoff,			/* angular offset at ref.	*/
		pa,			/* position angle of drift	*/
		timres,			/* time resolution		*/
		angres,			/* angular resolution		*/
		bad;			/* blanking value		*/
 	long	system;			/* type of offsets		*/
	double	image;			/* image frequency		*/
	float	colla,			/* collimation error az		*/
		colle;			/* collimation error el		*/
};

#define	BEAM_SECT -11
#define	BEAM_LEN 5
struct	beam_descr			/* beam switch 			*/
{					/*	spectra or drifts	*/
	float	azimuth,		/* observation azimuth		*/
		elevation,		/* observation elevation	*/
		throw,			/* beam spacing			*/
		pa;			/* position angle of beams	*/
	long	system;			/* system for angle		*/
};

#define	CONT_SECT -15
#define	CONT_LEN 19
struct	cont_descr		/* doub gauss & base fit results	*/
{					/* 	drifts only		*/
	long	nline;			/* number of components		*/
	float	sigma_base,		/* sigma on base		*/
		sigma_raie,		/* sigma on line		*/
		fit[8],			/* results			*/
		err[8];			/* errors			*/
};
