#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <math.h>
#ifndef __DARWIN__
#include <malloc.h>
#endif
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>

#include "header.h"
#include "sdd.h"
#include "caclib_proto.h"

#define SIZEOFDIR (sizeof(struct DIRECTORY))

#define MAX_SCANS 16384

int swapThisSdd = 0;
struct HEADER     *sa;

static int pdopen();

int               datalen;

struct HEADER head;
float         data[4096*4];

/*
 * Cactus File @(#)swapdatum.c	1.3
 */

/*
 * Convert between EEEI and IEEE byte order
 * The routines are symmetrical so they do both EEEI -> IEEE and IEEE -> EEEI
 *
 * These return the converted number and don't change the original:
 * swapd(&x) 	convert a double
 * swapf(&x) 	convert a float
 * swapl(&x) 	convert a unsigned int (old long)
 * swapi(&x) 	convert an int
 * swaps(&x) 	convert an unsigned short
 *  
 * These are called with the address of the original and change it.  (No return)
 * in_swapd(&x) 	convert a double
 * in_swapf(&x) 	convert a float
 * in_swapl(&x) 	convert a unsigned int (old long)
 * in_swapi(&x) 	convert an int
 * in_swapus(&x) 	convert an unsigned short
 * in_swaps(&x) 	convert a signed short (Actually the same as unsigned)
 *
 * swapD(x)	same as swapd but takes a value not a pointer as an argument
 */

double swapd( d )
double *d;
{
  union {
    double d;
    unsigned char c[sizeof(double)];
  }u;
  unsigned char t;

  u.d = *d;

  t = u.c[0];
  u.c[0] = u.c[7];
  u.c[7] = t;

  t = u.c[1];
  u.c[1] = u.c[6];
  u.c[6] = t;

  t = u.c[2];
  u.c[2] = u.c[5];
  u.c[5] = t;

  t = u.c[3];
  u.c[3] = u.c[4];
  u.c[4] = t;

  return(u.d);
}

float swapf(f)
float *f;
{
  union {
    float f;
    unsigned char c[4];
  }u;
  unsigned char t;

  u.f = *f;
  
  t = u.c[0];
  u.c[0] = u.c[3];
  u.c[3] = t;

  t = u.c[1];
  u.c[1] = u.c[2];
  u.c[2] = t;

  return(u.f);
}

unsigned int swapl(l)
unsigned int *l;
{
  union {
    unsigned int l;
    unsigned char c[4];
  }u;
  unsigned char temp;

  u.l = *l;
  temp = u.c[0];
  u.c[0] = u.c[3];
  u.c[3] = temp;

  temp = u.c[1];
  u.c[1] = u.c[2];
  u.c[2] = temp;

  return(u.l);
}

int swapi(l)
int *l;
{
  union {
    int l;
    unsigned char c[4];
  }u;
  unsigned char temp;

  u.l = *l;
  temp = u.c[0];
  u.c[0] = u.c[3];
  u.c[3] = temp;

  temp = u.c[1];
  u.c[1] = u.c[2];
  u.c[2] = temp;

  return(u.l);
}

unsigned short swaps(s)
unsigned short *s;
{
  union {
    unsigned s;
    unsigned char c[2];
  }u;
  unsigned char temp;

  u.s = *s;
  temp = u.c[0];
  u.c[0] = u.c[1];
  u.c[1] = temp;

  return(u.s);
}

void swapus(s)
unsigned short *s;
{
  union SWAPS {
    unsigned short *s;
    unsigned char c[2];
  }*u;
  unsigned char t;

  u = (union SWAPS *)s;
  
  t = u->c[0];
  u->c[0] = u->c[1];
  u->c[1] = t;
}

void in_swapf(f)
float *f;
{
  union SWAPF {
    float f;
    unsigned char c[4];
  }*u;
  unsigned char t;

  u = (union SWAPF *)f;
  
  t = u->c[0];
  u->c[0] = u->c[3];
  u->c[3] = t;

  t = u->c[1];
  u->c[1] = u->c[2];
  u->c[2] = t;
}

void in_swaps(s)
short *s;
{
  union SWAPS {
    short s;
    unsigned char c[2];
  }*u;
  unsigned char t;

  u = (union SWAPS *)s;
  
  t = u->c[0];
  u->c[0] = u->c[1];
  u->c[1] = t;

}

void in_swapd(d)
double *d;
{
  union SWAPD {
    double d;
    unsigned char c[4];
  }*u;
  unsigned char t;

  u = (union SWAPD *)d;
  
  t = u->c[0];
  u->c[0] = u->c[7];
  u->c[7] = t;

  t = u->c[1];
  u->c[1] = u->c[6];
  u->c[6] = t;

  t = u->c[2];
  u->c[2] = u->c[5];
  u->c[5] = t;

  t = u->c[3];
  u->c[3] = u->c[4];
  u->c[4] = t;
}

void in_swapl(ll)
unsigned int *ll;
{
  union SWAPLL {
    unsigned int l;
    unsigned char c[4];
  }*u;
  unsigned char t;

  u = (union SWAPLL *)ll;
  
  t = u->c[0];
  u->c[0] = u->c[3];
  u->c[3] = t;

  t = u->c[1];
  u->c[1] = u->c[2];
  u->c[2] = t;
}

void in_swapi(ii)
int *ii;
{
  union SWAPII {
    int i;
    unsigned char c[4];
  }*u;
  unsigned char t;

  u = (union SWAPII *)ii;
  
  t = u->c[0];
  u->c[0] = u->c[3];
  u->c[3] = t;

  t = u->c[1];
  u->c[1] = u->c[2];
  u->c[2] = t;
}


double swapD( d )
double d;
{
  union {
    double d;
    unsigned char c[sizeof(double)];
  }u;
  unsigned char t;

  u.d = d;

  t = u.c[0];
  u.c[0] = u.c[7];
  u.c[7] = t;

  t = u.c[1];
  u.c[1] = u.c[6];
  u.c[6] = t;

  t = u.c[2];
  u.c[2] = u.c[5];
  u.c[5] = t;

  t = u.c[3];
  u.c[3] = u.c[4];
  u.c[4] = t;

  return(u.d);
}


/* swapsdd.c */

int swapBoot(boot)
struct BOOTSTRAP  *boot;
{
  int i;
  unsigned int *p;

  if(boot->version != 1)
  {
    swapThisSdd = 1;

    for(i=0,p=(unsigned int *)&boot->num_index_rec;i<8;i++,p++)
    {
      in_swapl(p);
    }
  }
  else
  {
    swapThisSdd = 0;
  }

  return(0);
}


int swapDir(dir)
struct DIRECTORY *dir;
{

  in_swapl(&dir->start_rec);
  in_swapl(&dir->end_rec);

  in_swapf(&dir->h_coord);
  in_swapf(&dir->v_coord);
 
/* leave the char "source" alone */

  in_swapf(&dir->scan);
  in_swapf(&dir->freq_res);

  in_swapd(&dir->rest_freq);

  in_swapf(&dir->lst);
  in_swapf(&dir->ut);

  in_swaps(&dir->obsmode);
  in_swaps(&dir->phase_rec);
  in_swaps(&dir->pos_code);
  in_swaps(&dir->unused);

  return(0);
}


int swapHeader(head)
struct HEADER *head;
{
  int j;
  double *d;
  short  *s;

  for(j=0,s=&head->headcls;j<16;j++,s++)
  {
    in_swaps(s);
  }

/* class 1 */

  in_swapd(&head->headlen);
  in_swapd(&head->datalen);
  in_swapd(&head->scan);
  /* leave the chars alone */

/* class 2 */

  for(j=0,d=&head->xpoint;j<12;j++,d++)
  {
    in_swapd(d);
  }
  /* leave the chars alone */

/* class 3 */

  for(j=0,d=&head->utdate;j<8;j++,d++)
  {
    in_swapd(d);
  }
  /* leave the chars alone */

/* class 4 */

  for(j=0,d=&head->epoch;j<16;j++,d++)
  {
    in_swapd(d);
  }
  /* leave the chars alone */

/* class 5 */

  for(j=0,d=&head->tamb;j<6;j++,d++)
  {
    in_swapd(d);
  }

/* class 6 */

  for(j=0,d=&head->scanang;j<10;j++,d++)
  {
    in_swapd(d);
  }
  /* leave the chars alone */

/* class 7 */

  for(j=0,d=&head->bfwhm;j<5;j++,d++)
  {
    in_swapd(d);
  }
  /* leave the chars alone */

/* class 8 */

  for(j=0,d=&head->appeff;j<5;j++,d++)
  {
    in_swapd(d);
  }

/* class 9 */

  for(j=0,d=&head->synfreq;j<25;j++,d++)
  {
    in_swapd(d);
  }

/* class 10 */

  for(j=0,d=head->openpar;j<10;j++,d++)
  {
    in_swapd(d);
  }

/* class 11 */

  for(j=0,d=&head->current_disk;j<15;j++,d++)
  {
    in_swapd(d);
  }
  /* leave the chars "linename" alone */
  for(j=0,d=&head->refpt_vel;j<13;j++,d++)
  {
    in_swapd(d);
  }

  for(j=0,d=head->spares05;j<6;j++,d++)
  {
    in_swapd(d);
  }

/* class 12 */

  for(j=0,d=&head->obsfreq;j<20;j++,d++)
  {
    in_swapd(d);
  }

  /* leave the chars "polariz" alone */

  in_swapd(&head->effint);

  /* leave the chars "rx_info" alone */

/* class 13 */

  for(j=0,d=&head->nostac;j<11;j++,d++)
  {
    in_swapd(d);
  }

  return(0);
}



int swapData(data, n)
float *data;
int n;
{
  int i;
  float *f;

  f = data;

  for(i=0;i<n;i++,f++)
  {
    in_swapf(f);
  }

  return(0);
}


/*  read_scan_ds.c */

int checkBoot(b, fn)
struct BOOTSTRAP *b;
char *fn;
{
  int bytesinindex, bytesused;

  if( SIZEOFDIR != b->bytperent ) {
    log_msg("directory sizes dont match");
    return(1);
  }

  bytesinindex = b->num_index_rec * b->bytperrec;
  bytesused = b->num_entries_used * b->bytperent;
 
  if( bytesinindex < bytesused + SIZEOFDIR )
  {
    log_msg("Dir struct of %s, full.", fn );

    return(1);
  }

  if( b->typesdd || !b->version)
  {
    log_msg("SDD version or type of %s is invalid.", fn );

    return(1);
  }

  if( SIZEOFDIR != b->bytperent)
  {
    log_msg( "File %s has faulty direcory size, can't verify", fn );

    return(1);
  }

  if( b->num_entries_used <= 0)
  {
    log_msg( "File %s is empty", fn );

    return(1);
  }

  return(0);
}


int read_scan(scan, filename, data, head)
float scan;
char *filename;
float *data;
struct HEADER *head;
{
  int fd, dirl, num, max;
  struct BOOTSTRAP boot;
  struct DIRECTORY dir;
  int i, l;

  if((fd = pdopen(filename))<0)
  {
    return(-1);
  }

  if(read( fd, (char *)&boot, sizeof(struct BOOTSTRAP))<0 )
  {
    log_perror("read boot");
    close(fd);

    return(-1);
  }
  
  swapBoot(&boot);

  if(checkBoot(&boot, filename))
  {
    close(fd);

    return(-1);
  }

  if(scan < 0.0)
  {
    dirl = (boot.num_entries_used-1) * boot.bytperent + boot.bytperrec;
    lseek( fd, (off_t)dirl, SEEK_SET );

    if(read( fd, (char *)&dir, SIZEOFDIR )!= SIZEOFDIR )
    {
      log_perror("readdir");
      close(fd);

      return(-1);
    }

    if(swapThisSdd)
    {
      swapDir(&dir);
    }

    close(fd);

    return((int)dir.scan);
  }

  num = boot.num_entries_used;
  while(num >= 0)
  {
    dirl = num * boot.bytperent + boot.bytperrec;
    lseek( fd, (off_t)dirl, SEEK_SET );

    if(read( fd, (char *)&dir, SIZEOFDIR ) != SIZEOFDIR )
    {
      log_perror("readdir");
      close(fd);

      return(-1);
    }

    if(swapThisSdd)
    {
      swapDir(&dir);
    }

    if( scan == dir.scan )
    {
      break;
    }

    num--;
  }

  if( num < 0)
  {
    log_msg("Scan #%.2f NOT in file %s", scan, filename);
    close(fd);

    return(-1);
  }
  
  max = boot.bytperrec * (dir.start_rec-1);
  lseek( fd, (off_t)max, SEEK_SET ); 

  if(read(fd, (char *)head, sizeof(struct HEADER) )<0)
  {
    perror("read header");
    close(fd);

    return(-1);
  }

  if(swapThisSdd)
  {
    swapHeader(head);
  }

  datalen = (int)head->datalen;
  bzero((char *)data, datalen );

  if( read( fd, (char *)data, datalen) <0)
  {
    perror("read data");
    log_msg("Error reading scan %7.2f data", scan);
    close(fd);

    return(-1);
  }

  if(swapThisSdd)
  {
    swapData(data, datalen / 4);
  }

				/* now check for abnormal float in data array */
  l = datalen / sizeof(float);

  for(i=0;i<l;i++)
  {
    if(isnan( (double)data[i]))
    {
      data[i] = 0.0;
    }
  }
 
  close(fd);

  return(0);
}


static int pdopen(name)
char *name;
{
  int fd;

  if( (fd = open(name, O_RDONLY, 0666 ))<0) 
  {
    log_msg("Unable to open %s", name );

    return(-1);
  }

  return(fd);
}



int findSize(fname)
char *fname;
{
  int fd, n;
  struct BOOTSTRAP  boot;

  if( (fd = pdopen(fname))<0)
  {
    return(-1);
  }

  if( read( fd, (char *)&boot, sizeof(struct BOOTSTRAP))<0)
  {
    perror("read boot");
    close(fd);

    return(-1);
  }

  swapBoot(&boot);

  close(fd);

  n = boot.num_entries_used+1;

  if(sa)
  {
    free(sa);
  }

  if( (sa = (struct HEADER *)calloc(n, sizeof(struct HEADER))) == NULL)
  {
    fprintf(stderr,"Error in findSize( malloc() )\n");

    return(-1);
  }

  return(0);
}



int findScans(fname)
char *fname;
{
  int fd, max, dirl, num, j = 0;
  int sizeofDIR;
  struct BOOTSTRAP  boot;
  struct DIRECTORY  dir;
  struct HEADER     head;

  sizeofDIR = sizeof(struct DIRECTORY);

  if((fd = pdopen(fname))<0)
  {
    return(-1);
  }

  if(read( fd, (char *)&boot, sizeof(struct BOOTSTRAP))<0)
  {
    perror("read boot");
    close(fd);

    return(-1);
  }

  swapBoot(&boot);
  /* check bootstrap block */

  checkBoot(&boot, fname);

  num = 0;
  while(num < boot.num_entries_used)
  {
    dirl = num * boot.bytperent + boot.bytperrec;
    lseek( fd, (off_t)dirl, SEEK_SET );

    if(read( fd, (char *)&dir, sizeofDIR ) != sizeofDIR)
    {
      perror("readdir");
      close(fd);

      return(-1);
    }

    if(swapThisSdd)
    {
      swapDir(&dir);
    }

    max = boot.bytperrec * (dir.start_rec-1);
    lseek( fd, (off_t)max, SEEK_SET );

    if(read(fd, (char *)&head, sizeof(struct HEADER) )<0)
    {
      perror("read header");
      close(fd);

      return(-1);
    }

    if(swapThisSdd)
    {
      swapHeader(&head);
    }

    head.align3[0] = (double)num; /* Save the directory entry here */

    bcopy((char *)&head, (char *)&sa[j], sizeof(struct HEADER));

    num++;
    j++;

    if(num > MAX_SCANS)
    {
      fprintf(stderr, "File %s contains too many scans\n", fname);

      return(num);
    }
  }

  close(fd);

  return(j);
}


int convertSdd(in, out, nos)
char *in, *out;
int nos;
{
  int i, ret;
  float scan;

  for(i=0;i<nos;i++)
  {
    scan = sa[i].scan;

    log_msg("Attempting to read scan %.2f", scan);

    ret = read_scan(scan, in, &data, &head);

    log_msg("read_scan returned: %d", ret);

    if(!ret)
    {
      log_msg("Writing scan %.2f to %s", scan, out);

      write_sdd(out, &head, (char *)&data);
    }
  }

  return(0);
}



int main(argc, argv)
int argc;
char *argv[];
{
  int nos;
  char inFile[256], outFile[256], sysBuf[256];
  
  if(argc < 3)
  {
    fprintf(stderr, "Usage: %s sun_sdd_file_in linux_sdd_file_out\n", argv[0]);
    exit(1);
  }

  strcpy(inFile,  argv[1]);
  strcpy(outFile, argv[2]);

  findSize(inFile);

  nos = findScans(inFile);

  log_msg("Found %d scans in %s", nos, inFile);

  /* delete output file */
  sprintf(sysBuf, "rm -i %s", outFile);
//  system(sysBuf);

  new_sdd(outFile);

  convertSdd(inFile, outFile, nos);

  return(0);
}
