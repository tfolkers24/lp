
#include "header.h"
//#include "fbotf.h"

#define MAXFLY  10000 /* Maximum no. of on-the-fly data points */

struct VERS {
  unsigned short num; 
  unsigned short index;
};

struct SCAN {
  struct HEADER head;
  int valid;
};

struct OTFSCAN {
  int valid;
  int be;
  struct OTFMSG *ifs[8]; /* locked memory, include the entire data array */
  int iflen[8];          /* needed because munlock needs it */
  long *time;
  int baseif;            /* needed to determine proper if num for MAC */
  struct HEADER head;
};


struct QUEUEMSG {
  struct QUEUEMSG *next;
  int len;
  int dev; /* last_msg() */
  char *msg;
};


#ifndef LINEFLY
struct LINEFLY {
  int scan;              /* scan number for this computation  */
  float lst[MAXFLY];     /* LST corresponding to data taker (hours) */
  float ra[MAXFLY];      /* Corresponding RA offsets (degrees) */
  float dec[MAXFLY];     /* Corresponding Dec. offsets */
  float az[MAXFLY];      /* Corresponding Az. offsets */
  float el[MAXFLY];      /* Corresponding El. offsets */
};
#endif
