procedure yfact
#
# Usage: yfact vane_scan_num sky_scan_num
# This procedure requires 3 args
# Remember; the first arg in the command itself
#
ntoken? 2
#
h1
gget secondArg
#
h2
gget thirdArg
#
image /y h1 h2
show
#
end
