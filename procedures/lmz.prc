procedure lmz
#
# Usage: lmz normal_freq shifted_freq [/q] [/p]
# This procedure requires 2 args
# Remember; the first arg in the command itself
#
ntoken? 2
#
#saveenv
#
# Zero out images
#
image /z /a
#
freq secondArg
#
h1
acc
peak
#
freq thirdArg
h2
acc
autoshift h1 /d
peak
#
image /s h1 h2
h0
xx
peak
#
image /l
#
#restore
