procedure beffu
#
# Usage: beffu bscan escan rej1 rej2 [/q] [/p]
# This procedure requires 4 args
# Remember; the first arg in the command itself
#
ntoken? 4
# Save the environment so this procedure doesn't 
# affect any parameters otherwise set
saveenv
#
rejection fourthArg fourthArg fifthArg fifthArg
# Set parameters
mode cont
nbeff = 4
plottmb = 1
feed 0
calmode none
date *
# Clear out the object database
object /i
#
bscan secondArg
escan thirdArg
source  *
freq *
# Accum the USB scans one IF at a time
c2
# Now, before saving the first results compute planet flux
# Compute plantary values
# This now saves the expect Tmb for matching planets
source /a
freq = restfreq / 1000.0
date /a
plan
# Save the above results
object /r
# Force a Non-Reset
accumreset=2
# Accum the USB scans one IF at a time
c4
# Save the above results
object /r
# Set axis lables
accaxis num tastar
# Review the database for Current Object
object /s
# restore the environment
restore
