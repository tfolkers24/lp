procedure beff4r
#
# Usage: beff4r bscan escan h-lsb-rej h-usb-rej v-lsb-rej v-usb-rej [/q] [/p]
# This procedure requires at least 6 args
# Remember; the first arg in the command itself
#
ntoken? 6
# Set these before saving env
#
rejection fourthArg fifthArg sixthArg seventhArg
#
# Save the environment so this procedure doesn't 
# affect any parameters otherwise set
saveenv
# Set parameters
mode cont
nbeff = 4
plottmb = 1
feed 0
calmode none
delfreq 0.50
date *
# Clear out the object database
object /i
#
source  *
#
bscan = secondArg
escan = thirdArg
# Set USB Freq
freq *
# Accum the USB scans one IF at a time
c2
# Now, before saving the first results compute planet flux
# Compute plantary values
# This now saves the expect Tmb for matching planets
source /a
freq = restfreq / 1000.0
plan
# Save the above results
object /r
# Force a Non-Reset
accumreset=2
# Accum the USB scans one IF at a time
c4
# Save the above results
object /r
# Force a Non-Reset
accumreset=2
# Set LSB Freq
freq *
# Accum the LSB scans one IF at a time
c1
# Compute plantary values
# This now saves the expect Tmb for matching planets
source /a
freq = restfreq / 1000.0
plan
# Save the above results
object /r
# Force a Non-Reset
accumreset=2
# Accum the LSB scans one IF at a time
c3
# Save the above results
object /r
# Set axis lables
accaxis num tastar
# Review the database for Current Object
object /s
# restore the environment
restore
