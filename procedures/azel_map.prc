procedure azelmap
#
# Process a beam map
# Usage: azelmap bscan escan freq source feed nbase
# This procedure requires 6 args
# Remember; the first arg in the command itself
#
ntoken? 6
#
saveenv
#
linearplot
dobaseline = 1
calmode none
bscan  secondArg
escan  thirdArg
freq fourthArg
delfreq 0.500000
source fifthArg
feed sixthArg
nbase  seventhArg
mapangle 180 90
#
# Grid the map
#
say "Gridding Map with Delay"
gridmap /d
#
# Compute the gaussian profiles
#
say "Computing Map Gaussian Profiles"
mapgauss
#
# Display the map
#
say "Displaying Map"
plotmap
#
restore
#
end
