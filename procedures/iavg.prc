procedure iavg
#
# Usage: iavg normal_freq shifted_freq [/q] [/p]
# This procedure requires 2 args
# Remember; the first arg in the command itself
#
# Shift it image freq 2X the wrong way so as to average
# up the images lines and smear the signal lines.
# Requires doctoring up the header so sum will work
#
ntoken? 2
#
saveenv
#
freq secondArg
#
h1
acc
peak
#
freq thirdArg
h2
acc
autoshift /o h1
peak
#
image /s h1 h2
h0
xx
peak
#
image /l
#
restore
