procedure lmz3
#
# Usage: lmz normal_freq shifted_freq1 shifted_freq2 [/q] [/p]
# This procedure requires 3 args
# Remember; the first arg in the command itself
#
# This proc assumes you have the baseline regions defined
#
ntoken? 3
#
#saveenv
#
# Zero out images
#
image /z /a
#
freq secondArg
#
h1
accum /q
baseline
peak
#
freq thirdArg
h2
accum /q
#
# Shift this array to match h1
#
autoshift h1 /d
baseline
peak
#
freq fourthArg
h3
accum /q
#
# Shift this array to match h1
#
autoshift h1 /d
baseline
peak
#
# Now sum the all up
#
# First sum the rest + shifted_freq1 and place in h0
#
image /s h1 h2
#
# Next sum the first summation + shifted_freq2 and place in h0
#
image /s h0 h3
#
h0
xx
peak
#
image /l
#
#restore
