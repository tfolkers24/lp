procedure gdiff
#
# Usage: gdiff first scan second scan
# This procedure requires 3 args
# Remember; the first arg in the command itself
#
ntoken? 2
#
h1
gget secondArg
show
#
h2
gget thirdArg
show
#
image /d h1 h2
xx
#
end
