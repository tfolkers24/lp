procedure beff-lsb
#
# Usage: beff-lsb bscan escan source freq rej1 rej2 [/q] [/p]
# This procedure requires 6 args
# Remember; the first arg in the command itself
#
ntoken? 6
#
# Set these before saving env
#
rejection sixthArg sixthArg seventhArg seventhArg
#
# Save the environment so this procedure doesn't 
# affect any parameters otherwise set
#
saveenv
#
# Set parameters
#
mode cont
nbeff = 2
feed 0
calmode none
date *
bscan secondArg
escan thirdArg
source  fourthArg
freq fifthArg
#
# Accum the scans one IF at a time
#
c13
#
# Set axis lables
#
accaxis num tastar
#
# Compute plantary values
# This now saves the expect Tmb for matching planets
#
plan
#
# Clear out the object database
#
object /i
#
# Save the above results
#
object /r
#
# Review the database for Current Object
#
object /s
#
# restore the environment
#
restore
