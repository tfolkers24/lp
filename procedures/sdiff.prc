procedure sdiff
#
# Usage: sdiff first scan second scan
# This procedure requires 3 args
# Remember; the first arg in the command itself
#
ntoken? 2
#
h1
get secondArg
show
#
h2
get thirdArg
show
#
image /d h1 h2
xx
#
end
