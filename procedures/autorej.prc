procedure autorej
#
# Usage: autorej ratio
# This procedure requires 1 Arg
# Remember; the first arg in the command itself
#
ntoken? 1
#
ignore /c
#
stack /f
stack /l
#
startstack
  get stackscan
  show
  base
  rms
  selfrej secondarg
  delay 1000000
  stack /i
endstack
#
print acount
#
end
