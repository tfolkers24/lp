#
# Add linuxpops to PATH
#
LINUXPOPS_HOME="/home/analysis/linuxpops"
export LINUXPOPS_HOME
#
if ! [[ "$PATH" =~ "$LINUXPOPS_HOME/bin:" ]]
then
    PATH="$LINUXPOPS_HOME/bin:$PATH"
fi
export PATH
