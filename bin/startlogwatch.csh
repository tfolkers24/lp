#!/bin/csh -f
#
if ( "$1" == "" || "$2" == "" ) then
  echo "Usage $0 pid session"
  exit
endif
#
xterm -tn $1 -geometry 165x36+450+530 -e tail -f /tmp/$2/logger.log /tmp/$2/logger.stdout.log /tmp/$2/xgraphic.stdout.log &
#
exit
