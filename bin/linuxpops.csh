#
if ( "$?LINUXPOPS_HOME" == 0 ) then
  echo "Must define LINUXPOPS_HOME"
  exit
endif
#
if ("$?OBSINIT" == 0 ) then
  setenv OBSINIT sys
endif
#
if ("$?OBSDAT" == 0 ) then
  setenv OBSDAT /home/data/"$OBSINIT"
endif
#
setenv LPMEMORY "$HOME"/"$OBSINIT"/LPMEMORY-64

if ( "$?PRINTER" == 0 ) then
  setenv PRINTER lp
endif
#
setenv LINUXPOPS_PROCS "$LINUXPOPS_HOME"/procedures
setenv LPSETUP "$LINUXPOPS_PROCS"/batch.init
setenv LINUXPOPS_HELP_DIR "$LINUXPOPS_HOME"/help
setenv DEMO_DIR "$LINUXPOPS_HOME"/demo
setenv LINUXPOPS_DOC_VIEWER atril
#
# For MacOS
#
setenv DYLD_FALLBACK_LIBRARY_PATH /usr/local/mysql/lib
#
if ( "$?XMGRACE" == 0 ) then
  if ( -e /usr/local/grace/bin/xmgrace ) then
    setenv XMGRACE /usr/local/grace/bin/xmgrace
  else if ( -e /usr/bin/xmgrace ) then
    setenv XMGRACE /usr/bin/xmgrace
  endif
endif
#
# Check PATH
#
echo $PATH | grep linuxpops >& /dev/null
#
if ( "$status" == "1" ) then
  setenv PATH "$LINUXPOPS_HOME"/bin:"$PATH"
endif
#
if ( "$USER" == "obs" ) then
  (cd "$HOME"/"$OBSINIT"; makemem )
endif
