#!/bin/csh -f
#
if ( "$1" == "" ) then
  echo "Usage: $0 [smt | 12m]"
  exit
endif
#
foreach f ( `find /home/archive/2013-2014/$1/data -name "sdd.???_???"` )
  echo $f
  ./dumpscans $f | grep SEQ
  echo " "
end
