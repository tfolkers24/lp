#!/usr/bin/python
import re
from optparse import OptionParser
import sys
import os

def main():
    """Parse options, read file and perform substitutions"""

    # Option parsing
    parser = OptionParser()
    parser.add_option("-f", "--file", dest="filename",
                      help="rotate image in FILE", metavar="FILE",)
    parser.add_option("-o", "--output", dest="outfile",
                      help="output file name [FILE-rot]", metavar="OUTFILE")
    parser.add_option("-r", "--rotation", dest="rotation",
                      help="image rotation in degrees [0]", metavar="ROTATION",
                      default=0)
    parser.add_option("-b", "--bottom", dest="bottom",
                      help="image bottom offset in inches [0]", metavar="OFFSET",
                      default=0)
    parser.add_option("-l", "--left", dest="left",
                      help="image left offset in inches [.25]", metavar="OFFSET",
                      default=.25)
    parser.add_option("-p", "--papersize", dest="papersize",
                      help="paper size in pixels [770x610]", metavar="XSIZExYSIZE",
                      default='770x610')
    parser.add_option("-s", "--scale", dest="scale",
                      help="scale factor [1]", metavar="SCALE",
                      default=1)
    parser.add_option("-O", "--optimized", dest="opt",
                      help="run with GEMPAK optimization [False]", metavar="FLAG",
                      default=False)
    (options, args) = parser.parse_args()

    # Argument handling
    if options.filename == None:
        print "ERROR: input file name (-f or --file) must be provided\n"
        return(-1)
    if options.outfile == None:
        outFileName = options.filename + '-rot'
    else:
        outFileName = options.outfile
    papersize = re.sub('x',' ',options.papersize)

    # Open and read input file
    try:
        inFile = open(options.filename,'r')
        try:
            psData = inFile.read()
        finally:
            inFile.close()
    except IOError:
        print "ERROR: cannot open/read input file "+options.filename+"\n"
        return(-1)

    # Substitution compilations
    Roll2Layout = re.compile('(/Roll2 {<< /PageSize \[2448 3168\] >> setpagedevice } def)',re.M)
    customLayout = '        /Custom { << /PageSize ['+str(papersize)+'] >> setpagedevice } def'
    letterName = re.compile('{/Letter}')
    customName = '{/Custom}'
    rotation = re.compile('\d*\.?\d*(\sinch\s)\d*\.?\d*(\sinch\stranslate\s)\d*\.?\d*(\srotate\s)(\d*\.*\d*)\s(\d*\.*\d*)')
    rotation_bw = re.compile('\d*\.?\d*(\sinch\s)\d*\.?\d*(\sinch\stranslate\s)(\d*\.*\d*)\s(\d*\.*\d*)')
    optRotation = re.compile('8.25 inch .25 inch translate 90 rotate 0.03125 0.03125')

    # Get values for rotation and scaling    
    rot = rotation.search(psData)
    if rot:
        defaultScaleX = float(rot.group(4))
        defaultScaleY = float(rot.group(5))
        scale = float(options.scale) * defaultScaleX
        portrait = str(options.left)+rot.group(1)+str(options.bottom)+rot.group(2)+ \
                   str(options.rotation)+rot.group(3)+str(scale)+' '+str(scale)
        if options.opt == False: optRotation = rotation
    if not rot:
        rot = rotation_bw.search(psData)
        defaultScaleX = float(rot.group(3))
        defaultScaleY = float(rot.group(4))
        scale = float(options.scale) * defaultScaleX
        portrait = str(options.left)+rot.group(1)+str(options.bottom)+rot.group(2)+ \
                   str(options.rotation)+' rotate '+str(scale)+' '+str(scale)
        if options.opt == False: optRotation = rotation_bw
    if not rot:
        print "ERROR: PostScript does not conform to standards\n"
        return(-1)

    # Substitute postscript data
    psData = Roll2Layout.sub(r'\1\n'+customLayout,psData)
    psData = letterName.sub(customName,psData)    
    psData = optRotation.sub(portrait,psData)

    # Open output file
    try:
        outFile = open(outFileName,'w')
    except IOError:
        print "ERROR: cannot open "+outFileName+" for writing\n"
        return(-1)

    # Write to output file and close
    try:
        outFile.write(psData)
    except IOError:
        print "ERROR: cannot write to "+outFileName+"\n"
        return(-1)
    outFile.close()

if __name__ == "__main__":
    sys.exit(main())
