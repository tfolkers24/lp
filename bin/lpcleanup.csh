#!/bin/csh
#
# This is a safer clean up script; should only kill
# processes owned by this terminal/shell
#
rm -f LPMEMORY.LOCKFILE LPMEMORY-64.LOCKFILE
#
if ( "$1" == "all" ) then
  echo "Killing all Linuxpops process owned by $USER"
  killall -HUP gnuplot gnuplot_x11 helper xgraphic linuxpops greview logger dataserv dataspec dataplot
  exit
endif
#
set PPID=`ps -o ppid= -p $$`
echo "Killing a child processes of PID: $PPID"
#
# This will even kill this script, so nothing beyond the pkill.
#
pkill -P $PPID
