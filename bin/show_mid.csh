#!/bin/csh -f
#
if ( "$1" == "" || "$2" == "" ) then
  echo "Usage: $0 middle_file_name session"
  exit
endif
#
/usr/local/grace/bin/xmgrace -printfile /tmp/$2/ccal_plot.ps -nxy $1
