#ifndef COLORS_H
#define COLORS_H

#define ANSI_BLACK       0
#define ANSI_RED         1
#define ANSI_GREEN       2
#define ANSI_YELLOW      3
#define ANSI_BLUE        4
#define ANSI_MAGENTA     5
#define ANSI_CYAN        6
#define ANSI_WHITE       7
#define ANSI_RESERVE     8
#define ANSI_RESET       9

#define MAX_ANSI_COLORS 10

#ifdef DEFINE_ANSI_COLORS_STRINGS

char *ansi_colors[] = { "black",
                        "red",
                        "green",
                        "yellow",
                        "blue",
                        "magenta",
                        "cyan",
                        "white",
                        "reserve",
                        "reset"
};

#endif

#endif
