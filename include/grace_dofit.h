/*
 * Grace - Graphics for Exploratory Data Analysis
 * 
 * Home page: http://plasma-gate.weizmann.ac.il/Grace/
 * 
 * Copyright (c) 1991-95 Paul J Turner, Portland, OR
 * Copyright (c) 1996-98 GRACE Development Team
 * 
 * Maintained by Evgeny Stambulchik <fnevgeny@plasma-gate.weizmann.ac.il>
 * 
 * 
 *                           All Rights Reserved
 * 
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 * 
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 * 
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __AS274C_H_
#define __AS274C_H_

static int includ(int np, int nrbar, double w, 
           double *xrow, double yelem, double *d, double *rbar, 
           double *thetab, double *sserr);

static int clear(int np, int nrbar, double *d, double *rbar,
          double *thetab, double *sserr);

static int regcf(int np, int nrbar, double *d, double *rbar,
          double *thetab, double *tol, double *beta,
          int nreq);

static int tolset(int np, int nrbar, double *d, double *rbar, double *tol);

static int sing(int np, int nrbar, double *d, 
         double *rbar, double *thetab, double *sserr,
         double *tol, int *lindep);

static int ss(int np, double *d, double *thetab, 
       double *sserr, double *rss);

static int cov(int np, int nrbar, double *d, 
        double *rbar, int nreq, double *rinv, double *var, 
        double *covmat, int dimcov, double *sterr);

static void inv(int np, int nrbar, double *rbar, int nreq, double *rinv);

static int pcorr(int np, int nrbar, double *d, 
          double *rbar, double *thetab, double *sserr, int in, 
          double *cormat, int dimc, double *ycorr);

static void cor(int np, double *d, double *rbar, 
         double *thetab, double *sserr, double *work,
         double *cormat, double *ycorr);

static int vmove(int np, int nrbar, int *vorder, 
          double *d, double *rbar, double *thetab, double *rss, 
          int from, int to, double *tol);

static int reordr(int np, int nrbar, int *vorder, 
	double *d, double *rbar, double *thetab, double *rss, 
           double *tol, int *list, int n, int pos1);

static int hdiag(double *xrow, int np, int nrbar, double *d,
          double *rbar, double *tol, int nreq, double *hii);

static void putdvec(const char *s, double *x, int l, int h);

static void pr_utdm_v(double *x, int N, int width, int precision);

static double *dvector(int l, int h);

static int *ivector(int l, int h);

static double **dmatrix(int rl, int rh, int cl, int ch);

#endif /* __AS274C_H_ */
