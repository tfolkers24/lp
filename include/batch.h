#ifndef BATCH_H
#define BATCH_H

#define MAX_BATCH_LINES    256
#define MAX_BATCH_LWIDTH   128
#define MAX_BATCH_COMMANDS 256

struct BATCH_COMMAND {
	char fname[128];
	char name[32];
	int  used;
	int  nlines;
	int  ntokens;
	char usage[256];
	char lines[MAX_BATCH_LINES][MAX_BATCH_LWIDTH];
};

#endif
