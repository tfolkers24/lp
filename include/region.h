#ifndef REGION_H
#define REGION_H

struct REGION_BOX {
	int    indx;
	int    color;
	double ra;
	double dec;
	double rsize;
	double dsize;
	char   title[32];

	struct REGION_BOX *next;
};

#endif
