#ifndef FLUX_H
#define FLUX_H

#include "planets.h"

#define MAX_FLUX_HISTORY 1024

struct FLUX_HISTORY_ELEMENT {
	char date[16];
	float flux;
};

struct FLUX_INFO {
	char   magic[32];
	char   name[80];

	int    planet;
	int    num;
	int    plotwin;
	int    manFreq;

	float  bfreq;
	float  efreq;

	struct TBRIGHT values[MAX_JUPITER_ENTRIES];
};


struct FLUX_HISTORY {
	char   magic[32];
	char   name[80];
	char   datestr[80];
	int    num;
	int    plotwin;

	struct FLUX_HISTORY_ELEMENT history[MAX_FLUX_HISTORY];
};

#endif
