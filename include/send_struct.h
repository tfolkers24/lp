
/* Struct for sending gridder cells to the main dataserv OTF map */
/* Only send the required number of channels */
struct SEND_STRUCT {

	char magic[16];				/*   16 bytes */

	int    noint;				/*    4 bytes */
	int    scanct;				/*    4 bytes */
	int    col;				/*    4 bytes */
	int    row;				/*    4 bytes */
	int    version;				/*    4 bytes */
	int    pad;				/*    4 bytes */

	double bscan;				/*    8 bytes */
	double escan;				/*    8 bytes */
	double time;				/*    8 bytes */
	double tsys;				/*    8 bytes */
	double wt;				/*    8 bytes */
	double nyquistRA;			/*    8 bytes */
	double nyquistDEC;			/*    8 bytes */

						/*   96 bytes */

	struct HEADER head;			/* 1536 bytes */

	float chans[MAX_CHANS];			/* 16538 * 2 * 4 */
};

#define SEND_STRUCT_SHORT_SIZE (sizeof(struct SEND_STRUCT) - (sizeof(float) * MAX_CHANS))
