#ifndef SCANS_H

#define SCANS_H

struct SCANS {
	char     mode[16];
	char     source[80];
	double   scan;
	double   freq;
	double   ut;
	double   utdate;
	double   if_feed;
	double   nophase;
	double   samprat;
	double   noint;
	double   az;
	double   el;
	double   sideband;
	double   tsys;
	double   tcal;
	double   xcell0;
	double   ycell0;
	double   noxpts;
	double   noypts;
	int      obsmode;
};

#endif

