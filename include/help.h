#ifndef HELP_H
#define HELP_H

#define TELLLIST    "acc|env|foc|ignored|qk5|map|ewf|nsf|ondata|seq|tip"


#define MAX_LETTERS 26

struct HELP_COMMANDS {
	int  n;
	char commands[128][80];
};


struct HELP_STRUCT {
	char *letter;
	int pad;

	struct HELP_COMMANDS commands[MAX_LETTERS];
};

#define MAX_HELP_STRINGS 64

struct HELP_H_FLAG {
        char *cmd;
        char *help[MAX_HELP_STRINGS];
};

#endif
