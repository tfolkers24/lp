#ifndef FORMULA_H
#define FORMULA_H

#define MAX_FORMULA_LEN 256
#define MAX_FORMULAS     26

struct FORMULA {
	int used;
	int pad;

	char formula[MAX_FORMULA_LEN];
};

struct FORMULAS {
	int n;
	int pad;

	struct FORMULA formulas[MAX_FORMULAS];
};

#endif
