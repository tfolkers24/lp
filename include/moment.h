#ifndef MOMENT_H
#define MOMENT_H

#define MAX_MOMENT_MEASUREMENTS 1024
#define MAX_MOMENT_IFS             4

#define MOM_X_AXIS_SCAN        0
#define MOM_X_AXIS_EL          1

#define MOM_Y_AXIS_AVG         0
#define MOM_Y_AXIS_AREA        1
#define MOM_Y_AXIS_KKMS        2
#define MOM_Y_AXIS_SUM         3
#define MOM_Y_AXIS_TSYS        4


struct SPECTRAL_MOMENT_ELEMENT {
	double scan;		/* X scale elements */
	double el;

	double tsys;		/* Y scale elements */
	double freq;
        double sum;
        double avg;
        double area_kmhz;
        double kkms;
};

struct SPECTRAL_MOMENT_STRUCT {
	char magic[16];

	int  n;
	int  issmt;

	int  plotx;		/* MOM_X_AXIS_* */
	int  ploty;		/* MOM_Y_AXIS_* */

	struct HEADER head;

	struct SPECTRAL_MOMENT_ELEMENT measurements[MAX_MOMENT_MEASUREMENTS][MAX_MOMENT_IFS];
};

#define SPEC_MOM_STRUCT_SIZE (sizeof(struct SPECTRAL_MOMENT_STRUCT))

#endif
