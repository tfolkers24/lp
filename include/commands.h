#ifndef COMMANDS_H
#define COMMANDS_H

struct COMMANDS {
        char *cmd; 		/* Command */
        int   ntoken; 		/* Expected number of tokens/args */
        char *args;   		/* Help string showing possible args */
        int   (*fctn)(); 	/* Function to execute */
	char *fctnStr;		/* Function String */
	int   mode;		/* SPEC, CONT. ALL */
        char *help;		/* Help string */
        int   (*post)(); 	/* Function to execute after command is executed */
};


struct CALIBRATOR {
        char date[80];
	int    band;
        double ra;
        double dec;
        double jk;
        double errBar;
        double freq;
        char   name[128];
};

#define MAX_DYN_VARS 2048

#define DYN_TYPE_UNUSED 0
#define DYN_TYPE_INT    1
#define DYN_TYPE_DOUBLE 2

/* The number of protected dynamic variables at the top of the stack */
#define DYN_VARIBLE_PROTECTED 6

struct DYNAMIC_VARIABLES {
        char    name[80];
        double *dval;
	int    *ival;
        int     type;
	int (*fctn)();
};


#endif
