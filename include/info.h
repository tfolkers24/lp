#ifndef INFO_H
#define INFO_H

#define FAIL_INFO(msg)   st_fail("%s(%s:%d): %s", __func__, __FILE__, __LINE__, msg);
#define REPORT_INFO(msg) st_report("%s(%s:%d): %s", __func__, __FILE__, __LINE__, msg);

#endif
