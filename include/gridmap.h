#define MAX_MAP_W      256
#define MAX_MAP_H      256
#define NORMAL           1
#define ABNORMAL         2
#define MAX_GMAP_SCANS  16

struct BEAM_MAP_ELEM {
        int        x;           /* Indx into the map for this element */
        int        y;           /* Indx into the map for this element */
        int        n;           /* Number of scans integtared into this element */
        int      pad;

        float azoff;           /* Az  offset for this cell */
        float eloff;           /* El  offset for this cell */
        float ra;              /* RA  for this cell */
        float dec;             /* Dec for this cell */

        float  tsys;           /* Current itegrated tsys for this element */
        float itime;           /* Total itegration Time for this element */
        float    wt;           /* Current itegrated weight for this element */
        float  rval;           /* Current map value (K) for this element */
        float  pval;           /* Current Plotting value (K) for this element */

	struct HEADER *headers[MAX_GMAP_SCANS];
};

struct BEAM_MAP {
	char   yaxis_label[80]; /* (K) or dB's */
	char   source[32];	/* First Object entered into the map */

        int map_w;              /* Number of map cols */
        int map_h;              /* Number of map rows */

        int plot_xb;            /* Start Number of plotted cols */
        int plot_xe;            /* End   Number of plotted cols */
        int plot_yb;            /* Start Number of plotted rows */
        int plot_ye;            /* End   Number of plotted rows */

        int nbase;              /* Number of samples on each end for baseline removal */
        int base_removed;       /* Baseline has been removed from raw data */
	int nscans;		/* Total number of scans loaded */
	int offsets_filled;     /* If the offset table have been computed */


        float map_size_x;      /* Map Az size in arcsec */
        float map_size_y;      /* Map El size in arcsec */
        float map_step_x;      /* Map Az cell size in arcsec */
        float map_step_y;      /* Map El cell size in arcsec */

        float map_min;         /* Current Min Value of Raw data */
        float map_max;         /* Current Max Value of Raw data */
        float map_plot_min;    /* Current Min Value of Plotted data */
        float map_plot_max;    /* Current Max Value of Plotted data */

        float gmap_results;    /* Gaussian fitting results */
        float gmap_bw1;
        float gmap_bw2;
        float gmap_peak1;
        float gmap_peak2;

	float fscan;		/* First Scan in Map */
	float lscan;		/* Last  Scan in Map */

	double freq;		/* Freq of the first scan entered into the map */
	double beamthrow;	/* beam throw of the first scan entered into the map */

        struct BEAM_MAP_ELEM bmes[MAX_MAP_W][MAX_MAP_H];;
};

