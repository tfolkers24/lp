#ifndef GLOBAL_H

#define GLOBAL_H

#include "sdd.h"
#include "header.h"
#include "class.h"
#include "defines.h"
#include "env.h"

#define MAX_FREQS 256

struct FREQS {
	double freq;
	int    used;
	int    count;
	int    pad;
};


struct SOURCES {
	char name[32];
	int  num;
	int  used;
	int  mode;
	int  nfreqs;
	int  pad;
	struct FREQS freqs[MAX_FREQS];
};


#define WAIT_FOR_MOUSE      0
#define WAIT_FOR_PLOT       1
#define WAIT_FOR_MAP_ANGLE  2

struct XGRAPHIC_SEND {
	char magic[16];

	int    plot_used;
	int    mouse_used;
	int    map_angle_used;
	int    pad;

	double yplotmin;
	double yplotmax;
	double xplotmin;
	double xplotmax;

	double last_mouse_x;
	double last_mouse_y;
	double last_mouse_x2;
	double last_mouse_y2;

	double map_angle_1;
	double map_angle_2;
};


struct GLOBAL_MEMORY {
	int    which_env;
        int    ndvars_used;
	struct ENVIRONMENT envrs[MAX_ENV ];
};


extern struct GLOBAL_MEMORY *CGM;

#endif

