#ifndef HEAD_H
#define HEAD_H

#define CLASS_ALL       0
#define CLASS_SOURCE    1
#define CLASS_RECEIVER  2
#define CLASS_MAPPING   3
#define CLASS_WEATHER   4
#define CLASS_TELESCOPE 5

struct HEAD_PARAMS {
        char   *name;
        double *dptr;
        char   *cptr;
        int     type;
        int     size;
        int     class;
	int     nclass;
	int     pad;
};

#endif
