
#ifndef EXTERN_H


#define EXTERN_H

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <math.h>
#include <signal.h>
#include <time.h>
#include <termios.h>
#include <unistd.h>
#include <fnmatch.h>
#include <libgen.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_fit.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_multifit_nlinear.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_version.h>
#include <readline/readline.h>
#include <readline/history.h>

#include "observing_par.h"
#include "extended_pars.h"
#include "cactus.h"
#include "loglib.h"
#include "sdd.h"
#include "sex.h"
#include "scans.h"
#include "global.h"
#include "head.h"
#include "beam.h"
#include "commands.h"
#include "gridmap.h"
#include "gaussian.h"
#include "shell.h"
#include "window.h"
#include "image.h"
#include "ephem.h"
#include "help.h"
#include "planets.h"
#include "flux.h"
#include "accum.h"
#include "object.h"
#include "gminit.h"
#include "info.h"
#include "lp_fft.h"
#include "channels.h"
#include "ansi_colors.h"
#include "parse.h"
#include "batch.h"
#include "alias.h"
#include "strings_and_things.h"
#include "histogram.h"
#include "tsys.h"
#include "scan_sum.h"
#include "ffb.h"
#include "alma.h"
#include "focus.h"
#include "formula.h"
#include "moment.h"

#include "caclib_proto.h"
#include "linuxlib_proto.h"

#ifndef XGRAPHICS
#include "proto.h"
#endif

#ifndef DEG_TO_RAD
#define DEG_TO_RAD (M_PI/180.0)
#endif

#ifndef PI
#define PI (M_PI)
#endif

#ifndef SOL
#define SOL           0.299792458        /* speed of light (m/ns) */
#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

/* Global Variables */
extern int envNumber;
extern int doNotRemoveLockFile;
extern int last_valid;
extern int last_plot;
extern int interpl8_debug;
extern int verboseLevel;
extern int onlineversion;
extern int last_scan_count;
extern int stquiet;
extern int site;
extern int currentImage;
extern int oldColor;
extern int use_multi_hist;
extern int doingOTF;
extern int control_c_hit;
extern int valid_bl_averages;
extern int no_terminal;
extern int focus_scaling_formula_set;


extern struct SOCK *xgraphic, *helper, *monitorSock, *dataserv;

/* linuxlib.a */
extern int lastFileSize;
extern int numOfSources;
extern struct HEADER  *sa;
extern struct SOURCES sources[MAX_SOURCE_LIST];

extern struct ENVIRONMENT *env;

extern char *prompt;

extern char  currentCmd[];
extern char  rmsResultsStr[];
extern char  peakResultsStr1[];
extern char  peakResultsStr2[];

extern char  pkg_home[], obs_home[], data_home[], obs_inits[], site_name[],
             printer[], help_home[], lockFileName[], mailboxdefs[], demo_dir[],
             globalName[], onlineDataFile[], logfilename[], hostname[],
	     session[], objGlobalName[], gnuplot_ver[], default_env[], 
	     focus_scaling_formula[], doc_viewer[], xmgr_plotter[];

extern char *versionStr;
extern char *listHeader;
extern char *dateStr;
extern char *machStr;
extern char highlightBuf[], normalBuf[];

extern char *calmodeStrs[];
extern char *xmodeStrs[];
extern char *modeStrs[];
extern char *plotStyleStrs[];
extern char *gridModeStrs[];
extern char *mg_sideband[];
extern char *kp_sideband[];
extern char *old_kp_sideband[];
extern char *siteNames[];
extern char *eph_source_names[];
extern char *ansi_colors[];
extern char *fiveStrs[];
extern char *accum_x_strs[];
extern char *accum_y_strs[];

/* Global */
extern double pi;
extern double plancK;
extern double boltzmann;
//extern double myRawArray[MAX_MAP_W][MAX_MAP_H];
//extern double myPlotArray[MAX_MAP_W][MAX_MAP_H];

extern double autoScale;
extern double tmpBeamEff[4];
extern double datamin, datamax;
//extern double gmap_bw1, gmap_bw2, gmap_peak1, gmap_peak2;
extern double decon_beam_size;
extern double last_mouse_x;
extern double last_mouse_x2;
extern double last_mouse_y;
extern double last_mouse_y2;
extern double manual_tcal;
extern double gnuplotLevel;
extern double gnupatchlevel;
extern double hghterr,cnterr, hwerr;
extern double bl_averages[MAX_BASELINES];

extern FILE *errPrint_fp;

extern struct COMMANDS commands[];

extern double dvars_holders[MAX_DYN_VARS];
extern struct DYNAMIC_VARIABLES dvars[MAX_DYN_VARS];

extern struct LP_IMAGE_INFO H0; /*!< Places where data is read into */
extern struct LP_IMAGE_INFO H1;
extern struct LP_IMAGE_INFO H2;
extern struct LP_IMAGE_INFO H3;
extern struct LP_IMAGE_INFO H4;
extern struct LP_IMAGE_INFO H5;
extern struct LP_IMAGE_INFO H6;
extern struct LP_IMAGE_INFO H7;
extern struct LP_IMAGE_INFO H8;
extern struct LP_IMAGE_INFO H9;

extern struct LP_IMAGE_INFO *h0; /*!< Pointer to the currently selected H? */

extern struct LP_IMAGE_INFO accum_image;

extern struct ACCUM_RESULTS      accum_results[4];
extern struct ACCUM_RESULTS_SEND accum_results_send;

extern struct CONTINUUM_PLAIN    cont_plain_send;
extern struct CONTINUUM_SPTIP    cont_sptip_send;

extern struct ACCUM_RESULTS_SEND local_ars;

extern struct FLUX_HISTORY flux_history;
extern struct FLUX_INFO    flux_info;

extern struct STRINGS_AND_THINGS sat;
extern struct HELP_H_FLAG help_h_flag[];
extern struct GAUSSIAN_FIT gfit;

extern struct FFB_CHANNELS ffb_chans[];

extern struct BEAM_MAP beam_map;

extern struct FOCUS_HISTORY_SEND focus_send;

extern struct FORMULAS formulas;

#else
#warning EXTERN_H already definded
#endif
