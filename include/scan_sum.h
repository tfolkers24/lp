#ifndef SCAN_SUM_H
#define SCAN_SUM_H

#define MAX_SCAN_SUM 512

#define SCAN_SUM_X_SCANS 0
#define SCAN_SUM_X_UT    1
#define SCAN_SUM_X_LST   2
#define SCAN_SUM_X_AZ    3
#define SCAN_SUM_X_EL    4
#define SCAN_SUM_X_NUM   5

#define SCAN_SUM_Y_PEAK  0
#define SCAN_SUM_Y_RMS   1
#define SCAN_SUM_Y_TSYS  2
#define SCAN_SUM_Y_TCAL  3

struct SCAN_SUM {
	double scan[MAX_IFS];
	double   ut[MAX_IFS];
	double  lst[MAX_IFS];
	double   az[MAX_IFS];
	double   el[MAX_IFS];

	double peak[MAX_IFS];
	double  rms[MAX_IFS];
	double tsys[MAX_IFS];
	double tcal[MAX_IFS];
};


struct SCAN_SUM_SEND {
	char            magic[16];
	struct HEADER   head;

	int             last_scan;
	int             nscans;

	int             xaxis;
	int		yaxis;

	int		yscale;  /* o = autoscale, 1 = fixed_scale */
	int		pad;

	float		ylower;
	float		yupper;

	int		plotwin;
	int		xdelay;

	struct SCAN_SUM scans[MAX_SCAN_SUM];
};

#endif
