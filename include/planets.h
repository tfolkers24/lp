
#ifndef EPHEM_H
#define EPHEM_H

#define  MERCURY	0
#define  VENUS		1
#define  MARS		2
#define  JUPITER	3
#define  SATURN		4
#define  URANUS		5
#define  NEPTUNE	6
#define  MOON		7
#define  MAX_PLANETS    8

#define   H          6.626e-34          /* Planck's constant */
#define   K          1.381e-23          /* Boltzmann's constant */
#define   C          0.299792458        /* speed of light (m/ns) */
#define   LN2        0.693147181        /* ln(2.0) */
#define   CMB        2.725              /* Cosmic Microwave Bkgd */
#define   R0         1.524              /* Mars mean heliocentric distance */
#define   T_R0       206.8              /* Mars temp at R0 */
#define   P          29.530589          /* Period of Moon */


#define MAX_VENUS_ENTRIES    170
#define MAX_MARS_ENTRIES      16
#define MAX_JUPITER_ENTRIES 4750
#define MAX_URANUS_ENTRIES   270
#define MAX_NEPTUNE_ENTRIES  800
#define MAX_VESTA_ENTRIES     64

struct TBRIGHT {
        float freq;
        float tb;
};


struct EPH_TIME {
	int year;
	int mon;
	int day;
	int hour;
	int min;
	int pad;
};


struct EPH_STATE {
	double f_sky;      // sky frequency (GHz)
	double rej;        // rejection (dB)
	double T_ant;      // antenna temperature TA* (K)
	double ta_s;       // antenna temperature std dev (+/-)
	double eta_mb;     // main-beam efficiency    
	double err;        // main-beam efficiency uncertainty
	double lambda;    
	double theta_mb;   // Telescope main beam FWHM
};



struct EPH_OBJECT {
	char   name[80];
	char   dbase[32];  /* Database source */
	double ra;         /* Right Accension */
	double dec;        /* Declination */
	double illum;      /* % of illumination as seen from Earth */
	double dmajor;     /* Arcsec of diameter */
	double dminor;     /* Arcsec of diameter */
	double sdist;      /* AU from Sun */
	double rdot;       /* ??? */
	double dist;       /* Object distance from Earth */
	double vel;        /* Object Velocity reletive to Earth */
	double sangle;     /* Angle between Sun and Object */
	double tsrc;       // Source temperature (K)
	double trj;        // planet diameter (arcsec)
	double flux;       // planet flux
	double flux2;      // planet flux, different method
	double tmb;        // Temperature in the main beam
	double csize;      // Planet convoled size with beam
	double frac;	   // Fractional time between lines of Ephem tables
};


struct NEW_EPHEM {
	int               planet;
	struct EPH_STATE  state;
	struct EPH_TIME   time;
	struct EPH_OBJECT planets[MAX_PLANETS];
};


#endif
