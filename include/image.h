#ifndef IMAGE_H
#define IMAGE_H

#define MAX_PLOT_ARRAYS    4

struct BASELINE_INFO {
	char   magic[32];   /* From identifiing structure in sock comm */
	int    doBaseLine;  /* Remove baseline if true */
	int    baselines[MAX_BASELINES][2];/* Baseline areas */

	float  a;           /* Lsqfit results SLOPE */
	float  b;           /* Lsqfit results INTERCEPT */
	float  rms;         /* RMS of baseline area */
};

/* Plotting criteria */
struct PLOT_INFO {
	char   magic[32];

	int    markers;     /* If non-zero, display channel markers in the plot */
        int    plotStyle;   /* POINTS, LINES, HISTO */

	int    x1mode;      /* CHAN, FREQ, VEL */
	int    x2mode;      /* CHAN, FREQ, VEL */

        int    gridMode;    /* Plot grid mode: X, Y, Full, None */
	int    zline;       /* Show zero line if true */

	int    badchshow;   /* Show the removed bad channels if true */
	int    bmark;       /* Show the baseline regions if true */

	int    bshow;       /* Show the baseline fitting if true */
	int    mshow;       /* Show the filter bank markers */

	int    bdrop;       /* Number of beginning channels to drop */
	int    edrop;       /* Number of ending channels to drop */

        int    dobadch;     /* Remove bad channels if specified */
	int    yscale;      /* AUTO vs. FIXED flag */

	int    plot_noint;  /* Actual number of plotable channels after any smoothing */
	int    bplot;       /* A baseline plot is available */

	int    splot;       /* A shell plot is available */
	int    nspline;     /* Number of B-spline coeef to use */

	int    yplotmode;   /* LINEAR vs. LOG plot */
	int    nightmode;   /* True = Nightmode active */

	int    xstart;	    /* Upper left 'X' window placement; if either non-zero */
	int    ystart;	    /* Upper left 'Y' window placement; if either non-zero */

	float  paperw;      /* User defined Width  of PDF plot in inches */
	float  paperh;      /* User defined Height of PDF plot in inches */

	float  ylower;      /* User specified ploting lower range */
	float  yupper;      /* User specified ploting upper range */

	float  plotmin;     /* Minimum Plotting Range */
	float  plotmax;     /* Maximum Plotting Range */
};

/* Accumilator info */
struct ACCUM_INFO {
	char   magic[32];
	int    scanct;      /* Total number of scans added; if accum */
	float  bscan;       /* First scan Added */
	float  escan;       /* Last scan Added */
	float  wt;          /* Accumulated weight of total data */
};

struct MISC_HEADER_PARAMS {
	char   backend[8];
	double scan;
	double restfreq;
	double stsys;
	double tcal;
	double freqres;
	double sideband;
	double refpt;
	double refpt_vel;
	double deltax;
};


struct LP_IMAGE_INFO {
	char   magic[32];
	char   name[32];    /* "H0", "H1", ... */
	char   title[256];

	int    mode;        /* CONT, SPEC, OTF */
	int    peakChan;    /* Channel/Velocity of peak Intensity */
	int    plotwin;
	int    narray;      /* Number of yplot arrays to follow */
	int    nsize;       /* Size, in bytes, of arrays that follow */
	int    satActive;   /* If true, there are strings, markers or lines to plot */

			    /* Because xgraphics can actually outrun the gnuplot program
			       we need to delay xgraphics a bit to allow gnuplot to
			       finish plotting before moving on to the next step; otherwise
			       files like /tmp/SESSION/scan.dem get over-written by xgraphics
			       while gnuplot is in the process of reading it.
			       This is expecially obvious when on a remote site; not local
			       and it takes gnuplot a finite amount of time to perform XWindow
			       functions */
	int    xdelay;      /* Delay xgraphics uses to account for slow internet In uSec */
	int    issmt;       /* If data is from SMT */

	int    shifted;     /* This data has beend shifted */
	int    xaxisotfoff; /* If true, label X-Axis is offset: 1 = Ra, 2 = Dec */;

	float  datamin;     /* Minimum Intensity */
	float  datamax;     /* Maximum Intensity */

	struct HEADER             head;
	struct ACCUM_INFO         ainfo;
	struct BASELINE_INFO      binfo;
	struct GAUSSIAN           ginfo;
	struct PLOT_INFO          pinfo;
	struct SHELL              sinfo;
	struct WINDOWS            winfo;

	struct MISC_HEADER_PARAMS mhp[MAX_PLOT_ARRAYS]; /* Contains info for quad display */

	int    badChans[MAX_BAD_CHANS];

	/* These arrays are not transmitted when using external viewer */
	float  yplot1[MAX_CHANS];   /* Up to (MAX_PLOT_ARRAYS) plots available */
	float  yplot2[MAX_CHANS];
	float  yplot3[MAX_CHANS];
	float  yplot4[MAX_CHANS];

	float   bplot[MAX_CHANS];   /* Pre-filled baseline/bshape Array linuxpops fills this one */
	float   gplot[MAX_CHANS];   /* Gaussian Array; 			xgraphics fills this one */
	float   splot[MAX_CHANS];   /* Shell Array			xgraphics fills this one */
	float  x1data[MAX_CHANS];   /* X-Axis values; 			xgraphics fills this one */
	float  x2data[MAX_CHANS];   /* Values for top X2; 		xgraphics fills this one */
};

	/* Define a constant for the size without the last 9 arrays */
#define SHORT_IMAGE_SIZE (sizeof(struct LP_IMAGE_INFO) - (9 * MAX_CHANS * sizeof(float)))

#endif
