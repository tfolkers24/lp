#ifndef ENV_H

#define ENV_H

#include "defines.h"
#include "shell.h"
#include "window.h"

struct ENVIRONMENT {
	char   title[80];		/*!< Environment  Title: User define-able */
	char   filename[256];		/*!< Current Selected datafile */
	char   gfilename[256];		/*!< Current Selected gains datafile */
	char   source[64];		/*!< Currently Selected Source */
	char   backend[16];		/*!< Spectrometer/Continuum Backend Name: i.e. F1M-HU */
	char   savefile[256];		/*!< Save filename */
	char   userfile[256];		/*!< User filename; Used by 'print /w' command */
	char   moment_linename[256];    /*!< Moment line name */
	char   spare_chars[1792];

	int    accum;			/*!< If True, We are in the process of accumulating scans */
	int    accumcount;		/*!< Total number of scans last accumulated */
	int    accumreset;		/*!< If True, Next call to the accum function resets all registers */
	int    acc_plot_select_x;	/*!< Accumm plot x axis: 'num', 'el', 'ut', 'scannum' */
	int    acc_plot_select_y;	/*!< Accumm plot y axis: 'sig', 'sky', 'smror', 'tsys', 'tastar', 'tr', 'jansky' */
	int    acount;			/*!< Total number of items in stack */
	int    allow_five_pt;		/*!< Allow five point centers to be accumulated */
	int    ashift;			/*!< # of channels to shift spectra - or + */
	int    autobad;			/*!< Automatically remove bad channels when encountered */
	int    autotau;			/*!< If True, Process SPTIP's as you encounter them during accum */
	int    spare_a_ints[16];

	int    badchanepoch;		/*!< Epoch of the 'fixed' bad channel array */
	int    badchanshow;		/*!< If True, Place markers on defined bad channels */
	int    badchans[MAX_BAD_CHANS]; /*!< Array of flagged bad channels */
	int    baselines[MAX_BASELINES][2]; /*!< Defined baseline regions */
	int    bdrop;			/*!< # of channels to drop from beginning of spectral plot */
	int    bkmatch;			/*!< Backend matching mode: 0 = FEED, 1 = RESOLUTION, 2 = NAME */
	int    bleadtrail;		/*!< 0 = straight, 1 = polynomial fitting of leading and trailing channels; */
	int    bmark;			/*!< If True, show baseline region boxes on plot */
	int    bshow;			/*!< If True, Show the baseline fit on plot */
	int    bsmoothing;		/*!< Linear vs polynomial intep between baseline regions */
	int    btokens;			/*!< # of batch tokens in current command */
	int    baselineremoved;		/*!< True is baselines have been removed */
	int    spare_b_ints[15];

	int    calmode;			/*!< Cal Mode: 0 = NONE, 1 = CALSCALE, 2 = BEAMEFF, 3 = TELE_EFF */
	int    check_bkname;
	int    check_date;		/*!< If True, Use the Date in the search criteria */
	int    check_feed;		/*!< If True, Use the IF Feed in the search criteria */
	int    check_freq;		/*!< If True, Use the Frequency in the search criteria */
	int    check_scans;		/*!< If True, Use the Actual Scan Numbers in the search criteria */
	int    check_source;		/*!< If True, Use the Source Name in the search criteria */
	int    currentImage;		/*!< Current image header index */
	int    spare_c_ints[16];

	int    displaymode;		/*!< Night mode vs Day mode; may not be fully synced yet */
	int    dobadchan;		/*!< If True, smooth over defined bad channels */
	int    dobaseline;		/*!< if True, remove computed baseline */
	int    doing_fives;		/*!< It True, analyse 5 point centers like a seq */
	int    dospike;			/*!< If True, Locate and remove and spikes in data: Experimental */
	int    spare_d_ints[16];

	int    edrop;			/*!< Amount of channels to remove from end of plot */
	int    elgain;			/*!< If True, Compute and use the defined Elevation Gain Terms */
	int    spare_e_ints[16];

	int    feed;			/*!< Which feed we are processing at and given time */
	int    spare_f_ints[16];

	int    glength;			/*!< The number of data points to include in Gaussian fitting */
	int    gridmode;		/*!< Plot window grid mode: 0 = NONE, 1 = X_ONLY, 2 = Y_ONLY, 3 = FULL */
	int    spare_g_ints[16];

	int    spare_h_ints[16];

	int    ignoremapoff;		/*!< If True, Ignore small mapping offsets */
	int    ignorepos;		/*!< If True, ignore RA, Dec & Vel in accum function: Think Planets */
	int    invert;			/*!< If True, invert the phases of continuum data */
	int    issmt;			/*!< If True, data file is from SMT */
	int    spare_i_ints[16];

	int    spare_j_ints[16];

	int    spare_k_ints[16];

	int    last_scan_count;		/*!< # of scans accumulated in last 'accum' */
	int    loopdelay1;		/*!< Delay for any background loops */
	int    loopdelay2;		/*!< Delay for any foreground loops */
	int    spare_l_ints[16];

	int    mapangle;		/*!< Preliminary Grid Map plotting angles */
	int    mapbaselines;		/*!< # of cells on ends of row used for baselines; neg # for center cells; 0 to skip altogether */
	int    markers;			/*!< # of filterbank channels per card: see mshow */
	int    map_w;			/*!< Grid map width in cells */
	int    map_h;			/*!< Grid map height in cells */
	int    mode;			/*!< 1 = MODE_CONT, 2 = MODE_SPEC, 3 = MODE_OTF */
	int    mshow;			/*!< If True, draw markers on spectral plot show filter card breaks */
	int    moment_bchan;		/*!< Beginning and ending channel numbers to perform 'moment' on */
	int    moment_echan;
	int    spare_m_ints[14];

	int    nbeff;			/*!< # of columns to printout during beam eff function */
	int    nbox;			/*!< # of channels to use in the boxcar function */
	int    nbshape;			/*!< # of channels to smooth for bshape command */
	int    ngchan;			/*!< Number of channels to zero out left/right of gauss auto fit */
	int    nfit;			/*!< # of parameters for curve fitting 1 - 10 */
	int    nignored;		/*!< # of scans in the ignore-scan array */
	int    nsave;			/*!< Current selected nsave value: Used for fetching saved scans */
	int    nspline;			/*!< # of terms to use in bspline function */
	int    numofscans;		/*!< # of scans in currently selected data file */
	int    nwindows;		/*!< Number of defined Windows of Interest */
	int    spare_n_ints[16];

	int    ons;			/*!< If True, display the 'on' phases in continuum plot */

	int    otf_cols;
	int    otf_rows;
	int    otf_map_xcell;
	int    otf_map_ycell;
	int    otf_bint_chan;		/*!< Beginning integration channel/region */
	int    otf_eint_chan;		/*!< Ending integration channel/region */
	int    otf_3d_bchan;		/*!< Beginning channel of a 3d data cube */
	int    otf_3d_echan;		/*!< Ending channel of a 3d data cube */
	int    otf_3d_interp;		/*!< # of planes to create b/t channels in a 3d data cube */
	int    otf_movie_bchan;		/*!< Beginning channel of 'vel' run */
	int    otf_movie_echan;		/*!< Ending channel of 'vel' run */
	int    otf_movie_nchan;		/*!< # of channels to avg for 'vel' run */
	int    otf_cutoff;		/*!< % of the bottom-end of data to cut off */
	int    otf_baselines;		/*!< Remove baselines from individual spectral */
	int    otf_color;		/*!< gray, pseudo, flame1, flame2, flame3, flame4, flame5, flame6 */
	int    otf_ephem_obj;		/*!< If non-zero, Ignore source Ra & Dec when gridding */
	int    otf_single_spec;		/*!< Which Spectral Channel to plot in lower left window */
	int    otf_if_choice;		/*!< Which IF to process */
	int    otf_load_choice;		/*!< Preview, fast, step */
	int    otf_single_bin;		/*!< # of channels to bin for 'otf_single_spec' */
	int    otf_ignore_color;	/*!< If true, ignore changes in color; slow internet helper */
	int    otf_ignore_repaint;	/*!< If true, ignore repaints of grid between tile loads */
	int    otf_show_beam_size;      /*!< If True, place a beam size circle on plot */
	int    otf_chan_map_cent;       /*!< OTF Channel Map Center Channel */
	int    otf_chan_map_cols;       /*!< OTF Channel Map # of Columns */
	int    otf_chan_map_rows;       /*!< OTF Channel Map # of Rows */
	int    otf_chan_map_avg;	/*!< OTF Channel Map # os Chans to Avg */
	int    otf_baseline_order;      /*!< The 'order' of baseline removal */
	int    otf_show_regions;	/*!< Show region outlines, if True & loaded */
	int    otf_noint;		/*!< # of channels in OTF bandpass */
	int    otf_spare_ints[64];

	int    overwrite;		/*!< If true, then save will overwrite an existing nsave */
	int    spare_o_ints[16];

	int    peakChan;		/*!< Channel number with the peak intensity */
	int    plotstyle;		/*!< Plot Style: 0 = HISTOGRAM, 1 = LINES, 2 = POINTS */
	int    plottmb;			/*!< If true, plot the expected Tmb too; 0 no plot, 1 = LSB, 2 USB */
	int    plotwin;			/*!< Currently selected Gunplot Window */
	int    procspec;		/*!< If True, Further processing needed: think TP Scans */
	int    spare_p_ints[16];

	int    spare_q_ints[16];

	int    read_dir;		/*!< 0 = read data files from back to front [default], 1 = front to back */
	int    rm_fit_data;		/*!< If True, remove the bplot data from yplot: Experimental */
	int    spare_r_ints[16];

	int    sf_fitcenter;		/*!< User setable chan # of spectral line center if not in center */
	int    sf_start;		/*!< User selectable starting sample for Spectral Line Focus */
	int    sf_end;			/*!< User selectable Ending sample for Spectral Line Focus */
	int    sf_bchans;		/*!< User supplied number of baseline channels to use on the ends */
	int    shift_allow;		/*!< If True, allow automatic freq shifting of spectra as encountered  */
	int    split_phases;		/*!< If True, do not average ON and OFF five points: Experimental */
	int    stackdone;		/*!< We have reached the end of the stack */
	int    stackindx;		/*!< Current stack pointer */
	int    strictsb;		/*!< If True, Avoid averaging USB and LSB continuum */
	int    shell_label;		/*!< If True, place a number label next to shell fit */
	int    spare_s_ints[15];

	int    spare_t_ints[16];

	int    usesat;			/*!< Display Strings And Things if True */
	int    used;			/*!< True if this environment is used */
	int    usegslgauss;		/*!< If True, use the gsl Gaussian fitting, else use unipops version */
	int    use_scaling;		/*!< If True, Use data_scaling_x to scale raw data when read */
	int    spare_u_ints[15];

	int    spare_v_ints[16];

	int    xdelay;			/*!< Amount of time to delay between gnuplot calls; uSec */
	int    xstart;			/*!< Upper left placement of gnuplot window; if non-zero */
	int    x1mode;			/*!< Bottom X-Axis of Spectral plot: 0 = FREQ, 1 = VEL, 2 = CHAN */
	int    x2mode;			/*!< Top    X-Axis of Spectral plot: 0 = FREQ, 1 = VEL, 2 = CHAN */
	int    spare_x_ints[16];

	int    yaxis_scale;		/*!< 0 = LINEAR_PLOT,  1 = LOG_PLOT */
	int    yscale;			/*!< 0 = Y_AUTO_SCALE, 1 = Y_FIXED_SCALE */
	int    ystart;
	int    spare_y_ints[16];

	int    zline;			/*!< If True, Show the zero line in spectral plots */
	int    spare_z_ints[16];


	double accumtr;			/*!< Running accum Tr */
	double accumtastar;		/*!< Running accum TaStar */
	double apperatureeff;		/*!< Resulting Aperture efficiency */
	double avgEl;			/*!< Average Elevation of latest accum */
	double autoGaussXfactor;	/*!< Factor to use when auto creating 'windows' based on Gaussian fits */
	double autoGaussYfactor;	/*!< Factor to use when auto creating 'windows' based on Gaussian fits */
	double spare_a_doubles[6];

	double beam_eff;		/*!< Computed Beam Eff */
	double bkres;			/*!< backend resolution When searching using BKMATCH_RESOLUTION */
	double bm_eff[MAX_BEAMS];	/*!< Resulting beam efficiency */
	double bp_avg;			/*!< Average temp of the bandpass (bdrop - edrop) */
	double bscan;			/*!< Beginning scan range during searching */
	double spare_b_doubles[8];

	double cal_scale;		/*!< Continuum calibration scale when in CALMODE_CALSCALE */
	double ccur;			/*!< Last mouse click channel */
	double cnterr;			/*!< Center Error term for last Gaussian fit */
	double curScan;			/*!< Last scan loaded */
	double spare_c_doubles[8];

	double datamax;			/*!< Largest  value in h0->data array */ 
	double datamin;			/*!< Smallest value in h0->data array */ 
	double date;			/*!< Date used in searching */
	double dfreq;			/*!< Delta freq tolerance used during searching */
	double dish_size;		/*!< User defined dish_size and sub-reflector taper dB's */
	double dvel;			/*!< Delta Velocity tolerance */
	double data_scale_1;		/*!< if non-zero, scale each scale by this amount at readtime 4 IF's worth */
	double data_scale_2;
	double data_scale_3;
	double data_scale_4;
	double spare_d_doubles[4];

	double elgain_term1;		/*!< User input elevation gain curve term 1 */
	double elgain_term2;		/*!< User input elevation gain curve term 2 */
	double elgain_term3;		/*!< User input elevation gain curve term 3 */
	double elgain_term4;		/*!< User input elevation gain curve term 4 */
	double elgain_term5;		/*!< User input elevation gain curve term 5 */
	double escan;			/*!< Ending scan range during searching */
	double expectedflux;		/*!< User supplied Expected Source Janskys */
	double expectedtmb;		/*!< User supplied Expected Source T-main-beam */
	double spare_e_doubles[8];

	double fcur;			/*!< Last mouse click freq */
	double fft_xb;			/*!< FFT X-axis Start */
	double fft_xe;			/*!< FFT X-Axis End */
	double fittedtau;		/*!< Last fitted tau from SPTIP */
	double focus_center_diff;	/*!< If non-zero, focus fit must be no more that this amount different then input */
	double focus_cnterr_limit;	/*!< If non-zero, Gaussian Center Error must be less than this to qualify as good */
	double focus_fit_1;		/*!< Store results of spec focus here */
	double focus_fit_2;
	double focus_freq_upper;	/*!< If non-zero, then limit focus fit to these limits; use with 'freq *' */
	double focus_freq_lower;	/*!< If non-zero, then limit focus fit to these limits; use with 'freq *' */
	double focus_lower_limit;	/*!< If non-zero, Focus Tmb must be higher than this to qualify as good */
	double fr;			/*!< Freq-switch Referance Offset */
	double freq;			/*!< Rest Freq used during Searching */
	double fs;			/*!< Freq-switch Signal Offset */
	double spare_f_doubles[8];

	double gaussPeak;		/*!< Peak Results of Gaussian Fit */
	double gaussCenter;		/*!< Center Results of Gaussian Fit */
	double gaussHP;			/*!< Width Results of Gaussian Fit */
	double gaussSum;		/*!< Area under the Gaussian Results */
	double gmap_zmin;		/*!< Z-Axis plotting min; used if either is non-zero */
	double gmap_zmax;		/*!< Z-Axis plotting max; used if either is non-zero */
	double gmap_gamma;		/*!< Grid map Gamma; Applied if non-zero */
	double spare_g_doubles[8];

	double hghterr;			/*!< Height Error term for last Gaussian fit */
	double hwdc;			/*!< Fractional time of backend integration; i.e. 0.90 for filters */
	double hwerr;			/*!< Width Error term for last Gaussian fit */
	double spare_h_doubles[8];

	double if_feed;			/*!< [1, 2, 3 or 4]: requested feed: 0.01, 0.02, 0.03, 0.04 */
	double ignoredscans[MAX_IGNORED];
	double intercept;		/*!< Last Intercept from the baseline operation */ 
	double spare_i_doubles[8];

	double jfact;			/*!< Currently used Jk/K factor */
	double jupiter_tmb;		/*!< Jupiter Temperature-Main-Beam */
	double spare_j_doubles[8];

	double kj_factor;		/*!< If Non Zero, use this value for Kelvin to Jansky Conversion */
	double spare_k_doubles[8];

	double lowerdb;			/*!< Lowest fabs(map_value) allowed when using decibel map plot */
	double spare_l_doubles[8];

	double mantau0;			/*!< User supplied tau0 value: overrides autotau */
	double map_angle[2];		/*!< Grid Map angle used for plotting */
	double mars_tmb;		/*!< Mars Temperature-Main-Beam */
	double mercury_tmb;		/*!< Mercury Temperature-Main-Beam: filled in by 'plan' command */

	double moment_freq;
	double moment_sum;
	double moment_avg;
	double moment_area_kmhz;
	double moment_kkms;

	double spare_m_doubles[3];

	double neptune_tmb;		/*!< Neptune Temperature-Main-Beam */
	double newres;			/*!< New resolution to convert data to. Used by chngres */
	double nyquist;			/*!< Nyquist sample size based on rest freq */
	double spare_n_doubles[8];

	double oob_val;			/*!< Out Of Bounds Value; Set OOB values to this; else the avg is used */

	double otf_nyquistra;		/*!< Size of individual cells in RA */
	double otf_nyquistdec;		/*!< Size of individual cells in Dec */
	double otf_map_ra;		/*!< Center of Map in RA */
	double otf_map_dec;		/*!< Center of Map in Dec */
	double otf_map_xsize;		/*!< Size of map in RA */
	double otf_map_ysize;		/*!< Size of map in Dec */
	double otf_grid_scale;		/*!< Magnification scale of plot */
	double otf_gamma;		/*!< Map Gamma correction */
	double otf_size_factor;		/*!< Scale factor for Automatic OTF Map creation */
	double otf_delay;		/*!< Delay used for velocity movie */
	double otf_3d_vrange;		/*!< Scaling factor for Vel Range: nominally 1.0  */
	double otf_3d_urms;		/*!< Max rms allowed in 3d Cube output */
	double otf_map_velocity;	/*!< Velocity of OTF Map */
	double otf_map_restfreq;	/*!< Rest Freq of OTF Map */
	double otf_map_freqres;		/*!< Backend Resolution of OTF Map */
	double otf_zrange_factor;	/*!< The factor to multiply the z-axis by; if non-zero */
	double otf_beam_ra;		/*!< Where to place the beam-size circle on the map */
	double otf_beam_dec;
	double otf_spare_doubles[64];

	double spare_o_doubles[8];

	double paperw;			/*!< Width  of plot window; 0 == normal 800 pixels */
	double paperh;			/*!< Height of plot window; 0 == normal 600 pixels */
	double ptime;			/*!< Time of day used in the 'planets' program; 0.0 - 23.99 */
	double p2p;			/*!< Computed Peak to Peak */
	double spare_p_doubles[8];

	double spare_q_doubles[8];

	double rejections[MAX_BEAMS];   /*!< Currently receiver rejections */
	double rms;			/*!< Last rms value over the baseline region */
	double rst;			/*!< Results of the latest calculation */
	double spare_r_doubles[8];

	double saturn_tmb;		/*!< Saturn Temperature-Main-Beam */
	double sf_fitmhz;		/*!< User defined Width of Spectral Focus/Five-pt fitting (MHz) */
	double slope;			/*!< Last slope from the baseline operation */
	double stackscan;		/*!< Current scan pointed to by stack pointer */
	double shellthreshold;		/*!< User selectable shell detection threshold; normally 0.80 i.e. 80% */
	double spare_s_doubles[7];

	double taper;			/*!< Used if value > 0.0 */
	double tcallimit;		/*!< Tcal Upper limit for Spectral/Cont accum. */
	double tcur;			/*!< Last mouse click temperature */
	double theta_mb;		/*!< Manual Theta Main Beam in arcsecs; >0.0 overrides computed value */
	double totalweight;		/*!< Weighted average of accumulations */
	double trms;			/*!< Theoretical RMS */
	double tsyslimit;		/*!< Tsys Upper limit for Spectral/Cont accum. */
	double spare_t_doubles[8];

	double uranus_tmb;		/*!< Uranus Temperature-Main-Beam */
	double spare_u_doubles[8];

	double vcur;			/*!< Last mouse click velocity */
	double venus_tmb;		/*!< Venus Temperature-Main-Beam */
	double spare_v_doubles[8];

	double spare_w_doubles[8];

	double xplotmin;		/*!< Xgraphics Computed X-Plot Range Min */
	double xplotmax;		/*!< Xgraphics Computed X-Plot Range Max */
	double spare_x_doubles[8];

	double yplotmin;		/*!< Xgraphics Computed Y-Plot Range Min */
	double yplotmax;		/*!< Xgraphics Computed Y-Plot Range Max */
	double ylower;			/*!< User set lower y scale for plotting */
	double yupper;			/*!< User set upper y scale for plotting */
	double spare_y_doubles[8];

	double spare_z_doubles[8];

	struct SHELL	sinfo;		/*!< Shell fitting info */
	struct WINDOWS	winfo;		/*!< Area of Interest Windows 32 doubles */
};

#endif
