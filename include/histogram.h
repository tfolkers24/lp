#ifndef HISTO_H
#define HISTO_H

#define MAX_HISTO_LENGTH (1024)


struct HISTO_INFO {
	char   magic[32];
	int    plotwin;
	int    gset;
	int    num;

	double max;
	double mean;
	double sigma;
	double sum;
	double gauss[3];

	struct HEADER head;

	float  xdata[MAX_HISTO_LENGTH];
	float  ydata[MAX_HISTO_LENGTH];
	float  gdata[MAX_HISTO_LENGTH];
};

#endif
