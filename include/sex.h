/*
 *  Cactus file @(#)sex.h	1.2
 *        Date 12/08/03
 *
 */
#define SSS_S100		'1'
#define MMMM_SS			'2'
#define MMM_SS_OVER		'3'
#define DDDD_MM			'4'
#define MMM_SS_S100		'5'
#define DDDD_MM_SS		'6'
#define DDDD_MM_SS_S100		'7'
#define DDDD_MM_SS_SS100	'8'
#define HH_MM_SS		'9'
#define DD_MM_SS		'T'
#define IGNORE_EXP		'E'
#define HH_MM			'H'
#define DDD_MM_SS_S100		'D'
