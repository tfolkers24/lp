/*
 Cactus file @(#)observing_par.h	1.18
        Date 04/12/10
*/

#ifndef OBSERVING_PAR
#define OBSERVING_PAR

	/* Backend codes formerly enum	backends_12m */

#define MYSTERY_BE    0
#define ANALOG_CBE    1
#define DIGITAL_CBE   2
#define HOLOGRAPHY_BE 3
#define FB_25_KHZ     4
#define FB_30_KHZ     5
#define FB_100_KHZ    6
#define FB_250_KHZ    7
#define FB_500_KHZ    8
#define FB_1_MHZ      9
#define FB_2_MHZ     10
#define HYBRID_CORR  11

	/* Observing modes formerly enum obstypes_12m */

#define MYSTERY_OTYPE	0
#define PS		1
#define APS		2
#define FS		3
#define BSP		4
#define TP_ON		5
#define TP_OFF		6
#define ATP		7
#define PSM		8
#define APM		9
#define FSM		10
#define TPM_ON		11
#define TPM_OFF		12
#define DRIFT		13
#define PS_CAL		14
#define BS_CAL		15
#define BLANKING	16
#define SEQUENCE	17
#define FIVE_PT		18
#define CONT_MAP	19
#define FOCALIZE	20
#define NS_FOCALIZE	21
#define TP_TIP		22
#define SP_TIP		23
#define D_ON		24
#define CALIBRATE	25
#define FS_PS		26
#define EW_FOCALIZE	27
#define ZEROS		28
#define TLPW            29      
#define FQSW            30      
#define NOCL            31      
#define PLCL            32      
#define ONOF            33      
#define BMSW            34      
#define PSSW            35      
#define DRFT            36      
#define OTF             37      
#define S_ON            38      
#define S_OF            39      
#define QK5             40
#define QK5A            41
#define PSS1            42 /* PS flip or PS-1 mode */
#define VLBI            43
#define PZC             44
#define CPZM            45
#define PSPZ            46
#define CPZ1            47
#define CPZ2            48
#define CCPZ            49
#define CCPM            50
#define TPPZ            51
#define OPT             52
#define COLDCAL         53

/* Velocity definitions */
#define	RADIO			1
#define	OPTICAL			2
#define	RELATIVISTIC		3

/* Velocity frames */
#define	HELIOCENTRIC		1
#define LSR			2
#define GEOCENTRIC		3
#define TOPOCENTRIC		4

/* Calibration types */
//#define DIRECT		0
//#define NOISE		1
//#define CHOP		2

/* Filterbank modes */
#define SERIES		1
#define PARALLEL	2

/* Code for DBE's extrn_sr when controlled by SAM bus */
#define SAM 2


/* Values for BACKEND parameters in CACTUS (Powers of 2) */
#define	HYBRID	1		/* Hybrid Spectrometer observations */
#define	DIGBE	2		/* Continuum Digital Backend observations */
#define	FILTERS	4		/* Filterbank observations */

/* Sideband specifiers */
#define	LSB	1
#define	USB	2

#endif
