#ifndef ALIAS_H
#define ALIAS_H

#define MAX_ALIAS 256

struct ALIAS {
	int  used;
	char keyword[64];
	char alias[MAX_ALIAS];
};

#endif
