
#ifndef PARSE_H
#define PARSE_H

/* Command flags */

enum parse_flags {
	PF_A_FLAG = 0,
	PF_B_FLAG,
	PF_C_FLAG,
	PF_D_FLAG,
	PF_E_FLAG,
	PF_F_FLAG,
	PF_G_FLAG,
	PF_H_FLAG,
	PF_I_FLAG,
	PF_J_FLAG,
	PF_K_FLAG,
	PF_L_FLAG,
	PF_M_FLAG,
	PF_N_FLAG,
	PF_O_FLAG,
	PF_P_FLAG,
	PF_Q_FLAG,
	PF_R_FLAG,
	PF_S_FLAG,
	PF_T_FLAG,
	PF_U_FLAG,
	PF_V_FLAG,
	PF_W_FLAG,
	PF_X_FLAG,
	PF_Y_FLAG,
	PF_Z_FLAG,

	PF_0_FLAG,
	PF_1_FLAG,
	PF_2_FLAG,
	PF_3_FLAG,
	PF_4_FLAG,
	PF_5_FLAG,
	PF_6_FLAG,
	PF_7_FLAG,
	PF_8_FLAG,
	PF_9_FLAG,
	PF_10_FLAG,
	PF_11_FLAG,
	PF_12_FLAG,
	PF_13_FLAG,
	PF_14_FLAG,
	PF_15_FLAG,
	PF_16_FLAG,
	PF_17_FLAG,
	PF_18_FLAG,
	PF_19_FLAG,
	PF_20_FLAG,
	PF_21_FLAG,
	PF_22_FLAG,
	PF_23_FLAG,
	PF_24_FLAG,

	PF_Cbk_FLAG,
	PF_Crd_FLAG,
	PF_Cgr_FLAG,
	PF_Cyl_FLAG,
	PF_Cbl_FLAG,
	PF_Cmg_FLAG,
	PF_Ccy_FLAG,
	PF_Cwt_FLAG,

	PF_ADD_FLAG,
	PF_SUB_FLAG,
	PF_MUL_FLAG,
	PF_DIV_FLAG,

	PF_LOG_FLAG,
	PF_HEX_FLAG,
	PF_EXP_FLAG,

	PF_W0_FLAG,
	PF_W1_FLAG,
	PF_W2_FLAG,
	PF_W3_FLAG,
	PF_W4_FLAG,
	PF_W5_FLAG,
	PF_W6_FLAG,
	PF_W7_FLAG,
	PF_W8_FLAG,
	PF_W9_FLAG,
	PF_W10_FLAG,
	PF_W11_FLAG,
	PF_W12_FLAG,
	PF_W13_FLAG,
	PF_W14_FLAG,
	PF_W15_FLAG,

	PF_LAST_FLAG
};

#define MAX_PARSE_FLAGS PF_LAST_FLAG+1


struct PARSE_FLAGS {
        char *token;
	char *name;

        int   index;
        int   set;
        int   order;
	int   pad;
};

#ifdef INCLUDE_PARSE_FLAGS

struct PARSE_FLAGS parse_flags[] = {
        { "/a",         "A Flag",     PF_A_FLAG,   0, 0, 0 },
        { "/b",         "B Flag",     PF_B_FLAG,   0, 0, 0 },
        { "/c",         "C Flag",     PF_C_FLAG,   0, 0, 0 },
        { "/d",         "D Flag",     PF_D_FLAG,   0, 0, 0 },
        { "/e",         "E Flag",     PF_E_FLAG,   0, 0, 0 },
        { "/f",         "F Flag",     PF_F_FLAG,   0, 0, 0 },
        { "/g",         "G Flag",     PF_G_FLAG,   0, 0, 0 },
        { "/h",         "H Flag",     PF_H_FLAG,   0, 0, 0 },
        { "/i",         "I Flag",     PF_I_FLAG,   0, 0, 0 },
        { "/j",         "J Flag",     PF_J_FLAG,   0, 0, 0 },
        { "/k",         "K Flag",     PF_K_FLAG,   0, 0, 0 },
        { "/l",         "L Flag",     PF_L_FLAG,   0, 0, 0 },
        { "/m",         "M Flag",     PF_M_FLAG,   0, 0, 0 },
        { "/n",         "N Flag",     PF_N_FLAG,   0, 0, 0 },
        { "/o",         "O Flag",     PF_O_FLAG,   0, 0, 0 },
        { "/p",         "P Flag",     PF_P_FLAG,   0, 0, 0 },
        { "/q",         "Q Flag",     PF_Q_FLAG,   0, 0, 0 },
        { "/r",         "R Flag",     PF_R_FLAG,   0, 0, 0 },
        { "/s",         "S Flag",     PF_S_FLAG,   0, 0, 0 },
        { "/t",         "T Flag",     PF_T_FLAG,   0, 0, 0 },
        { "/u",         "U Flag",     PF_U_FLAG,   0, 0, 0 },
        { "/v",         "V Flag",     PF_V_FLAG,   0, 0, 0 },
        { "/w",         "W Flag",     PF_W_FLAG,   0, 0, 0 },
        { "/x",         "X Flag",     PF_X_FLAG,   0, 0, 0 },
        { "/y",         "Y Flag",     PF_Y_FLAG,   0, 0, 0 },
        { "/z",         "Z Flag",     PF_Z_FLAG,   0, 0, 0 },

        { "/0",         "0 Flag",     PF_0_FLAG,   0, 0, 0 }, /* This may mess with the math parser ??? */
        { "/1",         "1 Flag",     PF_1_FLAG,   0, 0, 0 },
        { "/2",         "2 Flag",     PF_2_FLAG,   0, 0, 0 },
        { "/3",         "3 Flag",     PF_3_FLAG,   0, 0, 0 },
        { "/4",         "4 Flag",     PF_4_FLAG,   0, 0, 0 },
        { "/5",         "5 Flag",     PF_5_FLAG,   0, 0, 0 },
        { "/6",         "6 Flag",     PF_6_FLAG,   0, 0, 0 },
        { "/7",         "7 Flag",     PF_7_FLAG,   0, 0, 0 },
        { "/8",         "8 Flag",     PF_8_FLAG,   0, 0, 0 },
        { "/9",         "9 Flag",     PF_9_FLAG,   0, 0, 0 },
        { "/10",        "10 Flag",    PF_10_FLAG,  0, 0, 0 },
        { "/11",        "11 Flag",    PF_11_FLAG,  0, 0, 0 },
        { "/12",        "12 Flag",    PF_12_FLAG,  0, 0, 0 },
        { "/13",        "13 Flag",    PF_13_FLAG,  0, 0, 0 },
        { "/14",        "14 Flag",    PF_14_FLAG,  0, 0, 0 },
        { "/15",        "15 Flag",    PF_15_FLAG,  0, 0, 0 },
        { "/16",        "16 Flag",    PF_16_FLAG,  0, 0, 0 },
        { "/17",        "17 Flag",    PF_17_FLAG,  0, 0, 0 },
        { "/18",        "18 Flag",    PF_18_FLAG,  0, 0, 0 },
        { "/19",        "19 Flag",    PF_19_FLAG,  0, 0, 0 },
        { "/20",        "20 Flag",    PF_20_FLAG,  0, 0, 0 },
        { "/21",        "21 Flag",    PF_21_FLAG,  0, 0, 0 },
        { "/22",        "22 Flag",    PF_22_FLAG,  0, 0, 0 },
        { "/23",        "23 Flag",    PF_23_FLAG,  0, 0, 0 },
        { "/24",        "24 Flag",    PF_24_FLAG,  0, 0, 0 },

        { "/Cbk",       "Cbk Flag",   PF_Cbk_FLAG, 0, 0, 0 },
        { "/Crd",       "Crd Flag",   PF_Crd_FLAG, 0, 0, 0 },
        { "/Cgr",       "Cgr Flag",   PF_Cgr_FLAG, 0, 0, 0 },
        { "/Cyl",       "Cyl Flag",   PF_Cyl_FLAG, 0, 0, 0 },
        { "/Cbl",       "Cbl Flag",   PF_Cbl_FLAG, 0, 0, 0 },
        { "/Cmg",       "Cmg Flag",   PF_Cmg_FLAG, 0, 0, 0 },
        { "/Ccy",       "Ccy Flag",   PF_Ccy_FLAG, 0, 0, 0 },
        { "/Cwt",       "Cwt Flag",   PF_Cwt_FLAG, 0, 0, 0 },

        { "/add",       "Add Flag",   PF_ADD_FLAG, 0, 0, 0 },
        { "/sub",       "Sub Flag",   PF_SUB_FLAG, 0, 0, 0 },
        { "/mul",       "Mul Flag",   PF_MUL_FLAG, 0, 0, 0 },
        { "/div",       "Div Flag",   PF_DIV_FLAG, 0, 0, 0 },

        { "/log",       "Log Flag",   PF_LOG_FLAG, 0, 0, 0 },
        { "/hex",       "Hex Flag",   PF_HEX_FLAG, 0, 0, 0 },
        { "/exp",       "Exp Flag",   PF_EXP_FLAG, 0, 0, 0 },

        { "/w0",         "w0 Flag",   PF_W0_FLAG,  0, 0, 0 },
        { "/w1",         "w1 Flag",   PF_W1_FLAG,  0, 0, 0 },
        { "/w2",         "w2 Flag",   PF_W2_FLAG,  0, 0, 0 },
        { "/w3",         "w3 Flag",   PF_W3_FLAG,  0, 0, 0 },
        { "/w4",         "w4 Flag",   PF_W4_FLAG,  0, 0, 0 },
        { "/w5",         "w5 Flag",   PF_W5_FLAG,  0, 0, 0 },
        { "/w6",         "w6 Flag",   PF_W6_FLAG,  0, 0, 0 },
        { "/w7",         "w7 Flag",   PF_W7_FLAG,  0, 0, 0 },
        { "/w8",         "w8 Flag",   PF_W8_FLAG,  0, 0, 0 },
        { "/w9",         "w9 Flag",   PF_W9_FLAG,  0, 0, 0 },
        { "/w10",        "w10 Flag",  PF_W10_FLAG,  0, 0, 0 },
        { "/w11",        "w11 Flag",  PF_W11_FLAG,  0, 0, 0 },
        { "/w12",        "w12 Flag",  PF_W12_FLAG,  0, 0, 0 },
        { "/w13",        "w13 Flag",  PF_W13_FLAG,  0, 0, 0 },
        { "/w14",        "w14 Flag",  PF_W14_FLAG,  0, 0, 0 },
        { "/w15",        "w15 Flag",  PF_W15_FLAG,  0, 0, 0 },

        { "/lt",        "Last Flag",  PF_LAST_FLAG, 0, 0, 0 },
};

#endif


struct PARSE_STACK {
	int ncmds; 		/* total number of individual commands */
	int ntokens;  		/* Total number of tokens in currently parsed command */

	char  cmd_tok[MAX_TOKENS][MAX_TOKENS];  /* Separate commands that where delineated by a ';' char */
	char      tok[MAX_TOKENS][MAX_TOKENS];  /* Commands + Arguments */

	struct PARSE_FLAGS pflags[MAX_PARSE_FLAGS];

	char *firstArg;
	char *secondArg;
	char *thirdArg;
	char *fourthArg;
	char *fifthArg;
	char *sixthArg;
	char *seventhArg;
	char *eighthArg;
	char *ninethArg;
	char *tenthArg;

	char currentCmd[256];
};

extern struct PARSE_STACK parse_stack[MAX_PARSE_STACK];
extern struct PARSE_STACK *args;


struct QUERY_VAR {
        char               name[256];	/* Var name */
        int                ival;	/* Int results */
        int                ntype;	/* DOUBLE, INTEGER, CHARSTAR */
        int                stype;	/* DVAR, GSVAR, HEADER */
        int                indx;	/* Index into dval_list or gsvar_list */
        double             dval;	/* Double results */
        char               cval[256];	/* Char results */
        char               buf[256];	/* Ascii representation of the value */
	struct HEAD_PARAMS phead;	/* Pointer to HEADER values */

};

#endif
