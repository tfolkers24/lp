
#ifndef GAUSSIAN_H
#define GAUSSIAN_H

#define MAX_GAUSSIAN_FITS  24

struct GAUSSIAN_FIT {
	int    xmode;
	int    pad;

	int    used;
	int    active;

	double peak;
	double center;
	double hp;
	double sumK;
	double sumKm;
	double hghterr;
	double cnterr;
	double hwerr;

	char   linename[80];
};

struct GAUSSIAN {
	int nfits;
	int pad;
	struct GAUSSIAN_FIT fits[MAX_GAUSSIAN_FITS];
};

#define MAX_GAUSSIAN_RESULTS 2048

struct GAUSSIAN_ELEMENT {

        double scan;
        double el;
        double peak[4];
        double sum[4];
};

struct GAUSSIAN_RESULTS {
        char magic[16];

        char backend[4][16];

        int  n;
        int pad;

        struct GAUSSIAN_ELEMENT scans[MAX_GAUSSIAN_RESULTS];
};


#endif
