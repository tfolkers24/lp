/*
 * structure of mailboxdefs file
 *
 * used by tcpmg.c and io.c of rambo   
 */

/* every destination has a port # as defined by the mailboxdefs file */

#define MAXPORT  10 
#define MAXNAME  30
#define MAXDEST 256

struct DEST {
  int dport;				/* Port number */
  char host[MAXNAME];			/* Computer that owns this mailbox */
  char dname[MAXNAME];			/* Mailbox name */
  short raw;				/* Non-zero if this is a `raw' socket */
};

struct DEST *find_dest();
struct DEST *find_port();
