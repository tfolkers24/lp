#ifndef FOCUS_H
#define FOCUS_H

#define MAX_FOCUS_HISTORY 512
#define MAX_FOCUS_LABELS    4
#define FOCUS_LABEL_SIZE  256 

#define FOCUS_SORTED_SCAN   1
#define FOCUS_SORTED_ELEV   2
#define FOCUS_SORTED_TAMB   3

#define FOCUS_SCALE_NONE    0
#define FOCUS_SCALE_ADD     1
#define FOCUS_SCALE_SUB     2
#define FOCUS_SCALE_MULTI   3
#define FOCUS_SCALE_DIVIDE  4

#define FOCUS_AXIS_AX       0
#define FOCUS_AXIS_NS       1
#define FOCUS_AXIS_EW       2

struct FOCUS_HISTORY_ELEM {
        char   source[32];

        float  scan;
	float  freq;
        float  el;
        float  F0;

        float  fit;
        float  peak;
        float  hp;
        float  cnterr;

	float  tamb;
	float  scaling;
};


struct FOCUS_HISTORY_SEND {
        char   magic[16];

	int    nfocs;
	int    bfit;				/* If True, there are baseline fitting values to plot */
	int    plotwin;
	int    lines;
	int    sort_order;			/* 0 = elevation; 1 = Temperature */
	int    scale;				/* 0 = none; 1 = add, 2 = sub, 3 = multi 4 = divide */
	int    axis;				/* 0 = AX, 1 = NS, 2 = EW */
	int    pad;

	double xlabel;
	double ylabel;

	char   blabels[MAX_FOCUS_LABELS][256];	/* Baseline Fit Labels */

        float  bplot[MAX_FOCUS_HISTORY];	/* Baseline arrays; in this case model fitting */

        struct FOCUS_HISTORY_ELEM focuses[MAX_FOCUS_HISTORY];
};

#endif
