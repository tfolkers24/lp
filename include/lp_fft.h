#ifndef LP_FFT_H
#define LP_FFT_H

#define MAX_FFT_LENGTH (MAX_CHANS / 2)


struct FFT_INFO {
	char   magic[32];
	int    num;
	int    plotwin;
	int    xset;
	float  xb;
	float  xe;

	struct HEADER head;

	float  xdata[MAX_FFT_LENGTH];
	float  ydata[MAX_FFT_LENGTH];
};

#endif
