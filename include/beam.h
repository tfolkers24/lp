
#define BEAMEFF_LOG    "beam_eff.log"

extern int nscans;
extern double sum, sqsum, avg, rms;

typedef struct
{
  double f_sky;     // sky frequency (GHz)
  double rej;       // rejection (dB)
  double T_ant;     // antenna temperature TA* (K)
  double ta_s;      // antenna temperature std dev (+/-)
  double theta_p;   // planet diameter (arcsec)
  double R_mars;    // heliocentric distance (a.u.)
  double eta_mb;    // main-beam efficiency    
  double err;       // main-beam efficiency uncertainty
  int    planet;    // 1) Jupiter  2) Venus  3) Mars  4) Saturn  5) Moon
  int    bknd;      // backend
  char   obs[4];    // observer inits
}STATE;

typedef struct
{
     int year;
     int mon;
     int day;
     int hour;
     int min;
     int UseCurrent;
} TIME;

typedef struct
{
     double VenDiam;
     double MarDiam;
     double MarDist;
     double JupDiam;
     double SatDiam;
     double SatIncl;
     double MoonAge; 
     double Day;
} EPH;

typedef struct
{
  char buffer[1000]; // result here
  STATE state;
  TIME time;
  EPH eph;
}GLOBAL;

extern GLOBAL global;

