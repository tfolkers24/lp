#ifndef FFB_H
#define FFB_H

#define MAX_FFB_CHANS 2560

struct FFB_CHANNELS {
        int  chan;
        int  crate;
        int  board;
        int  pad;
        char con2[16];
        char con3[16];
        char con4[16];
};

extern struct FFB_CHANNELS ffb_chans[MAX_FFB_CHANS];

#endif
