#ifndef OBJECT_H
#define OBJECT_H

#ifndef MAX_IFS
#define MAX_IFS 		 4
#endif

#ifndef MAX_MEASUREMENTS
#define MAX_MEASUREMENTS 	128
#endif

#ifndef MAX_OBJECTS
#define MAX_OBJECTS 		32
#endif

#define MODE_NUM                 1
#define MODE_EL                  2

#define MODE_NOLINES             0
#define MODE_LINES               1

#define MODE_JANSKYS             1
#define MODE_TMB                 2
#define MODE_EFF                 3
#define MODE_APP                 4


struct RESULT {
        double measuredTmb;
        double measuredTmbSigma;

        double measuredTr;
        double measuredTrSigma;

        double measuredFlux;
        double measuredFluxSigma;

	double beam_eff;
	double app_eff;
};


struct MEASUREMENT {
        int    nscans;
	int    used;

	double ut;
	double utdate;
	double averageEl;

	double burn;

	double tsys;
	double tcal;

        double bscan;
        double escan;

        double expectedTmb[MAX_IFS];
        double expectedFlux[MAX_IFS];

	struct RESULT results[MAX_IFS];
};

#ifndef MAX_NFIT
#define MAX_NFIT 20
#endif


struct OBJECT {
        char   magic[16];
        char   source[32];

	int    used;
	int    numMeas;

	int    xmode; /* Display mode; num vs el */
	int    ymode; /* Display mode; Jks vs tmb vs eff */

	int    lines; /* Display the yerrorlines if true */
	int    plotwin;

	int    bplot;
	int    ncoeff;

	int    fit_scaled; 		/* if true the fit curves have already been scaled */
	int    pad;

	double xmin;			/* if non-zero, use instead of auto-scale */
	double xmax;

	double xlabel;			/* X & Y positions of fit labels */
	double ylabel;

	double paperw;
	double paperh;

	double normalize[MAX_IFS];	/* if non zero, multiple valus by this. */

        double coeff[MAX_IFS][MAX_NFIT];

	struct MEASUREMENT measurements[MAX_MEASUREMENTS];

	float  fitcurves[MAX_IFS][MAX_MEASUREMENTS];
};


struct OBJECT_GLOBAL_MEMORY {
	int nobjIndx;
	int nobsIndx;

	struct OBJECT objects[MAX_OBJECTS];
};

extern struct OBJECT_GLOBAL_MEMORY *OGM;

#endif

