#ifndef TSYS_H
#define TSYS_H

#define MAX_TSYS_ARRAY    8192
#define MAX_TSYS_BACKENDS   20
#define MAX_TSYS_FREQS     128
#define MAX_TSYS_BINS      256

struct XYZ_ELEMENT {
	int   freq;
	float el;
	int   num;
	int   mask;
	int   tsys[MAX_TSYS_BACKENDS];
};


struct TSYS_ARRAY {
	int    num;
	int    pad;

	float  fmin;
	float  fmax;

	float  elmin;
	float  elmax;

	struct XYZ_ELEMENT array[MAX_TSYS_ARRAY];
};


struct TSYS_ELEM {
        int used;
        int freq;
        int tsys[90];
        int cnts[90];
};


struct TSYS_FREQ {
	int freq;
	int used;
	int pad;
	int elev[90];
};


struct TSYS_SEND {
	char   magic[32];
	int    plotwin;
	int    num; 	/* Number of freq bins */
	int    xdelay;

	struct TSYS_FREQ freqs[MAX_TSYS_FREQS];
};

struct TSYS_FREQ_ACCUM {
	int freq;
	int elev;
	int cnts;
};


struct TSYS_WORKING_ACCUM {
	struct TSYS_FREQ_ACCUM accum[90];
};


#endif
