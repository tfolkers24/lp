
#ifndef BACKENDS_H

#define BACKENDS_H

/* A backend is a collection of spectra, all the same resolution, produced by
 * the same physical spectrometer. There may be more than one backend per
 * physical spectrometer if it can generate multiple resolution spectra at the
 * same time, e.g. 250k and 1M FFBs. */

/* A part is one spectrum, produced by a single receiver IF signal. */


#define MAX_BACKENDS 5
#define MAX_PARTS    4

#define MAX_VALUE_1MHZ   65535
#define MAX_VALUE_250KHZ 65535
#define MAX_VALUE_AOS_A  65535
#define MAX_VALUE_AOS_B  65535
#define MAX_VALUE_CTS    65535

#define MAX_1MHZ_CHANNELS   2048
#define MAX_250KHZ_CHANNELS  512
#define MAX_FFB_CHANNELS MAX_250KHZ_CHANNELS + MAX_1MHZ_CHANNELS

#define MAX_AOS_A_CHANNELS 2048
#define MAX_AOS_B_CHANNELS 2048
#define MAX_AOS_C_CHANNELS 2048
#define MAX_AOS_CHANNELS (MAX_AOS_A_CHANNELS + MAX_AOS_B_CHANNELS + MAX_AOS_C_CHANNELS)

#define MAX_CTS_A_CHANNELS 7500
#define MAX_CTS_CHANNELS   (MAX_CTS_A_CHANNELS)

#define MAX_SPEC_CHANS (MAX_CTS_CHANNELS) // Set to the biggest backend

#define BACKEND_FB_1MHZ       0
#define BACKEND_FB_250KHZ     1
#define BACKEND_AOS_1MHZ      2
#define BACKEND_AOS_250KHZ    3
#define BACKEND_CTS_50KHZ     4
#define BACKEND_DBE_4GHZ      5  /* Define DBE backend here for rtdata's sake */
#define BACKEND_OPTICAL       6

//
// Min Refresh Rate per Backend in milliseconds
//
#define FFB_REFRESH_RATE_MS     100
#define AOS_REFRESH_RATE_MS     100
#define CTS_REFRESH_RATE_MS     200

//
// List of Physical Back Ends.
//
#define FFB_BE                0
#define AOS_BE                1
#define CTS_BE                2
#define MAX_PHYSICAL_BE       3
#define DBE_BE                4  /* Define DBE backend here for rtdata's sake */

/* DBE Channels */
#define N_IF_CHANS            4

#define MAX_DBE_CHAN         20
#define MAX_DBE_PHASE         8


#endif
