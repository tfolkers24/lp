#ifndef WINDOW_H
#define WINDOW_H

#include "defines.h"

struct WINDOW_ELEMENT {
	int    active;
	int    used;

        double xmin, xmax;
        double ymin, ymax;
};

struct WINDOWS {
        int nwindows;
        int pad;

        struct WINDOW_ELEMENT windows[MAX_WINDOWS];
};

#endif
