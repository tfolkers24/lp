#ifndef ACCUM_H
#define ACCUM_H

#define MAX_ACCUM_SCANS    512
#define MAX_IFS              4
#define MAX_NFIT            20

#define ACCUM_PLOT_X_NONE    0
#define ACCUM_PLOT_X_NUM     1
#define ACCUM_PLOT_X_EL      2
#define ACCUM_PLOT_X_UT      3
#define ACCUM_PLOT_X_SCAN    4
#define NUM_ACCUM_PLOT_X     5

#define ACCUM_PLOT_Y_NONE    0
#define ACCUM_PLOT_Y_SIG     1
#define ACCUM_PLOT_Y_SKY     2
#define ACCUM_PLOT_Y_SMROR   3
#define ACCUM_PLOT_Y_TSYS    4
#define ACCUM_PLOT_Y_TASTAR  5
#define ACCUM_PLOT_Y_TR      6
#define ACCUM_PLOT_Y_JANSKY  7
#define ACCUM_PLOT_Y_NORMAL  8
#define NUM_ACCUM_PLOT_Y     9

#define MAX_CONT_SAMPLES  4096		/* Maximum length of reduced contiuum samples */

#define MAX_GLABELS         16


struct ACCUM_SCAN_RESULT {
	float scan;
	float ut;
	float el;
	float sig;
	float sky;
	float smror;  /* S-R/R */
	float tsys;
	float tastar;
	float tr;
	float factor;
	float janskys;
	float normalized;
};

struct ACCUM_RESULTS {
	int    num;

	struct ACCUM_SCAN_RESULT results[MAX_ACCUM_SCANS];
};

struct ACCUM_RESULTS_SEND {
	char   magic[16];

	int    plot_select1;   /* Which x-axis to plot */
	int    plot_select2;   /* Which y-axis to plot */
	int    issmt;          /* Data is from SMT */
	int    scanct;         /* Number of scans in Accum */
	int    plotwin;
	int    valid;	       /* Which of the MAX_IFS is valid */
	int    plotExpected;   /* Plot the Expected values if True */
	int    bplot;	       /* if true, expect a fitted curve */
	int    ncoeff;         /* Num of coeff in curve fitting */
	int    pad;

	float  bscan;          /* First Scan in Accum */
	float  escan;          /* Last  Scan in Accum */

	float  paperw;	       /* Plot paper size */
	float  paperh;

	double avgTaStar[MAX_IFS];
	double taSigma[MAX_IFS];
	double expectedTaStar[MAX_IFS];

	double avgTr[MAX_IFS];
	double trSigma[MAX_IFS];
	double expectedTr[MAX_IFS];

	double avgJanskys[MAX_IFS];
	double jkSigma[MAX_IFS];
	double expectedJanskys[MAX_IFS];

	double coeff[MAX_IFS][MAX_NFIT];


	struct HEADER        head[MAX_IFS];
	struct ACCUM_RESULTS ar[MAX_IFS];

	float  fitcurve[MAX_IFS][MAX_ACCUM_SCANS];
};

struct CONTINUUM_PLAIN {
	char   magic[16];

	int    issmt;          /* Data is from SMT */
	int    dlen;           /* Lenght of actual Data */
	int    ons;            /* Display 'ons' if True */
        int    yaxis_scale;    /* Linear vs. log10() flag */
	int    ngauss;         /* Size of Gaussian data; should = dlen */
	int    plotwin;
	int    plotStyle;      /* histo, lines, points, linepts */
	int    gridMode;       /* None, x-only, y-only, full */
	int    autoscale;      /* 0 = Y_AUTO_SCALE, 1 = Y_FIXED_SCALE */
	int    zline;	       /* Add a zero line if true */
	int    xstart;         /* Xwindow placement; if non-zero */
	int    ystart;

	float  yplotmax;       /* Upper and lower extents of plot */
	float  yplotmin;

	char   glabels[MAX_GLABELS][80];  /* Gaussian Fit Results */

	char   xlabel[80];
	char   ylabel[80];

	struct HEADER head;

	float  xdata[MAX_CONT_SAMPLES];
	float  ydata[MAX_CONT_SAMPLES];
	float  gdata[MAX_CONT_SAMPLES];  /* Possible Gaussian Data */
};


struct TIP_RESULTS {
	float tau0;
	float sigmaA;
	float sigmaB;
	float a;
	float b;
};


struct CONTINUUM_SPTIP {
	char   magic[16];
	int    issmt;          /* Data is from SMT */
	int    dlen;           /* Lenght of actual Data */
	int    ons;            /* Display 'ons' if True */
	int    plotwin;

	struct TIP_RESULTS results[4];

	struct HEADER head1;
	struct HEADER head2;

	float  xdata[64];
	float  ydata1[64];
	float  ydata2[64];
};

#endif
