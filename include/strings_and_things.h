#ifndef STRINGS_AND_THINGS_H
#define STRINGS_AND_THINGS_H

#define MAX_LABEL_LENGTH      128
#define MAX_IMAGE_LABELS       64
#define MAX_IMAGE_MARKERS      32
#define MAX_IMAGE_HLINES       32
#define MAX_IMAGE_FORKS	       32
#define MAX_FORK_TINES	        8 
#define MAX_TBLOCK_LABELS       8
#define MAX_IMAGE_GAUSS_RESULTS 8
#define MAX_PSTAMPS            16

#ifndef MAGIC_SIZE
#define MAGIC_SIZE	       16
#endif

#define LABEL_CENTER 0
#define LABEL_LEFT   1
#define LABEL_RIGHT  2
#define LABEL_DOWN   3

struct IMAGE_LABELS {
	int   used;
	int   color;
	int   direct;
	int   nohead;
	int   size;
	int   pad;
	float x;
	float y;
	char  label[MAX_LABEL_LENGTH];
};


struct IMAGE_MARKERS {
	int   used;
	int   color;
	float x;
	float y;
	float y2;
	float pad;
};


struct IMAGE_HLINE {
	int used;
	int color;
	float value;
	float pad;
};


struct FORK_TINE {
	float x;
	float y;
};

struct IMAGE_FORKS {
	int              used;
	int              color;
	int              ntines;
	int		 pad;
	float            crossT;
	float            topT;
	char             label[MAX_LABEL_LENGTH];
	struct FORK_TINE tines[MAX_FORK_TINES];
};



struct IMAGE_DATE_STRING {
	int   used;
	int   color;
	float x;
	float y;
	char  date[80];
};


struct PLOT_BOX {
	float x;	/* Location on the plot in lower-axis units */
	float y;
	float w;	/* width & height in lower-axis units */
	float h;
};


struct TITLE_BLOCK {
	int   used;	/* If true, draw */
	int   color;	/* Color to use */
	int   drawbox;	/* If True, draw box around */
	int   nlabels; 	/* Contains number of labels used */

	struct PLOT_BOX     box;
	struct IMAGE_LABELS labels[MAX_TBLOCK_LABELS];
};


struct GAUSS_INFO_RESULTS {
	int    used;
	int    pad;

	double x;
	double y;

	char string[128];
};


struct IMAGE_GAUSS {
	int  used;
	int  color;

	struct GAUSS_INFO_RESULTS results[MAX_IMAGE_GAUSS_RESULTS];
};



struct STAMP {
        int   used;
        int   color;
        int   size;
        int   pad;

        double linexmin;
        double linexmax;
        double stampULx;
        double stampULy;
	double stampLRx;
	double stampLRy;

	double ymax;

        char  label[MAX_LABEL_LENGTH];
};


struct IMAGE_PSTAMPS {
        int nstamps;
        int pad;

        struct STAMP stamps[MAX_PSTAMPS];
};


struct STRINGS_AND_THINGS {
	char   magic[MAGIC_SIZE];

	struct TITLE_BLOCK        tblock;
	struct IMAGE_DATE_STRING  date;
	struct IMAGE_LABELS       labels[MAX_IMAGE_LABELS]; 	/* Plot Labels */
	struct IMAGE_MARKERS      markers[MAX_IMAGE_MARKERS]; 	/* Plot Markers */
	struct IMAGE_HLINE        hlines[MAX_IMAGE_HLINES]; 	/* User assigned horizontal T markers */
	struct IMAGE_FORKS 	  forks[MAX_IMAGE_FORKS]; 	/* plot markers for hyper-fine lines */
	struct IMAGE_GAUSS        gauss;			/* User place Gaussian info */
	struct IMAGE_PSTAMPS      pstamp;			/* Small plots overlayed on main plot */
};

#endif
