
#ifndef CHANNELS_H
#define CHANNELS_H

struct SCAN_INFO {
        double        mean;
	double        rms;
	int           len;
	float         data[MAX_CHANS];
	int           good[MAX_CHANS];
	struct HEADER head;
};



struct CHANNELS {
        double x;
        double y;
        double c;
	int    good;
};



struct DATASET {
	int              nbad;
	double           coeff[16];
	double           xd[MAX_CHANS];
	double           yd[MAX_CHANS];
	double           sig[MAX_CHANS];
	double           badchans[MAX_CHANS];
	struct SCAN_INFO info[2];
	struct CHANNELS  chans[MAX_CHANS];
};


#endif
