#ifndef DEFINES_H

#define DEFINES_H

#define MG_DIAMETER      (10.00)
#define MG_SUB_DIA       (0.686)
#define MG_SURFACE_RMS   (25.00)
#define MG_BLOCKAGE      (0.968)
#define MG_TAPER         (12.00)
#define MG_JANSKY_FACTOR (43.73)
#define MG_J_FACTOR      (35.16)

#define KP_DIAMETER      (12.00)
#define KP_SUB_DIA       (0.750)
#define KP_SURFACE_RMS   (55.00) /* was 65 */ /* As per Nick - 2016/01/06 */
#define KP_BLOCKAGE      (0.970)
#define KP_TAPER         (12.00) /* 10.4, 10.0 Measured Jan-05-2016; twf/glauria */
#define KP_JANSKY_FACTOR (30.72)
#define KP_J_FACTOR      (24.42)

#define SWITCHED               1
#define AVERAGE                2
#define DATASPREAD      0.000001

#ifndef CTR
#define CTR          0.017453292
#endif

#define GNUPLOT                1
#define PSPLOT                 2

#define CONTMAP               64

#define MAX_IMAGES            10
#define MAX_ENV               16
#define MAX_TRACES            16
#define MAX_SCANS          16384
#define MAX_CHANS         (16384*2)
#define MAX_IGNORED          256
#define MAX_TOKENS           256
#define MAX_BEAMS              4
#define MAX_BASELINES         64
#define MAX_BAD_CHANS        512 
#define MAX_SPEC_FOCUS        16   /* Maximum number of focus position for spec focus */

#define LINEAR_BASELINE_SMOOTHING 0
#define POLY_BASELINE_SMOOTHING   1

#define CALMODE_NONE           0
#define CALMODE_CALSCALE       1
#define CALMODE_BEAMEFF        2
#define CALMODE_TELE_EFF       3

#define INTEGER                1
#define FLOAT                  2
#define DOUBLE                 3
#define CHARSTAR               4

#define CHECK_SOURCE       (1<<0)
#define CHECK_SCANS        (1<<1)
#define CHECK_DATE         (1<<2)
#define CHECK_BKEND        (1<<3)
#define CHECK_FREQ         (1<<4)
#define CHECK_ALL          (CHECK_SOURCE | CHECK_SCANS | CHECK_DATE | CHECK_BKEND | CHECK_FREQ)

#define VERBOSE_PARSE          1
#define VERBOSE_CALIBRATE      2
#define VERBOSE_CRITIRIA       4
#define VERBOSE_COMM           8
#define VERBOSE_FBOFFSET      16 
#define VERBOSE_BASELINES     32 
#define VERBOSE_STATS         64 
#define VERBOSE_RECURSIVE    128 
#define VERBOSE_BATCH        256 
#define VERBOSE_ALIAS        512 
#define VERBOSE_FILEOPS     1024 
#define VERBOSE_GRIDMAP     2048 
#define VERBOSE_FLAGS       4096 
#define VERBOSE_ATM         8192 
#define VERBOSE_STACK      16384 

#define MAX_VERBOSE        VERBOSE_STACK

#define VERBOSE_ALL       (VERBOSE_PARSE | VERBOSE_CALIBRATE | VERBOSE_CRITIRIA | VERBOSE_COMM | VERBOSE_FBOFFSET | VERBOSE_BASELINES | VERBOSE_STATS | VERBOSE_RECURSIVE | VERBOSE_BATCH | VERBOSE_ALIAS | VERBOSE_FILEOPS | VERBOSE_GRIDMAP | VERBOSE_FLAGS | VERBOSE_ATM | VERBOSE_STACK )

/* Program can be in one of three modes */
#define MODE_NOT_SET           0
#define MODE_ALL               0
#define MODE_CONT              1
#define MODE_SPEC              2
#define MODE_OTF               3

#define MODE_FLUX              4 /* MODE's from here on are to flag the xgraphic program */
#define MODE_FLUX_HISTORY      5 /* on what's coming in the image struct */
#define MODE_XCOR              6 /* Cross Correlation mode */

/* X-Axis can be plotted in one of the following */
#define XMODE_FREQ             0
#define XMODE_VEL              1
#define XMODE_CHAN             2

/* Plotting styles */
#define PLOT_STYLE_HISTO       0
#define PLOT_STYLE_LINES       1
#define PLOT_STYLE_POINTS      2
#define PLOT_STYLE_LINEPTS     3

/* Y-Axis Scale */
#define Y_AUTO_SCALE           0
#define Y_FIXED_SCALE          1

/* Y-Axis Plotting Mode */
#define LINEAR_PLOT	       0
#define LOG_PLOT	       1

/* Plotting Grid Mode */
#define GRID_MODE_NONE         0
#define GRID_MODE_X_ONLY       1
#define GRID_MODE_Y_ONLY       2
#define GRID_MODE_FULL         3

#define MAX_FILTER_SEGS       64

#define BKMATCH_FEED           0
#define BKMATCH_RESOLUTION     1
#define BKMATCH_NAME           2
#define BKMATCH_NAMERES        3

#define SPEED_OF_LIGHT        (299792458.0)
#define SPEED_OF_LIGHT_COAX   ((1.0 / sqrt(2.2)) * SPEED_OF_LIGHT)

#define SITE_NONE              0
#define SITE_KITT_PEAK         1
#define SITE_MT_GRAHAM         2
#define SITE_TUCSON            3

#define SEND_PLAIN             1
#define SEND_SPTIP             2

#define OFFS                   0
#define ONS                    1

#define DATA_FILE              0
#define GAINS_FILE             1

#define MAX_STACK           2048

#define READ_HISTORY  	       1
#define WRITE_HISTORY 	       2

#define MAX_SOURCE_LIST     1024

#define MAX_PARSE_STACK       16

#define MAX_WINDOWS	      16

#define READ_REVERSE           0
#define READ_FORWARD           1

/* which plot was last */
#define LAST_PLOT_SPEC_ACCUM      1
#define LAST_PLOT_SPEC_FFT        2
#define LAST_PLOT_SPEC_HISTO      3

#define LAST_PLOT_CONT_ACCUM      4
#define LAST_PLOT_CONT_PLAIN      5
#define LAST_PLOT_CONT_SPTIP      6
#define LAST_PLOT_CONT_OBJECT     7

#define LAST_PLOT_FLUX_INFO       8
#define LAST_PLOT_FLUX_HISTORY    9

#define LAST_PLOT_OTF_CUT        10

#define BOGUS_TSYS_VALUE	(10000.0)
#define BOGUS_TCAL_VALUE	(2000.0)

#endif

