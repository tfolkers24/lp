
#ifndef SHELL_H
#define SHELL_H

#define MAX_SHELL_FITS  24
#define MAX_SHELL_COEFF  8

struct SHELL_FIT {
	int    used;
	int    active;

	int    peak_left_chan;
	int    peak_right_chan;

	int    left_edge_chan;
	int    right_edge_chan;

	double area_kmhz;
	double area_kmhz_error;
	double sig_freq;
	double sig_freq_error;
	double img_freq;
	double img_freq_error;
	double vel_expansion;
	double vel_expansion_error;
	double horn_center_ratio;
	double horn_center_ratio_error;
	double temperature;

	double coeff[MAX_SHELL_COEFF];

	char   linename[80];
};

struct SHELL {
	int nshells;
	int label;

	struct SHELL_FIT fits[MAX_SHELL_FITS];
};


struct SHELL_GUESS {
	char  magic[16];

	int   npoints;
	int   pad;

	int   linetype;
	int   linecolor;

	int   pointtype;
	int   pointsize;

	/* User clicked points */
	double xd[ 256];
	double yd[ 256];

	/* Fit to those points */
	double fit[256];
};

#define SHELL_GUESS_SIZE (sizeof(struct SHELL_GUESS))


#endif
