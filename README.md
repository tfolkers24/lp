## Linuxpops -- Single Dish Data Analysis Program

This complete, from scratch, rewrite of Unipops was our attempt to implement the functionality of the long since dead and unsupported Condar and a usable replacement for Line. We have not endeavored to create a one for one replacement, but a functional replacement.

Many of the commands are missing as are the advanced parser functions and scripting language. Although, a limited 'stack' looping feature and linear scripting is supported. i.e. scripts that contain known commands.

What remains is basic data analysis necessary to reduce continuum and spectral line data. Such as displaying individual scans, analyzing sky-tips, focusing and five-points. In addition, it can perform beam efficiency measurements, based on sequence scans, and analyze dual-beam switched beam maps.

On the spectral line portion most functionality of Unipops Line are implemented.

And new to Linuxpops, OTF gridder and mapping analysis package


### Dependencies for Linux:

Packages needed to fetch and build linuxpops; These packages may be called something different in your repo

- automake
- boost boost-devel
- byacc
- calc calc-devel calc-libs
- community-mysql-devel or whatever package provides: mysql/mysql.h and libmysqlclient
- cproto
- doxygen (for documentation compiling)
- flex
- gcc-c++
- gfortran
- git
- gnuplot
- gsl, gsl-lite-devel, gsl-devel
- gtk2, gtk2-devel
- imake
- libnsl2, libnsl2-devel
- linuxdoc-tools
- libX11-devel
- perl
- readline, readline-devel


### Extra packages needed at run-time for certain Linuxpops functions:

- aatm	(Download latest here: https://www.mrao.cam.ac.uk/~bn204/alma/atmomodel.html)
	- Used for Atmospheric modeling.

- xmgrace (Install from your repo or compile and install xmgrace from lp/LP_Support/grace)
	- Most plotting is done in 'gnuplot' but a few routines use 'xmgrace'
	- Set your environmental variable XMGRACE to point to the binary.

- xterm
	- Used as a log-viewer. 'See logwatch command'

### Fetching:

Downloaded via git:

git clone https://tfolkers24@bitbucket.org/tfolkers24/lp.git


### Building for Linux:

- % cd lp
	- the top of the Linuxpops distribution
- % edit extra_flags and env.sh and env.csh to customize for your OS
	- % source env.sh or env.csh 
	- Used for setting installation home: LINUXPOPS_HOME

- % make makefiles
	- watch for any 'red' errors; correct as needed

- % make depend
	 - watch for any 'red' errors; correct as needed

- % make
- If everything builds OK:
	- % sudo -E make install

### Building on MacOS: (Experimental):

We have had moderate success compiling on MacOS. Most of the dependencies and build
differences have been included into the Makefiles and source code.  But, expect some
debugging to get it running.

- Install Xcode from the App Store
	- Open Xcode via the icon and install additional 

- Install MacPort for your OS
	- https://www.macports.org/install.php
	- Follow the instructions on their page for how to install command-line tools.
	- Restart your shell to finish installation paths

- sudo port install imake
- sudo port install gnuplot
- sudo port install gsl
- sudo port install cproto
- sudo port install gtk2*
- sudo port install calc
- sudo port install xmgr
- sudo port install zenity

- Fetch https://downloads.mysql.com/archives/community/
	- install version 5.7.16 or whatever works for your system

- Fetch https://github.com/fxcoudert/gfortran-for-macOS/releases/tag/6.1
	- Or version for your OS

- as root: Edit /opt/X11/bin/gccmakedep:
	- Change: CC=/opt/local/bin/clang-mp-3.9 to CC="/usr/bin/clang -D__DARWIN__"

- Edit env.sh or env.csh, depending on your shell:
	- Comment out and un-comment the appropriate defines
	- 'source env.sh' or 'source env.csh'

- Edit the extra_flags file to customize for MacOS

- % make makefiles
- % make depend
- % make
- % sudo -E make -n install
	- make sure to verify where everything goes

- If all is well,
	- sudo -E make install

### Running Linuxpops:

- Define the following environmental variables:
	- LINUXPOPS_HOME to be the same as used for install
	- LINUXPOPS_GNUPLOT to point to your local binary
	- LINUXPOPS_DOC_VIEWER to point to your favorite document viewer.
	- LINUXPOPS_DEFAULT_ENV to the number of your favorite working linuxpops environment.
	- SITE Set to "Tucson" if your are not at any of our telescopes. 
	- XMGRACE to point to your local binary
	- PRINTER to point to your local printer queue.
- Put $LINUXPOPS_HOME/bin in your shell's path

- Create a working directory to use for analysis such as ~/analysis
- cd to your working directory
- type 'makemem' to create the working GLOBAL memory
- type: 'startlinuxpops' to start the package.



> Written with [StackEdit](https://stackedit.io/).
