
#define MAX_DATA_CHANNELS  8192

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <ctype.h>
#include <fcntl.h>
#include <time.h>
#include <string.h>
#include <dirent.h>
#include <math.h>
#ifndef __DARWIN__
#include <malloc.h>
#endif
#include <signal.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/time.h>

#include "sex.h"
#include "loglib.h"
#include "cactus.h"
#include "sdd.h"
#include "header.h"
#include "observing_par.h"
#include "defines.h"
#include "otf.h"
#include "ds_image_info.h"

#include "spectra.h"
#include "spec.h"

#include "caclib_proto.h"

#ifdef DEBUG_MALLOC
#include "dmalloc.h"
#endif

#define TRUE           1
#define FALSE          0
#define CTR            0.017453292


extern int debug, sockmode, version, chanSelOTF, dontsend;
extern int badChanArray[MAX_BAD_CHAN];


extern struct SOCK *master, *helper, *specplot, *specdata;
