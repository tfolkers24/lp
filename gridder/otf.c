#define DEFINE_OTF_STRUCT
#include "ds_extern.h"
#include "caclib_proto.h"
#include "ds_proto.h"

#include "../include/defines.h"
#include "send_struct.h"

extern struct SOCK *dataserv;

struct HEADER lastHead;
struct DS_IMAGE_INFO ds_image_info;
extern struct DS_IMAGE_INFO *last_image;

struct SEND_STRUCT send_struct;

extern float scanNum, datamax, datamin;
extern int scan, noXTick, noYTick, 
	   opticalFileNeedDisplay, fbScanDone, aosScanDone;

extern char source[], filename[], gfilename[], mainFrameTitle[], oldBuf[];

float otfIFNumber;
int chanSelOTF  = 0;
int ephemObj    = 0;
int lastGraph   = 0;
int ok2grid     = 0;
int dontsend    = 0;
int zeroCellCount = 0;
int artificialCells = 0;

struct GRID_ACCUM workArray[WORK_ARRAY_X][WORK_ARRAY_Y];
double *workArrayChans = NULL;

struct MAP_SCANS    *ignore_scans_head=NULL;
struct SCAN_HEADERS *otf_scan_headers=NULL;

int badChanArray[MAX_BAD_CHAN];

struct OTF_GRID *gridHead = NULL;
struct OTF_GRID *gridLast = NULL;

struct SPECDATA sd;

int writeSingleChan = TRUE; /* for azimuth(deg) + total power output */

int otfInit()
{

  log_msg("otfInit(): Enter");

  otf.initialized        = 0;
  otf.cols               = 32;
  otf.rows               = 32;
  otf.noint              = 1024;
  otf.int_chans[0]       = 508;
  otf.int_chans[1]       = 516;
  otf.lastAction         = NORMAL_MAP;
  otf.manMapRa		 = 0.0;
  otf.manMapDec		 = 0.0;
  otf.manNyquistRa	 = 11.1761475;
  otf.manNyquistDec	 = 8.05853271;
  otf.manMapWidth	 = 0.60;
  otf.manMapHeight	 = 0.60;
  otf.IFNumber           = 0.01;
  otf.nyquistRa          = 22.35;
  otf.nyquistDec         = 22.35;
  otf.map_scans_head     = NULL;
  otf.gridArray          = NULL;
  otf.gridArrayChans     = NULL;
  otf.freqres		 = -1.0;
  otf.restfreq		 = -1.0;
  otf.velocity		 = -1.0;
  otf.xsource		 = -1.0;
  otf.ysource		 = -100.0;

  otfIFNumber = otf.IFNumber;

  log_msg("otfInit(): Done");

  return(0);
}


int freeOtfMemory()
{
  struct OTF_GRID *g;
  struct OTF_GRID *o;

  log_msg("freeOtfMemory(): Enter");

  if(otf.gridArray)
  {
    for(g=gridHead;g;g=o)
    {
      if(g->chans)
      {
        free((char *)g->chans);
      }

      o = g->next;
      free((char *)g);
    }

    free((char *)otf.gridArray);
    otf.gridArray = NULL;
  }

  gridHead = NULL;
  gridLast = NULL;

  if(workArrayChans)
  {
    free((char *)workArrayChans);
    workArrayChans = NULL;
  }

  remMapScans();

  log_msg("freeOtfMemory(): Done");

  return(0);
}


int otfGridInit(cols, rows, noint)
int cols, rows, noint;
{
  int col, row;
  int n, k;

  log_msg("otfGridInit(): Initializing %dx%dx%d Chs", cols, rows, noint);

  freeOtfMemory();

  ephemObj     = 0;

  n = cols * rows;
  if((otf.gridArray=(struct OTF_GRID **)malloc((unsigned)(n * sizeof(struct OTF_GRID *)))) == NULL)
  {
    log_msg("otfGridInit(): Error in malloc(%d)", n * sizeof(struct OTF_GRID));

    return(1);
  }

  bzero((char *)otf.gridArray, n*sizeof(struct OTF_GRID *));

  otf.cols          = cols;
  otf.rows          = rows;
  otf.numCellsTotal = cols * rows;
  otf.noint         = noint;

  n = WORK_ARRAY_X * WORK_ARRAY_Y * otf.noint;
  if((workArrayChans=(double *)malloc((unsigned)(n * sizeof(double)))) == NULL)
  {
    log_msg("otfGridInit(): Error in malloc(%d)", n * sizeof(double));

    workArrayChans = NULL;

    return(1);
  }

  otf.sizeofWorkArray = n * sizeof(double);

  bzero((char *)workArrayChans, otf.sizeofWorkArray);

  k = 0;
  for(col=0;col<WORK_ARRAY_X;col++)
  {
    for(row=0;row<WORK_ARRAY_Y;row++,k++)
    {
      workArray[col][row].chans = &workArrayChans[k*otf.noint];
    }
  }

  otf.initialized    = 1;

  log_msg("otfGridInit(): Done");

  return(0);
}


struct HEADER *do_addHeader(ver, scan, head)
int ver;
float scan;
struct HEADER *head;
{
  struct HEADER *h;
  static int    old_ver          = 0;
  static double old_scan         = 0.0;
  static struct HEADER *old_head = NULL;

   /* If it's the same ver and scan as the last call, return last head */
  if(old_ver == ver && fabs(old_scan-scan) < 0.01)
  {
    return(old_head);
  }

  if((h = checkScanHeader(ver, scan)))
  {
    old_ver  = ver;
    old_scan = scan;
    old_head = h;

    return(h);
  }
  else
  {
    if((h = addScanHeader(ver, scan, head)))
    {
      old_ver  = ver;
      old_scan = scan;
      old_head = h;

      return(h);
    }
    else
    {
      log_msg("do_addHeader(): Error adding scan %.2f, in ver %d", scan, ver);
    }
  }

  return((struct HEADER *)NULL);
}


struct HEADER *checkScanHeader(v, s)
int v;
float s;
{
  struct SCAN_HEADERS *m;
  float ss;

  if(otf_scan_headers == NULL)
  {
    return((struct HEADER *)NULL);
  }

  /* users can pass a scan number like 1234.0, which will ignore all
     scans with that prefix */
  ss = (float)(int)s;

  for(m=otf_scan_headers;m->next; m=m->next) 
  {
    if(m)
    {
      if((m->scan == s || m->scan == ss) && m->ver == v)
      {
        return(&m->head);
      }
    }
  }

  /* Check the last one, if valid */
  if(m)
  {
    if((m->scan == s || m->scan == ss) && m->ver == v)
    {
      return(&m->head);
    }
  }

  return((struct HEADER *)NULL);
}


struct HEADER *addScanHeader(v, s, head)
int v;
float s;
struct HEADER *head;
{
  struct SCAN_HEADERS *m;

  /* Create a fresh list */
  if(otf_scan_headers == NULL)
  {
    if((otf_scan_headers=(struct SCAN_HEADERS *)malloc((unsigned)(sizeof(struct SCAN_HEADERS))))==NULL)
    {
      log_msg("Error in addIgnoreScans malloc(%d)", sizeof(struct SCAN_HEADERS));

      return((struct HEADER *)NULL);
    }

    otf_scan_headers->ver  = v;
    otf_scan_headers->scan = s;

    bcopy((char *)head, (char *)&otf_scan_headers->head, sizeof(struct HEADER));

    otf_scan_headers->next = NULL;

    return(&otf_scan_headers->head);
  }

  /* Find next available slot */
  for(m=otf_scan_headers;m->next; m=m->next)
  {
    ;
  }

  /* Append new scan */
  if((m->next=(struct SCAN_HEADERS *)malloc((unsigned)(sizeof(struct SCAN_HEADERS))))==NULL)
  {
    log_msg("Error in addIgnoreScans malloc(%d)", sizeof(struct SCAN_HEADERS));

    return((struct HEADER *)NULL);
  }

  m->next->ver  = v;
  m->next->scan = s;

  bcopy((char *)head, (char *)&m->next->head, sizeof(struct HEADER));

  m->next->next = NULL;

  return(&m->next->head);
}


int remScanHeaders()
{
  struct SCAN_HEADERS *p;
  struct SCAN_HEADERS *q;

  for(p=otf_scan_headers; p; p=q)
  {
    q = p->next;

    if(p)
    {
      free((char *)p);
    }
  }

  otf_scan_headers = NULL;

  return(0);
}
/* TOMMIE */

int do_ignoreScan(buf)
char *buf;
{
  int n, v;
  float f;

  if(buf)
  {
    n = sscanf(buf, "%d %f", &v, &f);

    if(n == 2)
    {
      if((f > 0.0 && f < 49999.9) && (v > 0 && v < 999))
      {
        if(!checkIgnoreScans(v, f))
        {
          if(!addIgnoreScans(v, f))
	  {
            log_msg("Scan %.2f, in version %d ignored", f, v);
	  }
	  else
	  {
            log_msg("Scan %.2f, in version %d, could NOT be ignored", f, v);
	  }
        }
        else
        {
          log_msg("Scan %.2f already ignored in version %d", f, v);
        }
      }
      else
      {
	log_msg("Scan %f or version: %d, out of range", f, v);
        return(1);
      }
    }
    else
    {
      log_msg("do_ignoreScan(): Missing args for ignore: %d", n);
      return(1);
    }
  }

  return(0);
}


int checkIgnoreScans(v, s)
int v;
float s;
{
  struct MAP_SCANS *m;
  float ss;

  if(ignore_scans_head == NULL)
  {
    return(0);
  }

//  log_msg("Checking if scan %.2f in version %3d is ignored", s, v);

  /* users can pass a scan number like 1234.0, which will ifnore all
     scans with that prefix */
  ss = (float)(int)s;

  for(m=ignore_scans_head;m->next; m=m->next) 
  {
    if(m)
    {
      if((m->scan == s || m->scan == ss) && m->ver == v)
      {
        return(1);
      }
    }
  }

  if(m)
  {
    if((m->scan == s || m->scan == ss) && m->ver == v)
    {
      return(1);
    }
  }

  return(0);
}


int addIgnoreScans(v, s)
int v;
float s;
{
  struct MAP_SCANS *m;

  /* Create a fresh list */
  if(ignore_scans_head == NULL)
  {
    if((ignore_scans_head=(struct MAP_SCANS *)malloc((unsigned)(sizeof(struct MAP_SCANS))))==NULL)
    {
      log_msg("Error in addIgnoreScans malloc(%d)", sizeof(struct MAP_SCANS));

      return(1);
    }

    ignore_scans_head->ver  = v;
    ignore_scans_head->scan = s;
    ignore_scans_head->next = NULL;

    return(0);
  }

  /* Find next available slot */
  for(m=ignore_scans_head;m->next; m=m->next)
  {
    ;
  }

  /* Append new scan */
  if((m->next=(struct MAP_SCANS *)malloc((unsigned)(sizeof(struct MAP_SCANS))))==NULL)
  {
    log_msg("Error in addIgnoreScans malloc(%d)", sizeof(struct MAP_SCANS));

    return(1);
  }

  m->next->ver  = v;
  m->next->scan = s;
  m->next->next = NULL;

  return(0);
}


int remIgnoreScans()
{
  struct MAP_SCANS *p;
  struct MAP_SCANS *q;

  for(p=ignore_scans_head; p; p=q)
  {
    q = p->next;

    if(p)
    {
      free((char *)p);
    }
  }

  ignore_scans_head = NULL;

  return(0);
}


int do_saveIgnoreScans(fp)
FILE *fp;
{
  struct MAP_SCANS *m;
  int i=0;

  if(ignore_scans_head == NULL)
  {
    return(0);
  }

  for(m=ignore_scans_head;m->next; m=m->next, i++)
  {
    if(m)
    {
      fprintf(fp, "ignore %d %7.2f\n", m->ver, m->scan);
    }
  }

  if(m)
  {
    fprintf(fp, "ignore %d %7.2f\n", m->ver, m->scan);
  }

  return(0);
}



int do_printIgnoreScans()
{
  struct MAP_SCANS *m;
  int i=0;

  if(ignore_scans_head == NULL)
  {
    log_msg("None Ignored");

    return(0);
  }

  for(m=ignore_scans_head;m->next; m=m->next, i++)
  {
    if(m)
    {
      log_msg("Ignoring scan %3d = %7.2f, in Version: %d", i, m->scan, m->ver);
    }
  }

  if(m)
  {
    log_msg("Ignoring scan %3d = %7.2f, in Version: %d", i, m->scan, m->ver);
  }

  return(0);
}



int do_clearIgnore()
{
  remIgnoreScans();

  return(0);
}


int checkMapScans(v, s)
int v;
float s;
{
  struct MAP_SCANS *m;

  if(otf.map_scans_head == NULL)
  {
    return(0);
  }

  for(m=otf.map_scans_head;m->next; m=m->next) 
  {
    if(m)
    {
      if(m->scan == s && m->ver == v)
      {
        return(1);
      }
    }
  }

  if(m)
  {
    if(m->scan == s && m->ver == v)
    {
      return(1);
    }
  }

  return(0);
}


int addMapScans(v, s)
int v;
float s;
{
  struct MAP_SCANS *m;

  if(otf.map_scans_head == NULL)
  {
    if((otf.map_scans_head=(struct MAP_SCANS *)malloc((unsigned)(sizeof(struct MAP_SCANS))))==NULL)
    {
      log_msg("Error of addMapScans addMapScans(malloc(%d))", sizeof(struct MAP_SCANS));

      return(1);
    }

    otf.map_scans_head->ver  = v;
    otf.map_scans_head->scan = s;
    otf.map_scans_head->next = NULL;

    return(0);
  }

  for(m=otf.map_scans_head;m->next; m=m->next)
  {
    ;
  }

  if((m->next=(struct MAP_SCANS *)malloc((unsigned)(sizeof(struct MAP_SCANS))))==NULL)
  {
    log_msg("Error of addMapScans addMapScans(malloc(%d))", sizeof(struct MAP_SCANS));

    return(1);
  }

  m->next->ver  = v;
  m->next->scan = s;
  m->next->next = NULL;

  return(0);
}



int remMapScans()
{
  struct MAP_SCANS *p;
  struct MAP_SCANS *q;

  for(p=otf.map_scans_head; p; p=q)
  {
    q = p->next;

    if(p)
    {
      free((char *)p);
    }
  }

  otf.map_scans_head = NULL;

  return(0);
}


int do_printMapScans()
{
  struct MAP_SCANS *m;
  int i=0;

  if(otf.map_scans_head == NULL)
  {
    log_msg("No Scans Loaded");

    return(0);
  }

  for(m=otf.map_scans_head;m->next; m=m->next, i++)
  {
    if(m)
    {
      log_msg("scan %3d = %7.2f, in version: %d", i, m->scan, m->ver);
    }
  }

  if(m)
  {
    log_msg("scan %3d = %7.2f, in version: %d", i, m->scan, m->ver);
  }

  return(0);
}



int getNumSpectra(datalen, noint)
float datalen, noint;
{
  int dl;

  dl = (int)datalen / ( sizeof(float) * ( (int)noint +5));

  return(dl);
}



int resendSpecData(sd)
struct SPECDATA *sd;
{
  char sysBuf[256];
  static time_t oldTime=0;
  time_t t;

  t = time(NULL);

  if(abs(t-oldTime) > 5)
  {
    fprintf(stdout, "Starting specdata viewer\n");
    fflush(stdout);

    log_msg("sock_write() returned Error; starting specdata viewer");

    sprintf(sysBuf, "specdata &");

    system(sysBuf);

//    usleep(250000);

    /* resend */
//    ret = sock_write(specdata, (char *)sd, sizeof(*sd));

//    if(!ret)
//    {
//      fprintf(stdout, "sock_write() Still returning Error; Check Instalation\n");
//      fflush(stdout);

//      log_msg("sock_write() Still returning Error; Check Installation");

//    }
    
    oldTime = t;
  }

  return(0);
}



/*  --------------- entry point of otf ------------------ */

int do_otf( data, head, offdata, gainsdata)
float *data, *offdata, *gainsdata;
struct HEADER *head;
{
  bcopy((char *)head, (char *)&lastHead, sizeof(struct HEADER));

  otf.head       = &lastHead;
  otf.ondata     = data;
  otf.offdata    = offdata;
  otf.gainsdata  = gainsdata;
  otf.numSpectra = getNumSpectra(otf.head->datalen, otf.head->noint);

  if(otf.head->noint > MAX_CHANS)
  {
    ok2grid = 0;
    otf.noint = (int)otf.head->noint;
    log_msg("DATASERV: Can not grid %.0f chans", otf.head->noint);
  }
  else
  {
    ok2grid = 1;

    if(!fillOtfGrid())
    {
	;
    }
  }

  bzero((char *)&sd, sizeof(sd));

  strcpy(sd.datafile, filename);
  strcpy(sd.gainsfile, gfilename);
    
  sd.scanno       = (int)head->scan;
  sd.feed         = (int)round((otf.IFNumber*100.0));
  sd.arrayNum     = 2;

  return(0);
}



int doOtfGridReSize()
{
  if(debug)
  {
    log_msg("doOtfGridReSize()");
  }

  return(0);
}



int computeInten(bc, ec)
int bc, ec;
{
  int j, k, count=0, tcount=0;
  double accum, intInten, intInten2;
  struct OTF_GRID *g;

  if(!otf.gridArray || !otf.head)
  {
    log_msg("COMPUTEINTEN: OTF Array or header is empty");
    return(1);
  }

  if(bc < 0 || bc > otf.noint-1 || ec > otf.noint-1)
  {
    log_msg("COMPUTEINTEN: bc, %d; or ec, %d; greater then nochan, %d", bc, ec, otf.noint);
    return(1);
  }

  log_msg("computeInten(): Started");

			/* now compute area under curve for selected chans */
  for(g=gridHead;g;g=g->next)
  {
    if(g->set == 1)
    {
      tcount++;

      if(g->ddirty) /* Only work on valid celles */
      {
        k     = 0;
        accum = 0.0;
        j     = bc;

        do
        {
          {
            accum += g->chans[j];
          }
  
          k++;
        } while(j++<ec);

        g->kms = (float)(accum * fabs(otf.head->deltax));
        intInten = (float)(accum / (double)k) * 1.0;

		/* Apply Gamma correction if cell > 0.0 */
	if(intInten > 0.0)
        {
          intInten2 = (double)256 * pow((intInten / (double)256), 1.0);

	  if(isnan(intInten2))
          {
	    log_msg("Col %d, Row %d: intInten = %f using Gamma of %f", g->col, g->row, intInten, 1.0);
            g->intInten = intInten;
          }
          else
          {
            g->intInten = intInten2;
          }
	}
	else
	{
          g->intInten = intInten;
	}

        g->ddirty = 0;
        count++;
      }

    /* Assign & Apply scaling factor */

      g->plotVal1 = g->intInten;
      g->plotVal2 = g->kms;

      if(isnan(g->plotVal1))
      {
        g->plotVal1 = 0.0;
      }
    }
  }

  log_msg("computeInten(): Computed %d out of %d populated cells", count, tcount);

  return(0);
}


int sendOTF()
{
  struct OTF_GRID *g;

  if(!otf.gridArray)
  {
    log_msg("sendOTF: OTF Array is empty");

    return(1);
  }

  if(debug)
  {
    log_msg("sendOTF: Recomputing OTF Cell Min & Max Values");
  }

  for(g=gridHead;g;g=g->next)
  {
    if(g->set == 1) /* Only work on valid celles */
    {
      sprintf(send_struct.magic, "SEND_STRUCT");

      bcopy((char *)g->head,  (char *)&send_struct.head,  sizeof(struct HEADER));
      bcopy((char *)g->chans, (char *)&send_struct.chans, sizeof(float) * otf.noint);

	/* Fill in struct with specific values for each cell */

//      sock_write(dataserv, (char *)&send_struct, SEND_STRUCT_SHORT_SIZE + sizeof(float) * otf.noint);
    }
  }

  return(0);
}


int computeMinMax()
{
  struct OTF_GRID *g;

  if(!otf.gridArray)
  {
    log_msg("COMPUTEMINMAX: OTF Array is empty");

    return(1);
  }

  if(debug)
  {
    log_msg("COMPUTEMINMAX: Recomputing OTF Cell Min & Max Values");
  }

  otf.valueMin =  999999999.0;
  otf.valueMax = -999999999.0;

  otf.numCellsUsed = 0;
  zeroCellCount    = 0;
  artificialCells  = 0;

  for(g=gridHead;g;g=g->next)
  {
    if(g->set == 1) /* Only work on valid celles */
    {
      if(g->col == 0 && g->row == 0) 	/* How many cells think there are 0, 0 */
      {
        zeroCellCount++;
      }

      if(g->artificial)
      {
	artificialCells++;
      }

      if(g->plotVal1 > otf.valueMax)
      {
        otf.valueMax = g->plotVal1;

        otf.valueMaxX = g->col;
        otf.valueMaxY = g->row;
      }

      if(g->plotVal1 < otf.valueMin)
      {
        otf.valueMin = g->plotVal1;

        otf.valueMinX = g->col;
        otf.valueMinY = g->row;
      }

      otf.numCellsUsed++;
    }
  }

  return(0);
}



int scaleData(data)
float *data;
{
  int i;

  for(i=0;i<otf.numSpectra;i++)
  {
    data[i] *= 3600.0;
  }

  return(0);
}




int subtractPredictedRA(data)
float *data;
{
  float xmin, xscale;
  int i;

  xscale = otf.head->inttime * otf.head->deltaxr / 3600.0;
  xmin   = xscale / -2.0;
  xscale = xscale / (float)otf.numSpectra;

  if(data[0] > 0)
  {
    for(i=0;i<otf.numSpectra;i++)
    {
      data[i] += (xmin + (xscale * (float)i));
      data[i] *= 3600.0;
    }
  }
  else
  {
    for(i=0;i<otf.numSpectra;i++)
    {
      data[i] -= (xmin + (xscale * (float)i));
      data[i] *= 3600.0;
    }
  }

  return(0);
}



int initGrid()
{
  int ret;

  if((ret = otfGridInit(otf.cols, otf.rows, otf.noint)))
  {
    if(debug)
    {
      log_msg("initGrid(): otfGridInit() returned %d", ret);
    }

    return(1);
  }

  return(0);
}



int fillOtfGrid()
{
  int noint;

  if(!otf.head)
  {
    return(1);
  }

  otf.xsource    = otf.manMapRa;
  otf.ysource    = otf.manMapDec;
  otf.nyquistRa  = otf.manNyquistRa;
  otf.nyquistDec = otf.manNyquistDec;

  if(checkMapScans(version, otf.head->scan))
  {
    log_msg("Scan %.2f, version: %d, already in grid", otf.head->scan, version);

    return(1);
  }

  if(fabs(otf.restfreq - otf.head->restfreq) > 0.5 && otf.restfreq != -1.0)
  {
    log_msg("Rest Freq %.2f: NOT the same as grid %.2f", otf.head->restfreq, otf.restfreq);

    return(1);
  }

  if(fabs(otf.velocity - otf.head->velocity) > 0.5 && otf.velocity != -1.0)
  {
//    log_msg("Velocity %.2f: NOT the same as grid %.2f", otf.head->velocity, otf.velocity);

    if(!otf.allowVelShift)
    {
      return(1);
    }
  }

  if(fabs(otf.freqres - otf.head->freqres) > 0.001  && otf.freqres != -1.0 )
  {
    log_msg("Resolution %.2f: NOT the same as grid %.2f", otf.head->freqres, otf.freqres);

    return(1);
  }

  noint = (int)otf.head->noint;

  if(noint != otf.noint)		     /* this fixed a very hidious bug */
  {
    log_msg("Number of Channels %d, Not the same as grid %d", noint, otf.noint);

    return(1);
  }

  otf.row = (int)(otf.head->ycell0 - 1.0);

  accumDataIntoWork();

  addMapScans(version, otf.head->scan);

  return(0);
}


int accumDataIntoWork()
{
  int i, j, first = 1, rej;
  int workCent_x = -1, workCent_y = -1;
  int colIndx, rowIndx;
  int cell_x, cell_y;
  float *raData, *decData;
  float ra, dec;
  struct GRID_ACCUM *p;
  double *w;
  float  *o;
  double deltaRa = 0.0, deltaDec = 0.0, cosDec;

  zeroWorkArray();

  raData  = &otf.ondata[otf.noint * otf.numSpectra + otf.numSpectra*1];
  decData = &otf.ondata[otf.noint * otf.numSpectra + otf.numSpectra*2];

  rej = 0;

  if(!ephemObj)
  {
    deltaRa  = otf.xsource  - otf.head->xsource;
    deltaDec = otf.ysource - otf.head->ysource;
  }

  if(debug)
  {
    log_msg("ACCUMULATOR:   Loading Scan %7.2f", otf.head->scan);
  }

  cosDec = cos(otf.head->ysource * CTR);

  for(i=0;i<otf.numSpectra;i++)
  {
    ra  = (*raData++ / cosDec - deltaRa)  * 3600.0;
    dec = (*decData++         - deltaDec) * 3600.0;

    colIndx = (int)rint(((float)otf.cols-1.0) - ((ra   / otf.nyquistRa) +  (float)otf.cols        / 2.0));
    rowIndx = (int)rint(                        ((dec / otf.nyquistDec) + ((float)otf.rows + 1.0) / 2.0) -1.0);

//    colIndx = (otf.cols-1) - ((int)(ra  / otf.nyquistRa)  + otf.cols / 2);
//    rowIndx = (int)rint(((dec / otf.nyquistDec) + ((float)otf.rows+1.0) / 2.0) -1.0);

//    log_msg("ACCUMULATOR(): ra = %f, colIndx = %3d, Dec = %f, rowIndx = %3d", ra, colIndx, dec, rowIndx);

    if(colIndx<0 || colIndx>otf.cols-1 || rowIndx<0 || rowIndx>otf.rows-1)
    {
      rej++;

      continue;
    }

    if(first)
    {
      workCent_x = colIndx;
      workCent_y = rowIndx;
      first = 0;
    }

    cell_x = workCent_x - colIndx;
    cell_y = workCent_y - rowIndx;

    if(cell_x < WORK_ARRAY_X_M || cell_x > WORK_ARRAY_X_P || cell_y < WORK_ARRAY_Y_M || cell_y > WORK_ARRAY_Y_P)
    {
      normalizeAndCal();

      accumWorkIntoGrid();

      zeroWorkArray();

      workCent_x = colIndx;
      workCent_y = rowIndx;
      cell_x     = 0;
      cell_y     = 0;
    }
      
    p = &workArray[cell_x + WORK_ARRAY_X_CENT] [cell_y + WORK_ARRAY_Y_CENT];
    w = &p->chans[0];
    o = &otf.ondata[i*otf.noint];

    p->n++;
    p->col = colIndx;
    p->row = rowIndx;

    for(j=0;j<otf.noint;j++, w++, o++)
    {
      *w += (double)*o;
    }
  }

  if(rej)
  {
    if(rej == otf.numSpectra)
    {
      log_msg("ACCUMULATOR: Scan: %.2f: All %d Spectra Out of Bounds", otf.head->scan, rej);
      return(0);
    }

    log_msg("ACCUMULATOR: Scan: %.2f: %d Spectra Out of Bounds", otf.head->scan, rej);
  }

  normalizeAndCal();
  accumWorkIntoGrid();

  return(0);
}



int zeroWorkArray()
{
  int col, row;
  struct GRID_ACCUM *p;

  for(col=0;col<WORK_ARRAY_X;col++)
  {
    for(row=0;row<WORK_ARRAY_Y;row++)
    {
      p = &workArray[col][row];
      p->n = 0;
      p->col = 0;
      p->row = 0;
      p->wt  = 0.0;
    }
  }

  bzero((char *)workArrayChans, otf.sizeofWorkArray);

  return(0);
}



int normalizeAndCal()
{
  int col, row, k;
  struct GRID_ACCUM *p;
  double *w;
  float  *o, *g;

  for(col=0;col<WORK_ARRAY_X;col++)
  {
    for(row=0;row<WORK_ARRAY_Y;row++)
    {
      p = &workArray[col][row];

      if(!p->n)					/* no data in this cell */
      {
        continue;
      }

      w = &p->chans[0];
      o = &otf.offdata[0];
      g = &otf.gainsdata[0];

      for(k=0;k<otf.noint;k++, w++, o++, g++)
      {
        *w = (((*w / (double)p->n) - (double)*o) / (double)*o) * (double)*g;
      }

      p->wt = (double)p->n * 0.1 / ((double)otf.head->stsys * (double)otf.head->stsys);
    }
  }

  return(0);
}


int insertVersion(g, ver)
struct OTF_GRID *g;
int ver;
{
  int i;

  for(i=0;i<8;i++) 		/* Check if it's already in here */
  {
    if(g->version[i] == ver)
    {
      return(0); 		/* yes, return */
    }
  }

  for(i=0;i<8;i++) 		/* No, so add it */
  {
    if(g->version[i] == 0)	/* Find an empty slot */
    {
      g->version[i] = ver;

      return(0);
    }
  }

  return(1);
}



int shiftOtfChans(in, n, shift)
double *in;
int n, shift;
{
  int i, k, s;
  double avg;
  static double out[MAX_CHANS];

  if(!shift) /* Nothing to do */
  {
    return(0);
  }

  bzero((char *)&out[0], sizeof(out));

  avg = 0.0;
  s = abs(shift); 
  if(shift > 0)
  {
//    log_msg("shiftOtfChans(): Shifting %d channels to the Right", s);

    k = s;
    for(i=0;i<(n-s);i++,k++) /* shift and copy */
    {
      out[k] = in[i];
    }

    /* find a suitable average */
    for(i=0;i<5;i++)
    {
      avg += in[i];
    }
    avg /= 5.0;

//    log_msg("shiftOtfChans(): filling %d Beginning channels with %f", s, avg);

    for(i=0;i<s;i++) /* Fill in first shift channels with average */
    {
      out[i] = avg;
    }
  }
  else
  {
//    log_msg("shiftOtfChans(): Shifting %d channels to the Left", s);

    k = 0;
    for(i=s;i<n;i++,k++) /* shift and copy */
    {
      out[k] = in[i];
    }

    /* find a suitable average */
    for(i=(n-5);i<n;i++)
    {
      avg += in[i];
    }
    avg /= 5.0;

//    log_msg("shiftOtfChans(): filling %d Ending channels with %f", s, avg);

    for(i=(n-s);i<n;i++,k++)
    {
      out[k] = avg; /* k still holds the index */
    }
  }

  /* put it back */

  for(i=0;i<n;i++)
  {
    in[i] = out[i];
  }

  return(0);
}



/* Accumilate the x x Y work array into the greater OTF map.
   If there is a velocity offset, here is where we fix that.
 */
int accumWorkIntoGrid()
{
  int    n, row, col, k, shift = 0;
  struct GRID_ACCUM *p;
  struct OTF_GRID   *f;
  float  *w;
  double *o;

  if(!otf.gridArray)
  {
    log_msg("ACCUM: OTF Array is empty");

    return(1);
  }

//  log_msg("accumWorkIntoGrid(): Enter: %.2f", otf.head->scan);

  shift = rint((otf.head->velocity - (double)otf.velocity) / otf.head->deltax);

  if(shift)
  {
    log_msg("accumWorkIntoGrid(): Need to shift %d channels, for %f %f", shift, otf.velocity, otf.head->velocity);
  }

  n = otf.noint;

  for(col=0;col<WORK_ARRAY_Y;col++)
  {
    for(row=0;row<WORK_ARRAY_X;row++)
    {
      p = &workArray[col][row];

      if(!p->n)					/* no data in this cell */
      {
        continue;
      }

      f = fetchMapCell(p->row, p->col);

      if(!f)	     /* malloc space for grid array info on a as needed basis */
      {
//        if((f = (struct OTF_GRID *)malloc((sizeof(struct OTF_GRID)))) == NULL)
        if((f = (struct OTF_GRID *)calloc((sizeof(struct OTF_GRID)), 1)) == NULL)
        {
          log_msg("Err accumWorkIntoGrid(malloc(%d))", sizeof(struct OTF_GRID));
          return(1);
        }

//        bzero((char *)f, sizeof(struct OTF_GRID));

	f->head = do_addHeader(version, lastHead.scan, &lastHead);

        setMapCell(p->row, p->col, f);

        if(gridLast)
	{
	  gridLast = gridLast->next = f;
	}
        else
	{
	  gridHead = gridLast = f;
	}

      }

      if(!f->chans)	    /* malloc space for channels on a as needed basis */
      {
        if((f->chans=(float *)malloc((unsigned)(n*sizeof(float))))==NULL)
        {
          log_msg("Error in accumWorkIntoGrid(malloc(%d))", n*sizeof(float));
          return(1);
        }
        bzero((char *)f->chans, n * sizeof( float) );
      }

      w = f->chans;
      o = p->chans;

      if(shift) /* shift before averaging in */
      {
        shiftOtfChans(o, n, shift);
      }

      for(k=0;k<n;k++, o++, w++)
      {
        *w = ((double)*w * f->wt + *o * p->wt) / (f->wt + p->wt);
      }

      f->wt      += p->wt;
      f->intInten = 0.0;
      f->set      = 1;
      f->time    += (float)p->n * 0.1;
      f->scanct  += 1.0;

      f->tsys     = sqrt(1.0 / (f->wt / f->time));

      if(f->bscan == 0.0)
      {
        f->bscan  = otf.head->scan;
      }

      f->escan    = otf.head->scan;
      f->col      = p->col;
      f->row      = p->row;

      insertVersion(f, version);

      strncpy(f->source, otf.head->object, 16);
      f->source[15] = '\0';

      f->bdirty = 1;
      f->ddirty = 1;

      otf.bdirty = 1;
      otf.ddirty = 1;
    }
  }

  return(1);
}


 
int do_otfGridLoad(buf)
char *buf;
{
  int i, b, e;
  char *cp, tb[80];
  char tmp[256], tbuf[256]; 
  double stime, etime;
 
  log_msg("do_otfGridLoad(): Enter");

  strcpy(tmp, buf); 

	/* Grab beginning scan number */
  cp = strtok(tmp,"\n\t ");

  if(cp)
  {
    b = atoi(cp);
  }
  else
  {
    log_msg("do_otfGridLoad(), no begin scan specified");

    return(1);
  }
	/* Grab ending scan number */
  cp = strtok((char *)NULL,"\n\t ");

  if(cp)
  {
    e = atoi(cp);
  }
  else
  {
    log_msg("do_otfGridLoad(), no end scan specified");

    return(1);
  }

  stime = getTimeD(0);

  for(i=b;i<=e;i++)
  {
    sprintf(tb,"%d",i);

    fbScanDone  = 1;
    aosScanDone = 3;

    do_scanDone(tb);
  }

  etime = (getTimeD(0) - stime) * 3600.0;

  log_msg("do_otfGridLoad(): Done loading scans");

  computeMinMax();

  sendOTF();

  sprintf(tbuf, "Load Done %f %d", etime, otf.numCellsUsed);
  sock_send(last_msg(), tbuf);

  if(otf.numCellsUsed == 0)
  {
    log_msg("EMPTY Grid: %s", buf);
  }

  return(0);
}


int do_otfGridInit()
{
  otf.initialized = 0;
  otf.head        = 0;

  initGrid();
 
  return(0);
}



int getChanOTF()
{
  return(otf.chanSelOTF);
}



#define FIRST_FFB	1
#define SECOND_FFB	2
#define THIRD_FFB	3
#define FOURTH_FFB	4
#define FIFTH_FFB	5
#define SIXTH_FFB	6
#define SEVENTH_FFB	7
#define EIGHTH_FFB	8



struct OTF_GRID *fetchMapCell(row, col)
int row, col;
{
  if((row < otf.rows) && (col < otf.cols))
  {
    return((struct OTF_GRID *)otf.gridArray[(row*otf.cols) + col]);
  }

  return((struct OTF_GRID *)NULL);
}



int setMapCell(row, col, f)
int row, col;
struct OTF_GRID *f;
{
  if(row <otf.rows && col < otf.cols)
  {
    otf.gridArray[(row*otf.cols) + col] = f;

    return(0);
  }

  return(1);
}


int do_otfListCells(buf)
char *buf;
{
  int row, col;
  struct OTF_GRID *f;

  if(!otf.gridArray)
  {
    log_msg("do_otfListCells(): OTF Array is empty");

    return(1);
  }

  for(row=1;row<otf.rows-1;row++)
  {
    for(col=1;col<otf.cols-1;col++)
    {
      f = fetchMapCell(row, col);

      if(f) 			/* There's data here */
      {
	log_msg("Col: %4d, Row: %4d, ScanCt: %.1f, Tsys: %.1f, Wt: %.2e", col, row, f->scanct, f->tsys, f->wt);
      }
    }
  }
 
  return(0);
}



int do_otfStatus()
{
  int    tScans = 0;
  char   raStr[256], decStr[256];
  struct MAP_SCANS *m;
  extern int log_dup2stdout;

  if(otf.map_scans_head != NULL)
  {
    for(m=otf.map_scans_head;m->next; m=m->next)
    {
      if(m)
      {
        tScans++;
      }
    }

    computeInten(otf.int_chans[0], otf.int_chans[1]);

    computeMinMax();
  }
  else
  {
    log_msg("No Scans Loaded");
  }

  sexagesimal(otf.manMapRa/15.0, raStr, DD_MM_SS);
  sexagesimal(otf.manMapDec,    decStr, DD_MM_SS);

  log_dup2stdout = 0;

  log_msg("OTF: Map Info:");

  log_msg("OTF: Map Coordinates: RA: %s;  Dec: %s",               raStr, decStr);
  log_msg("OTF: Map Cell Size:   RA: %.2f Sec;  Dec: %.2f Sec",   otf.manNyquistRa, otf.manNyquistDec);
  log_msg("OTF: Map Cols: %4d; Rows: %4d",                        otf.cols, otf.rows);
  log_msg("OTF: Map Rest Freq: %.6f",                             otf.restfreq / 1000.0);
  log_msg("OTF: Backend Resolution: %.3f MHz; noint: %d",         fabs(otf.freqres), otf.noint);

  log_msg("OTF: Number of Occupied Cells: %d out of %d",          otf.numCellsUsed, otf.numCellsTotal);
  log_msg("OTF: Number of Bogus 0, 0 cells: %d",                  zeroCellCount);
  log_msg("OTF: Number of Artificial cells: %d",                  artificialCells);
  log_msg("OTF: Total Number of OTF Map Scans: %d",               tScans);
  log_msg("OTF: Map Max Value: %10.6f @ Col: %d, Row: %d",        otf.valueMax, otf.valueMaxX, otf.valueMaxY);
  log_msg("OTF: Map Min Value: %10.6f @ Col: %d, Row: %d",        otf.valueMin, otf.valueMinX, otf.valueMinY);

  log_msg("OTF: initialized        = %d", otf.initialized);
  log_msg("OTF: col                = %d", otf.col);
  log_msg("OTF: row                = %d", otf.row);
  log_msg("OTF: cols               = %d", otf.cols);
  log_msg("OTF: rows               = %d", otf.rows);
  log_msg("OTF: numCellsTotal      = %d", otf.numCellsTotal);
  log_msg("OTF: numCellsUsed       = %d", otf.numCellsUsed);
  log_msg("OTF: noint              = %d", otf.noint);
  log_msg("OTF: numSpectra         = %d", otf.numSpectra);
  log_msg("OTF: int_chans[2]       = %d - %d", otf.int_chans[0], otf.int_chans[1]);
  log_msg("OTF: lastAction         = %d", otf.lastAction);
  log_msg("OTF: allowVelShift      = %d", otf.allowVelShift);
  log_msg("OTF: sizeofWorkArray    = %d", otf.sizeofWorkArray);

  log_msg("OTF: IFNumber           = %f", otf.IFNumber);
  log_msg("OTF: nyquistRa          = %f", otf.nyquistRa);
  log_msg("OTF: nyquistDec         = %f", otf.nyquistDec);
  log_msg("OTF: manMapRa           = %f", otf.manMapRa);
  log_msg("OTF: manMapDec          = %f", otf.manMapDec);
  log_msg("OTF: manMapWidth        = %f", otf.manMapWidth);
  log_msg("OTF: manMapHeight       = %f", otf.manMapHeight);
  log_msg("OTF: manNyquistRa       = %f", otf.manNyquistRa);
  log_msg("OTF: manNyquistDec      = %f", otf.manNyquistDec);
  log_msg("OTF: freqres            = %f", otf.freqres);
  log_msg("OTF: restfreq           = %f", otf.restfreq);
  log_msg("OTF: velocity           = %f", otf.velocity);
  log_msg("OTF: xsource            = %f", otf.xsource);
  log_msg("OTF: ysource            = %f", otf.ysource);

  log_msg("OTF: ");

  log_dup2stdout = 0;

  return(0);
}
