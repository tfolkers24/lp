#include "ds_extern.h"
#include "ds_proto.h"

#include "../include/defines.h"
#include "send_struct.h"

#ifndef __DARWIN__
#include <sys/sysinfo.h>
#endif

int pthread_setname_np(pthread_t thread, const char *name);

#ifdef GALACTIC
#undef GALACTIC
#endif

#ifdef AZEL
#undef AZEL
#endif

#ifdef USERDEF
#undef USERDEF
#endif

#define  TRUE   1
#define  FALSE  0
#define DATASPREAD      0.000001

int (*sock_callback)() = NULL;
char inMsg[16384];

struct SOCK *master, *helper, *dataserv;

int   datalen		= 0,
      manualInitials    = 0,
      manualVersion     = 0,
      sockmode		= 0,
      initialsChanged   = 0,
      printDataLines    = 0,
      useFixData	= 0,
      version           = 1,
      numOfCPUs         = 0;

float datamax, 
      datamin, 
      scanNum		= -1.0;


char  filename[256];
char  gfilename[256];

char  initials[256], source[SRCLEN+1], oldBuf[16384], tempBuf[16384],
      prgName[80], errBuf[512], localHost[80], saveBuf[16384],  
      tailFileName[80], datadir[128], fixeddir[128], _3ddir[128];

int   found_scans[MAX_BACKENDS*MAX_PARTS];

int scansDone[100];
int scansDoneInx = 0;
int scanDone     = 0;

static int block_for_event();

struct HEADER  onhead01;
struct HEADER  offhead01;
struct HEADER  gainshead01;

float  *ondata01;
float  *offdata01;
float  *gainsdata01;

int fitFail = 0;
int doNotSaveConfig = 0;

extern float offScanNum;

int PID;
int istty;


int to_helper(buf)
char *buf;
{
  if(buf && sockmode)
  {
    log_msg("To HELPER: '%s'", buf);

    sock_send(helper, buf);
  }

  return(0);
}



int getNProcs()
{
  int n, na;

#ifndef __DARWIN__
  n  = get_nprocs_conf();
  na = get_nprocs();
#else
  n  = 4;
  na = 4;
#endif

  numOfCPUs = na;
  log_msg("%d processors configured, %d processors available.", n, na);

  return(0);
}


int main(argc,argv)
int argc;
char *argv[];
{
  char *cp, tfile[256], logname[256], sockname[256];
  int opt;

  setCactusEnvironment();

  sockmode       = 0;

  while ((opt = getopt(argc, argv, "s:")) != -1)
  {
    switch (opt)
    {
      case 's':
        sockmode = 1;
        strcpy(sockname, optarg);
        break;

      default: /* '?' */
	fprintf(stderr, "Usage: %s -s sock_bind_name\n", argv[0]);
	exit(1);
        break;
    }
  }

  if(!sockmode)
  {
    fprintf(stderr, "Usage: %s -s sock_bind_name\n", argv[0]);
    exit(1);
  }

  istty = isatty(0);

  atexit((void *)exitProc);

#ifndef __DARWIN__
  signal(SIGINT,  (__sighandler_t)signalHandler);
  signal(SIGTERM, (__sighandler_t)signalHandler);
#else
  signal(SIGINT,  signalHandler);
  signal(SIGTERM, signalHandler);
#endif

  strcpy(prgName, "GRIDDER");

  cp = getenv("LPSESSION");
  if(cp)
  {
    sprintf(tfile, "LOGDIR=/tmp/%s", cp);

    putenv(tfile);
  }
  else
  {
    fprintf(stderr, "LPSESSION not defined\n");
    exit(1);
  }

  strcpy(logname, sockname);
  strlwr(logname);

  smart_log_open(logname, 3);

  log_eventdEnable(LOG_TO_LOGGER);

  otfInit();

  setpgid( (pid_t)0, (pid_t)0);        /* divorce myself from parents */

  PID = getpid();

  cp = getenv("TOPDIR");
  if(cp)
    sprintf(datadir,"%s",cp);
  else
    sprintf(datadir,"/home/data");

  cp = getenv("FIXDAT");
  if(cp)
    sprintf(fixeddir,"%s",cp);
  else
    sprintf(fixeddir,"/home/data4/fixed");

  log_msg("Binding to mailbox: %s on host %s", sockname, getenv("HOST"));

  sock_bind(sockname);
  master   = sock_connect("LINUXPOPS_MASTER");
  helper   = sock_connect("LINUXPOPS_HELPER");
  dataserv = sock_connect("LINUXPOPS_DATASERV");

  set_sock_callback( (int (*)(void))readMail );

  sock_bufct(sizeof(struct DS_IMAGE_INFO)+256);

  main_loop();

  return(0);
}


void signalHandler()
{
  exit(1);
}


int exitProc(status)
int status;
{
  log_msg("Exiting with status %d", status);

  return(status);
}



int argmatch(s1, s2, atleast)
char *s1;
char *s2;
int atleast;
{
  int l1 = strlen(s1);
  int l2 = strlen(s2);

  if(l1 < atleast)
    return 0;

  if(l1 > l2)
    return 0;

  return(strncmp(s1, s2, l1) == 0);
}


int usage(progname)
char *progname;
{
  fprintf(stderr, "\nUsage of '%s' command line arguments: \n", progname);
  fprintf(stderr, "-anon                      Set dataserv in command-line mode\n");
  fprintf(stderr, "-obs initials              Set initials to display;  defaults to current observer\n");
  fprintf(stderr, "-ver n dir                 Set datafile ver to n, located on disk dir;\n");
  fprintf(stderr, "                           defaults to current observers version\n");
  fprintf(stderr, "-usage                     This message\n");
  exit(0);
}


int verifyScanDone(scn)
int scn;
{
  int i;

  for(i=0;i<100;i++)
  {
    if(scn == scansDone[i])
    {
      scanDone = 1;
      return(0);
    }
  }
  insertScan(scn);
  scanDone = 0;

  return(1);
}



int insertScan(scn)
int scn;
{
  scansDoneInx++;
  if(scansDoneInx > 99)
    scansDoneInx = 0;

  scansDone[scansDoneInx] = scn;

  return(0);
}



int to_monitor(buf)
char *buf;
{
  log_msg("To Monitor: %s", buf);

  return(0);
}


int to_rambo(buf)
char *buf;
{
  log_msg("To Rambo: %s", buf);

  return(0);
}

int to_exec(buf)
char *buf;
{
  log_msg("To Exec: %s", buf);

  return(0);
}

int do_toggleFixData(buf)
char *buf;
{
  if(buf)
    useFixData = atoi(buf);
  else
    useFixData = 0;

  log_msg("Setting use of fixed data to %d", useFixData);

  return(0);
}


int showDisplay()
{
  int i, modeid, be, foundOne=0;
  extern float ifNumber;
  char *cp;
  extern float ifnums[];

  bzero((char*)&found_scans, sizeof(found_scans));

  cp = filename;
  for(i=0;i<4;i++)
  {
    ifNumber = ifnums[i];
    if(!peekScan(scanNum + ifNumber, cp, &be, &modeid, &onhead01))
    {
      found_scans[DBEA+i] = 1;
      foundOne++;
    }
  }

  cp = filename;
  for(i=0;i<8;i++)
  {
    ifNumber = ifnums[i];
    if(!peekScan(scanNum + ifNumber, cp, &be, &modeid, &onhead01))
    {
      found_scans[FFBA+i] = 1;
      foundOne++;
    }
  }

  cp = filename;
  for(i=0;i<4;i++)
  {
    ifNumber = ifnums[i];
    if(!peekScan(scanNum + ifNumber, cp, &be, &modeid, &onhead01))
    {
      found_scans[AOSA+i] = 1;
      foundOne++;
    }
  }

  for(i=4;i<5;i++)
  {
    ifNumber = ifnums[i];
    if(!peekScan(scanNum + ifNumber, cp, &be, &modeid, &onhead01))
    {
      found_scans[AOSC] = 1;
      foundOne++;
    }
  }

  cp = filename;
  for(i=0;i<4;i++)
  {
    ifNumber = ifnums[i];
    if(!peekScan(scanNum + ifNumber, cp, &be, &modeid, &onhead01))
    {
      found_scans[CTSA+i] = 1;
      foundOne++;
    }
  }

  if(!foundOne)
  {
    log_msg("No scan found for scan %.0f", scanNum);

    return(0);
  }

  for(i=0;i<20;i++)               /* set the ifNumber to the first scan found */
  {
    if(found_scans[i])
    {
      ifNumber = ifnums[i];
      break;
    }
  }

//  log_msg("Found %d scan(s), ifnum %.2f, BE = %d, modeid = %d", 
//                                       foundOne, ifNumber, be, modeid);

  switch(be)
  {
    case SPECTRAL_BE:
      switch(modeid) 
      {
	case OTF:
	  do_lineotf();
	  break;
	default:
	  break;
      }
      break;

    default:
      return(0);
      break;
  }

  return(0);
}



#ifdef TOMMIE
int matherr(x)
register struct exception *x;
{
  log_msg("Math Exception");
  switch(x->type)
  {
    case DOMAIN:		/* change sqrt to return sqrt(-arg1), not NaN */
        if(!strcmp(x->name, "sqrt"))
        {
          log_msg("Domain exception in %s", x->name);
          log_msg("Filename scanNum %d", (int)scanNum);
          log_msg("Command %s", oldBuf);
          x->retval = sqrt(-x->arg1);
          return (0);			       /* print message and set errno */
        }						      /* fall through */
    case SING:/* all other domain or sing exceptions, print message and abort */
       	log_msg("domain exception in %s", x->name);
        log_msg("Filename scanNum %d", (int)scanNum);
        log_msg("Command %s", oldBuf);
        abort( );
        break;
    case OVERFLOW:
        log_msg("Overflow exception in %s(%f)", x->name, x->arg1);
        log_msg("Filename scanNum %d", (int)scanNum);
        log_msg("Command %s", oldBuf);
        break;
    case UNDERFLOW:
        log_msg("Underflow exception in %s(%f)", x->name, x->arg1);
        log_msg("Filename scanNum %d", (int)scanNum);
        log_msg("Command %s", oldBuf);
        break;
  }
  return(0);
}
#endif



int main_loop()
{
  int count, n=0/*, xfd*/;
  int xfds[256];

  while (1)
  {
    count=0;

    if(!count) 
    {
      n=0;

/*xfd = */ block_for_event(xfds, n);
    }
  }

  return(0);
}
 

static int block_for_event(xfd, n)
int *xfd;
int n;
{
  int len, i;
  static int sel = 1;

  if(istty && !sockmode && sel == 0)
  {
    fprintf(stderr, "dataserv> ");
    fflush(stderr);
  }

  if(sockmode == 0)
  {
    sel = sock_sel(inMsg, &len, xfd, n, 0, 1);
  }
  else
  if(sockmode == 1)
  {
    sel = sock_sel(inMsg, &len, xfd, n, 10, 0);
  }

  if( sel == -1 )
  {                                                /* timeout */
    return(sel);
  } 

  for(i=0;i<n;i++)
  {
    if(sel == xfd[i])
    {
      return(sel);
    }
  }
 
  if(sock_callback)
  {
    (*sock_callback)(inMsg, len);
  }

  return(sel);
}


int set_sock_callback( call )
int (*call)();
{
  sock_callback = call;

  return(0);
}


int do_printData(buf)
char *buf;
{
  if(buf)
    printDataLines = atoi(buf);

  if(printDataLines > 100)
    printDataLines = 0;

  return(0);
}

int determine_min_max(ydata, dl, scan)
float *ydata;
int dl;
float scan;
{
  int i;

  if(ydata == NULL)
     return(1);
                                                   /* redetermine min and max */
  datamax = -9999999999.9;
  datamin = 9999999999.0;

  for(i=0;i<dl;i++)
  {
    if(ydata[i] > datamax)
      datamax = ydata[i];
    if(ydata[i] < datamin)
      datamin = ydata[i];
  }

  if(fabs(datamax - datamin) < DATASPREAD)
  {
    log_msg("determine_min_max(): Scan #%7.2f is all %.4f", scan, datamin);

    datamin -= 1.0;
    datamax += 1.0;

    return(1);
  }
  return(0);
}



int do_filename(buf)
char *buf;
{
  int n, v;

  if(buf)
  {
    strcmprs(buf);

    strcpy(filename, buf);

    log_msg("do_filename() filename set to %s", filename);
  }
  else
  {
    log_msg("do_filename(): Missing arg for filename");

    return(1);
  }

  n = strlen(filename);

  v = atoi(filename+(n-3));

  if(v > 0 && v < 999)
  {
    log_msg("Setting internal version to: %d", v);
    version = v;
  }
  else
  {
    log_msg("Problem parsing version number from %s", filename);
  }

  return(0);
}


int do_gfilename(buf)
char *buf;
{
  if(buf)
  {
    strcmprs(buf);

    strcpy(gfilename, buf);

    log_msg("do_gfilename() gfilename set to %s", gfilename);
  }
  else
  {
    log_msg("do_gfilename(): Missing arg for gfilename");

    return(1);
  }

  return(0);
}
