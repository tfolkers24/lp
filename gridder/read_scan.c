#include "ds_extern.h"
#include "modes.h"

#define SIZEOFDIR (sizeof(struct DIRECTORY))

#define MAX_SCANS 16384

static int pdopen();

extern int               datalen;
extern char		 errBuf[];
int                      ignoreThisChan;


int checkBoot(b, fn)
struct BOOTSTRAP *b;
char *fn;
{
  int bytesinindex, bytesused;

  if( SIZEOFDIR != b->bytperent ) {
    log_msg("directory sizes dont match");
    return(1);
  }

  bytesinindex = b->num_index_rec * b->bytperrec;
  bytesused = b->num_entries_used * b->bytperent;
 
  if( bytesinindex < bytesused + SIZEOFDIR ) {
    log_msg("Dir struct of %s, full.", fn );
 /*
    return(1);
 */
  }

  if( b->typesdd || !b->version  ) {
    log_msg("SDD version or type of %s is invalid.", fn );
    return(1);
  }

  if( SIZEOFDIR != b->bytperent ) {
    log_msg( "File %s has faulty direcory size, can't verify", fn );
    return(1);
  }

  if( b->num_entries_used <= 0 ) {
/*
    log_msg( "File %s is empty", fn );
 */
    return(1);
  }
  return(0);
}


int fixVel(head)
struct HEADER *head;
{
  if(head->refpt_vel == 0.0)
    head->refpt_vel = head->velocity;

  return(0);
}


int read_scan(scan, filename, data, head)
float scan;
char *filename;
float **data;
struct HEADER *head;
{
  int fd, dirl, num, max;
  struct BOOTSTRAP boot;
  struct DIRECTORY dir;
  int i, l;
  extern int checkIgnoreScans(int v, double s);

  if(checkIgnoreScans(version, scan))
  {
    log_msg("READ_SCAN: Scan %.2f, in version %d Ignored", scan, version);

    return(-2);
  }
  
  if(*data) {
    free((char *)*data);
    *data = NULL;
  }

  if( (fd = pdopen(filename))<0)
    return(-1);

  if( read( fd, (char *)&boot, sizeof(struct BOOTSTRAP))<0 ) {
    log_perror("read boot");
    close(fd);
    return(-1);
  }
  
  if( checkBoot(&boot, filename)) {
    close(fd);
    return(-1);
  }

  if( scan < 0.0) {
    dirl = (boot.num_entries_used-1) * boot.bytperent + boot.bytperrec;
    lseek( fd, (off_t)dirl, SEEK_SET );
    if(read( fd, (char *)&dir, SIZEOFDIR )!= SIZEOFDIR ){
      log_perror("readdir");
      close(fd);
      return(-1);
    }
    close(fd);
    return((int)dir.scan);
  }

  num = boot.num_entries_used;

  while(num >= 0) 
  {
    dirl = num * boot.bytperent + boot.bytperrec;
    lseek( fd, (off_t)dirl, SEEK_SET );

    if(read( fd, (char *)&dir, SIZEOFDIR ) != SIZEOFDIR )
    {
      log_perror("readdir");
      close(fd);

      return(-1);
    }
    if( scan == dir.scan )
    {
      break;
    }

    num--;
  }

  if( num < 0)
  {
//    log_msg("READ_SCAN: Scan %.2f NOT Found in %s", scan, filename);
    close(fd);

    return(-1);
  }
  
  max = boot.bytperrec * (dir.start_rec-1);
  lseek( fd, (off_t)max, SEEK_SET ); 

  if(read(fd, (char *)head, sizeof(struct HEADER) )<0) 
  {
    perror("read header");
    close(fd);

    return(-1);
  }

  fixVel(head);

  datalen = (int)head->datalen;
  *data = (float *)malloc( (unsigned int)datalen );
  bzero((char *)*data, datalen );

  if( read( fd, (char *)*data, datalen) <0 ) {
    perror("read data");
    log_msg("Error reading scan %7.2f data", scan);
    close(fd);
    return(-1);
  }

  if(head->tcal < 0)
    ignoreThisChan = 1;
  else
    ignoreThisChan = 0;

				/* now check for abnormal float in data array */
  l = datalen / sizeof(float);

  for(i=0;i<l;i++)
  {
    if(isnan( (double)(*data)[i]))
    {
      (*data)[i] = 0.0;
    }
  }
 
  close(fd);

  return(0);
}


static int pdopen(name)
char *name;
{
  int fd;

  if( (fd = open(name, O_RDONLY, 0666 ))<0) 
  {
    log_msg("Unable to open: %s", name );

    return(-1);
  }

  return(fd);
}


/*      given an id, return the field associated with it */
int modefield(id, field)
short int id;
char *field;
{
  if(id >= 0 || id < NUMMODES)
  {
    strcpy(field, modes[id].field);
    return(0);
  }
  else
  {
    strcpy(field, modes[0].field);
    return(1);
  }
}



int peekScan(scan, filename, be, modeid, head )
float scan;
char *filename;
int *be, *modeid;
struct HEADER *head;
{
  int fd, dirl, bytesinindex, bytesused, num, max;
  struct BOOTSTRAP boot;
  struct DIRECTORY dir;
  char tmode[20];

  if( (fd = pdopen(filename))<0)
    return(-1);

//  log_msg("PEEK: Looking for scan %.2f in %s", scan, filename);
 
  if( read( fd, (char *)&boot, sizeof(struct BOOTSTRAP))<0 ) {
    log_perror("read boot");
    close(fd);
    return(-1);
  }

  /* check bootstrap block */
 
  if( SIZEOFDIR != boot.bytperent ) {
    log_msg("directory sizes dont match");
    close(fd);
    return(-1);
  }
 
  bytesinindex = boot.num_index_rec * boot.bytperrec;
  bytesused = boot.num_entries_used * boot.bytperent;
 
  if( bytesinindex < bytesused + SIZEOFDIR ) {
    log_msg("Dir struct of %s, full.", filename );
  }
 
  if( boot.typesdd || !boot.version  ) {
    log_msg("SDD ver or type of %s, invalid.", filename );
    close(fd);
    return(-1);
  }
 
  if( SIZEOFDIR != boot.bytperent ) {
    log_msg( "File %s has faulty dir size", filename );
    close(fd);
    return(-1);
  }
 
  if( boot.num_entries_used <= 0 ) {
    log_msg( "File %s is empty", filename );
    close(fd);

    return(-1);
  }
 
  if( scan < 0.0)
  {
    dirl = (boot.num_entries_used-1) * boot.bytperent + boot.bytperrec;
    lseek( fd, (off_t)dirl, SEEK_SET );
    if(read( fd, (char *)&dir, SIZEOFDIR ) != SIZEOFDIR ){
      log_perror("readdir");
      close(fd);
      return(-1);
    }
  }
  else
  {
    num = boot.num_entries_used;
    while(num >= 0) {
      dirl = num * boot.bytperent + boot.bytperrec;
      lseek( fd, (off_t)dirl, SEEK_SET );
      if(read( fd, (char *)&dir, SIZEOFDIR ) != SIZEOFDIR ){
        log_perror("readdir");
        close(fd);
        return(-1);
      }
      if( scan == dir.scan )
        break;
      num--;
    }

    if( num < 0)
    {
      close(fd);
      return(-1);
    }
  }

  max = boot.bytperrec * (dir.start_rec-1);
  lseek( fd, (off_t)max, SEEK_SET );

  if(read(fd, (char *)head, sizeof(struct HEADER) )<0) 
  {
    perror("read header");
    close(fd);

    return(-1);
  }

  fixVel(head);

  strncpy( tmode, head->obsmode, 4);
  tmode[4] = '\0';

  if(strstr(tmode, "CONT"))
    *be = DIGITAL_BE;
  else
  if(strstr(tmode, "LINE"))
    *be = SPECTRAL_BE;
  else
    *be = MYSTERY_BE;

  dir.obsmode &= 0x7f;
  *modeid = dir.obsmode;
 
  close(fd);

//  log_msg("PEEK: Found scan %.2f in %s", scan, filename);

  return(0);
}
