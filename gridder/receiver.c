#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <math.h>

#include "caclib_proto.h"

int main()
{
  int sel, len, count = 0;;
  char message[4096*4];

  sock_bind("LINUXPOPS_DATASERV");

  while(1)
  {
    sel = sock_sel(message, &len, NULL, 0, 10, 0);

    if(sel == -1)
    {
      log_msg("sock_sel(): timeout");

      log_msg("Count: %d", count);

      count = 0;
    }
    else
    if(sel > 0)
    {
      log_msg("Got: %s, len: %d", message, len);

      count++;
    }
  }

  return(0);
}
