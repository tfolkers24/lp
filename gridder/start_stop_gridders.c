#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <dirent.h>
#include <math.h>
#ifndef __DARWIN__
#include <malloc.h>
#endif
#include <signal.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/time.h>

#include "mbdef.h"
#include "sex.h"
#include "loglib.h"
#include "cactus.h"
#include "sdd.h"
#include "header.h"
#include "observing_par.h"
#include "defines.h"

#include "caclib_proto.h"

struct SOCK *helper;
struct DEST *dest;

#ifdef TOMMIE
struct DEST {
  int dport;                            /* Port number */
  char host[MAXNAME];                   /* Computer that owns this mailbox */
  char dname[MAXNAME];                  /* Mailbox name */
  short raw;                            /* Non-zero if this is a `raw' socket */
};
#endif

#define MAX_HOSTS 80

char hosts[MAX_HOSTS][MAX_HOSTS];


int insertHost(h)
char *h;
{
  int i;
  static int hostindx = 0;

  for(i=0;i<MAX_HOSTS;i++)
  {
    if(!strcmp(hosts[i], h))
    {
      return(0);
    }
  }

  strcpy(hosts[hostindx], h);

  hostindx++;

  return(1);
} 

int start_gridders(hn, mb, ss, lh)
char *hn, *mb, *ss, *lh;
{
  int i, ret;
  char sysBuf[256];

  for(i=0;i<MAXDEST;i++)
  {
    dest = get_dest(i);

    if(dest)
    {
      if(strcmp(dest->dname, "GRIDDER_LOADER") && strstr(dest->dname, "GRIDDER_"))
      {
        ret = insertHost(dest->host);

	if(ret)
	{
	  if(strcmp(hn, dest->host))
	  {
            sprintf(sysBuf, "ssh %s mkdir -p /tmp/%s; scp -q %s %s:%s", dest->host, ss, mb, dest->host, mb);
            log_msg("Copying mailboxdefs: %s", sysBuf);
            system(sysBuf);
	  }
	}

        sprintf(sysBuf, "ssh %s \"setenv MAILBOXDEFS %s; setenv LPSESSION %s; %s/bin/gridder -s %s &\" &", dest->host, mb, ss, lh, dest->dname);
        log_msg("Launching: %s", sysBuf);

        system(sysBuf);
      }

      usleep(50000);
    }
    else
    {
      break;
    }
  }

  sleep(1);

  return(0);
}



int stop_gridders(mb, lh)
char *mb, *lh;
{
  int i, ret;
  char sysBuf[256];

  for(i=0;i<MAXDEST;i++)
  {
    dest = get_dest(i);

    if(dest)
    {
      if(strcmp(dest->dname, "GRIDDER_LOADER") && strstr(dest->dname, "GRIDDER_"))
      {
	ret = insertHost(dest->host);

        if(ret)
        {
          sprintf(sysBuf, "ssh %s \"killall -v -q gridder\"", dest->host);
          log_msg("Launching: %s", sysBuf);

          system(sysBuf);
        }
      }

      usleep(50000);
    }
    else
    {
      break;
    }
  }

  return(0);
}

int main(argc, argv)
int argc;
char *argv[];
{
  char *hn, *mb, *ss, *lh;

  if(argc < 2)
  {
    fprintf(stderr, "Usage: %s [start | stop]\n", argv[0]);
    exit(1);
  }

  setCactusEnvironment();

  smart_log_open("gridder_control", 3);

  hn = getenv("HOST");
  if(!hn)
  {
    log_msg("HOST not defined");
    exit(1);
  }
  log_msg("HOST: %s", hn);

  mb = getenv("MAILBOXDEFS");
  if(!mb)
  {
    log_msg("MAILBOXDEFS not defined");
    exit(1);
  }
  log_msg("MAILBOXDEFS: %s", mb);

  ss = getenv("LPSESSION");
  if(!ss)
  {
    log_msg("LPSESSION not defined");
    exit(1);
  }
  log_msg("LPSESSION: %s", ss);

  lh = getenv("LINUXPOPS_HOME");
  if(!lh)
  {
    log_msg("LINUXPOPS_HOME not defined");
    exit(1);
  }
  log_msg("LINUXPOPS_HOME: %s", lh);

  sock_bind("ANONYMOUS");

  helper = sock_connect("LINUXPOPS_HELPER");

  if(!strcmp(argv[1], "start"))
  {
    start_gridders(hn, mb, ss, lh);
  }
  else
  if(!strcmp(argv[1], "stop"))
  {
    stop_gridders(mb, lh);
  }
  else
  {
    fprintf(stderr, "Usage: %s [start | stop]\n", argv[0]);
    exit(1);
  }

  return(0);
}

