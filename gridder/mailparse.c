#include "ds_extern.h"
#include <libgen.h>

#include "ds_proto.h"

#define  TRUE            1
#define  FALSE           0

#define INT		 1
#define FLOAT		 2
#define DOUBLE		 3
#define CHAR		 4

extern float otfIFNumber, ifNumber, scanNum;

extern char prgName[], saveBuf[], oldBuf[], initials[], errBuf[];

extern int restoreMsg, singleChan, singleOtfBin, doTPprocess, doFsProcess, sockmode;

extern int fitChan[2], off_center_line, auto_locate, domarker;
extern int fixedZero, ignore_gains, useFixData, ephemObj;

extern double which_two[2];
extern int    which_two_i[2];

int debug = 0;
int stopFlag = 0;

int fbScanDone     = 0,
    aosScanDone    = 0,
    dbeScanDone    = 0,
    ignoreBadChans = 0;

struct SOCK *mailBoxes[MAX_MAIL_BOXES];

static char *spaces = "                                                                     ";


struct MAIL_PARSE {
	char *msg;
	int   log;
	int   save_config;
	int   count;
	int   type;
	char  *format;
	void  *ptr;
	int   len;
	int   (*fctn)();
	char *help;
};


struct MAIL_PARSE parse_table[] = {
 /* Commands that change internal variables and are saved to dataserv.conf */
/*     msg,              log, save, cnt,   type,   fmt,             *ptr,           len,               fctn(),         *help */

  { "allowvelshift",       1,    1,   1,    INT,   "%d ", (void *)&otf.allowVelShift, 13, do_otfGridAllowVelShift, "0 | 1, toggle allowVelShift"},

  { "clearignore",         1,    0,   0,      0,    NULL,                       NULL, 11,         do_clearIgnore, "Clear Ignore scan array."},
  { "configfromscan",      1,    0,   0,      0,    NULL,                       NULL, 14,         configFromScan, "Configure OTF from filename & scanno"},

  { "debug",               1,    1,   1,    INT,   "%d ",             (void *)&debug,  5,               do_debug, "Turn on Debugging Output"},

  { "ephemobj",            1,    1,   1,    INT,   "%d ",          (void *)&ephemObj,  8,            do_ephemObj, "Lock Grid For Ephem's"},

  { "filename",            1,    0,   0,      0,    NULL,                       NULL,  8,            do_filename, "Set the Data  File Name"},
  { "gfilename",           1,    0,   0,      0,    NULL,                       NULL,  9,           do_gfilename, "Set the Gains File Name"},

  { "help",                0,    0,   0,      0,    NULL,                       NULL,  4,                do_help, "Displays this list"},

  { "ignore",              1,    1,   0,      0,    NULL,                       NULL,  6,          do_ignoreScan, "v n, ignore scan n in version v."},

  { "load", 		   0,    0,   0,      0,    NULL,                       NULL,  4,                do_load, "Load a Script of Commands"},

  { "otfchans",            1,    1,   2,    INT,   "%d ",     (void *)&otf.int_chans,  8,            do_otfChans, "bc ec, set int. range bc to ec"},
  { "otfgridcellsizedec",  1,    1,   1, DOUBLE,   "%f ", (void *)&otf.manNyquistDec, 18,  do_otfGridCellSizeDec, "x, Set Grid Dec size to x"},
  { "otfgridcellsizera",   1,    1,   1, DOUBLE,   "%f ",  (void *)&otf.manNyquistRa, 17,   do_otfGridCellSizeRa, "x, Set Grid Ra size to x"},
  { "otfgridcols",         1,    1,   1,    INT,   "%d ",          (void *)&otf.cols, 11,         do_otfGridCols, "c, Set Map X Size in Cols"},
  { "otfgriddec",          1,    1,   1, DOUBLE,   "%f ",     (void *)&otf.manMapDec, 10,          do_otfGridDec, "x, Set Grid Center Dec to x"},
  { "otfgridfreqres",      1,    1,   1, DOUBLE,   "%f ",       (void *)&otf.freqres, 14,      do_otfGridFreqres, "freq, Set Grid Freq Resolution to 'freq'"},
  { "otfgridinit",         1,    0,   0,      0,    NULL,                       NULL, 11,         do_otfGridInit, "Reinitializes OTF grid array"},
  { "otfgridload",         1,    0,   0,      0,    NULL,                       NULL, 11,         do_otfGridLoad, "bs es [delay], loads scans bs to es"},
  { "otfgridnoint",        1,    1,   1,    INT,   "%d ",         (void *)&otf.noint, 12,        do_otfGridNoint, "noint, Set Grid number of Channels to 'noint'"},
  { "otfgridra",           1,    1,   1, DOUBLE,   "%f ",      (void *)&otf.manMapRa,  9,           do_otfGridRa, "x, Set Grid Center Ra to x"},
  { "otfgridrestfreq",     1,    1,   1, DOUBLE,   "%f ",      (void *)&otf.restfreq, 15,     do_otfGridRestfreq, "freq, Set Grid Rest Freq to freq"},
  { "otfgridrows",         1,    1,   1,    INT,   "%d ",          (void *)&otf.rows, 11,         do_otfGridRows, "c, Set Map Y Size in Rows"},
  { "otfgridsizedec",      1,    1,   1, DOUBLE,   "%f ",  (void *)&otf.manMapHeight, 14,      do_otfGridSizeDec, "x, Set Grid Dec H to x"},
  { "otfgridsizera",       1,    1,   1, DOUBLE,   "%f ",   (void *)&otf.manMapWidth, 13,       do_otfGridSizeRa, "x, Set Grid Ra  W to x"},
  { "otfgridvelocity",     1,    1,   1, DOUBLE,   "%f ",      (void *)&otf.velocity, 15,     do_otfGridVelocity, "vel, Set Grid Velocity to vel"},
  { "otfifchoice",         1,    1,   1,    INT, "0x%x ",    (void *)&otf.chanSelOTF, 11,         do_otfIFchoice, "n, Load IF Number n"},
  { "otflistcells",        1,    0,   0,      0,    NULL,                       NULL, 12,        do_otfListCells, "List info on each map cell"},
  { "otfstatus",           1,    0,   0,      0,    NULL,                       NULL,  9,           do_otfStatus, "Print a brief summary of the current OTF map"},

  { "printignorescans",    1,    0,   0,      0,    NULL,                       NULL, 16,    do_printIgnoreScans, "Print out scans ignored"},
  { "printmapscans",       1,    0,   0,      0,    NULL,                       NULL, 13,       do_printMapScans, "Print out scans in OTF grid"},

  { "q",                   0,    0,   0,      0,    NULL,                       NULL,  1,                do_quit, "Exit program"},
  { "quit",                0,    0,   0,      0,    NULL,                       NULL,  4,                do_quit, "Exit program"},

  { "scan_done",           1,    0,   0,      0,    NULL,                       NULL,  9,            do_scanDone, "n, display scan #n"},
  { "status",              1,    0,   0,      0,    NULL,                       NULL,  6,              do_status, "Shows brief status msg"},
  { "stop",                1,    0,   0,      0,    NULL,                       NULL,  4,                do_stop, "Stop any loops,load,velr"},

  {  NULL,                 0,    0,   0,      0,    NULL,                       NULL,  0,              (void *)0,  NULL }
};


char *vxstrtok();


int do_load(buf)
char *buf;
{
  char inBuf[256];
  FILE *fp;

  if(buf)
  {
    stopFlag = 0;

    strcmprs(buf);

    fp = fopen(buf, "r");

    if(fp == NULL)
    {
      log_msg("DATASERV: Unable to open %s", buf);
      return(1);
    }

    while(fgets(inBuf, 256, fp) != NULL)
    {
      if(!strncmp(inBuf, "#", 1))
      {
        continue;
      }

      parseMail(inBuf);

      if(stopFlag)
      {
        fclose(fp);
        stopFlag = 0;

        return(0);
      }
    }

    fclose(fp);
  }

  return(0);
}


int readMail(buf, len)
char *buf;
int len;
{
  char *cp;
  static char cmdbuf[4096];

  if(buf)
  {
    cp = vxstrtok(buf, NULL, "\n");
    while(cp)
    {
      strcpy(cmdbuf, cp);
      parseMail(cmdbuf);
      cp = vxstrtok(NULL, cp, "\n");
    }
  }

  return(0);
}


int parseMail(buf)
char *buf;
{
  char *s;
  struct MAIL_PARSE *p;

  deNewLineIt(buf);

  ifNumber = 0.01;
  strlwr(buf);

  if(strstr(buf, "fb_scan"))
  {
    fbScanDone = 1;
  }
  else
  if(strstr(buf, "aos_scan"))
  {
    aosScanDone++;
  }
  else
  if(strstr(buf, "dbe_scan"))
  {
    dbeScanDone = 1;
  }
  else
  if(!strncmp(buf, "scan_done", 9))
  {
    fbScanDone = dbeScanDone = 1;
    aosScanDone = 3;
  }

  for(p=parse_table;p->msg;p++)
  {
    if(!strncmp(p->msg, buf, p->len))
    {
      if(p->log && strlen(buf) < 2048)
      {
        s = (char *)sock_name(last_msg());
        if(s)
          log_msg("%s: (%s) %s", prgName, sock_name(last_msg()), buf);
        else
          log_msg("%s: %s", prgName,  buf);
      }

      p->fctn(buf+p->len);

      return(0);
    }
  }

  log_msg("Command %s NOT found", buf);

  return(0);
}



int do_scanDone(buf)
char *buf;
{
  restoreMsg = 0;
  strcpy(saveBuf, oldBuf);

  if(buf)
  {
    scanNum = atof(buf);
  }
  else
  {
    return(1);
  }
   
  if( fbScanDone || aosScanDone >= 3 || dbeScanDone )
  {
    dbeScanDone = 0;

    verifyScanDone((int)scanNum);

    showDisplay();

    if(restoreMsg)
      strcpy(oldBuf, saveBuf);
    else
      sprintf(oldBuf, "scan_done %d", (int)scanNum);

    return(0);
  }

  return(1);
}



int do_calDone()
{
  fbScanDone = aosScanDone = dbeScanDone = 0;
  
  return(0);
}



int do_status()
{
  log_msg("Dataserv O.K.");
  
  return(0);
}



int do_quit()
{
  signalHandler();
  
  return(0);
}



int do_stop()
{
  stopFlag = 1;
  
  return(0);
}


int do_help(buf)
char *buf;
{
  struct MAIL_PARSE *p;
  char tbuf[128];
  char *cp;

  cp = strtok(buf, " \n\t");

  sprintf(tbuf, "Command              Arguments\n");
  printf("\n%s\n", tbuf);

  if(sockmode)
  {
    log_msg(tbuf);
  }

  for(p=parse_table;p->msg;p++)
  {
    if(cp)
    {
      if(!strncmp(p->msg, cp, strlen(cp)))
      {
        sprintf(tbuf, "%s: %s", p->msg, spaces);
        strcpy(tbuf+18, p->help);
        printf("%s\n", tbuf);

	if(sockmode)
	{
          log_msg(tbuf);
	}
      }
    }
    else
    {
      sprintf(tbuf, "%s: %s", p->msg, spaces);
      strcpy(tbuf+18, p->help);
      printf("%s\n", tbuf);

      if(sockmode)
      {
        log_msg(tbuf);
      }

    }
  }

  return(0);
}



char *vxstrtok(string, last_token, delim)
char *string, *last_token, *delim;
 
/* char *vxstrtok(char *string, char *last_token, char *delim) --
 * Like strtok, but can be used in VxWorks.  The first call passes a non-NULL
 * "string".  A pointer to the first character in "string" not in "delim" is
 * returned (or NULL if none).  The next occurance of a character in "delim"
 * past this first character is set to NUL and a second NUL is appended to
 * "string".  Subsequent calls must pass a NULL pointer "string" and the
 * previous return value ("last_token").  In these calls, the NUL following
 * last_token is replaced by the first character in "delim" and the scan
 * continues until the next non-"delim" past this point is found or the
 * end of the string is found (NULL returned).  Again, if a non-"delim"
 * character is found, the next "delim" character following it is replaced by
 * a NUL.
 */
 
{
  int n, i;
  char *ptr, *ret;

  if(string) {
						/* First call on a new string */
    string[strlen(string)+1] = '\0';	       /* Add a second NUL to the end */
    ptr = string;
  } else if(!last_token || !*last_token || !delim || !*delim)
    return NULL;
  else {
				      /* A subsequent call on the same string */
    ptr = last_token + strlen(last_token);	     /* End of previous token */
    *ptr++ = delim[0];				      /* Put a delimiter back */
  }
				   /* Scan for the next non-"delim" character */
  for(n=strlen(delim); *ptr; ptr++) {
    for(i=0; i < n && *ptr != delim[i]; i++)
      continue;
    if(i == n)
      break;							 /* Found one */
  }
  if(!*ptr)
    return NULL;					     /* End of string */
 
  ret = ptr++;					   /* Save beginning of token */
 
					   /* Scan for next "delim" character */
  for( ; *ptr; ptr++) {
    for(i=0; i < n && *ptr != delim[i]; i++)
      continue;
    if(i != n)
       break;							/* Found one */
  }
  *ptr = '\0';						      /* Mark the end */
  return ret;					      /* Return the beginning */
}


int do_debug(buf)
char *buf;
{
  if(buf)
  {
    debug = atoi(buf);
  }

  return(0);
}



/* OTF routines */

int do_ephemObj(buf)
char *buf;
{
  if(buf)
  {
    ephemObj = atoi(buf);
  }
  else
  {
    ephemObj = 0;
  }

  return(0);
}



int do_otfGridCellSizeRa(buf)
char *buf;
{
  double f, atof();

  if(buf)
  {
    f = atof(buf);
  }

  if(f > 0.0 )
  {
    otf.manNyquistRa = f;
  }

  return(0);
}



int do_otfGridCellSizeDec(buf)
char *buf;
{
  double f, atof();

  if(buf)
  {
    f = atof(buf);
  }

  if(f > 0.0 )
  {
    otf.manNyquistDec = f;
  }

  return(0);
}


int do_otfGridSizeRa(buf)
char *buf;
{
  double f, atof();

  if(buf)
  {
    f = atof(buf);
  }

  if(f > 0.0 )
  {
    otf.manMapWidth = f;
  }

  return(0);
}



int do_otfGridSizeDec(buf)
char *buf;
{
  double f, atof();

  if(buf)
  {
    f = atof(buf);
  }

  if(f > 0.0 )
  {
    otf.manMapHeight = f;
  }

  return(0);
}


int do_otfGridRestfreq(buf)
char *buf;
{
  double f, atof();

  if(buf)
  {
    f = atof(buf);
  }

  otf.restfreq = f;

  return(0);
}


int do_otfGridFreqres(buf)
char *buf;
{
  double f, atof();

  if(buf)
  {
    f = atof(buf);
  }

  otf.freqres = f;

  return(0);
}


int do_otfGridCols(buf)
char *buf;
{
  int cols, atoi();

  if(buf)
  {
    cols = atoi(buf);
  }

  otf.cols = cols;

  return(0);
}


int do_otfGridRows(buf)
char *buf;
{
  int rows, atoi();

  if(buf)
  {
    rows = atoi(buf);
  }

  otf.rows = rows;

  return(0);
}


int do_otfGridNoint(buf)
char *buf;
{
  int noint, atoi();

  if(buf)
  {
    noint = atoi(buf);
  }

  otf.noint = noint;

  return(0);
}


int do_otfGridAllowVelShift(buf)
char *buf;
{
  int allow, atoi();

  if(buf)
  {
    allow = atoi(buf);
  }

  otf.allowVelShift = allow;

  return(0);
}


int do_otfGridRa(buf)
char *buf;
{
  double f, atof();

  if(buf)
  {
    f = atof(buf);
  }

  if(f >= 0.0 )
  {
    otf.manMapRa = f;
  }

  return(0);
}



int do_otfGridDec(buf)
char *buf;
{
  double f, atof();

  if(buf)
  {
    f = atof(buf);
  }

  if(f >= -90.0 || f <= 90.0)
  {
    otf.manMapDec = f;
  }

  return(0);
}


int do_otfGridVelocity(buf)
char *buf;
{
  double f, atof();

  if(buf)
  {
    f = atof(buf);
  }

  otf.velocity = f;

  return(0);
}


int do_otfIFchoice(buf)
char *buf;
{
  if(buf)
  {
    otf.chanSelOTF = atoi(buf);
  }

  if(otf.chanSelOTF < 0)
  {
    otf.chanSelOTF = 0;
  }

  chanSelOTF = otf.chanSelOTF;

  return(0);
}


int do_otfChans(buf)
char *buf;
{
  char *cp;
  char tmp[256];;

  strcpy(tmp, buf);
  cp = strtok(tmp," \n\t");
  if(cp)
  {
    otf.int_chans[0]  = atoi(cp);
  }
  else
  {
    log_msg("Bad begin chan specifier in otfchans");
    return(1);
  }

  cp = strtok((char *)NULL," \n\t");
  if(cp)
  {
    otf.int_chans[1] = atoi(cp);
  }
  else
  {
    log_msg("Bad ending chan specifier in otfchans");
    return(1);
  }

  return(0);
}



int getFeed(scan)
double scan;
{
  int feed;
  double tmp;
    
  tmp = scan - floor(scan);
    
  if(tmp > 0.1) /* is this the MAC data; .11 .12 */
  {
    tmp -= 0.1;
  }

  tmp *= 100.0;
  feed = (int)(tmp + 0.25);

  return(feed);
}


int configFromScan(buf)
char *buf;
{
  int be, modeid, center, ifnum;
  int noint, cols, rows;
  char fname[256], gname[256], tbuf[256];
  char *cp, tmp[256], ifilename[256];
  double scan, dish, nyquistra, nyquistdec, map_xsize, map_ysize;
  struct HEADER head;
  extern char filename[];

  if(strlen(buf))
  {
    strcpy(tmp, buf);
    cp = strtok(tmp, " \n\t");

    if(cp)
    {
      strcpy(fname, cp);
    }
    else
    {
      log_msg("Missing filename arg to configFromScan(%s)", buf);
      return(1);
    }

    cp = strtok(NULL, " \n\t");

    if(cp)
    {
      scan = atof(cp);
    }
    else
    {
      log_msg("Missing scan arg to configFromScan(%s)", buf);
      return(1);
    }

    if(!peekScan(scan, fname, &be, &modeid, &head))
    {
      log_msg("Good read");

      ifnum = getFeed(scan);
      ifnum--;

      sprintf(tbuf, "%d", ifnum);
      log_msg("Setting IF to %d", ifnum);

      do_otfIFchoice(tbuf);

      do_filename(fname);
      sprintf(gname, "%s/g%s", dirname(fname), basename(fname));
      do_gfilename(gname);

      noint = (int)head.noint;
      center = noint / 2;
      log_msg("otfchans %d %d", center-1, center+1);
      otf.int_chans[0] = center-1;
      otf.int_chans[1] = center+1;
     
      log_msg("otfgridvelocity %f", head.refpt_vel);
      otf.velocity = head.refpt_vel;

      log_msg("otfgridfreqres %f", head.freqres);
      otf.freqres = head.freqres;

      log_msg("otfgridrestfreq %f", head.restfreq);
      otf.restfreq = head.restfreq;

      log_msg("otfgridra %f", head.epocra);
      otf.manMapRa = head.epocra;

      log_msg("otfgriddec %f", head.epocdec);
      otf.manMapDec = head.epocdec;

      if(!strncmp(head.telescop, "ARO-HHT", 7))
      {
        log_msg("Data File is SMT Data");
        dish = 10.0;
      }
      else
      if(!strncmp(head.telescop, "NRAO 12M", 8) || !strncmp(head.telescop, "ARO-12M", 7))
      {
        log_msg("Data File is 12M Data");
        dish = 12.0;
      }
      else
      {
        dish = 12.0;
      }

      nyquistra  = (2.997925e8 / (2.0 * dish * head.restfreq)) * 0.2062648;
      nyquistdec = nyquistra * 0.90;

      log_msg("Over building map size by factor of 2.0");

      cols       = (int)(head.noxpts * 2.0);
      rows       = (int)(head.noypts * 2.0);
      map_xsize  = (cols * nyquistra)  / 3600.0;
      map_ysize  = (rows * nyquistdec) / 3600.0;

      log_msg("otfgridsizera %f", map_xsize);
      otf.manMapWidth = map_xsize;

      log_msg("otfgridsizedec %f", map_ysize);
      otf.manMapHeight = map_ysize;

      log_msg("otfgridcellsizera %f", nyquistra);
      otf.manNyquistRa = nyquistra;

      log_msg("otfgridcellsizedec %f", nyquistdec);
      otf.manNyquistDec = nyquistdec;

      log_msg("otfgridcols %d", cols);
      otf.cols = cols;

      log_msg("otfgridrows %d", rows);
      otf.rows = rows;

      log_msg("Total available map cells: %d", cols * rows);

      log_msg("otfgridnoint %d", noint);
      otf.noint = noint;

      log_msg("otfgridinit");
      do_otfGridInit();

      /* Now load any ignore file */
      sprintf(fname, filename);
      sprintf(ifilename, "%s/ignores.%s", dirname(fname), basename(fname));

      log_msg("Clearing out ignored scans");
      do_clearIgnore();

      log_msg("Loading ignores from: %s", ifilename);
      do_load(ifilename);

      sock_send(last_msg(), "Config Done");
    }
    else
    {
      log_msg("Bad read");
    }
  }
  else
  {
    log_msg("Missing filename and scan args to configFromScan(%s)", buf);
    return(1);
  }

  return(0);
}
