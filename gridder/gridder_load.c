#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>
#include <math.h>

#include "mbdef.h"
#include "loglib.h"
#include "caclib_proto.h"

#define MAX_GRIDDER_HOSTS    256
#define MAX_GRIDDER_THREADS 1024
#define MAX_GRIDDER_TILES   1024

struct GRIDDER {
	int busy;		/* Gridder is currently gridding a tile */
	int grids;		/* How many tiles has this thread proccessed */

	int ncells[256];	/* Number of cells loaded in each tile */

	char host[64];		/* Gridder hostname */
	char sockname[64];	/* Gridder Thread within that host */

	struct SOCK *sock;	/* Open socket to that thread */

	double start;		/* Start time of the current tile */
	double stop;		/* stop  time of the current tile */
	double times[256];	/* Execution time for each tiles griddered */

};

struct GRIDDER gridders[MAX_GRIDDER_THREADS];


struct TALLY {
	char   host[32];

	int    grids;
	int    used;
	int    nthreads;
	int    ncells;

	double total_time;
	double min_time;
	double max_time;
	double avg_time;
};
struct TALLY tallies[MAX_GRIDDER_HOSTS];


struct TILE {
	char fname[256];
	char source[256];

	int  bscan;
	int  escan;
};

struct TILE tiles[MAX_GRIDDER_TILES];

int used_threads;
int used_tiles;
int used_tallies;

char tok[40][40];
char *delim = "\n\t, ";

struct SOCK *logger;

double ifNum;
double mjdStart, mjdStop;

int get_tok(edit)
char *edit;
{   
  int c = 0;
  char *token;

  bzero((char *)&tok, sizeof(tok));

  token = strtok(edit, delim);
  while(token != (char *)NULL)
  {   
    sprintf(tok[c++], "%s", token);
    token = strtok((char *)NULL, delim);
  }

  return(c);
}



int loadHost(h)
struct GRIDDER *h;
{
  int i;
  struct TALLY *t;

  t = &tallies[0];
  for(i=0;i<MAX_GRIDDER_HOSTS;i++,t++) 	/* look to see if I already loaded it */
  {
    if(!strcmp(h->host, t->host))	/* Alread loaded */
    {
      t->nthreads++;

      return(0);
    }
  }

  /* first time for this one; find first available slot */

  t = &tallies[0];
  for(i=0;i<MAX_GRIDDER_HOSTS;i++,t++)
  {
    if(!t->used)
    {
      log_msg("GRIDDER_LOAD: Found unique host: %s", t->host);

      strcpy(t->host, h->host);
      t->used = 1;
      t->nthreads++;

      t->min_time = 99999.0; 			/* preset this */

      used_tallies++;

      return(0);
    }
  }

  log_msg("Load host: %s, too many hosts; exiting", h->host);

  exit(1);

  return(1);
}


struct TALLY *findHost(h)
struct GRIDDER *h;
{
  int i;
  struct TALLY *t;

  t = &tallies[0];
  for(i=0;i<used_tallies;i++,t++)
  {
    if(!strcmp(h->host, t->host))
    {
      return(t);
    }
  }

  log_msg("Tally host: %s, not found; exiting", h->host);

  exit(1);

  return((struct TALLY *)NULL);
}


int initGridders()
{
  int    i;
  struct DEST *dest;
  struct GRIDDER *p;
  
  p = &gridders[0];
  for(i=0;i<MAXDEST;i++)
  { 
    dest = get_dest(i);
  
    if(dest)
    {
      if(strcmp(dest->dname, "GRIDDER_LOADER") && strstr(dest->dname, "GRIDDER_"))
      {
        strcpy(p->host, dest->host); 
        strcpy(p->sockname, dest->dname); 

        p->sock = sock_connect(p->sockname);

        loadHost(p);

	sock_send(p->sock, " ");

        p++;
        used_threads++;
      }
    }
  }

  log_msg("GRIDDER_LOAD: Loaded %d gridder threads", used_threads);

  return(0);
}


int markDone(mb, tbuf)
struct SOCK *mb;
char *tbuf;
{
  int i;
  struct GRIDDER *p;

  get_tok(tbuf);

  p = &gridders[0];
  for(i=0;i<used_threads;i++,p++)
  {
    if(p->sock == mb)
    {
      log_msg("GRIDDER_LOAD: Marking %s thread done", p->sockname);
      p->busy = 0;
      p->stop = getTimeD(0);

      p->times[p->grids] = atof(tok[0]);
      p->ncells[p->grids] = atoi(tok[1]);
      p->grids++;

      return(0);
    }
  }

  return(1);
}


int parseMessage(buf)
char *buf;
{
  struct SOCK *mb;

  mb = last_msg();
  log_msg("GRIDDER_LOAD: Got %s from %s", buf, sock_name(mb));

  if(!strncasecmp(buf, "load done", 9))
  {
    log_msg("GRIDDER_LOAD: Got Done from %s", sock_name(mb));

    markDone(mb, buf+9);
  }

  return(0);
}


/* Return the next available free gridder thread 

   1) Loop until a free thread one shows up 
   2) Check incoming socket messages for possible 'done'
      commands from finished threads 

   never exits without finding a free one.

   Possible problem is that this doesn't spead the load, i.e.
   if front loads all the work on the first units.  So, if
   the first render farm computers get done quickly, then the 
   last units may never see action. to be seen if thist needs
   to be tweaked.
*/
struct GRIDDER *getNextGridder()
{
  int    i, sel, len;
  char   message[4096];
  struct GRIDDER *p;

  while(1)
  {
    p = &gridders[0];
    for(i=0;i<used_threads;i++,p++)
    {
      if(!p->busy)
      {
        return(p);
      }

      sel = sock_sel(message, &len, NULL, 0, -1, 0);	/* See if any pending "Done" commands */

      if(sel > 0)
      {
        sel = sock_sel(message, &len, NULL, 0, 1, 0);	/* actually read it */
        if(sel > 0)
        {
          parseMessage(message);
        }
      }
    }

    usleep(10000);	/* don't spin too fast */
  }

  return((struct GRIDDER *)NULL);
}


int printMissing()
{
  int    i;
  struct GRIDDER *p;

  p = &gridders[0];
  for(i=0;i<used_threads;i++,p++)
  {
    if(p->busy)
    {
      log_msg("GRIDDER_LOAD: %s on host: %s, still busy", p->sockname, p->host);
    }
  }

  return(0);
}


int allDone()
{
  int    i, n=0;
  struct GRIDDER *p;

  p = &gridders[0];
  for(i=0;i<used_threads;i++,p++)
  {
    if(p->busy)
    {
    //  log_msg("GRIDDER_LOAD: %s still busy", p->sockname);
      n++;
    }
  }

  if(n)
  {
    log_msg("GRIDDER_LOAD: Waiting on %d threads", n);

    if(n<6)
    {
      printMissing();
    }

    return(1);
  }

  return(n);
}

int printAndExit()
{
  int    i, j;
  char   bigBuf[512], tbuf[256];
  struct GRIDDER *p;
  struct TALLY   *t, tally;

  bzero((char *)&tally, sizeof(tally));

  p = &gridders[0];
  for(i=0;i<used_threads;i++,p++)
  {
    t = findHost(p);

    sprintf(bigBuf, "GRIDDER_LOAD: %12.12s %12.12s %2d ", p->host, p->sockname, p->grids);

    t->grids += p->grids;

    for(j=0;j<p->grids;j++)
    {
      if(p->times[j] < t->min_time)
      {
	t->min_time = p->times[j];
      }
      if(p->times[j] > t->max_time)
      {
	t->max_time = p->times[j];
      }

      t->total_time += p->times[j];
      t->ncells     += p->ncells[j];

      sprintf(tbuf, "%6.3f %4d", p->times[j], p->ncells[j]);

      strcat(bigBuf, tbuf);
    }

    log_msg(bigBuf);
  }

  log_msg(" ");

  log_msg("GRIDDER_LOAD:        Host   Threads Tiles   Min      Max    Total      Avg      Cells");

  t = &tallies[0];
  for(i=0;i<MAX_GRIDDER_HOSTS;i++,t++)
  {
    if(t->used)
    {
      if(t->grids)
      {
        t->avg_time = t->total_time / (double)t->grids;
      }

      log_msg("GRIDDER_LOAD: %12.12s %7d %4d %8.3f %8.3f %8.3f %8.3f %10d", 
				t->host, t->nthreads, t->grids,
				t->min_time,
				t->max_time,
				t->total_time,
				t->avg_time,
				t->ncells);

      tally.nthreads   += t->nthreads;
      tally.grids      += t->grids;
      tally.total_time += t->total_time;
      tally.avg_time   += t->avg_time;
      tally.ncells     += t->ncells;
    }
  }

  log_msg("GRIDDER_LOAD:  -----------------------------------------------------------------------");

  log_msg("GRIDDER_LOAD: %12.12s %7d %4d %8.3f %8.3f %8.3f %8.3f %10d", 
				"Totals", tally.nthreads, tally.grids,
				0.0,
				0.0,
				tally.total_time,
				tally.avg_time,
				tally.ncells);

  log_msg("GRIDDER_LOAD: %12.12s %7d %4d %8.3f %8.3f %8.3f %8.3f", 
				"Averages", tally.nthreads/used_tallies, tally.grids/used_tallies,
				0.0,
				0.0,
				tally.total_time/(double)used_tallies,
				tally.avg_time/(double)used_tallies);
/*
GRIDDER_LOAD:      render1       7   47    0.234    6.586  152.616    3.247
*/

  mjdStop = getmJulDate(0);
  log_msg("GRIDDER_LOAD: Total time: %.2f Seconds", (mjdStop-mjdStart) * 86400);

  exit(0);
}


struct TILE *getNextTile()
{
  static int indx = -1;

  indx++;
  if(indx >= used_tiles)
  {
    return((struct TILE *)NULL);
  }
  else
  {
    return(&tiles[indx]);
  }
}
  


int loadGridderFile(fname)
char *fname;
{
  int  n;
  char inBuf[256], pbuf[256];
  FILE *fp;
  struct TILE *p;

  fp = fopen(fname, "r");

  if(!fp)
  {
    log_msg("GRIDDER_LOAD: unable to open %s", fname);
    exit(1);
  }

  p = &tiles[0];
  while(fgets(inBuf, 256, fp) != NULL)
  {
    if(!strncmp(inBuf, "#", 1))
    {
      continue;
    }

    strcpy(pbuf, inBuf);

    n = get_tok(pbuf);

    if(n == 4)
    {
      strcpy(p->fname, tok[0]);
      strcpy(p->source, tok[1]);

      p->bscan = atoi(tok[2]);
      p->escan = atoi(tok[3]);
      
      p++;
      used_tiles++;
    }
    else
    {
      log_msg("GRIDDER_LOAD: Bad format in: %s", inBuf);
    }
  }

  log_msg("GRIDDER_LOAD: Loaded %d tiles", used_tiles);

  fclose(fp);

  return(0);
}


int main(argc, argv)
int argc;
char *argv[];
{
  int sel, len;
  char *hn, *mb, *ss, *lh;
  char sbuf[256];
  char message[4096];
  double scan;
  struct GRIDDER *g;
  struct TILE    *t;

  if(argc < 3)
  {
    log_msg("Usage: %s gridder_load_file IF_#", argv[0]);
    exit(1);
  }

  ifNum = atof(argv[2]);

  setCactusEnvironment();

  smart_log_open("gridder_load", 3);

  log_eventdEnable(LOG_TO_LOGGER);

  hn = getenv("HOST");
  if(!hn)
  {
    log_msg("GRIDDER_LOAD: HOST not defined");
    exit(1);
  }
  log_msg("GRIDDER_LOAD: HOST: %s", hn);

  mb = getenv("MAILBOXDEFS");
  if(!mb)
  { 
    log_msg("GRIDDER_LOAD: MAILBOXDEFS not defined");
    exit(1);
  }
  log_msg("GRIDDER_LOAD: MAILBOXDEFS: %s", mb);

  ss = getenv("LPSESSION");
  if(!ss)
  {
    log_msg("GRIDDER_LOAD: LPSESSION not defined");
    exit(1);
  }
  log_msg("GRIDDER_LOAD: LPSESSION: %s", ss);

  lh = getenv("LINUXPOPS_HOME");
  if(!lh)
  {
    log_msg("GRIDDER_LOAD: LINUXPOPS_HOME not defined");
    exit(1);
  }
  log_msg("GRIDDER_LOAD: LINUXPOPS_HOME: %s", lh);

  sock_bind("GRIDDER_LOADER");

  initGridders();

  loadGridderFile(argv[1]);

  mjdStart = getmJulDate(0);

  while(1)
  {
    g = getNextGridder();
    t = getNextTile();

    if(!t)
    {
      log_msg("GRIDDER_LOAD: Last tile sent");
      log_msg("GRIDDER_LOAD: Waiting on last threads to finish");

      while(1)
      {

        sel = sock_sel(message, &len, NULL, 0, 1, 0);
        if(sel > 0)
        {
          parseMessage(message);
        }
        else
        {
          if(!allDone())
          {
            log_msg("GRIDDER_LOAD: All Done");
	  
            printAndExit();
          }
        }

        usleep(10000);
      }
    }

    scan = (double)t->bscan + ifNum / 100.0;

    sprintf(sbuf, "configfromscan %s %.2f", t->fname, scan);
    log_msg("GRIDDER_LOAD: To %s, %s", g->sockname, sbuf);
    sock_send(g->sock, sbuf);

    sprintf(sbuf, "otfgridload %d %d", t->bscan, t->escan);
    log_msg("GRIDDER_LOAD: To %s, %s", g->sockname, sbuf);
    sock_send(g->sock, sbuf);

    g->busy = 1;

    g->start = getTimeD(0);
  }

  return(0);
}
