#include "ds_extern.h"
#include "ds_proto.h"

#ifdef GALACTIC
#undef GALACTIC
#endif

#ifdef AZEL
#undef AZEL
#endif

#ifdef USERDEF
#undef USERDEF
#endif


extern struct HEADER  onhead01;
extern struct HEADER  offhead01;
extern struct HEADER  gainshead01;

extern float  *ondata01;
extern float  *offdata01;
extern float  *gainsdata01;

extern float scanNum, otfIFNumber, datamin, datamax;
extern int chanSelOTF, lockScale;
extern char filename[], gfilename[], source[], prgName[];
extern int found_scans[MAX_BACKENDS*MAX_PARTS];

int mhz_4if_mode  =  0;
int khz_4if_mode  =  0;
int aos_4if_mode  =  0;
int non_if_mode   =  0;
int dbe_mode      =  0;

struct SPECTRA_DISPLAY_PARAMS sdp[] = {

	/* FFB-A */ /* FFB-B */ /* FFB-C */ /* FFB-D */
  { 1, 0.01,  0.66, 1.0,  &ondata01, &onhead01, 0, &mhz_4if_mode, BACKEND_FFB, "F1M-1"},
  { 1, 0.02,  0.66, 1.0,  &ondata01, &onhead01, 0, &mhz_4if_mode, BACKEND_FFB, "F1M-2"},
  { 1, 0.03,  0.66, 1.0,  &ondata01, &onhead01, 0, &mhz_4if_mode, BACKEND_FFB, "F1M-3"},
  { 1, 0.04,  0.66, 1.0,  &ondata01, &onhead01, 0, &mhz_4if_mode, BACKEND_FFB, "F1M-4"},

	/* FB2-A */ /* FB2-B */ /* FB2-C */ /* FB2-D */
  { 1, 0.05,  0.33, 0.66, &ondata01, &onhead01, 0, &khz_4if_mode, BACKEND_FFB, "FQM-1"},
  { 1, 0.06,  0.33, 0.66, &ondata01, &onhead01, 0, &khz_4if_mode, BACKEND_FFB, "FQM-2"},
  { 1, 0.07,  0.33, 0.66, &ondata01, &onhead01, 0, &khz_4if_mode, BACKEND_FFB, "FQM-3"},
  { 1, 0.08,  0.33, 0.66, &ondata01, &onhead01, 0, &khz_4if_mode, BACKEND_FFB, "FQM-4"},

	/* AOS-AB */
  { 1, 0.01, 0.009, 0.33, &ondata01, &onhead01, 0, &aos_4if_mode, BACKEND_AOS, "A1M-1"},
  { 1, 0.02, 0.009, 0.33, &ondata01, &onhead01, 0, &aos_4if_mode, BACKEND_AOS, "A1M-2"},
  { 0, 0.02, 0.009, 0.33, &ondata01, &onhead01, 0, &aos_4if_mode, BACKEND_AOS, "A1M-3"},
  { 0, 0.02, 0.009, 0.33, &ondata01, &onhead01, 0, &aos_4if_mode, BACKEND_AOS, "A1M-4"},

	/* AOS-C */
  { 1, 0.05, 0.009, 0.33, &ondata01, &onhead01, 0, &aos_4if_mode, BACKEND_AOS, "AQM-1"},
  { 0, 0.05, 0.009, 0.33, &ondata01, &onhead01, 0, &aos_4if_mode, BACKEND_AOS, "AQM-2"},
  { 0, 0.05, 0.009, 0.33, &ondata01, &onhead01, 0, &aos_4if_mode, BACKEND_AOS, "AQM-3"},
  { 0, 0.05, 0.009, 0.33, &ondata01, &onhead01, 0, &aos_4if_mode, BACKEND_AOS, "AQM-4"},
           
	/* CTS-A */
  { 1, 0.01, 0.009, 0.33, &ondata01, &onhead01, 0, &aos_4if_mode, BACKEND_CTS, "C50-1"},
  { 0, 0.01, 0.009, 0.33, &ondata01, &onhead01, 0, &aos_4if_mode, BACKEND_CTS, "C50-2"},
  { 0, 0.01, 0.009, 0.33, &ondata01, &onhead01, 0, &aos_4if_mode, BACKEND_CTS, "C50-3"},
  { 0, 0.01, 0.009, 0.33, &ondata01, &onhead01, 0, &aos_4if_mode, BACKEND_CTS, "C50-4"},

	/* DBE */
  { 1, 0.01, 0.009, 0.33, &ondata01, &onhead01, 0, &dbe_mode,     BACKEND_DBE},
  { 1, 0.02, 0.009, 0.33, &ondata01, &onhead01, 0, &dbe_mode,     BACKEND_DBE},
  { 1, 0.03, 0.009, 0.33, &ondata01, &onhead01, 0, &dbe_mode,     BACKEND_DBE},
  { 1, 0.04, 0.009, 0.33, &ondata01, &onhead01, 0, &dbe_mode,     BACKEND_DBE}
};


float ifnums[] = { 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08};

int ignore_gains = 0,
    restoreMsg   = 0;

extern int printDataLines, lastGraph;

float ifNumber = 0.01;


int loadFile(ifNum)
float ifNum;
{
  char *cp;
  int ret=0;

  cp = filename;

  if( (ret = read_scan( scanNum + ifNum, cp, &ondata01, &onhead01)) != 0 )
    return(ret);

  strncpy(source, onhead01.object, SRCLEN);
  source[SRCLEN] = '\0';

  return(ret);
}



int do_linetotalmapoff()
{
  restoreMsg=1;

  return(0);
}



int do_lineotf()
{
  int i, n, k, ret;
  float requestOff, requestGains;
  char *cp;

  if(debug)
    log_msg("Entering do_lineotf");

  k = chanSelOTF;

  otfIFNumber  = sdp[k].ifnum;

  if(debug)
    log_msg("Using %s for OTF", sdp[k].name);

  cp = filename;

  if(debug)
    log_msg("Using %s file ", cp);

  ret = read_scan( scanNum+otfIFNumber, cp, &ondata01, &onhead01);

  if(ret)
  {
    if(ret != -2)
    {
      log_msg("On scan #%.2f, NOT found in %s", scanNum+otfIFNumber, cp);
    }

    return(0);
  }

  requestOff = onhead01.offscan;

  if(read_scan( requestOff, cp, &offdata01, &offhead01))
  {
    log_msg("Off scan #%.2f, NOT found in %s", requestOff, cp);
    return(0);
  }

  requestGains = (double)((int)onhead01.gains) + otfIFNumber;

  cp = gfilename;

  if(read_scan( requestGains, cp, &gainsdata01, &gainshead01))
  {
    log_msg("Gain scan #%.2f, NOT found in %s", requestGains, cp);
    return(0);
  }

  if(ignore_gains)
  {
    log_msg("Ignoring Gains");

    n = (int)gainshead01.noint;

    for(i=0;i<n;i++)
    {
      gainsdata01[i] = 400.0;
    }
  }

  do_otf(ondata01, &onhead01, offdata01, gainsdata01);

  return(0);
}


int setSourceName(head)
struct HEADER *head;
{
  char temp[128];

  lastGraph = 0;
  strncpy(source, head->object, SRCLEN);
  source[SRCLEN] = '\0';
  sprintf(temp, "Source: %s", source);

  return(0);
}



int clipDataProc(data, head, nchan, cd)
float *data;
struct HEADER *head;
int nchan, cd;
{
  int i, j, n;

  if(cd == 0)
    return(1);

  n = nchan - (cd*2);
  for(j=0, i=cd;i<(n+cd);i++)
    data[j++] = data[i];

  head->noint = head->noint - (cd*2);
  head->datalen = head->datalen - (cd*2*sizeof(float));
  head->refpt = head->refpt - (float)cd;

  return(0);
}




int freeDataPointers()
{
  if(ondata01) 
  {
    printData(ondata01);
    free((char *)ondata01);
    ondata01 = (float *)NULL;
  }

  if(offdata01) 
  {
    free((char *)offdata01);
    offdata01 = (float *)NULL;
  }

  if(gainsdata01) 
  {
    free((char *)gainsdata01);
    gainsdata01 = (float *)NULL;
  }

  return(0);
}



int printData(f)
float *f;
{
  int i=0, j=0;
  char tb[20], outBuf[256];

  if(printDataLines)
  {
    strcpy(outBuf, " ");
    while(i<printDataLines)
    {
      sprintf(tb, "%.4f ", f[j]);
      strcat(outBuf, tb);
      j++;
      if(!(j%10))
      {
        log_msg("%s\n", outBuf);
        strcpy(outBuf, " ");
        i++;
      }
    }
  }

  return(0);
}
