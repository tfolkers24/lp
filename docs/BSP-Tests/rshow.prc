procedure rshow
#
double lpeak
double ratio
double p2p
double ubeam
#
autobad = 1
#
stack fill
stack list
#
startstack 
  get stackscan /q
  baseline /q /i
  peak /q /i
  lpeak = datamax /q
  rms /q /i
  p2p = rms / 0.707 * 2.0 /q
  ratio = lpeak / p2p /q
  ubeam = pbeam - 120.0 /q
#
# the /n stops print of "var =" /e for scientific format
#
  print RESULTS: @Space scan ubeam p2p ratio rms /n /e
#
  stack inc 
endstack
#
print acount
#
end
