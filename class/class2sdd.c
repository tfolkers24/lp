
/* class.c, write SMT data to a class data file */

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#ifndef __DARWIN__
#include <malloc.h>
#endif
#include <math.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "header.h"
#include "sdd.h"
#include "class.h"
#include "slalib.h"

#include "caclib_proto.h"
#include "./proto.h"

#define SECONDS_ON_AUG27_2024 (1724716800+86400) /* Stupid Class File date format... */
#define SPL                   (0.299792458)

double deg_sin(double);
double deg_cos(double);

float data[8192];
struct HEADER head;

struct file_index        findex;
struct index_ent         entry[65535];

struct CLASS_SCAN {
	struct scan_ent          scan;
	struct index_ent        *entry;
	struct general_descr     general;
	struct position_descr    position;
	struct spectro_descr     spectro;
	struct base_descr        base;
	struct history_descr     history;
	struct plot_descr        plot;
	struct fswitch_descr     fswitch;
	struct calibration_descr calibration;
	struct skydip_descr      skydip;
	struct gauss_descr       gauss;
	struct shell_descr       shell;
	struct hfs_descr         hfs;
	struct drift_descr       drift;
	struct beam_descr        beam;
	struct cont_descr        cont;
};

struct CLASS_SCAN cscan;


/* Take what limited info I have from the various class sections
   and recreate a usable sdd header. Then write to disk.
 */
int format_and_write(outfile)
char *outfile;
{
  int i, n;
  time_t sdate, date2024;
  struct tm *tmnow;
  int year;
  static float  hold[65535];
  double utdate;


  bzero((char *)&head, sizeof(head));

  /* fill in a lot of default stuff */
  head.headlen = sizeof(head);
  strncpy(head.obsid,    "wtf xxx",      7);
  strncpy(head.precis,   "R*4",          3);
  strncpy(head.observer, "Who Knows",    9);
  strncpy(head.projid,   "wtf-2001",     8);
  strncpy(head.obsmode,  "LINEBSP ",     8);

  if(strstr(cscan.entry->telescope, "SMT"))
  {
    strncpy(head.cl11type, "PROTOHHT",   8);
    strncpy(head.telescop, "ARO-HHT",    7);
  }
  else
  {
    strncpy(head.cl11type, "PROTO12M",   8);
    strncpy(head.telescop, "NRAO 12M",   8);
  }

  head.norchan = 1.0;
  head.noswvar = 1.0;
  head.nophase = 1.0;
  head.cycllen = 1.0;
  head.spn     = 1.0;

  if( cscan.spectro.velocity == VLSR)
  {
    strncpy(head.veldef,  "RADILSR", 7);
  }
  else
  if( cscan.spectro.velocity == VHEL)
  {
    strncpy(head.veldef,  "RADIHELO", 8);
  }
  else
  if( cscan.spectro.velocity == VEAR)
  {
    strncpy(head.veldef,  "RADIEART", 8);
  }
  else
  if( cscan.spectro.velocity == VOBS)
  {
    strncpy(head.veldef,  "RADIOBS", 7);
  }

  strncpy(head.typecal, "VANE",     4);
  strncpy(head.polariz, "11211121", 8);
  
  /* Entry */
  head.scan = (double)cscan.entry->obs;
  strncpy(head.object,   cscan.entry->source,     12);
  strcmprs(head.object);
  strncpy(head.linename, cscan.entry->line,       12);
  strcmprs(head.linename);
  strncpy(head.backend,  cscan.entry->telescope+8, 4);

  if(strstr(head.backend, "FFBA"))
  {
    head.scan += 0.11;
  }
  else
  if(strstr(head.backend, "FFBB"))
  {
    head.scan += 0.12;
  }
  else
  if(strstr(head.backend, "FFBC"))
  {
    head.scan += 0.13;
  }
  else
  if(strstr(head.backend, "FFBD"))
  {
    head.scan += 0.14;
  }
  else
  if(strstr(head.backend, "AOSA"))
  {
    head.scan += 0.11;
  }
  else
  if(strstr(head.backend, "AOSB"))
  {
    head.scan += 0.12;
  }

  date2024 = SECONDS_ON_AUG27_2024;
  sdate    = cscan.entry->date_obs * 86400;
  sdate    = date2024 + sdate; /* Add because sdate is already negitive */
  tmnow    = localtime(&sdate);
  year     = tmnow->tm_year;

  if(year < 93)
  {
    year += 2000;
  }
  else
  {
    year += 1900;
  }

  utdate      = (double)year + (double)(tmnow->tm_mon+1) / 100.0 + (double)tmnow->tm_mday / 10000.0;
  head.utdate = utdate;

  if(cscan.entry->offset_type == EQUAT)
  {
    if(cscan.position.epoch == 2000)
    {
      strncpy(head.coordcd, "2000RADC", 8);
    }
    else
    {
      strncpy(head.coordcd, "1950RADC", 8);
    }
  }
  else
  if(cscan.entry->offset_type == GALAC)
  {
    strncpy(head.coordcd, "GALACTIC", 8);
  }
  else
  {
    strncpy(head.coordcd, "UNKNOWN", 7);
  }

  /* General */
  head.ut      = cscan.general.ut_time / 3600.0;
  head.lst     = cscan.general.lst_time / 3600.0;
  head.az      = RADIANS2DEGREES(cscan.general.azimuth);
  head.el      = RADIANS2DEGREES(cscan.general.elevation);
  head.tauh2o  = cscan.general.tau;
  head.stsys   = cscan.general.tsys;
  head.inttime = cscan.general.integration;
  head.effint  = cscan.general.integration;
  head.samprat = cscan.general.integration;

  /* Position */
  head.epoch   = (double)cscan.position.epoch;
  head.epocra  = RADIANS2DEGREES(cscan.position.lambda);
  head.epocdec = RADIANS2DEGREES(cscan.position.beta);
  head.xsource = head.epocra  - RADIANS2DEGREES(cscan.position.lambda_off) / cos(cscan.position.beta_off); 
  head.ysource = head.epocdec - RADIANS2DEGREES(cscan.position.beta_off);
  head.xref    = head.epocra;
  head.yref    = head.epocdec;
  head.indx    = head.epocra; /* Who cares what the 'current' positions are? */
  head.indy    = head.epocdec;/* Make then the same as epoch */

  /* Spectro */
  head.restfreq  = cscan.spectro.rest;
  head.datalen   = cscan.spectro.nchan * sizeof(float);
  head.noint     = cscan.spectro.nchan;
  head.refpt     = cscan.spectro.reference;
  head.freqres   = cscan.spectro.fres;
  head.bw        = head.freqres * head.noint;
  head.deltax    = cscan.spectro.vres;
  head.velocity  = cscan.spectro.voff;
  head.refpt_vel = cscan.spectro.voff;
  head.x0        = cscan.spectro.voff;

  head.badchv = cscan.spectro.bad;

  if(cscan.spectro.image > head.freqres)
  {
    head.sideband = 1.0;   /* LSB */
    head.freqres *= -1.0; /* Reverse X axis the scale */
    head.deltax  *= -1.0; /* Reverse X axis the scale */
  }
  else
  {
    head.sideband = 2.0;   /* USB */
  }

  head.obsfreq = cscan.spectro.sky;
  head.rvsys   = cscan.spectro.vteles;


  /* History */
  head.openpar[8] = cscan.history.start[0];
  head.openpar[9] = cscan.history.start[1];


  /* Calibration */
  head.etal     = cscan.calibration.beam_eff;
  head.etafss   = cscan.calibration.forw_eff;
  head.gaini    = cscan.calibration.gain_im;
  head.mmh2o    = cscan.calibration.h2omm;
  head.pressure = cscan.calibration.pamb;
  head.tamb     = cscan.calibration.tamb - ZEROC;
  head.tatms    = cscan.calibration.tatmsig;
  head.tchop    = cscan.calibration.tchop;
  head.tcold    = cscan.calibration.tcold;
  head.taus     = cscan.calibration.tausig;
  head.taui     = cscan.calibration.tauima;
  head.trx      = cscan.calibration.trec;
  head.tcal     = cscan.calibration.factor;
  head.count[0] = cscan.calibration.counts[0];
  head.count[1] = cscan.calibration.counts[1];
  head.count[2] = cscan.calibration.counts[2];


  /* set misc stuff to reasonable values */

  if(head.restfreq > 200000.0 && head.restfreq < 300000.0) /* 1mm */
  {
    strncpy(head.frontend, "1.3mmALA",     8);
    head.lofact   = 3.0;
    head.harmonic = 7.0;
    head.synfreq  = 10000.0;
    head.loif     = 100.0;
    head.firstif  = 6000.0;
  }
  else
  {
//#warning Dont forget to add other receivers
  }

  {
    double lambda, theta, hm;

    lambda     = SPL / head.restfreq;
    theta      = 1.28 * (lambda / 10.0); // Changed from 1.22 t.w.f/glauria
    hm         = theta * (180.0 / M_PI) * 1800000.0;
    head.wl    = lambda*1e6;
    head.bfwhm = hm;
  }

  {
    double rar, decr, glongr, glatr;

    rar  = DEGREES2RADIANS(head.epocra);
    decr = DEGREES2RADIANS(head.epocdec);

    slaEqgal(rar, decr, &glongr, &glatr);

    head.gallong = RADIANS2DEGREES(glongr);
    head.gallat  = RADIANS2DEGREES(glatr);
  }

  /* Fake the rest of these where there is no Class file equivalent */
  {
    /* Class 2 */
    {
      head.xpoint = 234.5;
      head.ypoint = 62.941;
      head.uxpnt  = 1.234;
      head.uypnt  = 7.7;
      head.focusr = 1.83;
      head.focusv = -1.68;
    }

    /* Class 5 */
    {
      head.humidity = 45.0;
      head.refrac   = 0.0122;
    }

    /* Class 6 */
    {
      head.xzero   = 153.5;
      head.yzero   = 135.0;
      head.deltaxr =  15.0;
      head.deltayr =  15.0;
      head.nopts   = 400.0;
      head.noxpts  =  20.0;
      head.noypts  =  20.0;
      head.xcell0  =   1.0;
      head.ycell0  =  20.0;
      strncpy(head.frame, "AZ_EL", 5);
    }

    /* Class 8 */
    {
      head.appeff  = 0.32;
      head.beameff = 0.55;
      head.antgain = 1.00;
    }

    /* Class 9 */
    {
      head.bmthrow  =  240.0;
      head.obstol   =    2.0;
      head.pbeam[0] =  120.0;
      head.mbeam[0] = -120.0;
      head.gains    = head.scan;
    }

    /* Class 11 */
    {
      head.tip_humid   = 42.0;
      head.refract_45  =  0.0122;
      head.ref_correct = 34.0;
      head.parallactic = 57.34;
    }
  }

  /* because class has the data in increasing velocity not freq; invert */
  n = head.noint;

  if(n > 65535)
  {
    printf("Scan %.2f contains too many channels %d\n", head.scan, n);
    return(1);
  }

  for(i=0;i<n;i++)
  {
    hold[i] = data[(n-1)-i];
  }

  bcopy((char *)hold, (char *)data, n*sizeof(float));

  printf("Writing scan %8.2f to %s\n", head.scan, outfile);

  write_sdd(outfile, &head, (char *)data);

  return(0);
}


int class_read(infile, outfile)
char *infile, *outfile;
{
  int  i, j, nscans, fd;
  long  scan_off, diff, old_addr = 0;
  char eqStr[80];

  fd = open(infile, O_RDONLY, Mode);

  if(fd == -1)
  {
    log_msg("Can't open class infile: %s", infile);

    return(0);
  }

  lseek(fd, 0L, SEEK_SET);
  read(fd, (char *) &findex, sizeof (findex));

  printf("Code     = '%4.4s'\n", findex.code);
  printf("next_rec = %6d\n", findex.next_rec);
  printf("num_ent  = %6d\n", findex.num_ent);
  printf("num_ext  = %6d\n", findex.num_ext);
  printf("next_ext = %6d\n", findex.next_ent);

  printf("Code     = ");
  for(i=0;i<251;i++)
  {
    printf("%d, ", findex.ext_addr[i]);
    if(findex.ext_addr[i] == 0)
    {
      break;
    }
  }

  printf("\n");


  printf(" ID:   Ver  Source       Line          Telescope      l-Offset  b-offset  Frame  Scan    addr    Delta    Offset\n");

  nscans = findex.next_ent-1;
//  nscans = findex.num_ent;

  if(nscans >= 65535)
  {
    fprintf(stderr, "Too many entries: %d, in file %s\n", nscans, infile);
    exit(1);
  }

  for(i=0;i<nscans;i++)
  {
    read(fd, (char *) &entry[i], sizeof (struct index_ent));

    if(entry[i].offset_type == UNKNOWN)
    {
      strcpy(eqStr, "Un");
    }
    else
    if(entry[i].offset_type == EQUAT)
    {
      strcpy(eqStr, "Eq");
    }
    else
    if(entry[i].offset_type == GALAC)
    {
      strcpy(eqStr, "Ga");
    }
    else
    if(entry[i].offset_type == HORIZ)
    {
      strcpy(eqStr, "Ho");
    }
    else
    if(entry[i].offset_type == PLANET)
    {
      strcpy(eqStr, "Pl");
    }
    else
    if(entry[i].offset_type == GEOCEN)
    {
      strcpy(eqStr, "Ea");
    }

    diff = entry[i].addr - old_addr;
    scan_off = REC_LEN * (entry[i].addr-1);
    printf("%5d;  %d %12.12s  %12.12s  %12.12s  %+8.3f   %+8.3f  %4.4s  %6d  %6d  %6ld  %10ld\n",
	entry[i].obs,
	entry[i].version,
	entry[i].source,
	entry[i].line,
	entry[i].telescope,
	entry[i].offsets[0] * 3600.0,
	entry[i].offsets[1] * 3600.0,
	eqStr,
	entry[i].scan,
	entry[i].addr,
	diff,
        scan_off);

    old_addr = entry[i].addr;
  }

  for(i=0;i<nscans;i++)
  {
    bzero((char *)&cscan.scan,        sizeof(cscan.scan));

    scan_off = REC_LEN * (entry[i].addr-1);
    lseek(fd, scan_off, SEEK_SET);

    read(fd, (char *) &cscan.scan, sizeof (cscan.scan));

    for(j=0;j<cscan.scan.num_sec;j++)
    {
      switch(cscan.scan.sect_info[j])
      {
	case GENERAL_SECT:     
		read(fd, &cscan.general, cscan.scan.sect_info[j+cscan.scan.num_sec]*4);
		break;

	case POSITION_SECT:    
		read(fd, &cscan.position, cscan.scan.sect_info[j+cscan.scan.num_sec]*4);
		break;

	case SPECTRO_SECT:     
		read(fd, &cscan.spectro, cscan.scan.sect_info[j+cscan.scan.num_sec]*4);
		break;

	case BASE_SECT:        
		read(fd, &cscan.base, cscan.scan.sect_info[j+cscan.scan.num_sec]*4);
		break;

	case HISTORY_SECT:     
		read(fd, &cscan.history, cscan.scan.sect_info[j+cscan.scan.num_sec]*4);
		break;

	case PLOT_SECT:        
		read(fd, &cscan.plot, cscan.scan.sect_info[j+cscan.scan.num_sec]*4);
		break;

	case FSWITCH_SECT:     
		read(fd, &cscan.fswitch, cscan.scan.sect_info[j+cscan.scan.num_sec]*4);
		break;

	case CALIBRATION_SECT: 
		read(fd, &cscan.calibration, cscan.scan.sect_info[j+cscan.scan.num_sec]*4);
		break;

	case SKYDIP_SECT:      
		read(fd, &cscan.skydip, cscan.scan.sect_info[j+cscan.scan.num_sec]*4);
		break;

	case GAUSS_SECT:       
		read(fd, &cscan.gauss, cscan.scan.sect_info[j+cscan.scan.num_sec]*4);
		break;

	case SHELL_SECT:       
		read(fd, &cscan.shell, cscan.scan.sect_info[j+cscan.scan.num_sec]*4);
		break;

	case HFS_SECT:         
		read(fd, &cscan.hfs, cscan.scan.sect_info[j+cscan.scan.num_sec]*4);
		break;

	case DRIFT_SECT:       
		read(fd, &cscan.drift, cscan.scan.sect_info[j+cscan.scan.num_sec]*4);
		break;

	case BEAM_SECT:        
		read(fd, &cscan.beam, cscan.scan.sect_info[j+cscan.scan.num_sec]*4);
		break;

	case CONT_SECT:        
		read(fd, &cscan.cont, cscan.scan.sect_info[j+cscan.scan.num_sec]*4);
		break;

	default:               
		printf("Found Unknown     Section %d\n", cscan.scan.sect_info[j]); 
		break;
      }
    }
    
    read(fd, (char *)&data[0], cscan.scan.data_size*4);

    cscan.entry = &entry[i];

    format_and_write(outfile);

  }

  close(fd);

  return(1);
}


int main(argc, argv)
int argc;
char *argv[];
{
  char sysBuf[256], infile[256], outfile[256];

  if(argc < 3)
  {
    fprintf(stderr, "Usage: %s class_file sdd_file\n", argv[0]);
    exit(3);
  }
  strcpy(infile,  argv[1]);
  strcpy(outfile, argv[2]);

  sprintf(sysBuf, "rm -f %s", outfile);
  system(sysBuf);

  new_sdd(outfile);

  class_read(infile, outfile);

  return(0);
}
