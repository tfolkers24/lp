#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <string.h>

#include "header.h"

#define MAX_DATA_CHANNELS  8192
#include "ds_image_info.h"

#include "loglib.h"

#include "caclib_proto.h"
#include "proto.h"

struct SOCK *master;

struct DS_IMAGE_INFO ds_image_info;

char message[sizeof(ds_image_info)+256];

int sendQuit()
{
  struct SOCK *socks[256];
  int i, n;

  log_msg("Entering shutdown routine");

  findSocketsButThese("TOMMIE", socks, &n);

  log_msg("Got %d external sockets", n);

  if(n)
  {
    for(i=0;i<n;i++)
    {
      if(socks[i] != master)
      {
        log_msg("Sending quit to %x", socks[i]);
        sock_send(socks[i], "quit");
      }
    }
  }

  return(0);
}


int main()
{
  int sel, len, mfd=0;
  char *cp, session[256], logdir[256];

  setCactusEnvironment();

  cp = getenv("LPSESSION");

  if(cp)
  {
    strcpy(session, cp);
  }
  else
  {
    strcpy(session, "12345");
  }

  sprintf(logdir, "LOGDIR=/tmp/%s/", session);
  putenv(logdir);

  log_open("helper", 4);

  sock_bind("LINUXPOPS_HELPER");

  log_eventdEnable(LOG_TO_LOGGER);

  master = sock_connect("LINUXPOPS_MASTER");

  sock_bufct(sizeof(message));

  while(1)
  {
    sel = sock_sel(message, &len, NULL, 0, 60, 0);

    log_msg("sel = %d", sel);

    if(sel > 0)
    {
      log_msg("Got from External program: %s", message);

      if(len == sizeof(ds_image_info))
      {
        log_msg("Got DS_IMAGE_INFO structure: %d bytes", len);

        bcopy(message,     (char *)&ds_image_info, len);
        sock_write(master, (char *)&ds_image_info, len);
      }
      else
      if(mfd && sel == mfd)
      {
        log_msg("Got from Master: %s", message);

        if(strstr(message, "exit"))
        {
          log_msg("Exiting");
          log_msg("lp_exit");

	  sendQuit();

          exit(0);
        }
      }
      else
      if(strstr(message, "master")) /* Master reporting in */
      {
        mfd = sel;
        log_msg("Master FD = %d", mfd);
      }
      else /* anything else gets passed up to master */
      {
        log_msg("Sending to Master");
        sock_send(master, message);
      }
    }
    else
    if(sel == -1)
    {
      log_msg("Timeout");
    }
  }

  return(0);
}
