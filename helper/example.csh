#!/bin/csh -f
#
if ( "$1" == "" ) then
  echo "Usage: $0 mailboxdefs-file"
  exit
endif
#
setenv MAILBOXDEFS $1
alias sock /home/corona/cactus/lib/sock
set mb=LINUXPOPS_HELPER
#
sleep 1
#
foreach f ( 3 4 7 11 12 14 15 )
  echo "switch $f" | sock $mb
  echo "acc" | sock $mb
  sleep 5
end
#
exit
