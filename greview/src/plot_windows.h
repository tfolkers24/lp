#define MAX_Y_PLOTS  400
#define MAX_X_PLOTS   10 
#define MAX_PLOT_WINDOWS (MAX_X_PLOTS * MAX_Y_PLOTS)

#define PLOT_WIDTH    80
#define PLOT_HEIGHT   60

#define X_START        4
#define Y_START        8
#define X_SPACING     85
#define Y_SPACING     65

#define MAX_PLOT_INDX 8192

#define BOXL            2
#define BOXR            2
#define BOXT            2
#define BOXB            2
#define NUM_DIV_X       4
#define NUM_DIV_Y       4


struct PLOT_WINDOW {
        GtkWidget *frame;
        GtkWidget *da;
        GdkPixmap *pixmap;

        char fname[32];
        char dname[32];

        int x, y, w, h;

	struct HEADER head;

	GdkPoint points[MAX_PLOT_INDX];
};

