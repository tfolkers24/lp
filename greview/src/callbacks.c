#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <string.h>

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"

#include "header.h"
#include "defines.h"
#include "gaussian.h"
#include "shell.h"
#include "window.h"
#include "image.h"
#include "global.h"
#include "plot_windows.h"
#include "linuxlib_proto.h"
#include "caclib_proto.h"
#include "proto.h"

struct PLOT_WINDOW *last_pw = NULL;

extern int debug;
extern struct SOCK *helper;

/* This is called when the main window gets a "delete_event" signal, which
   typically happens when the user clicks on the close icon on the window's
   title bar. If we didn't handle this, the window would be destroyed but
   our application would not exit. */
gboolean
on_main_window_delete_event            (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{
  gtk_exit (0);

  /* Shouldn't reach here, but we add it to stop compiler warnings. */
  return FALSE;
}


/* Create a pixmap for use in background drawing */
gboolean
on_scan_da_configure_event             (GtkWidget       *widget,
                                        GdkEventConfigure *event,
                                        gpointer         user_data)
{
  struct PLOT_WINDOW *pw;

  if(debug)
  {
    st_print("Window Configure Called\n");
  }

  if(widget->name)
  {
    pw = findPlotWin(widget->name);

    if(pw)
    {
      if(pw->pixmap)
      {
        if(debug)
        {
          st_print("Window Configure Called; Pixmap Destroyed\n");
        }
        gdk_pixmap_unref(pw->pixmap);
      }

      pw->pixmap = gdk_pixmap_new(widget->window,
                                  widget->allocation.width,
                                  widget->allocation.height,
                                  -1);

      gdk_draw_rectangle(pw->pixmap,
                         widget->style->black_gc,
                         TRUE,
                         0, 0,
                         widget->allocation.width,
                         widget->allocation.height);
    }
    else
    {
      fprintf(stderr, "Unable to locate widget matching %s\n", widget->name);
    }
  }
  else
  {
    fprintf(stderr, "Widget Name NOT set\n");
  }

  return FALSE;
}


/* Redraw the screen from the backing pixmap */
gboolean
on_scan_da_expose_event                (GtkWidget       *widget,
                                        GdkEventExpose  *event,
                                        gpointer         user_data)
{
  struct PLOT_WINDOW *pw;

  if(debug)
  {
    st_print("Window Expose Called\n");
  }

  if(widget->name)
  {
    pw = findPlotWin(widget->name);

    if(pw)
    {
      gdk_draw_pixmap(widget->window,
                      widget->style->fg_gc[GTK_WIDGET_STATE (widget)],
                      pw->pixmap,
                      event->area.x, event->area.y,
                      event->area.x, event->area.y,
                      event->area.width, event->area.height);
    }
    else
    {
      fprintf(stderr, "Unable to locate widget matching %s\n", widget->name);
    }
  }
  else
  {
    fprintf(stderr, "Widget Name NOT set\n");
  }


  return FALSE;
}


gboolean
on_scan_da_button_press_event          (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{
  char tbuf[256];
  struct PLOT_WINDOW *pw;

//  st_report("on_scan_da_button_press_event():");

  if(widget->name)
  {
    pw = findPlotWin(widget->name);

    if(pw)
    {
      last_pw = pw;

      if(pw->head.scan > 0.0)
      {
        sprintf(tbuf, "get %.2f; show; print el\n", pw->head.scan);
        sock_send(helper, tbuf);
      }
    }
    else
    {
      st_report("on_scan_da_button_press_event(): No plto window found for %s", widget->name);
    }
  }
  else
  {
    st_report("on_scan_da_button_press_event(): Widget has no name");
  }

  return FALSE;
}


void
on_ignore_but_pressed                  (GtkButton       *button,
                                        gpointer         user_data)
{
  char tbuf[256];

  st_fail("Got Ignore Button Press event");

  if(last_pw)
  {
    sprintf(tbuf, "ignore %.2f\n", last_pw->head.scan);
    sock_send(helper, tbuf);
  }
  else
  {
    st_fail("You must click on a plot first");
  }
}


void
on_restore_but_pressed                 (GtkButton       *button,
                                        gpointer         user_data)
{
  char tbuf[256];

  printf("Got Restore Button Press event\n");

  if(last_pw)
  {
    sprintf(tbuf, "ignore -%.2f\n", last_pw->head.scan);
    sock_send(helper, tbuf);
  }
  else
  {
    st_fail("You must click on a plot first");
  }

}


void
on_quit_but_pressed                    (GtkButton       *button,
                                        gpointer         user_data)
{
//  printf("Got Quit Button Press event\n");

  exit(0);
}


void
on_bscan_but_clicked                   (GtkButton       *button,
                                        gpointer         user_data)
{
  char tbuf[256];

  printf("Got bscan Button Press event\n");

  if(last_pw)
  {
    sprintf(tbuf, "bscan %.2f\n", last_pw->head.scan);
    sock_send(helper, tbuf);
  }
  else
  {
    st_fail("You must click on a plot first");
  }
}


void
on_escan_but_clicked                   (GtkButton       *button,
                                        gpointer         user_data)
{
  char tbuf[256];

  printf("Got escan Button Press event\n");

  if(last_pw)
  {
    sprintf(tbuf, "escan %.2f\n", last_pw->head.scan);
    sock_send(helper, tbuf);
  }
  else
  {
    st_fail("You must click on a plot first");
  }
}


void
on_reload_but_clicked                  (GtkButton       *button,
                                        gpointer         user_data)
{
  printf("Got Refresh Button Press event\n");

  loadScans();
}

