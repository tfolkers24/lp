#include <gtk/gtk.h>

gboolean
on_main_window_delete_event            (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

gboolean
on_scan_da_configure_event             (GtkWidget       *widget,
                                        GdkEventConfigure *event,
                                        gpointer         user_data);

gboolean
on_scan_da_expose_event                (GtkWidget       *widget,
                                        GdkEventExpose  *event,
                                        gpointer         user_data);

gboolean
on_scan_da_button_press_event          (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

gboolean
on_scan_da_key_press_event             (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data);

gboolean
on_scan_da_focus_in_event              (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data);

gboolean
on_scan_da_focus_out_event             (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data);

void
on_scan_da_grab_focus                  (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_ignore_but_pressed                  (GtkButton       *button,
                                        gpointer         user_data);

void
on_restore_but_pressed                 (GtkButton       *button,
                                        gpointer         user_data);

void
on_quit_but_pressed                    (GtkButton       *button,
                                        gpointer         user_data);

void
on_bscan_but_clicked                   (GtkButton       *button,
                                        gpointer         user_data);

void
on_escan_but_clicked                   (GtkButton       *button,
                                        gpointer         user_data);

void
on_reload_but_clicked                  (GtkButton       *button,
                                        gpointer         user_data);
