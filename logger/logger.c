/*
   This is the spectral logging daemon.
   It runs on Linux. It is intended to log all system messages.
   It will place messages into a specified logfile using the loglib and
   socklib libraries.

*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <math.h>

#include "sdd.h"
#include "header.h"
#include "caclib_proto.h"


int main(argc,argv)
int argc;
char *argv[];
{
//  struct SOCK *master;
  int len;
  char message[2048], logname[80], *cp, session[256], logdir[512];

  setCactusEnvironment();

  cp = getenv("LPSESSION");

  if(cp)
  {
    strcpy(session, cp);
  }
  else
  {
    strcpy(session, "12345");
  }

  sprintf(logdir, "LOGDIR=/tmp/%s/", session);
  putenv(logdir);

//  setpgid((pid_t)0, (pid_t)0);                 /* divorce myself from parents */

  strcpy(logname, "logger");

//  putenv("LOGDIR=/tmp");
  smart_log_open(logname, 3);

  sock_bind("LINUXPOPS_LOGGER"); 
//  master = sock_connect("LINUXPOPS_MASTER");

  log_msg("LP_LOGGER: Started");

  cp = getenv("MAILBOXDEFS");
  log_msg("MAILBOXDEFS: %s", cp);

  while(1) 
  {
    bzero(message, sizeof(message));

    switch(sock_sel( message, &len, NULL, 0, 1, 0)) 
    {
      case -1:
        break;

      case -2:
        exit(1);
        break;

      default:
        deNewLineIt(message);

        if(strstr(message, "lp_exit"))
        {
          log_msg("Exiting");

          exit(0);
        }

        log_msg("%s %s", sock_name(last_msg()), message);

        break;
    }
  }

  return(0);
}
