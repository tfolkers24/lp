#ifndef VXWORKS

#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <errno.h>
#include <string.h>

#else

#include   "vxWorks.h"
#include   "stdioLib.h"
#include   "systime.h"
#include   "ioLib.h"
#include   "socket.h"
#include   "inetLib.h"
#include   "in.h"
#include   "netinet/tcp.h"
static char *strtok();

#endif

#include "mbdef.h"

static int init_dest();
static int destflag = 0;
static struct DEST mailboxes[MAXDEST];

static struct DEST def_mailboxes[MAXDEST] = {
  1710, "localhost", "MONITOR", 0, 
  1712, "localhost", "RAMBO", 0,
  1714, "localhost", "EXEC", 0,
  1716, "localhost", "RTDATA", 0,
  1717, "localhost", "STATUS", 0,
  1726, "localhost", "COMPOSE", 0,
  1727, "localhost", "EXECTEST", 0,
  1733, "localhost", "SOUND", 0,
  1743, "localhost", "OBSERVER", 0,
  1746, "localhost", "GLOB", 0,
  1747, "localhost", "OBSTOOL", 0,
  1759, "localhost", "DATASERV", 0,
  0,    "",          "",	 0
};


/* Return a pointer to the struct DEST for mailbox `name' (or NULL if none) */

struct DEST *find_dest(name)
char *name;
{
  int i;

  if( destflag == 0 )
    init_dest();

  for( i=0; i<MAXDEST; i++ )
    if( mailboxes[i].dname[0] == '\0' )
      break;
    else if( strcmp( mailboxes[i].dname, name ) == 0 )
      return( &mailboxes[i] );

  for( i=0; i<MAXDEST; i++ )
    if( def_mailboxes[i].dname[0] == '\0' )
      break;
    else if( strcmp( def_mailboxes[i].dname, name ) == 0 )
      return( &def_mailboxes[i] );

  return(NULL);
}


struct DEST *get_dest(i)
int i;
{
  if( destflag == 0 )
  {
    init_dest();
  }

  if(i < MAXDEST)
  {
    if(mailboxes[i].dname[0] != '\0')
    {
      return( &mailboxes[i] );
    }
  }

  return(NULL);
}


/*
 * Return a pointer to the first struct DEST for a mailbox with a port number 
 * `p' (or NULL if none).  If p=0, continue the search for the previous port 
 * number beginning after the last one found (or NULL if no more found)
 */

struct DEST *find_port(p)
int p;
{
  static int port, i_standard, i_default;

  if (p) {
    i_standard = 0;		/* Start from the beginning */
    i_default = 0;
    port = p;			/* And remember the port number */
  }

  if( destflag == 0 )
    init_dest();

  for( ; i_standard<MAXDEST; i_standard++ )
    if( mailboxes[i_standard].dname[0] == '\0' )
      break;
    else if( mailboxes[i_standard].dport == port )
      return( &mailboxes[i_standard++] );

  for( ; i_default<MAXDEST; i_default++ )
    if( def_mailboxes[i_default].dname[0] == '\0' )
      break;
    else if( def_mailboxes[i_default].dport == port )
      return( &def_mailboxes[i_default++] );

  return(NULL);
}


/* Read the mailboxdefs file and store in mailboxes[] */
static int init_dest()
{
  static char delim[] = ", \t\n";
  struct DEST *d;
  FILE *fd;
  int i, count;
  char buf[256], *q, *p, *cp;

  p = "/home/corona/cactus/mailboxdefs";

  cp = getenv("MAILBOXDEFS");

  if(cp)
  {
    p = cp;
  }

  bzero( &mailboxes[0], sizeof(struct DEST)*MAXDEST);

  if( (fd = fopen( p, "r" ) )== NULL ) 
  {
    destflag = 1;
    return(1);
  }

  count = 0;
  while( fgets(buf, 256, fd ) != NULL )
  {
    if( buf[0] == '#' || buf[0] == '\n' )
      continue;

    if( (i = atoi(strtok( buf, delim ))) == 0 )
      continue;

    mailboxes[count].dport = abs(i);	/* Ports are always positive */
    mailboxes[count].raw = (i < 0);	/* Negative port means a `raw' socket */
    strncpy(mailboxes[count].host,  strtok(NULL, delim), MAXNAME );
    strncpy(mailboxes[count].dname, strtok(NULL, delim), MAXNAME );

    if( ++count >= MAXDEST ) {
      printf("too many entries in mailboxdefs\n");
      break;
    }
  }

  fclose(fd);
  destflag = 1;

  return(0);
}

/*
    bind before connect to avoid known OS hang bug 
*/

int bind_any(fd)
int fd;
{

#ifndef VXWORKS
  int port, ret;
  struct sockaddr_in sin;

  port = 30000;

  while(1) {
    bzero( &sin, sizeof( struct sockaddr_in ) );
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = INADDR_ANY;
    sin.sin_port = htons(port);

    if((ret = bind(fd, (struct sockaddr *)&sin, sizeof (sin)))>= 0)
      break;

    if( port-- < 1800 || errno != EADDRINUSE )
      break;
  }
  if( ret < 0 )
    perror("bind_any");
#endif
}

#ifdef VXWORKS

/* 
  Jeff version of the unix standard call
*/

static char *strtok(s, delim)
char *s, *delim;
{
  static char *last = NULL, lastchar;
  char *d, *start, looking;

  if( s == NULL )  {
    if( last == NULL )
      return(NULL);
    s = last;
    *s = lastchar;
  }

  if( s == NULL || delim == NULL || *delim == '\0' )
    return(NULL);

  last = start = NULL;
  looking = 0;
  d = delim;
  while(*s) {
    while(*d) {
      if( *s == *d )
        break;
      d++;
    }
    if( *d ) {
      if( looking ){ /* found it */
        last = s;
        lastchar = *s;
        *s = '\0';
        break; 
      }
    } else if( start == NULL ) {
      start = s; 
      looking = 1;
    }
    
    d = delim;
    s++;
  }

  return(start); 
}

#endif
