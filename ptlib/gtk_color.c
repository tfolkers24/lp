#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <sys/time.h>
#include <time.h>
#include <gtk/gtk.h>

#define DEFINE_GTK_COLORS 
#include "gtk_color.h"

GdkColor colors[MAX_GTK_COLORS];
GdkColormap *cmap = NULL;


gint allocColors(GtkWidget *drawable)
{
  int i;

  if(!cmap)
    cmap = gdk_colormap_get_system();
  else
    return(1);

  for(i=0;i<MAX_GTK_COLORS;i++)
  {
    if(!colors[i].pixel)
    {
      colors[i].red   = gtk_color_defs[i].red;
      colors[i].green = gtk_color_defs[i].green;
      colors[i].blue  = gtk_color_defs[i].blue;

      if(!gdk_colormap_alloc_color(cmap, &gtk_colors[i], FALSE, TRUE))
      {
        fprintf(stderr, "Unable to allocate %s\n", gtk_color_defs[i].name);
                                              /* fill in color with something */
        bcopy(&drawable->style->black, &gtk_colors[i], sizeof(GdkColor)); 
      }
    }
  }

  return(0);
}
