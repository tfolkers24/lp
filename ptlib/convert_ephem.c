#include <stdio.h>

/* 
   Cactus File @(#)convert_ephem.c	1.6
           SID 1.6
          Date 10/30/96
   
  This program converts an epehemeris of the below format to the NRAO 12 meter
  control system ephemris format.

  Usage:
  convert_ephem Objectname < belowformatfile > standardformatfile

                     R.A. (apparent) DEC.  AIR-   DISTANCE               VELOCIT
Y    TRACKING RATES
  UT DATE / TIME   HR MN  SEC  DEG AM ASEC MASS  SUN  EARTH  AZ   ALT   HELIO TO
PO    R.A.    DEC.

1995 Jun 08 00:00s 03 49 34.74 +20 08 55.7 4.34 1.140 2.070 285.8 +13.0  +9.9 +1
0.6 +0.0404 +0.0097
1995 Jun 08 00:10s 03 49 36.36 +20 09 01.6 5.09 1.140 2.070 287.0 +10.9  +9.9 +1
0.6 +0.0405 +0.0097
1995 Jun 08 00:20s 03 49 37.98 +20 09 07.4 6.13 1.140 2.070 288.2  +8.9  +9.9 +1
0.6 +0.0405 +0.0097

*/
static char SccsId[] = "@(#)convert_ephem.c	1.6\t10/30/96";

char *strtok();

int geti();
double getf();
double get_mjd();
int get_mon();

static char delim[] = " \t:";

main(argc, argv)
int argc;
char *argv[];
{
  int yr, mon, day, hour, min, ra_h, ra_min, hp_flag;
  int dec_deg, dec_min, outlines;
  double ra_sec, dec_sec;
  double nrao_jd;
  double topo_vel;
  double ra, dec;
  double hp, gd;
  double negdec;
  char *p, buf[256];
  char *name;
 
  if( argv[1] ) 
    name = argv[1];
  else
    name = "object";

  hp_flag = ( argv[2] && strcmp(argv[2], "-hp" )==0 );

  printf("#\n# Converted ephemeris of %s\n#\n", argv[1] );

  outlines = 0;
  while( gets( buf ) ) {
    yr = geti( buf );
    if( yr < 1990 || yr > 2005 )
      continue;
    
    mon  = get_mon(NULL);

    day  = geti(NULL);
    hour = geti(NULL);
    min  = geti(NULL);
    nrao_jd = get_mjd( yr, mon, day, hour, min );
    ra_h = geti(NULL);
    ra_min  = geti(NULL);
    ra_sec  = getf(NULL);
    dec_deg = geti(NULL);
    dec_min  = geti(NULL);
    dec_sec = getf(NULL);
    getf(NULL);
    getf(NULL);
    gd = getf(NULL);
    getf(NULL);
    getf(NULL);
    getf(NULL);
    topo_vel = getf(NULL);
    ra  = ra_h + ra_min/60.0 + ra_sec/3600.0;
    ra *= 15.0; /* hours to degrees */
    negdec = dec_deg < 0 ? -1.0 : 1.0;
    dec = abs(dec_deg) + dec_min/60.0 + dec_sec/3600.0;
    dec *= negdec;
    if( hp_flag )
      hp = 0.0;
    else
      hp = 8.794148 / gd;
    
    printf("EPHEM 11 D %11.6f D %11.6f J %14.8f %10.2f %10.3f %s\n",
      ra, dec, nrao_jd, hp, topo_vel, name );
    
    outlines++;
    name = "";
  }
  if( outlines > 2000 )
    fprintf( stderr, "WARNING: Ephemeris greater than 2000 lines\n");
}


geti(s)
char *s;
{
  char *p;
  char *strtok();

  p = strtok(s, delim);
  if( p )
    return(atoi(p));
  else
    return(0);
}

double getf(s)
char *s;
{
  char *p;
  double atof();
  char *strtok();

  p = strtok(s, delim);
  if( p )
    return(atof(p));
  else
    return(0);
}

/* boy is this general */

get_mon(s)
char *s;
{
  char *p;

  p = strtok(s, delim);
  if( strncmp("Jan", p, 3 ) == 0 )
    return(1);
  else if( strncmp("Feb", p, 3 ) == 0 )
    return(2);
  else if( strncmp("Mar", p, 3 ) == 0 )
    return(3);
  else if( strncmp("Apr", p, 3 ) == 0 )
    return(4);
  else if( strncmp("May", p, 3 ) == 0 )
    return(5);
  else if( strncmp("Jun", p, 3 ) == 0 )
    return(6);
  else if( strncmp("Jul", p, 3 ) == 0 )
    return(7);
  else if( strncmp("Aug", p, 3 ) == 0 )
    return(8);
  else if( strncmp("Sep", p, 3 ) == 0 )
    return(9);
  else if( strncmp("Oct", p, 3 ) == 0 )
    return(10);
  else if( strncmp("Nov", p, 3 ) == 0 )
    return(11);
  else if( strncmp("Dec", p, 3 ) == 0 )
    return(12);
  else
    return(0);
}

/* get the nrao mjd (1989) */

double get_mjd( yr, mon, day, hour, min ) 
int yr, mon, day, hour, min;
{
  double mjd;
  int i;
  static int daysinmon[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

  mjd = (double)leapdays(1989, yr);

  if( yr != 2000 && yr %4 == 0 && mon > 2 )
    mjd += 1.0;
  
  mon--;
  for( i=0; i<mon; i++ )
    mjd += daysinmon[i];

  day--;
  mjd += (double)day + (double)hour/24.0 + (double)min/24.0/60.0;
  return(mjd+47527.0);
}

/* Get integer no. of days between Jan 0 start year and Jan 0 end year */

leapdays( start, end )
int start, end;
{
  int ndays, diff;

  diff = end - start;

  if( diff <= 0 )
    return(0);

  ndays = diff*365.0 + diff/4.0;

  if(start <= 2000 && end > 2000 )
    ndays--;    /* 2000 is not a leap year */

  return(ndays);
}

