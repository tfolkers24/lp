#include <stdio.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>

#include "mbdef.h"
#include "sock.h"

#include "caclib_pt_proto.h"


/*
      This includes the definitions of sock_write and sock_send for the 
      non-xview users
      xview support is obtained by using the library in sockxviewlib.c
      and this module will not be linked.
*/

extern struct SOCK sock_kbd;
extern int last_conn;
extern int logCallingSockFlag;

int sockWriteNoBitch = 0;

int sock_send(bs, s, message )
struct BOUND *bs;
struct SOCK *s;
char *message;
{
  return(sock_write(bs, s, message, strlen(message)));
}

/*
  like sock_send but takes pointer and count as in write
*/

int sock_write( bs, s, message, l )
struct BOUND *bs;
struct SOCK *s;
char *message;
int l;
{
  int ret, err, len, flag = 0;
  unsigned long count;
  unsigned long net;
  char *name;

  if( (long)s == -1 || !message || l <= 0 )
  {
    return(0);
  }

  if(!s)
  {
    if(!logCallingSockFlag)
    {
      log_msg("no mailbox for: %s", message );
    }

    return(0);
  }

  while( flag < 2 )
  {
    if( s->fd < 0 )
    {
      if( strcmp( s->dest->dname, "ANONYMOUS") == 0 )
      {
        return(0);
      }

      if( (s->fd = socket(s->sin.sin_family, SOCK_STREAM, 0 )) <0) 
      {
	if(s->dest->dname)
        {
          char buf[80];

          sprintf(buf,"sock_write, socket(%s)", s->dest->dname);
          log_perror(buf);
	}
	else
        {
          log_perror("sock_write, socket");
        }

        return(0);
      }

      bind_any(s->fd);

      if( connect( s->fd, (struct sockaddr *)&s->sin, sizeof(struct sockaddr_in) ) <0 )
      {
        close(s->fd);
        s->fd = -1;

	if(s->dest->dname)
        {
          if(!sockWriteNoBitch)
          {
            char buf[80];

            sprintf(buf,"sock_write, connect(%s)", s->dest->dname);
            log_perror(buf);
          }
	}
	else
        {
          log_perror("sock_write, connect");
        }

        return(0);
      }

	/* It's not a raw socket.  Send the mailbox name */
      if( ! s->dest->raw && ! bs->dest->raw)
      {
	if( bs->bind_fd >0 ) 
        {
	  count = strlen(bs->dest->dname);
	  name =  bs->dest->dname;
	} 
	else /* if he never bound then send ANONYMOUS */
	{
	  count = 9;
	  name = "ANONYMOUS";
	}

	net = htonl(count);

	if( (ret = write( s->fd, &net, sizeof(unsigned int)))>0 )
        {
	  ret = write( s->fd, name, count);
        }
      }
    }

    count = l;

	/* It's not a raw socket.  Send the byte count first */
    if(!s->dest->raw && !bs->dest->raw)
    {
      net = htonl(count);
      ret = write( s->fd, &net, sizeof(unsigned int));
    } 
    else
    {
      ret = 1;			/* Raw socket always writes the data */
    }

    if(ret > 0) 
    {
      int ct, ix;

      ct = count;
      ix = 0;

      while( ct > 0 && ret > 0 )
      {
        ret = write( s->fd, &message[ix], ct);

        ct -= ret;
        ix += ret;
      }
    }

/*
   this checks to see if vxworks box (or sun) has rebooted 
*/
    usleep(1);
    len = 4;
    err = 0;

    if( getsockopt( s->fd, SOL_SOCKET, SO_ERROR, (char *)&err, &len ) < 0 || err != 0 )
    {
      ret = count + 1;
    }

    if( ret != count ) 
    {
      if(ret <0 )
      {
	if(s->dest->dname) 
        {
          char buf[80];

          sprintf(buf,"sock_write(%s)", s->dest->dname);
          log_perror(buf);
	}
	else
        {
          log_perror("sock_write");
        }
      }

      if(!logCallingSockFlag)
      {
        log_msg("closing out mailbox: %s", s->dest->dname );
      }

      close( s->fd);

      s->fd = -1;
      flag += 1;
    } 
    else
    {
      break;
    }
  }

  return(1);
}
