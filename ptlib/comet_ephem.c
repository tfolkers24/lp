/*
 * Cactus File @(#)comet_ephem.c	1.5
 *         SID 1.5
 *        Date 07/23/97
 */
static char SccsId[] = "@(#)comet_ephem.c	1.5\t07/23/97";

/* COMET_EPHEM -- 
 * Usage:	comet_ephem [-initials] [input file [output file root name]]
 *
 * Read a file containing a comet ephemeris as produced by Yeoman's JPL program
 * and convert it to a form which can be loaded into Tracker-Servo.
 *
 * If no input file is specified, the user is prompted for the file name.
 * If no output file is specified, the user is prompted for the root name of 
 * the file.  ".eph" will be added to the root name specified.  The output is 
 * ordinarily written to the current working directory.  This ought to be the
 * same directory when the OBS user puts his catalog files: 
 *   		/home/obs/<initials>
 * where <initials> are the observer's initials.  If these initials are 
 * specified after a "-" on the command line, comet_ephem will write the 
 * file in the corresponding directory.   
 * Example:
 * 			comet_ephem -abc mueller.yoeman mueller_1
 *
 * will read file mueller.yoeman and write the corresponding Tracker-Servo 
 * ephemeris in /home/obs/abc/mueller_1.eph.
 *
 * The input file must contain topocentric or geocentric apparent R.A. and Dec.
 * (or apparent topocentric azimuth and elevation) and radial velocity and must 
 * be evenly spaced in time.
 *
 * The following is the first 4 lines of a sample file:
 *	Mueller
 *	  YR  MN DY  HR   J.D.(UT)    R.A. 1950.0 DEC.      R.A.  DATE  DEC.    DELTA  DELTAD    R    RDOT   TMAG  NMAG  THETA   BETA
 *	 1992  3 16  0.0  2448697.5  0 32.226  -11 56.52   0 34.357  -11 42.60  1.0038  23.90 0.2779 -42.63  0.00  0.00  15.97  80.16
 *	 1992  3 16  1.0  2448697.5  0 31.954  -11 57.12   0 34.085  -11 43.20  1.0044  23.94 0.2768 -42.51  0.00  0.00  15.91  80.07
 *
 * The first line contains the name of the comet.  If the comet name is 
 * followed by a number between 10 and 20 inclusive, that is the ephemeris 
 * table number in Tracker-Servo where the ephemeris will be loaded.  (Default
 * is 10.)  The second line optionally contains the column headings.  (It is 
 * skipped.)  The remaining lines contain the coordinates, etc (free format):
 * J.D.= JULIAN DATE(UNIVERSAL TIME)
 * R.A. AND DEC. - 1950.0 = TOPOCENTRIC RIGHT ASCENSION AND DECLINATION REFERRED TO THE MEAN EQUATOR AND EQUINOX OF 1950.0
 * R.A. AND DEC. - DATE = TOPOCENTRIC RIGHT ASCENSION AND DECLINATION REFERRED TO THE APPARENT EQUATOR AND EQUINOX OF DATE,
 *                        R.A. IN HOURS AND TIME MINUTES, DEC. IN DEGREES AND ARC MINUTES
 * DELTA= TOPOCENTRIC DISTANCE OF OBJECT IN A.U.
 * DELTAD = D(DELTA)/DT, IN KM/SEC
 * R= HELIOCENTRIC DISTANCE OF OBJECT IN A.U.
 * RDOT = D(R)/DT, IN KM/SEC
 * TMAG= TOTAL   MAGNITUDE =  0.0 +  0.00*DLOG10(DELTA) +  0.00*DLOG10(R)
 * NMAG= NUCLEAR MAGNITUDE =  0.0 +  0.00*DLOG10(DELTA) +  0.00*DLOG10(R) + 0.00*BETA
 * THETA= SUN-EARTH-OBJECT ANGLE IN DEGREES
 * BETA= SUN-OBJECT-EARTH ANGLE IN DEGREES
 *
 * (The only columns COMET_EPHEM needs are YR, MN, DY, HR, R.A. DATE, DEC. DATE,
 * and DELTAD, but something must occupy the unused columns to the left of 
 * DELTAD so that the correct numbers are found).
 *
 * If the coordinates are geocentric coordinates of date, they should be put 
 * in the columns containing topocentric R. A. and Dec. of date above, the 
 * DELTA column should contain the geocentric distance in A.U., and a line 
 * beginning with the word, GEOCENTRIC, should preceed the comet name.
 *
 * If the coordinates are azimuth and elevation, they should be put in the 
 * columns where R.A. and Dec. are normally read and they should both be 
 * expressed in degrees and arc minutes, and a line beginning with the word, 
 * AZEL, should be inserted before the comet name.
 */
 
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>

#define NONE 1.e-38
#define abs(x) (x) < 0 ? -(x) : (x)

main(argc, argv)
int argc;
char *argv[];
{
    char *mnames[12] = {"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", 
    			"SEP", "OCT", "NOV", "DEC"};
    static int daycnt[] = {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 
    									334};
    static int daymon[] = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 999};
    double millenium = 4078.;				/* Mar 1, 2000 */

    char inname[200], outname[200], name[200], line[200], line0[200], 
			firstline[120], *token, 	path[240], initials[4], *ptr, 
			*sgn, delim[] = " \t\n";
    FILE *infile, *outfile;
    int i, n, np, mon, yr, table, lenpath;

    /* (Decode the columns marked non-zero) */
    int col[15] = {1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1};

    double ra, dec, az, el, hour, month, day, year, rah, ram, decd, decm, 
	    delta, deltad, eptime, azd, azm, eld, elm, hpar, lastday, lasthour;
    double *par[10] = {&year, &month, &day, &hour, &rah, &ram, &decd,
	    &decm, &delta, &deltad};
    char *parname[10] = {"year", "month", "day", "hour", "R.A. hours", 
		"R.A. minutes", "dec. degrees", "dec. minutes", "distance",
		"radial velocity"};
    int geocentric = 0, azel = 0;

    if (argc > 4) {
	fprintf(stderr,
	    "Usage: comet_ephem [-initials] [input-file [output-file-root]]\n");
	exit();
    }

    inname[0] = -1;
    outname[0] = -1;
    initials[0] = -1;

    for (i=1; i < argc; i++) {
	if (argv[i][0] == '-') {
	    argv[i]++;
	    if (strlen(argv[i]) > 3) {
		fprintf(stderr,"Bad initials: %s (more than 3 characters)\n", 
								       argv[i]);
		exit();
	    }
	    strcpy(initials, argv[i]);
	    continue;
	}
	if (inname[0] < 0)
	    strcpy(inname, argv[i]);
	else
	    strcpy(outname, argv[i]);
    }

    if (initials[0] >= 0) {
	sprintf(path, "/home/obs/%s/", initials);
	lenpath = strlen(path);
    } else
	lenpath = 0;

    while (1) {					/* Do until "break" */
	if (inname[0] == -1) {
	    clearerr(stdin);
	    fprintf(stderr, "Input file name: ");
	    if (fgets(line, 199, stdin) == NULL || 
				           sscanf(line, "%199s", inname) != 1) {
		inname[0] = -1;
		continue;
	    }
	}

	infile = fopen(inname, "r");
	if (infile == NULL) {
	    fprintf(stderr, "Could not open %s\n", inname);
	    inname[0] = -1;
	} else
	    break;
    }

    while (1) {					/* Do until "break" */
	if (outname[0] == -1) {
	    clearerr(stdin);
	    fprintf(stderr, "Ephemeris output file: ");	    
	    if (fgets(line, 199, stdin) == NULL || 
					  sscanf(line, "%199s", outname) != 1) {
		    outname[0] = -1;
		    continue;
	    }
	}
	strcpy(path + lenpath, outname);
	strcpy(path + lenpath + strlen(outname), ".eph");

	outfile = fopen(path, "w");
	if (outfile == NULL) {
	    fprintf(stderr, "Could not open %s\n", path);
	    outname[0] = -1;
	} else
	    break;
    }						/* Try for output file again */

    for (token=NULL; token == NULL; ) {		/* Read object name */
	if (fgets(line, 199, infile) == NULL) {
	    fprintf(stderr, "Unexpected end-of-file\n");
	    exit();
	}
	strcpy(name, line);
        strtok(line, delim);
	strupr(line);
	if (!geocentric && !azel && !strcmp(line, "GEOCENTRIC")) {
	    geocentric = 1;			/* Geocentric coordinates */
	    continue;
	} else if (!geocentric && !azel && !strcmp(line, "AZEL")) {
	    azel = 1;				/* Az-el coordinates */
	    par[4] = &azd;
	    par[5] = &azm;
	    par[6] = &eld;
	    par[7] = &elm;
	    parname[4] = "azimuth degrees";
	    parname[5]= "azimuth minutes";
	    parname[6] = "elevation degrees";
	    parname[7]= "elevation minutes";
	    continue;
	} else
	    token = strtok(name, delim);
    }

    /* Look for a table number */
    token = strtok(NULL, delim);
    if (token != NULL) {
	table = strtol(token, &ptr, 10);	/* Decode the table number */
	if (table < 10 || table > 20 || strlen(ptr) != 0)
	    table = 10;				/* Illegal table number */
    } else
	table = 10;				/* Default table number */
    fgets(line, 199, infile);			/* Skip the heading */

    if (azel) {
	printf("                      ----------- Apparent ----------\n");
        printf("  UT Date    Hour     Azimuth   Elevation  Radial Vel.\n");
    } else if (geocentric) {
	printf("                     --------------- Geocentric --------------\n");
	printf("  UT Date    Hour    R.A. (Date)  Dec.   Distance  Radial Vel.\n");
    } else {
	printf("                     --------- Topocentric ---------\n");
	printf("  UT Date    Hour    R.A. (Date)  Dec.   Radial Vel.\n");
    }

    for (n=0; ; ) {
	if (fgets(line, 199, infile) == NULL)
	    break;				/* Done */
	strcpy(line0, line);			/* Save a copy */
	token = strtok(line, delim);
	for (i=0; i < 10; i++)
	    *par[i] = NONE;			/* Set defaults */

	/* Decode the columns marked */
	for (i=0, np=0; i < 15 && token != NULL; i++) {
	    if (i == 11)
		sgn = token;			/* Remember declination sign */
	    if (!strcmp(token, "+") || !strcmp(token, "-")) {
		    /* Isolated sign; advance to next item */
		    if ((token = strtok(NULL, delim)) == NULL)
			break;			/* The end */
	    }
	    if (col[i]) {
		*par[np++] = strtod(token, &ptr);
		if (strlen(ptr) != 0)
		    *par[np-1] = NONE;		/* Illegal number */
	    }
	    token = strtok(NULL, delim);
	}

	if (np == 0)
	    continue;				/* Blank line */
	else if (np < 10) {
	    fprintf(stderr, "Too few items on line; ignoring:\n%s", line0);
	    continue;
	}
	if (!geocentric)
	    delta = 1.e10;	/* Don't need delta if not geocentric coords */
	
	for (i=0, np=0; i < 10; i++) {
	    if (*par[i] == NONE)
		fprintf(stderr, "Error parsing %s\n", parname[i]);
	    else
		np++;
	}
	if (np != 10) {
	    fprintf(stderr, "in: %sLine ignored\n", line0);
	    continue;
	}

	if (month < 1 || month > 12) {
	    fprintf(stderr, "Illegal month number %.0f in: %sLine ignored\n", 
    								month, line0);
	    continue;
	}
	mon = month - 1.;

	if (day < 1 || day > daycnt[mon]) {
	    fprintf(stderr, "Illegal day of month %.0f in: %sLine ignored\n", 
    								day, line0);
	    continue;
	}

	yr = year;
	if (yr < 1990 || yr > 2050) {
	    fprintf(stderr, "Illegal year %d in: %sLine ignored\n", yr, line0);
	    continue;
	}
	if (name[0]) {
	    /* Output first line */
	    sprintf(firstline, 
			"# Tracker ephemeris for %s, %6.3f H %2.0f %s %d to ", 
			name, hour, day, mnames[mon], yr);
	    fprintf(outfile, "%-119s\nCLEARSOURCE %d\n", firstline, table);
	}

	/* Get days since Jan 0, 1989 */
        eptime = daycnt[mon] + day + 365*(yr - 1989) + (yr - 1989)/4 +
    								hour/24.;
	/* If "yr" is a leap year & mon > Feb. */
	if (!(yr % 4) && mon > 1) eptime += 1.;

	if (eptime > millenium) eptime -= 1.;	/* Year 2000 is not a leap year */

	if (azel) {
	    az = azd + azm/60.;
	    el = eld + elm/60.;
	    if (az < 0. || az > 360.) {
		fprintf(stderr, "Illegal azimuth %.4f in: %sLine ignored\n", 
    								az, line0);
		continue;
	    }
	    if (el < 0. || el > 90.) {
		fprintf(stderr, "Illegal elevation %.4f in: %sLine ignored\n", 
    								el, line0);
		continue;
	    }
	    printf("%2.0f %s %d %6.3f %4.0f %6.3f %3.0f %6.3f %9.2f\n", day, 
		    mnames[mon], yr, hour, azd, azm, eld, elm, deltad);
	    fprintf(outfile, "EPHEM %d 0 0 D %.4f D %.4f J %.6f 0 %.2f %s\n",
				table, az, el, eptime, deltad, name);

	} else {
	    ra = 15.*rah + ram/4.;
    	    decd = abs(decd);
	    dec = decd + decm/60.;
	    if (*sgn == '-')
		dec = -dec;
	    else
		sgn = " ";

	    if (ra < 0. || ra > 360.) {
		fprintf(stderr, "Illegal R.A. %.4f in: %sLine ignored\n", 
								ra/15., line0);
		continue;
	    }
	    if (dec < -60. || dec > 90.) {
		fprintf(stderr, "Illegal declination %.4f in: %sLine ignored\n",
								 dec, line0);
		continue;
	    }

	    if (geocentric)
		printf("%2.0f %s %d %6.3f %2.0f %6.3f %c%2.0f %6.3f %9.5f %9.2f\n",
			day, mnames[mon], yr, hour, rah, ram, *sgn, decd, decm, 
    			delta, deltad);
	    else
		printf("%2.0f %s %d %6.3f %2.0f %6.3f %c%2.0f %6.3f %9.2f\n", 
			day, mnames[mon], yr, hour, rah, ram, *sgn, decd, decm, 
    			deltad);

	    /* Convert Horizontal Parallax into arcsec */
	    hpar = 8.794148/delta;

	    fprintf(outfile, "EPHEM %d D %.4f D %.4f J %.6f %.2f %.2f %s\n",
				table, ra, dec, eptime, hpar, deltad, name);

	}
	n++;
	name[0] = '\0';
        lastday = day;
	lasthour = hour;
    }
    fflush(outfile);
    if (lseek(fileno(outfile), 0, SEEK_SET) == -1) {
	fprintf(stderr, "Could not seek beginning of output file\n");
	perror("lseek");
    } else {
	sprintf(firstline + strlen(firstline), "%6.3f %2.0f %s %d", lasthour, 
						lastday, mnames[mon], yr);
	fprintf(outfile, "%-119s\n", firstline);
    }
    fprintf(stderr, "%d positions processed\n", n);
}
