/* *** change log ***

date    who Description
------- --- ----------------------------------------------------
19nov02 twf added different time stamp with fractional seconds()
 */

/*
    This is an attempt at a logger library that will handle message logging in 
    a uniform way. The idea is to support log files in a given directory.
    When the newlog command is issued the logfiles are purged - that is
    olderlog is deleted, oldlog is renamed to olderlog and log is renamed to
    oldlog, and a new file log is created.

 Here is a brief description of the routines you can use.

    log_msg( a, b, c... ) - parameters like printf. This will log a message 
       to the log file. Log_msg will prepend the current time.
    log_perror( msg ) - Like perror, this will log a unix message 
       to the log file. Log_perror will prepend the current time.
    log_newlog() - causes the logfile to be closed and renamed. Should be 
       executed when a newlog command is received.
    log_open(name, how) - opens and initializes log file. name is the 
      full unix name.  of the logfile - e.g. /home/corona/cactus/rambo/rambo. 
      This will create files in the /home/corona/cactus/rambo directory called:
      rambo.log, rambo.oldlog and rambo.olderlog. If how is 1 also write 
      message to stderr.
    log_eventdEnable(enable) - Turns on event logging. All messages sent to
      event daemon too. (Hopefully EVENTD doesn't turn this feature on
      too or we will have a loop. This assumes that you did a sock_bind()
      first.

    There is no vxworks equivalent because you should just send a socket
    message to the appropriate logger daemon. You can start a log daemon by 
    running vxlogd

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <errno.h>
#include <ctype.h>
#include <sys/time.h>
#include <time.h>
#include <math.h>
#ifdef USE_THREADS
#include <pthread.h>
#endif

#include "loglib.h"
#include "caclib_pt_proto.h"

int loglib_raw = 0;
int log_dup2stdout = 0;

char *getNewCtime();

static char *months[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", 
			  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
};


static FILE *logfile = NULL;
static char *logname = NULL;
static int   log_how = 4;
int          sendToEventd;
int          lastEnable = 0;
int          logCallingSockFlag = 0;
struct SOCK *eventdSock;

char loglib_buffer[4096];


/*
 * how = 0: log only using ctime timestamp.
 * how = 1: log & write to stderr using ctime timestamp.
 * how = 2: log & write to stderr using no timestamp.
 * how = 3: log & write to stderr using millisec timestamps.
 * how = 4: log only using millisec timestamps.
 * how = 5: log & write to stderr using millisec timestamps + threadname.
 * how = 6: log only using millisec timestamps + threadname.
 */

int log_msg( const char *fmt, ...)
{
  FILE *out;
  time_t tm;
  int how;
  char buf[4096*2], rslt[4096*2], *t, *ctime();
  char timestr[256];
  va_list args;
#ifdef USE_THREADS
  pthread_t thID;
  char tname[80];
#endif

  if( logfile ) 
  {
    how = log_how;
    out = logfile;
  } 
  else 
  {
    how = 4;
    out = stderr;
  }
  
#ifdef USE_THREADS
  if( how == 3 || how == 4 || how == 5 | how == 6)
  {
    t = getNewCtime(0);

    thID = pthread_self();
    pthread_getname_np(thID, tname, sizeof(tname));
  }
#else
  if( how == 3 || how == 4)
  {
    t = getNewCtime(0);
  }
#endif
  else
  if( how == 2 )
  {
    t ="";
  }
  else 
  { 
    time(&tm);
    t = ctime(&tm);
    if( !t || strlen(t) == 0 )
    {
      t = " "; 
    }
    else 
    {
      strcpy( timestr, t );
      t = timestr;
      t[strlen(t)-1] = ' ';
    }
  }

  va_start(args, fmt);

  vsprintf(buf, fmt, args );

  va_end(args);

//  if(eventdSock)
//  {
//    logCallingSockFlag = 1;
//    sock_send(eventdSock, buf);
//    logCallingSockFlag = 0;
//  }

  if(log_dup2stdout)
  {
    fprintf(stdout, "%s\n", buf ); 
    fflush(stdout);
  }

  if(loglib_raw)
  {
#ifdef USE_THREADS
    if(how > 4)
    {
      sprintf(rslt, "%s (%s): %s", t, tname, buf ); 
    }
    else
#endif
    {
      sprintf(rslt, "%s%s", t, buf ); 
    }
  }
  else
  {
#ifdef USE_THREADS
    if(how > 4)
    {
      sprintf(rslt, "%s (%s): %s\n", t, tname, buf ); 
    }
    else
#endif
    {
      sprintf(rslt, "%s%s\n", t, buf ); 
    }
  }

  strcpy(loglib_buffer, rslt);

  fwrite(rslt, 1, strlen(rslt), out );

  if( how == 1 || how == 2 | how == 3 ) 
  {
    fwrite(rslt, 1, strlen(rslt), stderr );
    fflush(stderr);
  }

  fflush(out);
}

/**************************************************************/
int log_perror( msg )
char *msg;
{
  FILE *out;
  long tm;
  char err[4096], *t, *ctime();
  char timestr[256];
  int how;

  if( logfile ) {
    how = log_how;
    out = logfile;
  } else {
    how = 0;
    out = stderr;
  }
 
  if( how > 1 )
    t ="";
  else { 
    time(&tm);
    t = ctime(&tm);
    if( !t || strlen(t) == 0 )
      t = " "; 
    else {
      strcpy( timestr, t );
      t = timestr;
      t[strlen(t)-1] = ' ';
    }
  }

  sprintf( err, "%s %s, %s\n", t, msg, strerror(errno) );

  fwrite(err, 1, strlen(err), out );
  fflush(out);
  if( how ) {
    fwrite(err, 1, strlen(err), stderr );
    fflush(stderr);
  }
}

int log_eventdEnable(enable)
int enable;
{
//  if(enable == LOG_TO_LOGGER)
//  {
//    eventdSock = sock_connect("LINUXPOPS_LOGGER");
//  }

  if(!enable)
  {
    eventdSock = (struct SOCK *)NULL;
  }

  lastEnable = enable;

  return(enable);
}

 
int log_open(name, how)
char *name;
int how;
{
  char buf[4096], *cp, *getenv();
  FILE *fopen();

  if(logname) 
  {
    log_msg("log_open already set");

    return(0);
  }

  cp = getenv("LOGDIR");
  if(cp)
  {
    sprintf(buf, "%s/%s.log", cp, name);
  }
  else
  {
    sprintf(buf, "%s.log", name);
  }

  if((logfile = fopen( buf, "a+" )) == 0 ) 
  {
    log_perror("log_open");

    return(0);
  }

  logname = name;
  log_how = how;

  return(1);
}



int newlog2datefile()
{
  char log[4096], oldlog[4096], buf[4096], *cp;
  FILE *fopen();
  char *getDate();

  log_msg("--NewLog--");

  cp = getenv("LOGDIR");

  if(cp)
  {
    sprintf(log, "%s/%s.log", cp, logname);
    sprintf(oldlog, "%s/%s.%s", cp, logname, getDate(0));
  }
  else
  {
    sprintf(log, "%s.log", logname);
    sprintf(oldlog, "%s.%s", logname, getDate(0));
  }


  if( logfile )
    fclose(logfile);
  else {
    log_msg("newlog - no logfile");
    return(1);
  }
  sprintf(buf, "touch %s", log);
  system(buf);

  sprintf(buf, "mv %s %s", log, oldlog);
  system(buf);

  if( (logfile = fopen( log, "a+" )) == NULL )
  {
    log_perror("log_newlog");
    return(1);
  }

  return(0);
}


int log_newlog()
{
  char log[4096], oldlog[4096], olderlog[4096], oldestlog[4096], buf[4096], *cp;
  FILE *fopen();

  log_msg("--NewLog--");

  cp = getenv("LOGDIR");

  if(cp)
  {
    sprintf(log, "%s/%s.log", cp, logname );
    sprintf(oldlog, "%s/%s.oldlog", cp, logname );
    sprintf(olderlog, "%s/%s.olderlog", cp, logname );
    sprintf(oldestlog, "%s/%s.oldestlog", cp, logname );
  }
  else
  {
    sprintf(log, "%s.log", logname );
    sprintf(oldlog, "%s.oldlog", logname );
    sprintf(olderlog, "%s.olderlog", logname );
    sprintf(oldestlog, "%s.oldestlog", logname );
  }

  if( logfile )
    fclose(logfile);
  else {
    log_msg("newlog - no logfile");
    return(1);
  }

  sprintf(buf, "rm -f %s", oldestlog );           system(buf);
  sprintf(buf, "touch %s", olderlog );            system(buf);
  sprintf(buf, "mv %s %s", olderlog, oldestlog ); system(buf);
  sprintf(buf, "touch %s", oldlog );              system(buf);
  sprintf(buf, "mv %s %s", oldlog, olderlog );    system(buf);
  sprintf(buf, "touch %s", log );                 system(buf);
  sprintf(buf, "mv %s %s", log, oldlog);          system(buf);

  if( (logfile = fopen( log, "a+" )) == NULL )
    log_perror("log_newlog");

  return(0);
}


char *getNewCtime(offset)
int offset;
{
  double timed;
  long usec;
  static char timestr[256];
  struct timeval tvv,*tp;
  static struct tm *tmnow;
  long sec, toff;

  tp = &tvv;
  gettimeofday(tp,0);
  tp->tv_usec += 500;               /* Add .0005 sec for rounding to millisec */
  if (tp->tv_usec >= 1000000)
  {
    tp->tv_usec -= 1000000;
    tp->tv_sec++;
  }

  toff = offset * 3600;
  sec = tp->tv_sec + toff;                                      /* add offset */
  tmnow = localtime(&sec);
  usec = ((double)tp->tv_usec / 1000000.0) * 1000.0;            /* Rounded ms */

  sprintf(timestr,"%02d %3.3s %02d %02d:%02d:%02d.%03ld ",
		tmnow->tm_mday, 
		months[tmnow->tm_mon], 
		tmnow->tm_year-100,
		tmnow->tm_hour,
		tmnow->tm_min,
		tmnow->tm_sec,
		usec);
  return(timestr);
}
