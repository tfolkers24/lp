#ifndef lint
static const char yysccsid[] = "@(#)yaccpar	1.9 (Berkeley) 02/21/93";
#endif

#include <stdlib.h>
#include <string.h>

#define YYBYACC 1
#define YYMAJOR 1
#define YYMINOR 9
#define YYPATCH 20100216

#define YYEMPTY        (-1)
#define yyclearin      (yychar = YYEMPTY)
#define yyerrok        (yyerrflag = 0)
#define YYRECOVERING() (yyerrflag != 0)

#define YYPREFIX "yy"

/* compatibility with bison */
#ifdef YYPARSE_PARAM
/* compatibility with FreeBSD */
#ifdef YYPARSE_PARAM_TYPE
#define YYPARSE_DECL() yyparse(YYPARSE_PARAM_TYPE YYPARSE_PARAM)
#else
#define YYPARSE_DECL() yyparse(void *YYPARSE_PARAM)
#endif
#else
#define YYPARSE_DECL() yyparse(void)
#endif /* YYPARSE_PARAM */

extern int YYPARSE_DECL();

#line 2 "mkgs.y"

/*
 * Cactus File @(#)mkgs.y	1.2
 *         SID 1.2
 *        Date 8/6/96
 */

#include <stdio.h>
#include <ctype.h>

#line 14 "mkgs.y"
typedef union {
         char *var;
         int tok;
       } YYSTYPE;
#line 50 "y.tab.c"
#define STRUCT 257
#define LBRACE 258
#define RBRACE 259
#define SEMI 260
#define LB 261
#define RB 262
#define CHARSTAR 263
#define DOUBLE 264
#define INTEGER 265
#define LONG 266
#define FLOAT 267
#define SHORT 268
#define UNSIGNED 269
#define VAR 270
#define COMMENT 271
#define YYERRCODE 256
static const short yylhs[] = {                           -1,
    0,    0,    0,    0,    1,    4,    4,    5,    5,    5,
    5,    5,    5,    5,    2,    2,    3,    3,    3,    3,
    3,
};
static const short yylen[] = {                            2,
    0,    2,    2,    2,    6,    2,    1,    1,    6,    3,
    6,    9,    7,    4,    1,    2,    1,    1,    1,    1,
    1,
};
static const short yydefred[] = {                         1,
    0,    3,    0,    4,    2,    0,    0,    0,    0,   17,
   20,   19,   18,   21,    0,    8,    0,   15,    0,    7,
    0,    0,   16,    0,    0,    6,    0,    0,   10,    0,
    5,   14,    0,    0,    0,    0,    0,    0,    0,    9,
   11,    0,   13,    0,    0,   12,
};
static const short yydgoto[] = {                          1,
    5,   17,   18,   19,   20,
};
static const short yysindex[] = {                         0,
 -256,    0, -267,    0,    0, -253, -240, -234, -233,    0,
    0,    0,    0,    0, -246,    0, -232,    0, -255,    0,
 -231, -221,    0, -254, -230,    0, -228, -229,    0, -227,
    0,    0, -225, -220, -218, -216, -213, -226, -212,    0,
    0, -219,    0, -210, -211,    0,
};
static const short yyrindex[] = {                         0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,
};
static const short yygindex[] = {                         0,
    0,    0,   35,    0,   34,
};
#define YYTABLESIZE 53
static const short yytable[] = {                          2,
    3,    8,    6,   25,    7,   29,   30,    9,   10,   11,
   12,   13,   14,   15,    4,   16,    8,   10,   11,   12,
   13,   14,    9,   10,   11,   12,   13,   14,   15,   31,
   16,   32,   33,   41,   42,   21,   22,   24,   27,   28,
   34,   37,   35,   38,   36,   39,   40,   43,   46,   23,
   44,   45,   26,
};
static const short yycheck[] = {                        256,
  257,  257,  270,  259,  258,  260,  261,  263,  264,  265,
  266,  267,  268,  269,  271,  271,  257,  264,  265,  266,
  267,  268,  263,  264,  265,  266,  267,  268,  269,  260,
  271,  260,  261,  260,  261,  270,  270,  270,  270,  261,
  270,  262,  270,  262,  270,  262,  260,  260,  260,   15,
  270,  262,   19,
};
#define YYFINAL 1
#ifndef YYDEBUG
#define YYDEBUG 1
#endif
#define YYMAXTOKEN 271
#if YYDEBUG
static const char *yyname[] = {

"end-of-file",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,"STRUCT","LBRACE","RBRACE","SEMI",
"LB","RB","CHARSTAR","DOUBLE","INTEGER","LONG","FLOAT","SHORT","UNSIGNED","VAR",
"COMMENT",
};
static const char *yyrule[] = {
"$accept : list",
"list :",
"list : list construct",
"list : list error",
"list : list COMMENT",
"construct : STRUCT VAR LBRACE vars RBRACE SEMI",
"vars : vars line",
"vars : line",
"line : COMMENT",
"line : CHARSTAR VAR LB VAR RB SEMI",
"line : notchar VAR SEMI",
"line : notchar VAR LB VAR RB SEMI",
"line : notchar VAR LB VAR RB LB VAR RB SEMI",
"line : STRUCT VAR VAR LB VAR RB SEMI",
"line : STRUCT VAR VAR SEMI",
"notchar : typepp",
"notchar : UNSIGNED typepp",
"typepp : DOUBLE",
"typepp : FLOAT",
"typepp : LONG",
"typepp : INTEGER",
"typepp : SHORT",

};
#endif
#if YYDEBUG
#include <stdio.h>
#endif

/* define the initial stack-sizes */
#ifdef YYSTACKSIZE
#undef YYMAXDEPTH
#define YYMAXDEPTH  YYSTACKSIZE
#else
#ifdef YYMAXDEPTH
#define YYSTACKSIZE YYMAXDEPTH
#else
#define YYSTACKSIZE 500
#define YYMAXDEPTH  500
#endif
#endif

#define YYINITSTACKSIZE 500

int      yydebug;
int      yynerrs;

typedef struct {
    unsigned stacksize;
    short    *s_base;
    short    *s_mark;
    short    *s_last;
    YYSTYPE  *l_base;
    YYSTYPE  *l_mark;
} YYSTACKDATA;

#define YYPURE 0

int      yyerrflag;
int      yychar;
YYSTYPE  yyval;
YYSTYPE  yylval;

/* variables for the parser stack */
static YYSTACKDATA yystack;
#line 81 "mkgs.y"

yyerror(s)
char *s;
{
  extern int count;

  printf( "%s at line %d\n", s, count );
}

yywrap() { return(1); }
#line 216 "y.tab.c"
/* allocate initial stack or double stack size, up to YYMAXDEPTH */
static int yygrowstack(YYSTACKDATA *data)
{
    int i;
    unsigned newsize;
    short *newss;
    YYSTYPE *newvs;

    if ((newsize = data->stacksize) == 0)
        newsize = YYINITSTACKSIZE;
    else if (newsize >= YYMAXDEPTH)
        return -1;
    else if ((newsize *= 2) > YYMAXDEPTH)
        newsize = YYMAXDEPTH;

    i = data->s_mark - data->s_base;
    newss = (data->s_base != 0)
          ? (short *)realloc(data->s_base, newsize * sizeof(*newss))
          : (short *)malloc(newsize * sizeof(*newss));
    if (newss == 0)
        return -1;

    data->s_base  = newss;
    data->s_mark = newss + i;

    newvs = (data->l_base != 0)
          ? (YYSTYPE *)realloc(data->l_base, newsize * sizeof(*newvs))
          : (YYSTYPE *)malloc(newsize * sizeof(*newvs));
    if (newvs == 0)
        return -1;

    data->l_base = newvs;
    data->l_mark = newvs + i;

    data->stacksize = newsize;
    data->s_last = data->s_base + newsize - 1;
    return 0;
}

#if YYPURE || defined(YY_NO_LEAKS)
static void yyfreestack(YYSTACKDATA *data)
{
    free(data->s_base);
    free(data->l_base);
    memset(data, 0, sizeof(*data));
}
#else
#define yyfreestack(data) /* nothing */
#endif

#define YYABORT  goto yyabort
#define YYREJECT goto yyabort
#define YYACCEPT goto yyaccept
#define YYERROR  goto yyerrlab

int
YYPARSE_DECL()
{
    int yym, yyn, yystate;
#if YYDEBUG
    const char *yys;

    if ((yys = getenv("YYDEBUG")) != 0)
    {
        yyn = *yys;
        if (yyn >= '0' && yyn <= '9')
            yydebug = yyn - '0';
    }
#endif

    yynerrs = 0;
    yyerrflag = 0;
    yychar = YYEMPTY;
    yystate = 0;

#if YYPURE
    memset(&yystack, 0, sizeof(yystack));
#endif

    if (yystack.s_base == NULL && yygrowstack(&yystack)) goto yyoverflow;
    yystack.s_mark = yystack.s_base;
    yystack.l_mark = yystack.l_base;
    yystate = 0;
    *yystack.s_mark = 0;

yyloop:
    if ((yyn = yydefred[yystate]) != 0) goto yyreduce;
    if (yychar < 0)
    {
        if ((yychar = yylex()) < 0) yychar = 0;
#if YYDEBUG
        if (yydebug)
        {
            yys = 0;
            if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
            if (!yys) yys = "illegal-symbol";
            printf("%sdebug: state %d, reading %d (%s)\n",
                    YYPREFIX, yystate, yychar, yys);
        }
#endif
    }
    if ((yyn = yysindex[yystate]) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
    {
#if YYDEBUG
        if (yydebug)
            printf("%sdebug: state %d, shifting to state %d\n",
                    YYPREFIX, yystate, yytable[yyn]);
#endif
        if (yystack.s_mark >= yystack.s_last && yygrowstack(&yystack))
        {
            goto yyoverflow;
        }
        yystate = yytable[yyn];
        *++yystack.s_mark = yytable[yyn];
        *++yystack.l_mark = yylval;
        yychar = YYEMPTY;
        if (yyerrflag > 0)  --yyerrflag;
        goto yyloop;
    }
    if ((yyn = yyrindex[yystate]) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
    {
        yyn = yytable[yyn];
        goto yyreduce;
    }
    if (yyerrflag) goto yyinrecovery;

    yyerror("syntax error");

    goto yyerrlab;

yyerrlab:
    ++yynerrs;

yyinrecovery:
    if (yyerrflag < 3)
    {
        yyerrflag = 3;
        for (;;)
        {
            if ((yyn = yysindex[*yystack.s_mark]) && (yyn += YYERRCODE) >= 0 &&
                    yyn <= YYTABLESIZE && yycheck[yyn] == YYERRCODE)
            {
#if YYDEBUG
                if (yydebug)
                    printf("%sdebug: state %d, error recovery shifting\
 to state %d\n", YYPREFIX, *yystack.s_mark, yytable[yyn]);
#endif
                if (yystack.s_mark >= yystack.s_last && yygrowstack(&yystack))
                {
                    goto yyoverflow;
                }
                yystate = yytable[yyn];
                *++yystack.s_mark = yytable[yyn];
                *++yystack.l_mark = yylval;
                goto yyloop;
            }
            else
            {
#if YYDEBUG
                if (yydebug)
                    printf("%sdebug: error recovery discarding state %d\n",
                            YYPREFIX, *yystack.s_mark);
#endif
                if (yystack.s_mark <= yystack.s_base) goto yyabort;
                --yystack.s_mark;
                --yystack.l_mark;
            }
        }
    }
    else
    {
        if (yychar == 0) goto yyabort;
#if YYDEBUG
        if (yydebug)
        {
            yys = 0;
            if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
            if (!yys) yys = "illegal-symbol";
            printf("%sdebug: state %d, error recovery discards token %d (%s)\n",
                    YYPREFIX, yystate, yychar, yys);
        }
#endif
        yychar = YYEMPTY;
        goto yyloop;
    }

yyreduce:
#if YYDEBUG
    if (yydebug)
        printf("%sdebug: state %d, reducing by rule %d (%s)\n",
                YYPREFIX, yystate, yyn, yyrule[yyn]);
#endif
    yym = yylen[yyn];
    if (yym)
        yyval = yystack.l_mark[1-yym];
    else
        memset(&yyval, 0, sizeof yyval);
    switch (yyn)
    {
case 2:
#line 32 "mkgs.y"
	{ got_construct(yystack.l_mark[0].var); }
break;
case 3:
#line 34 "mkgs.y"
	{ yyerrok; yyclearin; YYACCEPT; }
break;
case 5:
#line 39 "mkgs.y"
	{ yyval.var = yystack.l_mark[-4].var; }
break;
case 8:
#line 47 "mkgs.y"
	{ comment(yystack.l_mark[0].var); }
break;
case 9:
#line 49 "mkgs.y"
	{ add_char(yystack.l_mark[-4].var, yystack.l_mark[-2].var ); }
break;
case 10:
#line 51 "mkgs.y"
	{ add_notchar( yystack.l_mark[-2].tok, yystack.l_mark[-1].var); }
break;
case 11:
#line 53 "mkgs.y"
	{ add_array(yystack.l_mark[-5].tok, yystack.l_mark[-4].var, yystack.l_mark[-2].var); }
break;
case 12:
#line 55 "mkgs.y"
	{ printf( "/* skipping %s */\n", yystack.l_mark[-7].var );}
break;
case 13:
#line 57 "mkgs.y"
	{ printf( "/* skipping struct array %s %s */\n", yystack.l_mark[-5].var, yystack.l_mark[-4].var );}
break;
case 15:
#line 62 "mkgs.y"
	{ yyval.tok = yystack.l_mark[0].tok; }
break;
case 16:
#line 64 "mkgs.y"
	{ yyval.tok = yystack.l_mark[0].tok; }
break;
case 17:
#line 68 "mkgs.y"
	{ yyval.tok = yystack.l_mark[0].tok; }
break;
case 18:
#line 70 "mkgs.y"
	{ yyval.tok = yystack.l_mark[0].tok; }
break;
case 19:
#line 72 "mkgs.y"
	{ yyval.tok = yystack.l_mark[0].tok; }
break;
case 20:
#line 74 "mkgs.y"
	{ yyval.tok = yystack.l_mark[0].tok; }
break;
case 21:
#line 76 "mkgs.y"
	{ yyval.tok = yystack.l_mark[0].tok; }
break;
#line 482 "y.tab.c"
    }
    yystack.s_mark -= yym;
    yystate = *yystack.s_mark;
    yystack.l_mark -= yym;
    yym = yylhs[yyn];
    if (yystate == 0 && yym == 0)
    {
#if YYDEBUG
        if (yydebug)
            printf("%sdebug: after reduction, shifting from state 0 to\
 state %d\n", YYPREFIX, YYFINAL);
#endif
        yystate = YYFINAL;
        *++yystack.s_mark = YYFINAL;
        *++yystack.l_mark = yyval;
        if (yychar < 0)
        {
            if ((yychar = yylex()) < 0) yychar = 0;
#if YYDEBUG
            if (yydebug)
            {
                yys = 0;
                if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
                if (!yys) yys = "illegal-symbol";
                printf("%sdebug: state %d, reading %d (%s)\n",
                        YYPREFIX, YYFINAL, yychar, yys);
            }
#endif
        }
        if (yychar == 0) goto yyaccept;
        goto yyloop;
    }
    if ((yyn = yygindex[yym]) && (yyn += yystate) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yystate)
        yystate = yytable[yyn];
    else
        yystate = yydgoto[yym];
#if YYDEBUG
    if (yydebug)
        printf("%sdebug: after reduction, shifting from state %d \
to state %d\n", YYPREFIX, *yystack.s_mark, yystate);
#endif
    if (yystack.s_mark >= yystack.s_last && yygrowstack(&yystack))
    {
        goto yyoverflow;
    }
    *++yystack.s_mark = (short) yystate;
    *++yystack.l_mark = yyval;
    goto yyloop;

yyoverflow:
    yyerror("yacc stack overflow");

yyabort:
    yyfreestack(&yystack);
    return (1);

yyaccept:
    yyfreestack(&yystack);
    return (0);
}
