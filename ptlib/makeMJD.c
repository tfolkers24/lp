#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <math.h>

#include "caclib_pt_proto.h"

int main(argc, argv)
int argc;
char *argv[];
{
  int im, id, iy;
  double mjd, ut;

  if(argc < 5)
  {
    fprintf(stderr, "Usage: %s mm dd yyyy fractional_ut\n", argv[0]);
    exit(1);
  }

  im = atoi(argv[1]);
  id = atoi(argv[2]);
  iy = atoi(argv[3]);

  ut = atof(argv[4]);

  iy -= 2000;

  mjd = makeJulDate(im, id, iy, ut);

  printf("%f\n", mjd);

  exit(0);
}
