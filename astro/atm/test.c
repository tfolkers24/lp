#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <libgen.h>
#include <time.h>
#include <sys/types.h>
#include <unistd.h>


int atm_atmosp_(float *tamb, float *press, float *h0);
int atm_transm_(float *mmwat, float *am, float *freqs, float *tems, float *tatms, float *tauo, float *tauwat, float *tautots, int *ier);

int main()
{
  int   ier;
  float tamb, press, h0/*, rh*/;
  float mmwat, am, freqs;		/* Inputs */
  float tems, tatms, tauo, tauwat, tautots; /* Returns */
//  float trans;

  tamb  = 273.4;
  press = 571;
//  rh    = 5.0;
  h0    = 3.186; /* SMT */
  h0    = 4.267; /* MK */

  atm_atmosp_(&tamb, &press, &h0); // One call keeps model in memory

  mmwat = 1.0;
  am    = 1.0;
  freqs = 34.0;

  while(freqs < 1000.0)
  {
    atm_transm_(&mmwat, &am, &freqs, &tems, &tatms, &tauo, &tauwat, &tautots, &ier);

    printf("%f %f %f %f %f %f\n", freqs, tems, tatms, tauo, tauwat, tautots);

    freqs += 0.1;
  }

  return(0);
}
