* Last processed by NICE on 19-Mar-2002 20:40:00
* Customized for :  IEEE, LINUX, UNIX, MOTIF, F77
      SUBROUTINE EXCESS_PATH (F_GHZ, P_ATM, P_VAP, T, DH,
     $Z, PATH, C_SNELL, N_INDEX)
C--------------------------------------------------------------------------
*
* Calculation of the excess path length (path length difference between
* vacuum and atmospheric propagation) for a parallel medium of
* constant refraction index.
* Source for the path length formula: Thompson, Moran, Swenson (1986),
*                     Interferometry and Synthesis in Radio Astronomy, p 407
*
* Input:  real*4 f_ghz   frequency in GHz
*                p_atm   total atmospherical pressure in millibar
*                p_vap   partial pressure of water vapor in millibar
*                t       temperature in kelvin
*                z       Zenith distance angle (radians)
* Output: real*8 c_snell : Constant of Snell's law
*                n_index : refraction index
*         real*4 path    : path length difference in units of dh.
*
* Author: 25-Jan-1994  Michael Bremer, IRAM
C-------------------------------------------------------------------------
      REAL REFRACT_TOTAL, F_GHZ, P_ATM, P_VAP, T, DH, Z, PATH, REFR
      REAL*8 SIN_Z, COS_Z,  C_SNELL, N_INDEX
*----------------------------------------------
*
      REFR    = REFRACT_TOTAL( F_GHZ, P_ATM, P_VAP, T)
*
* Apply the definition of refractivity to get the refraction index :
*
      N_INDEX = 1.0D+00 + REFR * 1.0D-06
*
* c_snell stays constant along the line of sight (Snell's law) and
* is calculated if the given value is .lt. 0. This should make life
* easier when dealing with multiple layers.
*
      IF (C_SNELL .LT. 0) C_SNELL = SIN( Z ) * N_INDEX
      SIN_Z   = C_SNELL / N_INDEX
      COS_Z   = SQRT(1.D+00 - SIN_Z * SIN_Z)
*
      PATH = REFR * 1.0D-06 * DH / COS_Z
*
      END
*<FF>
      FUNCTION REFRACT_TOTAL (F_GHZ, P_ATM, P_VAP, T )
C-------------------------------------------------------------------
C
C Calculation of the total atmospheric refractivity (dry component and
C water vapor), taking into account the dependences from
C frequency, pressure, temperature
C Source of formulae: Hill and Cliffort (1981), Radio Science 16, pp. 77-82
C                and  Thompson, Moran, Swenson (1986),
C                     Interferometry and Synthesis in Radio Astronomy, p 407
C
C Input: real*4 f_ghz  frequency in GHz
C               p_atm  total atmospherical pressure in millibar
C               p_vap  partial pressure of water vapor in millibar
C               t      temperature in kelvin
C
C Author: 25-Jan-1994  Michael Bremer, IRAM
C
C-------------------------------------------------------------------
      REAL REFRACT_TOTAL, F_GHZ, P_ATM, P_VAP, T
      REAL REF_DRY, REF_VAP,  SC, REFRACT_VAPOR
*
*--------------------------------------------
*
* sc = scaling factor for the wavelenght dependent part of the wet refration
* (normal conditions 300K, 1013mbar, 80% humidity -> partial pressure of
*  water vapor 28.2mbar ):
*
      SC      = (P_VAP / 28.2) * (300./T)**2
      REF_DRY = 77.493 * P_ATM / T
      REF_VAP = - 12.8 * P_VAP / T + REFRACT_VAPOR( F_GHZ ) * SC
      REFRACT_TOTAL = REF_DRY + REF_VAP
      END
*<FF>
      FUNCTION REFRACT_VAPOR (F_GHZ)
C-----------------------------------------------------------------------
C
C Function to calculate the refractivity of water vapor 0-480 GHz, under
C conditions T=300K, P=1atm, 80% rel. humidity (i.e. partial pressure of
C water vapor 28.2 mbar).
C
C Source: Hill and Clifford (1981), Radio Science 16, curve p. 80
C Method of digitalisation: zoomed copy to a transparency,
C                           points read by cursor.
C                           Approx. errors: F +-1.5 GHz, R +-0.1
C
C Author: 24-Jan-1993 Michael Bremer, IRAM
C------------------------------------------------------------------------
      REAL REFRACT_VAPOR, F_GHZ
      INTEGER NPOINT, I
      PARAMETER (NPOINT = 53)
      REAL FREQ(NPOINT), REFR(NPOINT), U
*
      DATA FREQ /   0.00 ,  18.00 ,  22.53 ,  47.95 ,
     $59.41 ,  79.04 ,  98.67 , 115.85 ,
     $133.03 , 152.66 , 167.39 , 181.70 ,
     $183.57 , 185.66 , 188.11 , 195.47 ,
     $215.65 , 235.29 , 250.83 , 271.28 ,
     $288.28 , 305.46 , 315.27 , 321.64 ,
     $323.50 , 327.18 , 328.82 , 336.18 ,
     $348.45 , 358.27 , 366.45 , 371.35 ,
     $374.63 , 377.90 , 379.53 , 378.72 ,
     $381.35 , 382.99 , 387.90 , 393.44 ,
     $405.71 , 427.26 , 431.07 , 437.80 ,
     $441.07 , 446.16 , 448.80 , 449.62 ,
     $456.98 , 469.07 , 472.97 , 476.43 ,
     $480.01 /
      DATA REFR / 115.64 , 115.68 , 115.59 , 115.69 ,
     $115.76 , 115.88 , 116.07 , 116.25 ,
     $116.50 , 116.81 , 117.24 , 118.08 ,
     $117.80 , 116.25 , 116.38 , 117.00 ,
     $117.65 , 118.21 , 118.71 , 119.39 ,
     $120.07 , 120.87 , 121.53 , 122.26 ,
     $122.45 , 121.33 , 121.33 , 122.20 ,
     $123.38 , 124.37 , 125.61 , 126.91 ,
     $128.27 , 129.85 , 125.73 , 121.89 ,
     $119.29 , 119.29 , 121.89 , 123.30 ,
     $125.70 , 129.18 , 129.87 , 132.29 ,
     $132.42 , 134.90 , 129.51 , 125.30 ,
     $129.07 , 133.47 , 134.59 , 134.24 ,
     $134.96 /
* -------------------------------
*
* negative frequencies are NOT accepted (not even in jest):
*
      IF (F_GHZ .LT. 0) THEN
         WRITE(6,*) 'E-ATM,  Error from refract_vapor: frequency < 0'
         STOP
      ENDIF
*
* Find the frequency interval (i-1,i) of the input frequency:
      I = 2
*
10    CONTINUE
      IF (FREQ(I) .GT. F_GHZ) GOTO 20
      I = I + 1
*
      IF (I .LE. NPOINT) GOTO 10
*
*     Print an error message, if the frequency range has been checked and the
*     requested frequency lies beyond, and give the last data range value:
*      PRINT *,'Error from refract_vapor: ',F_GHZ,' outside 0-480 GHz.'
*
      REFRACT_VAPOR = REFR(NPOINT)
      RETURN
*
20    CONTINUE
*
* Perform linear interpolation between the interval borders:
*
      U = (F_GHZ - FREQ(I-1)) / (FREQ(I) - FREQ(I-1))
      REFRACT_VAPOR = REFR(I-1) + (REFR(I) - REFR(I-1)) * U
      END
