* Last processed by NICE on 19-Mar-2002 20:40:00
* Customized for :  IEEE, LINUX, UNIX, MOTIF, F77
      FUNCTION FVVW(V,VL,DV)
C----------------------------------------------------------------------
C	VAN VLECK & WEISSKOPF'S PROFILE
C
C J CERNICHARO
C----------------------------------------------------------------------
      REAL FVVW,V,DV,VL,PI,DV2,A1,A2
      DATA PI/3.141592654/
*
      FVVW=DV*V/VL/PI
      DV2=DV*DV
      A1=DV2+(V-VL)**2
      A2=DV2+(V+VL)**2
      FVVW=FVVW*(1./A1+1./A2)
      END
