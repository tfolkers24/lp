* Last processed by NICE on 19-Mar-2002 20:40:00
* Customized for :  IEEE, LINUX, UNIX, MOTIF, F77
C+VMS
c      SUBROUTINE ATM_M(ERROR)
C-----------------------------------------------------------------------
C Map the table file to be interpolated.
C-----------------------------------------------------------------------
C Global:
c      INCLUDE 'inc:format.inc'
c      INTEGER*4 ATM_ADDR, ATM_LENGTH
c      COMMON /ATMFILE/ ATM_ADDR, ATM_LENGTH
C Dummy:
c      LOGICAL ERROR
C Local:
c      INTEGER LUN, NF, LENC, IER, N(5), I
c      INTEGER BUFFER(128)
c      CHARACTER FILE*80, DEVICE*80
c      CHARACTER*4 CODE
c      INCLUDE '($secdef)'
c      INCLUDE '($ssdef)'
c      INTEGER*4 ADDR_SEC(2), SYS$CRMPSC,ADDR_MAP(2)
c      INTEGER FILEID, IOCHAN, FLAG, ICODE
c      LOGICAL RDONLY
C-----------------------------------------------------------------------
c      ATM_LENGTH = 0
c      CALL SIC_GETLUN(LUN)
c      FILE = 'GAG_ATMOSPHERE'
c      CALL SIC_GTLGTR(FILE)
c      NF = LENC(FILE)
c      OPEN (UNIT=LUN, FILE=FILE(1:NF), FORM='unformatted',
c     $STATUS='old', RECL=128*FACUNF, ACCESS='direct',
c     $READONLY, SHARED, IOSTAT = IER)
c      IF (IER.NE.0) THEN
c         CALL PUTIOS('F-ATM_I, Open error: ',IER)
c         WRITE(6,*) 'I-ATM_I, Filename: ',FILE(1:NF)
c         ERROR = .TRUE.
c         RETURN
c      ENDIF
*
* first record
c      READ(LUN,REC=1,ERR=998,IOSTAT = IER) BUFFER
c      CALL R4TOR4(BUFFER(1), %REF(CODE), 1)
c      IF (CODE.NE.'VAX_') THEN
c         WRITE(6,*) 'F-ATM, Wrong file format '//CODE
c         RETURN
c      ENDIF
c      CLOSE(LUN)
c      CALL SIC_FRELUN(LUN)
*
c      DO I=1,5
c         N(I) = BUFFER(I+1)
c      ENDDO
c      ATM_LENGTH = 6 + N(1)+N(2)+N(3)+N(4)+N(5)
c     $+ 2*N(1)*N(2)*N(3)*N(4)*N(5) + 2*N(1)*N(2)*N(3)
c      ATM_LENGTH=128*((ATM_LENGTH-1)/128+2)
*
c      RDONLY = .TRUE.
c      CALL GDF_OPNBDF(FILE(1:NF),IOCHAN,RDONLY,IER)
*
c      CALL GET_VM(ATM_LENGTH, ATM_ADDR, ERROR)
c      IF (ERROR) RETURN
c      ADDR_SEC(1) = ATM_ADDR
c      ADDR_SEC(2) = ATM_ADDR+ATM_LENGTH*4
c      ICODE = SYS$CRMPSC(ADDR_SEC,ADDR_MAP,,,,,,%VAL(IOCHAN),
c     $%VAL(ATM_LENGTH/128),,,)
c      ATM_ADDR = ADDR_MAP(1)
c      CALL LIB$SIGNAL(%VAL(ICODE))
c      CALL SYS$DASSGN(%VAL(IOCHAN))
c      RETURN
*
c998   IF (IER.NE.0) THEN
c         CALL PUTIOS('F-ATM_I, Read Error: ',IER)
c         WRITE(6,*) 'I-ATM_I, Filename: ',FILE(1:NF)
c         ERROR = .TRUE.
c      ENDIF
*
c      CLOSE(LUN)
c      CALL SIC_FRELUN(LUN)
c      RETURN
c      END
C-VMS
*<FF>
      BLOCK DATA ATM_DATA
      INTEGER*4 ATM_ADDR, ATM_LENGTH
      COMMON /ATMFILE/ ATM_ADDR, ATM_LENGTH
      DATA ATM_LENGTH/0/
      END
*<FF>
      SUBROUTINE ATM_I(ERROR)
C-----------------------------------------------------------------------
C Make sure the atm data will be interpolated from file
C-----------------------------------------------------------------------
      LOGICAL ERROR
      INTEGER*4 ATM_ADDR, ATM_LENGTH
      COMMON /ATMFILE/ ATM_ADDR, ATM_LENGTH
      SAVE /ATMFILE/
*
      ATM_LENGTH = -1
      RETURN
      END
*<FF>
      SUBROUTINE ATM_INIT(ERROR)
C-----------------------------------------------------------------------
C (internal entry point)
C Actually read the table file to be interpolated.
C-----------------------------------------------------------------------
* Global:
      INCLUDE 'inc:format.inc'
      INTEGER*4 ATM_ADDR, ATM_LENGTH
      COMMON /ATMFILE/ ATM_ADDR, ATM_LENGTH
      SAVE /ATMFILE/
* Dummy:
      LOGICAL ERROR
* Local:
      INTEGER LUN, NF, LENC, IER
      INTEGER BUFFER(128)
      CHARACTER FILE*80
      CHARACTER*4 CODE, NCODE
      INTEGER ICODE
      EQUIVALENCE (ICODE,CODE)
C+VAX
c      EXTERNAL IER4VA, EIR4VA, R4TOR4, EII4VA
c      DATA NCODE/'VAX_'/
C-VAX
C+EEEI
c      EXTERNAL VAR4EI, IER4EI, R4TOR4, VAI4EI, IEI4EI
c      DATA NCODE/'EEEI'/
C-EEEI
C+IEEE
      EXTERNAL VAR4IE, EIR4IE, R4TOR4, VAI4IE, EII4IE
      DATA NCODE/'IEEE'/
C-IEEE
C+WIN32
c!MS$ATTRIBUTES ALIAS:'_VAR4IE@12' ::  VAR4IE
c!MS$ATTRIBUTES ALIAS:'_EIR4IE@12' ::  EIR4IE
c!MS$ATTRIBUTES ALIAS:'_R4TOR4@12' ::  R4TOR4
c!MS$ATTRIBUTES ALIAS:'_VAI4IE@12' ::  VAI4IE
c!MS$ATTRIBUTES ALIAS:'_EII4IE@12' ::  EII4IE
C-WIN32
C-----------------------------------------------------------------------
      ATM_LENGTH = 0
      CALL SIC_GETLUN(LUN)
      FILE = 'GAG_ATMOSPHERE'
      CALL SIC_GTLGTR(FILE)
      NF = LENC(FILE)
      OPEN (UNIT=LUN, FILE=FILE(1:NF), FORM='unformatted',
     $STATUS='old', RECL=128*FACUNF, ACCESS='direct',
C+VMS+ULTRIX
c     $BLOCKSIZE=8192, READONLY, SHARED,
C-VMS-ULTRIX
     $IOSTAT = IER)
      IF (IER.NE.0) THEN
         CALL PUTIOS('F-ATM_I, Open error: ',IER)
         WRITE(6,*) 'I-ATM_I, Filename: ',FILE(1:NF)
         ERROR = .TRUE.
         RETURN
      ENDIF
*
* first record
      READ(LUN,REC=1,ERR=998,IOSTAT = IER) BUFFER
      ICODE = BUFFER(1)
      IF (CODE.NE.NCODE) THEN
         WRITE(6,*) 'I-ATM_I, Converting from ',CODE,' to ',NCODE
      ELSE
         WRITE(6,*) 'I-ATM_I, Native table format'
      ENDIF
C+VAX
c      IF (CODE.EQ.'VAX_') THEN
c         CALL ATM_DECODE(LUN,BUFFER,R4TOR4,R4TOR4,IER,ERROR)
c      ELSEIF (CODE.EQ.'IEEE') THEN
c         CALL ATM_DECODE(LUN,BUFFER,IER4VA,R4TOR4,IER,ERROR)
c      ELSEIF (CODE.EQ.'EEEI') THEN
c         CALL ATM_DECODE(LUN,BUFFER,EIR4VA,EII4VA,IER,ERROR)
c      ENDIF
C-VAX
C+IEEE
      IF (CODE.EQ.'VAX_') THEN
         CALL ATM_DECODE(LUN,BUFFER,VAR4IE,VAI4IE,IER,ERROR)
      ELSEIF (CODE.EQ.'IEEE') THEN
         CALL ATM_DECODE(LUN,BUFFER,R4TOR4,R4TOR4,IER,ERROR)
      ELSEIF (CODE.EQ.'EEEI') THEN
         CALL ATM_DECODE(LUN,BUFFER,EIR4IE,EII4IE,IER,ERROR)
      ENDIF
C-IEEE
C+EEEI
c      IF (CODE.EQ.'VAX_') THEN
c         CALL ATM_DECODE(LUN,BUFFER,VAR4EI,VAI4EI,IER,ERROR)
c      ELSEIF (CODE.EQ.'IEEE') THEN
c         CALL ATM_DECODE(LUN,BUFFER,IER4EI,IEI4EI,IER,ERROR)
c      ELSEIF (CODE.EQ.'EEEI') THEN
c         CALL ATM_DECODE(LUN,BUFFER,R4TOR4,R4TOR4,IER,ERROR)
c      ENDIF
C-EEEI
998   IF (IER.NE.0) THEN
         CALL PUTIOS('F-ATM_I, Read Error: ',IER)
         WRITE(6,*) 'I-ATM_I, Filename: ',FILE(1:NF)
         ERROR = .TRUE.
      ENDIF
*
      CLOSE(LUN)
      CALL SIC_FRELUN(LUN)
      END
*<FF>
      SUBROUTINE ATM_DECODE(LUN,BUFFER,R4,I4,IER,ERROR)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
* Global:
      INCLUDE 'inc:memory.inc'
      INTEGER*4 ATM_ADDR, ATM_LENGTH
      COMMON /ATMFILE/ ATM_ADDR, ATM_LENGTH
      SAVE /ATMFILE/
* Dummy:
      INTEGER LUN, BUFFER(128), IER
      LOGICAL ERROR
      EXTERNAL R4, I4
* Local:
      INTEGER K, N(5), IREC, SIC_GETVM
      INTEGER*4 IP, IPR, GAG_POINTER
C-----------------------------------------------------------------------
      IER = 0
      CALL I4(BUFFER(2),N(1),5)
      ATM_LENGTH = 6 + N(1)+N(2)+N(3)+N(4)+N(5)
     $+ 2*N(1)*N(2)*N(3)*N(4)*N(5) + 2*N(1)*N(2)*N(3)
c      CALL GET_VM(ATM_LENGTH, ATM_ADDR, ERROR)
c      IF (ERROR) RETURN
      IREC = SIC_GETVM(ATM_LENGTH, ATM_ADDR)
      IF (IREC .NE. 1) RETURN
      IP = GAG_POINTER(ATM_ADDR,MEMORY)
      IPR = IP
      CALL I4(BUFFER(2),MEMORY(IPR+1),5)
      CALL R4(BUFFER(7),MEMORY(IPR+6),122)
*
* other records
      IREC = 2
      IPR = IPR+128
      DO K=129, ATM_LENGTH-128, 128
         READ(LUN,REC=IREC,IOSTAT=IER) BUFFER
         CALL R4(BUFFER,MEMORY(IPR),128)
         IPR = IPR+128
         IREC=IREC+1
      ENDDO
      IF (IPR.LT.IP+ATM_LENGTH) THEN
         READ(LUN,REC=IREC,IOSTAT=IER) BUFFER
         CALL R4(BUFFER,MEMORY(IPR),IP+ATM_LENGTH-IPR)
      ENDIF
      END
*<FF>
      SUBROUTINE ATM_ATMOSP_I(TEM,PRE,ALT)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
* Global:
      INCLUDE 'inc:memory.inc'
      INTEGER*4 ATM_ADDR, ATM_LENGTH
      COMMON /ATMFILE/ ATM_ADDR, ATM_LENGTH
      SAVE /ATMFILE/
* Dummy:
      REAL ALT, TEM, PRE, WAT,AIR,FRE,TEMI,TAT,TAUOX,TAUW,TAUT, PAT
      INTEGER IER
* Local:
      INTEGER*4 GAG_POINTER, IPK, IP(5), IPI, IPTOX, IPTW, IPTEM,
     $IPPAT
      INTEGER IX(5), I, N(5)
      REAL W(5,32), XI(5), KV
      SAVE W, XI, KV, IX, N, IP, IPK, IPI, IPTOX, IPTW,
     $IPTEM, IPPAT
      LOGICAL ERROR
C-----------------------------------------------------------------------
*
      IF (ATM_LENGTH.LT.0) THEN
         ERROR = .FALSE.
         CALL ATM_INIT(ERROR)
         IF (ERROR) RETURN
      ENDIF
      IF (ATM_LENGTH.GT.0) THEN
         IPI = GAG_POINTER(ATM_ADDR, MEMORY)
         IPK = IPI+6
         DO I=1, 5
            N(I) = MEMORY(IPI+I)
            IP(I) = IPK
            IPK = IPK+N(I)
         ENDDO
         IPTOX = IPK
         IPTW = IPK+N(1)*N(2)*N(3)
         IPTEM = IPTW+N(1)*N(2)*N(3)
         IPPAT = IPTEM+N(1)*N(2)*N(3)*N(4)*N(5)
         CALL INDEXP(N(1),MEMORY(IP(1)),PRE,IX(1),XI(1))
         CALL INDEXP(N(2),MEMORY(IP(2)),TEM,IX(2),XI(2))
      ELSE
         CALL ATM_ATMOSP(TEM,PRE,ALT)
      ENDIF
      RETURN
*
      ENTRY ATM_TRANSM_I(WAT,AIR,FRE,TEMI,TAT,TAUOX,TAUW,TAUT,IER)
      IF (ATM_LENGTH.EQ.0) THEN
         CALL ATM_TRANSM(WAT,AIR,FRE,TEMI,TAT,TAUOX,TAUW,TAUT,IER)
         RETURN
      ENDIF
      IF (ATM_LENGTH.LT.0) THEN
         ERROR = .FALSE.
         CALL ATM_INIT(ERROR)
         IF (ERROR) RETURN
      ENDIF
      CALL INDEXP(N(3),MEMORY(IP(3)),FRE,IX(3),XI(3))
      CALL INDEXP(N(4),MEMORY(IP(4)),WAT,IX(4),XI(4))
      CALL INDEXP(N(5),MEMORY(IP(5)),AIR,IX(5),XI(5))
      CALL INTERP(5,N,MEMORY(IPTEM),IX,XI,TEMI,W)
      CALL INTERP(3,N,MEMORY(IPTOX),IX,XI,TAUOX,W)
      CALL INTERP(3,N,MEMORY(IPTW),IX,XI,TAUW,W)
      TAUW = TAUW*WAT
      TAUT = TAUW+TAUOX
      KV = TAUT*AIR
      TAT = TEMI /(1.-EXP(-KV))
      RETURN
*
      ENTRY ATM_PATH_I(WAT,AIR,FRE,PAT,IER)
      IF (ATM_LENGTH.LT.0) THEN
         ERROR = .FALSE.
         CALL ATM_INIT(ERROR)
         IF (ERROR) RETURN
      ENDIF
      IF (ATM_LENGTH.LE.0) THEN
         CALL ATM_PATH(WAT,AIR,FRE,PAT,IER)
         RETURN
      ENDIF
      CALL INDEXP(N(3),MEMORY(IP(3)),FRE,IX(3),XI(3))
      CALL INDEXP(N(4),MEMORY(IP(4)),WAT,IX(4),XI(4))
      CALL INDEXP(N(5),MEMORY(IP(5)),AIR,IX(5),XI(5))
      CALL INTERP(5,N,MEMORY(IPPAT),IX,XI,PAT,W)
      RETURN
      END
*<FF>
      SUBROUTINE INDEXP(N,XT,X,IX,XI)
C-----------------------------------------------------------------------
C computes the index and fractional increment of value x in table xt
C e.g.       x = xt(ix)+ xi*(xt(ix+1)-xt(ix))
C returns ix=1, xi=0. if x < xt(1)
C         ix=n, xi=0. if x > xt(n)
C-----------------------------------------------------------------------
* Dummy:
      INTEGER N                          ! dimension of table xt
      REAL XT(N)                         ! input table
      REAL X                             ! input value of x
      REAL XI                            ! fractional increment
      INTEGER IX                         ! index
* Local:
      INTEGER I
*-----------------------------------------------------------------------
      IX = 1
      XI = 0.0
      IF (X.LT.XT(1)) RETURN
      IF (N.LT.2) RETURN
      DO I=1, N-1
         IF (X.LT.XT(I+1)) THEN
            IX = I
            XI = (X-XT(I))/(XT(I+1)-XT(I))
            RETURN
         ENDIF
      ENDDO
      IX = N
      END
*<FF>
      SUBROUTINE INTERP(NV,N,T,IX,XI,V,W)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C Dummy:
      INTEGER NV,                        ! number of variables
     $N(NV),                             ! dimensions of the table
     $IX(NV)                             ! index of x value xx
* where the function is to be taken
      REAL XI(NV)                        ! fractional increment of x value
* e.g. xx = x(ix)+xi*(x(ix+1)-x(ix))
      REAL T(*)                          ! table (nv dimensions)
      REAL V                             ! interpolated value (output)
      REAL W(NV,*)                       ! work array of dim (nv,2**nv)
* Local:
      INTEGER M, NN, IP, I, IPV, J, K
*-----------------------------------------------------------------------
      M = 0
*
* w(1,m) contains the values at the corners of the nv-th order hypercube
      DO WHILE (M.LT.2**NV)
         NN = 1
         IP = 1
         K = 1
         DO I= NV, 1, -1
            IPV = IX(I)
            IF (MOD(M/K,2).NE.0 .AND. XI(I).GT.0) THEN
               IPV = IPV+1
            ENDIF
            IP = IP + (IPV-1)*NN
            NN = NN*N(I)
            K = K*2
         ENDDO
         M = M+1
         W(NV,M) = T(IP)
      ENDDO
*
* Now interpolate
      DO I = NV, 2, -1
         M = 2**(I-1)
         DO J=1, M
            W(I-1,J) = W(I,2*J-1)+(W(I,2*J)-W(I,2*J-1))*XI(I)
         ENDDO
      ENDDO
      V = W(1,1)+(W(1,2)-W(1,1))*XI(1)
      RETURN
      END
*<FF>
      SUBROUTINE ATMOS_I_TABLE(COMPUTE,FILE,NFILE,
     $N_F,FMIN,FMAX,H0,ERROR)
C-----------------------------------------------------------------------
C Write a table of atmospheric emission data or further interpolation.
C-----------------------------------------------------------------------
* Global:
      INCLUDE 'inc:format.inc'
      INCLUDE 'inc:memory.inc'
      INTEGER*4 ATM_ADDR, ATM_LENGTH
      COMMON /ATMFILE/ ATM_ADDR, ATM_LENGTH
      SAVE /ATMFILE/
* Dummy:
      CHARACTER*(*) FILE
      INTEGER NFILE, N_F
      REAL FMIN,FMAX,H0
      LOGICAL ERROR, COMPUTE
* Local:
      INTEGER I, IER, LENC, IREC, SIC_GETVM
      INTEGER N_P, N_T, N_W, N_A, M_F
      REAL PMIN,PMAX,TMIN,TMAX,WMIN,WMAX,AMIN,AMAX
      PARAMETER (N_P=5, PMIN=985, PMAX=1030,
     $N_T=10, TMIN=250., TMAX=305,
* reset to 250
*     $M_F = 10,
     $M_F = 250,
* actually exp(-water/10.)
     $N_W=7, WMIN=0.999, WMAX=0.001,
     $N_A=10, AMIN=1.0, AMAX=10.0)
      REAL*4 P(N_P), T(N_T), F(M_F), W(N_W), A(N_A)
      REAL*4 TATM, TAUOX, TAUW, TAUT, TEMIS, PATHS
      INTEGER IA, IW, IF, IT, IP, LUN, LENGTH, K, BUFFER(128)
      CHARACTER NAME*80, CODE*4
      INTEGER*4 IPR, IP1, IPEM, IPPA, IPTOX, IPTW,
     $GAG_POINTER
      INTEGER*4 ICODE
      EQUIVALENCE (CODE, ICODE)
*
* Why SAVE ?
*      SAVE
C-----------------------------------------------------------------------
      IF (N_F.GT.M_F) THEN
         WRITE(6,*) 'F-ATM, Too many frequency points'
         ERROR = .TRUE.
         RETURN
      ENDIF
*
      NAME = FILE
      CALL SIC_PARSEF(NAME,FILE,' ','.bin')
      CALL SIC_GETLUN(LUN)
      NFILE = LENC(FILE)
      OPEN(UNIT=LUN,FILE=FILE(1:NFILE),STATUS='new',ACCESS='direct',
C+VMS
c     $BLOCKSIZE=8192,
C-VMS
     $FORM='unformatted',IOSTAT=IER,RECL=128*FACUNF)
      IF (IER.NE.0) THEN
         WRITE(6,*) 'I-ATM, Filename: ',FILE(1:NFILE)
         CALL PUTIOS('F-ATM, Open error: ',IER)
         ERROR = .TRUE.
         RETURN
      ENDIF
      IF (COMPUTE) THEN
*
* intervals
         LENGTH = 6 + N_P + N_T + N_W + N_A + N_F
     $   + 2*N_P*N_T*N_F + 2*N_P*N_T*N_F*N_W*N_A
         IF (LENGTH.GT.ATM_LENGTH) THEN
            IF (ATM_LENGTH.GT.0) CALL FREE_VM(ATM_LENGTH, ATM_ADDR)
            ATM_LENGTH=LENGTH
            IREC = SIC_GETVM(ATM_LENGTH,ATM_ADDR)
            IF (IREC .NE. 1) THEN
               WRITE(6,*) 'F-ATM, error getting memory'
               RETURN
            ENDIF
         ENDIF
         IPR = GAG_POINTER(ATM_ADDR, MEMORY)
         DO I=1, N_P
            P(I) = ((N_P-I)*PMIN+(I-1)*PMAX)/(N_P-1.)
     $      *2.0**(-H0/5.5)
         ENDDO
         DO I=1, N_T
            T(I) = ((N_T-I)*TMIN+(I-1)*TMAX)/(N_T-1)
         ENDDO
         DO I=1, N_W
            W(I) = ((N_W-I)*WMIN+(I-1)*WMAX)/(N_W-1)
            W(I) = -10.*LOG(W(I))
         ENDDO
         DO I=1, N_A
            A(I) = ((N_A-I)*AMIN+(I-1)*AMAX)/(N_A-1)
         ENDDO
         DO I=1, N_F
            F(I) = ((N_F-I)*FMIN+(I-1)*FMAX)/(N_F-1)
         ENDDO
         IPTOX = IPR + 6 + N_P + N_T + N_W + N_A + N_F
         IPTW = IPTOX + N_P*N_T*N_F
         IPEM = IPTW + N_P*N_T*N_F
         IPPA = IPEM + N_P*N_T*N_F*N_W*N_A
*
         DO IP=1, N_P
            DO IT = 1, N_T
               WRITE(6,*) 'I-ATM, Temp: ',T(IT), '  Pres: ',P(IP)
               CALL ATM_ATMOSP(T(IT),P(IP),H0)
               DO IF=1, N_F
                  DO IW = 1, N_W
                     DO IA = 1, N_A
                        CALL ATM_TRANSM(W(IW),A(IA),F(IF),
     $                  TEMIS,TATM,TAUOX,TAUW,TAUT,IER)
                        CALL R4TOR4(TEMIS, MEMORY(IPEM),1)
                        CALL ATM_PATH(W(IW),A(IA),F(IF),PATHS,
     $                  IER)
                        CALL R4TOR4(PATHS, MEMORY(IPPA),1)
                        TAUW = TAUW/W(IW)
                        IPEM = IPEM+1
                        IPPA = IPPA+1
                     ENDDO
                  ENDDO
                  CALL R4TOR4(TAUOX,MEMORY(IPTOX),1)
                  IPTOX = IPTOX+1
                  CALL R4TOR4(TAUW,MEMORY(IPTW),1)
                  IPTW = IPTW+1
               ENDDO
            ENDDO
         ENDDO
         MEMORY(IPR+1) = N_P
         MEMORY(IPR+2) = N_T
         MEMORY(IPR+3) = N_F
         MEMORY(IPR+4) = N_W
         MEMORY(IPR+5) = N_A
         IP1 = IPR+6
         CALL R4TOR4(P,MEMORY(IP1),N_P)
         IP1 = IP1+N_P
         CALL R4TOR4(T,MEMORY(IP1),N_T)
         IP1 = IP1+N_T
         CALL R4TOR4(F,MEMORY(IP1),N_F)
         IP1 = IP1+N_F
         CALL R4TOR4(W,MEMORY(IP1),N_W)
         IP1 = IP1+N_W
         CALL R4TOR4(A,MEMORY(IP1),N_A)
      ELSE
         IPR = GAG_POINTER(ATM_ADDR, MEMORY)
      ENDIF
*
* Write data
      IP1 = IPR
C+VAX
c      CODE = 'VAX_'
C-VAX
C+IEEE
      CODE = 'IEEE'
C-IEEE
C+EEEI
c      CODE = 'EEEI'
C-EEEI
      CALL R4TOR4(ICODE, MEMORY(IPR), 1)
      IREC=1
      DO K=1, ATM_LENGTH-128, 128
         CALL R4TOR4(MEMORY(IP1),BUFFER,128)
         WRITE(LUN,REC=IREC,IOSTAT=IER) BUFFER
         IP1 = IP1+128
         IREC=IREC+1
      ENDDO
      IF (IP1.LT.IPR+ATM_LENGTH) THEN
         CALL R4TOR4(MEMORY(IP1),BUFFER,IPR+ATM_LENGTH-IP1)
         WRITE(LUN,REC=IREC,IOSTAT=IER) BUFFER
      ENDIF
      WRITE(6,*) 'I-ATM, ',IREC,' records written.'
      CLOSE(LUN)
      CALL SIC_FRELUN(LUN)
      END
