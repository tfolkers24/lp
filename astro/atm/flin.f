* Last processed by NICE on 19-Mar-2002 20:40:00
* Customized for :  IEEE, LINUX, UNIX, MOTIF, F77
      FUNCTION FLIN(V,VL,DV)
C----------------------------------------------------------------------
C J Cernicharo, Model atmosphere.
C
C 	KINETIC PROFILE FORM
C
C 	V  FREQUENCY
C 	VL LINE FREQUENCY
C 	DV LINE WIDTH
C
C----------------------------------------------------------------------
      REAL FLIN,V,VL,DV,PI,V2
      DATA PI/3.141592654/
*
      FLIN=4.*V*VL*DV/PI
      V2=V*V
      FLIN=FLIN/(4.*V2*DV*DV+(VL*VL-V2)**2)
      END
