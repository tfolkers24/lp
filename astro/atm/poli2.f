* Last processed by NICE on 19-Mar-2002 20:40:00
* Customized for :  IEEE, LINUX, UNIX, MOTIF, F77
      SUBROUTINE POLI2(X1,X2,X3,Y1,Y2,Y3,A,B,C)
C----------------------------------------------------------------------
C 	THIS SUBROUTINE CALCULATES THE COEFFICIENTS A,B,C OF THE POLYNOMIAL OF
C 	SECOND DEGREE A+BX+CX**2, PASSING THROUGH THE DOTS (X1,Y1),
C	(X2,Y2),(X3,Y3)
C J.Cernicharo
C----------------------------------------------------------------------
      REAL X1,X2,X3,Y1,Y2,Y3,A,B,C
*
      C=(Y3-Y2)*(X2-X1)-(Y2-Y1)*(X3-X2)
      B=(X2-X1)*(X3*X3-X2*X2)-(X2*X2-X1*X1)*(X3-X2)
      C=C/B
      B=(Y2-Y1)-C*(X2*X2-X1*X1)
      B=B/(X2-X1)
      A=Y1-C*X1*X1-B*X1
      END
