* Last processed by NICE on 19-Mar-2002 20:40:00
* Customized for :  IEEE, LINUX, UNIX, MOTIF, F77
      FUNCTION PWAT(T,U)
C----------------------------------------------------------------------
C	Calculation of the partial pressure of the water vapour
C 	t is the temperature in degrees kelvin
C 	p is the total atmospheric pressure in millibars.
C 	u is the relative humidity
C 	pwat is the partial pressure of water vapour
C
C
C	Calculation of saturation pressure
C
C
C J Cernicharo
C----------------------------------------------------------------------
      REAL PWAT,T,U
      REAL P,ES,E
*
      P=1013.
      ES=6.105*EXP(25.22/T*(T-273.)-5.31*ALOG(T/273.))
      E=1.-(1.-U/100.)*ES/P
      PWAT=ES*U/100.*E
      PWAT=PWAT*216.5/T
      RETURN
      END
