* Last processed by NICE on 19-Mar-2002 20:40:00
* Customized for :  IEEE, LINUX, UNIX, MOTIF, F77
      FUNCTION KO2(T,P,V,IL)
C----------------------------------------------------------------------
C 	OPACITY OF THE ATMOSPHERE DUE TO OXYGEN (O2)
C 	T IS THE TEMPERATURE (K)
C 	P IS THE PRESSURE IN MILLIBARS
C 	V IS THE FREQUENCY AT WHICH YOU WANT TO CALCULATE KO2
C 	IL =0 KINETIC PROFILE
C 	IL =1 PROFILE DE VAN VLECK & WEISSKOPF
C 	KO2 OPACITY IN CM-1
C
C 	WIDTH OF THE REBER... GOOD APPROACH IN THE WINGS
C
C J.Cernicharo
C----------------------------------------------------------------------
      REAL KO2,T,P,V
      INTEGER IL,L,J
      REAL FMEN(20),FMAS(20),RN(20),FDEB(6),B1(6),B2(6),B3(6)
*
      REAL TA,V2,SUM,E0,DV1,DV,DV2,A1,A2,A3,E,PI,RD,RD0,GG,B,RR
      REAL FLIN,FVVW
C
C	FMEN ... FRECUENCIAS EN GHZ  N-
C
      DATA FMEN/118.750343,62.486255,60.306044,59.164215,
     $58.323885,57.612488,56.968180,56.363393,55.783819,
     $55.221372,54.671145,54.1302,53.5959,53.0669,52.5424,
     $52.0214,51.50302,50.9873,50.4736,49.9618/
C
C	FMAS ... FRECUENCIAS EN GHZ N+
C
      DATA FMAS/56.264766,58.446580,59.590978,60.434776,
     $61.15057,61.800169,62.411223,62.997991,63.56852,
     $64.127777,64.678914,65.22412,65.764744,66.30206,
     $66.83677,67.36951,67.90073,68.4308,68.9601,69.4887/
C
C	N    ... NUMERO CUANTICO DE ROTACION
C
      DATA RN/1.,3.,5.,7.,9.,11.,13.,15.,17.,19.,21.,23.,
     $25.,27.,29.,31.,33.,35.,37.,39./
C
C	RAYAS CON DN=2
C
      DATA FDEB/368.499,424.7638,487.25,715.3944,773.841,834.147/
      DATA B1/6.79E-6,6.43E-05,2.39E-5,9.79E-6,5.71E-5,1.83E-5/
      DATA B2/.202,.0112,.0112,.0891,.0798,.0798/
      DATA B3/15.6E-4,14.7E-4,14.7E-4,14.4E-4,14E-4,14E-4/
      DATA PI/3.141592654/
      KO2=1.44E-05*P*V/T/T/T
      TA=300./T
      V2=V**2
      SUM=0.
      E0=2.07/T
      DV1=1.41E-03*P*300./T
      DV=DV1
      IF(DV1.GT.0.0527)DV=DV/3.+0.03513
      DV2=DV*DV
      DO 1 L=1,20
         A1=(RN(L)**2+RN(L)+1.)*(2.*RN(L)+1.)/RN(L)/(RN(L)+1.)
         E=E0*RN(L)*(RN(L)+1.)
         A1=A1*2.*V*DV/PI/(V2+DV2)
         A2=RN(L)*(2.*RN(L)+3.)/(RN(L)+1.)
         IF(IL.EQ.0)A2=A2*FLIN(V,FMAS(L),DV)*FMAS(L)
         IF(IL.EQ.1)A2=A2*FVVW(V,FMAS(L),DV)*FMAS(L)
         A3=(RN(L)+1.)*(2.*RN(L)-1.)/RN(L)
         B=DV
         IF(L.EQ.1)B=DV1
         IF(IL.EQ.0.)A3=A3*FMEN(L)*FLIN(V,FMEN(L),B)
         IF(IL.EQ.1.)A3=A3*FMEN(L)*FVVW(V,FMEN(L),B)
1     SUM=SUM+(A1+A2+A3)*EXP(-E)
      KO2=SUM*KO2
C
C	RAYAS CON DN=2
C
      RD=P*TA**3*4.193E-07*V
      RD0=0.
      DO 10 J=1,6
C	IF(V.LE.FDEB(J)+200..AND.V.GE.FDEB(J)-200.)GO TO 15
C	GO TO 10
15       RR=B1(J)*EXP(B2(J)*(1.-TA))
         GG=B3(J)*P*TA**.9
         IF(IL.EQ.0)RR=RR*FLIN(V,FDEB(J),GG)
         IF(IL.EQ.1)RR=RR*FVVW(V,FDEB(J),GG)
         RD0=RD0+RR
10    CONTINUE
      RD=RD*RD0
      KO2=KO2+RD
      RETURN
      END
