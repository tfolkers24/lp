* Last processed by NICE on 19-Mar-2002 20:40:00
* Customized for :  IEEE, LINUX, UNIX, MOTIF, F77
      SUBROUTINE KVATM(NP,P,T,RHO,H,AGU,OXI,V,TEMI,TATM,TAG,TAGU,TOX,
     $TOXI,ILAG,ILOX,KVAT,AMA,IER)
C----------------------------------------------------------------------
C 	opacity of the atmosphere at frequency 'v' due to the
C 	water vapor and oxygen.
C
C 	np ....  number of layers
C 	h ....   thickness of layers (cm)
C 	p ....   pressure (millibars)
C 	t ....   temperature (k)
C 	rho ...  quantity of water vapour (gr/m**3)
C 	temi ..  total emissivity of the atmosphere (k)
C 	kvat ..  total opacity (nepers)
C 	tatm ..  average temperature of the atmosphere (k)
C 	water... opacity due to water vapour (nepers)
C 	oxi ...  opacity due to oxygen ( " )
C 	tag ...  emissivity due to water vapour (oxygen-free atmosphere)
C 	tox ...  same for oxygen (without water vapour)
C 	tagu ..  average temperature of the water vapour layer (k)
C 	toxi ..  mean oxygen layer temperature (k)
C
C J.Cernicharo
C----------------------------------------------------------------------
      INTEGER NP,ILAG,ILOX,IER
      REAL AGU,OXI,V,TEMI,TATM,TAG,TAGU,TOX,TOXI,KVAT,AMA
      REAL H(*),P(*),T(*),RHO(*)
*
      INTEGER J
      REAL R,PR,TEM,DH,OX,AG,KV
      REAL KH2O, KO2
*
      TEMI=0.
      KV=0.
      TAG=0.
      TOX=0.
      AGU=0.
      OXI=0.
      DO 1 J=1,NP
         R=RHO(J)
         PR=P(J)
         TEM=T(J)
         DH=H(J)
         AG=KH2O(R,TEM,PR,V,ILAG)*DH*AMA
         OX=KO2(TEM,PR,V,ILOX)*DH*AMA
         TAG=TAG+TEM*EXP(-AGU)*(1.-EXP(-AG))
         AGU=AGU+AG
         TOX=TOX+TEM*EXP(-OXI)*(1.-EXP(-OX))
         OXI=OXI+OX
         TEMI=TEMI+TEM*EXP(-KV)*(1.-EXP(-AG-OX))
1     KV=AGU+OXI
      KVAT=KV
      IF ( KV.LE.1.E-10 ) THEN
         IER = 1
      ELSEIF ( OXI.LE.1.E-20) THEN
         IER = 2
      ELSEIF ( AGU.LE.1.E-20 ) THEN
         IER = 3
      ELSE
         TATM=TEMI/(1.-EXP(-KV))
         TAGU=TAG/(1.-EXP(-AGU))
         TOXI=TOX/(1.-EXP(-OXI))
         IER = 0
      ENDIF
      END
