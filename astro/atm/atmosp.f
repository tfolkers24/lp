* Last processed by NICE on 19-Mar-2002 20:40:00
* Customized for :  IEEE, LINUX, UNIX, MOTIF, F77
C+HPUX
c$OPTIMIZE LEVEL1
C-HPUX
      SUBROUTINE ATM_ATMOSP(T0,P0,H0)
C----------------------------------------------------------------------
C 	Compute an atmospheric model, interpolated between standard 
C	atmospheres of winter and summer (subroutines ase45 and asj 
C	45), to fit with temperature t0 (k) and pressure p0 (mbar)
C	at altitude h0 (km).  15 layers are used.
C 	The transmission of the model atmosphere can then be computed 
C	by calling entry point transm.
C
C input 	t0	R	temperature	(K)
C		po	R	pressure	(mbar)
C		h0	R	altitude	(km)
C----------------------------------------------------------------------
      INTEGER MP
      PARAMETER (MP=80)
      REAL  T(MP), H(MP), P(MP), R(MP), RR(MP),
     $PE, TE, DE, H0, T0, P0, PJ, TJ, DJ, APE, ATE, APJ, ATJ, P1,
     $T1, D1, HEIGHT, R1, WATER, AIRMASS, TAUW, TAUOX, TAUT, TEMI,
     $FREQ, TAG, TOX, TAGU, TOXI, TATM
      INTEGER NP, J, IER
      REAL PATH
      REAL DPATH, Z, PR_AG
      REAL*8 N_INDEX, C_SNELL
*
      SAVE T,H,P,R,NP

C
C      WRITE(*, '(A)') 'LIBATM: ATM_ATMOSP Called'
C      WRITE(*, '(A,F9.3,A,F9.3,A,F9.3)') 'TAMB = ',T0,
C     $' PRESS = ',P0,' H0 = ',H0
C
*
* average summer and winter model atmospheres according to
* given values of temperature and pression
      CALL ASE45(PE,TE,DE,H0)
      CALL ASJ45(PJ,TJ,DJ,H0)
      APE = (P0-PJ)/(PE-PJ)
      APJ = (P0-PE)/(PJ-PE)
      ATE = (T0-TJ)/(TE-TJ)
      ATJ = (T0-TE)/(TJ-TE)
*
* set layers
      DO J=1,6
         H(J) = .5E5
      ENDDO
      DO J=7,12
         H(J) = 2.E5
      ENDDO
      DO J=13,15
         H(J) = 15.E5
      ENDDO
      NP = 15
*
* Set t,p, and r (H2O for 1mm precipitable content) profiles
      HEIGHT = H0
      P1 = P0
      T1 = T0
      R1 = .5
      DO J = 1, NP
         HEIGHT = HEIGHT + H(J)/100000.  ! in km.
         P(J) = P1
         T(J) = T1
         R(J) = R1
         CALL ASE45(PE,TE,DE,HEIGHT)
         CALL ASJ45(PJ,TJ,DJ,HEIGHT)
         P1 = APE*PE+APJ*PJ
         T1 = ATE*TE+ATJ*TJ
         D1 = DE*(1+(P1-PE)/PE-(T1-TE)/TE)
         R1 = .5*EXP(-.5*(HEIGHT-H0))
         IF(HEIGHT.GT.15.) R1 = R1 + D1*2E-6
         P(J) = (P(J) + P1)/2.
         T(J) = (T(J) + T1)/2.
         R(J) = (R(J) + R1)/2.
      ENDDO
      RETURN
*
      ENTRY ATM_TRANSM(WATER,AIRMASS,FREQ,TEMI,TATM,TAUOX,TAUW,TAUT
     $,IER)
C----------------------------------------------------------------------
C 	Compute atmospheric emission and  absorption.
C
C Input:
C 	water 	R	H2O precipitable content(mm)
C	airmass R	Number of air masses
C	freq 	R	Frequency		(GHz)
C
C Output:
C	temi	R	atmosph emission	(K)
C	tatm   	R	mean temperature	(K)
C	tauox	R	Oxygen optical depth  AT ZENITH	(nepers)
C       tauw   	R	Water  optical depth  AT ZENITH	(nepers)
C       taut   	R	Total  optical depth  AT ZENITH	(nepers)
C	IER	I	Error code
C----------------------------------------------------------------------
C      WRITE(*, '(A)') 'LIBATM: ATM_TRANSM Called'
C      WRITE(*,'(F9.3,F9.3,F9.3,F9.3,F9.3,F9.3,F9.3,F9.3)') WATER,
C     *AIRMASS,FREQ,TEMI,TATM,TAUOX,TAUW,TAUT
C
      DO J = 1, NP
         RR(J) = R(J) * WATER
      ENDDO
      IER = 0
      CALL KVATM(NP,P,T,RR,H,TAUW,TAUOX,FREQ,TEMI,TATM,TAG,TAGU,TOX,
     $TOXI,0,0,TAUT,AIRMASS,IER)
      TAUOX = TAUOX / AIRMASS            ! RL 14 MAR 86
      TAUW = TAUW / AIRMASS              !
      TAUT = TAUT / AIRMASS              !
C
C      WRITE(*, '(A)') 'LIBATM: ATM_TRANSM Return'
C      WRITE(*,'(F9.3,F9.3,F9.3,F9.3,F9.3,F9.3,F9.3,F9.3)') WATER,
C     *AIRMASS,FREQ,TEMI,TATM,TAUOX,TAUW,TAUT
C      WRITE(*,'(A)') ' '
C
      RETURN
*
      ENTRY ATM_PATH(WATER,AIRMASS,FREQ,PATH,IER)
C----------------------------------------------------------------------
C       integrated optical pathlength of atmosphere
C
C	np .... numero de capas
C	h  .... espesor de las capas     (cm)
C	p  .... presion (milibares)
C	t  .... temperatura (k)
C	rho ... cantidad de vapor de agua (gr/m**3)
C----------------------------------------------------------------------
C-----------------------------------------------------------------------
* MB: zenith distance angle from airmass (parallel layers):
      Z = ACOS( 1. / AIRMASS)
      C_SNELL = -1
      PATH    = 0.
*
      DO J=1,NP
*
* partial pressure of water vapor. Rspec = Rgas/M_H2O = 8314/18.02 = 461.4
* Conversion from pascal->mbar 1e-2, g->kg 1e-3:
         PR_AG = 4.614E-03  * T(J) * R(J) * WATER
         CALL EXCESS_PATH (FREQ, P(J), PR_AG, T(J), H(J),
     $   Z, DPATH, C_SNELL, N_INDEX)
*         IF (J .EQ. 1) DI = Z - ASIN(SIN(Z) / N_INDEX)
         PATH = PATH + DPATH
      ENDDO
      END
