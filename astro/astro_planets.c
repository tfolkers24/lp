#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define MAX_PLANETS 7

char *planet_strs[] = { 
	"Mercury", 
	"Venus", 
	"Mars", 
	"Jupiter", 
	"Saturn", 
	"Uranus", 
	"Neptune"
};

struct PLANET {
	char  name[16], 
	      ra[16], dec[16];
	float az, el;
	float dt, ds;		/* geocentric, heliocentric distance */
	float maj, min;		/* Major, Minor semi-diameter */
	float pa, tb, s;	/* Position Angle of Major Meridian */
	float freq, beam;	/* OBS Freq, beam size @ this freq */
	float tmb, flux;	/* ??Temp Main Beam??, Flux in Janskys */
	float size,		/* Size of what? */
	      vel, sunDist;
	int   sunAvoid;		/* Object is with the Bill Avoidance Zone */
};

struct PLANET tmp;
struct PLANET planets[MAX_PLANETS];


char tok[40][40];

int get_tok(edit)
char *edit;
{
  int c = 0;
  char *token;

  token = strtok(edit, " =()\n\t");
  while(token != NULL)
  {
    sprintf(tok[c++], "%s", token);
    token = strtok(NULL, " =()\n\t");
  }

  return(c);
}

struct PLANET *whichPlanet(wp)
char *wp;
{
  int i;
  struct PLANET *p;

  p = &planets[0];
  for(i=0;i<MAX_PLANETS;i++,p++)
  {
    if(!strcasecmp(wp, p->name))
      return(p);
  }

  return(NULL);
}

char *monStr[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun",
                   "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

int main(argc, argv)
int argc;
char *argv[];
{
  FILE *fp;
  int i, notyet = 1;
  char pbuf[80], *string, line[256], timeStr[256], dateStr[256], site[256];
  struct PLANET *p;
  float freq;
  double fdate;
  int year, mon, day=29;

  if(argc < 5)
  {
    fprintf(stderr, "Usage: %s frequency time date site\n", argv[0]);
    fprintf(stderr, "Where Frequency = 60.0-1000.0 GHz\n");
    fprintf(stderr, "Where time is in the form: hh:mm:ss \n");
    fprintf(stderr, "Where date is in the form: yyyy.mm.dd \n");
    fprintf(stderr, "Where site is in either SMT or KITT_PEAK\n");
    exit(1);
  }

  freq = atof(argv[1]);

  if(freq < 60.0 || freq > 1000.0)
  {
    fprintf(stderr, "Usage: %s frequency\n", argv[0]);
    fprintf(stderr, "Where Frequency = 60.0-1000.0 GHz\n");
    exit(1);
  }

  strcpy(timeStr, argv[2]);

  fdate = atof(argv[3]);

  year = (int)fdate;

  fdate = fdate - (double)year;

  fdate *= 100.0;

  mon = (int)fdate;

  fdate = fdate - (double)mon;

  fdate *= 100.0;

  day = (int)round(fdate);

  mon--;

  sprintf(dateStr, "%02d-%3.3s-%4d", day, monStr[mon], year);

  strcpy(site, argv[4]);

  for(i=0;i<MAX_PLANETS;i++) /* fill in names */
  {
    strcpy(planets[i].name, planet_strs[i]);
  }

  sprintf(pbuf, "./astro.csh %f %s %s %s", freq, timeStr, dateStr, site);

//  printf("%s\n", pbuf);

  if( (fp = popen(pbuf, "r") ) <= (FILE *)0 )
  {
    fprintf(stderr, "Unable to open %s\n",pbuf);
    exit(1);
  }

  while((string = fgets(line, sizeof(line), fp)) != NULL)
  {
    if(notyet) /* throw them out until you see mercury */
    {
      if(!strstr(line, "MERCURY"))
        continue;
      else
	notyet = 0;
    }

    if(strstr(line, "RA ") && strstr(line, "Dec ")) /* First line */
    {
      get_tok(line);
      strcpy(tmp.name, tok[0]);

      if((p = whichPlanet(tmp.name)) == NULL)
      {
        fprintf(stderr, "Error parsing %s", line);
        exit(1);
      }

      strcpy(p->ra, tok[2]);
      strcpy(p->dec, tok[4]);
      p->az = atof(tok[6]);
      p->el = atof(tok[8]);
      p->sunDist = atof(tok[10]);
      p->vel = atof(tok[12]);
      p->sunAvoid = 0;
    }
    else
    if(strstr(line, "within Sun avoidance")) /* Sun Avoid line */
    {
      if(p)
        p->sunAvoid = 1;
    }
    else
    if(strstr(line, "DT=") && strstr(line, "DS=")) /* Second line */
    {
      if(p)
      {
        get_tok(line);

        p->dt = atof(tok[1]);
        p->ds = atof(tok[3]);
        p->maj = atof(tok[5]);
        p->min = atof(tok[7]);
        p->pa = atof(tok[9]);
        p->tb = atof(tok[11]);
        p->s = atof(tok[14]);
      }
    }
    else
    if(strstr(line, "Frequency ") && strstr(line, "Flux")) /* Third line */
    {
      if(p)
      {
        get_tok(line);

        p->freq = atof(tok[1]);
        p->beam = atof(tok[3]);
        p->tmb = atof(tok[5]);
        p->flux = atof(tok[7]);
        p->size = atof(tok[9]);

	/* done with this one */
        p = (struct PLANET *)NULL;
      }
    }
  }

  pclose(fp);

  printf("  Name       RA            Dec        Vel  Sun  SAZ  Dist  ");
  printf("Major  Minor Tbright Freq  Beam   Tmb   Flux\n\n");

  p = &planets[0];

  for(i=0;i<MAX_PLANETS;i++, p++)
  {
    printf("%8.8s %s %13s %5.1f %5.1f  ",
		p->name, p->ra, p->dec, p->vel, p->sunDist);

    printf("%d %5.2f  %5.2f\" %5.2f\" %6.2f %5.1f ",
		p->sunAvoid, p->dt, p->maj, p->min, p->tb, p->freq);

    printf("%.1f\" %5.1f %5.0f\n",
		p->beam, p->tmb, p->flux);
  }

  exit(0);
}
