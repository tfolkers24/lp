#!/bin/csh -f
#
echo "$0 $1 $2 $3"
#
if ( "$1" == "" || "$2" == "" || "$3" == "" || "$4" == "" ) then
  echo "Usage: $0 freq time date site"
  echo "Where Frequency = 100.0-1000.0 GHz"
  echo "Where time is in the form: hh:mm:ss"
  echo "Where date is in the form: dd-Mon-yyyy"
  echo "Where site is Either SMT or KITT_PEAK"
  exit
endif
#
#set utinfo=`date -u | awk '{printf("%s %s-%s-%s\n", $4, $3,$2,$6)}'` 
#set utinfo="23:55:00 29-Mar-2015"
set utinfo="$2 $3"
echo observatory $4 > /tmp/eph1$$
echo time $utinfo >> /tmp/eph1$$
echo let frequency $1 >> /tmp/eph1$$
if ( "$4" == "SMT" ) then
  echo "let sun_limit 41" >> /tmp/eph1$$
else
  echo "let sun_limit 5" >> /tmp/eph1$$
endif
#
echo let temperature 25.0 >> /tmp/eph1$$
echo let zero_pressure 527.0 >> /tmp/eph1$$
echo let airmass 2.0 >> /tmp/eph1$$
echo let water 2.5 >> /tmp/eph1$$
echo let forward_eff 85.0 >> /tmp/eph1$$
echo let gain_image 1.0 >> /tmp/eph1$$
echo let trec 65 >> /tmp/eph1$$
echo let freq_sig $1  >> /tmp/eph1$$
echo let freq_ima 217.1  >> /tmp/eph1$$
#
echo atmosphere >> /tmp/eph1$$
#
echo say tsys = \'tsys\'  >> /tmp/eph1$$
echo say true_pressure = \'true_pressure\' >> /tmp/eph1$$
echo say tau_h2o = \'tau_h2o\' >> /tmp/eph1$$
echo say tau_o2 = \'tau_o2\' >> /tmp/eph1$$
echo say emis_sig = \'EMIS_SIG\' >> /tmp/eph1$$
echo say emis_ima = \'EMIS_IMA\'  >> /tmp/eph1$$
echo say path_sig = \'PATH_SIG\'  >> /tmp/eph1$$
echo say path_ima = \'PATH_IMA\'  >> /tmp/eph1$$
echo exit >> /tmp/eph1$$

/home/analysis/gag/linux-intel/bin/astro < /tmp/eph1$$

#rm -f /tmp/eph1*
