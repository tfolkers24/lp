* Last processed by NICE on 19-Mar-2002 20:38:00
* Customized for :  IEEE, LINUX, UNIX, MOTIF, F77
      INTEGER CODE_READ_DATA
      INTEGER CODE_READ_HEADER
      INTEGER CODE_UPDATE_HEADER
      INTEGER CODE_WRITE_HEADER
      INTEGER CODE_WRITE_DATA
      INTEGER CODE_READ_IMAGE
      INTEGER CODE_FREE_IMAGE
      INTEGER CODE_CREATE_IMAGE
*
      PARAMETER (CODE_READ_DATA=-1)
      PARAMETER (CODE_READ_HEADER=-2)
      PARAMETER (CODE_UPDATE_HEADER=-3)
      PARAMETER (CODE_WRITE_HEADER=-4)
      PARAMETER (CODE_WRITE_DATA=-5)
      PARAMETER (CODE_READ_IMAGE=-6)
      PARAMETER (CODE_FREE_IMAGE=-7)
      PARAMETER (CODE_CREATE_IMAGE=-8)
C+HPUX+F90
*
* /XIPAR/
c      TYPE LOCATION
c      SEQUENCE
c      INTEGER*4 AL64
c      INTEGER*4 SIZE
c      INTEGER*4 ADDR
c      INTEGER ISLO
c      INTEGER MSLO
c      LOGICAL READ
c      LOGICAL GETVM
c      END TYPE
* /XCPAR/
c      TYPE STRINGS
c      SEQUENCE
c      CHARACTER*12 TYPE
c      CHARACTER*12 UNIT                  ! 56
c      CHARACTER*12 CODE(4)               ! 59
c      CHARACTER*12 SYST                  ! 71
c      CHARACTER*12 NAME                  ! 75,76,77
c      CHARACTER*12 LINE
c      END TYPE
* /XPAR/
c      TYPE GILDAS_HEADER
c      SEQUENCE
c      INTEGER*4 ITYP(3)                  !  1
c      INTEGER*4 FORM                     !  4
c      INTEGER*4 NVB                      !  5
c      INTEGER*4 FILL(5)                  !  6
c      INTEGER*4 GENE                     ! 11
c      INTEGER*4 NDIM                     ! 12
c      INTEGER*4 DIM(4)                   ! 13
c      REAL*8 REF1                        ! 17
c      REAL*8 VAL1                        ! 19
c      REAL*8 INC1                        ! 21
c      REAL*8 REF2                        ! 23
c      REAL*8 VAL2                        ! 25
c      REAL*8 INC2                        ! 27
c      REAL*8 REF3                        ! 29
c      REAL*8 VAL3                        ! 31
c      REAL*8 INC3                        ! 33
c      REAL*8 REF4                        ! 35
c      REAL*8 VAL4                        ! 37
c      REAL*8 INC4                        ! 39
*
c      INTEGER*4 BLAN                     ! 41
c      REAL*4 BVAL                        ! 42
c      REAL*4 EVAL                        ! 43
*
c      INTEGER*4 EXTR                     ! 44
c      REAL*4 RMIN                        ! 45
c      REAL*4 RMAX                        ! 46
c      INTEGER*4 MIN1                     ! 47
c      INTEGER*4 MAX1                     ! 48
c      INTEGER*4 MIN2                     ! 49
c      INTEGER*4 MAX2                     ! 50
c      INTEGER*4 MIN3                     ! 51
c      INTEGER*4 MAX3                     ! 52
c      INTEGER*4 MIN4                     ! 53
c      INTEGER*4 MAX4                     ! 54
*
c      INTEGER*4 DESC                     ! 55
c      INTEGER*4 IUNI(3)                  ! 56
c      INTEGER*4 ICOD(3,4)                ! 59
c      INTEGER*4 ISYS(3)                  ! 71
c      INTEGER*4 DUM1                     ! Void
*
c      INTEGER*4 POSI                     ! 74
c      INTEGER*4 ISOU(3)                  ! 75
c      REAL*8 RA                          ! 78
c      REAL*8 DEC                         ! 80
c      REAL*8 LII                         ! 82
c      REAL*8 BII                         ! 84
c      REAL*4 EPOC                        ! 86
c      INTEGER*4 DUM2                     ! Void
*
c      INTEGER*4 PROJ                     ! 87
c      INTEGER*4 PTYP                     ! 88
c      REAL*8 A0                          ! 89
c      REAL*8 D0                          ! 91
c      REAL*8 PANG                        ! 93
c      INTEGER*4 XAXI                     ! 95
c      INTEGER*4 YAXI                     ! 96
*
c      INTEGER*4 SPEC                     ! 97
c      INTEGER*4 ILIN(3)                  ! 98
c      REAL*8 FRES                        !101
c      REAL*8 FIMA                        !103
c      REAL*8 FREQ                        !105
c      REAL*4 VRES                        !107
c      REAL*4 VOFF                        !108
c      INTEGER*4 FAXI                     !109
*
c      INTEGER*4 RESO                     !110
c      REAL*4 MAJO                        !111
c      REAL*4 MINO                        !112
c      REAL*4 POSA                        !113
*
c      INTEGER*4 SIGM                     ! 114
c      REAL*4 NOISE                       ! 115
c      REAL*4 RMS                         ! 116
c      END TYPE
*
c      TYPE SIC_HEADER
c      SEQUENCE
c      INTEGER*4 ITYP(3)                  !  1
c      INTEGER*4 FORM                     !  4
c      INTEGER*4 NVB                      !  5
c      INTEGER*4 FILL(5)                  !  6
c      INTEGER*4 GENE                     ! 11
c      INTEGER*4 NDIM                     ! 12
c      INTEGER*4 DIM(4)                   ! 13
c      REAL*8 CONVERT(3,4)                ! 17-40
*
c      INTEGER*4 BLAN                     ! 41
c      REAL*4 BLANK(2)                    ! 42-43
*
c      INTEGER*4 EXTREMA                  ! 44
c      REAL*4 MIN                         ! 45
c      REAL*4 MAX                         ! 46
c      INTEGER*4 WHERE(2,4)               ! 47-54
*
c      INTEGER*4 DESC                     ! 55
c      INTEGER*4 IUNI(3)                  ! 56 Unit
c      INTEGER*4 ICOD(3,4)                ! 59 Unit1,2,3,4
c      INTEGER*4 ISYS(3)                  ! 71 System
c      INTEGER*4 DUM1                     ! Void
*
c      INTEGER*4 POSI                     ! 74
c      INTEGER*4 ISOU(3)                  ! 75  Source
c      REAL*8 RA                          ! 78
c      REAL*8 DEC                         ! 80
c      REAL*8 LII                         ! 82
c      REAL*8 BII                         ! 84
c      REAL*4 EPOC                        ! 86
c      INTEGER*4 DUM2                     ! Void
*
c      INTEGER*4 PROJ                     ! 87
c      INTEGER*4 PTYP                     ! 88
c      REAL*8 A0                          ! 89
c      REAL*8 D0                          ! 91
c      REAL*8 ANGLE                       ! 93
c      INTEGER*4 X_AXIS                   ! 95
c      INTEGER*4 Y_AXIS                   ! 96
*
c      INTEGER*4 SPEC                     ! 97
c      INTEGER*4 ILIN(3)                  ! 98 Line
c      REAL*8 FREQRES                     ! 101
c      REAL*8 FREQOFF                     ! 103
c      REAL*8 RESTFRE                     ! 105
c      REAL*4 VELRES                      ! 107
c      REAL*4 VELOFF                      ! 108
c      INTEGER*4 F_AXIS                   ! 109
*
c      INTEGER*4 BEAM                     ! 110
c      REAL*4 MAJOR                       ! 111
c      REAL*4 MINOR                       ! 112
c      REAL*4 PA                          ! 113
*
c      INTEGER*4 SIGMA                    ! 114
c      REAL*4 NOISE                       ! 115
c      REAL*4 RMS                         ! 116
c      END TYPE
C-HPUX-F90
C+F90
*
* Gildas X,Y,Z version
c      TYPE GILDAS
c      SEQUENCE
c      CHARACTER*256 FILE                 ! File name
c      TYPE (STRINGS) :: CHAR
c      TYPE (LOCATION) :: LOCA
c      TYPE (GILDAS_HEADER) :: GIL
c      INTEGER*4 BLC(4)
c      INTEGER*4 TRC(4)
c      INTEGER*4 HEADER                   ! Defined / Undefined
c      INTEGER*4 STATUS                   ! Last error code
c      REAL, POINTER :: R1D(:)
c      REAL(KIND=8), POINTER :: D1D(:)
c      INTEGER, POINTER :: I1D(:)
c      REAL, POINTER :: R2D(:,:)
c      REAL(KIND=8), POINTER :: D2D(:,:)
c      INTEGER, POINTER :: I2D(:,:)
c      REAL, POINTER :: R3D(:,:,:)
c      REAL(KIND=8), POINTER :: D3D(:,:,:)
c      INTEGER, POINTER :: I3D(:,:,:)
c      REAL, POINTER :: R4D(:,:,:,:)
c      REAL(KIND=8), POINTER :: D4D(:,:,:,:)
c      INTEGER, POINTER :: I4D(:,:,:,:)
c      END TYPE
*
* SIC Variable version
c      TYPE SIC
c      SEQUENCE
c      CHARACTER*256 FILE                 ! File name
c      TYPE (STRINGS) :: CHAR
c      TYPE (LOCATION) :: LOCA
c      TYPE (SIC_HEADER) :: SIC
c      INTEGER*4 BLC(4)
c      INTEGER*4 TRC(4)
c      INTEGER*4 HEADER
c      INTEGER*4 STATUS
c      REAL, POINTER :: R1D(:)
c      REAL(KIND=8), POINTER :: D1D(:)
c      INTEGER, POINTER :: I1D(:)
c      REAL, POINTER :: R2D(:,:)
c      REAL(KIND=8), POINTER :: D2D(:,:)
c      INTEGER, POINTER :: I2D(:,:)
c      REAL, POINTER :: R3D(:,:,:)
c      REAL(KIND=8), POINTER :: D3D(:,:,:)
c      INTEGER, POINTER :: I3D(:,:,:)
c      REAL, POINTER :: R4D(:,:,:,:)
c      REAL(KIND=8), POINTER :: D4D(:,:,:,:)
c      INTEGER, POINTER :: I4D(:,:,:,:)
c      END TYPE
*
C-F90
C+HPUX-F90
*
* Gildas X,Y,Z version
c      TYPE GILDAS
c      SEQUENCE
c      CHARACTER*256 FILE                 ! File name
c      TYPE (STRINGS) :: CHAR
c      TYPE (LOCATION) :: LOCA
c      TYPE (GILDAS_HEADER) :: GIL
c      INTEGER*4 BLC(4)
c      INTEGER*4 TRC(4)
c      INTEGER*4 HEADER                   ! Defined / Undefined
c      INTEGER*4 STATUS                   ! Last error code
c      END TYPE
*
* SIC Variable version
c      TYPE SIC
c      SEQUENCE
c      CHARACTER*256 FILE                 ! File name
c      TYPE (STRINGS) :: CHAR
c      TYPE (LOCATION) :: LOCA
c      TYPE (SIC_HEADER) :: SIC
c      INTEGER*4 BLC(4)
c      INTEGER*4 TRC(4)
c      INTEGER*4 HEADER
c      INTEGER*4 STATUS
c      END TYPE
*
C-HPUX-F90
