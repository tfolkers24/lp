* Last processed by NICE on 19-Mar-2002 20:38:00
* Customized for :  IEEE, LINUX, UNIX, MOTIF, F77
C-----------------------------------------------------------------
c
c All addresses, either returned by the system following a call to
c SIC_GETVM, or computed using the LOCWRD() or LOCSTR() functions,
c are conveyed in the program(s) as an offset in an array of
c INTEGERS named MEMORY.
c
c The software is built on 4-byte integers and reals, and SIC_GETVM
c allocates memory by multiples of 4 bytes. Usually one computes the
c offset as
c	INCLUDE 'inc:memory.inc'
c	INTEGER(KIND=AD) IP,GAG_POINTER,ADDRESS
c	( ... )
c	IP=GAG_POINTER(ADDRESS,MEMORY)
c where AD=4 for 32-bit machines, 8 for 64 bit machines.
c
c Afterwards, the adress is "recalled"  as MEMORY(IP).
c It is possible to perform some algebra on the adresses (ADDRESS) or
c the pointers (IP), such as IP=IP+1 => MEMORY(IP) will point
c 1*4 bytes later, i.e., on another R*4 if IP was already "remembering"
c the address of an R*4.
c
c It would have been equivalent to compute ADDRESS=ADDRESS+4, then
c	IP=GAG_POINTER(ADDRESS,MEMORY).
c It is then evident that working on adresses and IPs is NOT the
c same.
c
c We have equivalenced the MEMORY (I4) array with a byte array
c MEMBYT for the rare case where the address returned does not
c fall naturally on a 32-byte word in memory. This can happen
c when getting the address of a (sub)-string through the LOCSTR
c function. In that case, one uses IP = BYTPNT (ADDRESS,MEMBYT)
c and MEMBYT(IP).
c
c Note: Memory allocation provides 32-bit alignement -- through
c tricks in the C code of SIC_GETVM for the rare 16 bit computers.
c The case in point above is the address of an
c internal character array (got by LOCSTR) or function address
c (got by LOCWRD), which can be 8 or 16 bit aligned on some machines.
c
c To alleviate potential problems, the routine GAG_POINTER
c will complain if it cannot translate an address to an offset
c in a I*4 array on computer not 32 bit aligned. It will be then up to
c the programmer to use BYTPNT and additional arithmetics instead
c of GAG_POINTER.
c
C---------------------------------------------------------------------
      REAL*8    MEMOR8(1)
      INTEGER*1      MEMBYT(8)
      INTEGER*4 MEMORY(2)
      CHARACTER MEMCHAR(8)
      EQUIVALENCE (MEMORY,MEMBYT)
      EQUIVALENCE (MEMOR8,MEMORY)
      EQUIVALENCE (MEMCHAR,MEMORY)
* The whole thing is is common now
      COMMON /OURPOINTERREF/ MEMORY
