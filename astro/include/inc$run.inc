* Last processed by NICE on 19-Mar-2002 20:38:00
* Customized for :  IEEE, LINUX, UNIX, MOTIF, F77
      INTEGER MTASK
      PARAMETER (MTASK=4)                ! Maximum number of active tasks
      INTEGER MBXINT(30)                 ! Mailbox message
      CHARACTER*20 PRCNAM(MTASK)         ! Process name
      INTEGER LPRCNAM(MTASK),            ! Length of process name
     $PRCID(MTASK),                      ! Process ID
     $PRCSTAT(MTASK),                    ! Process completion status
     $PRCIOSB(2),                        ! Process status block
     $ITASK,                             ! Last activated task
     $NTASK,                             ! Number of active tasks
     $EFRUN                              ! Event flag for WAIT command
      LOGICAL PRCENDED,                  ! A process ended ?
     $WABORT,                            ! WAIT command aborted
     $ASLEEP                             ! Logical flag to trace where we are
* in GILDAS (.TRUE. means in SIC)
      COMMON /MAILBOX/ PRCNAM,LPRCNAM,PRCID,PRCSTAT,PRCENDED,PRCIOSB
     $,MBXINT,ASLEEP,ITASK,NTASK,EFRUN,WABORT
C+VMS
c      VOLATILE /MAILBOX/
C-VMS
      SAVE /MAILBOX/
