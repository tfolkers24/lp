* Last processed by NICE on 19-Mar-2002 20:38:00
* Customized for :  IEEE, LINUX, UNIX, MOTIF, F77
      INTEGER MPROJ,P_NONE,P_GNOMONIC,P_ORTHO,P_AZIMUTHAL
      INTEGER P_STEREO,P_LAMBERT,P_AITOFF,P_RADIO
      INTEGER TYPE_UN,TYPE_EQ,TYPE_GA,TYPE_HO
      INTEGER VEL_UNK,VEL_LSR,VEL_HEL,VEL_EAR,VEL_AUT,VEL_OBS
      INTEGER KIND_SPEC,KIND_CONT,KIND_SKY,KIND_ONOFF, KIND_FOCUS
      INTEGER A_VELO,A_FREQ,A_WAVE
      INTEGER MOD_FREQ,MOD_POS,MOD_FOLD
*
      PARAMETER (MPROJ  = 7     )        ! Number of supported projections
      PARAMETER (P_NONE  = 0    )        ! Unprojected data
      PARAMETER (P_GNOMONIC = 1 )        ! Radial Tangent plane
      PARAMETER (P_ORTHO = 2    )        ! Dixon Tangent plane
      PARAMETER (P_AZIMUTHAL = 3)        ! Schmidt Tangent plane
      PARAMETER (P_STEREO = 4   )        ! Stereographic
      PARAMETER (P_LAMBERT = 5  )        ! Lambert equal area
      PARAMETER (P_AITOFF = 6   )        ! Aitoff equal area
      PARAMETER (P_RADIO   = 7  )        ! Classic Single dish radio mapping
      PARAMETER (TYPE_UN = 1    )        ! Unknown system
      PARAMETER (TYPE_EQ = 2    )        ! Equatorial
      PARAMETER (TYPE_GA = 3    )        ! Galactic
      PARAMETER (TYPE_HO = 4    )        ! Horizontal
      PARAMETER (VEL_UNK = 0    )        ! Unsupported referential (planetary...)
      PARAMETER (VEL_LSR = 1    )        ! LSR referential
      PARAMETER (VEL_HEL = 2    )        ! Heliocentric referential
      PARAMETER (VEL_OBS = 3    )        ! Observatory referential
      PARAMETER (VEL_EAR = 4    )        ! Earth-Moon barycenter referential
      PARAMETER (VEL_AUT  = -1  )        ! Take referential from data
      PARAMETER (KIND_SPEC = 0  )        ! Spectroscopic data
      PARAMETER (KIND_CONT = 1  )        ! Continuum drift
      PARAMETER (KIND_SKY = 2   )        ! Skydip data
      PARAMETER (KIND_ONOFF = 3 )        ! Continuum on/off
      PARAMETER (KIND_FOCUS = 4)         ! Focus data
      PARAMETER (A_VELO  = 1    )        ! X unit is velocity
      PARAMETER (A_FREQ  = 2    )        ! X unit is frequency
      PARAMETER (A_WAVE  = 3    )        ! X unit is wavelength
      PARAMETER (MOD_FREQ = 0   )        ! Frequency switch
      PARAMETER (MOD_POS = 1    )        ! Position switch
      PARAMETER (MOD_FOLD = 2   )        ! Folded frequency switch
*
      CHARACTER*13 PROJNAM(0:MPROJ)
      DATA PROJNAM(0) /'NONE'/
      DATA PROJNAM(1) /'GNOMONIC'/
      DATA PROJNAM(2) /'ORTHOGRAPHIC'/
      DATA PROJNAM(3) /'AZIMUTHAL'/
      DATA PROJNAM(4) /'STEREOGRAPHIC'/
      DATA PROJNAM(5) /'LAMBERT'/
      DATA PROJNAM(6) /'AITOFF'/
      DATA PROJNAM(7) /'RADIO'/
      SAVE PROJNAM
*
