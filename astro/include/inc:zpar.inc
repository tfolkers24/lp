* Last processed by NICE on 19-Mar-2002 20:38:00
* Customized for :  IEEE, LINUX, UNIX, MOTIF, F77
      CHARACTER*120 Z_FILE               ! File name
      CHARACTER*12 Z_TYPE                ! Image type
      CHARACTER*12 Z_UNIT                ! Image unit
      CHARACTER*12 Z_CODE(4)             !	Axis type
      CHARACTER*12 Z_SYST                ! System type
      CHARACTER*12 Z_NAME                ! Source name
      CHARACTER*12 Z_LINE                ! Line name
      COMMON /ZCPAR/ Z_FILE,
     $Z_TYPE,Z_UNIT,Z_CODE,Z_SYST,Z_NAME,Z_LINE
      INTEGER*4 Z_ADDR                   ! start map address
      INTEGER*4 Z_SIZE                   ! mapped size
      INTEGER*4 Z_ISLO                   ! Image slot
      INTEGER*4 Z_MSLO                   ! Memory Slot
      INTEGER*4 Z_AL64                   ! 64 bit alignment
      LOGICAL Z_READ                     ! Read Only status
      LOGICAL Z_PA32                     ! Padding for 32 bit machines
      COMMON /ZIPAR/ Z_AL64,Z_SIZE,Z_ADDR,Z_ISLO,Z_MSLO,Z_READ,Z_PA32
      INTEGER*4 Z_BUFF(MXPAR)
      REAL*8 Z_CONV(3,4)
      EQUIVALENCE (Z_CONV(1,1),Z_REF1)
      EQUIVALENCE (Z_ITYP,Z_BUFF)
*
* /XPAR/
      INTEGER*4 Z_ITYP(3)                !  1
      INTEGER*4 Z_FORM                   !  4
      INTEGER*4 Z_NVB                    !  5
      INTEGER*4 Z_FILL(5)                !  6
      INTEGER*4 Z_GENE                   ! 11
      INTEGER*4 Z_NDIM                   ! 12
      INTEGER*4 Z_DIM(4)                 ! 13
      REAL*8 Z_REF1                      ! 17
      REAL*8 Z_VAL1                      ! 19
      REAL*8 Z_INC1                      ! 21
      REAL*8 Z_REF2                      ! 23
      REAL*8 Z_VAL2                      ! 25
      REAL*8 Z_INC2                      ! 27
      REAL*8 Z_REF3                      ! 29
      REAL*8 Z_VAL3                      ! 31
      REAL*8 Z_INC3                      ! 33
      REAL*8 Z_REF4                      ! 35
      REAL*8 Z_VAL4                      ! 37
      REAL*8 Z_INC4                      ! 39
*
      INTEGER*4 Z_BLAN                   ! 41
      REAL*4 Z_BVAL                      ! 42
      REAL*4 Z_EVAL                      ! 43
*
      INTEGER*4 Z_EXTR                   ! 44
      REAL*4 Z_RMIN                      ! 45
      REAL*4 Z_RMAX                      ! 46
      INTEGER*4 Z_MIN1                   ! 47
      INTEGER*4 Z_MAX1                   ! 48
      INTEGER*4 Z_MIN2                   ! 49
      INTEGER*4 Z_MAX2                   ! 50
      INTEGER*4 Z_MIN3                   ! 51
      INTEGER*4 Z_MAX3                   ! 52
      INTEGER*4 Z_MIN4                   ! 53
      INTEGER*4 Z_MAX4                   ! 54
*
      INTEGER*4 Z_DESC                   ! 55
      INTEGER*4 Z_IUNI(3)                ! 56
      INTEGER*4 Z_ICOD(3,4)              ! 59
      INTEGER*4 Z_ISYS(3)                ! 71
*
      INTEGER*4 Z_DUM1                   ! Void
      INTEGER*4 Z_POSI                   ! 74
      INTEGER*4 Z_ISOU(3)                ! 75,76,77
      REAL*8 Z_RA                        ! 78
      REAL*8 Z_DEC                       ! 80
      REAL*8 Z_LII                       ! 82
      REAL*8 Z_BII                       ! 84
      REAL*4 Z_EPOC                      ! 86
      INTEGER*4 Z_DUM2                   ! Void
*
      INTEGER*4 Z_PROJ                   ! 87
      INTEGER*4 Z_PTYP                   ! 88
      REAL*8 Z_A0                        ! 89
      REAL*8 Z_D0                        ! 91
      REAL*8 Z_PANG                      ! 93
      INTEGER*4 Z_XAXI                   ! 95
      INTEGER*4 Z_YAXI                   ! 96
*
      INTEGER*4 Z_SPEC                   ! 97
      INTEGER*4 Z_ILIN(3)                ! 98
      REAL*8 Z_FRES                      !101
      REAL*8 Z_FIMA                      !103
      REAL*8 Z_FREQ                      !105
      REAL*4 Z_VRES                      !107
      REAL*4 Z_VOFF                      !108
      INTEGER*4 Z_FAXI                   !109
*
      INTEGER*4 Z_RESO                   !110
      REAL*4 Z_MAJO                      !111
      REAL*4 Z_MINO                      !112
      REAL*4 Z_POSA                      !113
*
      INTEGER*4 Z_SIGMA                  !114
      REAL*4 Z_NOISE                     !115
      REAL*4 Z_RMS                       !116
*
      COMMON /ZPAR/Z_ITYP,Z_FORM,Z_NVB,Z_FILL,Z_GENE,Z_NDIM,Z_DIM,
     $Z_REF1,Z_VAL1,Z_INC1,Z_REF2,Z_VAL2,Z_INC2,Z_REF3,Z_VAL3,
     $Z_INC3,Z_REF4,Z_VAL4,Z_INC4,Z_BLAN,Z_BVAL,Z_EVAL,Z_EXTR,
     $Z_RMIN,Z_RMAX,Z_MIN1,Z_MAX1,Z_MIN2,Z_MAX2,Z_MIN3,Z_MAX3,
     $Z_MIN4,Z_MAX4,Z_DESC,Z_IUNI,Z_ICOD,Z_ISYS,Z_DUM1,Z_POSI,Z_ISOU,
     $Z_RA,Z_DEC,Z_LII,Z_BII,Z_EPOC,Z_DUM2,Z_PROJ,Z_PTYP,Z_A0,Z_D0,
     $Z_PANG,Z_XAXI,Z_YAXI,Z_SPEC,Z_ILIN,Z_FRES,Z_FIMA,Z_FREQ,
     $Z_VRES,Z_VOFF,Z_FAXI,Z_RESO,Z_MAJO,Z_MINO,Z_POSA,
     $Z_SIGMA,Z_NOISE,Z_RMS
*
      SAVE /ZPAR/, /ZCPAR/, /ZIPAR/
!
! Used for Windows
!
!MS$ATTRIBUTES DLLIMPORT :: ZPAR
!MS$ATTRIBUTES DLLIMPORT :: ZCPAR
!MS$ATTRIBUTES DLLIMPORT :: ZIPAR
