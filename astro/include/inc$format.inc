* Last processed by NICE on 19-Mar-2002 20:38:00
* Customized for :  IEEE, LINUX, UNIX, MOTIF, F77
* FORMAT.INC ------------------------------------------------------------
* Note : Continuation lines are prohibited in this module,
*        for Fortran-77 and Fortran-90 compatibility
* Note : these parameters must be negative
      INTEGER*4 VAX, EEEI, IEEE
      INTEGER*4 FMT_R4,FMT_R8,FMT_I4,FMT_L,FMT_I2,FMT_BY
      INTEGER*4 FMT_C4,FMT_C8,FMT_UN,HARDWARE
      INTEGER*4 VAX_R4,VAX_R8,VAX_C4,VAX_C8,VAX_I4,VAX_I2,VAX_L
      INTEGER*4 IEE_R4,IEE_R8,IEE_C4,IEE_C8,IEE_I4,IEE_I2,IEE_L
      INTEGER*4 EEI_R4,EEI_R8,EEI_C4,EEI_C8,EEI_I4,EEI_I2,EEI_L
      INTEGER*4 READ_ONLY,MHEAD
*
* Byte and untyped data
      PARAMETER (READ_ONLY=1000000,MHEAD=128)
      PARAMETER (FMT_BY=-6,FMT_UN=0)
*
* VAX reals imply D_FLOAT
      PARAMETER (VAX=0,VAX_R4=-1,VAX_R8=-2)
      PARAMETER (VAX_I4=-13,VAX_I2=-15,VAX_L=-4)
      PARAMETER (VAX_C4=-7,VAX_C8=-8)
*
* IEEE for non swapped machines (VAX like or DEC like)
      PARAMETER (IEEE=-10,IEE_R4=-11,IEE_R8=-12)
      PARAMETER (IEE_I4=-13,IEE_I2=-15,IEE_L=-14)
      PARAMETER (IEE_C4=-17,IEE_C8=-18)
*
* EEEI for swapped machines (IBM like)
      PARAMETER (EEEI=-20,EEI_R4=-21,EEI_R8=-22)
      PARAMETER (EEI_I4=-3,EEI_I2=-5,EEI_L=-24)
      PARAMETER (EEI_C4=-27,EEI_C8=-28)
*
C+VAX
c      PARAMETER (FMT_I4=VAX_I4,FMT_I2=VAX_I2,FMT_L=VAX_L)
c      PARAMETER (FMT_R4=VAX_R4,FMT_R8=VAX_R8)
C      PARAMETER (FMT_C4=VAX_C4,FMT_C8=VAX_C8,HARDWARE=VAX)
C-VAX
C+EEEI
c      PARAMETER (FMT_I4=EEI_I4,FMT_I2=EEI_I2,FMT_L=EEI_L)
c      PARAMETER (FMT_R4=EEI_R4,FMT_R8=EEI_R8)
c      PARAMETER (FMT_C4=EEI_C4,FMT_C8=EEI_C8,HARDWARE=EEEI)
C-EEEI
C+IEEE
      PARAMETER (FMT_I4=IEE_I4,FMT_I2=IEE_I2,FMT_L=IEE_L)
      PARAMETER (FMT_R4=IEE_R4,FMT_R8=IEE_R8)
      PARAMETER (FMT_C4=IEE_C4,FMT_C8=IEE_C8,HARDWARE=IEEE)
C-IEEE
* ------------------------------------------------------------------------
* Size unit for unformatted files. Recordsizes are counted in words by DEC
* and in bytes by every other constructor.
      INTEGER FACUNF
C+VMS+ULTRIX+OSF
c      PARAMETER (FACUNF=1)
C-VMS-ULTRIX-OSF
C+UNIX-ULTRIX-OSF
      PARAMETER (FACUNF=4)
C-UNIX-ULTRIX-OSF
