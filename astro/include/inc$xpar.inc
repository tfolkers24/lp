* Last processed by NICE on 19-Mar-2002 20:38:00
* Customized for :  IEEE, LINUX, UNIX, MOTIF, F77
      INTEGER*4 MXCPAR,MXIPAR,MXPAR
      PARAMETER (MXPAR=128)              ! 128 header integers
      PARAMETER (MXIPAR=6)               ! 5 Extra integers
      PARAMETER (MXCPAR=9)               ! 9 character*12 strings
      CHARACTER*120 X_FILE               ! File name
      CHARACTER*12 X_TYPE                ! Image type
      CHARACTER*12 X_UNIT                ! Image unit
      CHARACTER*12 X_CODE(4)             !	Axis type
      CHARACTER*12 X_SYST                ! System type
      CHARACTER*12 X_NAME                ! Source name
      CHARACTER*12 X_LINE                ! Line name
      COMMON /XCPAR/ X_FILE,
     $X_TYPE,X_UNIT,X_CODE,X_SYST,X_NAME,X_LINE
      INTEGER*4 X_ADDR                   ! start map address
      INTEGER*4 X_SIZE                   ! mapped size
      INTEGER*4 X_ISLO                   ! Image slot
      INTEGER*4 X_MSLO                   ! Memory Slot
      INTEGER*4 X_AL64                   ! 64 bit alignment
      LOGICAL X_READ                     ! Read Only status
      LOGICAL X_PA32                     ! Padding for 32 bit machines
      COMMON /XIPAR/ X_AL64,X_SIZE,X_ADDR,X_ISLO,X_MSLO,X_READ,X_PA32
      INTEGER*4 X_BUFF(MXPAR)
      REAL*8 X_CONV(3,4)
      EQUIVALENCE (X_CONV(1,1),X_REF1)
      EQUIVALENCE (X_ITYP,X_BUFF)
*
* /XPAR/
      INTEGER*4 X_ITYP(3)                !  1
      INTEGER*4 X_FORM                   !  4
      INTEGER*4 X_NVB                    !  5
      INTEGER*4 X_FILL(5)                !  6
      INTEGER*4 X_GENE                   ! 11
      INTEGER*4 X_NDIM                   ! 12
      INTEGER*4 X_DIM(4)                 ! 13
      REAL*8 X_REF1                      ! 17
      REAL*8 X_VAL1                      ! 19
      REAL*8 X_INC1                      ! 21
      REAL*8 X_REF2                      ! 23
      REAL*8 X_VAL2                      ! 25
      REAL*8 X_INC2                      ! 27
      REAL*8 X_REF3                      ! 29
      REAL*8 X_VAL3                      ! 31
      REAL*8 X_INC3                      ! 33
      REAL*8 X_REF4                      ! 35
      REAL*8 X_VAL4                      ! 37
      REAL*8 X_INC4                      ! 39
*
      INTEGER*4 X_BLAN                   ! 41
      REAL*4 X_BVAL                      ! 42
      REAL*4 X_EVAL                      ! 43
*
      INTEGER*4 X_EXTR                   ! 44
      REAL*4 X_RMIN                      ! 45
      REAL*4 X_RMAX                      ! 46
      INTEGER*4 X_MIN1                   ! 47
      INTEGER*4 X_MAX1                   ! 48
      INTEGER*4 X_MIN2                   ! 49
      INTEGER*4 X_MAX2                   ! 50
      INTEGER*4 X_MIN3                   ! 51
      INTEGER*4 X_MAX3                   ! 52
      INTEGER*4 X_MIN4                   ! 53
      INTEGER*4 X_MAX4                   ! 54
*
      INTEGER*4 X_DESC                   ! 55
      INTEGER*4 X_IUNI(3)                ! 56
      INTEGER*4 X_ICOD(3,4)              ! 59
      INTEGER*4 X_ISYS(3)                ! 71
*
      INTEGER*4 X_DUM1                   ! Void
      INTEGER*4 X_POSI                   ! 74
      INTEGER*4 X_ISOU(3)                ! 75,76,77
      REAL*8 X_RA                        ! 78
      REAL*8 X_DEC                       ! 80
      REAL*8 X_LII                       ! 82
      REAL*8 X_BII                       ! 84
      REAL*4 X_EPOC                      ! 86
      INTEGER*4 X_DUM2                   ! Void
*
      INTEGER*4 X_PROJ                   ! 87
      INTEGER*4 X_PTYP                   ! 88
      REAL*8 X_A0                        ! 89
      REAL*8 X_D0                        ! 91
      REAL*8 X_PANG                      ! 93
      INTEGER*4 X_XAXI                   ! 95
      INTEGER*4 X_YAXI                   ! 96
*
      INTEGER*4 X_SPEC                   ! 97
      INTEGER*4 X_ILIN(3)                ! 98
      REAL*8 X_FRES                      !101
      REAL*8 X_FIMA                      !103
      REAL*8 X_FREQ                      !105
      REAL*4 X_VRES                      !107
      REAL*4 X_VOFF                      !108
      INTEGER*4 X_FAXI                   !109
*
      INTEGER*4 X_RESO                   !110
      REAL*4 X_MAJO                      !111
      REAL*4 X_MINO                      !112
      REAL*4 X_POSA                      !113
*
      INTEGER*4 X_SIGMA                  !114
      REAL*4 X_NOISE                     !115
      REAL*4 X_RMS                       !116
      COMMON /XPAR/X_ITYP,X_FORM,X_NVB,X_FILL,X_GENE,X_NDIM,X_DIM,
     $X_REF1,X_VAL1,X_INC1,X_REF2,X_VAL2,X_INC2,X_REF3,X_VAL3,
     $X_INC3,X_REF4,X_VAL4,X_INC4,X_BLAN,X_BVAL,X_EVAL,X_EXTR,
     $X_RMIN,X_RMAX,X_MIN1,X_MAX1,X_MIN2,X_MAX2,X_MIN3,X_MAX3,
     $X_MIN4,X_MAX4,X_DESC,X_IUNI,X_ICOD,X_ISYS,X_DUM1,X_POSI,X_ISOU,
     $X_RA,X_DEC,X_LII,X_BII,X_EPOC,X_DUM2,X_PROJ,X_PTYP,X_A0,X_D0,
     $X_PANG,X_XAXI,X_YAXI,X_SPEC,X_ILIN,X_FRES,X_FIMA,X_FREQ,
     $X_VRES,X_VOFF,X_FAXI,X_RESO,X_MAJO,X_MINO,X_POSA,
     $X_SIGMA,X_NOISE,X_RMS
*
      SAVE /XPAR/, /XCPAR/, /XIPAR/
!
! Used for Windows
!
!MS$ATTRIBUTES DLLIMPORT :: XPAR
!MS$ATTRIBUTES DLLIMPORT :: XCPAR
!MS$ATTRIBUTES DLLIMPORT :: XIPAR
!
