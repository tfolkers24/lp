* Last processed by NICE on 19-Mar-2002 20:38:00
* Customized for :  IEEE, LINUX, UNIX, MOTIF, F77
      CHARACTER*120 Y_FILE               ! File name
      CHARACTER*12 Y_TYPE                ! Image type
      CHARACTER*12 Y_UNIT                ! Image unit
      CHARACTER*12 Y_CODE(4)             !	Axis type
      CHARACTER*12 Y_SYST                ! System type
      CHARACTER*12 Y_NAME                ! Source name
      CHARACTER*12 Y_LINE                ! Line name
      COMMON /YCPAR/ Y_FILE,
     $Y_TYPE,Y_UNIT,Y_CODE,Y_SYST,Y_NAME,Y_LINE
      INTEGER*4 Y_ADDR                   ! start map address
      INTEGER*4 Y_SIZE                   ! mapped size
      INTEGER*4 Y_ISLO                   ! Image slot
      INTEGER*4 Y_MSLO                   ! Memory Slot
      INTEGER*4 Y_AL64                   ! 64 bit alignment
      LOGICAL Y_READ                     ! Read Only status
      LOGICAL Y_PA32                     ! Padding for 32 bit machines
      COMMON /YIPAR/ Y_AL64,Y_SIZE,Y_ADDR,Y_ISLO,Y_MSLO,Y_READ,Y_PA32
      INTEGER*4 Y_BUFF(MXPAR)
      REAL*8 Y_CONV(3,4)
      EQUIVALENCE (Y_CONV(1,1),Y_REF1)
      EQUIVALENCE (Y_ITYP,Y_BUFF)
*
* /XPAR/
      INTEGER*4 Y_ITYP(3)                !  1
      INTEGER*4 Y_FORM                   !  4
      INTEGER*4 Y_NVB                    !  5
      INTEGER*4 Y_FILL(5)                !  6
      INTEGER*4 Y_GENE                   ! 11
      INTEGER*4 Y_NDIM                   ! 12
      INTEGER*4 Y_DIM(4)                 ! 13
      REAL*8 Y_REF1                      ! 17
      REAL*8 Y_VAL1                      ! 19
      REAL*8 Y_INC1                      ! 21
      REAL*8 Y_REF2                      ! 23
      REAL*8 Y_VAL2                      ! 25
      REAL*8 Y_INC2                      ! 27
      REAL*8 Y_REF3                      ! 29
      REAL*8 Y_VAL3                      ! 31
      REAL*8 Y_INC3                      ! 33
      REAL*8 Y_REF4                      ! 35
      REAL*8 Y_VAL4                      ! 37
      REAL*8 Y_INC4                      ! 39
*
      INTEGER*4 Y_BLAN                   ! 41
      REAL*4 Y_BVAL                      ! 42
      REAL*4 Y_EVAL                      ! 43
*
      INTEGER*4 Y_EXTR                   ! 44
      REAL*4 Y_RMIN                      ! 45
      REAL*4 Y_RMAX                      ! 46
      INTEGER*4 Y_MIN1                   ! 47
      INTEGER*4 Y_MAX1                   ! 48
      INTEGER*4 Y_MIN2                   ! 49
      INTEGER*4 Y_MAX2                   ! 50
      INTEGER*4 Y_MIN3                   ! 51
      INTEGER*4 Y_MAX3                   ! 52
      INTEGER*4 Y_MIN4                   ! 53
      INTEGER*4 Y_MAX4                   ! 54
*
      INTEGER*4 Y_DESC                   ! 55
      INTEGER*4 Y_IUNI(3)                ! 56
      INTEGER*4 Y_ICOD(3,4)              ! 59
      INTEGER*4 Y_ISYS(3)                ! 71
*
      INTEGER*4 Y_DUM1                   ! Void
      INTEGER*4 Y_POSI                   ! 74
      INTEGER*4 Y_ISOU(3)                ! 75,76,77
      REAL*8 Y_RA                        ! 78
      REAL*8 Y_DEC                       ! 80
      REAL*8 Y_LII                       ! 82
      REAL*8 Y_BII                       ! 84
      REAL*4 Y_EPOC                      ! 86
      INTEGER*4 Y_DUM2                   ! Void
*
      INTEGER*4 Y_PROJ                   ! 87
      INTEGER*4 Y_PTYP                   ! 88
      REAL*8 Y_A0                        ! 89
      REAL*8 Y_D0                        ! 91
      REAL*8 Y_PANG                      ! 93
      INTEGER*4 Y_XAXI                   ! 95
      INTEGER*4 Y_YAXI                   ! 96
*
      INTEGER*4 Y_SPEC                   ! 97
      INTEGER*4 Y_ILIN(3)                ! 98
      REAL*8 Y_FRES                      !101
      REAL*8 Y_FIMA                      !103
      REAL*8 Y_FREQ                      !105
      REAL*4 Y_VRES                      !107
      REAL*4 Y_VOFF                      !108
      INTEGER*4 Y_FAXI                   !109
*
      INTEGER*4 Y_RESO                   !110
      REAL*4 Y_MAJO                      !111
      REAL*4 Y_MINO                      !112
      REAL*4 Y_POSA                      !113
*
      INTEGER*4 Y_SIGMA                  !114
      REAL*4 Y_NOISE                     !115
      REAL*4 Y_RMS                       !116
      COMMON /YPAR/Y_ITYP,Y_FORM,Y_NVB,Y_FILL,Y_GENE,Y_NDIM,Y_DIM,
     $Y_REF1,Y_VAL1,Y_INC1,Y_REF2,Y_VAL2,Y_INC2,Y_REF3,Y_VAL3,
     $Y_INC3,Y_REF4,Y_VAL4,Y_INC4,Y_BLAN,Y_BVAL,Y_EVAL,Y_EXTR,
     $Y_RMIN,Y_RMAX,Y_MIN1,Y_MAX1,Y_MIN2,Y_MAX2,Y_MIN3,Y_MAX3,
     $Y_MIN4,Y_MAX4,Y_DESC,Y_IUNI,Y_ICOD,Y_ISYS,Y_DUM1,Y_POSI,Y_ISOU,
     $Y_RA,Y_DEC,Y_LII,Y_BII,Y_EPOC,Y_DUM2,Y_PROJ,Y_PTYP,Y_A0,Y_D0,
     $Y_PANG,Y_XAXI,Y_YAXI,Y_SPEC,Y_ILIN,Y_FRES,Y_FIMA,Y_FREQ,
     $Y_VRES,Y_VOFF,Y_FAXI,Y_RESO,Y_MAJO,Y_MINO,Y_POSA,
     $Y_SIGMA,Y_NOISE,Y_RMS
*
      SAVE /YPAR/, /YCPAR/, /YIPAR/
!
! Used for Windows
!
!MS$ATTRIBUTES DLLIMPORT :: YPAR
!MS$ATTRIBUTES DLLIMPORT :: YCPAR
!MS$ATTRIBUTES DLLIMPORT :: YIPAR
