* Last processed by NICE on 19-Mar-2002 20:38:00
* Customized for :  IEEE, LINUX, UNIX, MOTIF, F77
* PI is defined with more digits than necessary to avoid losing
* the last few bits in the decimal to binary conversion
      REAL*8 PI
      PARAMETER (PI=3.14159265358979323846D0)
      REAL*4 PIS
      PARAMETER (PIS=3.141592653)
* Relative precision of REAL*4
      REAL*4 EPSR4
      PARAMETER (EPSR4=1E-7)
      REAL*4 MAXR4
      PARAMETER (MAXR4=1E38)
* Maximum acceptable integer
      INTEGER MAX_INTEG
      PARAMETER (MAX_INTEG=2147483647)
