#!/bin/csh -f
#
echo "$0 $1 $2 $3"
#
if ( "$1" == "" || "$2" == "" || "$3" == "" || "$4" == "" ) then
  echo "Usage: $0 freq time date site"
  echo "Where Frequency = 100.0-1000.0 GHz"
  echo "Where time is in the form: hh:mm:ss"
  echo "Where date is in the form: dd-Mon-yyyy"
  echo "Where site is Either SMT or KITT_PEAK"
  exit
endif
#
#set utinfo=`date -u | awk '{printf("%s %s-%s-%s\n", $4, $3,$2,$6)}'` 
#set utinfo="23:55:00 29-Mar-2015"
set utinfo="$2 $3"
echo observatory $4 > /tmp/eph1$$
echo time $utinfo >> /tmp/eph1$$
echo let frequency $1 >> /tmp/eph1$$
if ( "$4" == "SMT" ) then
  echo "let sun_limit 41" >> /tmp/eph1$$
else
  echo "let sun_limit 5" >> /tmp/eph1$$
endif
echo planet >> /tmp/eph1$$
echo exit >> /tmp/eph1$$

/home/analysis/gag/linux-intel/bin/astro < /tmp/eph1$$

rm -f /tmp/eph1*
