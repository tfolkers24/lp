%{

/*
 * Cactus File @(#)mkgs.y	1.2
 *         SID 1.2
 *        Date 8/6/96
 */

#include <stdio.h>
#include <ctype.h>


int yylex(void);
int yyerror(char *s);
int ignore(void);
struct GSKEY *new_gs(void);
int add_notchar(int v, char *name);
int add_array(int v, char *name, char *sz);
int comment(char *p);
int add_char(char *name, char *sz);
int got_construct(char *name);
char *get_field(int f);
char *get_log(int l);
char *get_rambo(int l);

%}

%union {
         char *var;
         int tok;
       }

%start list
%token STRUCT LBRACE RBRACE SEMI LB RB CHARSTAR
%token <tok> DOUBLE INTEGER LONG FLOAT SHORT UNSIGNED
%token <var> VAR COMMENT

%type <var> construct
%type <tok> notchar
%type <tok> typepp

%%

list :
     | list construct
       { got_construct($2); }
     | list error 
        { yyerrok; yyclearin; YYACCEPT; }
     | list COMMENT 
     ;

construct : STRUCT VAR LBRACE vars RBRACE SEMI
       { $$ = $2; }
      ;

vars : vars line 
     | line
     ;

line : COMMENT
       { comment($1); }
     | CHARSTAR VAR LB VAR RB SEMI
       { add_char($2, $4 ); }
     | notchar VAR SEMI
       { add_notchar( $1, $2); }
     | notchar VAR LB VAR RB SEMI
       { add_array($1, $2, $4); }
     | notchar VAR LB VAR RB LB VAR RB SEMI
       { printf( "/* skipping %s */\n", $2 );}
     | notchar VAR LB VAR RB LB VAR RB LB VAR RB SEMI
       { printf( "/* skipping %s */\n", $2 );}
     | STRUCT VAR VAR LB VAR RB SEMI
       { printf( "/* skipping struct array %s %s */\n", $2, $3 );}
     | STRUCT VAR VAR SEMI
     ;

notchar: typepp
         { $$ = $1; };
       | UNSIGNED typepp
         { $$ = $2; };
       ;

typepp: DOUBLE
         { $$ = $1; };
       | FLOAT
         { $$ = $1; };
       | LONG
         { $$ = $1; };
       | INTEGER
         { $$ = $1; };
       | SHORT
         { $$ = $1; };
       ;


%%

int yyerror(s)
char *s;
{
  extern int count;

  printf( "%s at line %d\n", s, count );

  return(0);
}

int yywrap() 
{ 
  return(1);
}
