#ifndef GM_INIT_H
#define GM_INIT_H

struct GSKEY {
  void   *offset;/* Offset into structure in byte*/
  char   *name;  /* Human readable form of variable */
  int     type;  /* INTEGER FLOAT DOUBLE CHARSTAR */
  int     size;  /* 1 if not array                */
  int    *iaddr;  /* Address of Char variable in env struct */ 
  char   *caddr;  /* Address of Int variable in env struct */
  double *daddr;  /* Address of Double variable in env struct */
};

#endif
