%{
/*
 * Cactus File @(#)mkgs.l	1.2
 *         SID 1.2
 *        Date 8/6/96
 */
#include "y.tab.h"
#define SKIP(x) if( cf ) return( x )
extern int count;
int cf = 1;
int    ignore();
double atof();
%}
%%
struct                    { SKIP(STRUCT);   }
int                       { yylval.tok = INTEGER; SKIP(INTEGER);  }
long                      { yylval.tok = LONG;    SKIP(LONG);     }
double                    { yylval.tok = DOUBLE;  SKIP(DOUBLE);   }
float                     { yylval.tok = FLOAT;   SKIP(FLOAT);    }
char                      { SKIP(CHARSTAR); }
short                     { SKIP(SHORT); }
unsigned                  { SKIP(UNSIGNED); }
\;                        { SKIP(SEMI); }
\{                        { SKIP(LBRACE); }
\}                        { SKIP(RBRACE); }
\[                        { SKIP(LB); }
\]                        { SKIP(RB); }
\/\*[^\n]+\*\/                { yylval.var = (char *)malloc( strlen(yytext)+1);
                            strcpy( yylval.var, yytext);cf=1;return( COMMENT );}
\/\*[^*]*\n               { cf = 0; count++; }
\*\/                      { cf = 1; }
extern.*\n                { count++; /* ignore */ }
[ \t]                     { ; /* ignore */ }
[A-Za-z0-9_*]+ { yylval.var = (char *)malloc( strlen(yytext)+1);
                       strcpy( yylval.var, yytext );SKIP( VAR );}
\#.*\n              { count++; /* ignore */ }
[ \t]               { ; /* ignore */ }
[\n]               { count++; ignore(); /* ignore */ }
[^ ]              { SKIP( yytext[0] ); }
%%
