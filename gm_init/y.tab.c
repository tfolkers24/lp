/* original parser id follows */
/* yysccsid[] = "@(#)yaccpar	1.9 (Berkeley) 02/21/93" */
/* (use YYMAJOR/YYMINOR for ifdefs dependent on parser version) */

#define YYBYACC 1
#define YYMAJOR 2
#define YYMINOR 0
#define YYPATCH 20210808

#define YYEMPTY        (-1)
#define yyclearin      (yychar = YYEMPTY)
#define yyerrok        (yyerrflag = 0)
#define YYRECOVERING() (yyerrflag != 0)
#define YYENOMEM       (-2)
#define YYEOF          0
#define YYPREFIX "yy"

#define YYPURE 0

#line 2 "mkgs.y"

/*
 * Cactus File @(#)mkgs.y	1.2
 *         SID 1.2
 *        Date 8/6/96
 */

#include <stdio.h>
#include <ctype.h>


int yylex(void);
int yyerror(char *s);
int ignore(void);
struct GSKEY *new_gs(void);
int add_notchar(int v, char *name);
int add_array(int v, char *name, char *sz);
int comment(char *p);
int add_char(char *name, char *sz);
int got_construct(char *name);
char *get_field(int f);
char *get_log(int l);
char *get_rambo(int l);

#ifdef YYSTYPE
#undef  YYSTYPE_IS_DECLARED
#define YYSTYPE_IS_DECLARED 1
#endif
#ifndef YYSTYPE_IS_DECLARED
#define YYSTYPE_IS_DECLARED 1
#line 28 "mkgs.y"
typedef union YYSTYPE {
         char *var;
         int tok;
       } YYSTYPE;
#endif /* !YYSTYPE_IS_DECLARED */
#line 58 "y.tab.c"

/* compatibility with bison */
#ifdef YYPARSE_PARAM
/* compatibility with FreeBSD */
# ifdef YYPARSE_PARAM_TYPE
#  define YYPARSE_DECL() yyparse(YYPARSE_PARAM_TYPE YYPARSE_PARAM)
# else
#  define YYPARSE_DECL() yyparse(void *YYPARSE_PARAM)
# endif
#else
# define YYPARSE_DECL() yyparse(void)
#endif

/* Parameters sent to lex. */
#ifdef YYLEX_PARAM
# define YYLEX_DECL() yylex(void *YYLEX_PARAM)
# define YYLEX yylex(YYLEX_PARAM)
#else
# define YYLEX_DECL() yylex(void)
# define YYLEX yylex()
#endif

#if !(defined(yylex) || defined(YYSTATE))
int YYLEX_DECL();
#endif

/* Parameters sent to yyerror. */
#ifndef YYERROR_DECL
#define YYERROR_DECL() yyerror(const char *s)
#endif
#ifndef YYERROR_CALL
#define YYERROR_CALL(msg) yyerror(msg)
#endif

extern int YYPARSE_DECL();

#define STRUCT 257
#define LBRACE 258
#define RBRACE 259
#define SEMI 260
#define LB 261
#define RB 262
#define CHARSTAR 263
#define DOUBLE 264
#define INTEGER 265
#define LONG 266
#define FLOAT 267
#define SHORT 268
#define UNSIGNED 269
#define VAR 270
#define COMMENT 271
#define YYERRCODE 256
typedef int YYINT;
static const YYINT yylhs[] = {                           -1,
    0,    0,    0,    0,    1,    4,    4,    5,    5,    5,
    5,    5,    5,    5,    5,    2,    2,    3,    3,    3,
    3,    3,
};
static const YYINT yylen[] = {                            2,
    0,    2,    2,    2,    6,    2,    1,    1,    6,    3,
    6,    9,   12,    7,    4,    1,    2,    1,    1,    1,
    1,    1,
};
static const YYINT yydefred[] = {                         1,
    0,    3,    0,    4,    2,    0,    0,    0,    0,   18,
   21,   20,   19,   22,    0,    8,    0,   16,    0,    7,
    0,    0,   17,    0,    0,    6,    0,    0,   10,    0,
    5,   15,    0,    0,    0,    0,    0,    0,    0,    9,
   11,    0,   14,    0,    0,   12,    0,    0,    0,   13,
};
static const YYINT yydgoto[] = {                          1,
    5,   17,   18,   19,   20,
};
static const YYINT yysindex[] = {                         0,
 -256,    0, -267,    0,    0, -253, -240, -232, -231,    0,
    0,    0,    0,    0, -246,    0, -230,    0, -255,    0,
 -229, -219,    0, -254, -217,    0, -228, -225,    0, -223,
    0,    0, -222, -218, -216, -213, -210, -226, -209,    0,
    0, -215,    0, -208, -224,    0, -214, -205, -207,    0,
};
static const YYINT yyrindex[] = {                         0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
};
static const YYINT yygindex[] = {                         0,
    0,    0,   15,    0,   33,
};
#define YYTABLESIZE 57
static const YYINT yytable[] = {                          2,
    3,    8,    6,   25,    7,   29,   30,    9,   10,   11,
   12,   13,   14,   15,    4,   16,    8,   10,   11,   12,
   13,   14,    9,   10,   11,   12,   13,   14,   15,   23,
   16,   32,   33,   41,   42,   46,   47,   21,   22,   24,
   27,   28,   31,   37,   34,   38,   35,   36,   39,   40,
   43,   26,   50,   45,   44,   48,   49,
};
static const YYINT yycheck[] = {                        256,
  257,  257,  270,  259,  258,  260,  261,  263,  264,  265,
  266,  267,  268,  269,  271,  271,  257,  264,  265,  266,
  267,  268,  263,  264,  265,  266,  267,  268,  269,   15,
  271,  260,  261,  260,  261,  260,  261,  270,  270,  270,
  270,  261,  260,  262,  270,  262,  270,  270,  262,  260,
  260,   19,  260,  262,  270,  270,  262,
};
#define YYFINAL 1
#ifndef YYDEBUG
#define YYDEBUG 1
#endif
#define YYMAXTOKEN 271
#define YYUNDFTOKEN 279
#define YYTRANSLATE(a) ((a) > YYMAXTOKEN ? YYUNDFTOKEN : (a))
#if YYDEBUG
static const char *const yyname[] = {

"end-of-file",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,"STRUCT","LBRACE","RBRACE","SEMI",
"LB","RB","CHARSTAR","DOUBLE","INTEGER","LONG","FLOAT","SHORT","UNSIGNED","VAR",
"COMMENT",0,0,0,0,0,0,0,"illegal-symbol",
};
static const char *const yyrule[] = {
"$accept : list",
"list :",
"list : list construct",
"list : list error",
"list : list COMMENT",
"construct : STRUCT VAR LBRACE vars RBRACE SEMI",
"vars : vars line",
"vars : line",
"line : COMMENT",
"line : CHARSTAR VAR LB VAR RB SEMI",
"line : notchar VAR SEMI",
"line : notchar VAR LB VAR RB SEMI",
"line : notchar VAR LB VAR RB LB VAR RB SEMI",
"line : notchar VAR LB VAR RB LB VAR RB LB VAR RB SEMI",
"line : STRUCT VAR VAR LB VAR RB SEMI",
"line : STRUCT VAR VAR SEMI",
"notchar : typepp",
"notchar : UNSIGNED typepp",
"typepp : DOUBLE",
"typepp : FLOAT",
"typepp : LONG",
"typepp : INTEGER",
"typepp : SHORT",

};
#endif

#if YYDEBUG
int      yydebug;
#endif

int      yyerrflag;
int      yychar;
YYSTYPE  yyval;
YYSTYPE  yylval;
int      yynerrs;

/* define the initial stack-sizes */
#ifdef YYSTACKSIZE
#undef YYMAXDEPTH
#define YYMAXDEPTH  YYSTACKSIZE
#else
#ifdef YYMAXDEPTH
#define YYSTACKSIZE YYMAXDEPTH
#else
#define YYSTACKSIZE 10000
#define YYMAXDEPTH  10000
#endif
#endif

#define YYINITSTACKSIZE 200

typedef struct {
    unsigned stacksize;
    YYINT    *s_base;
    YYINT    *s_mark;
    YYINT    *s_last;
    YYSTYPE  *l_base;
    YYSTYPE  *l_mark;
} YYSTACKDATA;
/* variables for the parser stack */
static YYSTACKDATA yystack;
#line 97 "mkgs.y"

int yyerror(s)
char *s;
{
  extern int count;

  printf( "%s at line %d\n", s, count );

  return(0);
}

int yywrap() 
{ 
  return(1);
}
#line 265 "y.tab.c"

#if YYDEBUG
#include <stdio.h>	/* needed for printf */
#endif

#include <stdlib.h>	/* needed for malloc, etc */
#include <string.h>	/* needed for memset */

/* allocate initial stack or double stack size, up to YYMAXDEPTH */
static int yygrowstack(YYSTACKDATA *data)
{
    int i;
    unsigned newsize;
    YYINT *newss;
    YYSTYPE *newvs;

    if ((newsize = data->stacksize) == 0)
        newsize = YYINITSTACKSIZE;
    else if (newsize >= YYMAXDEPTH)
        return YYENOMEM;
    else if ((newsize *= 2) > YYMAXDEPTH)
        newsize = YYMAXDEPTH;

    i = (int) (data->s_mark - data->s_base);
    newss = (YYINT *)realloc(data->s_base, newsize * sizeof(*newss));
    if (newss == 0)
        return YYENOMEM;

    data->s_base = newss;
    data->s_mark = newss + i;

    newvs = (YYSTYPE *)realloc(data->l_base, newsize * sizeof(*newvs));
    if (newvs == 0)
        return YYENOMEM;

    data->l_base = newvs;
    data->l_mark = newvs + i;

    data->stacksize = newsize;
    data->s_last = data->s_base + newsize - 1;
    return 0;
}

#if YYPURE || defined(YY_NO_LEAKS)
static void yyfreestack(YYSTACKDATA *data)
{
    free(data->s_base);
    free(data->l_base);
    memset(data, 0, sizeof(*data));
}
#else
#define yyfreestack(data) /* nothing */
#endif

#define YYABORT  goto yyabort
#define YYREJECT goto yyabort
#define YYACCEPT goto yyaccept
#define YYERROR  goto yyerrlab

int
YYPARSE_DECL()
{
    int yym, yyn, yystate;
#if YYDEBUG
    const char *yys;

    if ((yys = getenv("YYDEBUG")) != 0)
    {
        yyn = *yys;
        if (yyn >= '0' && yyn <= '9')
            yydebug = yyn - '0';
    }
#endif

    /* yym is set below */
    /* yyn is set below */
    yynerrs = 0;
    yyerrflag = 0;
    yychar = YYEMPTY;
    yystate = 0;

#if YYPURE
    memset(&yystack, 0, sizeof(yystack));
#endif

    if (yystack.s_base == NULL && yygrowstack(&yystack) == YYENOMEM) goto yyoverflow;
    yystack.s_mark = yystack.s_base;
    yystack.l_mark = yystack.l_base;
    yystate = 0;
    *yystack.s_mark = 0;

yyloop:
    if ((yyn = yydefred[yystate]) != 0) goto yyreduce;
    if (yychar < 0)
    {
        yychar = YYLEX;
        if (yychar < 0) yychar = YYEOF;
#if YYDEBUG
        if (yydebug)
        {
            if ((yys = yyname[YYTRANSLATE(yychar)]) == NULL) yys = yyname[YYUNDFTOKEN];
            printf("%sdebug: state %d, reading %d (%s)\n",
                    YYPREFIX, yystate, yychar, yys);
        }
#endif
    }
    if (((yyn = yysindex[yystate]) != 0) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == (YYINT) yychar)
    {
#if YYDEBUG
        if (yydebug)
            printf("%sdebug: state %d, shifting to state %d\n",
                    YYPREFIX, yystate, yytable[yyn]);
#endif
        if (yystack.s_mark >= yystack.s_last && yygrowstack(&yystack) == YYENOMEM) goto yyoverflow;
        yystate = yytable[yyn];
        *++yystack.s_mark = yytable[yyn];
        *++yystack.l_mark = yylval;
        yychar = YYEMPTY;
        if (yyerrflag > 0)  --yyerrflag;
        goto yyloop;
    }
    if (((yyn = yyrindex[yystate]) != 0) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == (YYINT) yychar)
    {
        yyn = yytable[yyn];
        goto yyreduce;
    }
    if (yyerrflag != 0) goto yyinrecovery;

    YYERROR_CALL("syntax error");

    goto yyerrlab; /* redundant goto avoids 'unused label' warning */
yyerrlab:
    ++yynerrs;

yyinrecovery:
    if (yyerrflag < 3)
    {
        yyerrflag = 3;
        for (;;)
        {
            if (((yyn = yysindex[*yystack.s_mark]) != 0) && (yyn += YYERRCODE) >= 0 &&
                    yyn <= YYTABLESIZE && yycheck[yyn] == (YYINT) YYERRCODE)
            {
#if YYDEBUG
                if (yydebug)
                    printf("%sdebug: state %d, error recovery shifting\
 to state %d\n", YYPREFIX, *yystack.s_mark, yytable[yyn]);
#endif
                if (yystack.s_mark >= yystack.s_last && yygrowstack(&yystack) == YYENOMEM) goto yyoverflow;
                yystate = yytable[yyn];
                *++yystack.s_mark = yytable[yyn];
                *++yystack.l_mark = yylval;
                goto yyloop;
            }
            else
            {
#if YYDEBUG
                if (yydebug)
                    printf("%sdebug: error recovery discarding state %d\n",
                            YYPREFIX, *yystack.s_mark);
#endif
                if (yystack.s_mark <= yystack.s_base) goto yyabort;
                --yystack.s_mark;
                --yystack.l_mark;
            }
        }
    }
    else
    {
        if (yychar == YYEOF) goto yyabort;
#if YYDEBUG
        if (yydebug)
        {
            if ((yys = yyname[YYTRANSLATE(yychar)]) == NULL) yys = yyname[YYUNDFTOKEN];
            printf("%sdebug: state %d, error recovery discards token %d (%s)\n",
                    YYPREFIX, yystate, yychar, yys);
        }
#endif
        yychar = YYEMPTY;
        goto yyloop;
    }

yyreduce:
#if YYDEBUG
    if (yydebug)
        printf("%sdebug: state %d, reducing by rule %d (%s)\n",
                YYPREFIX, yystate, yyn, yyrule[yyn]);
#endif
    yym = yylen[yyn];
    if (yym > 0)
        yyval = yystack.l_mark[1-yym];
    else
        memset(&yyval, 0, sizeof yyval);

    switch (yyn)
    {
case 2:
#line 46 "mkgs.y"
	{ got_construct(yystack.l_mark[0].var); }
#line 467 "y.tab.c"
break;
case 3:
#line 48 "mkgs.y"
	{ yyerrok; yyclearin; YYACCEPT; }
#line 472 "y.tab.c"
break;
case 5:
#line 53 "mkgs.y"
	{ yyval.var = yystack.l_mark[-4].var; }
#line 477 "y.tab.c"
break;
case 8:
#line 61 "mkgs.y"
	{ comment(yystack.l_mark[0].var); }
#line 482 "y.tab.c"
break;
case 9:
#line 63 "mkgs.y"
	{ add_char(yystack.l_mark[-4].var, yystack.l_mark[-2].var ); }
#line 487 "y.tab.c"
break;
case 10:
#line 65 "mkgs.y"
	{ add_notchar( yystack.l_mark[-2].tok, yystack.l_mark[-1].var); }
#line 492 "y.tab.c"
break;
case 11:
#line 67 "mkgs.y"
	{ add_array(yystack.l_mark[-5].tok, yystack.l_mark[-4].var, yystack.l_mark[-2].var); }
#line 497 "y.tab.c"
break;
case 12:
#line 69 "mkgs.y"
	{ printf( "/* skipping %s */\n", yystack.l_mark[-7].var );}
#line 502 "y.tab.c"
break;
case 13:
#line 71 "mkgs.y"
	{ printf( "/* skipping %s */\n", yystack.l_mark[-10].var );}
#line 507 "y.tab.c"
break;
case 14:
#line 73 "mkgs.y"
	{ printf( "/* skipping struct array %s %s */\n", yystack.l_mark[-5].var, yystack.l_mark[-4].var );}
#line 512 "y.tab.c"
break;
case 16:
#line 78 "mkgs.y"
	{ yyval.tok = yystack.l_mark[0].tok; }
#line 517 "y.tab.c"
break;
case 17:
#line 80 "mkgs.y"
	{ yyval.tok = yystack.l_mark[0].tok; }
#line 522 "y.tab.c"
break;
case 18:
#line 84 "mkgs.y"
	{ yyval.tok = yystack.l_mark[0].tok; }
#line 527 "y.tab.c"
break;
case 19:
#line 86 "mkgs.y"
	{ yyval.tok = yystack.l_mark[0].tok; }
#line 532 "y.tab.c"
break;
case 20:
#line 88 "mkgs.y"
	{ yyval.tok = yystack.l_mark[0].tok; }
#line 537 "y.tab.c"
break;
case 21:
#line 90 "mkgs.y"
	{ yyval.tok = yystack.l_mark[0].tok; }
#line 542 "y.tab.c"
break;
case 22:
#line 92 "mkgs.y"
	{ yyval.tok = yystack.l_mark[0].tok; }
#line 547 "y.tab.c"
break;
#line 549 "y.tab.c"
    }
    yystack.s_mark -= yym;
    yystate = *yystack.s_mark;
    yystack.l_mark -= yym;
    yym = yylhs[yyn];
    if (yystate == 0 && yym == 0)
    {
#if YYDEBUG
        if (yydebug)
            printf("%sdebug: after reduction, shifting from state 0 to\
 state %d\n", YYPREFIX, YYFINAL);
#endif
        yystate = YYFINAL;
        *++yystack.s_mark = YYFINAL;
        *++yystack.l_mark = yyval;
        if (yychar < 0)
        {
            yychar = YYLEX;
            if (yychar < 0) yychar = YYEOF;
#if YYDEBUG
            if (yydebug)
            {
                if ((yys = yyname[YYTRANSLATE(yychar)]) == NULL) yys = yyname[YYUNDFTOKEN];
                printf("%sdebug: state %d, reading %d (%s)\n",
                        YYPREFIX, YYFINAL, yychar, yys);
            }
#endif
        }
        if (yychar == YYEOF) goto yyaccept;
        goto yyloop;
    }
    if (((yyn = yygindex[yym]) != 0) && (yyn += yystate) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == (YYINT) yystate)
        yystate = yytable[yyn];
    else
        yystate = yydgoto[yym];
#if YYDEBUG
    if (yydebug)
        printf("%sdebug: after reduction, shifting from state %d \
to state %d\n", YYPREFIX, *yystack.s_mark, yystate);
#endif
    if (yystack.s_mark >= yystack.s_last && yygrowstack(&yystack) == YYENOMEM) goto yyoverflow;
    *++yystack.s_mark = (YYINT) yystate;
    *++yystack.l_mark = yyval;
    goto yyloop;

yyoverflow:
    YYERROR_CALL("yacc stack overflow");

yyabort:
    yyfreestack(&yystack);
    return (1);

yyaccept:
    yyfreestack(&yystack);
    return (0);
}
