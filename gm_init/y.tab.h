#define STRUCT 257
#define LBRACE 258
#define RBRACE 259
#define SEMI 260
#define LB 261
#define RB 262
#define CHARSTAR 263
#define DOUBLE 264
#define INTEGER 265
#define LONG 266
#define FLOAT 267
#define SHORT 268
#define UNSIGNED 269
#define VAR 270
#define COMMENT 271
#ifdef YYSTYPE
#undef  YYSTYPE_IS_DECLARED
#define YYSTYPE_IS_DECLARED 1
#endif
#ifndef YYSTYPE_IS_DECLARED
#define YYSTYPE_IS_DECLARED 1
typedef union YYSTYPE {
         char *var;
         int tok;
       } YYSTYPE;
#endif /* !YYSTYPE_IS_DECLARED */
extern YYSTYPE yylval;
