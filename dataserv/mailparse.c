#include "ds_extern.h"

#include "ds_proto.h"

extern struct SPECTRA_DISPLAY_PARAMS sdp[MAX_IF_NUMS];

#define  TRUE            1
#define  FALSE           0

#define INT		 1
#define FLOAT		 2
#define DOUBLE		 3
#define CHAR		 4

extern float otfIFNumber, ifNumber, scanNum;

extern char prgName[], saveBuf[], oldBuf[], initials[],
	    errBuf[], manInitials[], manVersion[];

extern int thisIsaRefresh, restoreMsg, manualInitials, manualVersion,
	   singleChan, singleOtfBin, doTPprocess, doFsProcess, sockmode;

extern int fitChan[2], off_center_line, auto_locate, domarker;
extern int fixedZero, ignore_gains, useFixData, ephemObj, ignorePos;

extern double which_two[2];
extern int    which_two_i[2];

int debug = 0;
int stopFlag = 0;

int fbScanDone     = 0,
    aosScanDone    = 0,
    dbeScanDone    = 0,
    ignoreBadChans = 0;

struct SOCK *mailBoxes[MAX_MAIL_BOXES];

static char *spaces = "                                                                     ";


struct MAIL_PARSE {
	char *msg;
	int   log;
	int   save_config;
	int   count;
	int   type;
	char  *format;
	void  *ptr;
	int   len;
	int   (*fctn)();
	char *help;
};


struct MAIL_PARSE parse_table[] = {
 /* Commands that change internal variables and are saved to dataserv.conf */
/*     msg,              log, save, cnt,   type,   fmt,             *ptr,           len,               fctn(),         *help */

  { "allowvelshift",       1,    1,   1,    INT,   "%d ", (void *)&otf.allowVelShift, 13, do_otfGridAllowVelShift, "0 | 1, toggle allowVelShift"},

  { "badchan",             1,    1,   1,    INT,   "%d ",    (void *)&ignoreBadChans,  7,             do_badChan, "0 | 1, toggle badchannel checking"},
  { "baselines",           1,    1,   8,    INT,   "%d ",  (void *)&otf.baselines[0],  9,           do_baseLines, "8n, Set baseline regions to these 8 numbers"},

  { "clearignore",         1,    0,   0,      0,    NULL,                       NULL, 11,         do_clearIgnore, "Clear Ignore scan array."},

  { "debug",               1,    1,   1,    INT,   "%d ",             (void *)&debug,  5,               do_debug, "Turn on Debugging Output"},

  { "ephemobj",            1,    1,   1,    INT,   "%d ",          (void *)&ephemObj,  8,            do_ephemObj, "Lock Grid For Ephem's"},

  { "etimeclear",          1,    1,   1,      0,    NULL,                       NULL, 10,          do_etimeClear, "Reset Function Call Time Listing"},
  { "etimedump",           1,    1,   1,      0,    NULL,                       NULL,  9,           do_etimeDump, "Dump .h format of Function Calls"},
  { "etimeshow",           1,    1,   1,      0,    NULL,                       NULL,  9,           do_etimeShow, "Show Function Call Time Listing"},

  { "fixedzero",           1,    1,   1,    INT,   "%d ",         (void *)&fixedZero,  9,           do_fixedzero, "Set zero point to center"},

  { "filename",            1,    0,   0,      0,    NULL,                       NULL,  8,            do_filename, "Set the Data  File Name"},
  { "gfilename",           1,    0,   0,      0,    NULL,                       NULL,  9,           do_gfilename, "Set the Gains File Name"},

  { "hard_copy",           1,    0,   0,      0,    NULL,                       NULL,  9,            do_hardCopy, "Print PS file of selected window"},
  { "help",                0,    0,   0,      0,    NULL,                       NULL,  4,                do_help, "Displays this list"},

  { "ignore",              1,    1,   0,      0,    NULL,                       NULL,  6,          do_ignoreScan, "v n, ignore scan n in version v."},
  { "ig_gains",            1,    1,   1,    INT,   "%d ",      (void *)&ignore_gains,  8,         do_ignoreGains, "0 | 1, Toggle gains ignoring"},
  { "ig_pos",              1,    1,   1,    INT,   "%d ",         (void *)&ignorePos,  6,           do_ignorePos, "Ignore Source Position"},
  { "ig_repaints",         1,    1,   1,    INT,   "%d ",(void *)&otf.ignoreAllRepaints,11,    do_ignoreRepaints, "Ignore All Repaints Until NOT Set"},

  { "load", 		   0,    0,   0,      0,    NULL,                       NULL,  4,                do_load, "Load a Script of Commands"},

  { "mallocmap",           1,    0,   0,      0,    NULL,                       NULL,  9,           do_mallocmap, "Print out malloc map."},

  { "otf3dcube",           1,    0,   0,      0,    NULL,                       NULL,  9,           do_otf3dcube, "bc, ec: plot 3d of Grid"},
  { "otf3dplot",           1,    0,   0,      0,    NULL,                       NULL,  9,           do_otf3dplot, "bc, ec: plot 3d of Grid"},
  { "otfbadchan",          1,    0,   0,      0,    NULL,                       NULL, 10,          do_otfBadChan, "Array of bad channels"},
  { "otfbaseline",         1,    1,   1,    INT,   "%d ",  (void *)&otf.gridBaseLine, 11,         do_otfBaseLine, "0 | 1, toggle baseline removal"},
  { "otfbeamarm",          1,    1,   1,    INT,   "%d ",(void *)&otf.armBeamLocation,10,          do_otfBeamArm, "0 | 1 Arm the flag to set Beam Location"},
  { "otfbeamsize",         1,    1,   1,      0,    NULL,                       NULL, 11,         do_otfBeamSize, "0 | 1 [ra dec], toggle Beam Size Marker"},
  { "otfchannelmap",       1,    0,   0,      0,    NULL,                       NULL, 13,       do_otfChannelMap, "c,x,y,s: Draw a Channel Map x by y, start @ c, space s"},
  { "otfchans",            1,    1,   2,    INT,   "%d ",     (void *)&otf.int_chans,  8,            do_otfChans, "bc ec, set int. range bc to ec"},
  { "otfcolorchoice",      1,    0,   0,      0,    NULL,                       NULL, 14,      do_otfColorChoice, "gray | pseudo | flame[1-6], Set otf-plot Color Choice"},
  { "otfcutoff",           1,    0,   0,      0,    NULL,                       NULL,  9,           do_otfCutOff, "Lower level of an OTF Image"},
  { "otfdisplayarray",     1,    1,   1,    INT,   "%d ",      (void *)&otf.arrayNum, 15,     do_otfDisplayArray, "0 | 1 | 2 | 3 | 4 | 5, Time ra dec az el"},
  { "otfdebug",            1,    0,   0,      0,    NULL,                       NULL,  8,     do_otfSpectraDebug, "n, set OTF Spectral plot to n"},
  { "otffixbadcells",      1,    0,   0,      0,    NULL,                       NULL, 14,      do_otfFixBadCells, "Fill in miss'n OTF cells"},
  { "otfgamma",            1,    0,   0,      0,    NULL,                       NULL,  8,            do_otfGamma, "v, Set otf-plot Gamma factor to v"},
  { "otfgridcellsizedec",  1,    1,   1, DOUBLE,   "%f ", (void *)&otf.manNyquistDec, 18,  do_otfGridCellSizeDec, "x, Set Grid Dec size to x"},
  { "otfgridcellsizera",   1,    1,   1, DOUBLE,   "%f ",  (void *)&otf.manNyquistRa, 17,   do_otfGridCellSizeRa, "x, Set Grid Ra size to x"},
  { "otfgridchancompress", 1,    1,   1,    INT,   "%d ",  (void *)&otf.chanCompress, 19, do_otfGridChanCompress, "i, Set Chan Smooth factor i"},
  { "otfgridcols",         1,    1,   1,    INT,   "%d ",          (void *)&otf.cols, 11,         do_otfGridCols, "c, Set Map X Size in Cols"},
  { "otfgriddec",          1,    1,   1, DOUBLE,   "%f ",     (void *)&otf.manMapDec, 10,          do_otfGridDec, "x, Set Grid Center Dec to x"},
  { "otfgriddump",         1,    0,   0,      0,    NULL,                       NULL, 11,         do_otfGridDump, "filename, dump OTF values to file"},
  { "otfgridfreqres",      1,    1,   1, DOUBLE,   "%f ",       (void *)&otf.freqres, 14,      do_otfGridFreqres, "freq, Set Grid Freq Resolution to 'freq'"},
  { "otfgridinit",         1,    0,   0,      0,    NULL,                       NULL, 11,         do_otfGridInit, "Reinitializes OTF grid array"},
  { "otfgridload",         1,    0,   0,      0,    NULL,                       NULL, 11,         do_otfGridLoad, "bs es [delay], loads scans bs to es"},
  { "otfgridnoint",        1,    1,   1,    INT,   "%d ",         (void *)&otf.noint, 12,        do_otfGridNoint, "noint, Set Grid number of Channels to 'noint'"},
  { "otfgridra",           1,    1,   1, DOUBLE,   "%f ",      (void *)&otf.manMapRa,  9,           do_otfGridRa, "x, Set Grid Center Ra to x"},
  { "otfgridrestfreq",     1,    1,   1, DOUBLE,   "%f ",      (void *)&otf.restfreq, 15,     do_otfGridRestfreq, "freq, Set Grid Rest Freq to freq"},
  { "otfgridrows",         1,    1,   1,    INT,   "%d ",          (void *)&otf.rows, 11,         do_otfGridRows, "c, Set Map Y Size in Rows"},
  { "otfgridscale",        1,    0,   0,      0,    NULL,                       NULL, 12,        do_otfGridScale, "v, Set otf-plot Scaling factor to v"},
  { "otfgridsizedec",      1,    1,   1, DOUBLE,   "%f ",  (void *)&otf.manMapHeight, 14,      do_otfGridSizeDec, "x, Set Grid Dec H to x"},
  { "otfgridsizera",       1,    1,   1, DOUBLE,   "%f ",   (void *)&otf.manMapWidth, 13,       do_otfGridSizeRa, "x, Set Grid Ra  W to x"},
  { "otfgridsum",          1,    1,   1,      0,    NULL,                       NULL, 10,      do_otfSpectralSum, "Show Sum of all Scans in Grid"},
  { "otfgridvelocity",     1,    1,   1, DOUBLE,   "%f ",      (void *)&otf.velocity, 15,     do_otfGridVelocity, "vel, Set Grid Velocity to vel"},
  { "otfifchoice",         1,    1,   1,    INT, "0x%x ",    (void *)&otf.chanSelOTF, 11,         do_otfIFchoice, "n, Load IF Number n"},
  { "otfinterp",           1,    0,   0,      0,    NULL,                       NULL,  9,           do_otfInterp, "Do a XY interpolation of OTF grid"},
  { "otflistcells",        1,    0,   0,      0,    NULL,                       NULL, 12,        do_otfListCells, "List info on each map cell"},
  { "otfloadchoice",       1,    0,   0,      0,    NULL,                       NULL, 13,       do_otfLoadChoice, "Preview|Step|Fast, Load"},
  { "otflockscale",        1,    0,   0,      0,    NULL,     (void *)&otf.lockScale, 12,        do_otfLockScale, "Lock Grid Scale"},
  { "otfnumcolors",        1,    0,   0,      0,    NULL,                       NULL, 12,        do_otfNumColors, "n, Set otf-plot Color Range to n"},
  { "otfregular",          1,    0,   0,      0,    NULL,                       NULL, 10,          do_otfRegular, "Return to raw OTF grid data"},
  { "otfregiondelete",     1,    0,   0,      0,    NULL,                       NULL, 15,          do_remRegions, "Delete all Regions"},
  { "otfregionlist",       1,    0,   0,      0,    NULL,                       NULL, 13,         do_listRegions, "List Loaded Regions"},
  { "otfregionload",       1,    0,   0,      0,    NULL,                       NULL, 13,          do_loadRegion, "Load a Region file"},
  { "otfregionon",         1,    0,   0,    INT,   "%d ",    (void *)&otf.regionshow, 11,          do_showRegion, "Show Region Outlines"},
  { "otfrmbadchan",        1,    0,   0,      0,    NULL,                       NULL, 12,        do_otfRmBadChan, "Remove Bad Channels From Grid"},
  { "otfrms",              1,    0,   0,      0,    NULL,                       NULL,  6,              do_otfRms, "Plot rms distribution on Map"},
  { "otfrmrms",            1,    0,   0,    INT,   "%d ",                       NULL,  8,            do_otfRmRMS, "rmslimit, Remove Cells with rms > rmslimit"},
  { "otftsys",             1,    0,   0,      0,    NULL,                       NULL,  7,             do_otfTsys, "Plot Tsys distribution on Map"},
  { "otftime",             1,    0,   0,      0,    NULL,                       NULL,  7,             do_otfTime, "Plot Total Integrated Time on Map"},
  { "otfsendplot",         1,    0,   0,      0,    NULL,                       NULL, 11,         do_otfSendPlot, "Send the Current Spectral Plot to Linuxpops"},
  { "otfsinglebin",        1,    1,   1,    INT,   "%d ",  (void *)&otf.singleOtfBin, 12,        do_otfSingleBin, "n, num of channels to bin in singleplot"},
  { "otfsinglechan",       1,    1,   1,    INT,   "%d ",    (void *)&otf.singleChan, 13,       do_otfSingleChan, "n, set the OTF singleplot to chan #n"},
  { "otfslice",            1,    0,   0,      0,    NULL,                       NULL,  8,            do_otfSlice, "Write out a single slice through map"},
  { "otfspike",            1,    0,   0,      0,    NULL,                       NULL,  8,             spikeCheck, "max, Zero any fabs(data) > max"},
  { "otfstatus",           1,    0,   0,      0,    NULL,                       NULL,  9,           do_otfStatus, "Print a brief summary of the current OTF map"},
  { "otfvelrun",           1,    0,   0,      0,    NULL,                       NULL,  9,           do_otfVelRun, "b e # dir [delay], vel movie"},
  { "otfzap",              1,    0,   0,      0,    NULL,                       NULL,  6,             do_zapCell, "col row, zap the data in cell: col x row"},
  { "wotfinterp",          1,    0,   0,      0,    NULL,                       NULL,  9,         interpAndWrite, "Write out a XY interpolation of OTF grid"},

  { "printdata",           1,    0,   0,      0,    NULL,                       NULL,  9,           do_printData, "n, Print out N Lines of Data"},
  { "printignorescans",    1,    0,   0,      0,    NULL,                       NULL, 16,    do_printIgnoreScans, "Print out scans ignored"},
  { "printmapscans",       1,    0,   0,      0,    NULL,                       NULL, 13,       do_printMapScans, "Print out scans in OTF grid"},

  { "q",                   0,    0,   0,      0,    NULL,                       NULL,  1,                do_quit, "Exit program"},
  { "quit",                0,    0,   0,      0,    NULL,                       NULL,  4,                do_quit, "Exit program"},

  { "scan_done",           1,    0,   0,      0,    NULL,                       NULL,  9,            do_scanDone, "n, display scan #n"},
  { "set_clip",            1,    1,   0,      0,    NULL,                       NULL,  8,             do_setClip, "n, Set beginning and ending clip length"},
  { "status",              1,    0,   0,      0,    NULL,                       NULL,  6,              do_status, "Shows brief status msg"},
  { "stop",                1,    0,   0,      0,    NULL,                       NULL,  4,                do_stop, "Stop any loops,load,velr"},

  { "xsync",               1,    0,   0,      0,    NULL,                       NULL,  5,               do_xsync, "0 | 1, toggle Xlib synchronize"},

  {  NULL,                 0,    0,   0,      0,    NULL,                       NULL,  0,              (void *)0,  NULL }
};


char *vxstrtok();


int do_tiltmeter(buf)
char *buf;
{

  return(0);
}

int do_load(buf)
char *buf;
{
  char inBuf[256];
  FILE *fp;

  if(buf)
  {
    stopFlag = 0;

    strcmprs(buf);

    fp = fopen(buf, "r");

    if(fp == NULL)
    {
      log_msg("DATASERV: Unable to open %s", buf);
      return(1);
    }

    while(fgets(inBuf, 256, fp) != NULL)
    {
      if(!strncmp(inBuf, "#", 1))
      {
        continue;
      }

      parseMail(inBuf);

      if(stopFlag)
      {
        fclose(fp);
        stopFlag = 0;

        return(0);
      }
    }

    fclose(fp);
  }

  return(0);
}


int readMail(buf, len)
char *buf;
int len;
{
  char *cp;
  static char cmdbuf[4096];

  if(buf)
  {
    cp = vxstrtok(buf, NULL, "\n");
    while(cp)
    {
      strcpy(cmdbuf, cp);
      parseMail(cmdbuf);
      cp = vxstrtok(NULL, cp, "\n");
    }
  }

  return(0);
}


int parseMail(buf)
char *buf;
{
  char *s;
  struct MAIL_PARSE *p;

  deNewLineIt(buf);

  ifNumber = 0.01;
  strlwr(buf);

  if(strstr(buf, "fb_scan"))
  {
    fbScanDone = 1;
  }
  else
  if(strstr(buf, "aos_scan"))
  {
    aosScanDone++;
  }
  else
  if(strstr(buf, "dbe_scan"))
  {
    dbeScanDone = 1;
  }
  else
  if(!strncmp(buf, "scan_done", 9))
  {
    fbScanDone = dbeScanDone = 1;
    aosScanDone = 3;
  }

  for(p=parse_table;p->msg;p++)
  {
    if(!strncmp(p->msg, buf, p->len))
    {
      if(p->log && strlen(buf) < 2048)
      {
        s = (char *)sock_name(last_msg());
        if(s)
          log_msg("%s: (%s) %s", prgName, sock_name(last_msg()), buf);
        else
          log_msg("%s: %s", prgName,  buf);
      }

      p->fctn(buf+p->len);
      thisIsaRefresh = 0;
      setInValid(0);

      return(0);
    }
  }
  log_msg("Command %s NOT found", buf);
  thisIsaRefresh = 0;
  setInValid(0);

  return(0);
}



int do_scanDone(buf)
char *buf;
{
  restoreMsg = 0;
  strcpy(saveBuf, oldBuf);

  if(buf)
  {
    scanNum = atof(buf);
  }
  else
  {
    return(1);
  }
   
  if( fbScanDone || aosScanDone >= 3 || dbeScanDone )
  {
    dbeScanDone = 0;

    if(!thisIsaRefresh)
      setInValid(1);

    verifyScanDone((int)scanNum);

    show_display();

    if(restoreMsg)
      strcpy(oldBuf, saveBuf);
    else
      sprintf(oldBuf, "scan_done %d", (int)scanNum);

    return(0);
  }

  return(1);
}



int do_calDone()
{
  fbScanDone = aosScanDone = dbeScanDone = 0;
  
  return(0);
}



int do_status()
{
  log_msg("Dataserv O.K.");
  
  return(0);
}



int do_badChan(buf)
char *buf;
{
  if(buf)
    ignoreBadChans = atoi(buf);

  if(!thisIsaRefresh)
    setInValid(1);

  verifyScanDone((int)scanNum);
  fbScanDone = dbeScanDone = 1;
  aosScanDone = 3;

  show_display();
  
  return(0);
}




int do_quit()
{
  signalHandler();
  
  return(0);
}



int do_stop()
{
  stopFlag = 1;
  
  return(0);
}


int do_xsync(buf)
char *buf;
{
  int v;

  if(buf)
  {
    v = atoi(buf);

    if(v == 0 || v == 1)
      synchronize(v);
  }
  
  return(0);
}



int do_help(buf)
char *buf;
{
  struct MAIL_PARSE *p;
  char tbuf[128];
  char *cp;

  cp = strtok(buf, " \n\t");

  sprintf(tbuf, "Command              Arguments\n");
  printf("\n%s\n", tbuf);

  if(sockmode)
  {
    log_msg(tbuf);
  }

  for(p=parse_table;p->msg;p++)
  {
    if(cp)
    {
      if(!strncmp(p->msg, cp, strlen(cp)))
      {
        sprintf(tbuf, "%s: %s", p->msg, spaces);
        strcpy(tbuf+18, p->help);
        printf("%s\n", tbuf);

	if(sockmode)
	{
          log_msg(tbuf);
	}
      }
    }
    else
    {
      sprintf(tbuf, "%s: %s", p->msg, spaces);
      strcpy(tbuf+18, p->help);
      printf("%s\n", tbuf);

      if(sockmode)
      {
        log_msg(tbuf);
      }

    }
  }

  printKeySymHelp();
  
  return(0);
}



int do_setClip(buf)
char *buf;
{
  int which, val;
  char *cp;

  if(buf)
  {
    cp = strtok(buf, " \n\t");
    if(cp)
    {
      which = atoi(cp);
      cp = strtok(NULL, " \n\t");
      if(cp)
      {
        val = atoi(cp);
      }
      else
      {
        log_msg("Dataserv: clipdata command missing n designator");
        return(1);
      }
    }
    else
    {
      log_msg("Dataserv: clipdata command missing 'be' designator");
      return(1);
    }
  }
  else
    return(1);

  if(which >= 0 && which < MAX_IF_NUMS)
    sdp[which].clip = val;

  if(!thisIsaRefresh)
    setInValid(1);

  verifyScanDone((int)scanNum);
  fbScanDone = dbeScanDone = 1;
  aosScanDone = 3;

  show_display();
  
  return(0);
}


int do_ignoreGains(buf)
char *buf;
{
  extern int ignore_gains;

  if(buf)
    ignore_gains = atoi(buf);

  if(!thisIsaRefresh)
    setInValid(1);

  verifyScanDone((int)scanNum);
  fbScanDone = dbeScanDone = 1;
  aosScanDone = 3;

  show_display();
  
  return(0);
}


int do_ignoreRepaints(buf)
char *buf;
{
  if(buf)
  {
    otf.ignoreAllRepaints = atoi(buf);
  }

  log_msg("Setting ignoreAllRepaints to %d", otf.ignoreAllRepaints);

  return(0);
}

int do_hardCopy(buf)
char *buf;
{
  char *cp, win[80], file[128], tmp[256];
  int land, rev;

  if(buf)
  {
    strcpy(tmp, buf);
    cp = strtok(tmp, " \n\r");
    if(cp)
      strcpy(win, cp);
    else
    {
      log_msg("Dataserv: Hardcopy command incomplete, window");
      return(1);
    }
    cp = strtok(NULL, " \n\r");
    if(cp)
      strcpy(file, cp);
    else
    {
      log_msg("Dataserv: Hardcopy command incomplete, filename");
      return(1);
    }
    cp = strtok(NULL, " \n\r");
    if(cp)
      land = atoi(cp);
    else
    {
      log_msg("Dataserv: Hardcopy command incomplete, land");
      return(1);
    }
    cp = strtok(NULL, " \n\r");
    if(cp)
      rev = atoi(cp);
    else
    {
      log_msg("Dataserv: Hardcopy command incomplete, rev");
      return(1);
    }
    xgr_hardCopy(win, land, rev, file);
  }

  return(0);
}



int do_diagHarCopyServ()
{
  do_hardCopy("dataserv /tmp/dataserv.pdf 1 1");
  system("lpr /tmp/dataserv.pdf");
  system("chmod 666 /tmp/dataserv.pdf");
  
  return(0);
}


int do_diagHarCopyGrid()
{
  do_hardCopy("otf /tmp/dataserv.pdf 1 1");
  system("lpr /tmp/dataserv.pdf");
  system("chmod 666 /tmp/dataserv.pdf");
  
  return(0);
}

int do_makeGif(buf)
char *buf;
{
  char *cp, win[80], file[128], tmp[256];

  if(buf)
  {
    strcpy(tmp, buf);
    cp = strtok(tmp, " \n\r");
    if(cp)
      strcpy(win, cp);
    else
    {
      log_msg("Dataserv: make_gif command incomplete");
      return(1);
    }
    cp = strtok(NULL, " \n\r");
    if(cp)
      strcpy(file, cp);
    else
    {
      log_msg("Dataserv: make_gif command incomplete");
      return(1);
    }
    xgr_makeGif(win, file);
  }

  return(0);
}



char *vxstrtok(string, last_token, delim)
char *string, *last_token, *delim;
 
/* char *vxstrtok(char *string, char *last_token, char *delim) --
 * Like strtok, but can be used in VxWorks.  The first call passes a non-NULL
 * "string".  A pointer to the first character in "string" not in "delim" is
 * returned (or NULL if none).  The next occurance of a character in "delim"
 * past this first character is set to NUL and a second NUL is appended to
 * "string".  Subsequent calls must pass a NULL pointer "string" and the
 * previous return value ("last_token").  In these calls, the NUL following
 * last_token is replaced by the first character in "delim" and the scan
 * continues until the next non-"delim" past this point is found or the
 * end of the string is found (NULL returned).  Again, if a non-"delim"
 * character is found, the next "delim" character following it is replaced by
 * a NUL.
 */
 
{
  int n, i;
  char *ptr, *ret;

  if(string) {
						/* First call on a new string */
    string[strlen(string)+1] = '\0';	       /* Add a second NUL to the end */
    ptr = string;
  } else if(!last_token || !*last_token || !delim || !*delim)
    return NULL;
  else {
				      /* A subsequent call on the same string */
    ptr = last_token + strlen(last_token);	     /* End of previous token */
    *ptr++ = delim[0];				      /* Put a delimiter back */
  }
				   /* Scan for the next non-"delim" character */
  for(n=strlen(delim); *ptr; ptr++) {
    for(i=0; i < n && *ptr != delim[i]; i++)
      continue;
    if(i == n)
      break;							 /* Found one */
  }
  if(!*ptr)
    return NULL;					     /* End of string */
 
  ret = ptr++;					   /* Save beginning of token */
 
					   /* Scan for next "delim" character */
  for( ; *ptr; ptr++) {
    for(i=0; i < n && *ptr != delim[i]; i++)
      continue;
    if(i != n)
       break;							/* Found one */
  }
  *ptr = '\0';						      /* Mark the end */
  return ret;					      /* Return the beginning */
}


int do_mallocmap()
{
/*
  mallocmap();
 */
  
  return(0);
}



int do_fixedzero(buf)
char *buf;
{

  if(buf)
    fixedZero = atoi(buf);

  if(!thisIsaRefresh)
    setInValid(1);

  verifyScanDone((int)scanNum);
  fbScanDone = dbeScanDone = 1;
  aosScanDone = 3;

  show_display();
  
  return(0);
}



int do_markers(buf)
char *buf;
{
  extern int domarker;

  if(buf)
    domarker = atoi(buf);

  if(!thisIsaRefresh)
    setInValid(1);

  verifyScanDone((int)scanNum);
  fbScanDone = dbeScanDone = 1;
  aosScanDone = 3;

  show_display();
  
  return(0);
}



int do_debug(buf)
char *buf;
{
  if(buf)
  {
    debug = atoi(buf);
  }

  return(0);
}



/* OTF routines */

int do_otfLockScale(buf)
char *buf;
{
  if(buf)
  {
    otf.lockScale = atoi(buf);
  }
  else
  {
    otf.lockScale = 0;
  }

  return(0);
}


int do_ephemObj(buf)
char *buf;
{
  if(buf)
  {
    ephemObj = atoi(buf);
  }
  else
  {
    ephemObj = 0;
  }

  return(0);
}


int do_otfGridChanCompress(buf)
char *buf;
{
  int i;

  if(buf)
  {
    i = atoi(buf);
  }

  if(i == 0 || i == 2 || i == 4 || i == 8)
  {
    otf.chanCompress = i;
  }

  return(0);
}


int do_otfGridCellSizeRa(buf)
char *buf;
{
  double f, atof();

  if(buf)
  {
    f = atof(buf);
  }

  if(f > 0.0 )
  {
    otf.manNyquistRa = f;
  }

  return(0);
}



int do_otfGridCellSizeDec(buf)
char *buf;
{
  double f, atof();

  if(buf)
  {
    f = atof(buf);
  }

  if(f > 0.0 )
  {
    otf.manNyquistDec = f;
  }

  return(0);
}


int do_otfGridSizeRa(buf)
char *buf;
{
  double f, atof();

  if(buf)
  {
    f = atof(buf);
  }

  if(f > 0.0 )
  {
    otf.manMapWidth = f;
  }

  return(0);
}



int do_otfGridSizeDec(buf)
char *buf;
{
  double f, atof();

  if(buf)
  {
    f = atof(buf);
  }

  if(f > 0.0 )
  {
    otf.manMapHeight = f;
  }

  return(0);
}


int do_otfGridRestfreq(buf)
char *buf;
{
  double f, atof();

  if(buf)
  {
    f = atof(buf);
  }

  otf.restfreq = f;

  return(0);
}


int do_otfGridFreqres(buf)
char *buf;
{
  double f, atof();

  if(buf)
  {
    f = atof(buf);
  }

  otf.freqres = f;

  return(0);
}


int do_otfGridCols(buf)
char *buf;
{
  int cols, atoi();

  if(buf)
  {
    cols = atoi(buf);
  }

  otf.cols = cols;

  return(0);
}


int do_otfGridRows(buf)
char *buf;
{
  int rows, atoi();

  if(buf)
  {
    rows = atoi(buf);
  }

  otf.rows = rows;

  return(0);
}


int do_otfGridNoint(buf)
char *buf;
{
  int noint, atoi();

  if(buf)
  {
    noint = atoi(buf);
  }

  otf.noint = noint;

  return(0);
}


int do_otfGridAllowVelShift(buf)
char *buf;
{
  int allow, atoi();

  if(buf)
  {
    allow = atoi(buf);
  }

  otf.allowVelShift = allow;

  return(0);
}


int do_otfGridRa(buf)
char *buf;
{
  double f, atof();

  if(buf)
  {
    f = atof(buf);
  }

  if(f >= 0.0 )
  {
    otf.manMapRa = f;
  }

  return(0);
}



int do_otfGridDec(buf)
char *buf;
{
  double f, atof();

  if(buf)
  {
    f = atof(buf);
  }

  if(f >= -90.0 || f <= 90.0)
  {
    otf.manMapDec = f;
  }

  return(0);
}


int do_otfGridVelocity(buf)
char *buf;
{
  double f, atof();

  if(buf)
  {
    f = atof(buf);
  }

  otf.velocity = f;

  return(0);
}


int do_otfCutOff(buf)
char *buf;
{
  int i;

  if(buf)
  {
    i = atoi(buf);
  }

  if(i >=0 && i < 100)
  {
    otf.cutoff = i;
  }

  doOtfGridRePaint();

  return(0);
}


int do_otfBadChan(buf)
char *buf;
{
  int i;
  char *cp, tmp[256];
  extern int ignoreBadChans;

  for(i=0;i<MAX_BAD_CHAN;i++)
  {
    badChanArray[i] = 0;
  }

  ignoreBadChans = 0;

  i = 0;
  if(buf)
  {
    strcpy(tmp, buf);
    cp = strtok(tmp, "\t\n ");

    while(cp)
    {
      badChanArray[i] = atoi(cp);
      cp = strtok(NULL, "\t\n ");
      ignoreBadChans++;

      i++;
    }
  }

  return(0);
}



int do_otfSpectraDebug(buf)
char *buf;
{
  int v;

  if(buf)
  {
    v = atoi(buf);

    if(v > otf.numSpectra)
    {
      log_msg("Requested spectra %d > numOfSpectra %d", v, otf.numSpectra);
      return(1);
    }

    otf.debugSpectra = v;

    if(!thisIsaRefresh)
    {
      setInValid(1);
    }

    verifyScanDone((int)scanNum);
    show_display();
  }

  return(0);
}


int do_otfLoadChoice(buf)
char *buf;
{
  if(buf)
  {
    if(strstr(buf,"step"))
    {
      otf.loadChoice = LOAD_STEP;
    }
    else
    if(strstr(buf,"fast"))
    {
      otf.loadChoice = LOAD_FAST;
    }
    else
    {
      otf.loadChoice = LOAD_PREVIEW;
    }
  }

  otf.noPlot = 0;

  return(0);
}


int do_otfIFchoice(buf)
char *buf;
{
  if(buf)
  {
    otf.chanSelOTF = atoi(buf);
  }

  if(otf.chanSelOTF < 0)
  {
    otf.chanSelOTF = 0;
  }

  chanSelOTF = otf.chanSelOTF;

  return(0);
}

int do_otfNumColors(buf)
char *buf;
{
  char *cp;
  int n;

  if(buf)
  {
    cp = strtok(buf, " \n\t");
    if(cp)
    {
      n = atoi(cp);

      if(n == 16 || n == 32 || n == 64 || n == 128 || n == 256)
      {
        xgr_setNumColors(n);
      }
    }

    if(otf.head == NULL)
    {
      return(1);
    }

    if(otf.lastAction == INTERP)
    {
      otfInterp();
    }
    else
    if(otf.lastAction == NORMAL_MAP)
    {
      paintOtfGrid();
    }
    else
    if(otf.lastAction == CHANNEL_MAP)
    {
      do_otfChannelMap(NULL);
    }
  }

  return(0);
}


int do_otfInterp()
{
  otf.newShade = 1;

  if(otf.head == NULL)
  {
    return(1);
  }

  otfInterp();

  return(0);
}


int do_otfColorChoice(buf)
char *buf;
{
  char *cp;
  int c;

  if(buf)
  {
    cp = strtok(buf, " \n\t");
    if(cp)
    {
      if(!strcmp(cp, "pseudo"))
      {
        c = DS_PSEUDO;
      }
      else
      if(!strcmp(cp, "flame1"))
      {
        c = DS_FLAME1;
      }
      else
      if(!strcmp(cp, "flame2"))
      {
        c = DS_FLAME2;
      }
      else
      if(!strcmp(cp, "flame3"))
      {
        c = DS_FLAME3;
      }
      else
      if(!strcmp(cp, "flame4"))
      {
        c = DS_FLAME4;
      }
      else
      if(!strcmp(cp, "flame5"))
      {
        c = DS_FLAME5;
      }
      else
      if(!strcmp(cp, "flame6"))
      {
        c = DS_FLAME6;
      }
      else
      if(!strcmp(cp, "flame7"))
      {
        c = DS_FLAME7;
      }
      else
      if(!strcmp(cp, "flame8"))
      {
        c = DS_FLAME8;
      }
      else
      if(!strcmp(cp, "flame9"))
      {
        c = DS_FLAME9;
      }
      else
      {
        c = DS_GRAYSCALE;
      }

      xgr_setColorChoice(c);
    }

    otf.newShade = 1;

    if(otf.head == NULL)
    {
      if(sockmode)
      {
        to_helper("done");
      }

      return(1);
    }

    if(otf.lastAction == INTERP)
    {
      otfInterp();
    }
    else
    if(otf.lastAction == NORMAL_MAP)
    {
      paintOtfGrid();
    }
    else
    if(otf.lastAction == CHANNEL_MAP)
    {
      do_otfChannelMap(NULL);
    }
  }

  otf.colorChoice = c;

  if(sockmode && !dontsend)
  {
    to_helper("done");
  }

  return(0);
}


int do_otfGamma(buf)
char *buf;
{
  char *cp;

  if(buf)
  {
    cp = strtok(buf, " \n\t");
    if(cp)
    {
      otf.gamma = atof(cp);
    }
  }

  otf.valueMaxChanged = 1;
  otf.intenChanged    = 1;
  otf.newShade        = 1;

  if(otf.head == NULL)
  {
    return(1);
  }

  paintOtfGrid();

  return(0);
}

int do_otfGridScale(buf)
char *buf;
{
  char *cp;

  if(buf)
  {
    cp = strtok(buf, " \n\t");
    if(cp)
    {
      otf.scaleFactor = atof(cp);
    }
  }

  otf.valueMaxChanged = 1;
  otf.intenChanged    = 1;
  otf.newShade        = 1;

  if(otf.head == NULL)
  {
    return(1);
  }

  paintOtfGrid();

  return(0);
}

int do_otfSingleChan(buf)
char *buf;
{
  char tb[80];

  if(buf)
  {
    otf.singleChan = atoi(buf);
  }

  if(!thisIsaRefresh)
  {
    setInValid(1);
  }

  sprintf(tb,"MSG_RPT Dataserv: Setting OTF Single Display Channel to  %d.", otf.singleChan);
  to_monitor(tb);

  log_msg(tb);

  verifyScanDone((int)scanNum);

  show_display();

  return(0);
}



int do_otfSingleBin(buf)
char *buf;
{
  char tb[80];
  int old;

  old = otf.singleOtfBin;

  if(buf)
  {
    otf.singleOtfBin = atoi(buf);
  }

  if(!thisIsaRefresh)
  {
    setInValid(1);
  }

  if(otf.singleOtfBin > 32 || otf.singleOtfBin < 0)
  {
    log_msg("single Bin value %d, too big, or large", otf.singleOtfBin);
    otf.singleOtfBin = old;
  }

  sprintf(tb,"MSG_RPT Dataserv: Setting OTF Bin Number to  %d.",otf.singleOtfBin);

  to_monitor(tb);

  log_msg(tb);

  verifyScanDone((int)scanNum);

  show_display();

  return(0);
}


int do_otfDisplayArray(buf)
char *buf;
{
  char tb[80];
  static char *ar[] = { "AUTO", "TIME", "RA", "DEC", "AZ", "EL" };
  int old;

  old = otf.arrayNum;

  if(buf)
  {
    otf.arrayNum = atoi(buf);
  }

  if(!thisIsaRefresh)
  {
    setInValid(1);
  }

  if(otf.arrayNum>-1 && otf.arrayNum < 6)
  {
    sprintf(tb,"MSG_RPT Dataserv: Setting OTF Display Array to  %s.",
                        ar[otf.arrayNum]);
    to_monitor(tb);
    log_msg(tb);

    verifyScanDone((int)scanNum);
    show_display();
  }
  else
  {
    otf.arrayNum = old;
  }

  return(0);
}

int do_otfTsys(buf)
char *buf;
{
  otf.whichPlot = PLOT_TSYS;

  otf.valueMaxChanged = 1;
  otf.intenChanged    = 1;
  otf.newShade        = 1;

  if(otf.head == NULL)
  {
    return(1);
  }

  paintOtfGrid();
  displayRequestedOtf();

  return(0);
}



int do_otfRms(buf)
char *buf;
{
  otf.whichPlot = PLOT_RMS;

  otf.valueMaxChanged = 1;
  otf.intenChanged    = 1;
  otf.newShade        = 1;

  if(otf.head == NULL)
  {
    return(1);
  }

  paintOtfGrid();
  displayRequestedOtf();

  return(0);
}


int do_otfTime(buf)
char *buf;
{
  otf.whichPlot = PLOT_TIME;

  otf.valueMaxChanged = 1;
  otf.intenChanged    = 1;
  otf.newShade        = 1;

  if(otf.head == NULL)
  {
    return(1);
  }

  paintOtfGrid();
  displayRequestedOtf();

  return(0);
}

int do_otfChans(buf)
char *buf;
{
  char *cp;
  char tmp[256];;

  strcpy(tmp, buf);
  cp = strtok(tmp," \n\t");
  if(cp)
  {
    otf.int_chans[0]  = atoi(cp);
  }
  else
  {
    log_msg("Bad begin chan specifier in otfchans");
    return(1);
  }

  cp = strtok((char *)NULL," \n\t");
  if(cp)
  {
    otf.int_chans[1] = atoi(cp);
  }
  else
  {
    log_msg("Bad ending chan specifier in otfchans");
    return(1);
  }

  otf.valueMaxChanged = 1;
  otf.intenChanged    = 1;
  otf.newShade        = 1;

  if(otf.head == NULL)
  {
    return(1);
  }

  markDataDirty();

  paintOtfGrid();

  displayRequestedOtf();

  return(0);
}


int do_otfBeamSize(buf)
char *buf;
{
  int i;
  char *cp;

  cp = strtok(buf,"\n\t ");
  if(cp)
  {
    i = atoi(cp);
  }
  else
  {
    log_msg("do_otfBeamSize(), no arg specified");
    return(1);
  }

  cp = strtok(NULL,"\n\t ");
  if(cp)
  {
    otf.beamRa = atof(cp);
  }

  cp = strtok(NULL,"\n\t ");
  if(cp)
  {
    otf.beamDec = atof(cp);
  }

  otf.valueMaxChanged = 1;
  otf.intenChanged    = 1;
  otf.newShade        = 1;
  otf.placeBeamSize   = i;

  if(otf.head == NULL)
  {
    return(1);
  }

  paintOtfGrid();
  displayRequestedOtf();

  return(0);
}


int do_otfBeamArm(buf)
char *buf;
{
  int i;
  char *cp;

  cp = strtok(buf,"\n\t ");
  if(cp)
  {
    i = atoi(cp);
  }
  else
  {
    log_msg("do_otfBeamArm(), no arg specified");
    return(1);
  }

  otf.armBeamLocation = i;

  return(0);
}



int do_otfBaseLine(buf)
char *buf;
{
  char *cp;
  int bl;

  cp = strtok(buf,"\n\t ");
  if(cp)
  {
    bl = atoi(cp);
  }
  else
  {
    log_msg("do_otfBaseLine(), no baseline specified");
    return(1);
  }

  otf.valueMaxChanged = 1;
  otf.intenChanged    = 1;
  otf.newShade        = 1;
  otf.gridBaseLine    = bl;

  if(otf.head == NULL)
  {
    return(1);
  }

  paintOtfGrid();

  displayRequestedOtf();

  return(0);
}



int do_baseLines(buf)
char *buf;
{
  char *cp, tmp[256];
  int i=0;

  strcpy(tmp, buf);
  cp = strtok(tmp, " \n\t");
  while(cp)
  {
    if(i > 7)
    {
      return(1);
    }

    otf.baselines[i++] = atoi(cp);
    cp = strtok(NULL, " \n\t");
  }

  if(otf.head == NULL)
  {
    return(1);
  }

  recomputeBaseLines();

  otf.valueMaxChanged = 1;
  otf.intenChanged    = 1;
  otf.newShade        = 1;

  paintOtfGrid();

  displayRequestedOtf();

  return(0);
}



