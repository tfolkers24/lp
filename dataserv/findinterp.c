#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <math.h>

#include "caclib_proto.h"

#define MAX_GAP 5

double v1a[8], v2a[8];

int makeInterpTable(interval)
int interval;
{
  int    i;
  double val = 0.0, frac, findx;

  if(interval > MAX_GAP)
  {
    return(1);
  }

  findx = (double)interval + 1.0;

  frac = 1.0 / findx;

  for(i=0;i<interval;i++)
  {
    v1a[i] = frac * ((double)i + 1.0);
    v2a[i] = 1.0 - v1a[i];
  }

  log_msg("V1A: %d: %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f", interval,
    v1a[0], v1a[1], v1a[2], v1a[3], v1a[4], v1a[5], v1a[6], v1a[7]);

  log_msg("V2A: %d: %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f", interval,
    v2a[0], v2a[1], v2a[2], v2a[3], v2a[4], v2a[5], v2a[6], v2a[7]);


  return(val);
}


int main(argc, argv)
int argc;
char *argv[];
{
  int i, j;
  double val, v1, v2;

  v1 = 0.0;
  v2 = 1.0;

  log_msg("P1 = %f; P2 = %f", v1, v2);

  for(i=2;i<MAX_GAP;i++)
  {
    makeInterpTable(i);

    for(j=0;j<i;j++)
    {
      val = v1 * v1a[j] + v2 * v2a[j];
      log_msg("V: %f", val);
    }
  }

  return(0);
}

