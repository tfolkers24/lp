/*
 * specdata.c:  Program to display the called for scan;  Unlike the original
 *              dataserv program which would display the just completed observation,
 *		and perform analysis on the data, this program just displays the 
 *		data and doesn't do anything with it.
 *
 *		Sleeps on a socket waiting for a SPECDATA structure containing the 
 *		file names and scan number to display.  Only display Spectral data.
 */

#include "ds_extern.h"

#include "sd_proto.h"

#ifdef GALACTIC
#undef GALACTIC
#endif

#ifdef AZEL
#undef AZEL
#endif

#ifdef USERDEF
#undef USERDEF
#endif

#include "dataserv.bmp"
#include "datagrid.bmp"

#define  TRUE   1
#define  FALSE  0
#define DATASPREAD      0.000001

static int font0 = FONT_SIZE_SMALL;
static int font1 = FONT_SIZE_NORMAL;

float globalStart, globalStop;

int (*sock_callback)() = NULL;
char inMsg[16384];

struct SOCK *master, *helper, *xgraphic, *specplot;

int   datalen		= 0,
      manualInitials    = 0,
      manualVersion     = 0,
      thisIsaRefresh	= 0,
      initialsChanged   = 0,
      printDataLines    = 0,
      useFixData	= 0,
      version           = 1;

int debug      = 0;
int zerodata   = 0;


float datamax, 
      datamin, 
      scanNum		= -1.0;


#ifdef TOMMIE
char  filename[256];
char  gfilename[256];
int   found_scans[MAX_BACKENDS*MAX_PARTS];
#endif

char  initials[256], source[SRCLEN+1], oldBuf[16384], tempBuf[16384],
      prgName[80], errBuf[512], manInitials[80], localHost[80],
      deadServer[80], saveBuf[16384], manVersion[80], mainFrameTitle[80], 
      tailFileName[80], datadir[128], fixeddir[128], _3ddir[128];




int scansDone[100];
int scansDoneInx = 0;
int scanDone     = 0;

static int block_for_event();

struct HEADER  onhead;
struct HEADER  offhead;
struct HEADER  gainshead;

float  *ondata;
float  *offdata;

float  *gainsdata;

extern int _Xdebug;

int fitFail = 0;
int doNotSaveConfig = 0;

extern int noPlot, noYTick, noXTick;
extern struct XGR_HEAD xgr_head;
struct XGR_ *server;

struct OTF_STRUCT otf;

struct SPECDATA sd;

int PID;

  /* DO NOT FORGET TO ADD A HANDLER FOR ANY EXTRA MASKS ADDED, processEvent() */
long frame_mask = KeyPressMask | ExposureMask | StructureNotifyMask;

int istty;
int globalUsPixmap = 1;

extern int insque(), remque();

static char *spaces = "                                                                     ";

struct MAIL_PARSE {
        char *msg;
        int   log;
        int   save_config;
        int   count;
        int   type;
        char  *format;
        void  *ptr;
        int   len;
        int   (*fctn)();
        char *help;
};


struct MAIL_PARSE parse_table[] = {
 /* Commands that change internal variables and are saved to dataserv.conf */
/*   msg,   log, save, cnt,   type,   fmt,  *ptr,  len,     fctn(),   *help */
  { "help",  0,     0,   0,      0,  NULL,  NULL,    4,    do_help,   "Displays this list"},
  { "quit",  0,     0,   0,      0,  NULL,  NULL,    4,    do_quit,   "Exit program"},

  {  NULL,   0,     0,   0,      0,  NULL,  NULL,    0,  (void *)0,   NULL }
};


struct STURCT_KEYSYMS {
        KeySym sym;
        int   win;
        char *key;
        char *help;
};

struct STURCT_KEYSYMS keysyms[] = {
        { XK_q,  1,      "q",               "Quit Program" },
        { XK_p,  1,      "<control> + p",   "Screen Capture Window" }
};

#define MAX_KEYSYMS (sizeof(keysyms) / sizeof(struct STURCT_KEYSYMS))




int to_helper(buf)
char *buf;
{
  if(buf)
  {
    log_msg("To HELPER: '%s'", buf);

    sock_send(helper, buf);
  }

  return(0);
}


int to_xgraphic(buf)
char *buf;
{
  if(buf)
  {
    log_msg("To XGRAPHIC: %s", buf);

    sock_send(xgraphic, buf);
  }

  return(0);
}


int printKeySymHelp()
{
  int i;
  char tbuf[256];
  struct STURCT_KEYSYMS *k;

  sprintf(tbuf, "Press the Following Keys in Either the OTF or Spectral Plot Windows\n");

  printf("\n%s\n", tbuf);
  log_msg(" ");
  log_msg(tbuf);
  log_msg(" ");

  k = &keysyms[0];
  for(i=0;i<MAX_KEYSYMS;i++,k++)
  {
    if(k->win == 0)
    {
      sprintf(tbuf, "Press %s key for %s", k->key, k->help);
      printf("%s\n", tbuf);

      log_msg(tbuf);
    }
  }

  sprintf(tbuf, "Press the Following Keys Spectral Plot Windows\n");

  printf("\n%s\n", tbuf);
  log_msg(" ");
  log_msg(tbuf);
  log_msg(" ");

  k = &keysyms[0];
  for(i=0;i<MAX_KEYSYMS;i++,k++)
  {
    if(k->win == 1)
    {
      sprintf(tbuf, "Press %s key for %s", k->key, k->help);
      printf("%s\n", tbuf);

      log_msg(tbuf);
    }
  }

  return(0);
}


int do_help(buf)
char *buf;
{
  struct MAIL_PARSE *p;
  char tbuf[128];
  char *cp;

  cp = strtok(buf, " \n\t");

  sprintf(tbuf, "Command              Arguments\n");
  printf("\n%s\n", tbuf);

  log_msg(tbuf);

  for(p=parse_table;p->msg;p++)
  {
    if(cp)
    {
      if(!strncmp(p->msg, cp, strlen(cp)))
      {
        sprintf(tbuf, "%s: %s", p->msg, spaces);
        strcpy(tbuf+18, p->help);
        printf("%s\n", tbuf);

        log_msg(tbuf);
      }
    }
    else
    {
      sprintf(tbuf, "%s: %s", p->msg, spaces);
      strcpy(tbuf+18, p->help);
      printf("%s\n", tbuf);

      log_msg(tbuf);
    }
  }

  printKeySymHelp();

  return(0);
} 



int do_quit()
{
  signalHandler();

  return(0);
}


char *vxstrtok(string, last_token, delim)
char *string, *last_token, *delim;

/* char *vxstrtok(char *string, char *last_token, char *delim) --
 * Like strtok, but can be used in VxWorks.  The first call passes a non-NULL
 * "string".  A pointer to the first character in "string" not in "delim" is
 * returned (or NULL if none).  The next occurance of a character in "delim"
 * past this first character is set to NUL and a second NUL is appended to
 * "string".  Subsequent calls must pass a NULL pointer "string" and the
 * previous return value ("last_token").  In these calls, the NUL following
 * last_token is replaced by the first character in "delim" and the scan
 * continues until the next non-"delim" past this point is found or the
 * end of the string is found (NULL returned).  Again, if a non-"delim"
 * character is found, the next "delim" character following it is replaced by
 * a NUL.
 */

{
  int n, i;
  char *ptr, *ret;

  if(string) {
                                                /* First call on a new string */
    string[strlen(string)+1] = '\0';           /* Add a second NUL to the end */
    ptr = string;
  } else if(!last_token || !*last_token || !delim || !*delim)
    return NULL;
  else {
                                      /* A subsequent call on the same string */
    ptr = last_token + strlen(last_token);           /* End of previous token */
    *ptr++ = delim[0];                                /* Put a delimiter back */
  }
                                   /* Scan for the next non-"delim" character */
  for(n=strlen(delim); *ptr; ptr++) {
    for(i=0; i < n && *ptr != delim[i]; i++)
      continue;
    if(i == n)
      break;                                                     /* Found one */
  }
  if(!*ptr)
    return NULL;                                             /* End of string */

  ret = ptr++;                                     /* Save beginning of token */

                                           /* Scan for next "delim" character */
  for( ; *ptr; ptr++) {
    for(i=0; i < n && *ptr != delim[i]; i++)
      continue;
    if(i != n)
       break;                                                   /* Found one */
  }
  *ptr = '\0';                                                /* Mark the end */
  return ret;                                         /* Return the beginning */
}



int readMail(buf, len)
char *buf;
int len;
{
  char *cp;
  static char cmdbuf[4096];

  if(len == sizeof(struct SPECDATA))
  {
    if(debug)
    {
      log_msg("Got SPECDATA struct");
    }

    bcopy(buf, (char *)&sd, len);

    otf.IFNumber     = (float)sd.feed / 100.0;
    otf.singleChan   = sd.singleChan;
    otf.singleOtfBin = sd.singleOtfBin;
    otf.debugSpectra = sd.debugSpectra;
    otf.arrayNum     = sd.arrayNum;

    show_display();

    return(0);
  }

  if(debug)
  {
    log_msg("readMail(): Got %d bytes, %s", len, buf);
  }

  if(buf || len < sizeof(cmdbuf))
  {
    cp = vxstrtok(buf, NULL, "\n");
    while(cp)
    {
      strcpy(cmdbuf, cp);
      parseMail(cmdbuf);
      cp = vxstrtok(NULL, cp, "\n");
    }
  }

  return(0);
}


int parseMail(buf)
char *buf;
{
  char *s;
  struct MAIL_PARSE *p;

  deNewLineIt(buf);

  strlwr(buf);

  for(p=parse_table;p->msg;p++)
  {
    if(!strncmp(p->msg, buf, p->len))
    {
      if(p->log && strlen(buf) < 2048)
      {
        s = (char *)sock_name(last_msg());
        if(s)
          log_msg("%s: (%s) %s", prgName, sock_name(last_msg()), buf);
        else
          log_msg("%s: %s", prgName,  buf);
      }

      p->fctn(buf+p->len);

      return(0);
    }
  }

  log_msg("Command %s NOT found", buf);

  return(0);
}


int otfInit()
{
  if(debug)
  {
    log_msg("otfInit(): Enter");
  }

  otf.initialized        = 0;
  otf.bins               = 10;
  otf.plots              = 5;
  otf.singleChan         = 126;
  otf.arrayNum           = OTFRA;
  otf.singleOtfBin       = 8;
  otf.reqSpecIndx        = -1;
  otf.gridBaseLine       = 1;
  otf.noPlot             = 0;
  otf.newShade           = 1;
  otf.requestCol         = 5;
  otf.requestRow         = 5;
  otf.cols               = 32;
  otf.rows               = 32;
  otf.noint              = 1024;
  otf.int_chans[0]       = 508;
  otf.int_chans[1]       = 516;
  otf.lockScale          = 0;
  otf.valueMaxChanged    = 0;
  otf.intenChanged       = 0;
  otf.loadChoice         = LOAD_PREVIEW;
  otf.lastAction         = NORMAL_MAP;
  otf.noColorBar         = 0;
  otf.manMapRa           = 0.0;
  otf.manMapDec          = 0.0;
  otf.manNyquistRa       = 11.1761475;
  otf.manNyquistDec      = 8.05853271;
  otf.manMapWidth        = 0.60;
  otf.manMapHeight       = 0.60;
  otf.windowSizeSet      = 0;
  otf.chanCompress       = 1;
  otf.scaleFactor        = 1.0;
  otf.gamma              = 1.0;
  otf.whichPlot          = PLOT_INT_INTENSITY;
  otf.IFNumber           = 0.01;
  otf.chanSelOTF         = 0;
  otf.nyquistRa          = 22.35;
  otf.nyquistDec         = 22.35;
  otf.valueMax           = 10.0;
  otf.map_scans_head     = NULL;
  otf.gridArray          = NULL;
  otf.gridArrayChans     = NULL;
  otf.plotxb             = OTFPLOTXB;
  otf.plotyb             = OTFPLOTYB;
  otf.plotxe             = OTFPLOTXE;
  otf.plotye             = OTFPLOTYE;
  otf.freqres            = -1.0;
  otf.restfreq           = -1.0;
  otf.velocity           = -1.0;
  otf.xsource            = -1.0;
  otf.ysource            = -100.0;
  otf.peakStr[0]         = '\0';
  otf.ximage             = NULL;
//  otf.colorChoice        = doColor;
  otf.cutoff             = 0;
  otf.regionshowbold     = 0;
  otf.baselines[0]       = 1;
  otf.baselines[1]       = 30;
  otf.baselines[2]       = -1;
  otf.baselines[3]       = -1;
  otf.baselines[4]       = -1;
  otf.baselines[5]       = -1;
  otf.baselines[6]       = 224;
  otf.baselines[7]       = 254;

//  otfIFNumber = otf.IFNumber;

  if(debug)
  {
    log_msg("otfInit(): Done");
  }

  return(0);
}



int main(argc,argv)
int argc;
char *argv[];
{
  char *cp, tfile[256];
  int cw, ch, bg;
  int xpos, ypos;
  char title[80];

  setCactusEnvironment();

  manualInitials = 1;
  strcpy(manInitials, "ssk");

  atexit((void *)exitProc);

#ifndef __DARWIN__
  signal(SIGINT,  (__sighandler_t)signalHandler);
  signal(SIGTERM, (__sighandler_t)signalHandler);
#else
  signal(SIGINT,  signalHandler);
  signal(SIGTERM, signalHandler);
#endif

  strcpy(prgName, "SPECDATA");

  cp = getenv("LPSESSION");

  if(cp)
  {
    sprintf(tfile, "LOGDIR=/tmp/%s", cp);

    putenv(tfile);
  }
  else
  {
    fprintf(stderr, "No SESSION defined\n");
    exit(1);
  }

  smart_log_open("specdata", 3);
 

  setpgid( (pid_t)0, (pid_t)0);        /* divorce myself from parents */

  PID = getpid();

  cp = getenv("TOPDIR");
  if(cp)
    sprintf(datadir,"%s",cp);
  else
    sprintf(datadir,"/home/data");

  cp = getenv("FIXDAT");
  if(cp)
    sprintf(fixeddir,"%s",cp);
  else
    sprintf(fixeddir,"/home/data4/fixed");

  cp = getenv("_3DDIR");
  if(cp)
    sprintf(_3ddir,"%s",cp);
  else
    sprintf(_3ddir,"/home/data1/3d");

  cp = getenv("DISPLAY");

  if(cp)
  {
    sprintf(localHost, "%s", cp);
  }

  xgr_head.prev = xgr_head.next = &xgr_head;

  if((server=(struct XGR_ *)calloc(1,sizeof(struct XGR_)))==NULL)
  {
    log_msg("error in calloc for main server");
    return(1);
  }

  insque(server, xgr_head.prev);

  cw   = 650;
  ch   = 790;
  xpos =  10;
  ypos =  10;

  strcpy(server->serverName, localHost);

  if((server->dpy = XOpenDisplay(localHost)) == (Display *)NULL)
  {
    log_msg("Unable to open display %s", localHost);
    return(0);
  }


  sock_bind("LINUXPOPS_SPECDATA");
//  log_eventdEnable(1);                 /* enable loging to the event daemon */

  master   = sock_connect("LINUXPOPS_MASTER");
  helper   = sock_connect("LINUXPOPS_HELPER");
  xgraphic = sock_connect("LINUXPOPS_XGRAPHIC");
  specplot = sock_connect("LINUXPOPS_SPECPLOT");

  set_sock_callback( (int (*)(void))readMail );

  strcpy(title,"Specdata");
  strcpy(mainFrameTitle,"Specdata");

  otfInit();

  bg = DS_BLACK;

  createWindows(server, title, cw, ch, xpos, ypos, bg, globalUsPixmap);

  XSetErrorHandler(myErrorHandler);
  XSetIOErrorHandler((XIOErrorHandler)myIOErrorHandler);

  main_loop();

  return(0);
}


int myIOErrorHandler(display, myerr)
Display *display;
XErrorEvent *myerr;
{
  if(display)
  {
    log_msg("Server %s caused IO Trap", DisplayString(display));
    strcpy(deadServer, DisplayString(display));
  }
  else
  {
    log_msg("Server: Unknown Server caused IO Trap");
    strcpy(deadServer, "Unknown Server");
  }
  exit(2);
}



int myErrorHandler(display, myerr)
Display *display;
XErrorEvent *myerr;
{
  if(display)
  {
    log_msg("Server %s caused Xlib Error", DisplayString(display));
    strcpy(deadServer, DisplayString(display));
  }
  else
  {
    log_msg("Server: Unknown Server caused Xlib Error");
    strcpy(deadServer, "Unknown Server");
  }
  return(0);
}


void signalHandler()
{
  struct XGR_HEAD *p;
  struct XGR_     *s;

  exit(1);

  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;
    if(s->dpy)
    {
      log_msg("Signal: Shuting Down Server %s", DisplayString(s->dpy));
      XFlush(s->dpy);
      XCloseDisplay(s->dpy);
    }
  }
}


int exitProc(status)
int status;
{
  log_msg("Exiting with status %d", status);

  /* kill the spec viewer */
  sock_send(specplot, "quit");

  return(status);
}



int dummieEventProc(event)
XEvent *event;
{
  KeySym   keysym;
  XComposeStatus compose;
  char     buffer[1];
  int      bufsize=1;

  if(event->type == KeyPress)
  {
    XLookupString((XKeyEvent *)event, buffer, bufsize, &keysym, &compose);

    if(keysym == XK_p && event->xkey.state == ControlMask)
    {
      log_msg("Stand-by, Making Copy of SpecData Window.");
//      do_diagHarCopyServ();
    }
  }

  return(0);
}


#define WINDOW_PLOT 1
#define WINDOW_OTF  2

/* Create individual window */

struct WINDOW *createWindow(s, title, cw, ch, xpos, ypos, bg, usPixmap, which)
struct XGR_ *s;
char *title;
int cw, ch, xpos, ypos, bg, usPixmap, which;
{
  unsigned long value_mask;
  XSetWindowAttributes attr;
  XSizeHints size_hints;
  struct WINDOW *w;
  XWMHints wm_hints;
  XTextProperty *wi, *ic;

  if((w = (struct WINDOW *)calloc(1, sizeof(struct WINDOW ))) == NULL)
  {
    log_msg("error in calloc for main Plot window");
    exit(1);
  }

  insque(w, s->windows.prev);

  strcpy(w->frameTitle, title);
  w->canw       = cw;
  w->canh       = ch;
  w->background = bg;
  w->first      = 1;
  w->win        = XCreateSimpleWindow(s->dpy, RootWindow(s->dpy,s->scr_num),
                                      xpos, ypos, (unsigned int)w->canw, 
					    (unsigned int)w->canh, 
					1,  s->colors[DS_WHITE],
					    s->colors[DS_BLACK]);
  w->usPixmap   = usPixmap;

  if(w->usPixmap)
  {
    if(!(w->pixmap = XCreatePixmap(s->dpy, w->win, (unsigned int)w->canw, (unsigned int)w->canh, s->depth)))
    {
      log_msg("%s: Error in create of off screen pixmap\n", prgName);
      exit(1);
    }
  }

  if(which == XGR_MAIN_WIN)
  {
    XSelectInput(s->dpy, w->win, frame_mask );
  }

  size_hints.flags        = PPosition | PSize | PMinSize/* | PAspect*/;
  size_hints.min_width    = cw;
  size_hints.min_height   = ch;
  size_hints.x            = xpos;
  size_hints.y            = ypos;

  w->gc   = XCreateGC(s->dpy, RootWindow(s->dpy, s->scr_num), (unsigned long)0, (XGCValues *)NULL);

  w->invalid               = 1;

  if(which == XGR_MAIN_WIN)
  {
    w->icon = XCreateBitmapFromData(s->dpy, w->win, dataserv_bmp_bits,
						    dataserv_bmp_width, 
						    dataserv_bmp_height); 
  }
  else
  {
    w->icon = XCreateBitmapFromData(s->dpy, w->win, datagrid_bmp_bits,
						    datagrid_bmp_width, 
						    datagrid_bmp_height); 
  }
 
  XSetStandardProperties(s->dpy, w->win, 
			 w->frameTitle, (char *)NULL, w->icon,
			 (char **)NULL, 0, &size_hints);

  wi = NULL;
  ic = NULL;

  wm_hints.initial_state = NormalState;
  wm_hints.input         = True;
  wm_hints.icon_pixmap   = 0;
  wm_hints.flags         = StateHint | InputHint | WindowGroupHint;
  wm_hints.window_group  = w->win;

  XSetWMProperties(s->dpy, w->win, wi, ic, NULL, 0, &size_hints, &wm_hints, NULL);


  if(DoesBackingStore(XDefaultScreenOfDisplay(s->dpy)))
  {
    if(debug)
    {
      log_msg("createWindow(): Setting Backing Store for Plot Window");
    }

    value_mask = CWBackingStore;
    attr.backing_store = WhenMapped;
    XChangeWindowAttributes(s->dpy, w->win, value_mask, &attr);
  }

  XMapWindow(s->dpy, w->win);
  w->mapped       = 1;

  XSetWindowBackground(s->dpy, w->win, s->colors[w->background]);
  XSetBackground(s->dpy, w->gc, s->colors[w->background]);
  XClearWindow(s->dpy, w->win);

  s->cur = w;

  return(w);
}


int createWindows(s, title, cw, ch, xpos, ypos, bg, usPixmap)
struct XGR_ *s;
char *title;
int cw, ch, xpos, ypos, bg, usPixmap;
{
  int x, y, w, h;
  struct WINDOW *win;

  log_msg("DisplayInfo for %s - %s major %d minor %d release %d", 
           DisplayString(s->dpy), ServerVendor(s->dpy), ProtocolVersion(s->dpy),
	   ProtocolRevision(s->dpy), VendorRelease(s->dpy));

  s->xfd          = XConnectionNumber(s->dpy);
  s->scr_num      = DefaultScreen(s->dpy);
  s->numFonts     = XGR_MAX_FONTS;
  s->fontNames    = findFixedFonts(s->dpy, &s->numFonts);
  s->windows.prev = s->windows.next = &s->windows;
  s->depth        = XDefaultDepthOfScreen(XDefaultScreenOfDisplay(s->dpy));
  s->userXimage   = NULL;

  x = xpos;
  y = ypos;
  w = cw;
  h = ch;
						/* Create main plot window */
  win = createWindow(s, title, w, h, x, y, bg, usPixmap, XGR_MAIN_WIN);
  win->repaint_proc = mainFrameRepaint;
  win->event_proc   = dummieEventProc;
  s->plotwin        = win;

  xgr_graphInit(s);

  return(0);
}




int argmatch(s1, s2, atleast)
char *s1;
char *s2;
int atleast;
{
  int l1 = strlen(s1);
  int l2 = strlen(s2);

  if(l1 < atleast)
    return 0;

  if(l1 > l2)
    return 0;

  return(strncmp(s1, s2, l1) == 0);
}


int usage(progname)
char *progname;
{
  fprintf(stderr, "\nUsage of '%s' command line arguments: \n", progname);
  fprintf(stderr, "-ws  x y                   Set Window size to x y\n");
  fprintf(stderr, "-nopixmap                  Set dataserv in non-pixmap mapping mode\n");
  fprintf(stderr, "-obs initials              Set initials to display;  defaults to current observer\n");
  fprintf(stderr, "-ver n dir                 Set datafile ver to n, located on disk dir;\n");
  fprintf(stderr, "                           defaults to current observers version\n");
  fprintf(stderr, "-maxcolors [16|32|64|128]  Set max colors of image display;  default 64\n");
  fprintf(stderr, "-color [GRAY|PSEUDO|FLAME] Set color mode of image display;  default PSEUDO\n");
  fprintf(stderr, "-usage                     This message\n");
  exit(0);
}


int mainFrameRepaint()
{
  if(strlen(sd.datafile))
  {
    showDisplay();
  }

  return(0);
}


int verifyScanDone(scn)
int scn;
{
  int i;

  for(i=0;i<100;i++)
  {
    if(scn == scansDone[i])
    {
      scanDone = 1;
      return(0);
    }
  }

  insertScan(scn);
  scanDone = 0;

  return(1);
}



int insertScan(scn)
int scn;
{
  scansDoneInx++;
  if(scansDoneInx > 99)
    scansDoneInx = 0;

  scansDone[scansDoneInx] = scn;

  return(0);
}



int to_monitor(buf)
char *buf;
{
  log_msg("To Monitor: %s", buf);

  return(0);
}


int to_rambo(buf)
char *buf;
{
  log_msg("To Rambo: %s", buf);

  return(0);
}

int to_exec(buf)
char *buf;
{
  log_msg("To Exec: %s", buf);

  return(0);
}

int do_toggleFixData(buf)
char *buf;
{
  if(buf)
    useFixData = atoi(buf);
  else
    useFixData = 0;

  log_msg("Setting use of fixed data to %d", useFixData);

  return(0);
}


int show_display()
{
  showDisplay();

  freeDataPointers();

  return(0);
}


/* dummie function; we don't ignore scans here; alway return 0 */
int checkIgnoreScans(v, s)
int v;
float s;
{
  return(0);
}


int loadFile(be, modeid)
int *be, *modeid;
{
  char *cp;
  int ret=0;
  float scan;

  if(strlen(sd.datafile) == 0)
  {
    log_msg("No structure sent yet");

    return(1);
  }

  if(debug)
  {
    log_msg("Scan = %d, feed = %d", sd.scanno, sd.feed);
  }

  scan = (float)sd.scanno + (float)sd.feed / 100.0;

  cp = sd.datafile;

  if(debug)
  {
    log_msg("Looking for scan %.2f in %s", scan, cp);
  }

  if(peekScan(scan, cp, be, modeid, &onhead))
  {
    log_msg("Scan %.2f, not found in %s", scan, cp);
    return(1);
  }

  if( (ret = read_scan( scan, cp, &ondata, &onhead)) != 0 )
  {
    return(ret);
  }

  if(*modeid == OTF)
  {
    scan = onhead.offscan;

    if(debug)
    {
      log_msg("Scan is OTF: Loading Off Scan %.2f", scan);
    }

    if( (ret = read_scan( scan, cp, &offdata, &offhead)) != 0 )
    {
      log_msg("Off Scan %.2f, not found in %s", scan, cp);
      return(ret);
    }

    cp   = sd.gainsfile;
    scan = onhead.gains;

    if(debug)
    {
      log_msg("Loading Gains Scan %.2f", scan);
    }

    if( (ret = read_scan( scan, cp, &gainsdata, &gainshead)) != 0 )
    {
      log_msg("Gains Scan %.2f, not found in %s", scan, cp);
      return(ret);
    }
  }

  strncpy(source, onhead.object, SRCLEN);
  source[SRCLEN] = '\0';

  return(ret);
}


int showDisplay()
{
  int ret, be, modeid;

  /* Read scan here */

  ret = loadFile(&be, &modeid);

  if(ret)
  {
    return(1);
  }

  xgr_setCurWindow(XGR_MAIN_WIN, XGR_SET_PIXMAP);

  switch(modeid) 
  {
    case TP_OFF:
    case TPM_OFF:
      do_linetotalmapoff();
      break;

    case OTF:
      do_lineotf();
      break;

    default:
      do_linedefault();
      break;
  }


  xgr_pixmapToWindow(mainFrameTitle);

  xgr_mapUnmapWindow(mainFrameTitle, 1);

  return(0);
}



int main_loop()
{
  struct XGR_HEAD *p, *p2;
  struct XGR_ *s, *f;
  int count, n, xfd;
  int xfds[256];

  while (1)
  {
    count=0;

    for(p=xgr_head.next;p!=&xgr_head;p=p->next)
    {
      s = (struct XGR_ *)p;

      if(XPending(s->dpy))
      {
	processEvent(s);
	count++;
      }
    }

    if(!count) 
    {
      n=0;

      for(p2=xgr_head.next;p2!=&xgr_head;p2=p2->next)
      {
        f = (struct XGR_ *)p2;
        xfds[n] = f->xfd;
	n++;
      }

      xfd = block_for_event(xfds, n);

      for(p2=xgr_head.next;p2!=&xgr_head;p2=p2->next)
      {
        f = (struct XGR_ *)p2;

	if(xfd == f->xfd) 
	{
	  if(XPending(f->dpy) == 0)
	  {
	    if(f->dpy == server->dpy)	/* is it the main one */
	      exit(0);

            log_msg("Removeing %s in destoryFunc", DisplayString(f->dpy));
	    remque(f);
	    xgr_freeWindows(f);

	    break;
	  }
	}
      }
    }
  }

  return(0);
}
 

int processEvent(s)
struct XGR_ *s;
{
  XEvent event;

  while(XPending(s->dpy))
  {
    XNextEvent(s->dpy, &event);

    switch( event.type )
    {
      case Expose:
		/* Filter down to the last Expose event */
        while (XCheckTypedWindowEvent(s->dpy, event.xany.window, Expose, &event))
        {
	  ;
        }
 
        xgr_rePaintProc(event.xexpose.window);
 
        break;

      case ConfigureNotify:

		/* Filter down to the last ConfigureNotify event */
        while (XCheckTypedWindowEvent(s->dpy, event.xany.window, ConfigureNotify, &event))
        {
	  ;
        }

        if(event.xconfigure.window && !event.xconfigure.send_event)
	{
          xgr_reSizeProc(event.xconfigure.window, event.xconfigure.width, event.xconfigure.height);
	}

        break;

      case ButtonPress:
          xgr_eventProc(&event, event.xbutton.window, ButtonPress);
        break;

      case MotionNotify:
          xgr_eventProc(&event, event.xbutton.window, MotionNotify);
        break;

      case KeyPress:
          xgr_eventProc(&event, event.xbutton.window, KeyPress);
        break;

      case UnmapNotify:
        break;

      case DestroyNotify:
        log_msg("Destroy");

        exit((int)3);

        break;

      default:
        break;
    }
  }

  return(0);
} 




static int block_for_event(xfd, n)
int *xfd;
int n;
{
  int len, i;
  static int sel = 1;

  sel = sock_sel(inMsg, &len, xfd, n, 10, 0);

  if( sel == -1 )
  {                                                /* timeout */
    return(sel);
  } 

  for(i=0;i<n;i++)
  {
    if(sel == xfd[i])
    {
      return(sel);
    }
  }
 
  if(sock_callback)
  {
    (*sock_callback)(inMsg, len);
  }

  return(sel);
}


int set_sock_callback( call )
int (*call)();
{
  sock_callback = call;

  return(0);
}



int synchronize(v)
int v;
{
  struct XGR_HEAD *p;
  struct XGR_     *s;

  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;

    XSynchronize(s->dpy, v);
  }

  return(0);
}


              /* Create an X11 image given a desired width, height and depth. */
XImage *createXimage(display, w, h, depth)
Display *display;
int w, h, depth;
{
  Visual *visual;
  XImage *xi;
  unsigned ui;

  visual = DefaultVisual(display, DefaultScreen(display));

  if(depth == 1)
  {
    return((XImage *)NULL);
  }

  xi = XCreateImage(display, visual, (unsigned int)depth, ZPixmap,0,
                        (char *)NULL, (unsigned int)w, (unsigned int)h, 16, 0);
  if(xi == NULL)
  {
    log_msg("XCreateImage failed");
    return((XImage *)NULL);
  }

  ui = (unsigned)(xi->height*xi->bytes_per_line*sizeof(char));
  if((xi->data=(char *)malloc(ui))==NULL)
  {
    log_msg("Unable to malloc(%d) ximage in createXimage()", ui);
    XDestroyImage(xi);
    return((XImage *)NULL);
  }

  bzero((char *)xi->data, xi->height * xi->bytes_per_line * sizeof(char));

  return(xi);
}


int determine_min_max(ydata, dl, scan)
float *ydata;
int dl;
float scan;
{
  int i;

  if(ydata == NULL)
     return(1);
                                                   /* redetermine min and max */
  datamax = -9999999999.9;
  datamin = 9999999999.0;

  for(i=0;i<dl;i++)
  {
    if(ydata[i] > datamax)
      datamax = ydata[i];
    if(ydata[i] < datamin)
      datamin = ydata[i];
  }

  if(fabs(datamax - datamin) < DATASPREAD)
  {
    log_msg("determine_min_max(): Scan #%7.2f is all %.4f", scan, datamin);

    datamin -= 1.0;
    datamax += 1.0;

    return(1);
  }
  return(0);
}


int freeDataPointers()
{
  if(debug)
  {
    log_msg("Freeing Data Pointers");
  }

  if(ondata) 
  {
    free((char *)ondata);
    ondata = (float *)NULL;
  }

  if(offdata) 
  {
    free((char *)offdata);
    offdata = (float *)NULL;
  }

  if(gainsdata) 
  {
    free((char *)gainsdata);
    gainsdata = (float *)NULL;
  }

  return(0);
}


int displaySingleOtfSpectra()
{
  int i;
  float f, *xArray, *yArray;
  float xb, xe, yb, ye;
  float scale;
  char temp[80];

  xb = 0.05; xe = 0.995;
  yb = 0.05; ye = 1.000;

  if(sd.debugSpectra > otf.numSpectra)
  {
    log_msg("Requested spectra > numOfSpectra");
    return(1);
  }
  if((xArray = (float *)malloc((unsigned)(otf.noint*sizeof(float))))==NULL)
  {
    log_msg("Unable to malloc(%d) xArray in displaySingleOtfSpectra()", otf.noint*sizeof(float));
    return(1);
  }
  if((yArray = (float *)malloc((unsigned)(otf.noint*sizeof(float))))==NULL)
  {
    log_msg("Unable to malloc(%d) yArray in displaySingleOtfSpectra()", otf.noint*sizeof(float));
    free((char *)xArray);
    return(1);
  }

  if(sd.doTPprocess)
  {
    for(i=0;i<otf.noint;i++)
    {
      yArray[i] = ((ondata[i+sd.debugSpectra*otf.noint] - offdata[i])
                        / offdata[i]) * gainsdata[i];
    }
  }
  else
  {
    for(i=0;i<otf.noint;i++)
    {
      yArray[i] = ondata[i+sd.debugSpectra*otf.noint];
    }
  }

  if( onhead.sideband == 3.0)
  {
    f = otf.noint * onhead.freqres / 2.0;

    for(i=0;i<otf.noint;i++)
    {
      xArray[i] =  f - ((float)i * onhead.freqres);
    }
  }
  else
  {
    f = -otf.noint * onhead.freqres / 2.0;

    for(i=0;i<otf.noint;i++)
    {
      xArray[i] = f + ((float)i*onhead.freqres);
    }
  }

  scale = ye -yb;
  sprintf(temp, "Scan %7.2f: Spectra # %d", onhead.scan, sd.debugSpectra);
  xgr_drawText(xb, 0.905*scale + yb, temp, DS_RED, font1);
  xgr_drawBox(xb, 0.05*scale + yb, xe,  0.90*scale + yb, DS_GRAY50);
  globalStart = xArray[0];
  xgr_drawBin(xArray, yArray, otf.noint, DS_GREEN);

  if(xArray)
  {
    free((char *)xArray);
  }

  if(yArray)
  {
    free((char *)yArray);
  }

  return(1);
}




int getNumSpectra(datalen, noint)
float datalen, noint;
{
  int dl;

  dl = (int)datalen / ( sizeof(float) * ( (int)noint +5));

  return(dl);
}



int displayArrayGraph(xb, xe, yb, ye)
float xb, xe, yb, ye;
{
  float *xdata, *ydata, *sig = 0;
  int i;
  float scale, dec_slope, ra_slope;
  float a1, b1, siga1, sigb1, chi2_1, q1 = 0;
  char temp[80];

  scale  = ye -yb;
                                         /* Initialize the x-axis data points */
  if((xdata = (float *)malloc((unsigned)(otf.numSpectra*sizeof(float))))==NULL)
  {
    log_msg("Unable to malloc(%d) xdata in ArrayDisplay()", otf.numSpectra*sizeof(float));
    return(1);
  }

  for(i=0;i<otf.numSpectra;i++)
    xdata[i] = (float)i;

  if((ydata = (float *)malloc((unsigned)(otf.numSpectra*sizeof(float))))==NULL)
  {
    log_msg("Unable to malloc(%d) ydata in ArrayDisplay()", otf.numSpectra*sizeof(float));
    free((char *)xdata);
    return(1);
  }

  switch(sd.arrayNum)
  {
    case OTFAUTO:
                                   /* test which direction telescope is going */
                                                          /* try ra dir first */
      bcopy((char *)&ondata[otf.noint * otf.numSpectra + otf.numSpectra*1],
                (char *)ydata, otf.numSpectra*sizeof(float));

   /* we are not using a weighted lsq so 'sig' doesn't need to be initialized */
      lsqfit(xdata,ydata, 0, i, sig, 0, &b1, &a1, &sigb1, &siga1, &chi2_1, &q1);
      ra_slope = a1;


                                                              /* try dec next */
      bcopy((char *)&ondata[otf.noint * otf.numSpectra + otf.numSpectra*2],
                (char *)ydata, otf.numSpectra*sizeof(float));

      lsqfit(xdata,ydata, 0, i, sig, 0, &b1, &a1, &sigb1, &siga1, &chi2_1, &q1);
      dec_slope = a1;

      if((fabs(ra_slope) < fabs(dec_slope)))
      {
        bcopy((char *)&ondata[otf.noint *otf.numSpectra +otf.numSpectra*1],
                (char *)ydata, otf.numSpectra*sizeof(float));
        sprintf(temp, "R.A. Array");
        scaleData(ydata);
      }
      else
      {
        sprintf(temp, "Dec. Array");
        scaleData(ydata);
      }

      break;
    case OTFTIME:
        bcopy((char *)&ondata[otf.noint * otf.numSpectra], (char *)ydata, otf.numSpectra*sizeof(float));
        sprintf(temp, "Time Array");
        break;
    case OTFRA:
        bcopy((char *)&ondata[otf.noint * otf.numSpectra + otf.numSpectra*1], (char *)ydata, otf.numSpectra*sizeof(float));
        subtractPredictedRA(ydata);
        sprintf(temp, "R.A. Array");
        break;
    case OTFDEC:
        bcopy((char *)&ondata[otf.noint * otf.numSpectra + otf.numSpectra*2], (char *)ydata, otf.numSpectra*sizeof(float));
        scaleData(ydata);
        sprintf(temp, "Dec. Array");
        break;
    case OTFAZ:
        bcopy((char *)&ondata[otf.noint * otf.numSpectra + otf.numSpectra*3], (char *)ydata, otf.numSpectra*sizeof(float));
        sprintf(temp, "Az. Array");
        break;
    case OTFEL:
        bcopy((char *)&ondata[otf.noint * otf.numSpectra + otf.numSpectra*4], (char *)ydata, otf.numSpectra*sizeof(float));
        sprintf(temp, "El. Array");
        break;
    default:
        free((char *)xdata);
        free((char *)ydata);
        return(1);
  }

/*
  if( determine_min_max(ydata, otf.numSpectra, onhead.scan))
    return(0);
 */
  determine_min_max(ydata, otf.numSpectra, onhead.scan);

  xgr_drawBox(xb, 0.05*scale + yb, xe,  0.90*scale + yb, DS_GRAY75);
  xgr_drawText(xb, 0.905*scale + yb, temp, DS_RED, font0);

  globalStart = xdata[0];
  xgr_drawBin(xdata, ydata, otf.numSpectra, DS_GREEN);
  free((char *)xdata);
  free((char *)ydata);

  return(1);
}




int displaySingleChan(xb, xe, yb, ye)
float xb, xe, yb, ye;
{
  int i, j, sc, no;
  float scale;
  char t[80];
  double dtemp;
  float *xdata, *ydata;
  float *x, *y, *w, *o, *g;

  scale  = ye -yb;
  sc     = sd.singleChan -1;

  if(sc < 0)
  {
    sc = 0;
  }

  if((xdata = (float *)malloc((unsigned)(otf.numSpectra*sizeof(float))))==NULL)
  {
    log_msg("Unable to malloc(%d) xdata in singleChan()", otf.numSpectra*sizeof(float));
    return(0);
  }

  if((ydata = (float *)malloc((unsigned)(otf.numSpectra*sizeof(float))))==NULL)
  {
    log_msg("Unable to malloc(%d) ydata in singleChan", otf.numSpectra*sizeof(float));
    free((char *)xdata);

    return(0);
  }

  x  = xdata;
  y  = ydata;
  w  = &ondata[sc];
  o  = &offdata[sc];
  g  = &gainsdata[sc];
  no = otf.noint;

  for(i=0;i<otf.numSpectra; i++, x++, y++, w+=no)
  {
    *x = (float)i;
    *y = ((*w - *o) / *o) * *g;
  }

  if(sd.singleOtfBin)
  {
    for(i=0;i<otf.numSpectra-(sd.singleOtfBin-1);i+=sd.singleOtfBin)
    {
      dtemp = 0.0;

      for(j=0;j<sd.singleOtfBin;j++)
      {
        dtemp += ydata[i+j];
      }

      dtemp /= (float)sd.singleOtfBin;

      for(j=0;j<sd.singleOtfBin;j++)
      {
        ydata[i+j] = dtemp;
      }
    }
  }

  if( determine_min_max(ydata, otf.numSpectra, onhead.scan))
  {
    free((char *)xdata);
    free((char *)ydata);

    return(0);
  }

  sprintf(t,"Chan # %d Binned to %d Chans", sd.singleChan, sd.singleOtfBin);
  xgr_drawText(xb, 0.905*scale + yb, t, DS_RED, font0);
  xgr_drawBox(xb, 0.05*scale + yb, xe,  0.90*scale + yb, DS_GRAY75);

  globalStart = xdata[0];
  xgr_drawBin(xdata, ydata, otf.numSpectra, DS_GREEN);

  free((char *)xdata);
  free((char *)ydata);

  return(1);
}



int stuffOtf(plot, indxX, xArray, yArray)
int plot, *indxX;
float *xArray, *yArray;
{
  int i, j, plotStart, indx;
  double *accum;
  float f;

  plotStart = plot * otf.numSpectra / (otf.plots-1);
  indx      = *indxX;

  if(plot == 0)
  {
    ;
  }
  else
  if(plot == (otf.plots-1))
  {
    plotStart -= otf.bins;
  }
  else
  {
    plotStart -= (otf.bins / 2);
  }

  if( (accum = (double *)malloc((unsigned)(otf.noint*sizeof(double)))) == NULL)
  {
    log_msg("Unable to malloc(%d) accum in stuffOtf()", otf.noint*sizeof(double));
    return(-1);
  }

  for(i=0;i<otf.noint;i++)
  {
    accum[i] = 0.0;

    for(j=0;j<otf.bins;j++)
    {
      accum[i] += ondata[i+j*otf.noint+plotStart*otf.noint];
    }

    yArray[indx++] = ((accum[i] / (float)otf.bins - offdata[i]) / offdata[i]) * gainsdata[i];
  }
                                         /* Initialize the x-axis data points */
  indx = *indxX;
  if( onhead.sideband == 3.0)
  {
    f = otf.noint * onhead.freqres / 2.0;

    for(i=0;i<otf.noint;i++)
    {
      xArray[indx++] =  f - ((float)i*onhead.freqres);
    }
  }
  else
  {
    f = -otf.noint * onhead.freqres / 2.0;

    for(i=0;i<otf.noint;i++)
    {
      xArray[indx++] = f + ((float)i*onhead.freqres);
    }
  }

  *indxX = indx;
  free((char *)accum);

  return(1);
}



int do_otf( head)
struct HEADER *head;
{
  int i;
  float xb=0.050, xe=0.995, yb=0.50, ye=1.0;
  float scale, delta, dxb[12], dxe[12];
  char temp[80];
  int arraySize, indx;
  float  *xArray, *yArray;

  otf.numSpectra = getNumSpectra(head->datalen, head->noint);
  otf.noint      = (int)head->noint;

  if(otf.noint > MAX_DATA_CHANNELS)
  {
    log_msg("DATASERV: Can not grid %d chans", otf.noint);
    return(0);
  }

  xgr_setCurWindow(XGR_MAIN_WIN, XGR_SET_PIXMAP);
  xgr_clearScreen();

  if(sd.debugSpectra > 0)
  {
    displaySingleOtfSpectra();

    return(0);
  }

  scale  = ye -yb;
  delta = (xe - xb ) / (float)otf.plots;
  scale = ye - yb;
  sprintf(temp, "Scan %7.2f:", head->scan);

  xgr_drawText(xb, 0.905*scale + yb, temp, DS_RED, font0);

  arraySize = otf.noint * otf.plots;

  if((xArray = (float *)malloc((unsigned)(arraySize*sizeof(float))))==NULL)
  {
    log_msg("Unable to malloc(%d) xdata in do_otf()", arraySize*sizeof(float));

    return(1);
  }

  if((yArray = (float *)malloc((unsigned)(arraySize*sizeof(float))))==NULL)
  {
    log_msg("Unable to malloc(%d) ydata in do_otf()", arraySize*sizeof(float));
    free((char *)xArray);

    return(1);
  }


  indx = 0;
  for(i=0;i<otf.plots;i++)
  {
    dxb[i] = xb + (float)i*delta;

    if(stuffOtf(i, &indx, xArray, yArray) < 0)
    {
      if(xArray)
      {
        free((char *)xArray);
      }

      if(yArray)
      {
        free((char *)yArray);
      }

      return(1);
    }
  }

  if( determine_min_max(yArray, arraySize, head->scan))
  {
    if(xArray)
    {
      free((char *)xArray);
    }

    if(yArray)
    {
      free((char *)yArray);
    }

    return(0);
  }

  xgr_drawBox(xb, 0.05*scale + yb, xe,  0.90*scale + yb, DS_GRAY75);

  globalStart = xArray[0];
  noXTick     = 1;

  xgr_drawBin(xArray, yArray, arraySize, DS_GREEN);

  for(i=0;i<otf.plots;i++)
  {
    if(i<otf.plots-1)
    {
      dxe[i] = dxb[i+1];
    }
    else
    {
      dxe[i] = xe;
    }

    xgr_drawBox(dxb[i], 0.05*scale + yb, dxe[i], 0.90*scale + yb, DS_GRAY75);

    noPlot=1;
    noYTick=1;

    xgr_drawBin(xArray + i*otf.noint, yArray + i*otf.noint, otf.noint, DS_GREEN);
  }

  displaySingleChan(0.05, 0.495, 0.0, 0.5);
  displayArrayGraph(0.55, 0.995, 0.0, 0.5);

  if(xArray)
  {
    free((char *)xArray);
  }

  if(yArray)
  {
    free((char *)yArray);
  }

  xgr_pixmapToWindow(mainFrameTitle);

  return(0);
}




int do_linetotalmapoff()
{
  if(debug)
  {
    log_msg("Doing Total Map Off Plot");
  }

  return(0);
}


int do_lineotf()
{
  if(debug)
  {
    log_msg("Doing OTF Plot");
  }

  do_otf(&onhead);

  xgr_checkForEvents();

  return(0);
}


int do_linedefault()
{
  if(debug)
  {
    log_msg("Doing Default Spectral Plot");
  }

  return(0);
}


int subtractPredictedRA(data)
float *data;
{
  float xmin, xscale;
  int i;

  xscale = onhead.inttime * onhead.deltaxr / 3600.0;
  xmin   = xscale / -2.0;
  xscale = xscale / (float)otf.numSpectra;

  if(data[0] > 0)
  {
    for(i=0;i<otf.numSpectra;i++)
    {
      data[i] += (xmin + (xscale * (float)i));
      data[i] *= 3600.0;
    }
  }
  else
  {
    for(i=0;i<otf.numSpectra;i++)
    {
      data[i] -= (xmin + (xscale * (float)i));
      data[i] *= 3600.0;
    }
  }

  return(0);
}



int scaleData(data)
float *data;
{
  int i;

  for(i=0;i<otf.numSpectra;i++)
  {
    data[i] *= 3600.0;
  }

  return(0);
}


static float sqrarg;

#define SQR(a) (sqrarg=(a),sqrarg*sqrarg)

int lsqfit(x, y, bdata, ndata, sig, mwt, a, b, siga, sigb, chi2, q)
float x[],y[],sig[],*a,*b,*siga,*sigb,*chi2,*q;
int bdata,ndata,mwt;
{
  int i;
  float wt, t, sxoss, sx = 0.0, sy = 0.0, st2 = 0.0, ss, sigdat;

  *b=0.0;

  if(mwt)
  {
    ss=0.0;

    for(i=bdata;i<ndata;i++)
    {
      wt  = 1.0 / SQR(sig[i]);
      ss += wt;
      sx += x[i] * wt;
      sy += y[i] * wt;
    }
  }
  else
  {
    for(i=bdata;i<ndata;i++)
    {
      sx += x[i];
      sy += y[i];
    }

    ss=ndata-bdata;
  }

  sxoss = sx / ss;

  if(mwt)
  {
    for(i=bdata;i<ndata;i++)
    {
      t    = (x[i] - sxoss) / sig[i];
      st2 += t * t;
      *b  += t * y[i] / sig[i];
    }
  }
  else
  {
    for(i=bdata;i<ndata;i++)
    {
      t    = x[i] - sxoss;
      st2 += t * t;
      *b  += t * y[i];
    }
  }

  *b   /= st2;
  *a    = (sy - sx * (*b)) / ss;
  *siga = sqrt((1.0 + sx * sx / (ss * st2)) / ss);
  *sigb = sqrt(1.0 / st2);
  *chi2 = 0.0;

  if(mwt == 0)
  {
    for(i=bdata;i<ndata;i++)
    {
      *chi2 += SQR(y[i] - (*a) - (*b) * x[i]);
    }

    *q     = 1.0;
    sigdat = sqrt((*chi2) / (ndata - bdata - 2));
    *siga *= sigdat;
    *sigb *= sigdat;
  }

  return(0);
}

#undef SQR
