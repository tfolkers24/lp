/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */

#include "region.h"

#ifdef DATASERV
#include "ds_extern.h"
#include "ds_proto.h"

static int font0 = FONT_SIZE_SMALL;
#else
#include "../dataserv/ds_colors.h"
#include "extern.h"
#endif

struct REGION_BOX *region_box_head;

double max_ra, min_ra, max_dec, min_dec;

char region_tok[40][40];

char *region_delim = "\n\t,() ";

int regions_defined = 0;

int region_get_tok(edit)
char *edit;
{
  int c = 0;
  char *token;

  bzero((char *)&region_tok, sizeof(region_tok));

  token = strtok(edit, region_delim);
  while(token != (char *)NULL)
  {
    sprintf(region_tok[c++], "%s", token);
    token = strtok((char *)NULL, region_delim);
  }

  return(c);
}



int addRegionBox(color, ra, dec, rsize, dsize, title)
int color;
double ra, dec, rsize, dsize;
char *title;
{
  static int indx = 0;
  struct REGION_BOX *b;

  /* Create a fresh list */
  if(region_box_head == NULL)
  {
    if((region_box_head=(struct REGION_BOX *)malloc((unsigned)(sizeof(struct REGION_BOX))))==NULL)
    {
      log_msg("Error in addRegionBox malloc(%d)", sizeof(struct REGION_BOX));

      return(1);
    }

    region_box_head->indx   = indx = 0;
    region_box_head->color  = color;
    region_box_head->ra     = ra;
    region_box_head->dec    = dec;
    region_box_head->rsize  = rsize;
    region_box_head->dsize  = dsize;

    strcpy(region_box_head->title, title);

    region_box_head->next   = NULL;

#ifdef DATASERV
    log_msg("addRegionBox(indx=%d, color=%d, ra=%f, dec=%f, rsize=%f, dsize=%f, title=%s", indx, color, ra, dec, rsize, dsize, title);
#else
    st_report("addRegionBox(indx=%d, color=%d, ra=%f, dec=%f, rsize=%f, dsize=%f, title=%s", indx, color, ra, dec, rsize, dsize, title);
#endif

    indx++;

    regions_defined = indx;

    return(0);
  }

  /* Find next available slot */
  for(b=region_box_head;b->next; b=b->next)
  {
    ;
  }

  /* Append new scan */
  if((b->next=(struct REGION_BOX *)malloc((unsigned)(sizeof(struct REGION_BOX))))==NULL)
  {
    log_msg("Error in addRegionBox malloc(%d)", sizeof(struct REGION_BOX));

    return(1);
  }

  b->next->indx       = indx;
  b->next->color      = color;
  b->next->ra         = ra;
  b->next->dec        = dec;
  b->next->rsize      = rsize;
  b->next->dsize      = dsize;

  strcpy(b->next->title, title);

  b->next->next = NULL;

#ifdef DATASERV
  log_msg("addRegionBox(indx=%d, color=%d, ra=%f, dec=%f, rsize=%f, dsize=%f, title=%s", indx, color, ra, dec, rsize, dsize, title);
#else
  st_report("addRegionBox(indx=%d, color=%d, ra=%f, dec=%f, rsize=%f, dsize=%f, title=%s", indx, color, ra, dec, rsize, dsize, title);
#endif

  indx++;

  regions_defined = indx;

  return(0);
}


int remRegions()
{
  struct REGION_BOX *p;
  struct REGION_BOX *q;

#ifdef DATASERV
  log_msg("remRegions(); Removing Old Regions");
#else
  st_report("remRegions(); Removing Old Regions");
#endif

  for(p=region_box_head; p; p=q)
  {
    q = p->next;

    if(p)
    {
      free((char *)p);
    }
  }

  region_box_head = NULL;
  regions_defined = 0;

  return(0);
}


int listRegions()
{
  struct REGION_BOX *p;

#ifdef DATASERV
  log_msg("listRegions();");
#else
  st_report("listRegions();");
#endif

  for(p=region_box_head;p;p=p->next)
  {
    if(p)
    {
      if(p->ra != 0.0 && p->dec != 0.0)
      {
#ifdef DATASERV
        log_msg("BOX( %f, %f, %f, %f, %d ) %s", p->ra, p->dec, p->rsize, p->dsize, p->color, p->title);
#else
        st_report("BOX( %f, %f, %f, %f, %d ) %s", p->ra, p->dec, p->rsize, p->dsize, p->color, p->title);
#endif
      }
    }
  }

  return(0);
}



int computeRegionExtents()
{
  double ra, dec;
  struct REGION_BOX *p;

#ifdef DATASERV
  log_msg("listRegions();");
#else
  st_report("listRegions();");
#endif

  max_ra  = -1.0;
  min_ra  = 500.0;
  max_dec = -100;
  min_dec = 100.0;

  for(p=region_box_head;p;p=p->next)
  {
    if(p)
    {
      if(p->ra != 0.0 && p->dec != 0.0)
      {
	ra = p->ra + p->rsize / 2.0;
        if(ra > max_ra)
        {
          max_ra = ra;
        }

	ra = p->ra - p->rsize / 2.0;
        if(ra < min_ra)
        {
          min_ra = ra;
        }

	dec = p->dec + p->dsize / 2.0;
        if(dec > max_dec)
        {
          max_dec = dec;
        }

	dec = p->dec - p->dsize / 2.0;
        if(dec < min_dec)
        {
          min_dec = dec;
        }
      }
    }
  }

#ifdef DATASERV
  log_msg("Map externs: Ra+ = %f, Ra- = %f, Dec+ = %f, Dec- = %f", max_ra, min_ra, max_dec, min_dec);
  log_msg("Map Size: Ra = %f, Dec %f", (max_ra - min_ra), (max_dec - min_dec));
#else
  st_report("Map externs: Ra+ = %f, Ra- = %f, Dec+ = %f, Dec- = %f", max_ra, min_ra, max_dec, min_dec);
  st_report("Map Size: Ra = %f, Dec %f", (max_ra - min_ra), (max_dec - min_dec));
#endif

  return(0);
}


int do_listRegions(buf)
char *buf;
{
  listRegions();

  return(0);
}



int do_remRegions(buf)
char *buf;
{
#ifdef DATASERV
  char tbuf[256];
#endif

  remRegions();

#ifdef DATASERV
  otf.regionshow = 0;

  strcpy(tbuf, "0");

  do_showRegion(tbuf);
#endif

  return(0);
}


/*

# Region file format: DS9 version 4.1
global color=green dashlist=8 3 width=1 font="helvetica 10 normal roman" select=1 highlite=1 dash=0 fixed=0 edit=1 move=1 delete=1 include=1 source=1
fk5
box(20:24:44.4114,38:58:12,10',10',0) # color=green width=2
box(20:24:43.4389,39:08:12,10',10',0) # color=green width=2
box(20:24:42.4582,39:18:12,10',10',0) # color=green width=2

box(20:24:44.4114,38:58:12,10',10',0) # color=green width=2 text={CO21_001}
#box(20:24:43.4389,39:08:12,10',10',0) # color=green width=2 text={CO21_001}
#box(20:24:42.4582,39:18:12,10',10',0) # color=green width=2 text={CO21_001}
#box(20:24:41.4694,39:28:12,10',10',0) # color=green width=2 text={CO21_001}
#box(20:24:40.4723,39:38:12,10',10',0) # color=green width=2 text={CO21_001}
#box(20:24:39.4669,39:48:12,10',10',0) # color=green width=2 text={CO21_001}
#box(20:24:38.4531,39:58:12,10',10',0) # color=green width=2 text={CO21_001}



 0       1            2     3   4  5 6     7         8
box 20:24:41.4694 39:28:12 10' 10' 0 # color=green width=2
 */


int do_loadRegion(buf)
char *buf;
{
  int    n, color;
  double ra, dec, rsize, dsize, scale = 1.0;
  char   inBuf[256], fname[256], title[256], *cp, tmpBuf[256];
  FILE  *fp;

#ifdef DATASERV
  int reply = 0;
  char tbuf[256];
#endif

  remRegions();


  cp = strtok(buf, "\n\r\t ");
  if(!cp)
  {
    log_msg("do_loadRegion(): No filename passed");

    return(1);
  }

  strcpy(fname, cp);

  cp = strtok(NULL, "\n\r\t ");
  if(!cp)
  {
    log_msg("do_loadRegion(): No scale passed");

    return(1);
  }

  scale = atof(cp);

#ifdef DATASERV
  cp = strtok(NULL, "\n\r\t ");
  if(cp)
  {
    reply = atoi(cp);
  }

  log_msg("Reply = %d", reply);
#endif

  if( (fp = fopen(fname, "r") ) <= (FILE *)0)
  {
    log_perror("open");

    log_msg("do_loadRegion(): Unable to open region file '%s'", fname);

    return(1);
  }

#ifdef DATASERV
  log_msg("do_loadRegion(): Reading in region file: '%s'", fname);
#else
  st_report("do_loadRegion(): Reading in region file: '%s'", fname);
#endif

  while(fgets(inBuf, 256, fp) != NULL)
  {
    if(!strncmp(inBuf, "#", 1))
    {
      continue;
    }

    bzero((char *)&title, sizeof(title));
    color   = 0;
    ra 	    = 0.0;
    dec     = 0.0;
    rsize   = 0.0;
    dsize   = 0.0;

    strcpy(tmpBuf, inBuf);

	/* 0123456         */
	/* text={CO21_001} */
    if((cp = strstr(tmpBuf, "text")))
    {
      strcpy(title, cp+6);
      strcmprs(title); /* Strimp and excess spaces */

      title[strlen(title)-1] = '\0';
    }

    n = region_get_tok(inBuf);

    if(n > 0)
    {
      if(!strcmp(region_tok[0], "global"))
      {
      }
      else
      if(!strcmp(region_tok[0], "fk5"))
      {
      }
      else
      if(!strcmp(region_tok[0], "box"))
      {
	ra    = ctod(region_tok[1]) * 15.0;
	dec   = ctod(region_tok[2]);

	rsize = atof(region_tok[3]);
        if(strstr(region_tok[3], "'"))
        {
          rsize /= 60.0;
        }
	else
        if(strstr(region_tok[3], "\""))
        {
          rsize /= 3600.0;
        }

	dsize = atof(region_tok[4]);
        if(strstr(region_tok[4], "'"))
        {
          dsize /= 60.0;
        }
	else
        if(strstr(region_tok[4], "\""))
        {
          dsize /= 3600.0;
        }

	color = DS_BLACK;
	if(strstr(region_tok[7], "green"))
        {
	  color = DS_GREEN;
        }
	else
	if(strstr(region_tok[7], "red"))
        {
	  color = DS_RED;
        }
	else
	if(strstr(region_tok[7], "blue"))
        {
	  color = DS_BLUE;
        }
	else
	if(strstr(region_tok[7], "yellow"))
        {
	  color = DS_YELLOW;
        }
	else
	if(strstr(region_tok[7], "cyan"))
        {
	  color = DS_CYAN;
        }
	else
	if(strstr(region_tok[7], "magenta"))
        {
	  color = DS_MAGENTA;
        }

	addRegionBox(color, ra, dec, rsize, dsize, title);
      }
    }
  }

  fclose(fp);

  listRegions();

  computeRegionExtents();

  ra  = (max_ra  - min_ra)  / 2.0 + min_ra;
  dec = (max_dec - min_dec) / 2.0 + min_dec;

  rsize = (max_ra  - min_ra ) * scale;
  dsize = (max_dec - min_dec) * scale;

#ifdef DATASERV

  if(reply == 1)
  {
    log_msg("RA: %f, Dec: %f, Rsize = %f, Dsize = %f", ra, dec, rsize, dsize);
    sprintf(tbuf, "otf_map_ra = %f", ra);
    to_helper(tbuf);

    sprintf(tbuf, "otf_map_dec = %f", dec);
    to_helper(tbuf);

    sprintf(tbuf, "otf_map_xsize = %f", rsize);
    to_helper(tbuf);

    sprintf(tbuf, "otf_map_ysize = %f", dsize);
    to_helper(tbuf);
  }

  otf.regionshow = 1;

  paintOtfGrid();

#else
  extern double region_ra, region_dec, region_rsize, region_dsize;

  region_ra    = ra;
  region_dec   = dec;
  region_rsize = rsize;
  region_dsize = dsize;

#endif

  return(0);
}

#ifdef DATASERV

int do_showRegion(buf)
char *buf;
{
  char *cp;

  cp = strtok(buf, "\n\r\t ");

  if(cp)
  {
    otf.regionshow = atoi(cp);
  }
  else
  {
    log_msg("do_showRegion() missing arg");
    return(1);
  }

//  log_msg("do_showRegion(): Setting Region Show to %d", otf.regionshow);
  
  if(otf.regionshow)
  {
    otf.valueMaxChanged = 1;
    otf.intenChanged    = 1;
    otf.newShade        = 1;

    if(otf.head == NULL)
    {
      return(1);
    }

    paintOtfGrid();

    displayRequestedOtf();
  }
  else
  {
    doOtfGridRePaint();
  }

  return(0);
}


int do_toggleRegion()
{
  char tbuf[256];

  if(!regions_defined)
  {
    log_msg("No regions defined");
    printf("No regions defined\n");

    return(1);
  }

  if(otf.regionshow)
  {
    otf.regionshow = 0;
  }
  else
  {
    otf.regionshow     = 1;
    otf.regionshowbold = 0; /* Reset to 0 */
  }

  sprintf(tbuf, "%d", otf.regionshow);

  do_showRegion(tbuf);

  return(0);
}


int do_toggleRegionLabels()
{
  char tbuf[256];

  if(!regions_defined)
  {
    log_msg("No regions defined");
    printf("No regions defined\n");

    return(1);
  }

  if(otf.regionlabelshow)
  {
    otf.regionlabelshow = 0;
  }
  else
  {
    otf.regionlabelshow = 1;
  }

  sprintf(tbuf, "%d", otf.regionshow);

  do_showRegion(tbuf);

  return(0);
}


int do_toggleRegionBold(dir)
int dir;
{
  char tbuf[256];

  if(!regions_defined)
  {
    log_msg("No regions defined");
    printf("No regions defined\n");

    return(1);
  }

  if(dir == 1)
  {
    otf.regionshowbold++;
  }
  else
  {
    otf.regionshowbold--;
  }

  if(otf.regionshowbold > 23)
  {
    otf.regionshowbold = 0;
  }

  if(otf.regionshowbold < 0)
  {
    otf.regionshowbold = 23;
  }

  sprintf(tbuf, "%d", otf.regionshow);

  do_showRegion(tbuf);

  return(0);
}




int drawRegions()
{
  int c;
  float xl, xr, yt, yb;
  float xmin, xmax, ymin, ymax;
  float xscale, yscale;
  struct REGION_BOX *p;

  xscale = otf.manNyquistRa / 3600.0 * otf.cols;
  xmin = xscale / -2.0 - otf.manNyquistRa / 7200.0;
  xmax = xscale /  2.0 - otf.manNyquistRa / 7200.0;

  yscale = (otf.manNyquistDec * otf.rows) / 3600.0;
  ymin = yscale / -2.0;
  ymax = yscale /  2.0;

  otf.plotxb = OTFPLOTXB;
  otf.plotxe = OTFPLOTXE;
  otf.plotyb = OTFPLOTYB;
  otf.plotye = OTFPLOTYE;


  for(p=region_box_head;p;p=p->next)
  {
    if(p)
    {
      if(p->ra != 0.0 && p->dec != 0.0)
      {

        xl = (otf.manMapRa - p->ra) - (p->rsize / cos(otf.manMapDec * CTR)) / 2.0;
        xr = (otf.manMapRa - p->ra) + (p->rsize / cos(otf.manMapDec * CTR)) / 2.0;

        yt = (p->dec - otf.manMapDec) - p->dsize / 2.0;
        yb = (p->dec - otf.manMapDec) + p->dsize / 2.0;


        switch(otf.regionshowbold)
        {
//          case 1:  c = GRAY32;   break;
//          case 2:  c = GRAY50;   break;
//          case 3:  c = GRAY75;   break;
//          case 4:  c = BLACK;    break;

          case  0: c = p->color;           break;
	  default: c = otf.regionshowbold; break;
        }

        xgr_drawRectangle( xmin, xmax, ymin, ymax, xl, yt, xr, yb, c, 0);

	if(otf.regionlabelshow)
        {
	  /* Turns out, 'yb' discribes the top of the box */
          xgr_drawTextInGrid( xmin, xmax, ymin, ymax, xl, yb, p->title, c, font0);
	}

      }
    }
  }
  

  return(0);
}

#endif
