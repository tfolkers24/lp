#ifndef DS_COLORS_H
#define DS_COLORS_H

#define DS_WHITE	0
#define DS_BLACK	1
#define DS_GRAY32	2
#define DS_GRAY50	3
#define DS_GRAY75	4
#define DS_RED		5
#define DS_YELLOW	6
#define DS_GREEN	7
#define DS_MAGENTA	8
#define DS_BLUE		9
#define DS_MAROON	10
#define DS_ORANGE	11
#define DS_CYAN		12
#define DS_CORAL	13
#define DS_PURPLE	14
#define DS_SALMON	15
#define DS_BROWN	16
#define DS_ORCHID	17
#define DS_LAVENDER	18
#define DS_STEEL_BLUE	19
#define DS_TAN		20
#define DS_SIENNA	21
#define DS_PINK		22
#define DS_CYAN4	23
#define MAX_DS_COLORS   24 

#define START_COLOR      7
#define MAX_GRAY_SCALE 256 

#define DS_GRAYSCALE     0
#define DS_PSEUDO        1
#define DS_FLAME1        2
#define DS_FLAME2        3
#define DS_FLAME3        4
#define DS_FLAME4        5
#define DS_FLAME5        6
#define DS_FLAME6        7
#define DS_FLAME7        8
#define DS_FLAME8        9
#define DS_FLAME9       10

struct DS_RGB {
        int r;
        int g;
        int b;
};

#endif
