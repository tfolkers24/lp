#ifndef ETIME_H
#define ETIME_H

#define DO_DSTIME 1

#define DSTIME_START  1
#define DSTIME_STOP   2

struct DSTIME {
	char *funcName;

	int    indx;
	int    pad;

	int    ecalls;
	int    scalls;

	double stime;
	double etime;

	double ttime;
};

#endif
