#define DEFINE_OTF_STRUCT
#include "ds_extern.h"
#include "ds_proto.h"

static int font0 = FONT_SIZE_SMALL;
static int font1 = FONT_SIZE_NORMAL;

struct HEADER lastHead;
struct DS_IMAGE_INFO ds_image_info;
extern struct DS_IMAGE_INFO *last_image;

extern float scanNum, datamax, datamin, globalStart;
extern int scan, noXTick, noYTick, noPlot, doColor,
           thisIsaRefresh, max_gray_scale, xgr_hardFont,
	   opticalFileNeedDisplay, fbScanDone, aosScanDone;

extern char source[], filename[], gfilename[], mainFrameTitle[], oldBuf[];
extern struct XGR_HEAD xgr_head;

extern struct DS_RGB colormap[];

float globalXmin, globalXmax, globalYmin, globalYmax;
float otfIFNumber;
int chanSelOTF  = 0;
int ephemObj    = 0;
int lastGraph   = 0;
int ok2grid     = 0;
int dontsend    = 0;
int zeroCellCount = 0;
int artificialCells = 0;

struct GRID_ACCUM workArray[WORK_ARRAY_X][WORK_ARRAY_Y];
double *workArrayChans = NULL;

struct MAP_SCANS    *ignore_scans_head=NULL;
struct SCAN_HEADERS *otf_scan_headers=NULL;

int badChanArray[MAX_BAD_CHAN];

struct OTF_GRID *gridHead = NULL;
struct OTF_GRID *gridLast = NULL;

struct SPECDATA sd;

int writeSingleChan = TRUE; /* for azimuth(deg) + total power output */

int otfInit()
{
  do_etimeClear();

  log_msg("otfInit(): Enter");

  otf.initialized        = 0;
  otf.bins               = 10;
  otf.plots              = 5;
  otf.singleChan         = 126;
  otf.arrayNum           = OTFRA;
  otf.singleOtfBin       = 8;
  otf.reqSpecIndx        = -1;
  otf.gridBaseLine       = 1;
  otf.noPlot             = 0;
  otf.newShade           = 1;
  otf.requestCol         = 5;
  otf.requestRow         = 5;
  otf.cols               = 32;
  otf.rows               = 32;
  otf.noint              = 1024;
  otf.int_chans[0]       = 508;
  otf.int_chans[1]       = 516;
  otf.lockScale          = 0;
  otf.valueMaxChanged    = 0;
  otf.intenChanged       = 0;
  otf.loadChoice         = LOAD_PREVIEW;
  otf.lastAction         = NORMAL_MAP;
  otf.noColorBar         = 0;
  otf.manMapRa		 = 0.0;
  otf.manMapDec		 = 0.0;
  otf.manNyquistRa	 = 11.1761475;
  otf.manNyquistDec	 = 8.05853271;
  otf.manMapWidth	 = 0.60;
  otf.manMapHeight	 = 0.60;
  otf.windowSizeSet	 = 0;
  otf.chanCompress       = 1;
  otf.scaleFactor        = 1.0;
  otf.gamma              = 1.0;
  otf.whichPlot          = PLOT_INT_INTENSITY;
  otf.IFNumber           = 0.01;
  otf.chanSelOTF	 = 0;
  otf.nyquistRa          = 22.35;
  otf.nyquistDec         = 22.35;
  otf.valueMax           = 10.0;
  otf.map_scans_head     = NULL;
  otf.gridArray          = NULL;
  otf.gridArrayChans     = NULL;
  otf.plotxb             = OTFPLOTXB;
  otf.plotyb             = OTFPLOTYB;
  otf.plotxe             = OTFPLOTXE;
  otf.plotye             = OTFPLOTYE;
  otf.freqres		 = -1.0;
  otf.restfreq		 = -1.0;
  otf.velocity		 = -1.0;
  otf.xsource		 = -1.0;
  otf.ysource		 = -100.0;
  otf.peakStr[0]         = '\0';
  otf.ximage             = NULL;
  otf.colorChoice        = doColor;
  otf.cutoff		 = 0;
  otf.regionshowbold     = 0;
  otf.baselines[0]       = 1;
  otf.baselines[1]       = 30;
  otf.baselines[2]       = -1;
  otf.baselines[3]       = -1;
  otf.baselines[4]       = -1;
  otf.baselines[5]       = -1;
  otf.baselines[6]       = 224;
  otf.baselines[7]       = 254;

  otfIFNumber = otf.IFNumber;

  log_msg("otfInit(): Done");

  return(0);
}


int freeOtfMemory()
{
  struct OTF_GRID *g;
  struct OTF_GRID *o;

  log_msg("freeOtfMemory(): Enter");

  if(otf.gridArray)
  {
    for(g=gridHead;g;g=o)
    {
      if(g->chans)
      {
        free((char *)g->chans);
      }

      o = g->next;
      free((char *)g);
    }

    free((char *)otf.gridArray);
    otf.gridArray = NULL;
  }

  gridHead = NULL;
  gridLast = NULL;

  if(workArrayChans)
  {
    free((char *)workArrayChans);
    workArrayChans = NULL;
  }

  remMapScans();

  log_msg("freeOtfMemory(): Done");

  return(0);
}


int otfGridInit(cols, rows, noint)
int cols, rows, noint;
{
  int col, row;
  int n, k;

  log_msg("otfGridInit(): Initializing %dx%dx%d Chs", cols, rows, noint);

  otf.reqSpecIndx = -1;

  freeOtfMemory();

  ephemObj     = 0;

  n = cols * rows;
  if((otf.gridArray=(struct OTF_GRID **)malloc((unsigned)(n * sizeof(struct OTF_GRID *)))) == NULL)
  {
    log_msg("otfGridInit(): Error in malloc(%d)", n * sizeof(struct OTF_GRID));

    return(1);
  }

  bzero((char *)otf.gridArray, n*sizeof(struct OTF_GRID *));

  otf.cols          = cols;
  otf.rows          = rows;
  otf.numCellsTotal = cols * rows;
  otf.noint         = noint;

  n = WORK_ARRAY_X * WORK_ARRAY_Y * otf.noint;
  if((workArrayChans=(double *)malloc((unsigned)(n * sizeof(double)))) == NULL)
  {
    log_msg("otfGridInit(): Error in malloc(%d)", n * sizeof(double));

    workArrayChans = NULL;

    return(1);
  }

  otf.sizeofWorkArray = n * sizeof(double);

  bzero((char *)workArrayChans, otf.sizeofWorkArray);

  k = 0;
  for(col=0;col<WORK_ARRAY_X;col++)
  {
    for(row=0;row<WORK_ARRAY_Y;row++,k++)
    {
      workArray[col][row].chans = &workArrayChans[k*otf.noint];
    }
  }

  otf.regionshowbold = 0;
  otf.initialized    = 1;
  otf.windowSizeSet  = 0;

  log_msg("otfGridInit(): Done");

  return(0);
}


struct HEADER *do_addHeader(ver, scan, head)
int ver;
float scan;
struct HEADER *head;
{
  struct HEADER *h;
  static int    old_ver          = 0;
  static double old_scan         = 0.0;
  static struct HEADER *old_head = NULL;

#ifdef DO_DSTIME
  do_etime(DO_ADDHEADER, DSTIME_START);
#endif

   /* If it's the same ver and scan as the last call, return last head */
  if(old_ver == ver && fabs(old_scan-scan) < 0.01)
  {
#ifdef DO_DSTIME
    do_etime(DO_ADDHEADER, DSTIME_STOP);
#endif
    return(old_head);
  }

  if((h = checkScanHeader(ver, scan)))
  {
    old_ver  = ver;
    old_scan = scan;
    old_head = h;

#ifdef DO_DSTIME
    do_etime(DO_ADDHEADER, DSTIME_STOP);
#endif
    return(h);
  }
  else
  {
    if((h = addScanHeader(ver, scan, head)))
    {
      old_ver  = ver;
      old_scan = scan;
      old_head = h;

#ifdef DO_DSTIME
      do_etime(DO_ADDHEADER, DSTIME_STOP);
#endif
      return(h);
    }
    else
    {
      log_msg("do_addHeader(): Error adding scan %.2f, in ver %d", scan, ver);
    }
  }

#ifdef DO_DSTIME
  do_etime(DO_ADDHEADER, DSTIME_STOP);
#endif

  return((struct HEADER *)NULL);
}


struct HEADER *checkScanHeader(v, s)
int v;
float s;
{
#ifdef DO_DSTIME
  do_etime(CHECKSCANHEADER, DSTIME_START);
#endif

  struct SCAN_HEADERS *m;
  float ss;

  if(otf_scan_headers == NULL)
  {
#ifdef DO_DSTIME
    do_etime(CHECKSCANHEADER, DSTIME_STOP);
#endif
    return((struct HEADER *)NULL);
  }

  /* users can pass a scan number like 1234.0, which will ignore all
     scans with that prefix */
  ss = (float)(int)s;

  for(m=otf_scan_headers;m->next; m=m->next) 
  {
    if(m)
    {
      if((m->scan == s || m->scan == ss) && m->ver == v)
      {
#ifdef DO_DSTIME
        do_etime(CHECKSCANHEADER, DSTIME_STOP);
#endif
        return(&m->head);
      }
    }
  }

  /* Check the last one, if valid */
  if(m)
  {
    if((m->scan == s || m->scan == ss) && m->ver == v)
    {
#ifdef DO_DSTIME
      do_etime(CHECKSCANHEADER, DSTIME_STOP);
#endif
      return(&m->head);
    }
  }

#ifdef DO_DSTIME
  do_etime(CHECKSCANHEADER, DSTIME_STOP);
#endif

  return((struct HEADER *)NULL);
}


struct HEADER *addScanHeader(v, s, head)
int v;
float s;
struct HEADER *head;
{
  struct SCAN_HEADERS *m;

#ifdef DO_DSTIME
  do_etime(ADDSCANHEADER, DSTIME_START);
#endif

  /* Create a fresh list */
  if(otf_scan_headers == NULL)
  {
    if((otf_scan_headers=(struct SCAN_HEADERS *)malloc((unsigned)(sizeof(struct SCAN_HEADERS))))==NULL)
    {
      log_msg("Error in addIgnoreScans malloc(%d)", sizeof(struct SCAN_HEADERS));

#ifdef DO_DSTIME
      do_etime(ADDSCANHEADER, DSTIME_STOP);
#endif

      return((struct HEADER *)NULL);
    }

    otf_scan_headers->ver  = v;
    otf_scan_headers->scan = s;

    bcopy((char *)head, (char *)&otf_scan_headers->head, sizeof(struct HEADER));

    otf_scan_headers->next = NULL;

#ifdef DO_DSTIME
    do_etime(ADDSCANHEADER, DSTIME_STOP);
#endif

    return(&otf_scan_headers->head);
  }

  /* Find next available slot */
  for(m=otf_scan_headers;m->next; m=m->next)
  {
    ;
  }

  /* Append new scan */
  if((m->next=(struct SCAN_HEADERS *)malloc((unsigned)(sizeof(struct SCAN_HEADERS))))==NULL)
  {
    log_msg("Error in addIgnoreScans malloc(%d)", sizeof(struct SCAN_HEADERS));

#ifdef DO_DSTIME
    do_etime(ADDSCANHEADER, DSTIME_STOP);
#endif

    return((struct HEADER *)NULL);
  }

  m->next->ver  = v;
  m->next->scan = s;

  bcopy((char *)head, (char *)&m->next->head, sizeof(struct HEADER));

  m->next->next = NULL;

#ifdef DO_DSTIME
  do_etime(ADDSCANHEADER, DSTIME_STOP);
#endif

  return(&m->next->head);
}


int remScanHeaders()
{
  struct SCAN_HEADERS *p;
  struct SCAN_HEADERS *q;

#ifdef DO_DSTIME
  do_etime(REMSCANHEADERS, DSTIME_START);
#endif

  for(p=otf_scan_headers; p; p=q)
  {
    q = p->next;

    if(p)
    {
      free((char *)p);
    }
  }

  otf_scan_headers = NULL;

#ifdef DO_DSTIME
  do_etime(REMSCANHEADERS, DSTIME_STOP);
#endif
 
  return(0);
}
/* TOMMIE */

int do_ignoreScan(buf)
char *buf;
{
  int n, v;
  float f;

#ifdef DO_DSTIME
  do_etime(DO_IGNORESCAN, DSTIME_START);
#endif

  if(buf)
  {
    n = sscanf(buf, "%d %f", &v, &f);

    if(n == 2)
    {
      if((f > 0.0 && f < 49999.9) && (v > 0 && v < 999))
      {
        if(!checkIgnoreScans(v, f))
        {
          if(!addIgnoreScans(v, f))
	  {
            log_msg("Scan %.2f, in version %d ignored", f, v);
	  }
	  else
	  {
            log_msg("Scan %.2f, in version %d, could NOT be ignored", f, v);
	  }
        }
        else
        {
          log_msg("Scan %.2f already ignored in version %d", f, v);
        }
      }
      else
      {
	log_msg("Scan %f or version: %d, out of range", f, v);
#ifdef DO_DSTIME
        do_etime(DO_IGNORESCAN, DSTIME_STOP);
#endif
        return(1);
      }
    }
    else
    {
      log_msg("do_ignoreScan(): Missing args for ignore: %d", n);
#ifdef DO_DSTIME
      do_etime(DO_IGNORESCAN, DSTIME_STOP);
#endif
      return(1);
    }
  }

#ifdef DO_DSTIME
  do_etime(DO_IGNORESCAN, DSTIME_STOP);
#endif

  return(0);
}


int checkIgnoreScans(v, s)
int v;
float s;
{
  struct MAP_SCANS *m;
  float ss;

#ifdef DO_DSTIME
  do_etime(CHECKIGNORESCANS, DSTIME_START);
#endif

  if(ignore_scans_head == NULL)
  {
#ifdef DO_DSTIME
    do_etime(CHECKIGNORESCANS, DSTIME_STOP);
#endif
    return(0);
  }

  /* users can pass a scan number like 1234.0, which will ifnore all
     scans with that prefix */
  ss = (float)(int)s;

  for(m=ignore_scans_head;m->next; m=m->next) 
  {
    if(m)
    {
      if((m->scan == s || m->scan == ss) && m->ver == v)
      {
#ifdef DO_DSTIME
        do_etime(CHECKIGNORESCANS, DSTIME_STOP);
#endif
        return(1);
      }
    }
  }

  if(m)
  {
    if((m->scan == s || m->scan == ss) && m->ver == v)
    {
#ifdef DO_DSTIME
      do_etime(CHECKIGNORESCANS, DSTIME_STOP);
#endif
      return(1);
    }
  }

#ifdef DO_DSTIME
  do_etime(CHECKIGNORESCANS, DSTIME_STOP);
#endif

  return(0);
}


int addIgnoreScans(v, s)
int v;
float s;
{
  struct MAP_SCANS *m;

#ifdef DO_DSTIME
  do_etime(ADDIGNORESCANS, DSTIME_START);
#endif

  /* Create a fresh list */
  if(ignore_scans_head == NULL)
  {
    if((ignore_scans_head=(struct MAP_SCANS *)malloc((unsigned)(sizeof(struct MAP_SCANS))))==NULL)
    {
      log_msg("Error in addIgnoreScans malloc(%d)", sizeof(struct MAP_SCANS));

#ifdef DO_DSTIME
      do_etime(ADDIGNORESCANS, DSTIME_STOP);
#endif

      return(1);
    }

    ignore_scans_head->ver  = v;
    ignore_scans_head->scan = s;
    ignore_scans_head->next = NULL;

#ifdef DO_DSTIME
    do_etime(ADDIGNORESCANS, DSTIME_STOP);
#endif

    return(0);
  }

  /* Find next available slot */
  for(m=ignore_scans_head;m->next; m=m->next)
  {
    ;
  }

  /* Append new scan */
  if((m->next=(struct MAP_SCANS *)malloc((unsigned)(sizeof(struct MAP_SCANS))))==NULL)
  {
    log_msg("Error in addIgnoreScans malloc(%d)", sizeof(struct MAP_SCANS));

#ifdef DO_DSTIME
    do_etime(ADDIGNORESCANS, DSTIME_STOP);
#endif

    return(1);
  }

  m->next->ver  = v;
  m->next->scan = s;
  m->next->next = NULL;

#ifdef DO_DSTIME
  do_etime(ADDIGNORESCANS, DSTIME_STOP);
#endif

  return(0);
}


int remIgnoreScans()
{
  struct MAP_SCANS *p;
  struct MAP_SCANS *q;

#ifdef DO_DSTIME
  do_etime(REMIGNORESCANS, DSTIME_START);
#endif

  for(p=ignore_scans_head; p; p=q)
  {
    q = p->next;

    if(p)
    {
      free((char *)p);
    }
  }

  ignore_scans_head = NULL;

#ifdef DO_DSTIME
  do_etime(REMIGNORESCANS, DSTIME_STOP);
#endif
 
  return(0);
}


int do_saveIgnoreScans(fp)
FILE *fp;
{
  struct MAP_SCANS *m;
  int i=0;

#ifdef DO_DSTIME
  do_etime(DO_SAVEIGNORESCANS, DSTIME_START);
#endif

  if(ignore_scans_head == NULL)
  {
#ifdef DO_DSTIME
    do_etime(DO_SAVEIGNORESCANS, DSTIME_STOP);
#endif
    return(0);
  }

  for(m=ignore_scans_head;m->next; m=m->next, i++)
  {
    if(m)
    {
      fprintf(fp, "ignore %d %7.2f\n", m->ver, m->scan);
    }
  }

  if(m)
  {
    fprintf(fp, "ignore %d %7.2f\n", m->ver, m->scan);
  }

#ifdef DO_DSTIME
  do_etime(DO_SAVEIGNORESCANS, DSTIME_STOP);
#endif

  return(0);
}



int do_printIgnoreScans()
{
  struct MAP_SCANS *m;
  int i=0;

#ifdef DO_DSTIME
  do_etime(DO_PRINTIGNORESCANS, DSTIME_START);
#endif

  if(ignore_scans_head == NULL)
  {
    log_msg("None Ignored");

#ifdef DO_DSTIME
    do_etime(DO_PRINTIGNORESCANS, DSTIME_STOP);
#endif

    return(0);
  }

  for(m=ignore_scans_head;m->next; m=m->next, i++)
  {
    if(m)
    {
      log_msg("Ignoring scan %3d = %7.2f, in Version: %d", i, m->scan, m->ver);
    }
  }

  if(m)
  {
    log_msg("Ignoring scan %3d = %7.2f, in Version: %d", i, m->scan, m->ver);
  }

#ifdef DO_DSTIME
  do_etime(DO_PRINTIGNORESCANS, DSTIME_STOP);
#endif

  return(0);
}



int do_clearIgnore()
{
#ifdef DO_DSTIME
  do_etime(DO_CLEARIGNORE, DSTIME_START);
#endif

  remIgnoreScans();

#ifdef DO_DSTIME
  do_etime(DO_CLEARIGNORE, DSTIME_STOP);
#endif
 
  return(0);
}


int checkMapScans(v, s)
int v;
float s;
{
  struct MAP_SCANS *m;

#ifdef DO_DSTIME
  do_etime(CHECKMAPSCANS, DSTIME_START);
#endif

  if(otf.map_scans_head == NULL)
  {
#ifdef DO_DSTIME
    do_etime(CHECKMAPSCANS, DSTIME_STOP);
#endif
    return(0);
  }

  for(m=otf.map_scans_head;m->next; m=m->next) 
  {
    if(m)
    {
      if(m->scan == s && m->ver == v)
      {
#ifdef DO_DSTIME
        do_etime(CHECKMAPSCANS, DSTIME_STOP);
#endif
        return(1);
      }
    }
  }

  if(m)
  {
    if(m->scan == s && m->ver == v)
    {
#ifdef DO_DSTIME
      do_etime(CHECKMAPSCANS, DSTIME_STOP);
#endif
      return(1);
    }
  }

#ifdef DO_DSTIME
  do_etime(CHECKMAPSCANS, DSTIME_STOP);
#endif

  return(0);
}


int addMapScans(v, s)
int v;
float s;
{
  struct MAP_SCANS *m;

#ifdef DO_DSTIME
  do_etime(ADDMAPSCANS, DSTIME_START);
#endif

  if(otf.map_scans_head == NULL)
  {
    if((otf.map_scans_head=(struct MAP_SCANS *)malloc((unsigned)(sizeof(struct MAP_SCANS))))==NULL)
    {
      log_msg("Error of addMapScans addMapScans(malloc(%d))", sizeof(struct MAP_SCANS));

#ifdef DO_DSTIME
      do_etime(ADDMAPSCANS, DSTIME_STOP);
#endif

      return(1);
    }

    otf.map_scans_head->ver  = v;
    otf.map_scans_head->scan = s;
    otf.map_scans_head->next = NULL;

#ifdef DO_DSTIME
    do_etime(ADDMAPSCANS, DSTIME_STOP);
#endif

    return(0);
  }

  for(m=otf.map_scans_head;m->next; m=m->next)
  {
    ;
  }

  if((m->next=(struct MAP_SCANS *)malloc((unsigned)(sizeof(struct MAP_SCANS))))==NULL)
  {
    log_msg("Error of addMapScans addMapScans(malloc(%d))", sizeof(struct MAP_SCANS));

#ifdef DO_DSTIME
    do_etime(ADDMAPSCANS, DSTIME_STOP);
#endif

    return(1);
  }

  m->next->ver  = v;
  m->next->scan = s;
  m->next->next = NULL;

#ifdef DO_DSTIME
  do_etime(ADDMAPSCANS, DSTIME_STOP);
#endif

  return(0);
}



int remMapScans()
{
  struct MAP_SCANS *p;
  struct MAP_SCANS *q;

#ifdef DO_DSTIME
  do_etime(REMMAPSCANS, DSTIME_START);
#endif

  for(p=otf.map_scans_head; p; p=q)
  {
    q = p->next;

    if(p)
    {
      free((char *)p);
    }
  }

  otf.map_scans_head = NULL;

#ifdef DO_DSTIME
  do_etime(REMMAPSCANS, DSTIME_STOP);
#endif
 
  return(0);
}


int do_printMapScans()
{
  struct MAP_SCANS *m;
  int i=0;

#ifdef DO_DSTIME
  do_etime(DO_PRINTMAPSCANS, DSTIME_START);
#endif

  if(otf.map_scans_head == NULL)
  {
    log_msg("No Scans Loaded");

#ifdef DO_DSTIME
    do_etime(DO_PRINTMAPSCANS, DSTIME_STOP);
#endif

    return(0);
  }

  for(m=otf.map_scans_head;m->next; m=m->next, i++)
  {
    if(m)
    {
      log_msg("scan %3d = %7.2f, in version: %d", i, m->scan, m->ver);
    }
  }

  if(m)
  {
    log_msg("scan %3d = %7.2f, in version: %d", i, m->scan, m->ver);
  }

#ifdef DO_DSTIME
  do_etime(DO_PRINTMAPSCANS, DSTIME_STOP);
#endif

  return(0);
}



int getNumSpectra(datalen, noint)
float datalen, noint;
{
  int dl;

#ifdef DO_DSTIME
  do_etime(GETNUMSPECTRA, DSTIME_START);
#endif

  dl = (int)datalen / ( sizeof(float) * ( (int)noint +5));

#ifdef DO_DSTIME
  do_etime(GETNUMSPECTRA, DSTIME_STOP);
#endif

  return(dl);
}



int resendSpecData(sd)
struct SPECDATA *sd;
{
  char sysBuf[256];
  static time_t oldTime=0;
  time_t t;

  t = time(NULL);

  if(abs(t-oldTime) > 5)
  {
    fprintf(stdout, "Starting specdata viewer\n");
    fflush(stdout);

    log_msg("sock_write() returned Error; starting specdata viewer");

    sprintf(sysBuf, "specdata &");

    system(sysBuf);

//    usleep(250000);

    /* resend */
//    ret = sock_write(specdata, (char *)sd, sizeof(*sd));

//    if(!ret)
//    {
//      fprintf(stdout, "sock_write() Still returning Error; Check Instalation\n");
//      fflush(stdout);

//      log_msg("sock_write() Still returning Error; Check Installation");

//    }
    
    oldTime = t;
  }

  return(0);
}



/*  --------------- entry point of otf ------------------ */

int do_otf( data, head, offdata, gainsdata)
float *data, *offdata, *gainsdata;
struct HEADER *head;
{
  int  ret;
  extern int sockWriteNoBitch;			/* in ../cactus/lib/socknoxvlib.c */

#ifdef DO_DSTIME
  do_etime(DO_OTF, DSTIME_START);
#endif

  bcopy((char *)head, (char *)&lastHead, sizeof(struct HEADER));

  otf.head       = &lastHead;
  otf.ondata     = data;
  otf.offdata    = offdata;
  otf.gainsdata  = gainsdata;
  otf.numSpectra = getNumSpectra(otf.head->datalen, otf.head->noint);

  if(otf.head->noint > MAX_DATA_CHANNELS)
  {
    ok2grid = 0;
    otf.noint = (int)otf.head->noint;
    log_msg("DATASERV: Can not grid %.0f chans", otf.head->noint);
  }
  else
  {
    ok2grid = 1;

    if(!fillOtfGrid())
    {
      paintOtfGrid();
    }
  }

  bzero((char *)&sd, sizeof(sd));

  strcpy(sd.datafile, filename);
  strcpy(sd.gainsfile, gfilename);
    
  sd.scanno       = (int)head->scan;
  sd.feed         = (int)round((otf.IFNumber*100.0));
  sd.singleChan   = otf.singleChan;
  sd.singleOtfBin = otf.singleOtfBin;
  sd.debugSpectra = 0;
  sd.doTPprocess  = 1;
  sd.arrayNum     = 2;

  sockWriteNoBitch = 1; /* In case it hasn't started yet, turn off sock_write(): libary bitching */
  ret = sock_write(specdata, (char *)&sd, sizeof(sd));
  sockWriteNoBitch = 0;

  if(!ret) 		/* Process specplot, not running or died; start it */
  {

    resendSpecData(&sd);
  }

#ifdef DO_DSTIME
  do_etime(DO_OTF, DSTIME_STOP);
#endif

  return(0);
}



int do_otfRegular()
{
#ifdef DO_DSTIME
  do_etime(DO_OTFREGULAR, DSTIME_START);
#endif

  otf.newShade    = 1;
  otf.whichPlot   = PLOT_INT_INTENSITY;

  otf.valueMaxChanged = 1;
  otf.intenChanged    = 1;

  if(otf.head == NULL)
  {
#ifdef DO_DSTIME
    do_etime(DO_OTFREGULAR, DSTIME_STOP);
#endif
    return(1);
  }

  paintOtfGrid();
  displayRequestedOtf();

#ifdef DO_DSTIME
  do_etime(DO_OTFREGULAR, DSTIME_STOP);
#endif

  return(0);
}


int doOtfGridRePaint()
{
#ifdef DO_DSTIME
  do_etime(DOOTFGRIDREPAINT, DSTIME_START);
#endif

  if(debug)
  {
    log_msg("doOtfGridRePaint()");
  }

  otf.newShade = 1;

  if(otf.lastAction == INTERP)
  {
    if(debug)
    {
      log_msg("doOtfGridRePaint(): lastAction =  INTERP");
    }

    otfInterp();
  }
  else
  if(otf.lastAction == NORMAL_MAP)
  {
    if(debug)
    {
      log_msg("doOtfGridRePaint(): lastAction =  NORMAL_MAP");
    }

    paintOtfGrid();
  }
  else
  if(otf.lastAction == CHANNEL_MAP)
  {
    if(debug)
    {
      log_msg("doOtfGridRePaint(): lastAction =  CHANNEL_MAP");
    }

    do_otfChannelMap(NULL);
  }

  if(lastGraph == OTF_GRAPH)
  {
    if(debug)
    {
      log_msg("doOtfGridRePaint(): LastGraph =  OTF_GRAPH");
    }

    if(otf.lastAction == INTERP)
    {
      paintOtfGrid();
      otfInterp();
    }
    else
    if(otf.lastAction == NORMAL_MAP)
    {
      paintOtfGrid();
    }
    else
    if(otf.lastAction == CHANNEL_MAP)
    {
      do_otfChannelMap(NULL);
    }
  }
  else
  {
    if(debug)
    {
      log_msg("doOtfGridRePaint(): LastGraph =  Unknown");
    }
  }

#ifdef DO_DSTIME
  do_etime(DOOTFGRIDREPAINT, DSTIME_STOP);
#endif

  return(0);
}


int doOtfGridReSize()
{
#ifdef DO_DSTIME
  do_etime(DOOTFGRIDRESIZE, DSTIME_START);
#endif

  if(debug)
  {
    log_msg("doOtfGridReSize()");
  }

  otf.newShade = 1;

#ifdef DO_DSTIME
  do_etime(DOOTFGRIDRESIZE, DSTIME_STOP);
#endif

  return(0);
}



int do_otfRmBadChan()
{
  int i, bc, noint;
  struct OTF_GRID *g;

#ifdef DO_DSTIME
  do_etime(DO_OTFRMBADCHAN, DSTIME_START);
#endif

  if(!otf.gridArray || !otf.head)
  {
    log_msg("do_otfRmBadChan(): OTF Array or header is empty");
#ifdef DO_DSTIME
    do_etime(DO_OTFRMBADCHAN, DSTIME_STOP);
#endif
    return(1);
  }

  noint = otf.noint;

  for(i=0;i<MAX_BAD_CHAN;i++)
  {
    if((bc = badChanArray[i]))
    {
      bc--;

      if(bc > noint || bc < 0)	      /* if I have still screwed up, contiune */
      {
	continue;
      }

      log_msg("do_otfRmBadChan(): Removing Channel %4d from Map", bc);

      for(g=gridHead;g;g=g->next)
      {
	if(g->set == 1) /* Only work on valid celles */
	{
          if(bc == 0)
          {
            g->chans[bc] = g->chans[bc+1];
          }
          else
          if(bc == noint)
          {
            g->chans[bc] = g->chans[bc-1];
          }
          else
          {
            g->chans[bc] = (g->chans[bc-1] + g->chans[bc+1]) / 2.0;
          }

          g->bdirty = 1;
	  g->ddirty = 1;

          otf.bdirty = 1;
          otf.ddirty = 1;
	}
      }
    }
  }

  otf.valueMaxChanged = 1;
  otf.intenChanged    = 1;
  otf.newShade        = 1;

  log_msg("do_otfRmBadChan(): Recomputing Baselines");

  recomputeBaseLines();

  paintOtfGrid();
  displayRequestedOtf();

  log_msg("do_otfRmBadChan(): Done");

#ifdef DO_DSTIME
  do_etime(DO_OTFRMBADCHAN, DSTIME_STOP);
#endif

  return(0);
}


  /* Zero out any data points larger that 'max' */
int spikeCheck(buf)
char *buf;
{
  int i, noint, nspec, nspike;
  float max, val;
  struct OTF_GRID *g;

#ifdef DO_DSTIME
  do_etime(SPIKECHECK, DSTIME_START);
#endif

  if(!otf.gridArray || !otf.head)
  {
    log_msg("spikeCheck(): OTF Array or header is empty");
#ifdef DO_DSTIME
    do_etime(SPIKECHECK, DSTIME_STOP);
#endif
    return(1);
  }

  if(buf)
  {
    max = atof(buf);
  }
  else
  {
    log_msg("Missing arg for despike");

#ifdef DO_DSTIME
    do_etime(SPIKECHECK, DSTIME_STOP);
#endif

    return(1);
  }

  if(max > 0.0)
  {
    noint = otf.noint;

    nspec = 0;
    for(g=gridHead;g;g=g->next)
    {
      if(g->set == 1) /* Only work on valid cells */
      {
        nspike = 0;
        for(i=0;i<noint;i++)
        {
          if(otf.gridBaseLine)
          {
	    val = g->chans[i] - ((float)i * g->a + g->b);
          }
          else
          {
	    val = g->chans[i];
          }

          if(fabs(val) > max)
          {
            g->chans[i] = 0.0;

	    nspike++;
          }
        }

        if(nspike)
        {
          log_msg("Removed %d spikes in scan %.2f - %.2f", nspike, g->bscan, g->escan);

          nspec++;
        }
      }
    }

    recomputeBaseLines();
    forceDrawGrid();

    log_msg("Corrected %d spectra", nspec);
  }

#ifdef DO_DSTIME
  do_etime(SPIKECHECK, DSTIME_STOP);
#endif

  return(0);
}

int computeInten(bc, ec)
int bc, ec;
{
  int j, k, count=0, tcount=0;
  double accum, intInten, intInten2;
  struct OTF_GRID *g;

#ifdef DO_DSTIME
  do_etime(COMPUTEINTEN, DSTIME_START);
#endif

  if(!otf.gridArray || !otf.head)
  {
    log_msg("COMPUTEINTEN: OTF Array or header is empty");
#ifdef DO_DSTIME
    do_etime(COMPUTEINTEN, DSTIME_STOP);
#endif
    return(1);
  }

  if(bc < 0 || bc > otf.noint-1 || ec > otf.noint-1)
  {
    log_msg("COMPUTEINTEN: bc, %d; or ec, %d; greater then nochan, %d", bc, ec, otf.noint);
#ifdef DO_DSTIME
    do_etime(COMPUTEINTEN, DSTIME_STOP);
#endif
    return(1);
  }

  log_msg("computeInten(): Started");

			/* now compute area under curve for selected chans */
  for(g=gridHead;g;g=g->next)
  {
    if(g->set == 1)
    {
      tcount++;

      if(g->ddirty) /* Only work on valid celles */
      {
        if(otf.whichPlot != PLOT_TSYS && otf.whichPlot != PLOT_RMS) /* Don't waste time */
        {
          k     = 0;
          accum = 0.0;
          j     = bc;

          do
          {
            if(otf.gridBaseLine)
            {
              accum += g->chans[j] - ((float)j * g->a + g->b);
            }
            else
            {
              accum += g->chans[j];
            }
  
            k++;
          } while(j++<ec);
        }

        g->kms = (float)(accum * fabs(otf.head->deltax));
        intInten = (float)(accum / (double)k) * otf.scaleFactor;

		/* Apply Gamma correction if cell > 0.0 */
	if(intInten > 0.0)
        {
          intInten2 = (double)MAX_GRAY_SCALE * pow((intInten / (double)MAX_GRAY_SCALE), otf.gamma);

	  if(isnan(intInten2))
          {
	    log_msg("Col %d, Row %d: intInten = %f using Gamma of %f", g->col, g->row, intInten, otf.gamma);
            g->intInten = intInten;
          }
          else
          {
            g->intInten = intInten2;
          }
	}
	else
	{
          g->intInten = intInten;
	}

        g->ddirty = 0;
        count++;
      }

    /* Assign & Apply scaling factor */

      switch(otf.whichPlot)
      {
	case PLOT_INT_INTENSITY:
	  g->plotVal1 = g->intInten;
	  g->plotVal2 = g->kms;

	  break;

	case PLOT_RMS:
          g->plotVal1 = g->rms;
          g->plotVal2 = g->rms;

	  break;

	case PLOT_TSYS:
          g->plotVal1 = g->tsys;
          g->plotVal2 = g->tsys;

	  break;

	case PLOT_TIME:
          g->plotVal1 = g->time;
          g->plotVal2 = g->time;

	  break;
      }

      if(isnan(g->plotVal1))
      {
        g->plotVal1 = 0.0;
      }
    }
  }

  log_msg("computeInten(): Computed %d out of %d populated cells", count, tcount);

#ifdef DO_DSTIME
  do_etime(COMPUTEINTEN, DSTIME_STOP);
#endif

  return(0);
}


int computeMinMax()
{
  struct OTF_GRID *g;

#ifdef DO_DSTIME
  do_etime(COMPUTEMINMAX, DSTIME_START);
#endif

  if(!otf.gridArray)
  {
    log_msg("COMPUTEMINMAX: OTF Array is empty");
#ifdef DO_DSTIME
    do_etime(COMPUTEMINMAX, DSTIME_STOP);
#endif

    return(1);
  }

  if(!otf.valueMaxChanged)
  {
#ifdef DO_DSTIME
    do_etime(COMPUTEMINMAX, DSTIME_STOP);
#endif
    return(0);
  }

  if(otf.lockScale)
  {
#ifdef DO_DSTIME
    do_etime(COMPUTEMINMAX, DSTIME_STOP);
#endif
    return(0);
  }

  if(debug)
  {
    log_msg("COMPUTEMINMAX: Recomputing OTF Cell Min & Max Values");
  }

  otf.valueMin =  999999999.0;
  otf.valueMax = -999999999.0;

  otf.numCellsUsed = 0;
  zeroCellCount    = 0;
  artificialCells  = 0;

  for(g=gridHead;g;g=g->next)
  {
    if(g->set == 1) /* Only work on valid celles */
    {
      if(g->col == 0 && g->row == 0) 	/* How many cells think there are 0, 0 */
      {
        zeroCellCount++;
      }

      if(g->artificial)
      {
	artificialCells++;
      }

      if(g->plotVal1 > otf.valueMax)
      {
        otf.valueMax = g->plotVal1;

        otf.valueMaxX = g->col;
        otf.valueMaxY = g->row;
      }

      if(g->plotVal1 < otf.valueMin)
      {
        otf.valueMin = g->plotVal1;

        otf.valueMinX = g->col;
        otf.valueMinY = g->row;
      }

      otf.numCellsUsed++;
    }
  }

  otf.valueMaxChanged = 0;

#ifdef DO_DSTIME
  do_etime(COMPUTEMINMAX, DSTIME_STOP);
#endif

  return(0);
}



int computeShade()
{
  struct OTF_GRID *g;
  float d;

#ifdef DO_DSTIME
  do_etime(COMPUTESHADE, DSTIME_START);
#endif

  if(!otf.gridArray)
  {
    log_msg("COMPUTESHADE: OTF Array is empty");
#ifdef DO_DSTIME
    do_etime(COMPUTESHADE, DSTIME_STOP);
#endif
    return(1);
  }

  if(!otf.newShade)
  {
#ifdef DO_DSTIME
    do_etime(COMPUTESHADE, DSTIME_STOP);
#endif
    return(0);
  }

  if(debug)
  {
    log_msg("COMPUTESHADE:  Recomputing OTF Cell Color Shades");
  }

  d = (float)max_gray_scale / otf.valueMax * otf.scaleFactor;

  for(g=gridHead;g;g=g->next)
  {
    if(g->set == 1) /* Only work on valid celles */
    {
      if(otf.valueMax == 0.0)
      {
        g->shade = 1;
      }
      else
      {
        g->shade = (int)((g->plotVal1) * d);
      }

      if(g->shade > max_gray_scale-1)
      {
        g->shade = max_gray_scale-1;
      }

      if(g->shade < 1)
      {
        g->shade = 1;
      }

      if(g->plotVal1 == otf.valueMax)
      {
        if(otf.whichPlot == PLOT_TSYS)
        {
          sprintf(otf.peakStr, "  Tsys    %8.4f", g->plotVal1);
        }
        else
        if(otf.whichPlot == PLOT_RMS)
        {
          sprintf(otf.peakStr, "   rms    %8.4f", g->plotVal1);
        }
        else
        if(otf.whichPlot == PLOT_TIME)
        {
          sprintf(otf.peakStr, "  time    %8.4f", g->plotVal1);
        }
        else
        {
          sprintf(otf.peakStr, "Intensity %8.4f", g->plotVal1);
        }
      }
    }
  }

#ifdef DO_DSTIME
  do_etime(COMPUTESHADE, DSTIME_STOP);
#endif

  return(0);
}



int scaleData(data)
float *data;
{
  int i;

#ifdef DO_DSTIME
  do_etime(SCALEDATA, DSTIME_START);
#endif

  for(i=0;i<otf.numSpectra;i++)
  {
    data[i] *= 3600.0;
  }

#ifdef DO_DSTIME
  do_etime(SCALEDATA, DSTIME_STOP);
#endif
 
  return(0);
}




int subtractPredictedRA(data)
float *data;
{
  float xmin, xscale;
  int i;

#ifdef DO_DSTIME
  do_etime(SUBTRACTPREDICTEDRA, DSTIME_START);
#endif
 
  xscale = otf.head->inttime * otf.head->deltaxr / 3600.0;
  xmin   = xscale / -2.0;
  xscale = xscale / (float)otf.numSpectra;

  if(data[0] > 0)
  {
    for(i=0;i<otf.numSpectra;i++)
    {
      data[i] += (xmin + (xscale * (float)i));
      data[i] *= 3600.0;
    }
  }
  else
  {
    for(i=0;i<otf.numSpectra;i++)
    {
      data[i] -= (xmin + (xscale * (float)i));
      data[i] *= 3600.0;
    }
  }

#ifdef DO_DSTIME
  do_etime(SUBTRACTPREDICTEDRA, DSTIME_STOP);
#endif
 
  return(0);
}



int placeBeamSize()
{
  int color, size;
  float deltax, deltay, xstart, ystart, bsize;

#ifdef DO_DSTIME
  do_etime(PLACEBEAMSIZE, DSTIME_START);
#endif

  if(otf.beamRa == 0.0)
  {
    otf.beamRa  = otf.manMapRa;
    otf.beamDec = otf.manMapDec;
  }

  if(otf.placeBeamSize == 1)		/* Radio beam size in arc-seconds */
  {
    bsize = otf.head->bfwhm;

    color = DS_WHITE;
    size  = 1;
  }
  else
  if(otf.placeBeamSize == 2)	/* display the Moon's size in arc-seconds */
  {
    bsize = 32.0 * 60.0;

    color = DS_MAGENTA;
    size  = 2;
  }
  else
  {
    return(0);
  }

  deltax = bsize / 3600.0 / 15.0;
  deltay = bsize / 3600.0;

  xstart = otf.beamRa / 15.0 + deltax;
  ystart = otf.beamDec       + deltay;

  log_msg("placeBeamSize(): xstart = %f, ystart = %f, deltax = %f, deltay = %f", xstart, ystart, deltax, deltay);

  xgr_drawCircle(globalXmax, globalXmin, 
                 globalYmin, globalYmax, 
                 xstart,     ystart, 
                 xstart+deltax*2.0, ystart-deltay*2.0, 
                 color, size);

#ifdef DO_DSTIME
  do_etime(PLACEBEAMSIZE, DSTIME_STOP);
#endif

  return(0);
}


int initGrid()
{
  int ret;

  do_etimeClear();

#ifdef DO_DSTIME
  do_etime(INITGRID, DSTIME_START);
#endif

  if((ret = otfGridInit(otf.cols, otf.rows, otf.noint)))
  {
    if(debug)
    {
      log_msg("initGrid(): otfGridInit() returned %d", ret);
    }

#ifdef DO_DSTIME
    do_etime(INITGRID, DSTIME_STOP);
#endif

    return(1);
  }

#ifdef DO_DSTIME
  do_etime(INITGRID, DSTIME_STOP);
#endif

  return(0);
}


int paintOtfGrid()
{
  static int first=1;
  float xl, xr, yt, yb;
  float xmin, xmax, ymin, ymax, kmin, kmax;
  int   row, col;
  float deltax, deltay;
  float xscale, yscale;
  struct OTF_GRID *f;
  char tbuf[256];

  if(otf.ignoreAllRepaints)
  {
    if(debug)
    {
      log_msg("paintOtfGrid(): ignoreAllRepaints set: Ignoring ForceDrawGrid");
    }

    return(0);
  }

#ifdef DO_DSTIME
  do_etime(PAINTOTFGRID, DSTIME_START);
#endif

  log_msg("paintOtfGrid(): Enter");

  if(first)
  {
    log_msg("paintOtfGrid(): Resizing Gridder Window");

    first = 0;
  }

  if(debug)
  {
    log_msg(" ");
    log_msg("paintOtfGrid()");
  }

  if(!otf.head)
  {
    if(debug)
    {
      log_msg("paintOtfGrid(): No otf.head; returning");
    }

#ifdef DO_DSTIME
    do_etime(PAINTOTFGRID, DSTIME_STOP);
#endif

    return(1);
  }

  if(debug)
  {
    log_msg("paintOtfGrid(): otf.initialized = %d; ok2grid = %d", otf.initialized, ok2grid);
  }

  if(!otf.initialized)
  {
    initGrid();
  }

  lastGraph = OTF_GRAPH;

  if(otf.noPlot)
  {
    log_msg("paintOtfGrid(): Done");
#ifdef DO_DSTIME
    do_etime(PAINTOTFGRID, DSTIME_STOP);
#endif
    return(0);
  }

  if(!thisIsaRefresh)
  {
    setInValid(1);
  }

  xgr_setCurWindow(XGR_OTF_WIN, XGR_SET_PIXMAP);

  if(otf.head && !strncmp(otf.head->obsmode, "LINEOTF ", 8))
  {
    xgr_mapUnmapWindow("OTF Gridder", 1);
  }

  if(otf.newShade)
  {
    xgr_clearScreen();
  }

  xscale = otf.nyquistRa / 3600.0 * otf.cols; 
  xmin = xscale / -2.0  - otf.nyquistRa / 7200.0;
  xmax = xscale /  2.0  - otf.nyquistRa / 7200.0;
  xmin = xmin   / (15.0 * cos(otf.ysource * CTR));
  xmax = xmax   / (15.0 * cos(otf.ysource * CTR));

  yscale = (otf.nyquistDec * otf.rows) / 3600.0;
  ymin = yscale / -2.0;
  ymax = yscale /  2.0;

  globalXmin = xmin + otf.xsource / 15.0;
  globalXmax = xmax + otf.xsource / 15.0;
  globalYmin = ymin + otf.ysource;
  globalYmax = ymax + otf.ysource;

  otf.plotxb = OTFPLOTXB;
  otf.plotxe = OTFPLOTXE;
  otf.plotyb = OTFPLOTYB;
  otf.plotye = OTFPLOTYE;

  deltax = (xmax - xmin) / (float)otf.cols;
  deltay = (ymax - ymin) / (float)otf.rows;

  xgr_drawBox( otf.plotxb, otf.plotyb, otf.plotxe, otf.plotye, DS_GRAY75);

  computeInten(otf.int_chans[0], otf.int_chans[1]);

  computeMinMax();
  computeShade();

  kmin =  999999999.0;
  kmax = -999999999.0;

  if(debug)
  {
    log_msg("PAINTGRID:     Regenerating OTF Plot");
  }

  if(otf.gridArray)
  {
    for(row=0;row<otf.rows;row++)
    {
      for(col=0;col<otf.cols;col++)
      {
        f = fetchMapCell(row, col);

        if(!f)
	{
          continue;
	}

        if(f->set == -1) /* Used but ignored */
	{
          continue;
	}

        xl = xmin+(float)col     * deltax;
        xr = xmin+(float)(col+1) * deltax;
        yt = ymin+(float)row     * deltay;
        yb = ymin+(float)(row+1) * deltay;

        if(otf.newShade || f->shade != f->oldshade)
        {
	  if(f->shade > (otf.cutoff * max_gray_scale / 100))
          {
            xgr_drawFillBox( xmin, xmax, ymin, ymax, xl, yt, xr, yb, f->shade * -1);
          }
	  else
          {
            xgr_drawFillBox( xmin, xmax, ymin, ymax, xl, yt, xr, yb, -1); /* make it the lowet shade */
          }

          f->oldshade = f->shade;
        }

        if(f->plotVal2 > kmax)
	{
          kmax = f->plotVal2;
	}

        if(f->plotVal2 < kmin)
	{
          kmin = f->plotVal2;
	}
      }
    }

    otf.newShade = 0;
    xgr_drawBox( otf.plotxb, otf.plotyb, otf.plotxe, otf.plotye, DS_GRAY75);

    putOtfGrid();

    xgr_drawTextClear(0.01, 0.010, otf.peakStr, DS_YELLOW, font0);

    drawColorBar();

    xgr_drawGrid(kmin/otf.scaleFactor, kmax/otf.scaleFactor, ymin, ymax, 
			max_gray_scale, 1, 6, 1, -1, 1,
			DS_GRAY50, DS_YELLOW, XGR_PLOT_DEGREES);

    if(otf.whichPlot == PLOT_TSYS)
    {
      xgr_drawTextClear(0.5, 0.010, " Tsys", DS_YELLOW, font0);
    }
    else
    if(otf.whichPlot == PLOT_RMS)
    {
      xgr_drawTextClear(0.5, 0.010, " rms ", DS_YELLOW, font0);
    }
    else
    if(otf.whichPlot == PLOT_TIME)
    {
      xgr_drawTextClear(0.5, 0.010, " time", DS_YELLOW, font0);
    }
    else
    {
      xgr_drawTextClear(0.5, 0.010, " K * km/s", DS_YELLOW, font0);
    }

    sprintf(tbuf, "Scale: %6.2f", otf.scaleFactor);
    xgr_drawTextClear(0.3, 0.010, tbuf, DS_YELLOW, font0);

    sprintf(tbuf, "Gamma: %6.2f", otf.gamma);
    xgr_drawTextClear(0.75, 0.010, tbuf, DS_YELLOW, font0);

    xgr_drawBox( otf.plotxb, otf.plotyb, otf.plotxe, otf.plotye, DS_GRAY75);

    placeBeamSize();
  }

  if(otf.regionshow)
  {
    drawRegions();
  }

  xgr_pixmapToWindow("OTF Gridder");

  if(otf.valueMax < 0.0)
  {
    otf.valueMax = 10.0;
  }

  otf.lastAction = NORMAL_MAP;

  if(debug)
  {
    log_msg(" ");
  }

  log_msg("paintOtfGrid(): Done");

#ifdef DO_DSTIME
    do_etime(PAINTOTFGRID, DSTIME_STOP);
#endif

  return(0);
}


int putOtfGrid()
{
  if(!otf.cols || !otf.rows)
    return(0);

  xgr_drawGrid( globalXmax, globalXmin, globalYmax, globalYmin,
	 otf.cols, otf.rows, 4, 4, 1, 3, DS_GRAY50, DS_YELLOW, XGR_PLOT_SEX);

  return(0);
}



int drawColorBar()
{
  float xl, xr, yt, yb;
  float deltax, deltay;
  int i;
  float xmin, xmax, ymin, ymax;

  if(otf.noColorBar)
  {
    otf.noColorBar = 0;
    return(0);
  }
  xmin = globalXmin;
  xmax = globalXmax;
  ymin = globalYmin;
  ymax = globalYmax;
  deltax = (xmax - xmin) / (float)(max_gray_scale-1.0);
  deltay = (ymax - ymin) / (float)1;
  xgr_drawBox( OTFPLOTXB, OTFPLOTYB-0.08, OTFPLOTXE, OTFPLOTYB-0.06, DS_GRAY75);
  for(i=0;i<max_gray_scale;i++)
  {
    xl = xmin+(float)i*deltax;
    xr = xmin+(float)(i+1)*deltax;
    yt = ymin;
    yb = ymin +1.0*deltay;

    if( (i+1) > (otf.cutoff * max_gray_scale / 100))
    {
      xgr_drawFillBox( xmin, xmax, ymin, ymax, xl, yt, xr, yb,(i+1)*-1);
    }
    else
    {
      xgr_drawFillBox( xmin, xmax, ymin, ymax, xl, yt, xr, yb, DS_BLACK);
    }
  }

  return(0);
}



int fillOtfGrid()
{
  int noint;

  if(!otf.head)
  {
    return(1);
  }

  otf.xsource    = otf.manMapRa;
  otf.ysource    = otf.manMapDec;
  otf.nyquistRa  = otf.manNyquistRa;
  otf.nyquistDec = otf.manNyquistDec;

  if(checkMapScans(version, otf.head->scan))
  {
    log_msg("Scan %.2f, version: %d, already in grid", otf.head->scan, version);

    return(1);
  }

  if(fabs(otf.restfreq - otf.head->restfreq) > 0.5 && otf.restfreq != -1.0)
  {
    log_msg("Rest Freq %.2f: NOT the same as grid %.2f", otf.head->restfreq, otf.restfreq);

    return(1);
  }

  if(fabs(otf.velocity - otf.head->velocity) > 0.5 && otf.velocity != -1.0)
  {
//    log_msg("Velocity %.2f: NOT the same as grid %.2f", otf.head->velocity, otf.velocity);

    if(!otf.allowVelShift)
    {
      return(1);
    }
  }

  if(fabs(otf.freqres - otf.head->freqres) > 0.001  && otf.freqres != -1.0 )
  {
    log_msg("Resolution %.2f: NOT the same as grid %.2f", otf.head->freqres, otf.freqres);

    return(1);
  }

  noint = (int)otf.head->noint;

  if(noint != otf.noint)		     /* this fixed a very hidious bug */
  {
    log_msg("Number of Channels %d, Not the same as grid %d", noint, otf.noint);

    return(1);
  }

  otf.valueMaxChanged = 1;
  otf.row = (int)(otf.head->ycell0 - 1.0);

  accumDataIntoWork();

  addMapScans(version, otf.head->scan);

  otf.valueMaxChanged = 1;
  otf.intenChanged    = 1;
  otf.newShade        = 1;

  return(0);
}


int accumDataIntoWork()
{
  int i, j, first = 1, rej;
  int workCent_x = -1, workCent_y = -1;
  int colIndx, rowIndx;
  int cell_x, cell_y;
  float *raData, *decData;
  float ra, dec;
  struct GRID_ACCUM *p;
  double *w;
  float  *o;
  double deltaRa = 0.0, deltaDec = 0.0, cosDec;

  zeroWorkArray();

  raData  = &otf.ondata[otf.noint * otf.numSpectra + otf.numSpectra*1];
  decData = &otf.ondata[otf.noint * otf.numSpectra + otf.numSpectra*2];

  rej = 0;

  if(!ephemObj)
  {
    deltaRa  = otf.xsource  - otf.head->xsource;
    deltaDec = otf.ysource - otf.head->ysource;
  }

  if(debug)
  {
    log_msg("ACCUMULATOR:   Loading Scan %7.2f", otf.head->scan);
  }

  cosDec = cos(otf.head->ysource * CTR);

  for(i=0;i<otf.numSpectra;i++)
  {
    ra  = (*raData++ / cosDec - deltaRa)  * 3600.0;
    dec = (*decData++         - deltaDec) * 3600.0;

    colIndx = (int)rint(((float)otf.cols-1.0) - ((ra   / otf.nyquistRa) +  (float)otf.cols        / 2.0));
    rowIndx = (int)rint(                        ((dec / otf.nyquistDec) + ((float)otf.rows + 1.0) / 2.0) -1.0);

//    colIndx = (otf.cols-1) - ((int)(ra  / otf.nyquistRa)  + otf.cols / 2);
//    rowIndx = (int)rint(((dec / otf.nyquistDec) + ((float)otf.rows+1.0) / 2.0) -1.0);

//    log_msg("ACCUMULATOR(): ra = %f, colIndx = %3d, Dec = %f, rowIndx = %3d", ra, colIndx, dec, rowIndx);

    if(colIndx<0 || colIndx>otf.cols-1 || rowIndx<0 || rowIndx>otf.rows-1)
    {
      rej++;

      continue;
    }

    if(first)
    {
      workCent_x = colIndx;
      workCent_y = rowIndx;
      first = 0;
    }

    cell_x = workCent_x - colIndx;
    cell_y = workCent_y - rowIndx;

    if(cell_x < WORK_ARRAY_X_M || cell_x > WORK_ARRAY_X_P || cell_y < WORK_ARRAY_Y_M || cell_y > WORK_ARRAY_Y_P)
    {
      normalizeAndCal();

      accumWorkIntoGrid();

      zeroWorkArray();

      workCent_x = colIndx;
      workCent_y = rowIndx;
      cell_x     = 0;
      cell_y     = 0;
    }
      
    p = &workArray[cell_x + WORK_ARRAY_X_CENT] [cell_y + WORK_ARRAY_Y_CENT];
    w = &p->chans[0];
    o = &otf.ondata[i*otf.noint];

    p->n++;
    p->col = colIndx;
    p->row = rowIndx;

    for(j=0;j<otf.noint;j++, w++, o++)
    {
      *w += (double)*o;
    }
  }

  if(rej)
  {
    if(rej == otf.numSpectra)
    {
      log_msg("ACCUMULATOR: Scan: %.2f: All %d Spectra Out of Bounds", otf.head->scan, rej);
      return(0);
    }

    log_msg("ACCUMULATOR: Scan: %.2f: %d Spectra Out of Bounds", otf.head->scan, rej);
  }

  normalizeAndCal();
  accumWorkIntoGrid();

  return(0);
}



int zeroWorkArray()
{
  int col, row;
  struct GRID_ACCUM *p;

  for(col=0;col<WORK_ARRAY_X;col++)
  {
    for(row=0;row<WORK_ARRAY_Y;row++)
    {
      p = &workArray[col][row];
      p->n = 0;
      p->col = 0;
      p->row = 0;
      p->wt  = 0.0;
    }
  }

  bzero((char *)workArrayChans, otf.sizeofWorkArray);

  return(0);
}



int normalizeAndCal()
{
  int col, row, k;
  struct GRID_ACCUM *p;
  double *w;
  float  *o, *g;

  for(col=0;col<WORK_ARRAY_X;col++)
  {
    for(row=0;row<WORK_ARRAY_Y;row++)
    {
      p = &workArray[col][row];

      if(!p->n)					/* no data in this cell */
      {
        continue;
      }

      w = &p->chans[0];
      o = &otf.offdata[0];
      g = &otf.gainsdata[0];

      for(k=0;k<otf.noint;k++, w++, o++, g++)
      {
        *w = (((*w / (double)p->n) - (double)*o) / (double)*o) * (double)*g;
      }

      p->wt = (double)p->n * 0.1 / ((double)otf.head->stsys * (double)otf.head->stsys);
    }
  }

  return(0);
}


int insertVersion(g, ver)
struct OTF_GRID *g;
int ver;
{
  int i;

  for(i=0;i<8;i++) 		/* Check if it's already in here */
  {
    if(g->version[i] == ver)
    {
      return(0); 		/* yes, return */
    }
  }

  for(i=0;i<8;i++) 		/* No, so add it */
  {
    if(g->version[i] == 0)	/* Find an empty slot */
    {
      g->version[i] = ver;

      return(0);
    }
  }

  return(1);
}



int shiftOtfChans(in, n, shift)
double *in;
int n, shift;
{
  int i, k, s;
  double avg;
  static double out[MAX_DATA_CHANNELS];

  if(!shift) /* Nothing to do */
  {
    return(0);
  }

  bzero((char *)&out[0], sizeof(out));

  avg = 0.0;
  s = abs(shift); 
  if(shift > 0)
  {
//    log_msg("shiftOtfChans(): Shifting %d channels to the Right", s);

    k = s;
    for(i=0;i<(n-s);i++,k++) /* shift and copy */
    {
      out[k] = in[i];
    }

    /* find a suitable average */
    for(i=0;i<5;i++)
    {
      avg += in[i];
    }
    avg /= 5.0;

//    log_msg("shiftOtfChans(): filling %d Beginning channels with %f", s, avg);

    for(i=0;i<s;i++) /* Fill in first shift channels with average */
    {
      out[i] = avg;
    }
  }
  else
  {
//    log_msg("shiftOtfChans(): Shifting %d channels to the Left", s);

    k = 0;
    for(i=s;i<n;i++,k++) /* shift and copy */
    {
      out[k] = in[i];
    }

    /* find a suitable average */
    for(i=(n-5);i<n;i++)
    {
      avg += in[i];
    }
    avg /= 5.0;

//    log_msg("shiftOtfChans(): filling %d Ending channels with %f", s, avg);

    for(i=(n-s);i<n;i++,k++)
    {
      out[k] = avg; /* k still holds the index */
    }
  }

  /* put it back */

  for(i=0;i<n;i++)
  {
    in[i] = out[i];
  }

  return(0);
}



/* Accumilate the x x Y work array into the greater OTF map.
   If there is a velocity offset, here is where we fix that.
 */
int accumWorkIntoGrid()
{
  int    n, row, col, k, shift = 0;
  struct GRID_ACCUM *p;
  struct OTF_GRID   *f;
  float  *w;
  double *o;

  if(!otf.gridArray)
  {
    log_msg("ACCUM: OTF Array is empty");

    return(1);
  }

//  log_msg("accumWorkIntoGrid(): Enter: %.2f", otf.head->scan);

  shift = rint((otf.head->velocity - (double)otf.velocity) / otf.head->deltax);

  if(shift)
  {
    log_msg("accumWorkIntoGrid(): Need to shift %d channels, for %f %f", shift, otf.velocity, otf.head->velocity);
  }

  n = otf.noint;

  for(col=0;col<WORK_ARRAY_Y;col++)
  {
    for(row=0;row<WORK_ARRAY_X;row++)
    {
      p = &workArray[col][row];

      if(!p->n)					/* no data in this cell */
      {
        continue;
      }

      f = fetchMapCell(p->row, p->col);

      if(!f)	     /* malloc space for grid array info on a as needed basis */
      {
//        if((f = (struct OTF_GRID *)malloc((sizeof(struct OTF_GRID)))) == NULL)
        if((f = (struct OTF_GRID *)calloc((sizeof(struct OTF_GRID)), 1)) == NULL)
        {
          log_msg("Err accumWorkIntoGrid(malloc(%d))", sizeof(struct OTF_GRID));
          return(1);
        }

//        bzero((char *)f, sizeof(struct OTF_GRID));

	f->head = do_addHeader(version, lastHead.scan, &lastHead);

        setMapCell(p->row, p->col, f);

        if(gridLast)
	{
	  gridLast = gridLast->next = f;
	}
        else
	{
	  gridHead = gridLast = f;
	}

      }

      if(!f->chans)	    /* malloc space for channels on a as needed basis */
      {
        if((f->chans=(float *)malloc((unsigned)(n*sizeof(float))))==NULL)
        {
          log_msg("Error in accumWorkIntoGrid(malloc(%d))", n*sizeof(float));
          return(1);
        }
        bzero((char *)f->chans, n * sizeof( float) );
      }

      w = f->chans;
      o = p->chans;

      if(shift) /* shift before averaging in */
      {
        shiftOtfChans(o, n, shift);
      }

      for(k=0;k<n;k++, o++, w++)
      {
        *w = ((double)*w * f->wt + *o * p->wt) / (f->wt + p->wt);
      }

      f->wt      += p->wt;
      f->intInten = 0.0;
      f->set      = 1;
      f->time    += (float)p->n * 0.1;
      f->scanct  += 1.0;

      f->tsys     = sqrt(1.0 / (f->wt / f->time));

      if(f->bscan == 0.0)
      {
        f->bscan  = otf.head->scan;
      }

      f->escan    = otf.head->scan;
      f->col      = p->col;
      f->row      = p->row;

      insertVersion(f, version);

      strncpy(f->source, otf.head->object, 16);
      f->source[15] = '\0';

      f->bdirty = 1;
      f->ddirty = 1;

      otf.bdirty = 1;
      otf.ddirty = 1;

      computeBaseLine(f, 0, 0, 0, 0, 0, 0);
    }
  }

  return(1);
}


int markDataDirty()
{
  struct OTF_GRID *g;

  log_msg("markDataDirty()");

  for(g=gridHead;g;g=g->next)
  {
    if(g->set == 1) /* Only work on valid cells */
    {
      g->ddirty = 1;
    }
  }

  otf.ddirty = 1;

  return(0);
}


int markBaseDirty()
{
  struct OTF_GRID *g;

  log_msg("markBaseDirty()");

  for(g=gridHead;g;g=g->next)
  {
    if(g->set == 1) /* Only work on valid cells */
    {
      g->bdirty = 1;
    }
  }

  otf.bdirty = 1;

  return(0);
}



int recomputeBaseLines()
{
  struct OTF_GRID *g;

  if(!otf.gridArray)
  {
    log_msg("BASELINE: OTF Array is empty");

    return(1);
  }

  if(debug)
  {
    log_msg("BASELINE: Recomputing OTF Cell Baselines");
  }

  for(g=gridHead;g;g=g->next)
  {
    if(g->set == 1) /* Only work on valid celles */
    {
      computeBaseLine(g, 0, 0, 0, 0, 0, 0);
    }
  }

  return(1);
}


int computeBaseLine(g, data, a, b, Rms, noint, mode)
struct OTF_GRID *g;
float *data, *a, *b, *Rms;
int noint, mode;
{
  int i, j, l, k, noInt;
  float      x[MAX_DATA_CHANNELS], 
           sig[MAX_DATA_CHANNELS], 
      lsqArray[MAX_DATA_CHANNELS];
  float a1, b1, siga1, sigb1, chi2_1, q1;
  float *p, rms;
  double avg = 0.0;

  if(mode == 0)
  {
    if(!g->set)
    {
      return(0);
    }

    if(!otf.gridBaseLine)
    {
      return(0);
    }

    p = g->chans;
    noInt = otf.noint;
  }
  else
  {
    p = data;
    noInt = noint;
  }

  k=0;

  for(l=0;l<8;l+=2)
  {
    if(otf.baselines[l] == -1 || otf.baselines[l+1] == -1)
    {
      continue;
    }

    if(otf.baselines[l] > noInt || otf.baselines[l+1] > noInt)
    {
      continue;
    }

    for(j=otf.baselines[l];j<otf.baselines[l+1];j++)
    {
      sig[k] = 0.0;
      x[k] = (float)j;
      lsqArray[k++] = p[j];
    }
  }

  for(i=0;i<k;i++)
  {
    avg += lsqArray[i];
  }
  avg /= (double)k;

  if(k>=4)
  {
    lsqfit(x,lsqArray, 0, k, sig, 0, &b1, &a1, &sigb1, &siga1, &chi2_1, &q1);

    computeRms(lsqArray, k, &rms);

    if(mode == 0)
    {
      g->a   = a1;
      g->b   = b1;
      g->rms = rms;
    }
    else
    {
      *a   = a1;
      *b   = b1;
      *Rms = rms;
    }
  }
  else
  {
    if(mode == 0)
    {
      g->a   =   0.0;
      g->b   =   0.0;
      g->rms = 100.0;
    }
    else
    {
      *a   =   0.0;
      *b   =   0.0;
      *Rms = 100.0;
    }
  }

  g->bdirty = 0; /* Mark as clean */

  return(0);
}


int computeRms(y, n, rms)
float *y, *rms;
int n;
{
  int i;
  double mean, sums;

  if(!n)
  {
    return(1);
  }

  mean = 0.0;

  for(i=0;i<n;i++)
  {
    mean += y[i];
  }

  mean /= (float)n;

  sums = 0.0;

  for(i=0;i<n;i++)
  {
    sums += ((y[i] - mean) * (y[i] - mean));
  }

  sums /= (float)n;

  if(sums > 0.0)
  {
    *rms = sqrt(sums);
  }
  else
  {
    *rms = 100.0;
  }

  return(0);
}



struct STURCT_KEYSYMS {
	KeySym sym;
	int   win;
	char *key;
	char *help;
};

struct STURCT_KEYSYMS keysyms[] = {
	{ XK_1,		0,	"1",				"Set Lower Level Cutoff to 10 percent" },
	{ XK_2,		0,	"2",				"Set Lower Level Cutoff to 20 percent" },
	{ XK_3,		0,	"3",				"Set Lower Level Cutoff to 30 percent" },
	{ XK_4,		0,	"4",				"Set Lower Level Cutoff to 40 percent" },
	{ XK_5,		0,	"5",				"Set Lower Level Cutoff to 50 percent" },
	{ XK_6,		0,	"6",				"Set Lower Level Cutoff to 60 percent" },
	{ XK_7,		0,	"7",				"Set Lower Level Cutoff to 70 percent" },
	{ XK_8,		0,	"8",				"Set Lower Level Cutoff to 80 percent" },
	{ XK_9,		0,	"9",				"Set Lower Level Cutoff to 90 percent" },
	{ XK_0,		0,	"0",				"Set Lower Level Cutoff to Full Range" },
	{ XK_b,		0,	"b",				"Place a 'beam-size' marker on screen" },
	{ XK_B,		0,	"B",				"Toggle the Removal of Baselines" },
	{ XK_d,		0,	"d",				"Toggle up through Colors of Regions" },
	{ XK_D,		0,	"D",				"Toggle Down through Colors of Regions" },
	{ XK_f,		0,	"f",				"Set Color Scheme to Flame (Repeat to Toggle Through all)" },
	{ XK_g,		0,	"g",				"Set Color Scheme to Grayscale" },
	{ XK_i,		0,	"i",				"Show Map in Interp Mode" },
	{ XK_l,		0,	"l",				"Toggle Region Labels" },
	{ XK_m,		0,	"m",				"Show Channel Map Mosaic" },
	{ XK_o,		0,	"o",				"Reset Map to Original Values" },
	{ XK_p,		0,	"p",				"Set Color Scheme to Pseudo" },
	{ XK_p,		0,	"<control> + p",		"Screen Capture OTF Plot" },
	{ XK_r,		0,	"r",				"Revert Map to Regular Mode" },
	{ XK_R,		0,	"R",				"Show a Map of RMS Values" },
	{ XK_r,		0,	"<control> r",			"Toggle Region Outlines" },
	{ XK_t,		0,	"t",				"Show a Map of Tsys Values" },
	{ XK_T,		0,	"T",				"Show a Map of Integrated Time Values" },
	{ XK_Up,	0,	"Up-Arrow",			"Move Lower Threshold Up 1%" },
	{ XK_Down,	0,	"Down-Arrow",			"Move Lower Threshold Down 1%" },
	{ XK_Up,	0,	"<Control> Up-Arrow",		"Increase Gamma Effect 5%" },
	{ XK_Down,	0,	"<Control> Down-Arrow",		"Decrease Gamma Effect 5%" },
	{ XK_Up,	0,	"<shift> Up-Arrow",		"Increase Scale 5%" },
	{ XK_Down,	0,	"<shift> Down-Arrow",		"Decrease Scale 5%" },

	{ XK_Up,	1,	"Up-Arrow",			"Move Integration Region Up" },
	{ XK_Down,	1,	"Down-Arrow",			"Move Integration Region Down" },
	{ XK_Up,	1,	"<Control> Up-Arrow",		"Move Upper Integration Channel Up" },
	{ XK_Down,	1,	"<Control> Down-Arrow",		"Move Upper Integration Channel Down" },
	{ XK_Up,	1,	"<shift> Up-Arrow",		"Move Lower Integration Channel Up" },
	{ XK_Down,	1,	"<shift> Down-Arrow",		"Move Lower Integration Channel Down" },
	{ XK_p,		1,	"<control> + p",		"Screen Capture Spectrum Plot" }
};

#define MAX_KEYSYMS (sizeof(keysyms) / sizeof(struct STURCT_KEYSYMS))


int printKeySymHelp()
{
  int i;
  char tbuf[256];
  struct STURCT_KEYSYMS *k;

  sprintf(tbuf, "Press the Following Keys in Either the OTF or Spectral Plot Windows\n");

  printf("\n%s\n", tbuf);
  if(sockmode)
  {
    log_msg(" ");
    log_msg(tbuf);
    log_msg(" ");
  }

  k = &keysyms[0];
  for(i=0;i<MAX_KEYSYMS;i++,k++)
  {
    if(k->win == 0)
    {
      sprintf(tbuf, "Press %s key for %s", k->key, k->help);
      printf("%s\n", tbuf);

      if(sockmode)
      {
        log_msg(tbuf);
      }
    }
  }

  sprintf(tbuf, "Press the Following Keys Spectral Plot Windows\n");

  printf("\n%s\n", tbuf);
  if(sockmode)
  {
    log_msg(" ");
    log_msg(tbuf);
    log_msg(" ");
  }

  k = &keysyms[0];
  for(i=0;i<MAX_KEYSYMS;i++,k++)
  {
    if(k->win == 1)
    {
      sprintf(tbuf, "Press %s key for %s", k->key, k->help);
      printf("%s\n", tbuf);

      if(sockmode)
      {
        log_msg(tbuf);
      }
    }
  }

  return(0);
}



/* Keyboard input to OTF graid map window */
int dispatchOtfGridEvents(e)
XEvent *e;
{
  KeySym   keysym;
  XComposeStatus compose;
  char     buffer[1], tbuf[256], cbuf[256];
  char     fname[256], sysBuf[256];
  int      bufsize=1, change = 0;
  static int color = 0;
  static int old_cutoff = -1;

  XLookupString((XKeyEvent *)e, buffer, bufsize, &keysym, &compose);

  switch(keysym)
  {
    case XK_1:  otf.cutoff = 10; change = 2; break;
    case XK_2:  otf.cutoff = 20; change = 2; break;
    case XK_3:  otf.cutoff = 30; change = 2; break;
    case XK_4:  otf.cutoff = 40; change = 2; break;
    case XK_5:  otf.cutoff = 50; change = 2; break;
    case XK_6:  otf.cutoff = 60; change = 2; break;
    case XK_7:  otf.cutoff = 70; change = 2; break;
    case XK_8:  otf.cutoff = 80; change = 2; break;
    case XK_9:  otf.cutoff = 90; change = 2; break;
    case XK_0:  otf.cutoff =  0; change = 2; break;
    case XK_Up: 
	if(e->xkey.state & ControlMask)
        {
	  otf.gamma /= 1.05;
	  change = 4;
	}
	else
	if(e->xkey.state & ShiftMask)
        {
	  otf.scaleFactor *= 1.05;
	  change = 4;
	}
	else
	{
	  otf.cutoff++;

          if(otf.cutoff > 99)
          {
            otf.cutoff = 99;
          }

	  change = 2; 
	}
	break;

    case XK_Down: 
	if(e->xkey.state & ControlMask)
        {
	  otf.gamma *= 1.05;
	  change = 4;
	}
	else
	if(e->xkey.state & ShiftMask)
        {
	  otf.scaleFactor /= 1.05;
	  change = 4;
	}
	else
	{
	  otf.cutoff--;

          if(otf.cutoff < 0)
          {
	    otf.cutoff = 0;
          }

	  change = 2; 
	}

	break;

    case XK_b:
	if(otf.placeBeamSize == 0)
	{
	  otf.placeBeamSize = 1;
	}
	else
	if(otf.placeBeamSize == 1)
	{
	  otf.placeBeamSize = 2;
	}
	else
	if(otf.placeBeamSize == 2)
	{
	  otf.placeBeamSize = 0;
	}

	sprintf(tbuf, "otf_show_beam_size = %d /q", otf.placeBeamSize);
	to_helper(tbuf);

	change = 1;

	break;

    case XK_B:
	if(otf.gridBaseLine)
	{
	  otf.gridBaseLine = 0;
	}
	else
	{
	  otf.gridBaseLine = 1;
	}

	sprintf(tbuf, "otf_baselines = %d /q", otf.gridBaseLine);
	to_helper(tbuf);

	change = 1;

	break;

    case XK_g: sprintf(cbuf, "gray /q");   color = 0; change = 3; break;

    case XK_o: change = 5; break;

    case XK_p: 
	if(e->xkey.state & ControlMask) /* Grab screen */
	{
          sprintf(fname, "OTF-Plot-%s.png", getLogDate(0));

          sprintf(sysBuf, "import -w 0x%x %s", (unsigned int)server->otfwin->win, fname);

          log_msg("%s", sysBuf);

          system(sysBuf);

	  printf("Created file: %s\n", fname);
	}
	else
	{
	  sprintf(cbuf, "pseudo"); 
	  color = 1; 
	  change = 3; 
	}

	break;

    case XK_f: 

        if(color < 2)
        {
          color = 1;
        }

        color++;

        if(color > 7)
        {
          color = 2;
        }

        sprintf(cbuf, "flame%d", color-1);
	change = 3; 

	break;

    case XK_i: 
//        markDataDirty();
	do_otfInterp();  
	return(0);

    case XK_l: 
//        markDataDirty();
	do_toggleRegionLabels(); 
	return(0);

    case XK_m: 
//        markDataDirty();
	do_otfChannelMap(NULL); 
	return(0);

    case XK_d: 
//        markDataDirty();
	do_toggleRegionBold(1); 
	return(0);

    case XK_D: 
//        markDataDirty();
	do_toggleRegionBold(-1); 
	return(0);

    case XK_r: 
	if(e->xkey.state & ControlMask) 
	{
	  do_toggleRegion();
	}
        else
	{
//          markDataDirty();
	  do_otfRegular();
	}

	return(0);

    case XK_R: 
//        markDataDirty();
	do_otfRms(" ");  
	return(0);

    case XK_t: 
//        markDataDirty();
	do_otfTsys(" "); 
	return(0);

    case XK_T: 
//        markDataDirty();
	do_otfTime(" "); 
	return(0);

    case XK_P:

	break;

    case XK_Shift_L:
    case XK_Shift_R:
    case XK_Control_L:
    case XK_Control_R:
    case XK_Caps_Lock:
    case XK_Shift_Lock:
    case XK_Meta_L:
    case XK_Meta_R:
    case XK_Alt_L:
    case XK_Alt_R:
    case XK_Super_L:
    case XK_Super_R:
    case XK_Hyper_L:
    case XK_Hyper_R:
    case XK_Return:
    case XK_BackSpace:
    case XK_Tab:
    case XK_End:
    case XK_Home:
    case XK_Delete:
    case XK_Escape:

	break;

    default: log_msg("Un-handled key stroke: 0x%x", keysym); break;
  }

  switch(change)
  {
    case 1:
	break;

    case 2:

        if(otf.cutoff == old_cutoff) /* Prevent constant repaints */
        {
          return(0);
        }

	sprintf(tbuf, "otf_cutoff = %d /q", otf.cutoff);
	to_helper(tbuf);

	old_cutoff = otf.cutoff;

	break;

    case 3:

	sprintf(tbuf, "otf_color = %d /q", color);
	to_helper(tbuf);

	dontsend = 1;
	do_otfColorChoice(cbuf);
	dontsend = 0;

	return(0);

    case 4:
	sprintf(tbuf, "otf_gamma = %f /q", otf.gamma);
	to_helper(tbuf);

	sprintf(tbuf, "otf_grid_scale = %f /q", otf.scaleFactor);
	to_helper(tbuf);

	break;

    case 5:			/* Reset everything */
	do_otfRegular();

	otf.placeBeamSize = 1;
	sprintf(tbuf, "otf_show_beam_size = %d /q", otf.placeBeamSize);
	to_helper(tbuf);

	otf.gridBaseLine = 1;
	sprintf(tbuf, "otf_baselines = %d /q", otf.gridBaseLine);
	to_helper(tbuf);

	otf.cutoff = 0;
	sprintf(tbuf, "otf_cutoff = %d /q", otf.cutoff);
	to_helper(tbuf);

	old_cutoff = otf.cutoff;

	sprintf(tbuf, "otf_color = 0 /q");
	to_helper(tbuf);

	dontsend = 1;
	sprintf(cbuf, "pseudo /q"); 
	do_otfColorChoice(cbuf);
	dontsend = 0;

	otf.gamma = 1.0;
	sprintf(tbuf, "otf_gamma = %f /q", otf.gamma);
	to_helper(tbuf);

	otf.scaleFactor = 1.0;
	sprintf(tbuf, "otf_grid_scale = %f /q", otf.scaleFactor);
	to_helper(tbuf);
	
	break;
    
   default:
	return(0);
  }

	/* If you reach here, repaint plot */

  markDataDirty();
  markBaseDirty();

  otf.valueMaxChanged = 1;
  otf.intenChanged    = 1;
  otf.newShade        = 1;

  paintOtfGrid();
  displayRequestedOtf();

  return(0);
}



int checkKeyPress(event)
XEvent *event;
{
  KeySym   keysym;
  XComposeStatus compose;
  char     buffer[1], tbuf[256];
  char     fname[256], sysBuf[256];
  int      bufsize=1, change = 0;
  static int old_b = -1, old_e = -1;

  if(otf.head == NULL)
  {
    log_msg("No map to plot");

    return(1);
  }

	/* Seperate event handler for grid window */
  if(event->xbutton.window == server->otfwin->win)
  {
    dispatchOtfGridEvents(event);
    return(0);
  }

  XLookupString((XKeyEvent *)event, buffer, bufsize, &keysym, &compose);

  if(keysym == XK_p && event->xkey.state & ControlMask)
  {
    if(event->xkey.state & ControlMask) /* Print screen */
    {
      sprintf(fname, "SpectralPlot-%s.png", getLogDate(0));
      
      sprintf(sysBuf, "import -w 0x%x %s", (unsigned int)event->xany.window, fname);

      log_msg("%s", sysBuf);

      system(sysBuf);

      printf("Created file: %s\n", fname);
    }
  }
  else
  if(keysym == XK_Up && event->xkey.state & ControlMask)	/* Raise the upper channel only */
  {
    otf.int_chans[1]++;

    if(otf.int_chans[1] > otf.noint-1) 				/* Don't exceed number of chans */
    {
      otf.int_chans[1] = otf.noint -1;
    }

    change = 1;
  }
  else
  if(keysym == XK_Down && event->xkey.state & ControlMask)	/* Lower the upper channel only */
  {
    otf.int_chans[1]--;

    if(otf.int_chans[1] < otf.int_chans[0])			/* Don't go below lower chan */
    {
      otf.int_chans[1] = otf.int_chans[0];
    }

    change = 1;
  }
  else
  if(keysym == XK_Up && event->xkey.state & ShiftMask)		/* Raise the lower channel only */
  {
    otf.int_chans[0]++;

    if(otf.int_chans[0] > otf.int_chans[1]) 			/* Don't exceed upper chan */
    {
      otf.int_chans[0] = otf.int_chans[1];
    }

    change = 1;
  }
  else
  if(keysym == XK_Down && event->xkey.state & ShiftMask)	/* Lower the lower channel only */
  {
    otf.int_chans[0]--;

    if(otf.int_chans[0] < 0)					/* Don't go below 0 */
    {
      otf.int_chans[0] = 0;
    }

    change = 1;
  }
  else
  if(keysym == XK_Up)
  {
    otf.int_chans[0]++;
    otf.int_chans[1]++;

    if(otf.int_chans[1] > otf.noint-1)
    {
      otf.int_chans[1] = otf.noint -1;
    }

    if(otf.int_chans[0] > otf.int_chans[1])
    {
      otf.int_chans[0] = otf.int_chans[1];
    }

    change = 1;
  }
  else
  if(keysym == XK_Down)
  {
    otf.int_chans[0]--;
    otf.int_chans[1]--;

    if(otf.int_chans[0] < 0)
    {
      otf.int_chans[0] = 0;
    }

    if(otf.int_chans[1] < otf.int_chans[0])
    {
      otf.int_chans[1] = otf.int_chans[0] +1;
    }

    change = 1;
  }
  else
  if(keysym == XK_P)
  {
    log_msg("Shift P pressed");
  }

  if(change)
  {
    sprintf(tbuf, "otf_bint_chan = %d /q", otf.int_chans[0]);
    to_helper(tbuf);
    sprintf(tbuf, "otf_eint_chan = %d /q", otf.int_chans[1]);
    to_helper(tbuf);

    if(otf.int_chans[0] == old_b && otf.int_chans[1] == old_e) /* The same chans? */
    {
      return(0);
    }

    old_b = otf.int_chans[0];
    old_e = otf.int_chans[1];

    markDataDirty();

    otf.valueMaxChanged = 1;
    otf.intenChanged    = 1;
    otf.newShade        = 1;

    paintOtfGrid();
    displayRequestedOtf();
  }
 
  return(0);
}


int do_otfSendPlot(buf)
char *buf;
{
  int n, ret, col, row;

  n = sscanf(buf, "%d %d", &col, &row);

  if(n == 2)
  {
    ret = getRequestedCell(row, col);

    if(ret)
    {
      return(1);
    }
  }

  if(sockmode)
  {
    log_msg("Sending Copy of 'Spectral Plot Window to Linuxpops/Helper'.");

    sock_write(helper, (char *)&ds_image_info, sizeof(ds_image_info));
  }

  return(0);
}



int getRequestedCell(row, col)
int row, col;
{
  int indx;
  struct OTF_GRID *f;

  if((row < 0 || row > otf.rows) && (col < 0 || col > otf.cols))
  {
    log_msg("Requested row: %d, or cols: %d, out of bounds: %d, %d", row, col, otf.rows, otf.cols);
    return(1);
  }

  indx = col+row*otf.cols;

  f = fetchMapCell(row, col);;

  if(!f)
  {
    if(debug)
    {
      log_msg("No data for Cell: %d, %d", col, row);
    }

    return(1);
  }

  if(f->plotVal1)
  {
    if(otf.whichPlot == PLOT_TSYS)
    {
      sprintf(otf.peakStr, "Cell: %dx%d,  Tsys     %8.4f", col, row, f->plotVal1);
    }
    else
    if(otf.whichPlot == PLOT_RMS)
    {
      sprintf(otf.peakStr, "Cell: %dx%d,   rms     %8.4f", col, row, f->plotVal1);
    }
    else
    if(otf.whichPlot == PLOT_TIME)
    {
      sprintf(otf.peakStr, "Cell: %dx%d,   time    %8.4f", col, row, f->plotVal1);
    }
    else
    {
      sprintf(otf.peakStr, "Cell: %dx%d, Intensity %8.4f", col, row, f->plotVal1);
    }

    otf.requestCol  = col;
    otf.requestRow  = row;
    otf.reqSpecIndx = indx;

    displayRequestedOtf();

    xgr_setCurWindow(XGR_OTF_WIN, XGR_SET_WIN);

    xgr_drawTextClear(0.01, 0.010, otf.peakStr, DS_YELLOW, font0);

    return(0);
  }
  else
  {
//    if(debug)
    {
      log_msg("getRequestedCell(): No Intensity for cell: %d: %e", indx, f->plotVal1);
    }
  }

  return(1);
}


int otfGridDisplaySpec(event, mask)
XEvent *event;
unsigned int mask;
{
  struct WINDOW *w;
  int    col, row, indx, but;
  float  x,y;
  double ra, dec, raOff, decOff;
  char   tbuf[256];
  struct OTF_GRID *f;

  if(debug)
  {
    log_msg("otfGridDisplaySpec()");
  }

  if(!otf.gridArray)
  {
    log_msg("DISPLAYSPEC: OTF Array is empty");
    return(1);
  }

  but = event->xbutton.button;

  switch(event->type)
  {
    case MotionNotify:
      if(but == 2 || but == 3 || but == 4 || but == 5)
      {
        log_msg("Button Motion on Button: %d", but);
      }
    case ButtonPressMask:
      if(but == 2 || but == 3 || but == 4 || but == 5)
      {
        log_msg("Button Press on Button: %d", but);
        return(1);
      }
      break;

    case KeyPress:
      checkKeyPress(event);
      return(1);
      break;

    default:
      return(1);
      break;
  }

  if( xgr_findWindow(event->xbutton.display, event->xbutton.window, &w))
  {
    log_msg("xgr_findWindow() returned Non Zero");
    return(1);
  }

  x   = ((float)w->plotxe - (float)w->plotxb) / (float)otf.cols;
  x   = ((float)event->xbutton.x - (float)w->plotxb) / x - 0.5;
  col = (int)rint(x);

  y   = ((float)w->plotye - (float)w->plotyb) / (float)otf.rows;
  y   = ((float)w->plotye - (float)event->xbutton.y) / y - 0.5;
  row = (int)rint(y);

					/* Allow out of bounds clicking for placing beam marker */
  if(!otf.armBeamLocation && (col < 0 || col >= otf.cols || row < 0 || row >= otf.rows))
  {
    if(debug)
    {
      log_msg("Req Col or Row, out of bounds: %d %d", col, row);
    }

    return(1);
  }

  indx = col+row*otf.cols;
  f    = fetchMapCell(row, col);

  if(otf.armBeamLocation)				/* User setting BeamSize location */
  {
    log_msg("Got new beam location of Col: %d, Row: %d", col, row);
    otf.armBeamLocation = 0;

	/* Col & row seems to be off a bit; add a few here */

    row   += 2;

    ra     = otf.xsource / 15.0;
    dec    = otf.ysource;

    raOff  = otf.nyquistRa  * ((float)col - (float)(otf.cols / 2.0));
    decOff = otf.nyquistDec * ((float)row - (float)(otf.rows / 2.0));

    raOff /= 3600.0;
    raOff *= 1.0 - (12.0 / 1000.0);		/* Adjust a smidge */

    raOff /= (15.0 * cos(otf.head->ysource * CTR));
    ra    -= raOff;
    dec   += decOff / 3600.0;

    otf.beamRa  = ra * 15.0;
    otf.beamDec = dec;

	/* send new ra & dec back to lp */
    sprintf(tbuf, "otf_beam_ra = %f /q", otf.beamRa / 15.0);
    to_helper(tbuf);
    sprintf(tbuf, "otf_beam_dec = %f /q", otf.beamDec);
    to_helper(tbuf);

    if(otf.placeBeamSize == 0)
    {
      otf.placeBeamSize = 1;
    }

    forceDrawGrid();

    return(0);
  }

  if(!f)
  {
    if(debug)
    {
      log_msg("No data for Cell: %d, %d", col, row);
    }

    return(1);
  }

  if(f->set == -1)
  {
    if(debug)
    {
      log_msg("Cell: %d, %d, ignored", col, row);
    }

    return(1);
  }

  if(f->plotVal1)
  {
    if(otf.whichPlot == PLOT_TSYS)
    {
      sprintf(otf.peakStr, "Cell: %dx%d,  Tsys     %8.4f", col, row, f->plotVal1);
    }
    else
    if(otf.whichPlot == PLOT_RMS)
    {
      sprintf(otf.peakStr, "Cell: %dx%d,   rms     %8.4f", col, row, f->plotVal1);
    }
    else
    if(otf.whichPlot == PLOT_TIME)
    {
      sprintf(otf.peakStr, "Cell: %dx%d,   time    %8.4f", col, row, f->plotVal1);
    }
    else
    {
      sprintf(otf.peakStr, "Cell: %dx%d, Intensity %8.4f", col, row, f->plotVal1);
    }

    otf.requestCol  = col;
    otf.requestRow  = row;
    otf.reqSpecIndx = indx;

    displayRequestedOtf();

    xgr_setCurWindow(XGR_OTF_WIN, XGR_SET_WIN);

    xgr_drawTextClear(0.01, 0.010, otf.peakStr, DS_YELLOW, font0);

    return(0);
  }
  else
  {
  //  if(debug)
    {
      log_msg("otfGridDisplaySpec(): No Intensity for cell: %d: %e", indx, f->plotVal1);
    }
  }

  return(1);
}



int displayRequestedOtf()
{
  struct OTF_GRID **g;
  struct OTF_GRID *f;

  if(otf.reqSpecIndx < 0)
  {
    return(1);
  }

  if(!otf.gridArray)
  {
    log_msg("DISPLAYREQ: OTF Array is empty");
    return(1);
  }

  g = &otf.gridArray[otf.reqSpecIndx];
  f = (struct OTF_GRID *)*g;

  if(f && f->set == 1)
  {
    displayOtfGridSpectra(f);
  }

  return(0);
}



int displayOtfGridSpectra(s)
struct OTF_GRID *s;
{
  int j, noint, ret;
  double ra, dec, raOff, decOff;
  char sysBuf[256];
  extern int sockWriteNoBitch;			/* in ../cactus/lib/socknoxvlib.c */

  if(!otf.gridArray)
  {
    log_msg("DISPLAYGRIDSPEC: OTF Array is empty");
    return(1);
  }

  noint = otf.noint;

  ra     = otf.xsource / 15.0;
  dec    = otf.ysource;
  raOff  = otf.nyquistRa  * ((float)otf.requestCol - (float)(otf.cols / 2.0));
  decOff = otf.nyquistDec * ((float)otf.requestRow - (float)(otf.rows / 2.0));
  raOff /= 3600.0;
  raOff /= (15.0 * cos(otf.head->ysource * CTR));
  ra    -= raOff;
  dec   += decOff / 3600.0;

  strcpy(ds_image_info.magic, "DS_IMAGE_INFO");

  strncpy(ds_image_info.source, s->source, 16);
  ds_image_info.source[15] = '\0';

  strncpy(ds_image_info.coordcd, otf.head->coordcd, 8);
  ds_image_info.coordcd[7] = '\0';

  if(s->head)
  {
    bcopy((char *)s->head, (char *)&ds_image_info.head, sizeof(struct HEADER));
  }
  else
  {
    bcopy((char *)&lastHead, (char *)&ds_image_info.head, sizeof(struct HEADER));
  }

  ds_image_info.ra       = ra;
  ds_image_info.dec      = dec;
  ds_image_info.rms      = s->rms;
  ds_image_info.a        = s->a;
  ds_image_info.b        = s->b;
  ds_image_info.col      = otf.requestCol;
  ds_image_info.row      = otf.requestRow;
  ds_image_info.bscan    = s->bscan;
  ds_image_info.escan    = s->escan;
  ds_image_info.noint    = noint;
  ds_image_info.int_beg  = otf.int_chans[0];
  ds_image_info.int_end  = otf.int_chans[1];
  ds_image_info.freqres  = otf.head->freqres;
  ds_image_info.synfreq  = otf.head->synfreq;
  ds_image_info.sideband = otf.head->sideband;
  ds_image_info.freq     = otf.head->restfreq;
  ds_image_info.vel      = otf.head->refpt_vel;
  ds_image_info.refpt    = otf.head->refpt;
  ds_image_info.sb       = (int)otf.head->sideband;
  ds_image_info.date     = otf.head->utdate;
  ds_image_info.dv       = otf.head->deltax;
  ds_image_info.cal      = otf.head->tcal;
  ds_image_info.wt       = s->wt;
  ds_image_info.time     = s->time;
  ds_image_info.scanct   = (int)s->scanct;
  ds_image_info.tsys     = s->tsys;

  ds_image_info.head.inttime = s->time; /* Overwrite Header */
 
  if(otf.gridBaseLine)
  {
    for(j=0;j<noint;j++)
    {
      ds_image_info.ydata[j] = s->chans[j] - ((float)j * s->a + s->b);
    }
  }
  else
  {
    for(j=0;j<noint;j++)
    {
      ds_image_info.ydata[j] = s->chans[j];
    }
  }

  ds_image_info.doBaseline = otf.gridBaseLine;
  bcopy((char *)otf.baselines, (char *)ds_image_info.baselines, sizeof(ds_image_info.baselines));

  sockWriteNoBitch = 1;

  ret = sock_write(specplot, (char *)&ds_image_info, sizeof(ds_image_info));

  sockWriteNoBitch = 0;

  if(!ret)  /* Spec Plot is not running; start it */
  {
    fprintf(stdout, "Starting specplot viewer\n");
    fflush(stdout);

    log_msg("sock_write() returned Error; starting specplot viewer");

    sprintf(sysBuf, "specplot &");

    system(sysBuf);

    usleep(250000);

    /* resend */
    ret = sock_write(specplot, (char *)&ds_image_info, sizeof(ds_image_info));

    if(!ret)
    {
      fprintf(stdout, "sock_write() Still returning Error; Check Instalation\n");
      fflush(stdout);

      log_msg("sock_write() Still returning Error; Check Installation");
    }
  }

  return(1);
}



int getBaselines(b)
int *b;
{
  if(b)
  {
    bcopy((char *)otf.baselines, (char *)b, sizeof(ds_image_info.baselines));
  }

  return(otf.gridBaseLine);
}
 

 
int do_otfVelRun(buf)
char *buf;
{
  int i, b, e, n, ob, oe, record=0, interp=0;
  char dir[20], *cp, tmp[256];
  char sysBuf[256], fname[256], src[256];
  double d;

  strcpy(tmp, buf); 

  if(strstr(buf, "record"))
  {
    record = 1;
  }

  if(strstr(buf, "interp"))
  {
    interp = 1;
  }

  cp = strtok(tmp," \n\t");
  if(cp)  
  {
    b  = atoi(cp);
  }
  else 
  {  
    log_msg("Bad begin chan specifier in do_otfVelRun");
    return(1);
  }

  cp = strtok((char *)NULL," \n\t");
  if(cp)  
  {
    e = atoi(cp);
  }
  else 
  {
    log_msg("Bad ending chan specifier in do_otfVelRun");
    return(1);
  }

  cp = strtok((char *)NULL," \n\t");
  if(cp)  
  {
    n = atoi(cp);
  }
  else 
  {
    log_msg("Bad channel spacing chan specifier in do_otfVelRun");
    return(1);
  }

  cp = strtok((char *)NULL," \n\t");
  if(cp)  
  {
    strcpy(dir,cp);
  }
  else 
  {
    log_msg("Bad direction specifier in do_otfVelRun");
    return(1);
  }

  cp = strtok((char *)NULL," \n\t");
  if(cp)  
  {
    d = atof(cp);
  }
  else 
  {
    d = 0.0;
  }

  if(otf.head == NULL)
  {
    return(1);
  }
 
  otf.valueMaxChanged  = 1;
  otf.lockScale        = 1;
  ob                   = otf.int_chans[0];
  oe                   = otf.int_chans[1];

  if(!strncmp(dir, "forw", 4)) 
  {
    for(i=b;i<=e;i++)
    {
      if(i+(n-1)>otf.noint)
      {
        continue;
      }


      otf.int_chans[0] = i;
      otf.int_chans[1] = i+(n-1);
      otf.intenChanged = 1;
      otf.newShade     = 1;

      markDataDirty();

      paintOtfGrid();
      displayRequestedOtf();

      if(record)
      {
        if(interp)
        {
	  do_otfInterp();

          XFlush(server->dpy);

	  usleep(100000);
	}

        strncpy(src, otf.head->object, 16);
	src[16] = '\0';
        strcmprs(src);

	sprintf(fname, "%s-%.3f-%03d.png", src, otf.head->restfreq / 1000.0, i);

        sprintf(sysBuf, "import -w 0x%x %s", (unsigned int)server->otfwin->win, fname);

        log_msg("%s", sysBuf);

        system(sysBuf);
      }

      if(ds_delay(d))
      {
        break;
      }
    }
  }

  otf.valueMaxChanged  = 1;
  otf.intenChanged     = 1;
  otf.newShade         = 1;
  otf.lockScale        = 0;
  otf.int_chans[0]     = ob;
  otf.int_chans[1]     = oe;

  xgr_clearScreen();

  markDataDirty();

  paintOtfGrid();

  displayRequestedOtf();

  return(0);
}



int do_otfGridDump()
{
  int    row, col;
  struct OTF_GRID *f;

  if(!otf.gridArray)
  {
    log_msg("GRIDDUMP: OTF Array is empty");
    return(1);
  }

  if(otf.head == NULL)
  {
    return(1);
  }

  for(row=0;row<otf.rows;row++)
  {
    for(col=0;col<otf.cols;col++)
    {
      f = fetchMapCell(row, col);

      if(f && f->set == 1)
      {
        printf("%f ", f->plotVal1);
      }
      else
      {
        printf("%f ", 0.0);
      }
    }

    printf("\n");
  }

  return(0);
}




int do_otfGridLoad(buf)
char *buf;
{
  int i, b, e, repaint = 1;
  char *cp, tb[80];
  float d;
  char tmp[256]; 
  extern int stopFlag;
  int oldBaseline;
 
  log_msg("do_otfGridLoad(): Enter");

#ifdef DO_DSTIME
  do_etime(DO_OTFGRIDLOAD, DSTIME_START);
#endif

  strcpy(tmp, buf); 

	/* Grab beginning scan number */
  cp = strtok(tmp,"\n\t ");

  if(cp)
  {
    b = atoi(cp);
  }
  else
  {
    log_msg("do_otfGridLoad(), no begin scan specified");

#ifdef DO_DSTIME
    do_etime(DO_OTFGRIDLOAD, DSTIME_STOP);
#endif

    return(1);
  }
	/* Grab ending scan number */
  cp = strtok((char *)NULL,"\n\t ");

  if(cp)
  {
    e = atoi(cp);
  }
  else
  {
    log_msg("do_otfGridLoad(), no end scan specified");

#ifdef DO_DSTIME
    do_etime(DO_OTFGRIDLOAD, DSTIME_STOP);
#endif

    return(1);
  }
	/* Grab delay; if any */
  cp = strtok((char *)NULL,"\n\t ");

  if(cp)
  {
    d = atof(cp);
  }
  else
  {
    d = 0.0;
  }

  if(d < 0.0) /* It a sign to not repaint i.e. fast load */
  {
    repaint = 0;
    d = 0.0;
  }

  if(otf.loadChoice != LOAD_PREVIEW)
  {
    otf.noPlot = 1;
  }

  otf.newShade = 1;

  oldBaseline = otf.gridBaseLine;             /* don't baseline while loading */
  otf.gridBaseLine = 0;

  otf.ignoreAllRepaints = 1;

  for(i=b;i<=e;i++)
  {
    sprintf(tb,"%d",i);

    if(otf.loadChoice == LOAD_STEP)
    {
      if(!(i % 10) || i == b || i == e)
      {
        otf.newShade = 1;
        otf.noPlot = 0;
      }
      else
      {
        otf.noPlot = 1;
      }
    }

    fbScanDone  = 1;
    aosScanDone = 3;

    do_scanDone(tb);

    if(ds_delay(d))
    {
      log_msg("do_otfGridLoad(): Breaking out of load");

      stopFlag = 1;

      if(repaint)
      {
        forceDrawGrid();
      }

      break;
    }
    else
    {
      xgr_checkForEvents();
    }
  }

  otf.ignoreAllRepaints = 0;

  log_msg("do_otfGridLoad(): Done loading scans");

  otf.noPlot = 0; 

  otf.gridBaseLine = oldBaseline; /* now compute the baselines */

  if(otf.gridBaseLine)
  {
    if(repaint)
    {
      recomputeBaseLines();
    }
  }

  if(repaint)
  {
    forceDrawGrid();
  }

  if(sockmode)
  {
    to_helper("done");
  }

#ifdef DO_DSTIME
    do_etime(DO_OTFGRIDLOAD, DSTIME_STOP);
#endif

  return(0);
}


int do_otfGridInit()
{
  do_etimeClear();
 
  otf.initialized = 0;
  otf.head        = 0;
  otf.reqSpecIndx = -1;

  xgr_setCurWindow(XGR_OTF_WIN, XGR_SET_PIXMAP);

  xgr_clearScreen();

  xgr_pixmapToWindow("OTF Gridder");

  xgr_setWindowSize("OTF Gridder", 879, 779);
  usleep(10000);
  xgr_setWindowSize("OTF Gridder", 860, 780);

  initGrid();
 
  return(0);
}



int otfInterp()
{
  struct XGR_HEAD *p;
  struct XGR_ *s;
  struct WINDOW_HEAD *p2;
  struct WINDOW *s2;
  int w, h;

//  log_msg("otfInterp(): Entered");

  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;

    for(p2=s->windows.next;p2!=&s->windows;p2=p2->next)
    {
      s2 = (struct WINDOW *)p2;

      if(!strcmp(s2->frameTitle, "OTF Gridder"))
      {
        if(otf.ximage)
	{
	  XDestroyImage(otf.ximage);
          otf.ximage = NULL;
	}

	w = s2->plotxe-s2->plotxb;

        if(w%2)
	{
	  w--;
	}

	h = s2->plotye-s2->plotyb;

        otf.ximage = createXimage(s->dpy, w, h, s->depth);

	if(otf.ximage)
	{
	  s2->pixmapX = s2->plotxb;
	  s2->pixmapY = s2->plotyb;

//          log_msg("otfInterp(): x = %d, y = %d, w = %d, h = %d", s2->pixmapX, s2->pixmapY, w, h);

          interpAndPlot(s);
	}
      }
    }
  }

  if(otf.ximage)
  {
    XDestroyImage(otf.ximage);
  }

  otf.ximage = NULL;
  otf.lastAction = INTERP;
 
  return(0);
}



int interpAndPlot(s)
struct XGR_ *s;
{
  int    i, row, col, k, x, y;
  int    in, out;
  float *inarray=NULL, *outarray=NULL;
  unsigned char uc;
  float  d;
  struct OTF_GRID *f;

  if(!otf.gridArray)
  {
    log_msg("INTERP: OTF Array is empty");
    return(1);
  }

  if(!otf.ximage)
  {
    log_msg("No otf.ximage");
    return(1);
  }

  if(!otf.doingMosaic)
  {
    xgr_setCurWindow(XGR_OTF_WIN, XGR_SET_PIXMAP);

    xgr_mapUnmapWindow("OTF Gridder", 1);

    drawColorBar();

    xgr_setBox( otf.plotxb, otf.plotyb, otf.plotxe, otf.plotye);
  }

  in  = otf.cols          * otf.rows;
  out = otf.ximage->width * otf.ximage->height;

  if( (inarray = (float *)malloc((unsigned)(in * sizeof(float)))) == NULL)
  {
    log_msg("Unable to malloc(%d) inarray in interpAndPlot()", in * sizeof(float));

    return(1);
  }

  if( (outarray = (float *)malloc((unsigned)(out * sizeof(float)))) == NULL)
  {
    log_msg("Unable to malloc(%d) outarray in interpAndPlot()", in * sizeof(float));

    free((char *)inarray);

    return(1);
  }

  bzero((char *)inarray,  in  * sizeof(float));
  bzero((char *)outarray, out * sizeof(float));

  k = 0;
  for(row=otf.rows-1;row>=0;row--)
  {
    for(col=0;col<otf.cols;col++)
    {
      f = fetchMapCell(row, col);

      if(f && f->set == 1)
      {
        inarray[k++] = (float)f->shade;
      }
      else
      {
        inarray[k++] = 0.0;
      }
    }
  }

  interp(inarray, otf.cols, otf.rows, outarray, otf.ximage->width, otf.ximage->height);

  if(s->depth == 8)
  {
    for(i=0;i<out;i++)
    {
      d = (float)outarray[i];

      if(d < 1.0)
      {
        uc = 1;
      }
      else
      if(d > (float)(max_gray_scale-1))
      {
        uc = max_gray_scale-1;
      }
      else
      {
        uc = (unsigned char)outarray[i];
      }

      if(uc > (otf.cutoff * max_gray_scale / 100))
      {
        otf.ximage->data[i] = (unsigned char)s->grayScale[(int)uc];
      }
      else
      {
        otf.ximage->data[i] = (unsigned char)s->colors[DS_BLACK];
      }
    }
  }
  else
  {
    i = 0;

    for(y=0;y<otf.ximage->height;y++)
    {
      for(x=0;x<otf.ximage->width;x++,i++)
      {
        d = (float)outarray[i];

        if(d < 1.0)
	{
          uc = 1;
	}
        else
        if(d > (float)(max_gray_scale-1))
	{
          uc = max_gray_scale-1;
	}
        else
	{
          uc = (unsigned char)outarray[i];
	}

        if(uc > (otf.cutoff * max_gray_scale / 100))
	{
          XPutPixel(otf.ximage, x, y, s->grayScale[(int)uc]);
	}
        else
	{
          XPutPixel(otf.ximage, x, y, s->colors[DS_BLACK]);
	}
      }
    }
  }

  XPutImage(s->dpy, s->cur->act, s->cur->gc, otf.ximage, 0, 0,
		s->cur->pixmapX, s->cur->pixmapY,
		(unsigned int)otf.ximage->width, 
		(unsigned int)otf.ximage->height);

  if(!otf.doingMosaic)
  {
    putOtfGrid();

    placeBeamSize();

    if(otf.regionshow)
    {
      drawRegions();
    }

    xgr_pixmapToWindow("OTF Gridder");
  }

  if(inarray)
  {
    free((char *)inarray);
  }

  if(outarray)
  {
    free((char *)outarray);
  }

  return(0);
}


              /* Create an X11 image given a desired width, height and depth. */
XImage *createXimage(display, w, h, depth)
Display *display;
int w, h, depth;
{
  Visual *visual;
  XImage *xi;
  unsigned ui;
 
  visual = DefaultVisual(display, DefaultScreen(display));

  if(depth == 1)
  {
    return((XImage *)NULL);
  }

  xi = XCreateImage(display, visual, (unsigned int)depth, ZPixmap,0,
			(char *)NULL, (unsigned int)w, (unsigned int)h, 16, 0); 
  if(xi == NULL)
  {
    log_msg("XCreateImage failed");
    return((XImage *)NULL);
  }
 
  ui = (unsigned)(xi->height*xi->bytes_per_line*sizeof(char));
  if((xi->data=(char *)malloc(ui))==NULL)
  {
    log_msg("Unable to malloc(%d) ximage in createXimage()", ui);
    XDestroyImage(xi);
    return((XImage *)NULL);
  }

  bzero((char *)xi->data, xi->height * xi->bytes_per_line * sizeof(char));

  return(xi);
}



int do_otfChannelMap(buf)
char *buf;
{
  int i, j, bc, ec, indx, halfway, first=1;
  char *cp, tmp[256], vbuf[80];
  float scale, xb, xe, yb, ye, vbc, vec, refpt, deltax12, vel;
  static int cc=256, nx=5, ny=5, nc=1;

  if(buf)
  {
    strcpy(tmp, buf);
  }
  else
  {
    sprintf(tmp, "%d %d %d %d", cc, nx, ny, nc);
  }

  cp = strtok(tmp," \n\t");
  if(cp) 
  {
    cc = atoi(cp);
  }
  else 
  {
    log_msg("Bad Center channel in do_otfChannelMap()");
    return(1);
  }

  cp = strtok(NULL," \n\t");
  if(cp) 
  {
    nx = atoi(cp);
  }
  else 
  {
    log_msg("Bad num of col specifier in do_otfChannelMap()");
    return(1);
  }

  cp = strtok((char *)NULL," \n\t");
  if(cp) 
  { 
    ny = atoi(cp);
  }
  else 
  {
    log_msg("Bad num of row specifier in do_otfChannelMap()");
    return(1);
  }

  cp = strtok((char *)NULL," \n\t");
  if(cp) 
  { 
    nc = atoi(cp);
  }
  else 
  {
    log_msg("Bad num of channels specifier in do_otfChannelMap()");
    return(1);
  }

  if(otf.head == NULL)
  {
    return(1);
  }

  xgr_setCurWindow(XGR_OTF_WIN, XGR_SET_WIN); /* Us the none pixmap window */

  setInValid(1);

  xgr_clearScreen();

  refpt           = otf.head->refpt-0.5;
  deltax12        = otf.head->deltax;
  vel             = otf.head->refpt_vel;
  halfway         = (nx*ny*nc) / 2;
  indx            = cc - halfway;
  otf.doingMosaic = 1;
  xgr_hardFont    = -1;

  for(i=(ny-1);i>=0;i--)
  {
    for(j=0;j<nx;j++)
    {
      setInValid(1);

      xb = (float)j     / (float)nx;
      xe = (float)(j+1) / (float)nx;
      yb = (float)i     / (float)ny;
      ye = (float)(i+1) / (float)ny;

      scale  = ye-yb;
      xgr_drawBox(xb+0.005, 0.05*scale +yb, xe, 0.998*scale +yb, DS_GRAY75);

      bc               = indx;
      ec               = indx+(nc-1);
      indx            += nc;

      otf.intenChanged = 1;

      markDataDirty();		/* Force a recalculation of intensities */

      computeInten(bc, ec);

      otf.newShade = 1;

      computeShade();

      otf.noColorBar = 1;

      otf.newShade = 1;

      otfInterp();

      if(otf.head->sideband == 3.0)
      {
        vbc = vel - (((float)bc - refpt) * deltax12) + deltax12 / 2.0;
        vec = vel - (((float)ec - refpt) * deltax12) - deltax12 / 2.0;
      }
      else
      {
        vbc = vel - (((float)bc - refpt) * -deltax12) - deltax12 / 2.0;
        vec = vel - (((float)ec - refpt) * -deltax12) + deltax12 / 2.0;
      }

      if( (vec < vel && vbc > vel) || (vbc < vel && vec > vel))
      {
        sprintf(vbuf, "%.1f, %.1f %.1f km/s", vbc, vec, vel);
        xgr_drawPosText(0.01, 0.98, vbuf, DS_GREEN, font1);
      }
      else
      {
        sprintf(vbuf, "%.1f, %.1f km/s", vbc, vec);
        xgr_drawPosText(0.01, 0.98, vbuf, DS_WHITE, font1);
      }

      if(first)
      {
        xgr_drawPosText(0.01, 0.08, source, DS_GREEN, font1);
        first = 0;
      }
    }
  }

  xgr_hardFont = 0;

  xgr_setCurWindow(XGR_OTF_WIN, XGR_SET_WIN);

  otf.doingMosaic = 0;

  setInValid(0);

  sprintf(oldBuf, "otfchannelmap %d %d %d", nx, ny, nc);

  otf.intenChanged = 1;

  markDataDirty();		/* Force a recalculation of intensities */

  computeInten(otf.int_chans[0], otf.int_chans[1]);

  otf.newShade = 1;

  computeShade();

  otf.lastAction = CHANNEL_MAP;

  return(0);
}



int getChanOTF()
{
  return(otf.chanSelOTF);
}



double v1a[8], v2a[8];

int createInterpTable(max)
int max;
{
  int i;
  double frac;

  if(max < 0 || max > 8)
  {
    return(1);
  }

  bzero((char *)&v1a, sizeof(v1a));
  bzero((char *)&v2a, sizeof(v2a));

  frac = 1.0 / (double)max;

  for(i=0;i<(max+1);i++)
  {
    v1a[i] = 1.0 - (frac * (double)i);
    v2a[i] = frac * (double)i;
  }

  log_msg("V1A: %d: %f %f %f %f %f %f %f %f", max,
    v1a[0], v1a[1], v1a[2], v1a[3], v1a[4], v1a[5], v1a[6], v1a[7]);

  log_msg("V2A: %d: %f %f %f %f %f %f %f %f", max,
    v2a[0], v2a[1], v2a[2], v2a[3], v2a[4], v2a[5], v2a[6], v2a[7]);


  return(0);
}


double findInterp(v1, v2, indx, max)
double v1, v2;
int    indx, max;
{
  if(indx == 0) /* no use going any further */
  {
    return(v1);
  }

  if(indx > max)
  {
    return(v1);
  }

  return(v1 * v1a[indx] + v2 * v2a[indx]);
}




int do_otfSpectralSum(buf)
char *buf;
{
  int    row, col, k;
  struct OTF_GRID p1, *p1p, *p2p;
  float  chans[MAX_DATA_CHANNELS];
  float  *p1c, *p2c;

  if(!otf.gridArray)
  {
    log_msg("do_otfSpectralSum(): OTF Array is empty");

    return(1);
  }

  bzero((char *)&p1,    sizeof(p1));
  bzero((char *)&chans, sizeof(chans));

  p1p        = &p1;
  p1p->chans = &chans[0];

  for(row=0;row<otf.rows;row++) 			/* Search all cells for valid data */
  {
    for(col=0;col<otf.cols;col++)
    {
      p2p = fetchMapCell(row, col);;

      if(!p2p) /* No data here */
      {
        continue;
      }

      if(p2p->set == -1) /* data ignored */
      {
        continue;
      }

      p1c = p1p->chans;
      p2c = p2p->chans;

      for(k=0;k<otf.noint;k++, p1c++, p2c++)
      {
        *p1c = (*p1c * p1p->wt + *p2c * p2p->wt) / (p1p->wt + p2p->wt);
      }

      p1p->wt += p2p->wt;

      p1p->time = p1p->time + p2p->time;
      p1p->tsys = sqrt(1.0 / (p1p->wt / p1p->time));
    }
  }

  p1p->set = 1;

  computeBaseLine(p1p, 0, 0, 0, 0, 0, 0);

  strcpy(ds_image_info.magic, "DS_IMAGE_INFO");

  strncpy(ds_image_info.source, lastHead.object, 16);
  ds_image_info.source[15] = '\0';

  strncpy(ds_image_info.coordcd, otf.head->coordcd, 8);
  ds_image_info.coordcd[7] = '\0';

  bcopy((char *)&lastHead, (char *)&ds_image_info.head, sizeof(struct HEADER));

  ds_image_info.ra       = otf.manMapRa;
  ds_image_info.dec      = otf.manMapDec;
  ds_image_info.rms      = p1p->rms;
  ds_image_info.a        = p1p->a;
  ds_image_info.b        = p1p->b;
  ds_image_info.col      = otf.requestCol;
  ds_image_info.row      = otf.requestRow;
  ds_image_info.bscan    = 0;
  ds_image_info.escan    = 0;
  ds_image_info.noint    = otf.noint;
  ds_image_info.int_beg  = otf.int_chans[0];
  ds_image_info.int_end  = otf.int_chans[1];
  ds_image_info.freqres  = otf.head->freqres;
  ds_image_info.synfreq  = otf.head->synfreq;
  ds_image_info.sideband = otf.head->sideband;
  ds_image_info.freq     = otf.head->restfreq;
  ds_image_info.vel      = otf.head->refpt_vel;
  ds_image_info.refpt    = otf.head->refpt;
  ds_image_info.sb       = (int)otf.head->sideband;
  ds_image_info.date     = otf.head->utdate;
  ds_image_info.dv       = otf.head->deltax;
  ds_image_info.cal      = otf.head->tcal;
  ds_image_info.wt       = p1p->wt;
  ds_image_info.time     = p1p->time;
  ds_image_info.scanct   = 1;
  ds_image_info.tsys     = p1p->tsys;

  ds_image_info.head.inttime = p1p->time; /* Overwrite Header */
 
  if(otf.gridBaseLine)
  {
    for(k=0;k<otf.noint;k++)
    {
      ds_image_info.ydata[k] = p1p->chans[k] - ((float)k * p1p->a + p1p->b);
    }
  }
  else
  {
    for(k=0;k<otf.noint;k++)
    {
      ds_image_info.ydata[k] = p1p->chans[k];
    }
  }

  ds_image_info.doBaseline = otf.gridBaseLine;

  bcopy((char *)otf.baselines, (char *)ds_image_info.baselines, sizeof(ds_image_info.baselines));

  if(sockmode)
  {
    log_msg("Sending Copy of 'Spectral Plot Window to Linuxpops/Helper'.");

    sock_write(helper, (char *)&ds_image_info, sizeof(ds_image_info));
  }

  doSpecPlot(&ds_image_info);

  return(0);
}



/* Produce a 3d cube of data to be used in CloudCompare */
/* Args:

	bc:     beginning channel/velocity number
	ec:     Ending channel/velocity number
	interp: Number of artificial channels to interp between true channels
	vrange: Scale factor to multiple vel-range to get it to smooth better
	        in 3d cube.
	lowerThreashold:  only display cells with temp above this % of maxT.
 */


int do_otf3dcube(buf)
char *buf;
{
  int    row, col, l, m, bc, ec, interp, count=0, wroteOne = 0, zcnt;
  char  *cp, tmpStr[256], fname[256], zbuf[256];
  double x, y, z, vrange, lowerThres, zfactor = 1.0;
  double tmp, vel, refpt, deltax12;
  double accum1, vel1, intInten, velratio, velrange;
  double accum2, vel2, urms = 10.0, raSize, decSize;
  FILE  *fp, *pp;
  struct OTF_GRID *f;

  if(!otf.gridArray)
  {
    log_msg("do_otf3d(): OTF Array is empty");

    return(1);
  }

  strcpy(tmpStr, buf);

  cp = strtok(tmpStr," \n\t");
  if(cp)  
  {
    bc  = atoi(cp);
  }
  else
  {
    log_msg("do_otf3d(): Bad begin chan specifier in do_otf3d");

    return(1);
  }

  cp = strtok((char *)NULL," \n\t");
  if(cp)  
  {
    ec = atoi(cp);
  }
  else 
  {
    log_msg("do_otf3d(): Bad ending chan specifier in do_otf3d");

    return(1);
  }

  cp = strtok((char *)NULL," \n\t");
  if(cp)  
  {
    interp = atoi(cp);
  }
  else 
  {
    log_msg("do_otf3d(): Bad interp spacing in do_otf3d");

    return(1);
  }

  cp = strtok((char *)NULL," \n\t");
  if(cp)  
  {
    vrange = atof(cp);
  }
  else 
  {
    log_msg("do_otf3d(): Bad vel_range specifier in do_otf3d");

    return(1);
  }

  cp = strtok((char *)NULL," \n\t");
  if(cp)
  {
    lowerThres = atof(cp) * otf.valueMax;
  }
  else 
  {
    log_msg("do_otf3d(): Bad Lower Threshold specifier in do_otf3d");

    return(1);
  }

  cp = strtok((char *)NULL," \n\t");
  if(cp)
  {
    urms = atof(cp);
  }
  else 
  {
    log_msg("do_otf3d(): Bad Upper rms specifier in do_otf3d");

    return(1);
  }

  cp = strtok((char *)NULL," \n\t");
  if(cp)
  {
    zfactor = atof(cp);
  }
  else 
  {
    log_msg("do_otf3d(): Bad Z-Axis Factor specifier in do_otf3d");

    return(1);
  }

  if(fabs(zfactor) < 0.1) /* Come on Man! */
  {
    zfactor = 1.0;
  }

  if(lowerThres == 0.0 && otf.numCellsUsed > 500000)
  {
    log_msg("do_otf3d(): Bad Lower Threshold specifier with too many active celles: %d do_otf3d", otf.numCellsUsed);

    return(1);
  }

  if(ec-bc < 0 || ec>otf.noint || bc < 1 || vrange == 0.0 || interp < 1 || interp > 8)
  {
    log_msg("do_otf3d(): Bad parameters: %d [0 - <ec], %d [>bc - <noint-1], vrange: %.3f, %d [1 - 8]", 
					bc, ec, vrange, interp);

    return(1);
  }

	/* I use reduces because I can cross-mount it on my Mac without regard to permissions */
  strcpy(fname, "/home/reduced/otf-3d.pts"); 

  if( (fp = fopen(fname, "w") ) <= (FILE *)0) 
  {
    log_msg("do_otf3d(): Unable to open %s", fname);

    return(1);
  }

  createInterpTable(interp); /* Go create/update interp tables */

  refpt    = otf.head->refpt - 0.5;
  deltax12 = otf.head->deltax;
  vel      = otf.head->refpt_vel;

  if(otf.head->sideband == 3.0) 
  {
    vel1 = vel - (((double)bc - refpt) * deltax12) + deltax12 / 2.0;
    vel2 = vel - (((double)ec - refpt) * deltax12) - deltax12 / 2.0;
  }
  else 
  {
    vel1 = vel - (((double)bc - refpt) * -deltax12) - deltax12 / 2.0;
    vel2 = vel - (((double)ec - refpt) * -deltax12) + deltax12 / 2.0;
  }

  if(vel2 < vel1) /* Swap them */
  {
    tmp  = vel1;
    vel1 = vel2;
    vel2 = tmp;
  }

  velrange = vel2 - vel1;
  velratio = velrange * vrange; 

  raSize  = otf.nyquistRa  * (double)otf.cols;
  decSize = otf.nyquistDec * (double)otf.rows;

  log_msg("raSeize  = %f, decSeize  = %f", raSize, decSize);
  log_msg("velrange = %f, velration = %f, zfactor = %f", velrange, velratio, zfactor);

  /*          "5929.00 3297.00 1436.76    2.98" */
  fprintf(fp, "   X      Y       Z      Intensity    Tsys   rms\n");

  /* Throw up a progress bar */
  sprintf(zbuf, "zenity --progress --width=400 --height=200 --no-cancel --auto-close --text=\"Rendering 3D Cloud File\"");
  if( (pp = popen(zbuf, "w") ) <= (FILE *)0) 
  {
    log_msg("do_otf3d(): Unable to open %s", zbuf);
  }

  for(row=0;row<otf.rows;row++) 			/* Search all cells for valid data */
  {
    for(col=0;col<otf.cols;col++)
    {
      if(ds_delay(0.0))					/* Check if user requested to stop */
      {
        fclose(fp);

	if(pp)
	{
	  pclose(pp);
	}

        log_msg(" ");
        log_msg("Wrote %d cells to %s", count, fname);
        log_msg(" ");

        /* Force a redraw */
        otf.valueMaxChanged  = 1;
        otf.intenChanged     = 1;
        otf.newShade         = 1;
        otf.lockScale        = 0;

        xgr_clearScreen();

        paintOtfGrid();

        displayRequestedOtf();

        return(0);
      }

      f = fetchMapCell(row, col);

      if(!f) /* No data here */
      {
        continue;
      }

      if(f->set == -1) /* data ignored */
      {
        continue;
      }

      x = otf.nyquistRa  * ((double)col - (double)(otf.cols) * 0.5);
      y = otf.nyquistDec * ((double)row - (double)(otf.rows) * 0.5);

      for(l=bc;l<ec;l++)
      {
        if(otf.gridBaseLine)
        {
          accum1 = f->chans[l]   - ((double)l     * f->a + f->b);
          accum2 = f->chans[l+1] - ((double)(l+1) * f->a + f->b);
        }
        else
        {
          accum1 = f->chans[l];
          accum2 = f->chans[l+1];
        }

        if(accum1 < lowerThres && accum2 < lowerThres) /* No use going any further */
        {
          continue;
        }

        if(otf.head->sideband == 3.0) 
        {
          vel1 = vel - (((double)l     - refpt) *  deltax12) + deltax12 / 2.0;
          vel2 = vel - (((double)(l+1) - refpt) *  deltax12) + deltax12 / 2.0;
        }
        else 
        {
          vel1 = vel - (((double)l     - refpt) * -deltax12) - deltax12 / 2.0;
          vel2 = vel - (((double)(l+1) - refpt) * -deltax12) - deltax12 / 2.0;
        }

        for(m=0;m<interp;m++)
        {
          z        = findInterp(vel1,   vel2,   m, interp) * velratio;
          intInten = findInterp(accum1, accum2, m, interp);

          intInten *= otf.scaleFactor;

		/* Apply Gamma correction if cell > 0.0 */
	  if(intInten > 0.0)
          {
            intInten = (double)MAX_GRAY_SCALE * pow((intInten / (double)MAX_GRAY_SCALE), otf.gamma);
	  }

          if(intInten > lowerThres && f->rms < urms)
          {
            fprintf(fp, "%.4f %.4f %.4f %.4f %.4f %.4f\n", x, y, z * zfactor, intInten, f->tsys, f->rms);
	    wroteOne++;
            count++;
          }
        }
      }
    }

    if(wroteOne)
    {
      fflush(fp);
      log_msg("Wrote out Row: %d", row);

      wroteOne = 0;
    }

	/* Update the progress bar */
    if(pp)
    {
      zcnt = (int)(((double)row / (double)otf.rows) * 100.0);
      fprintf(pp, "%d\n", zcnt);
      fflush(pp);
    }
  }

  if(pp)
  {
    pclose(pp);
  }

  fclose(fp);

  log_msg(" ");
  log_msg("Wrote %d cells to %s", count, fname);
  log_msg(" ");

  /* Force a redraw */
  otf.valueMaxChanged  = 1;
  otf.intenChanged     = 1;
  otf.newShade         = 1;
  otf.lockScale        = 0;

  xgr_clearScreen();

  paintOtfGrid();

  displayRequestedOtf();

  return(0);
}


int do_otf3dplot(buf)
char *buf;
{
  int    row, col, count=0, interp = 0;
  char  *cp, fname[256], session[256];
  double x, y, z;
  FILE  *fp;
  struct OTF_GRID *f;

  if(!otf.gridArray)
  {
    log_msg("do_otf3dplot(): OTF Array is empty");

    return(1);
  }

  cp = strtok(buf, "\t\n ");

  if(!cp)
  {
    log_msg("do_otf3dplot(): Missing 'session' argument");

    return(1);
  }

  strcpy(session, cp);

  cp = strtok(NULL, "\t\n ");

  if(cp)
  {
    interp = atoi(cp);

    log_msg("do_otf3dplot(): Got Interp val: %d", interp);
  }

  strupr(session);

  sprintf(fname, "/tmp/%s/otf-3d-plot.dem", session);

  if( (fp = fopen(fname, "w") ) <= (FILE *)0) 
  {
    log_msg("do_otf3dplot(): Unable to open '%s'", fname);
    return(1);
  }

  fprintf(fp, "reset\n");
  fprintf(fp, "set title \"OTF Map\"\n");

  fprintf(fp, "set xlabel \"R.A. Offset\"\n");
  fprintf(fp, "set ylabel \"Dec. Offset\"\n");
  fprintf(fp, "set zlabel \"Intensity\"\n");

  fprintf(fp, "#\n");
  fprintf(fp, "set border 4095\n");
  fprintf(fp, "set hidden3d\n");
  fprintf(fp, "#\n");

  fprintf(fp, "set zrange  [%f:%f]\n", otf.valueMin, otf.valueMax);
//  fprintf(fp, "set zrange  [%f:%f]\n", 0.0, 10.0);
  fprintf(fp, "set cbrange [%f:%f]\n", otf.valueMin, otf.valueMax);
  fprintf(fp, "#\n");

  fprintf(fp, "set samples %d*%d\n", otf.cols, otf.rows);
  fprintf(fp, "set isosamples 60\n");

  fprintf(fp, "set ticslevel 0.1\n");

  if(interp)
  {
//    fprintf(fp, "set pm3d at bs interpolate %d,%d\n", interp, interp);
    fprintf(fp, "set pm3d interpolate %d,%d\n", interp, interp);
  }
  else
  {
//    fprintf(fp, "set pm3d at bs\n");
    fprintf(fp, "set pm3d\n");
  }

  fprintf(fp, "set palette color\n");
  fprintf(fp, "set palette model RGB\n");
  fprintf(fp, "set palette defined ( 0 \"blue\", 1 \"green\", 2 \"yellow\", 3 \"red\" )\n");
  fprintf(fp, "set palette gamma 1.0\n");
  fprintf(fp, "#\n");

  fprintf(fp, "set view 60.0, 30.0, 1.0, 1.0\n");

  fprintf(fp, "set terminal x11 background rgb \"black\" enhanced 1 size 1200, 850 position 800, 500\n");

  fprintf(fp, "set contour both\n");

  fprintf(fp, "set cntrparam levels incr %.2f,%.2f,%.2f\n", otf.valueMin, fabs(otf.valueMax-otf.valueMin) / 11.0, otf.valueMax);


  fprintf(fp, "splot \"/tmp/%s/otf-3d-plot.dat\" every 1::0::1024 with lines\n", session);

  fclose(fp);

  /* Write the data */
  sprintf(fname, "/tmp/%s/otf-3d-plot.dat", session);

  if( (fp = fopen(fname, "w") ) <= (FILE *)0) 
  {
    log_msg("do_otf3dplot(): Unable to open '%s'", fname);
    return(1);
  }

  for(row=0;row<otf.rows;row++)                       /* Search all cells for valid data */
  {
    for(col=0;col<otf.cols;col++)
    {
      f = fetchMapCell(row, col);

      if(f && f->set > 0)
      {
        x = otf.nyquistRa  * ((float)col - (float)(otf.cols) * 0.5);
        y = otf.nyquistDec * ((float)row - (float)(otf.rows) * 0.5);

   /* If No data here; gnuplot doesn like missing data so write zeros instead */
        if(f && f->set == 1) 
        {
          z = f->plotVal1;
        }
        else
        {
          z = 0.0;
        }

        fprintf(fp, "%.2f %.2f %.2f\n", x, y, z);

        count++;
      }
    }

    fprintf(fp, " \n");
  }


  fclose(fp);

  log_msg("do_otf3dplot(); Wrote %d cells to %s", count, fname);

  sprintf(fname, "xgraphic load \"/tmp/%s/otf-3d-plot.dem\"", session);

  to_xgraphic(fname);

  return(0);
}


/*
  shade = 254, 
  oldshade = 254, 
  set = 1, 
  col = 517, 
  row = 373, 
  scanct = 3, 
  intInten = 13.5080051, 
  kms = 13.5080051, 
  rms = 13.5080051, 
  chans = 0x18aac90, 
  a = -0.0395153947, 
  b = -6.85798168, 
  bscan = 50.0400009, 
  escan = 267.040009, 
  time = 1.29999995, 
  tsys = 212.764191, 
  wt = 2.87174771e-05, 
  source = "CO21_CygX_097  ", '\000' <repeats 16 times>, 
  head = 0x18a5038, 
  next = 0x18ab4a0
*/

/* Zero out any cell whos rms is greater than passwd arg */
int do_otfRmRMS(buf)
char *buf;
{
  int    row, col, k, count=0;
  char  *cp, fname[256];
  double rmsLimit;
  FILE  *fp;
  struct OTF_GRID *f;

  if(!otf.gridArray)
  {
    log_msg("do_otf3dplot(): OTF Array is empty");

    return(1);
  }

  cp = strtok(buf, "\t\n ");

  if(!cp)
  {
    log_msg("do_otfRmRMS(): Missing 'rms limit' argument");

    return(1);
  }

  rmsLimit = atof(cp);

  if(rmsLimit < 0.1)
  {
    log_msg("do_otfRmRMS(): 'rmslimit' argument %f, unresonable");

    return(1);
  }

  /* open file to write bad info */
  sprintf(fname, "bad_rms_cells.txt");

  if( (fp = fopen(fname, "w") ) <= (FILE *)0) 
  {
    log_msg("do_otfRmRMS(): Unable to open '%s'", fname);
    return(1);
  }

  for(row=0;row<otf.rows;row++)                       /* Search all cells for valid data */
  {
    for(col=0;col<otf.cols;col++)
    {
      f = fetchMapCell(row, col);

      if(!f) 			/* If No data here */
      {
        continue;
      }

      if(f->rms > rmsLimit)
      {
        fprintf(fp, "row: %4d, col: %4d, rms: %8.4f, bscan: %8.2f, escan %8.2f ", row, col, f->rms, f->bscan, f->escan);

        for(k=0;k<8;k++)
        {
          if(f->version[k])
          {
            fprintf(fp, "%3d ", f->version[k]);

          }
        }

        fprintf(fp, "\n");

        f->set       = -1;  /* Mark as no longer used; but allocated */
        f->shade     = 0; 
        f->oldshade  = 0; 
        f->col       = 0; 
        f->row       = 0; 
        f->scanct    = 0; 
        f->intInten  = 0.0; 
        f->kms       = 0.0; 
        f->rms       = 0.0; 
        f->a         = 0.0; 
        f->b         = 0.0; 
        f->bscan     = 0.0; 
        f->escan     = 0.0; 
        f->time      = 0.0; 
        f->tsys      = 0.0; 
        f->wt        = 0.0;
        f->source[0] = '\0';

        count++;
      }
    }
  }

  fclose(fp);

  log_msg("do_otfRmRMS(); Wrote %d cells to %s", count, fname);

  forceDrawGrid();

  return(0);
}


/* Zero out passed cell @ col and row*/
int do_zapCell(buf)
char *buf;
{
  int    row, col;
  char  *cp;
  struct OTF_GRID *f;

  if(!otf.gridArray)
  {
    log_msg("do_otf3dplot(): OTF Array is empty");

    return(1);
  }

  cp = strtok(buf, "\t\n ");

  if(!cp)
  {
    log_msg("do_zapCell(): Missing 'Col' argument");

    return(1);
  }
  col = atoi(cp);

  cp = strtok(NULL, "\t\n ");

  if(!cp)
  {
    log_msg("do_zapCell(): Missing 'Row' argument");

    return(1);
  }
  row = atoi(cp);

  if(col < 0 || col > otf.cols-1 || row < 0 || row > otf.rows-1)
  {
    log_msg("do_zapCell(): Col or Row out of limits: [0 - %d] [0 - %d]", otf.cols, otf.rows);

    return(1);
  }

  f = fetchMapCell(row, col);

  if(!f) 			/* If No data here */
  {
    log_msg("No data in cell: %d x %d", col, row);
    return(1);
  }

  f->set       = -1;  /* Mark as no longer used; but allocated */
  f->shade     = 0; 
  f->oldshade  = 0; 
  f->col       = 0; 
  f->row       = 0; 
  f->scanct    = 0; 
  f->intInten  = 0.0; 
  f->kms       = 0.0; 
  f->rms       = 0.0; 
  f->a         = 0.0; 
  f->b         = 0.0; 
  f->bscan     = 0.0; 
  f->escan     = 0.0; 
  f->time      = 0.0; 
  f->tsys      = 0.0; 
  f->wt        = 0.0;
  f->source[0] = '\0';

  log_msg("do_zapCell(); Cleared Cell: Col: %d x Row: %d", col, row);

  forceDrawGrid();

  return(0);
}



#define FIRST_FFB	1
#define SECOND_FFB	2
#define THIRD_FFB	3
#define FOURTH_FFB	4
#define FIFTH_FFB	5
#define SIXTH_FFB	6
#define SEVENTH_FFB	7
#define EIGHTH_FFB	8



struct OTF_GRID *fetchMapCell(row, col)
int row, col;
{
  if((row < otf.rows) && (col < otf.cols))
  {
    return((struct OTF_GRID *)otf.gridArray[(row*otf.cols) + col]);
  }

  return((struct OTF_GRID *)NULL);
}



int setMapCell(row, col, f)
int row, col;
struct OTF_GRID *f;
{
  if(row <otf.rows && col < otf.cols)
  {
    otf.gridArray[(row*otf.cols) + col] = f;

    return(0);
  }

  return(1);
}


#define MAX_GAP 5

int findNextCell(row, col, p2)
int row, col;
struct OTF_GRID **p2;
{
  int r;
  struct OTF_GRID *f;

  for(r=1;r<MAX_GAP;r++)
  {
    f = fetchMapCell(row+r, col);

    if(f && f->set == 1)
    {
      *p2 = f;

      return(r);
    }
  }

  return(0);
}



/* Gaps are usually caused by ignored rows; interpolate top to bottom */
/* An attempt to fill in the large gaps in a map;  limited to MAX_GAP (5) holes */
int do_fullInterpFill(buf)
char *buf;
{
  int    i, row, col, n, count = 0;
  double p1_avg, p2_avg;
  struct OTF_GRID *f;
  struct OTF_GRID *p1, *p2;

  for(col=2;col<otf.cols-2;col++) 		/* Stay away from edges */
  {
    p1 = (struct OTF_GRID *)NULL;

    for(row=2;row<otf.rows-2;row++)		/* ditto top & bottom */
    {
      f = fetchMapCell(row, col);

      if(f && f->set == 1) /* There is already valid data here */
      {
        p1 = f; /* Remember it as last valid cell */

        continue;
      }

      if(!p1) /* We have to have hit at least one valid cell in each col first */
      {
        continue;
      }

	/* this cell empty; find the next valid one; if any */

      n = findNextCell(row, col, &p2);

      if(n > 0)
      {
        log_msg("Col: %4d, Row: %4d missing, found next valid cell: %d %d, Gap: %d", col, row, p2->col, p2->row, n);

	/* Fill in missing cells using a weigthed interpolation between p1 & p2 */

	createInterpTable(n+1);

	/* fix this one first */
	p1_avg = 0.5 + v1a[1];
	p2_avg = 0.5 + v2a[1];
	log_msg("Averages: %f %f", p1_avg, p2_avg);
	weightedAvergeCells(f, p1, p1_avg, p2, p2_avg, 0, row, col, &count);	/* give each their weight */

	/* fix all others */
        for(i=1;i<n;i++)
	{
          f = fetchMapCell(row+i, col);						/* fetch next empty cell */

	  p1_avg = 0.5 + v1a[1+i];
	  p2_avg = 0.5 + v2a[1+i];
	  log_msg("Averages: %f %f", p1_avg, p2_avg);

	  weightedAvergeCells(f, p1, p1_avg, p2, p2_avg, 0, row, col, &count);	/* give each their weight */
        }

        p1 = (struct OTF_GRID *)NULL;
        p2 = (struct OTF_GRID *)NULL;

        row += n;	/* go ahead and jump to next row */
      }
      else	/* We have traveled more that MAX_GAP and haven't found anything; forget p1 */
      {
        p1 = (struct OTF_GRID *)NULL;
      }

      /* search for next valid cell vertically */
    }
  }

  forceDrawGrid();

  log_msg("do_fullInterpFill(): Repaired %d cells", count);

  if(count)
  {
    do_fullInterpFill(buf); /* Do it again until count == 0 */
  }

  return(0);
}



/* Given 2 valid cells, average them together using 
   their integrated weights and the passed weights;
   place results in the OTF_CELL pointed to by 'f'

   Malloc channels and the OTF_CELL itself if need be
 */
int weightedAvergeCells(f, p1, p1_avg, p2, p2_avg, try, row, col, count)
struct OTF_GRID *f, *p1, *p2;
double p1_avg, p2_avg;
int    try, row, col, *count;
{
  int    chan;
  float *gc, *p1c, *p2c;

  if( p1->set == 1 && p2->set == 1)		/* Both are good cells */
  {
    if(p1->wt && p2->wt)			/* Both have data to average */
    {
      if(!f)					/* did we already fix this on previous 'try' loop? */
      {
          			       		/* malloc space for grid array */
	if((f = (struct OTF_GRID *)malloc(sizeof(struct OTF_GRID))) == NULL)
	{
	  log_msg("Err FixBadCells(malloc(%d))", sizeof(struct OTF_GRID));
	  return(1);
	}

	bzero((char *)f, sizeof(struct OTF_GRID));

	setMapCell(row, col, f);

	gridLast = gridLast->next = f;		/* Add to master linked list */
						/* malloc space for channels */
	if((f->chans = (float *)malloc(otf.noint * sizeof(float))) == NULL)
	{
          log_msg("Err in FixBadCells(malloc(%d))", otf.noint * sizeof(float));
          return(1);
	}
      }

      bzero((char *)f->chans, otf.noint * sizeof( float) );

      *count = *count +1;

      gc  = f->chans;				/* Average in the first cell */
      p1c = p1->chans;

      for(chan=0;chan<otf.noint;chan++, p1c++, gc++)
      {
        *gc = (*gc * f->wt + (*p1c * p1->wt * p1_avg)) / (f->wt + (p1->wt * p1_avg));
      }

      f->wt += (p1->wt * p1_avg);

      gc  = f->chans;				/* Average in the second cell */
      p2c = p2->chans;

      for(chan=0;chan<otf.noint;chan++, p2c++, gc++)
      {
        *gc = (*gc * f->wt + (*p2c * p2->wt * p2_avg)) / (f->wt + (p2->wt * p2_avg));
      }

      f->wt += (p2->wt * p2_avg);

      f->intInten = 0.0;
      f->set      = 1;
      f->scanct   = p1->scanct + p2->scanct;
      f->time     = (p1->time * p1_avg) + (p2->time * p2_avg);

      f->tsys     = sqrt(1.0 / (f->wt / f->time));

      if(debug)
      {
	log_msg("%d: Setting Col: %3d, Row: %3d Tsys to: %7.1f, from %.1f %.1f", try, col, row, f->tsys, p1->tsys, p2->tsys);
      }

      f->bscan    = p2->bscan;
      f->escan    = p2->escan;

      f->artificial = 1;
      f->row = row;
      f->col = col;

      f->bdirty = 1;		/* Mark baselines dirty */
      f->ddirty = 1;		/* Mark data dirty */

      otf.bdirty = 1;
      otf.ddirty = 1;

      strcpy(f->source, p1->source);

      computeBaseLine(f, 0, 0, 0, 0, 0, 0);
    }
    else
    {
      log_msg("do_otfFixBadCells(): p1->wt or p2->wt == 0; %f %f; skipping", p1->wt, p2->wt);
    }
  }
  else
  {
    log_msg("do_otfFixBadCells(): p1->set or p2->set == 0; %d %d; skipping", p1->set, p2->set);
  }

  return(0);
}


/* Create an artificial cell based on the two ajacent cells, 
   either to the (left and right) or (above and below) 
 */
int do_otfFixBadCells(buf)
char *buf;
{
  int row, col, try, count = 0;
  struct OTF_GRID *p1, *p2;
  struct OTF_GRID *f;

  if(!otf.gridArray)
  {
    log_msg("FIXBADCELLS: OTF Array is empty");
    return(1);
  }

  if(strstr(buf, "interp"))
  {
    do_fullInterpFill(buf);

    return(0);
  }

  for(row=1;row<otf.rows-1;row++)
  {
    for(col=1;col<otf.cols-1;col++)
    {
      f = fetchMapCell(row, col);

      if(f && f->set == 1) /* There is already valid data here */
      {
        continue;
      }

	/* OK, this cell is empty; is there data to the left & right; or above or below ?? */

      for(try=0;try<2;try++) /* Try side by side; then above and below */
      {
	if(try == 0)
	{
          p1 = fetchMapCell(row, col-1);
          p2 = fetchMapCell(row, col+1);
	}
	else
	{
          p1 = fetchMapCell(row-1, col);
          p2 = fetchMapCell(row+1, col);
	}

	/* Is there two valid cells worth of data ??? */
        if(p1 && p2)
        {
	  weightedAvergeCells(f, p1, 1.0, p2, 1.0, try, row, col, &count);	/* give each equal weight */
	}
      }
    }
  }

  forceDrawGrid();

  log_msg("OTFFIXBADCELLS: Repaired %d cells", count);

  if(count)
  {
    do_otfFixBadCells(buf); /* Do it again until count == 0 */
  }

  return(0);
}


int do_otfListCells(buf)
char *buf;
{
  int row, col;
  struct OTF_GRID *f;

  if(!otf.gridArray)
  {
    log_msg("do_otfListCells(): OTF Array is empty");

    return(1);
  }

  for(row=1;row<otf.rows-1;row++)
  {
    for(col=1;col<otf.cols-1;col++)
    {
      f = fetchMapCell(row, col);

      if(f) 			/* There's data here */
      {
	log_msg("Col: %4d, Row: %4d, ScanCt: %.1f, Tsys: %.1f, Wt: %.2e", col, row, f->scanct, f->tsys, f->wt);
      }
    }
  }
 
  return(0);
}



int forceDrawGrid()
{
  if(otf.ignoreAllRepaints)
  {
    log_msg("forceDrawGrid(): ignoreAllRepaints set");

    return(0);
  }

  log_msg("forceDrawGrid(): Enter");

  otf.intenChanged    = 1; 

  computeInten(otf.int_chans[0], otf.int_chans[1]);

  otf.valueMaxChanged = 1; 

  computeMinMax();

  otf.newShade        = 1; 

  computeShade();

  paintOtfGrid();
 
  return(0);
}


int do_otfSlice(buf)
char *buf;
{
  int    j, row, col, first = 1, ret, scanct=0, dir;
  double raOff, decOff, bscan= 999999.9, escan = -999999.9;
  char  *cp, fname[256], source[256];
  struct OTF_GRID *f;
  FILE  *fp = NULL;

  if(!otf.gridArray)
  {
    log_msg("do_otfListCells(): OTF Array is empty");

    return(1);
  }

  j = 0;
  if((cp = strstr(buf, "row")))
  {
    dir = 1;

    row    = atoi(cp+4);

    for(col=1;col<otf.cols-1;col++)
    {
      f = fetchMapCell(row, col);

      if(f && f->set > 0) 			/* There's data here */
      {
        raOff  = otf.nyquistRa  * ((double)col - (double)(otf.cols / 2.0));
        raOff /= 3600.0;
        raOff /= (15.0 * cos(otf.head->ysource * CTR));
	raOff *= 3600.0; /* Convert to seconds of time */
	raOff *= 15.0;   /* Convert to arc Seconds */

        if(first)
        {
          ret = getRequestedCell(row, col); /* Do this, because it will populate the image_info struct */

          if(!ret)
	  {
	    log_msg("Got the row header");
	  }
	  else
	  {
	    log_msg("Could NOT get the row header");
	  }

	  strcpy(source, f->source);
    	  strcmprs(source); 
    	  sprintf(fname, "%s-Row-%d.otfdata", source, row);

	  if((fp = fopen(fname, "w")) <= (FILE *)NULL)
          {
	    log_msg("Unable to open: %s", fname);
	    return(1);
	  }

	  first = 0;
	}

	fprintf(fp, "%e %e\n", raOff, f->intInten);

	ds_image_info.ydata[j] = f->intInten;

	/* Check for bscan, escan and scanct extents */
        if(f->bscan > 0 && f->bscan < bscan)
        {
          bscan = f->bscan;
        }
        if(f->escan > 0 && f->escan > escan)
        {
          escan = f->escan;
        }
	if(f->scanct > scanct)
        {
	  scanct = f->scanct;
	}

	j++;
      }
    }
  }
  else
  if((cp = strstr(buf, "col")))
  {
    dir = 2;

    col    = atoi(cp+4);

    for(row=1;row<otf.rows-1;row++)
    {
      f = fetchMapCell(row, col);

      if(f && f->set > 0) 			/* There's data here */
      {
        decOff = otf.nyquistDec * ((double)row - (double)(otf.rows / 2.0));

        if(first)
        {
          ret = getRequestedCell(row, col); /* Do this, because it will populate the image_info struct */

          if(!ret)
	  {
	    log_msg("Got the row header");
	  }
	  else
	  {
	    log_msg("Could NOT get the row header");
	  }

	  strcpy(source, f->source);
    	  strcmprs(source); 
    	  sprintf(fname, "%s-Col-%d.otfdata", source, col);

	  if((fp = fopen(fname, "w")) <= (FILE *)NULL)
          {
	    log_msg("Unable to open: %s", fname);
	    return(1);
	  }

	  first = 0;
	}

	fprintf(fp, "%e %e\n", decOff, f->intInten);

	ds_image_info.ydata[j] = f->intInten;

	/* Check for bscan, escan and scanct extents */
        if(f->bscan > 0 && f->bscan < bscan)
        {
          bscan = f->bscan;
        }
        if(f->escan > 0 && f->escan > escan)
        {
          escan = f->escan;
        }
	if(f->scanct > scanct)
        {
	  scanct = f->scanct;
	}

	j++;
      }
    }
  }
  else
  {
    log_msg("Wrong Args to otfslice");
    return(1);
  }

  if(fp)
  {

    ds_image_info.head.noint     = (double)j;
    ds_image_info.head.refpt     = (double)j / 2.0;

    if(dir == 1)
    {
      ds_image_info.head.deltax    = otf.nyquistRa / cos(otf.manMapDec * M_PI / 180.0); /* Increase by cos(dec) */
    }
    else
    {
      ds_image_info.head.deltax    = otf.nyquistDec;
    }

    ds_image_info.head.velocity  = 0.0;
    ds_image_info.head.refpt_vel = 0.0;
    ds_image_info.bscan          = bscan;
    ds_image_info.escan          = escan;
    ds_image_info.scanct         = scanct;
    ds_image_info.noint          = j;
    ds_image_info.refpt          = (double)j / 2.0;

    log_msg("Sending Copy of 'Spectral Plot Window to Linuxpops/Helper'.");

    sock_write(helper, (char *)&ds_image_info, sizeof(ds_image_info));

    log_msg("Created File: %s", fname);

    fclose(fp);
  }

  return(0);
}


int do_otfStatus()
{
  int    tScans = 0;
  char   raStr[256], decStr[256];
  struct MAP_SCANS *m;
  extern int log_dup2stdout;

  if(otf.map_scans_head != NULL)
  {
    for(m=otf.map_scans_head;m->next; m=m->next)
    {
      if(m)
      {
        tScans++;
      }
    }

    markDataDirty();		/* Force a recalculation of intensities */

    computeInten(otf.int_chans[0], otf.int_chans[1]);

    computeMinMax();
  }
  else
  {
    log_msg("No Scans Loaded");

//    otf.valueMax  = 0.0;
//    otf.valueMaxX = 0;
//    otf.valueMaxY = 0;

//    otf.valueMin  = 0.0;
//    otf.valueMinX = 0;
//    otf.valueMinY = 0;

//    otf.manMapRa  = 0.0;
//    otf.manMapDec = 0.0;

//    otf.restfreq  = 0.0;
    
//    otf.cols = 0;
//    otf.rows = 0;
  }

  sexagesimal(otf.manMapRa/15.0, raStr, DD_MM_SS);
  sexagesimal(otf.manMapDec,    decStr, DD_MM_SS);

  log_dup2stdout = 1;

  log_msg("OTF: Map Info:");

  log_msg("OTF: Map Coordinates: RA: %s;  Dec: %s",               raStr, decStr);
  log_msg("OTF: Map Cell Size:   RA: %.2f Sec;  Dec: %.2f Sec",   otf.manNyquistRa, otf.manNyquistDec);
  log_msg("OTF: Map Cols: %4d; Rows: %4d",                        otf.cols, otf.rows);
  log_msg("OTF: Map Rest Freq: %.6f",                             otf.restfreq / 1000.0);
  log_msg("OTF: Backend Resolution: %.3f MHz; noint: %d",         fabs(otf.freqres), otf.noint);

  log_msg("OTF: Number of Occupied Cells: %d out of %d",          otf.numCellsUsed, otf.numCellsTotal);
  log_msg("OTF: Number of Bogus 0, 0 cells: %d",                  zeroCellCount);
  log_msg("OTF: Number of Artificial cells: %d",                  artificialCells);
  log_msg("OTF: Total Number of OTF Map Scans: %d",               tScans);
  log_msg("OTF: Map Max Value: %10.6f @ Col: %d, Row: %d",        otf.valueMax, otf.valueMaxX, otf.valueMaxY);
  log_msg("OTF: Map Min Value: %10.6f @ Col: %d, Row: %d",        otf.valueMin, otf.valueMinX, otf.valueMinY);

  log_msg("OTF: Map Scale Factor: %.2f;  Gamma Correction: %.2f", otf.scaleFactor, otf.gamma);

  log_msg("OTF: initialized        = %d", otf.initialized);
  log_msg("OTF: ignoreAllRepaints  = %d", otf.ignoreAllRepaints);
  log_msg("OTF: cutoff             = %d", otf.cutoff);
  log_msg("OTF: fixBadChan         = %d", otf.fixBadChan);
  log_msg("OTF: bins               = %d", otf.bins);
  log_msg("OTF: plots              = %d", otf.plots);
  log_msg("OTF: singleChan         = %d", otf.singleChan);
  log_msg("OTF: arrayNum           = %d", otf.arrayNum);
  log_msg("OTF: singleOtfBin       = %d", otf.singleOtfBin);
  log_msg("OTF: debugSpectra       = %d", otf.debugSpectra);
  log_msg("OTF: reqSpecIndx        = %d", otf.reqSpecIndx);
  log_msg("OTF: noPlot             = %d", otf.noPlot);
  log_msg("OTF: newShade           = %d", otf.newShade);
  log_msg("OTF: requestCol         = %d", otf.requestCol);
  log_msg("OTF: requestRow         = %d", otf.requestRow);
  log_msg("OTF: col                = %d", otf.col);
  log_msg("OTF: row                = %d", otf.row);
  log_msg("OTF: cols               = %d", otf.cols);
  log_msg("OTF: rows               = %d", otf.rows);
  log_msg("OTF: numCellsTotal      = %d", otf.numCellsTotal);
  log_msg("OTF: numCellsUsed       = %d", otf.numCellsUsed);
  log_msg("OTF: noint              = %d", otf.noint);
  log_msg("OTF: numSpectra         = %d", otf.numSpectra);
  log_msg("OTF: int_chans[2]       = %d - %d", otf.int_chans[0], otf.int_chans[1]);
  log_msg("OTF: lockScale          = %d", otf.lockScale);
  log_msg("OTF: valueMaxChanged    = %d", otf.valueMaxChanged);
  log_msg("OTF: intenChanged       = %d", otf.intenChanged);
  log_msg("OTF: loadChoice         = %d", otf.loadChoice);
  log_msg("OTF: lastAction         = %d", otf.lastAction);
  log_msg("OTF: noColorBar         = %d", otf.noColorBar);
  log_msg("OTF: colorChoice        = %d", otf.colorChoice);
  log_msg("OTF: chanSelOTF         = %d", otf.chanSelOTF);
  log_msg("OTF: gridBaseLine       = %d", otf.gridBaseLine);
  log_msg("OTF: windowSizeSet      = %d", otf.windowSizeSet);
  log_msg("OTF: placeBeamSize      = %d", otf.placeBeamSize);
  log_msg("OTF: regionshow         = %d", otf.regionshow);
  log_msg("OTF: regionshowbold     = %d", otf.regionshowbold);
  log_msg("OTF: allowVelShift      = %d", otf.allowVelShift);
  log_msg("OTF: whichPlot          = %d", otf.whichPlot);
  log_msg("OTF: sizeofWorkArray    = %d", otf.sizeofWorkArray);
  log_msg("OTF: baselines[8]       = %d %d %d %d %d %d %d %d",
					otf.baselines[0], otf.baselines[1],
					otf.baselines[2], otf.baselines[3],
					otf.baselines[4], otf.baselines[5],
					otf.baselines[6], otf.baselines[7]);

  log_msg("OTF: scaleFactor        = %f", otf.scaleFactor);
  log_msg("OTF: gamma              = %f", otf.gamma);
  log_msg("OTF: IFNumber           = %f", otf.IFNumber);
  log_msg("OTF: nyquistRa          = %f", otf.nyquistRa);
  log_msg("OTF: nyquistDec         = %f", otf.nyquistDec);
  log_msg("OTF: valueMin           = %f", otf.valueMin);
  log_msg("OTF: valueMax           = %f", otf.valueMax);
  log_msg("OTF: plotxb             = %f", otf.plotxb);
  log_msg("OTF: plotyb             = %f", otf.plotyb);
  log_msg("OTF: plotxe             = %f", otf.plotxe);
  log_msg("OTF: plotye             = %f", otf.plotye);
  log_msg("OTF: manMapRa           = %f", otf.manMapRa);
  log_msg("OTF: manMapDec          = %f", otf.manMapDec);
  log_msg("OTF: manMapWidth        = %f", otf.manMapWidth);
  log_msg("OTF: manMapHeight       = %f", otf.manMapHeight);
  log_msg("OTF: manNyquistRa       = %f", otf.manNyquistRa);
  log_msg("OTF: manNyquistDec      = %f", otf.manNyquistDec);
  log_msg("OTF: chanCompress       = %f", otf.chanCompress);
  log_msg("OTF: freqres            = %f", otf.freqres);
  log_msg("OTF: restfreq           = %f", otf.restfreq);
  log_msg("OTF: velocity           = %f", otf.velocity);
  log_msg("OTF: xsource            = %f", otf.xsource);
  log_msg("OTF: ysource            = %f", otf.ysource);
  log_msg("OTF: beamRa             = %f", otf.beamRa);
  log_msg("OTF: beamDec            = %f", otf.beamDec);

  log_msg("OTF: ");

  log_dup2stdout = 0;

  return(0);
}



int interpAndWrite(s)
struct XGR_ *s;
{
  int i, row, col, k;
  int in, out, cols, rows;
  float *inarray=NULL, *outarray=NULL;
  char fname[256];
  FILE *fp;
  struct OTF_GRID *f;

  if(!otf.gridArray)
  {
    log_msg("interpAndWrite(): OTF Array is empty");
    return(1);
  }

  sprintf(fname, "/tmp/dataserv-grid.%d.dat", getpid());

  if((fp = fopen(fname, "w")) == NULL)
  {
    log_msg("interpAndWrite(): Unable to open %s", fname);
    return(1);
  }

  in  = otf.cols * otf.rows;
  cols = otf.cols * 2;
  rows = otf.rows * 2;

  out = cols * rows;

  if( (inarray = (float *)malloc((unsigned)(in * sizeof(float)))) == NULL)
  {
    log_msg("interpAndWrite(): Unable to malloc(%d) inarray", in * sizeof(float));

    return(1);
  }

  if( (outarray = (float *)malloc((unsigned)(out * sizeof(float)))) == NULL)
  {
    log_msg("interpAndWrite(): Unable to malloc(%d) outarray", in * sizeof(float));

    free((char *)inarray);

    return(1);
  }

  bzero((char *)inarray,  in  * sizeof(float));
  bzero((char *)outarray, out * sizeof(float));

  k = 0;
  for(row=otf.rows-1;row>=0;row--)
  {
    for(col=0;col<otf.cols;col++)
    {
      f = fetchMapCell(row, col);

      if(f)
      {
        inarray[k++] = (float)f->shade;
      }
      else
      {
        inarray[k++] = 0.0;
      }
    }
  }

  interp(inarray, otf.cols, otf.rows, outarray, cols, rows);

  i = 0;
  for(row=0;row<rows;row++)
  {
    for(col=0;col<cols;col++,i++)
    {
      fprintf(fp, "%d %d %f\n", row, col, (float)outarray[i]);
    }

    fprintf(fp, "\n");
  }

  fclose(fp);

  if(inarray)
  {
    free((char *)inarray);
  }

  if(outarray)
  {
    free((char *)outarray);
  }

  return(0);
}



static float sqrarg;

#define SQR(a) (sqrarg=(a),sqrarg*sqrarg)

int lsqfit(x, y, bdata, ndata, sig, mwt, a, b, siga, sigb, chi2, q)
float x[],y[],sig[],*a,*b,*siga,*sigb,*chi2,*q;
int bdata,ndata,mwt;
{
  int i;
  float wt, t, sxoss, sx = 0.0, sy = 0.0, st2 = 0.0, ss, sigdat;

  do_etime(LSQFIT, DSTIME_START);

  *b=0.0;

  if(mwt)
  {
    ss=0.0;

    for(i=bdata;i<ndata;i++)
    {
      wt  = 1.0 / SQR(sig[i]);
      ss += wt;
      sx += x[i] * wt;
      sy += y[i] * wt;
    }
  }
  else 
  {
    for(i=bdata;i<ndata;i++) 
    {
      sx += x[i];
      sy += y[i];
    }

    ss=ndata-bdata;
  }

  sxoss = sx / ss;

  if(mwt) 
  {
    for(i=bdata;i<ndata;i++) 
    {
      t    = (x[i] - sxoss) / sig[i];
      st2 += t * t;
      *b  += t * y[i] / sig[i];
    }
  }
  else 
  {
    for(i=bdata;i<ndata;i++) 
    {
      t    = x[i] - sxoss;
      st2 += t * t;
      *b  += t * y[i];
    }
  }

  *b   /= st2;
  *a    = (sy - sx * (*b)) / ss;
  *siga = sqrt((1.0 + sx * sx / (ss * st2)) / ss);
  *sigb = sqrt(1.0 / st2);
  *chi2 = 0.0;

  if(mwt == 0) 
  {
    for(i=bdata;i<ndata;i++)
    {
      *chi2 += SQR(y[i] - (*a) - (*b) * x[i]);
    }

    *q     = 1.0;
    sigdat = sqrt((*chi2) / (ndata - bdata - 2));
    *siga *= sigdat;
    *sigb *= sigdat;
  }

  do_etime(LSQFIT, DSTIME_STOP);

  return(0);
}

#undef SQR
