#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <math.h>

#include "caclib_pt_proto.h"


void *threadProc(void *id)
{
  int *tid, len, sel;
  char sname[80];
  char message[4096*8];
  struct BOUND *bs;

  tid = (int*)id;

  sprintf(sname, "OTF_RECEIVER_%02d", *tid + 1);
  fprintf(stdout, "Using sockname: %s\n", sname);

  bs = sock_bind(sname);

  sock_bufct(bs, sizeof(message));

  fprintf(stdout, "thread_proc(): Thread ID:%d, Started\n", *tid);

  while(1)
  {
    sel = sock_sel(bs, message, &len, NULL, 0, 60, 0);

    if(sel == -1)
    {
      fprintf(stdout, "thread_proc(%d): Standing By\n", *tid);
    }
    if(sel > 0)
    {
      deNewLineIt(message);
      fprintf(stdout, "thread_proc(%d): received: %s\n", *tid, message);
    } 
    else
    {
      fprintf(stdout, "thread_proc(%d): sock_sel() returned: %d\n", *tid, sel);
    }
  }

  fprintf(stdout, "thread_proc(): Thread ID:%d, Ended\n", *tid);

  return(0);
}


int main(argc, argv)
int argc;
char *argv[];
{
  int i, rc, id[8];
  pthread_t tid[8];

  for(i=0;i<8;i++)
  {
    id[i] = i;
    if ((rc = pthread_create(&tid[i], NULL, threadProc, (void *)&id[i])))
    {
      fprintf(stderr, "main(): error: pthread_create for computeInten, rc: %d\n", rc);
      return(3);
    } 

    usleep(10000);
  }

  pthread_join(tid[7], NULL);

  fprintf(stdout, "main(): Last thread ended\n");

  usleep(100000);

  return(0);
}
