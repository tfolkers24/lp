#include "ds_extern.h"

#include "dataserv.bmp"

#include "sp_proto.h"

#define ROW0            (0.96)
#define ROW1            (0.92)
#define ROW2            (0.88)
#define ROW3            (0.84)
#define DATASPREAD      (0.000001)

int debug          = 0;
int zerodata       = 0;
int globalUsPixmap = 1;

int (*sock_callback)() = NULL;
char inMsg[16384];

static int font0 = FONT_SIZE_SMALL;

float  startRes, scanNum, datamax, datamin, globalStart, globalStop;
struct DS_IMAGE_INFO last_image;

char *months[] = { "Jan","Feb","Mar","Apr","May","Jun",
                   "Jul","Aug","Sep","Oct","Nov","Dec" };

extern int lockScale;
extern struct XGR_HEAD xgr_head;
struct XGR_ *server;

extern int insque(), remque();

char deadServer[256], prgName[256], localHost[256], tfile[256],
     title[256], mainFrameTitle[256];
static char *spaces = "                                                                     ";

struct SOCK *dataserv, *helper;

  /* DO NOT FORGET TO ADD A HANDLER FOR ANY EXTRA MASKS ADDED, processEvent() */
long frame_mask = KeyPressMask | ExposureMask | StructureNotifyMask;

struct OTF_STRUCT *myotf;

struct MAIL_PARSE {
        char *msg;
        int   log;
        int   save_config;
        int   count;
        int   type;
        char  *format;
        void  *ptr;
        int   len;
        int   (*fctn)();
        char *help;
};


struct MAIL_PARSE parse_table[] = {
 /* Commands that change internal variables and are saved to dataserv.conf */
/*   msg,   log, save, cnt,   type,   fmt,  *ptr,  len,     fctn(),   *help */
  { "help",  0,     0,   0,      0,  NULL,  NULL,    4,    do_help,   "Displays this list"},
  { "quit",  0,     0,   0,      0,  NULL,  NULL,    4,    do_quit,   "Exit program"},
  {  NULL,   0,     0,   0,      0,  NULL,  NULL,    0,  (void *)0,   NULL }
};

struct STURCT_KEYSYMS {
        KeySym sym;
        int   win;
        char *key;
        char *help;
};

struct STURCT_KEYSYMS keysyms[] = {
        { XK_Up,        1,      "Up-Arrow",                     "Move Integration Region Up" },
        { XK_Down,      1,      "Down-Arrow",                   "Move Integration Region Down" },
        { XK_Up,        1,      "<Control> Up-Arrow",           "Move Upper Integration Channel Up" },
        { XK_Down,      1,      "<Control> Down-Arrow",         "Move Upper Integration Channel Down" },
        { XK_Up,        1,      "<shift> Up-Arrow",             "Move Lower Integration Channel Up" },
        { XK_Down,      1,      "<shift> Down-Arrow",           "Move Lower Integration Channel Down" },
        { XK_p,         1,      "<control> + p",                "Screen Capture Spectrum Plot" }
};

#define MAX_KEYSYMS (sizeof(keysyms) / sizeof(struct STURCT_KEYSYMS))

int to_helper(buf)
char *buf;
{
  if(buf)
  {
    log_msg("To HELPER: '%s'", buf);

    sock_send(helper, buf);
  }

  return(0);
}


int to_dataserv(buf)
char *buf;
{
  if(buf)
  {
    log_msg("To DATASERV: '%s'", buf);

    sock_send(dataserv, buf);
  }

  return(0);
}


int do_quit()
{
  signalHandler();

  return(0);
}

int printKeySymHelp()
{
  int i;
  char tbuf[256];
  struct STURCT_KEYSYMS *k;

  sprintf(tbuf, "Press the Following Keys in Either the OTF or Spectral Plot Windows\n");

  printf("\n%s\n", tbuf);
  log_msg(" ");
  log_msg(tbuf);
  log_msg(" ");

  k = &keysyms[0];
  for(i=0;i<MAX_KEYSYMS;i++,k++)
  {
    if(k->win == 0)
    {
      sprintf(tbuf, "Press %s key for %s", k->key, k->help);
      printf("%s\n", tbuf);

      log_msg(tbuf);
    }
  }

  sprintf(tbuf, "Press the Following Keys Spectral Plot Windows\n");

  printf("\n%s\n", tbuf);
  log_msg(" ");
  log_msg(tbuf);
  log_msg(" ");

  k = &keysyms[0];
  for(i=0;i<MAX_KEYSYMS;i++,k++)
  {
    if(k->win == 1)
    {
      sprintf(tbuf, "Press %s key for %s", k->key, k->help);
      printf("%s\n", tbuf);

      log_msg(tbuf);
    }
  }

  return(0);
}


int do_help(buf)
char *buf;
{
  struct MAIL_PARSE *p;
  char tbuf[128];
  char *cp;

  cp = strtok(buf, " \n\t");

  sprintf(tbuf, "Command              Arguments\n");
  printf("\n%s\n", tbuf);

  log_msg(tbuf);

  for(p=parse_table;p->msg;p++)
  {
    if(cp)
    {
      if(!strncmp(p->msg, cp, strlen(cp)))
      {
        sprintf(tbuf, "%s: %s", p->msg, spaces);
        strcpy(tbuf+18, p->help);
        printf("%s\n", tbuf);

        log_msg(tbuf);
      }
    }
    else
    {
      sprintf(tbuf, "%s: %s", p->msg, spaces);
      strcpy(tbuf+18, p->help);
      printf("%s\n", tbuf);

      log_msg(tbuf);
    }
  }

  printKeySymHelp();

  return(0);
}

int doSpecWinRepaint()
{
  if(debug)
  {
    log_msg("doSpecWinRepaint()");
  }

  if(last_image.freqres > 0.0)
  {
    doSpecPlot(&last_image);
  }

  return(0);
}


int doSpecWinReSize()
{
  if(debug)
  {
    log_msg("doSpecWinReSize()");
  }

  return(0);
}


int putChanTemp(c, t)
int c;
float t;
{
  char tbuf[256];

  xgr_setCurWindow(XGR_SPEC_WIN, XGR_SET_PIXMAP);

  sprintf(tbuf, "Chan: %4d;  Intensity: %6.3fK", c, t);
  xgr_drawTextClear(0.05, ROW3, tbuf, DS_MAGENTA, font0);

  xgr_pixmapToWindow("Spectral Plot Window");

  xgr_setCurWindow(XGR_SPEC_WIN, XGR_SET_WIN);

  return(0);
}


int checkKeyPress(event)
XEvent *event;
{
  KeySym   keysym;
  XComposeStatus compose;
  char     buffer[1], tbuf[256];
  char     fname[256], sysBuf[256];
  int      bufsize=1, change = 0;

  int      int_chans[2];
  int      noint;

  if(last_image.freqres == 0.0)
  {
    log_msg("No map to plot");

    return(1);
  }

  int_chans[0] = last_image.int_beg;
  int_chans[1] = last_image.int_end;
  noint        = last_image.noint;

  XLookupString((XKeyEvent *)event, buffer, bufsize, &keysym, &compose);

  if(keysym == XK_p && event->xkey.state & ControlMask)
  {
    if(event->xkey.state & ControlMask) /* Print screen */
    {
      sprintf(fname, "SpectralPlot-%s.png", getLogDate(0));

      sprintf(sysBuf, "import -w 0x%x %s", (unsigned int)event->xany.window, fname);

      log_msg("%s", sysBuf);

      system(sysBuf);

      printf("Created file: %s\n", fname);
    }
  }
  else
  if(keysym == XK_Up && event->xkey.state & ControlMask)        /* Raise the upper channel only */
  {
    int_chans[1]++;

    if(int_chans[1] > noint-1)                          /* Don't exceed number of chans */
    {
      int_chans[1] = noint -1;
    }

    change = 1;
  }
  else
  if(keysym == XK_Down && event->xkey.state & ControlMask)      /* Lower the upper channel only */
  {
    int_chans[1]--;

    if(int_chans[1] < int_chans[0])                     /* Don't go below lower chan */
    {
      int_chans[1] = int_chans[0];
    }

    change = 1;
  }
  else
  if(keysym == XK_Up && event->xkey.state & ShiftMask)          /* Raise the lower channel only */
  {
    int_chans[0]++;

    if(int_chans[0] > int_chans[1])                     /* Don't exceed upper chan */
    {
      int_chans[0] = int_chans[1];
    }

    change = 1;
  }
  else
  if(keysym == XK_Down && event->xkey.state & ShiftMask)        /* Lower the lower channel only */
  {
    int_chans[0]--;

    if(int_chans[0] < 0)                                    /* Don't go below 0 */
    {
      int_chans[0] = 0;
    }

    change = 1;
  }
  else
  if(keysym == XK_Up)
  {
    int_chans[0]++;
    int_chans[1]++;

    if(int_chans[1] > noint-1)
    {
      int_chans[1] = noint -1;
    }

    if(int_chans[0] > int_chans[1])
    {
      int_chans[0] = int_chans[1];
    }

    change = 1;
  }
  else
  if(keysym == XK_Down)
  {
    int_chans[0]--;
    int_chans[1]--;

    if(int_chans[0] < 0)
    {
      int_chans[0] = 0;
    }

    if(int_chans[1] < int_chans[0])
    {
      int_chans[1] = int_chans[0] +1;
    }

    change = 1;
  }
  else
  if(keysym == XK_P)
  {
    log_msg("Shift P pressed");
  }

  if(change)
  {
    sprintf(tbuf, "otfchans  %d %d", int_chans[0], int_chans[1]);
    to_dataserv(tbuf);

    sprintf(tbuf, "otf_bint_chan = %d /q", int_chans[0]);
    to_helper(tbuf);
    sprintf(tbuf, "otf_eint_chan = %d /q", int_chans[1]);
    to_helper(tbuf);

  }

  return(0);
}



int doSpecPlotWinEvent(event, mask)
XEvent *event;
unsigned int mask;
{
  int chan;
  struct WINDOW *w;
  float x,y, fchan;
  struct DS_IMAGE_INFO *im;

  im = &last_image;

  if(debug)
  {
    log_msg("doSpecPlotWinEvent()");
  }

  if(im->freqres == 0.0)
  {
    log_msg("No Spectral Plot");
    return(1);
  }

  switch(event->type)
  {
    case MotionNotify:
    case ButtonPressMask:
      break;

    case KeyPress:
      checkKeyPress(event);
      return(1);
      break;

    default:
      return(1);
      break;
  }

  if( xgr_findWindow(event->xbutton.display, event->xbutton.window, &w))
  {
    log_msg("xgr_findWindow() returned Non Zero");

    return(1);
  }

  x   = ((float)w->plotxe - (float)w->plotxb);
  x   = ((float)event->xbutton.x - (float)w->plotxb) / x - 0.5;

  y   = ((float)w->plotye - (float)w->plotyb);
  y   = ((float)w->plotye - (float)event->xbutton.y) / y - 0.5;

  fchan = (float)im->noint;

  fchan = x * fchan + (fchan / 2.0);

  chan = floor(fchan);

  if(chan < 0)
  {
    chan = 0;
  }
  else
  if(chan > im->noint-1)
  {
    chan = im->noint-1;
  }

  if(debug)
  {
    log_msg("X = %f, Y = %f, Chan = %.2f, %d = %f", x, y, fchan, chan, im->ydata[chan]);
  }

  putChanTemp(chan, im->ydata[chan]);

  return(0);
}


char *vxstrtok(string, last_token, delim)
char *string, *last_token, *delim;

/* char *vxstrtok(char *string, char *last_token, char *delim) --
 * Like strtok, but can be used in VxWorks.  The first call passes a non-NULL
 * "string".  A pointer to the first character in "string" not in "delim" is
 * returned (or NULL if none).  The next occurance of a character in "delim"
 * past this first character is set to NUL and a second NUL is appended to
 * "string".  Subsequent calls must pass a NULL pointer "string" and the
 * previous return value ("last_token").  In these calls, the NUL following
 * last_token is replaced by the first character in "delim" and the scan
 * continues until the next non-"delim" past this point is found or the
 * end of the string is found (NULL returned).  Again, if a non-"delim"
 * character is found, the next "delim" character following it is replaced by
 * a NUL.
 */

{
  int n, i;
  char *ptr, *ret;

  if(string) {
                                                /* First call on a new string */
    string[strlen(string)+1] = '\0';           /* Add a second NUL to the end */
    ptr = string;
  } else if(!last_token || !*last_token || !delim || !*delim)
    return NULL;
  else {
                                      /* A subsequent call on the same string */
    ptr = last_token + strlen(last_token);           /* End of previous token */
    *ptr++ = delim[0];                                /* Put a delimiter back */
  }
                                   /* Scan for the next non-"delim" character */
  for(n=strlen(delim); *ptr; ptr++) {
    for(i=0; i < n && *ptr != delim[i]; i++)
      continue;
    if(i == n)
      break;                                                     /* Found one */
  }
  if(!*ptr)
    return NULL;                                             /* End of string */

  ret = ptr++;                                     /* Save beginning of token */

                                           /* Scan for next "delim" character */
  for( ; *ptr; ptr++) {
    for(i=0; i < n && *ptr != delim[i]; i++)
      continue;
    if(i != n)
       break;                                                   /* Found one */
  }
  *ptr = '\0';                                                /* Mark the end */
  return ret;                                         /* Return the beginning */
}



int readMail(buf, len)
char *buf;
int len;
{
  char *cp;
  static char cmdbuf[4096];

  if(len == sizeof(struct DS_IMAGE_INFO))
  {
//    log_msg("Got IMAGE struct");

    bcopy(buf, (char *)&last_image, len);

    doSpecPlot(&last_image);

    return(0);
  }

  log_msg("readMail(): Got %d bytes, %s", len, buf);

  if(buf || len < sizeof(cmdbuf))
  {
    cp = vxstrtok(buf, NULL, "\n");
    while(cp)
    {
      strcpy(cmdbuf, cp);
      parseMail(cmdbuf);
      cp = vxstrtok(NULL, cp, "\n");
    }
  }

  return(0);
}


int parseMail(buf)
char *buf;
{
  char *s;
  struct MAIL_PARSE *p;

  deNewLineIt(buf);

  strlwr(buf);

  for(p=parse_table;p->msg;p++)
  {
    if(!strncmp(p->msg, buf, p->len))
    {
      if(p->log && strlen(buf) < 2048)
      {
        s = (char *)sock_name(last_msg());
        if(s)
          log_msg("%s: (%s) %s", prgName, sock_name(last_msg()), buf);
        else
          log_msg("%s: %s", prgName,  buf);
      }

      p->fctn(buf+p->len);

      return(0);
    }
  }

  log_msg("Command %s NOT found", buf);

  return(0);
}


int myIOErrorHandler(display, myerr)
Display *display;
XErrorEvent *myerr;
{
  if(display)
  {
    log_msg("Server %s caused IO Trap", DisplayString(display));
    strcpy(deadServer, DisplayString(display));
  }
  else
  {
    log_msg("Server: Unknown Server caused IO Trap");
    strcpy(deadServer, "Unknown Server");
  }
  exit(2);
}



int myErrorHandler(display, myerr)
Display *display;
XErrorEvent *myerr;
{
  if(display)
  {
    log_msg("Server %s caused Xlib Error", DisplayString(display));
    strcpy(deadServer, DisplayString(display));
  }
  else
  {
    log_msg("Server: Unknown Server caused Xlib Error");
    strcpy(deadServer, "Unknown Server");
  }
  return(0);
}



void signalHandler()
{
  struct XGR_HEAD *p;
  struct XGR_     *s;

  exit(1);

  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;
    if(s->dpy)
    {
      log_msg("Signal: Shuting Down Server %s", DisplayString(s->dpy));
      XFlush(s->dpy);
      XCloseDisplay(s->dpy);
    }
  }
}

int exitProc(status)
int status;
{
  log_msg("Exiting with status %d", status);

  return(status);
}


int processEvent(s)
struct XGR_ *s;
{
  XEvent event;
/*
  struct WINDOW *xgr_getWin();
 */

  while(XPending(s->dpy))
  {
    XNextEvent(s->dpy, &event);

    switch( event.type )
    {
      case Expose:
                /* Filter down to the last Expose event */
        while (XCheckTypedWindowEvent(s->dpy, event.xany.window, Expose, &event))
        {
          ;
        }

        xgr_rePaintProc(event.xexpose.window);

        break;

      case ConfigureNotify:

                /* Filter down to the last ConfigureNotify event */
        while (XCheckTypedWindowEvent(s->dpy, event.xany.window, ConfigureNotify, &event))
        {
          ;
        }

        if(event.xconfigure.window && !event.xconfigure.send_event)
        {
          xgr_reSizeProc(event.xconfigure.window, event.xconfigure.width, event.xconfigure.height);
        }

        break;

      case ButtonPress:
          xgr_eventProc(&event, event.xbutton.window, ButtonPress);
        break;

      case MotionNotify:
          xgr_eventProc(&event, event.xbutton.window, MotionNotify);
        break;

      case KeyPress:
          xgr_eventProc(&event, event.xbutton.window, KeyPress);
        break;

      case UnmapNotify:
        break;

      case DestroyNotify:
        log_msg("Destroy");

        exit((int)3);

        break;

      default:
        break;
    }
  }

  return(0);
}


              /* Create an X11 image given a desired width, height and depth. */
XImage *createXimage(display, w, h, depth)
Display *display;
int w, h, depth;
{
  Visual *visual;
  XImage *xi;
  unsigned ui;

  visual = DefaultVisual(display, DefaultScreen(display));

  if(depth == 1)
  {
    return((XImage *)NULL);
  }

  xi = XCreateImage(display, visual, (unsigned int)depth, ZPixmap,0,
                        (char *)NULL, (unsigned int)w, (unsigned int)h, 16, 0);
  if(xi == NULL)
  {
    log_msg("XCreateImage failed");
    return((XImage *)NULL);
  }

  ui = (unsigned)(xi->height*xi->bytes_per_line*sizeof(char));
  if((xi->data=(char *)malloc(ui))==NULL)
  {
    log_msg("Unable to malloc(%d) ximage in createXimage()", ui);
    XDestroyImage(xi);
    return((XImage *)NULL);
  }

  bzero((char *)xi->data, xi->height * xi->bytes_per_line * sizeof(char));

  return(xi);
}



int determine_min_max(ydata, dl, scan)
float *ydata;
int dl;
float scan;
{
  int i;

  if(ydata == NULL)
     return(1);
                                                   /* redetermine min and max */
  datamax = -9999999999.9;
  datamin = 9999999999.0;

  for(i=0;i<dl;i++)
  {
    if(ydata[i] > datamax)
      datamax = ydata[i];
    if(ydata[i] < datamin)
      datamin = ydata[i];
  }

  if(fabs(datamax - datamin) < DATASPREAD)
  {
    log_msg("determine_min_max(): Scan #%7.2f is all %.4f", scan, datamin);

    datamin -= 1.0;
    datamax += 1.0;

    return(1);
  }
  return(0);
}


int putHeader(image)
struct DS_IMAGE_INFO *image;
{
  int mon, day, yr;
  char raStr[20], decStr[20], timeStr[20], mapStr[60], buf[256];
  extern struct XGR_ *server;

  xgr_setFont(server, font0);

  sexagesimal(image->time / 3600, timeStr, DDD_MM_SS_S100);
  strcmprs(timeStr);

  sexagesimal(image->ra, raStr, DD_MM_SS);
  sexagesimal(image->dec, decStr, DD_MM_SS);

  yr = (int)image->date;
  mon = (int)((image->date - (float)yr) * 100.0);
  day = (int)((image->date - (float)yr) * 10000.0 +0.5)-mon*100.0;

  sprintf(buf, "%16.16s %3d SCANS: %7.2f-%7.2f INT=%s DATE: %2d %s %2.2d",
                        image->source,
                        image->scanct,
                        image->bscan,
                        image->escan,
                        timeStr, day, months[mon-1], yr-2000);

  xgr_drawText(0.05, ROW0, buf, DS_MAGENTA, font0);

  if(image->col >=0)
  {
    sprintf(mapStr,"  Col %3d  Row %3d   ", image->col,image->row);
  }
  else
  {
    sprintf(mapStr, "( %s %s )", raStr, decStr);
  }

  sprintf(buf, "%s=%s %s %s CAL= %5.1f TSYS= %5.0f",
                        image->coordcd,
                        raStr,
                        decStr,
                        mapStr,
                        image->cal,
                        image->tsys);

  xgr_drawText(0.05, ROW1, buf, DS_MAGENTA, font0);

  sprintf(buf, "FREQ= %.2f SYN= %10.8f VEL= %7.1f DV= %6.2f FR= %4.0f SB= %d",
                        image->freq,
                        image->synfreq / 1000.0,
                        image->vel,
                        image->dv,
                        image->freqres * 1000.0,
                        image->sb );
  xgr_drawText(0.05, ROW2, buf, DS_MAGENTA, font0);

  sprintf(buf, "Rms %8.3fK",image->rms);
  xgr_drawText(0.80, ROW3, buf, DS_MAGENTA, font0);

  return(0);
}



int display_image(xb, xe, yb, ye, im)
float xb, xe, yb, ye;
struct DS_IMAGE_INFO *im;
{
  float *xdata;
  int i, noint;
  float scale;
//  char temp[80];
  float *data;

  if(debug)
  {
    log_msg("display_image();");
  }

  data = &im->ydata[0];

  noint = (int)im->noint;

  zerodata = 0;
  if(!lockScale)
  {
    if( determine_min_max(data, noint, im->bscan))
    {
      zerodata = 1;
    }
  }

  scale = ye -yb;
//  sprintf(temp, " %7.2f   %8.8s", im->scan, im->backend);
//  xgr_drawText(xb, 0.905*scale + yb, temp, DS_RED, font0);
  xgr_drawBox(xb, 0.05*scale + yb, xe,  0.90*scale + yb, DS_GRAY50);

                                         /* Initialize the x-axis data points */
  if( (xdata = (float *)malloc((unsigned)(noint*sizeof(float)))) == NULL)
  {
    log_perror("malloc(xdata)");
    return(-1);
  }

#ifdef TOMMIE
                                                /* Set the x-axis data points */
  startRes = (float)im->refpt*im->freqres;

  if( im->sb == 2.0) /* LSB */
  {
    for(i=0;i<noint;i++)
    {
      xdata[i] = startRes + ((float)i*im->freqres);
    }
  }
  else
  {
    for(i=0;i<noint;i++) /* USB */
    {
      xdata[i] = -startRes - ((float)i*im->freqres);
    }
  }
#else
    for(i=0;i<noint;i++)
    {
      xdata[i] = (float)i;
    }
#endif

  globalStart = xdata[0];
  globalStop  = xdata[noint-1];

  xgr_drawBin(xdata, data, noint, DS_GREEN);

  if(zerodata)
  {
    xgr_drawText(xb, 0.8*scale + yb, "Data is Zero", DS_RED, font0);
  }

  free((char *)xdata);

  return(1);
}


int doSpecPlot(im)
struct DS_IMAGE_INFO *im;
{
  int i;
  float xmin, xmax, ymin, ymax, yavg, xl, yt, xr, yb;
  extern float globalYMin, globalYMax;
  extern int drawGrid;

  if(debug)
  {
    log_msg("doSpecPlot():");
  }

  xgr_mapUnmapWindow("Spectral Plot Window", 1);

  xgr_setCurWindow(XGR_SPEC_WIN, XGR_SET_PIXMAP);

  xgr_clearScreen();

  im->freqres = 1.0;

  drawGrid = 1;
  display_image(0.05, 0.98, 0.01, 0.92, im);
  drawGrid = 0;

  if(im->doBaseline)
  {
    xmin = globalStart;
    xmax = globalStop;
    ymin = globalYMin;
    ymax = globalYMax;
    yavg = (ymax - ymin) / 15.0;

    if(debug)
    {
      log_msg("Baselines: %d %d %d %d %d %d %d %d",
                        im->baselines[0],
                        im->baselines[1],
                        im->baselines[2],
                        im->baselines[3],
                        im->baselines[4],
                        im->baselines[5],
                        im->baselines[6],
                        im->baselines[7]);

    }
    for(i=0;i<8;i+=2)
    {
      if(im->baselines[i] > 0 && im->baselines[i+1] > 0 )
      {
        xl = (float)im->baselines[i];
        xr = (float)im->baselines[i+1];
        yt = -yavg;
        yb =  yavg;

        if(debug)
        {
          log_msg("Put Rectangles(): %f %f %f %f %f %f %f %f %f %f",
                                startRes, im->freqres, xmin, xmax, ymin, ymax, xl, yt, xr, yb);
        }

        xgr_drawRectangle( xmin, xmax, ymin, ymax, xl, yt, xr, yb, DS_BLUE, 0);
      }
    }
  }

  if(im->int_beg >= 0 && im->int_end >= im->int_beg && im->int_end < im->noint-2)
  {
    xmin = globalStart;
    xmax = globalStop;
    ymin = globalYMin;
    ymax = globalYMax;
    yavg = (ymax - ymin) / 15.0;

    xl = (float)im->int_beg;
    xr = (float)im->int_end;
    yt = -yavg;
    yb =  datamax;

    xgr_drawRectangle( xmin, xmax, ymin, ymax, xl, yt, xr, yb, DS_YELLOW, 0);
  }

  putHeader(im);

  xgr_pixmapToWindow("Spectral Plot Window");

  xgr_setCurWindow(XGR_SPEC_WIN, XGR_SET_WIN);

  return(0);
}



/* Create individual window */

struct WINDOW *createWindow(s, title, cw, ch, xpos, ypos, bg, usPixmap, which)
struct XGR_ *s;
char *title;
int cw, ch, xpos, ypos, bg, usPixmap, which;
{
  unsigned long value_mask;
  XSetWindowAttributes attr;
  XSizeHints size_hints;
  struct WINDOW *w;
  XWMHints wm_hints;
  XTextProperty *wi, *ic;

  if((w = (struct WINDOW *)calloc(1, sizeof(struct WINDOW ))) == NULL)
  {
    log_msg("error in calloc for main Plot window");
    exit(1);
  }

  insque(w, s->windows.prev);

  strcpy(w->frameTitle, title);
  w->canw       = cw;
  w->canh       = ch;
  w->background = bg;
  w->first      = 1;
  w->win        = XCreateSimpleWindow(s->dpy, RootWindow(s->dpy,s->scr_num),
                                      xpos, ypos, (unsigned int)w->canw,
                                            (unsigned int)w->canh,
                                        1,  s->colors[DS_WHITE],		/* fg */
                                            s->colors[DS_BLACK]);		/* bg */
  w->usPixmap   = usPixmap;

  if(w->usPixmap)
  {
    if(!(w->pixmap = XCreatePixmap(s->dpy, w->win, (unsigned int)w->canw, (unsigned int)w->canh, s->depth)))
    {
      log_msg("%s: Error in create of off screen pixmap\n", prgName);
      exit(1);
    }
  }

  if(which == XGR_SPEC_WIN)
  {
    XSelectInput(s->dpy,w->win, frame_mask | ButtonPressMask | Button1MotionMask);
  }

  size_hints.flags        = PPosition | PSize | PMinSize/* | PAspect*/;
  size_hints.min_width    = cw;
  size_hints.min_height   = ch;
  size_hints.x            = xpos;
  size_hints.y            = ypos;

  w->gc   = XCreateGC(s->dpy, RootWindow(s->dpy, s->scr_num), (unsigned long)0, (XGCValues *)NULL);

  w->invalid               = 1;

  w->icon = XCreateBitmapFromData(s->dpy, w->win, dataserv_bmp_bits,
                                                  dataserv_bmp_width,
                                                  dataserv_bmp_height);

  XSetStandardProperties(s->dpy, w->win,
                         w->frameTitle, (char *)NULL, w->icon,
                         (char **)NULL, 0, &size_hints);
  wi = NULL;
  ic = NULL;

  wm_hints.initial_state = NormalState;
  wm_hints.input         = True;
  wm_hints.icon_pixmap   = 0;
  wm_hints.flags         = StateHint | InputHint | WindowGroupHint;
  wm_hints.window_group  = w->win;

  XSetWMProperties(s->dpy, w->win, wi, ic, NULL, 0, &size_hints, &wm_hints, NULL);


  if(DoesBackingStore(XDefaultScreenOfDisplay(s->dpy)))
  {
    if(debug)
    {
      log_msg("createWindow(): Setting Backing Store for Plot Window");
    }

    value_mask = CWBackingStore;
    attr.backing_store = WhenMapped;
    XChangeWindowAttributes(s->dpy, w->win, value_mask, &attr);
  }

  XMapWindow(s->dpy, w->win);
  w->mapped       = 1;

  XSetWindowBackground(s->dpy, w->win, s->colors[w->background]);
  XSetBackground(s->dpy, w->gc, s->colors[w->background]);
  XClearWindow(s->dpy, w->win);

  s->cur = w;

  return(w);
}



int createWindows(s, title, cw, ch, xpos, ypos, bg, usPixmap)
struct XGR_ *s;
char *title;
int cw, ch, xpos, ypos, bg, usPixmap;
{
  int x, y, w, h;
  struct WINDOW *win;

  log_msg("DisplayInfo for %s - %s major %d minor %d release %d",
           DisplayString(s->dpy), ServerVendor(s->dpy), ProtocolVersion(s->dpy),
           ProtocolRevision(s->dpy), VendorRelease(s->dpy));

  s->xfd          = XConnectionNumber(s->dpy);
  s->scr_num      = DefaultScreen(s->dpy);
  s->numFonts     = XGR_MAX_FONTS;
  s->fontNames    = findFixedFonts(s->dpy, &s->numFonts);
  s->windows.prev = s->windows.next = &s->windows;
  s->depth        = XDefaultDepthOfScreen(XDefaultScreenOfDisplay(s->dpy));
  s->userXimage   = NULL;

  x = xpos;
  y = ypos;
  w = cw;
  h = ch;

                                                /* create Spectral Plotting window */
  win               = createWindow(s, "Spectral Plot Window", w, h, x, y, bg, usPixmap, XGR_SPEC_WIN);
  win->repaint_proc = doSpecWinRepaint;
  win->resize_proc  = doSpecWinReSize;
  win->event_proc   = doSpecPlotWinEvent;
  s->specwin        = win;

  xgr_graphInit(s);

  return(0);
}


static int block_for_event(xfd, n)
int *xfd;
int n;
{
  int len, i;
  static int sel = 1;

  sel = sock_sel(inMsg, &len, xfd, n, 10, 0);

  if( sel == -1 )
  {                                                /* timeout */
    return(sel);
  }

  for(i=0;i<n;i++)
  {
    if(sel == xfd[i])
    {
      return(sel);
    }
  }

  if(sock_callback)
  {
    (*sock_callback)(inMsg, len);
  }

  return(sel);
}



int main_loop()
{
  struct XGR_HEAD *p, *p2;
  struct XGR_ *s, *f;
  int count, n, xfd;
  int xfds[256];

  while (1)
  {
    count=0;

    for(p=xgr_head.next;p!=&xgr_head;p=p->next)
    {
      s = (struct XGR_ *)p;

      if(XPending(s->dpy))
      {
        processEvent(s);
        count++;
      }
    }

    if(!count)
    {
      n=0;

      for(p2=xgr_head.next;p2!=&xgr_head;p2=p2->next)
      {
        f = (struct XGR_ *)p2;
        xfds[n] = f->xfd;
        n++;
      }

      xfd = block_for_event(xfds, n);

      for(p2=xgr_head.next;p2!=&xgr_head;p2=p2->next)
      {
        f = (struct XGR_ *)p2;

        if(xfd == f->xfd)
        {
          if(XPending(f->dpy) == 0)
          {
            if(f->dpy == server->dpy)   /* is it the main one */
              exit(0);

            log_msg("Removeing %s in destoryFunc", DisplayString(f->dpy));
            remque(f);
            xgr_freeWindows(f);

            break;
          }
        }
      }
    }
  }

  return(0);
}


int set_sock_callback( call )
int (*call)();
{
  sock_callback = call;

  return(0);
}


int main(argc, argv)
int argc;
char *argv[];
{
  int cw, ch, bg;
  int xpos, ypos;
  char *cp;

  setCactusEnvironment();

  setpgid( (pid_t)0, (pid_t)0);        /* divorce myself from parents */

  atexit((void *)exitProc);

#ifndef __DARWIN__
  signal(SIGINT,  (__sighandler_t)signalHandler);
  signal(SIGTERM, (__sighandler_t)signalHandler);
#else
  signal(SIGINT,  signalHandler);
  signal(SIGTERM, signalHandler);
#endif

  strcpy(prgName, "DATASERV");

  cp = getenv("LPSESSION");

  if(cp)
  {
    sprintf(tfile, "LOGDIR=/tmp/%s", cp);
    putenv(tfile);
  }

  smart_log_open("specplot", 3);
  log_eventdEnable(LOG_TO_LOGGER);

  cp = getenv("DISPLAY");
  if(cp)
    sprintf(localHost,"%s",cp);

  xgr_head.prev = xgr_head.next = &xgr_head;

  if((server=(struct XGR_ *)calloc(1,sizeof(struct XGR_)))==NULL)
  {
    log_msg("error in calloc for main server");
    return(1);
  }

  insque(server, xgr_head.prev);

  xpos = 0;
  ypos = 910;
  cw   = 720;
  ch   = 480;

  strcpy(server->serverName, localHost);

  if((server->dpy = XOpenDisplay(localHost)) == (Display *)NULL)
  {
    log_msg("Unable to open display %s", localHost);
    return(0);
  }

  sock_bind("LINUXPOPS_SPECPLOT");
  dataserv = sock_connect("LINUXPOPS_DATASERV");
  helper   = sock_connect("LINUXPOPS_HELPER");

  set_sock_callback( (int (*)(void))readMail );

  sock_bufct(sizeof(struct DS_IMAGE_INFO)+256);

  strcpy(title,          "Dataserv");
  strcpy(mainFrameTitle, "Dataserv");

  bg = DS_BLACK;

  createWindows(server, title, cw, ch, xpos, ypos, bg, globalUsPixmap);

  XSetErrorHandler(myErrorHandler);
  XSetIOErrorHandler((XIOErrorHandler)myIOErrorHandler);

  main_loop();

  return(0);
}
