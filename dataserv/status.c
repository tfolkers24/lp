#include "ds_extern.h"

#define ROW0            0.96
#define ROW1            0.92
#define ROW2            0.88
#define ROW3            0.84
#define ROW4            0.80
#define ROW5            0.76
#define ROW6            0.72
#define ROW7            0.68
#define ROW8            0.64

static int font0 = FONT_SIZE_SMALL;

int doStatus()
{
  int ret;
  char tbuf[256];

  log_msg("Status Update Entered");

  xgr_mapUnmapWindow("Status Window", 1);

  xgr_setCurWindow(XGR_STAT_WIN, XGR_SET_PIXMAP);

  sprintf(tbuf, "Map Cols: %4d;  Map Rows: %4d", otf.cols, otf.rows);
  xgr_drawTextClear(0.05, ROW0, tbuf, ORANGE, font0);

  sprintf(tbuf, "Map Coordinates: RA:  20:31:36;  Dec:  39:40:27");
  xgr_drawTextClear(0.05, ROW1, tbuf, ORANGE, font0);

  sprintf(tbuf, "Map Cell Size:   RA: 12.50 Sec;  Dec: 10.00 Sec");
  xgr_drawTextClear(0.05, ROW2, tbuf, ORANGE, font0);

  sprintf(tbuf, "Map Cols: 1168; Rows: 1285");
  xgr_drawTextClear(0.05, ROW3, tbuf, ORANGE, font0);

  sprintf(tbuf, "OTF: Map Rest Freq: 230.537906");
  xgr_drawTextClear(0.05, ROW4, tbuf, ORANGE, font0);

  sprintf(tbuf, "Backend Resolution: 1.000 MHz; noint: 512");
  xgr_drawTextClear(0.05, ROW5, tbuf, ORANGE, font0);

  sprintf(tbuf, "Number of Occupied Cells: 20777 out of 1500880");
  xgr_drawTextClear(0.05, ROW6, tbuf, ORANGE, font0);

  sprintf(tbuf, "Total Number of OTF Map Scans: 321");
  xgr_drawTextClear(0.05, ROW7, tbuf, ORANGE, font0);

  ret = xgr_pixmapToWindow("Status Window");

  if(ret)
  {
    log_msg("xgr_pixmapToWindow(): returned bad window");
  }

  xgr_setCurWindow(XGR_MAIN_WIN, XGR_SET_WIN); /* Set back to main */

  return(0);
}


int doStatWinRepaint()
{
  log_msg("doStatWinRepaint()");

  xgr_setCurWindow(XGR_STAT_WIN, XGR_SET_PIXMAP);
  xgr_clearScreen();
  xgr_pixmapToWindow("Status Window");

  xgr_setCurWindow(XGR_MAIN_WIN, XGR_SET_WIN); /* Set back to main */

  doStatus();

  return(0);
}


int doStatWinReSize()
{

  log_msg("doStatWinReSize()");

  return(0);
}


int doStatWinEvent(event, mask)
XEvent *event;
unsigned int mask;
{
  struct WINDOW *w;

  log_msg("doStatWinEvent()");

  switch(event->type)
  {
    case MotionNotify:
    case ButtonPressMask:
	log_msg("doStatWinEvent(): Button Motion or Button Press");
      break;

    case KeyPress:
	log_msg("doStatWinEvent(): Key Press");
      return(1);
      break;

    default:
      return(1);
      break;
  }

  if( xgr_findWindow(event->xbutton.display, event->xbutton.window, &w))
  {
    log_msg("doStatWinEvent(): xgr_findWindow() returned Non Zero");

    return(1);
  }

  doStatus();

  return(0);
}
