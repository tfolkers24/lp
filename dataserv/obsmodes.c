#include "ds_extern.h"
#include "ds_proto.h"

#ifdef GALACTIC
#undef GALACTIC
#endif

#ifdef AZEL
#undef AZEL
#endif

#ifdef USERDEF
#undef USERDEF
#endif


static int font1 = FONT_SIZE_NORMAL;

extern struct HEADER  onhead01;

/*
, onhead02, onhead03,
                      onhead04, onhead05, onhead06,
                      onhead07, onhead08, onhead09,
                      onhead10, onhead11, onhead12,
                      onhead13, onhead14, onhead15, 
                      onhead16;
 */

extern float  *ondata01, *ondata02, *ondata03,
              *ondata04, *ondata05, *ondata06,
              *ondata07, *ondata08, *ondata09,    
              *ondata10, *ondata11, *ondata12,
              *ondata13, *ondata14, *ondata15,
              *ondata16;

extern struct HEADER  offhead01;

extern float  *offdata01, *offdata02, *offdata03,
              *offdata04, *offdata05, *offdata06,
              *offdata07, *offdata08, *offdata09,
              *offhead10, *offdata11, *offdata12,   
              *offdata13, *offdata14, *offdata15,
              *offdata16;

extern struct HEADER  gainshead01;

extern float  *gainsdata01, *gainsdata02, *gainsdata03,
              *gainsdata04, *gainsdata05, *gainsdata06,
              *gainsdata07, *gainsdata08, *gainsdata09, 
              *gainsdata10, *gainsdata11, *gainsdata12, 
              *gainsdata13, *gainsdata14, *gainsdata15, 
              *gainsdata16;

extern float scanNum, otfIFNumber, datamin, datamax;
extern int chanSelOTF, lockScale;
extern char filename[], gfilename[], source[], prgName[];
extern int found_scans[MAX_BACKENDS*MAX_PARTS];

int mhz_4if_mode  =  0;
int khz_4if_mode  =  0;
int aos_4if_mode  =  0;
int non_if_mode   =  0;
int dbe_mode      =  0;

struct XBE_POS m4[] = { { COL_1+0.05, COL_2,},
			{ COL_2+0.05, COL_3 },
			{ COL_3+0.05, COL_4 },
			{ COL_4+0.05, COL_5 },
};

struct XBE_POS m3[] = { { COL_1 +0.05, COL_2B }, 
			{ COL_2B+0.05, COL_3B }, 
			{ COL_3B+0.05, COL_5  },
};

struct XBE_POS m2[] = { { COL_1+0.05, COL_3 }, 
			{ COL_3+0.05, COL_5 },
};

struct XBE_POS m1[] = { { COL_1+0.05, COL_5 },
};

struct SPECTRA_DISPLAY_PARAMS sdp[] = {

	/* FFB-A */ /* FFB-B */ /* FFB-C */ /* FFB-D */
  { 1, 0.01,  0.66, 1.0,  &ondata01, &onhead01, 0, &mhz_4if_mode, BACKEND_FFB, "F1M-1"},
  { 1, 0.02,  0.66, 1.0,  &ondata01, &onhead01, 0, &mhz_4if_mode, BACKEND_FFB, "F1M-2"},
  { 1, 0.03,  0.66, 1.0,  &ondata01, &onhead01, 0, &mhz_4if_mode, BACKEND_FFB, "F1M-3"},
  { 1, 0.04,  0.66, 1.0,  &ondata01, &onhead01, 0, &mhz_4if_mode, BACKEND_FFB, "F1M-4"},

	/* FB2-A */ /* FB2-B */ /* FB2-C */ /* FB2-D */
  { 1, 0.05,  0.33, 0.66, &ondata01, &onhead01, 0, &khz_4if_mode, BACKEND_FFB, "FQM-1"},
  { 1, 0.06,  0.33, 0.66, &ondata01, &onhead01, 0, &khz_4if_mode, BACKEND_FFB, "FQM-2"},
  { 1, 0.07,  0.33, 0.66, &ondata01, &onhead01, 0, &khz_4if_mode, BACKEND_FFB, "FQM-3"},
  { 1, 0.08,  0.33, 0.66, &ondata01, &onhead01, 0, &khz_4if_mode, BACKEND_FFB, "FQM-4"},

	/* AOS-AB */
  { 1, 0.01, 0.009, 0.33, &ondata01, &onhead01, 0, &aos_4if_mode, BACKEND_AOS, "A1M-1"},
  { 1, 0.02, 0.009, 0.33, &ondata01, &onhead01, 0, &aos_4if_mode, BACKEND_AOS, "A1M-2"},
  { 0, 0.02, 0.009, 0.33, &ondata01, &onhead01, 0, &aos_4if_mode, BACKEND_AOS, "A1M-3"},
  { 0, 0.02, 0.009, 0.33, &ondata01, &onhead01, 0, &aos_4if_mode, BACKEND_AOS, "A1M-4"},

	/* AOS-C */
  { 1, 0.05, 0.009, 0.33, &ondata01, &onhead01, 0, &aos_4if_mode, BACKEND_AOS, "AQM-1"},
  { 0, 0.05, 0.009, 0.33, &ondata01, &onhead01, 0, &aos_4if_mode, BACKEND_AOS, "AQM-2"},
  { 0, 0.05, 0.009, 0.33, &ondata01, &onhead01, 0, &aos_4if_mode, BACKEND_AOS, "AQM-3"},
  { 0, 0.05, 0.009, 0.33, &ondata01, &onhead01, 0, &aos_4if_mode, BACKEND_AOS, "AQM-4"},
           
	/* CTS-A */
  { 1, 0.01, 0.009, 0.33, &ondata01, &onhead01, 0, &aos_4if_mode, BACKEND_CTS, "C50-1"},
  { 0, 0.01, 0.009, 0.33, &ondata01, &onhead01, 0, &aos_4if_mode, BACKEND_CTS, "C50-2"},
  { 0, 0.01, 0.009, 0.33, &ondata01, &onhead01, 0, &aos_4if_mode, BACKEND_CTS, "C50-3"},
  { 0, 0.01, 0.009, 0.33, &ondata01, &onhead01, 0, &aos_4if_mode, BACKEND_CTS, "C50-4"},

	/* DBE */
  { 1, 0.01, 0.009, 0.33, &ondata01, &onhead01, 0, &dbe_mode,     BACKEND_DBE},
  { 1, 0.02, 0.009, 0.33, &ondata01, &onhead01, 0, &dbe_mode,     BACKEND_DBE},
  { 1, 0.03, 0.009, 0.33, &ondata01, &onhead01, 0, &dbe_mode,     BACKEND_DBE},
  { 1, 0.04, 0.009, 0.33, &ondata01, &onhead01, 0, &dbe_mode,     BACKEND_DBE}
};


float ifnums[] = { 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08};

int ignore_gains = 0,
    restoreMsg   = 0;

extern int ignoreThisChan, printDataLines, lastGraph, fitFail;

float ifNumber = 0.01;
struct BOX_DATA bd;


int loadFile(ifNum)
float ifNum;
{
  char *cp;
  int ret=0;

  cp = filename;

  if( (ret = read_scan( scanNum + ifNum, cp, &ondata01, &onhead01)) != 0 )
    return(ret);

  strncpy(source, onhead01.object, SRCLEN);
  source[SRCLEN] = '\0';

  return(ret);
}



int do_linetotalmapoff()
{
  restoreMsg=1;

  return(0);
}



int do_lineotf()
{
  int i, n, k, ret;
  float requestOff, requestGains;
  char *cp;

  if(debug)
    log_msg("Entering do_lineotf");

  k = chanSelOTF;

  otfIFNumber  = sdp[k].ifnum;

  if(debug)
    log_msg("Using %s for OTF", sdp[k].name);

  cp = filename;

  if(debug)
    log_msg("Using %s file ", cp);

  ret = read_scan( scanNum+otfIFNumber, cp, &ondata01, &onhead01);

  if(ret)
  {
    if(ret != -2)
    {
      log_msg("On scan #%.2f, NOT found in %s", scanNum+otfIFNumber, cp);
    }

    return(0);
  }

  requestOff = onhead01.offscan;

  if(read_scan( requestOff, cp, &offdata01, &offhead01))
  {
    log_msg("Off scan #%.2f, NOT found in %s", requestOff, cp);
    return(0);
  }

  requestGains = (double)((int)onhead01.gains) + otfIFNumber;

  cp = gfilename;

  if(read_scan( requestGains, cp, &gainsdata01, &gainshead01))
  {
    log_msg("Gain scan #%.2f, NOT found in %s", requestGains, cp);
    return(0);
  }

  if(ignore_gains)
  {
    log_msg("Ignoring Gains");

    n = (int)gainshead01.noint;

    for(i=0;i<n;i++)
    {
      gainsdata01[i] = 400.0;
    }
  }

  do_otf(ondata01, &onhead01, offdata01, gainsdata01);

  xgr_checkForEvents();

  return(0);
}


int setSourceName(head)
struct HEADER *head;
{
  char temp[128];

  lastGraph = 0;
  xgr_clearScreen();
  strncpy(source, head->object, SRCLEN);
  source[SRCLEN] = '\0';
  sprintf(temp, "Source: %s", source);
  xgr_drawText(0.38, 0.985, temp, DS_RED, font1);

  return(0);
}



int clipDataProc(data, head, nchan, cd)
float *data;
struct HEADER *head;
int nchan, cd;
{
  int i, j, n;

  if(cd == 0)
    return(1);

  n = nchan - (cd*2);
  for(j=0, i=cd;i<(n+cd);i++)
    data[j++] = data[i];

  head->noint = head->noint - (cd*2);
  head->datalen = head->datalen - (cd*2*sizeof(float));
  head->refpt = head->refpt - (float)cd;

  return(0);
}




int do_linedefault()
{
  int i, first=1, j;
  float old_y = -1.0;
  struct SPECTRA_DISPLAY_PARAMS *p;

  mhz_4if_mode = found_scans[FFBA] + found_scans[FFBB] + found_scans[FFBC] + found_scans[FFBD];
  khz_4if_mode = found_scans[FB2A] + found_scans[FB2B] + found_scans[FB2C] + found_scans[FB2D];
  aos_4if_mode = found_scans[AOSA] + found_scans[AOSB] + found_scans[AOSC] + found_scans[CTSA];
  dbe_mode     = found_scans[DBEA] + found_scans[DBEB] + found_scans[DBEC] + found_scans[DBED];

  log_msg("FFB = %d, FB2 = %d, AOS = %d, DBE = %d", mhz_4if_mode, khz_4if_mode, 
                                                    aos_4if_mode, dbe_mode);
 
  for(i=0,p = &sdp[0];i<MAX_IF_NUMS;i++,p++)
  {
    if(p->active)
    {
      if(!read_scan(scanNum + p->ifnum, filename, p->data, p->head))
      {
        if(first)
        {
          setSourceName(p->head);
          first = 0;
        }

	if(old_y != p->yb)
        {
	  j = 0;
          old_y = p->yb;
        }

        clipDataProc(*p->data, p->head, (int)p->head->noint, p->clip);

        if(*p->mode  == 4)
            display_spectra(m4[j].xb, m4[j].xe, p->yb, p->ye, *p->data, p->head, TRUE);
	else
        if(*p->mode  == 3)
            display_spectra(m3[j].xb, m3[j].xe, p->yb, p->ye, *p->data, p->head, TRUE);
	else
        if(*p->mode  == 2)
            display_spectra(m2[j].xb, m2[j].xe, p->yb, p->ye, *p->data, p->head, TRUE);
	else
        if(*p->mode  == 1)
            display_spectra(m1[j].xb, m1[j].xe, p->yb, p->ye, *p->data, p->head, TRUE);

	j++;
      }
    }
  }

  return(0);
}


int freeDataPointers()
{
  if(ondata01) {
    printData(ondata01);
    free((char *)ondata01);
    ondata01 = (float *)NULL;
  }
/*
  if(ondata02) {
    printData(ondata02);
    free((char *)ondata02);
    ondata02 = (float *)NULL;
  }
  if(ondata03) {
    printData(ondata03);
    free((char *)ondata03);
    ondata03 = (float *)NULL;
  }
  if(ondata04) {
    printData(ondata04);
    free((char *)ondata04);
    ondata04 = (float *)NULL;
  }
  if(ondata05) {
    printData(ondata05);
    free((char *)ondata05);
    ondata05 = (float *)NULL;
  }
  if(ondata06) {
    printData(ondata06);
    free((char *)ondata06);
    ondata06 = (float *)NULL;
  }
  if(ondata07) {
    printData(ondata07);
    free((char *)ondata07);
    ondata07 = (float *)NULL;
  }
  if(ondata08) {
    printData(ondata08);
    free((char *)ondata08);
    ondata08 = (float *)NULL;
  }
  if(ondata09) {
    printData(ondata09);
    free((char *)ondata09);
    ondata09 = (float *)NULL;
  }
*/


  if(offdata01) {
    free((char *)offdata01);
    offdata01 = (float *)NULL;
  }
  if(offdata02) {
    free((char *)offdata02);
    offdata02 = (float *)NULL;
  }
  if(offdata03) {
    free((char *)offdata03);
    offdata03 = (float *)NULL;
  }
  if(offdata04) {
    free((char *)offdata04);
    offdata04 = (float *)NULL;
  }
  if(offdata05) {
    free((char *)offdata05);
    offdata05 = (float *)NULL;
  }
  if(offdata06) {
    free((char *)offdata06);
    offdata06 = (float *)NULL;
  }
  if(offdata07) {
    free((char *)offdata07);
    offdata07 = (float *)NULL;
  }
  if(offdata08) {
    free((char *)offdata08);
    offdata08 = (float *)NULL;
  }
  if(offdata09) {
    free((char *)offdata09);
    offdata09 = (float *)NULL;
  }


  if(gainsdata01) {
    free((char *)gainsdata01);
    gainsdata01 = (float *)NULL;
  }
  if(gainsdata02) {
    free((char *)gainsdata02);
    gainsdata02 = (float *)NULL;
  }
  if(gainsdata03) {
    free((char *)gainsdata03);
    gainsdata03 = (float *)NULL;
  }
  if(gainsdata04) {
    free((char *)gainsdata04);
    gainsdata04 = (float *)NULL;
  }
  if(gainsdata05) {
    free((char *)gainsdata05);
    gainsdata05 = (float *)NULL;
  }
  if(gainsdata06) {
    free((char *)gainsdata06);
    gainsdata06 = (float *)NULL;
  }
  if(gainsdata07) {
    free((char *)gainsdata07);
    gainsdata07 = (float *)NULL;
  }
  if(gainsdata08) {
    free((char *)gainsdata08);
    gainsdata08 = (float *)NULL;
  }
  if(gainsdata09) {
    free((char *)gainsdata09);
    gainsdata09 = (float *)NULL;
  }

  return(0);
}



int printData(f)
float *f;
{
  int i=0, j=0;
  char tb[20], outBuf[256];

  if(printDataLines)
  {
    strcpy(outBuf, " ");
    while(i<printDataLines)
    {
      sprintf(tb, "%.4f ", f[j]);
      strcat(outBuf, tb);
      j++;
      if(!(j%10))
      {
        log_msg("%s\n", outBuf);
        strcpy(outBuf, " ");
        i++;
      }
    }
  }

  return(0);
}
