#include "ds_extern.h"


struct DSTIME etime[] = {

/* otf.c */
	{         "otfInit", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{   "freeOtfMemory", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{     "otfGridInit", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{    "do_addHeader", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{ "checkScanHeader", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{   "addScanHeader", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "remScanHeaders", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{   "do_ignoreScan", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "checkIgnoreScans", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "addIgnoreScans", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "remIgnoreScans", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_saveIgnoreScans", 	0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_printIgnoreScans", 	0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_clearIgnore", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "checkMapScans", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "addMapScans", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "remMapScans", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_printMapScans", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "getNumSpectra", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "displaySingleOtfSpectra", 	0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "stuffOtf", 			0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "goWriteSingleChan", 	0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "displaySingleChan", 	0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "displayArrayGraph", 	0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otf", 			0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfRegular", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "doOtfGridRePaint", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "doOtfGridReSize", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfRmBadChan", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "spikeCheck", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "computeInten", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "computeMinMax", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "computeShade", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "scaleData", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "subtractPredictedRA", 	0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "placeBeamSize", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "initGrid", 			0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "paintOtfGrid", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "putOtfGrid", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "drawColorBar", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "fillOtfGrid", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "accumDataIntoWork", 	0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "zeroWorkArray", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "normalizeAndCal", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "insertVersion", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "shiftOtfChans", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "accumWorkIntoGrid", 	0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "recomputeBaseLines", 	0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "computeBaseLine", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "computeRms", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "printKeySymHelp", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "dispatchOtfGridEvents", 	0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "checkKeyPress", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfSendPlot", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "getRequestedCell", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "otfGridDisplaySpec", 	0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "displayRequestedOtf", 	0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "displayOtfGridSpectra", 	0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "getBaselines", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfVelRun", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfGridDump", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfGridLoad", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_baseLines", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfBaseLine", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfBeamSize", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfGridInit", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfChans", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfTime", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfRms", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfTsys", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfDisplayArray", 	0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfSingleBin", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfSingleChan", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfGridScale", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfGamma", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfColorChoice", 	0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfNumColors", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfInterp", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "otfInterp", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "interpAndPlot", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "createXimage", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfChannelMap", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "getChanOTF", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfIFchoice", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfLoadChoice", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "createInterpTable", 	0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "findInterp", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfSpectralSum", 	0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otf3dcube", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otf3dplot", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfRmRMS", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfSpectraDebug", 	0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfBadChan", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "fetchMapCell", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "setMapCell", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "findNextCell", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_fullInterpFill", 	0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfFixBadCells", 	0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfListCells", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfCutOff", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "forceDrawGrid", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfGridRa", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfGridDec", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfGridVelocity", 	0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfGridNoint", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfGridAllowVelShift", 	0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfGridCols", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfGridRows", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfGridRestfreq", 	0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfGridFreqres", 	0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfGridSizeRa", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfGridSizeDec", 	0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfGridCellSizeRa", 	0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfGridCellSizeDec", 	0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfGridChanCompress", 	0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfStatus", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_ephemObj", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "do_otfLockScale", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "interpAndWrite", 		0, 0, 0, 0, 0.0, 0.0, 0.0 },
	{  "lsqfit", 			0, 0, 0, 0, 0.0, 0.0, 0.0 },

/* xx.c */

	{          NULL, 		0, 0, 0, 0, 0.0, 0.0, 0.0 }
};


double getElapseTime()
{
  struct timeval tvv,*tp;
  double d;

  tp = &tvv;
  gettimeofday(tp,0);

  d = (double)tp->tv_sec + (double)tp->tv_usec / 1.0e6;

  return(d);
}



int do_etimeInit()
{
  int i;
  struct DSTIME *p;

  p=&etime[0];

  for(i=0;p->funcName;i++,p++)
  {
    p->indx = i;
    p->scalls = 0;
    p->ecalls = 0;
    p->stime  = 0.0;
    p->etime  = 0.0;
    p->ttime  = 0.0;
  }

  return(0);
}


int do_etimeClear()
{
  do_etimeInit();

  return(0);
}

/* 
struct DSTIME {
        char *funcName;

        int    indx;
        int    pad;

	int    scalls;
	int    ecalls;

        double stime;
        double etime;

        double ttime;
};
*/

int do_etimeDump()
{
  int i;
  struct DSTIME *p;
  char tbuf[256];
  FILE *fp;

  if((fp = fopen("etime_funcs.h", "w")) == NULL)
  {
    log_msg("Unable to open etime_funcs.h");

    return(1);
  }

  fprintf(fp, "#ifndef DSTIME_FUNCS_H\n");
  fprintf(fp, "#define DSTIME_FUNCS_H\n\n");

  p=&etime[0];
  for(i=0;p->funcName;i++,p++)
  {
    strcpy(tbuf, p->funcName);
    strupr(tbuf);

    fprintf(fp, "#define %24.24s %5d\n", tbuf, p->indx);
  }

  fprintf(fp, "\n#define MAX_DSTIME_FUNCTIONS      %5d\n", i);

  fprintf(fp, "\n#endif\n");

  fclose(fp);

  printf("Wrote %d defines to 'etime_funcs.h'\n", i);

  return(0);
}


int do_etimeShow()
{
  int i;
  struct DSTIME *p;
  double tpercall;
  double ttime = 0.0;

  printf("\n");

  p=&etime[0];
  for(i=0;p->funcName;i++,p++)
  {
    if(p->ecalls)
    {
      tpercall = p->ttime / p->ecalls;
    }
    else
    {
      tpercall = 0.0;
    }

    printf("%24.24s %5d %10d %10d %12.6lf %e\n", 
		p->funcName, 
		p->indx, 
		p->scalls, 
		p->ecalls, 
		p->ttime, 
		tpercall);

    ttime += p->ttime;
  }

  printf("                           Total Execution Time: %12.6lf Seconds\n", ttime);

  return(0);
}


int do_etime(func, action)
int func, action;
{
  struct DSTIME *p;

  if(func < 0 || func > MAX_DSTIME_FUNCTIONS)
  {
    fprintf(stderr, "Bad function indx %d", func);
    return(1);
  }

  p=&etime[func];

  if(action == DSTIME_START)
  {
    p->stime = getElapseTime();
    p->scalls++;

    return(0);
  }

  if(action == DSTIME_STOP)
  {
    p->etime = getElapseTime();
    p->ecalls++;

    p->ttime += (p->etime - p->stime);

    return(0);
  }
  

  return(0);
}


/* 

#ifdef DO_DSTIME
  do_etime(OTFGRIDINIT, DSTIME_START);
#endif

#ifdef DO_DSTIME
  do_etime(OTFGRIDINIT, DSTIME_STOP);
#endif

 */
