#define COL_1   (0.000)
#define COL_2   (0.250)
#define COL_2B  (0.333)
#define COL_3   (0.500)
#define COL_3B  (0.666)
#define COL_4   (0.750)
#define COL_5   (0.995)

#define FFBA     0
#define FFBB     1
#define FFBC     2
#define FFBD     3

#define FB2A     4
#define FB2B     5
#define FB2C     6
#define FB2D     7

#define AOSA     8
#define AOSB     9
#define AOSC    10
#define AOSD    11

#define CTSA    12
#define CTSB    13
#define CTSC    14
#define CTSD    15

#define DBEA    16
#define DBEB    17
#define DBEC    18
#define DBED    19




struct SPECTRA_DISPLAY_PARAMS {
        int   active;
        float ifnum;
        float yb,  ye;
        float **data;
        struct HEADER *head;
        int   clip;
        int   *mode;
        int   fname_indx;
	char *name;
};

struct XBE_POS {
        float xb;
        float xe;
};

