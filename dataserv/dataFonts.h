/*
 *  Cactus file @(#)dataFonts.h	1.2
 *        Date 05/25/99
 *
 */
#define FONT_SIZE_SMALL	 0
#define FONT_SIZE_NORMAL 1
#define FONT_SIZE_BIG	 2

/*
static int font0 = FONT_SIZE_SMALL;
static int font1 = FONT_SIZE_NORMAL;
static int font2 = FONT_SIZE_BIG;
 */
