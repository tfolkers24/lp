#define LAST_SCAN	     -1
#define NEXT_LAST_SCAN	     -2
#define ERROR		      0
#define	NO_ERROR	      0
#define NOISE 		      1
#define AVERAGE		      2
#define LEFT 		      1
#define RIGHT		      2
#define SRCLEN 		     16

#define OTF_GRAPH	      1
#define TILT_GRAPH	      2
#define OPTICAL_GRAPH	      3

#define MYSTERY_BE	      0
#define DIGITAL_BE	      1
#define SPECTRAL_BE	      2
#define HOLOG_BE	      3

#define DBE_FIVE	      1
#define SPEC_FIVE	      2

#define LOAD_DBE              1
#define LOAD_FBS              2
#define LOAD_AOS              4
#define LOAD_ALL              7

#define MAX_MAIL_BOXES       10
#define MAX_BAD_CHAN         20

#define MAX_IF_NUMS	     20

#define WE_DONT_DO_HOLOGRAPHY 1

#define BACKEND_FFB           0
#define BACKEND_AOS           1
#define BACKEND_CTS           2
#define BACKEND_DBE           3
#define BACKEND_MAX           4
