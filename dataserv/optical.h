#ifndef OPTICAL_H

#define NPARM 3
#define YES		1
#define NO		0
#define TRUE		1
#define FALSE		0
#define MAX_BOXSIZE	60
#define MAX_PIXELS	MAX_BOXSIZE * MAX_BOXSIZE
#define PLATE_X 	(0.660)
#define PLATE_Y 	(0.660)
#define ASPECT_RATIO 	1.0
#define SKIP_X 		10


struct BOX_DATA {
  struct HEADER *head;
  int origin_x,
      origin_y,
      bs,
      bsq,
      bs_x,
      bs_y,
      fgbcol,
      fgbrow,
      num_of_peaks,
      where_peak,
      peak_x,
      peak_y,
      found_it,
      nofinds;

  float
      elev,
      rms,
      raw_peak,
      s2n,
      platexx,
      plateyy,
      gauss_xpeak,
      gauss_ypeak,
      gauss_x,
      gauss_y,
      gauss_xw,
      gauss_yw,
      gauss_x_a,
      gauss_x_b,
      gauss_y_a,
      gauss_y_b,
      datamin,
      datamax,
      x_array[MAX_BOXSIZE],
      y_array[MAX_BOXSIZE],
      *data;
};

#endif
#define OPTICAL_H
