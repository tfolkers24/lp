#include "ds_colors.h"

#ifdef DEFINE_COLORS

char *ds_colorsTxt[] = {"white",   "black",	  "gray40",	"gray50",
			"gray75",  "red",         "yellow",     "green",
			"magenta", "blue",        "maroon",	"orange",
			"cyan",    "coral",       "purple",     "salmon",
			"brown",   "orchid",      "lavender",	"steel blue",
			"tan",     "sienna",      "pink",       "cyan4"
};

#else

extern char *ds_colorsTxt[];

#endif

#define XGR_MAIN_WIN            1
#define XGR_OTF_WIN             2
#define XGR_SPEC_WIN            3
#define XGR_STAT_WIN            4

#define XGR_SET_PIXMAP  	1
#define XGR_SET_WIN     	2

#define XGR_NO_PIXMAP		0
#define XGR_USE_PIXMAP		1

#define XGR_PLOT_DEGREES	1
#define XGR_PLOT_SEX		2

#define XGR_MAX_FONTS		15

struct XGR_HEAD {
	struct XGR_HEAD *next;
	struct XGR_HEAD *prev;
};



struct WINDOW_HEAD {
	struct WINDOW_HEAD *next;
	struct WINDOW_HEAD *prev;
};


struct WINDOW {
	struct WINDOW_HEAD windows;/* MUST ALWAYS be FIRST struct element !!! */
	char    frameTitle[80];
	Window  act;
	Window  win;
        int     usPixmap;
	Pixmap  pixmap;
	Pixmap  icon;
        GC      gc;
        int     canw,
	        canh;
	int     foreground;
	int     background;
	int	pixmapX,
		pixmapY;
	int     plotxb, plotxe,
	        plotyb, plotye;
	int     invalid;
	int     first;
	int     mapped;
	int     (*repaint_proc)();
	int     (*resize_proc)();
	int     (*event_proc)();
};



struct XGR_ {
	struct XGR_HEAD list;	/* MUST ALWAYS be FIRST struct element !!!!!! */
	char prgName[80];
	char serverName[80];
        Display *dpy;
	int xfd;
        int scr_num;
	int depth;
	Colormap def_cmap;
	char **fontNames;
        int fontW, fontH;
	unsigned long colors[MAX_DS_COLORS];
	unsigned long grayScale[MAX_GRAY_SCALE];
	int numFonts;
	XImage      *userXimage;
        XFontStruct *font_info;
        XFontStruct *fontInfo[XGR_MAX_FONTS];
	int invalidServerFonts;

	struct WINDOW_HEAD windows;
	struct WINDOW *cur;

	struct WINDOW *plotwin;
	struct WINDOW *otfwin;
	struct WINDOW *specwin;
	struct WINDOW *statwin;
};
