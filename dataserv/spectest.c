#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>
#include <X11/keysym.h>

#include "header.h"
#include "otf.h"
#include "caclib_proto.h"


struct SPECDATA sd;

struct SOCK *specdata;

int main(argc, argv)
int argc;
char *argv[];
{
  double s;

  if(argc < 2)
  {
    fprintf(stderr, "Usage: %s xxxx.xx\n", argv[0]);
    return(1);
  }

  smart_log_open("spectest", 2);

  sock_bind("ANONYMOUS");

  specdata = sock_connect("LINUXPOPS_SPECDATA");

  bzero((char *)&sd, sizeof(sd));

  strcpy(sd.datafile, "/home/data/ssk/sdd_ffb.ssk_117");
  strcpy(sd.gainsfile, "/home/data/ssk/gsdd_ffb.ssk_117");

  s = atof(argv[1]);

  sd.scanno       = (int)s;
  sd.feed         = (int)((s*100.0) - (floor(s)*100.0));
  sd.singleChan   = 256;
  sd.singleOtfBin = 4;
  sd.debugSpectra = 0;
  sd.doTPprocess  = 1;
  sd.arrayNum     = 2;

  log_msg("scan = %d, feed = %d", sd.scanno, sd.feed);

  sock_write(specdata, (char *)&sd, sizeof(sd));

  return(0);
}
