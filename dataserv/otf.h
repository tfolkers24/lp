#ifndef OTF_H
#define OTF_H

#define OTFAUTO        0
#define OTFTIME        1
#define OTFRA          2
#define OTFDEC         3
#define OTFAZ          4
#define OTFEL          5

#define NORMAL_MAP     1
#define INTERP         2
#define CHANNEL_MAP    3
 
#define LOAD_PREVIEW   1
#define LOAD_STEP      2
#define LOAD_FAST      3

#define OTFPLOTXB       0.09	/* was 0.11 */
#define OTFPLOTXE       0.995
#define OTFPLOTYB       0.18	/* was .18 */
#define OTFPLOTYE       0.995

#ifndef USE_210x210_WORK_ARRAY

#define WORK_ARRAY_X		 210
#define WORK_ARRAY_X_CENT	 100
#define WORK_ARRAY_X_M		-100
#define WORK_ARRAY_X_P		 100

#define WORK_ARRAY_Y	 	 210
#define WORK_ARRAY_Y_CENT	 100
#define WORK_ARRAY_Y_M		-100
#define WORK_ARRAY_Y_P		 100

#else

#define WORK_ARRAY_X		1010
#define WORK_ARRAY_X_CENT	 500
#define WORK_ARRAY_X_M		-500
#define WORK_ARRAY_X_P		 500

#define WORK_ARRAY_Y	 	 32
#define WORK_ARRAY_Y_CENT	 14
#define WORK_ARRAY_Y_M		-14
#define WORK_ARRAY_Y_P		 14

#endif


#define PLOT_INT_INTENSITY	   0
#define PLOT_RMS		   1
#define PLOT_TSYS		   2
#define PLOT_TIME		   3


	/* struct sent to specdata to plot the current scan */
struct SPECDATA {
	char magic[16];

	char datafile[256];
	char gainsfile[256];

	int  scanno;
	int  feed;

	int  bchan;
	int  echan;

	int  bline;
	int  debugSpectra;

	int  singleChan;
	int  singleOtfBin;

	int  doTPprocess;
	int  arrayNum;

	int  cols;
	int  rows;

	double nyquistRa;
	double nyquistDec;
};



struct SCAN_HEADERS {
	int   ver;
        float scan;

	struct HEADER head;

        struct SCAN_HEADERS *next;
};


struct MAP_SCANS {
	int   ver;
        float scan;

        struct MAP_SCANS *next;
};

struct OTF_GRID {
	int    shade;
	int    oldshade;
	int    set;
	int    col;
	int    row;
	int    artificial;	/* cell that's be synthizied from surounding data */
	int    bdirty; 		/* Data dirty, so compute baselines */
	int    ddirty; 		/* Data dirty, so compute new shade */
	int    version[8];

	float  scanct;
	float  plotVal1;
	float  plotVal2;
	float  intInten;
	float  kms;
	float  rms;
	float  *chans;
	float  a;
	float  b;
	float  bscan;
	float  escan;
	float  time;

	float  tsys;
	float  wt;

	char   source[32];

	struct HEADER *head;

        struct OTF_GRID *next;
};
 

struct GRID_ACCUM {
	int col;
	int row;
	int n;

	double *chans;
	double wt;
};
 

struct OTF_STRUCT {
	int     initialized;
	int     ignoreAllRepaints;
	int     cutoff;
	int     lockGrid;
	int     fixBadChan;
	int     bins;
	int     plots;
	int     singleChan;
	int     arrayNum;
	int     singleOtfBin;
	int     debugSpectra;
	int     reqSpecIndx;
	int     noPlot;
	int     newShade;
	int     requestCol;
	int     requestRow;
	int     col;
	int     row;
	int     cols;
	int     rows;
	int     noint;
	int     numSpectra;
	int     int_chans[2];
	int     lockScale;
	int     doContour;
	int     valueMaxChanged;
	int     intenChanged;
	int     loadChoice;
	int     lastAction;
	int     noColorBar;
	int     colorChoice;
	int     doingMosaic;
	int     chanSelOTF;
	int     gridBaseLine;
	int     windowSizeSet;
	int	manConfig;
	int     placeBeamSize;
	int     regionshow;
	int     regionshowbold;
	int     regionlabelshow;
	int     allowVelShift;
	int     armBeamLocation;

	int     valueMinX;
	int     valueMinY;
	int     valueMaxX;
	int     valueMaxY;

        int     numCellsTotal;
	int     numCellsUsed;

	int	whichPlot;	/* What to plot: Inten, tsys, rms, time, etc */

	int     bdirty;		/* One or more baselines needs calc by Baseline thread */
	int     ddirty;		/* One or more integration values needs calc by intensity thread */

	int     baselines[8];

	long    sizeofWorkArray;

	float   scaleFactor;
	float   gamma;
	float   IFNumber;
	float   nyquistRa;
	float   nyquistDec;
	float   valueMin;
	float   valueMax;
	float   *ondata;
	float   *offdata;
	float   *gainsdata;
	float   plotxb;
	float   plotyb;
	float   plotxe;
	float   plotye;
	float  *gridArrayChans;

	float	manMapRa;
        float   manMapDec;
        float   manMapWidth;
        float   manMapHeight;
        float   manNyquistRa;
        float   manNyquistDec;
        float   chanCompress;
	float   freqres;
	float   restfreq;
	float   velocity;
	float   xsource;
	float   ysource;
	float   beamRa;
	float   beamDec;

	char    peakStr[80];

	struct  MAP_SCANS *map_scans_head;
	struct  OTF_GRID **gridArray;
	struct  HEADER *head;
	XImage  *ximage;
};

#ifdef DEFINE_OTF_STRUCT
struct OTF_STRUCT otf;
#else
extern struct OTF_STRUCT otf;
#endif

#endif
