
#define  TRUE   1
#define  FALSE  0

#define DEFINE_COLORS	TRUE
#include "ds_extern.h"
#include "ds_proto.h"

static int font0 = FONT_SIZE_SMALL;
static int font1 = FONT_SIZE_NORMAL;
static int font2 = FONT_SIZE_BIG;

int doColor = DS_PSEUDO;
int mcount  = 0;

int xntick, yntick;
int doFocal = 0;
int noXTick = 0;
int noYTick = 0;
int noPlot  = 0;
int xgr_hardFont = 0;
int fixedZero = 0;

static float xtickb, xticke, xinterval, ytickb, yticke, yinterval;

extern float globalStart, globalStop;
float globalYMin, globalYMax;

int   globalScale, drawGrid, currentColor;

int   lockScale = 0;

static int local_malloc();
extern int focalTicks;

struct XGR_HEAD xgr_head;

extern char prgName[80];

static unsigned long getColor();
static int drawXticks(), drawYticks(), tickGet(), find_ticks();
static int setColor(), allocColors();

/*
 * insque( entry, predecessor ) macro
 *
 *  The queue entry specified by the entry pointer "e" is inserted following
 *  the member specified by the predecessor pointer "p".
 *
 */
#define insque(e,p) (e)->next=((e)->prev=(p))->next; \
                    (p)->next=(p)->next->prev=(e)

/*
 * remque( entry ) macro
 *
 *  The queue entry specified by the entry pointer "e" is removed.
 *
 */
#define remque(e)   (e)->prev->next=(e)->next; \
                    (e)->next->prev=(e)->prev; \
                    (e)->next = (e)->prev = (e)

#define MAX_GRAY_SCALE 256

int max_gray_scale  = MAX_GRAY_SCALE;
int newMaxGrayScale = MAX_GRAY_SCALE;

struct DS_RGB colormap[MAX_GRAY_SCALE];

int xgr_graphInit(s)
struct XGR_ *s;
{
  newMaxGrayScale = MAX_GRAY_SCALE;

  allocColors(s);

  XSetWindowBackground(s->dpy, s->cur->win, s->colors[s->cur->background]);

  XSetBackground(s->dpy, s->cur->gc, s->colors[s->cur->background]);

  XClearWindow(s->dpy, s->cur->win);

  globalScale = 1;
  globalStart = 1.0;
  globalStop  = (double)MAX_GRAY_SCALE;

  return(0);
}



int xgr_setNumColors(n)
int n;
{
  if( n == 16 || n == 32 || n == 64 || n == 128 || n == 256)
  {
    newMaxGrayScale = n;
    xgr_setColorChoice(doColor);
  }

  return(0);
}


int xgr_setColorChoice(c)
int c;
{
  struct XGR_HEAD *p;
  struct XGR_ *s;
 
  doColor = c;
  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;
    allocColors(s);
  }

  return(0);
}



static int allocColors(s)
struct XGR_ *s;
{
  int i;

  if(s->def_cmap)
  {
    XFreeColors(s->dpy, s->def_cmap, s->grayScale, max_gray_scale, AllPlanes);
  }

  max_gray_scale = newMaxGrayScale;

  log_msg("allocColors(): Allocating %d colors for %s", max_gray_scale, DisplayString(s->dpy));

  if(!s->colors[0])
  {
    for(i=0;i<MAX_DS_COLORS;i++)
    {
      s->colors[i] = getColor(s, ds_colorsTxt[i], 1, 0, 0, 0);
    }
  }

  if(doColor == DS_PSEUDO)
  {
    loadPseudoScale(s);
  }
  else
  if(doColor >= DS_FLAME1)
  {
    loadFlameScale(s, doColor);
  }
  else
  {
    loadGrayScale(s);
  }

  log_msg("allocColors(): Done");

  return(0);
}




int loadGrayScale(s)
struct XGR_ *s;
{
  int i, r, g, b;
  char buf[80];
  int fac;

  fac = 256 / max_gray_scale;
  for(i=0;i<max_gray_scale;i++)
  {
    r = i*fac;
    g = i*fac;
    b = i*fac;

    sprintf(buf, "gray%d", i*fac);
    s->grayScale[i] = getColor(s, buf, 0, i*fac, i*fac, i*fac);

    colormap[i].r = r;
    colormap[i].g = g;
    colormap[i].b = b;
  }

  return(0);
}



int flame(a, b, x)
double a, b, x;
{
  int ret=0;

  if( x > b)
  {
    ret = (int)rint(a * sqrt(x-b));

    if(ret > 255)
    {
      ret = 255;
    }
  }

  return(ret);
}



int loadFlameScale(s, cindx)
struct XGR_ *s;
int cindx;
{
  int r,g,b, i, k, dx;
  char buf[80];
  static double aa[] = { 17.084, 18.80, 29.456 };
  static double bb[] = { 0.0,    115.0, 202.0 };

  k = 0;
  dx = MAX_GRAY_SCALE / max_gray_scale;

  for(i=0;i<MAX_GRAY_SCALE;i+=dx)
  {
    switch(cindx) {
      case DS_FLAME1:
        r =  flame(aa[0], bb[0], (double)i);
        g =  flame(aa[1], bb[1], (double)i);
        b =  flame(aa[2], bb[2], (double)i);
        break;

      case DS_FLAME2:
        r =  flame(aa[0], bb[0], (double)i);
        b =  flame(aa[1], bb[1], (double)i);
        g =  flame(aa[2], bb[2], (double)i);
        break;

      case DS_FLAME3:
        g =  flame(aa[0], bb[0], (double)i);
        r =  flame(aa[1], bb[1], (double)i);
        b =  flame(aa[2], bb[2], (double)i);
        break;

      case DS_FLAME4:
        b =  flame(aa[0], bb[0], (double)i);
        g =  flame(aa[1], bb[1], (double)i);
        r =  flame(aa[2], bb[2], (double)i);
        break;

      case DS_FLAME5:
        g =  flame(aa[0], bb[0], (double)i);
        b =  flame(aa[1], bb[1], (double)i);
        r =  flame(aa[2], bb[2], (double)i);
        break;

      case DS_FLAME6:
        b =  flame(aa[0], bb[0], (double)i);
        r =  flame(aa[1], bb[1], (double)i);
        g =  flame(aa[2], bb[2], (double)i);
        break;
    }

    colormap[i].r = r;
    colormap[i].g = g;
    colormap[i].b = b;

    sprintf(buf, "color%d", i);
    s->grayScale[k++] = getColor(s, buf, 0, r, g, b);
  }

  return(0);
}


int loadPseudoScale(s)
struct XGR_ *s;
{
  int r,g,b, i, k=0, dx;
  int b1, b2;
  char buf[80];

  b1 = 85;
  b2 = 180;
  dx = 256 / max_gray_scale;

  for(i=0;i<MAX_GRAY_SCALE;i+=dx)
  {
    if(i <= b1)
    {
      r = 0;
      b = 255;
      g = (int)((float)i * 255.0 / (float)b1);
    }
    else
    if(i > b1 && i < b2)
    {
      r = (int)((float)(i-b1) * 255.0 / (float)(b2-b1));
      g = 255;
      b = 255 - r;
    }
    else
    {
      r = 255;
      g = 255 - (int)((float)(i-b2) * 255.0 / (float)(255-b2));
      b = 0;
    }

    colormap[i].r = r;
    colormap[i].g = g;
    colormap[i].b = b;

    sprintf(buf, "color%d", i);
    s->grayScale[k++] = getColor(s, buf, 0, r, g, b);
  }

  return(0);
}


static int loadFont(s, fn)
struct XGR_ *s;
int fn;
{
  char errBuf[256];

  if(s->invalidServerFonts)
    return(0);

  setFrameFooter("Stand-by, Loading Fonts", LEFT);
  if((  s->fontInfo[fn] = XLoadQueryFont(s->dpy, s->fontNames[fn])) == NULL)
  {
    sprintf( errBuf,"Cannot open %s font", s->fontNames[fn]);
    log_msg("loadFont(): %s", DisplayString(s->dpy), errBuf);
    setFrameFooter(errBuf, RIGHT);
    s->invalidServerFonts = 1;
    return(0);
  }
  setFrameFooter("", LEFT);

  return(1);
}


int xgr_freeWindows(s)
struct XGR_ *s;
{
  struct WINDOW_HEAD *p2;
  struct WINDOW *w;
 
  if(!s)
    return(1);

  if(s->userXimage)
    XDestroyImage(s->userXimage);

  for(p2=s->windows.next;p2!=&s->windows;p2=p2->next)
  {
    w = (struct WINDOW *)p2;
    if(w->usPixmap && w->pixmap)
    {
      XFreePixmap(s->dpy, w->pixmap);
    }

    free((char *)w);
  }
  if(s->numFonts)
    if(s->fontNames)
      free((char *)s->fontNames);

  free((char *)s);

  return(0);
}



int xgr_setWindowSize(name, x, y)
char *name;
float x, y;
{
  struct XGR_HEAD *p;
  struct XGR_ *s;
  struct WINDOW_HEAD *p2;
  struct WINDOW *s2;
  int w, h, ww, x1, yy1;
  float fac;
  XSizeHints size_hints;
  static int oldw=-1, oldh=-1;

  log_msg("xgr_setWindowSize(): X = %f, y = %f", x, y);
 
  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;

    for(p2=s->windows.next;p2!=&s->windows;p2=p2->next)
    {
      s2 = (struct WINDOW *)p2;

      if(!strcmp(s2->frameTitle, name))
      {
	ww = WidthOfScreen(XDefaultScreenOfDisplay(s->dpy));

        log_msg("WidthOfScreen(): returns %d", ww);


        if( x == 1.0 && y == 1.0)
	{
	  w = 450;
	  h = 400;
	  x1 = ww -w -6;
	  yy1 = 23;
	}
	else
	{
	  fac = x / y;

	  if( fac < 1.0 )				   /* tall narrow map */
	  {
	    h = 600;
	    w = (int)((float)h * fac);
	  }
	  else						    /* wide short map */
	  {
	    w = 800;
	    h = (int)((float)w / fac);
	  }

	  x1 = ww -w -6;
	  yy1 = 23;
	}

        if(!strcmp(name, "Spectral Plot Window"))
        {
          x1  = 800;
	  yy1 = 0;
          w   = (int)x;
          h   = (int)y;
        }

        if(!strcmp(name, "OTF Gridder"))
        {
          x1  = 1000;
	  yy1 = 100;
//          w   = (int)x;
//          h   = (int)y;
        }

        if(w != s2->canw || h != s2->canh)
	{
	  if(w != oldw && h != oldh)
	  {
	    log_msg("xgr_setWindowSize(): Setting %s window to %d %d %d %d", name, x1, yy1, w, h);
	    oldw = w; oldh = h;
	  }

	  size_hints.flags        = PMinSize | PSize;
	  size_hints.min_width    = w / 4;
	  size_hints.min_height   = h / 4;
	  size_hints.width    	  = w;
	  size_hints.height   	  = h;

	  XSetWMNormalHints(s->dpy, s2->win, &size_hints);

	  XResizeWindow(s->dpy, s2->win, w, h);

	  XFlush(s->dpy);
	}

	break;
      }
    }
  }

  return(0);
}


int xgr_setCurWindow(win, which)
int win;
int which;
{
  struct XGR_HEAD *p;
  struct XGR_ *s;
  struct WINDOW_HEAD *p2;
 
  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;

    for(p2=s->windows.next;p2!=&s->windows;p2=p2->next)
    {
      switch(win)
      {
	case XGR_MAIN_WIN: s->cur = s->plotwin; break;
	case XGR_OTF_WIN:  s->cur = s->otfwin;  break;
	case XGR_SPEC_WIN: s->cur = s->specwin; break;
	case XGR_STAT_WIN: s->cur = s->statwin; break;

	default:
	fprintf(stderr, "Unknown Window: %d; exiting\n", win);
        exit(1);
	break;
      }

      if(which == XGR_SET_PIXMAP && s->cur->usPixmap)
      {
        s->cur->act = s->cur->pixmap;
//	log_msg("xgr_setCurWindow(): Setting Pixmap %p Active for Window %p", s->cur->pixmap, s->cur->act);
      }
      else
      {
        s->cur->act = s->cur->win;
//	log_msg("xgr_setCurWindow(): Setting Window %p Active for Window %p", s->cur->win, s->cur->act);
      }

      break;
    }
  }

  return(0);
}



int xgr_pixmapToWindow(name)
char *name;
{
  int ret = 1;
  struct XGR_HEAD *p;
  struct XGR_ *s;
  struct WINDOW_HEAD *p2;
  struct WINDOW *s2;
 
  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;

    for(p2=s->windows.next;p2!=&s->windows;p2=p2->next)
    {
      s2 = (struct WINDOW *)p2;

      if(!strcmp(s2->frameTitle, name) && s2->usPixmap)
      {
//        log_msg("xgr_pixmapToWindow(): Copying Pixmap %p to Window %p", s2->pixmap, s2->win);

        XCopyArea(s->dpy, s2->pixmap, s2->win, s2->gc,
			0, 0, (unsigned int)s2->canw, 
			      (unsigned int)s2->canh,
			0, 0);
	s->cur->act = s->cur->win;

	ret = 0;

	break;
      }
    }
  }

  return(ret);
}


int xgr_mapUnmapWindow(name, map)
char *name;
int map;
{
  struct XGR_HEAD *p;
  struct XGR_ *s;
  struct WINDOW_HEAD *p2;
  struct WINDOW *s2;
 
  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;
    for(p2=s->windows.next;p2!=&s->windows;p2=p2->next)
    {
      s2 = (struct WINDOW *)p2;
      if(!strcasecmp(s2->frameTitle, name))
      {
        if(map == 0)
 	  XIconifyWindow(s->dpy, s2->win, s->scr_num);
	else
	{
	  XMapWindow(s->dpy, s2->win);
	  XMapRaised(s->dpy, s2->win);
	}
	break;
      }
    }
  }

  return(0);
}



int xgr_findWindow(dpy, win, w)
Display *dpy;
Window win;
struct WINDOW **w;
{
  struct XGR_HEAD *p;
  struct XGR_ *s;
  struct WINDOW_HEAD *p2;
  struct WINDOW *s2;
 
  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;

    if(s->dpy == dpy)
    {
      for(p2=s->windows.next;p2!=&s->windows;p2=p2->next)
      {
        s2 = (struct WINDOW *)p2;

	if(s2->win == win)
	{
	  *w = s2;

	  return(0);
	}
      }
    }
  }

  w = (struct WINDOW **)NULL;

  return(1);
}



int xgr_putXimage(win_name, fx, fy, w, h, data)
char *win_name;
float fx, fy;
int  w, h;
unsigned char *data;
{
  struct XGR_HEAD *p;
  struct XGR_ *s;
  struct WINDOW_HEAD *p2;
  struct WINDOW *s2;
  int i, x, y;
 
  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;

    if(s->userXimage)
    {
      XDestroyImage(s->userXimage);
      s->userXimage = NULL;
    }

    if(!(s->userXimage=createXimage(s->dpy, w, h, s->depth)))
    {
      log_msg("xgr_putXimage(): Error in create of pixmap for %s\n", DisplayString(s->dpy));
      s->userXimage = NULL;
      return(1);
    }

    if(s->depth == 8)
    {
      for(i=0;i<w*h;i++)
        s->userXimage->data[i] = (unsigned char)s->grayScale[(int)data[i]];
    }
    else
    {
      for(i=0,y=0;y<h;y++)
        for(x=0;x<w;x++,i++)
          XPutPixel(s->userXimage, x, y, s->grayScale[(int)data[i]]);
    }

    for(p2=s->windows.next;p2!=&s->windows;p2=p2->next)
    {
      s2 = (struct WINDOW *)p2;
      if(strstr(s2->frameTitle, win_name) && s2->act)
      {
        x = (int)(fx * (float)s2->canw);
        y = (int)(fy * (float)s2->canh);
        XPutImage(s->dpy, s2->act, s2->gc, s->userXimage, 0, 0, x, y, w, h);
      }
    }
  }

  return(0);
}




int xgr_reSizeProc(window, width, height)
Window window;
int width, height;
{
  struct XGR_HEAD *p;
  struct XGR_ *s;
  struct WINDOW_HEAD *p2;
  struct WINDOW *s2;

  log_msg("xgr_reSizeProc(): Window Resizing: %d x %d", width, height);
 
  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;
    for(p2=s->windows.next;p2!=&s->windows;p2=p2->next)
    {
      s2 = (struct WINDOW *)p2;
      if(s2->win == window)
      {
        if(s2->canw != width || s2->canh != height)
        {
	  if(s2->usPixmap)
	  {
            if(s2->pixmap && s2->pixmap)
	    {
	      log_msg("xgr_reSizeProc(): Freeing Window %p, Pixmap %p", s2->win, s2->pixmap);

              XFreePixmap(s->dpy, s2->pixmap);

	      s2->pixmap = 0;
	    }

            if(!(s2->pixmap=XCreatePixmap(s->dpy, s2->win, (unsigned int)width, (unsigned int)height, s->depth)))
            {
              log_msg("xgr_reSizeProc(): Error in create of pixmap for %s\n", DisplayString(s->dpy));
              s2->usPixmap = 0;
            }
	    else
	    {
              XSetForeground(s->dpy, s->cur->gc, s->colors[DS_BLACK]);
              XFillRectangle(s->dpy, s2->pixmap, s->cur->gc, 0, 0, (unsigned int)s->cur->canw, (unsigned int)s->cur->canh);
	      log_msg("xgr_reSizeProc(): New Pixmap, %p for Window %p", s2->pixmap, s2->win);
	    }
	  }
        }

	s2->canw = width;
	s2->canh = height;
	s2->invalid = 1;

	if(s2->resize_proc)
	{
	  log_msg("xgr_reSizeProc(): Calling resize proc for win: %p", window);
	  return(s2->resize_proc(s2->win));
	}
      }
    }
  }

  return(1);
}



int xgr_eventProc(event, window, mask)
XEvent *event;
Window window;
unsigned int mask;
{
  struct XGR_HEAD *p;
  struct XGR_ *s;
  struct WINDOW_HEAD *p2;
  struct WINDOW *s2;
 
  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;
    for(p2=s->windows.next;p2!=&s->windows;p2=p2->next)
    {
      s2 = (struct WINDOW *)p2;

      if(s2->win == window)
      {
        if(s2->event_proc)
        {
	  return(s2->event_proc(event, mask));
	}
      }
    }
  }

  return(1);
}



int xgr_rePaintProc(window)
Window window;
{
  struct XGR_HEAD *p;
  struct XGR_ *s;
  struct WINDOW_HEAD *p2;
  struct WINDOW *s2;
 
  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;
    for(p2=s->windows.next;p2!=&s->windows;p2=p2->next)
    {
      s2 = (struct WINDOW *)p2;
      if(s2->win == window)
      {
        if(s2->first == 1)
        {
          s2->first=0;
          return(0);
        }
	s2->invalid = 1;
	return(s2->repaint_proc(s2->win));
      }
    }
  }

  return(1);
}



struct WINDOW *xgr_getWin(win)
Window win;
{
  struct XGR_HEAD *p;
  struct XGR_ *s;
  struct WINDOW_HEAD *p2;
  struct WINDOW *s2;
 
  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;
    for(p2=s->windows.next;p2!=&s->windows;p2=p2->next)
    {
      s2 = (struct WINDOW *)p2;
      if(s2->win == win)
      {
	return(s2);
      }
    }
  }

  return((struct WINDOW *)NULL);
}


int xgr_hardCopy(win, land, rev, file)
char *win, *file;
int land, rev;
{
  struct XGR_HEAD *p;
  struct XGR_ *s;
  struct WINDOW_HEAD *p2;
  struct WINDOW *s2;
 
  if(win == NULL || file == NULL)
    return(1);

  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;
    for(p2=s->windows.next;p2!=&s->windows;p2=p2->next)
    {
      s2 = (struct WINDOW *)p2;
      if(!strncasecmp(s2->frameTitle, win, strlen(win)))
      {
        xgr_mapUnmapWindow(s2->frameTitle, 1);
	XFlush(s->dpy);
//        writePS(s->dpy, s2->win, s->scr_num, 0, 0, s2->canw, s2->canh,
//			land, rev, s2->frameTitle, file);

	fprintf(stderr,"PRINT_DONE\n");

	return(0);
      }
    }
  }

  return(1);
}


int xgr_makeGif(win, file)
char *win, *file;
{
  struct XGR_HEAD *p;
  struct XGR_ *s;
  struct WINDOW_HEAD *p2;
  struct WINDOW *s2;
 
  if(win == NULL || file == NULL)
    return(1);

  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;
    for(p2=s->windows.next;p2!=&s->windows;p2=p2->next)
    {
      s2 = (struct WINDOW *)p2;
      if(!strncasecmp(s2->frameTitle, win, strlen(win)))
      {
        xgr_mapUnmapWindow(s2->frameTitle, 1);
	XFlush(s->dpy);
//	DumpScreen2Gif(file, GIF_DUMP_X_WINDOW, s2->win,
//			(int)s->dpy, s->def_cmap, s2->canw, s2->canh);
	fprintf(stderr,"GIF_DONE\n");

	return(0);
      }
    }
  }

  return(1);
}


int xgr_clearScreen()
{
  struct XGR_HEAD *p;
  struct XGR_ *s;
 
  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;

    if(!s->cur->invalid) 
    {
      continue;
    }

    if(s->cur->act)
    {
//      log_msg("xgr_clearScreen(): Clearing Window: %p", s->cur->act);

      XSetForeground(s->dpy, s->cur->gc, s->colors[s->cur->background]);
      XFillRectangle(s->dpy, s->cur->act, s->cur->gc, 0, 0,
		 (unsigned int)s->cur->canw, (unsigned int)s->cur->canh);
    }
  }

  return(0);
}


int xgr_clearArea(x, y, dx, dy)
float x, y, dx, dy;
{
  struct XGR_HEAD *p;
  struct XGR_ *s;
  int x1, x2, yy1, w1, h1;
 
  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;
    if(!s->cur->invalid) 
      continue;

    x1  = (int)( (float)s->cur->canw * x);
    x2  = (int)( (float)s->cur->canw * dx);
    w1  = x2 - x1;
    yy1 = s->cur->canh - (int)( (float)s->cur->canh * dy);
    h1 = yy1;
    
    if(s->cur->act)
    {
      XSetForeground(s->dpy, s->cur->gc, s->colors[s->cur->background]);
      XFillRectangle(s->dpy, s->cur->act, s->cur->gc, x1, yy1, 
			(unsigned int)w1, (unsigned int)h1);
    }
  }

  return(0);
}


          /* most of this code was shamelessly stolen from John Bradley's xv  */
int find_close_color(dpy, cmap, exact_color, color)
Display *dpy;
Colormap cmap;
XColor *exact_color;
char *color;
{
  int      scn, ncells, i, j;
  XColor   ctab[256];
  int      dc, d, mdist, close;
  int      rd, gd, bd, ri, gi, bi;

  scn    = DefaultScreen(dpy);
  ncells = DisplayCells(dpy, scn);

                   /* read entire colormap (or first 256 entries) into 'ctab' */
  dc = (ncells<256) ? ncells : 256;
  for(i=0;i<dc;i++)
     ctab[i].pixel = (unsigned long)i;

  XQueryColors(dpy, cmap, ctab, dc);

  mdist = 1000000;
  close = -1;
  ri = exact_color->red>>8;
  gi = exact_color->green>>8;
  bi = exact_color->blue>>8;

  for(j=0;j<dc;j++)
  {
    rd = ri - (ctab[j].red  >>8);
    gd = gi - (ctab[j].green>>8);
    bd = bi - (ctab[j].blue >>8);


    d = rd*rd + gd*gd + bd*bd;
    if(d<mdist)
    {
      mdist=d;
      close=j;
    }
  }
  if(close<0)                         /* This Can't Happen! (How reassuring.) */
  {
    log_msg("find_close_color(): Can't allocate %s", color );

    if(strstr(color, "black"))
      return(XBlackPixel(dpy, scn));
    else
      return(XWhitePixel(dpy, scn));
  }
  if( XAllocColor( dpy, cmap, &ctab[close]))
    return(ctab[close].pixel);
  else
  {
    log_msg("find_close_color(): Can't allocate %s", color );

    if(strstr(color, "black"))
      return(XBlackPixel(dpy, scn));
    else
      return(XWhitePixel(dpy, scn));
  }

  return(0);
}



static unsigned long getColor(s, color, flag, red, green, blue)
struct XGR_ *s;
char *color;
int flag, red, green, blue;
{
  XColor exact_color;

  if(!s->def_cmap)
  {
    s->def_cmap = DefaultColormap(s->dpy, s->scr_num);
  }

  if(flag)
  {
    if( !XParseColor( s->dpy, s->def_cmap, color, &exact_color ) )
    {
      log_msg("getColor(): %s is not in %s color database", DisplayString(s->dpy), color);

      if(strstr(color, "black"))
      {
        return(XBlackPixel(s->dpy, s->scr_num));
      }
      else
      {
        return(XWhitePixel(s->dpy, s->scr_num));
      }
    }
  }
  else
  {
    exact_color.red   = red * 255;
    exact_color.green = green * 255;
    exact_color.blue  = blue * 255;
    exact_color.flags = DoRed | DoGreen | DoBlue;
  }

  if( !XAllocColor( s->dpy, s->def_cmap, &exact_color ) )
  {
    log_msg("getColor(): Can't allocate %s for %s\n", color, DisplayString(s->dpy));

    return(find_close_color(s->dpy, s->def_cmap, &exact_color, color));
  }

  return(exact_color.pixel);
}



static int setColor(s, color)
struct XGR_ *s;
int color;
{
  if(!s->cur->invalid || s->cur->foreground == color)
  {
    return(0);
  }

  if(color>=0)
  {
    XSetForeground(s->dpy,s->cur->gc, s->colors[color] );
  }
  else
  {
    XSetForeground(s->dpy,s->cur->gc, s->grayScale[color * -1] );
  }

  s->cur->foreground = color;

  currentColor = color;

  return(0);
}




int xgr_drawLine(xdata, ydata, n, color)
float    *xdata;
float    *ydata;
int      n, color;
{
  struct XGR_HEAD *p;
  struct XGR_     *s;
  static XPoint *temp=NULL;
  int i,nt;
  float MaxY, MinY, MaxX, MinX;
  float xmax, xmin, ymax, ymin;
  extern int zerodata;
 
  if(debug)
  {
    log_msg("xgr_drawLine()");
  }

  if( (temp = (XPoint *)malloc((unsigned)(n*sizeof(XPoint))) ) == NULL)
  {
    log_perror("malloc(temp)");

    return(1);
  }

  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;

    if(!s->cur->invalid) 
    {
      continue;
    }
 
    setColor(s, color);

    MaxY = (float)s->cur->plotye;
    MinY = (float)s->cur->plotyb;
    MaxX = (float)s->cur->plotxe;
    MinX = (float)s->cur->plotxb;
    nt = 4;

    if(lockScale)
    {
      lockScale = 0;
    }
    else
    {
      tickGet(xdata,n, &xtickb, &xticke, &xntick, &xinterval, &xmin, &xmax);

      if(!zerodata)
      {
        tickGet(ydata,n, &ytickb, &yticke, &yntick, &yinterval, &ymin, &ymax);

        globalYMin = ytickb;
        globalYMax = yticke;
      }
      else
      {
        ytickb    = -1.0;
        yticke    =  1.0;
        yntick    = 4;
        yinterval = 2.0 / 6.0;
        ymin      = -1.0;
        ymax      =  1.0;
      }

      drawXticks(s, nt);

      doFocal = 0;

      drawYticks(s);
    }

    if(!noPlot)
    {
      for(i=0;i<n;i++)
      {
        temp[i].x = (int)(MinX+((MaxX-MinX)/(xmax  -xmin  ))*(xdata[i]-xmin  ));
        temp[i].y = (int)(MaxY-((MaxY-MinY)/(yticke-ytickb))*(ydata[i]-ytickb));
      }

      XDrawLines(s->dpy, s->cur->act, s->cur->gc, temp, n, 0 );

      if((ytickb < 0.0 && yticke > 0.0) || ytickb == 0.0 || yticke == 0.0)
      {
        setColor(s, DS_CYAN);

        for(i=0;i<n;i++)
	{
          temp[i].y = (int)(MaxY-((MaxY-MinY)/(yticke-ytickb))*(0.0-ytickb));
	}

        XDrawLines(s->dpy, s->cur->act, s->cur->gc, temp, n, 0 );

        setColor(s, DS_GREEN);
      }
    }
  }

  noXTick = 0;
  noYTick = 0;
  noPlot  = 0;

  free((char *)temp);

  return(0);
}


int xgr_drawLineDirect( xmin, xmax, ymin, ymax, xdata, ydata, n, color)
float xmin, xmax, ymin, ymax;
float    *xdata;
float    *ydata;
int      n, color;
{
  struct XGR_HEAD *p;
  struct XGR_     *s;
  static XPoint *temp=NULL;
  int i;
  float MaxY, MinY, MaxX, MinX;

  if( (temp = (XPoint *)malloc((unsigned)(n*sizeof(XPoint))) ) == NULL) {
    fprintf(stderr,"%s: Error in xgr_drawLineDirect(malloc())\n", prgName);
    return(1);
  }

  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;

    if(!s->cur->invalid) 
      continue;
 
    setColor(s, color);
    MaxY = (float)s->cur->plotye;
    MinY = (float)s->cur->plotyb;
    MaxX = (float)s->cur->plotxe;
    MinX = (float)s->cur->plotxb;
    for(i=0;i<n;i++)
    {
      temp[i].x = (int)(MinX+((MaxX-MinX)/(xmax-xmin))*(xdata[i]-xmin));
      temp[i].y = (int)(MaxY-((MaxY-MinY)/(ymax-ymin))*(ydata[i]-ymin));
    }
    XDrawLines(s->dpy, s->cur->act, s->cur->gc, temp, n, 0 );
  }

  free((char *)temp);

  return(0);
}




int xgr_drawPoints(xmin, xmax, ymin, ymax, xdata, ydata, n, color)
float xmin, xmax, ymin, ymax;
float *xdata;
float *ydata;
int   n, color;
{
  struct XGR_HEAD *p;
  struct XGR_     *s;
  static XPoint *temp=NULL;
  int i;
  float MaxY, MinY, MaxX, MinX;
 
  if( (temp = (XPoint *)malloc((unsigned)(n*sizeof(XPoint))) ) == NULL) {
    fprintf(stderr,"%s: Error in xgr_drawPoint( malloc() )\n", prgName);
    return(1);
  }

  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;

    if(!s->cur->invalid) 
      continue;
 
    setColor(s, color);
    MaxY = (float)s->cur->plotye;
    MinY = (float)s->cur->plotyb;
    MaxX = (float)s->cur->plotxe;
    MinX = (float)s->cur->plotxb;

    for(i=0;i<n;i++) {
      temp[i].x = (int)(MinX+((MaxX-MinX)/(xmax-xmin))*(xdata[i]-xmin));
      temp[i].y = (int)(MaxY-((MaxY-MinY)/(ymax-ymin))*(ydata[i]-ymin));
    }
    XDrawPoints(s->dpy, s->cur->act, s->cur->gc, temp, n, 0 );
  }

  free((char *)temp);

  return(0);
}




static int drawXticks(s, n)
struct XGR_ *s;
int n;
{
  float f,f1, deltaF;
  char buf[256];
  int i, delta, sl, save_color;

  if(noXTick)
  {
    return(0);
  }

  xgr_setFont(s, font0);

  delta = (s->cur->plotxe-s->cur->plotxb)/(n);

  if(delta < 1)
  {
    delta = 1;
  }

  f      = globalStart;
  f1     = globalStop;
  deltaF = (f1 - f) / (float)n;

  for(i=s->cur->plotxb;i<=s->cur->plotxe;i+=delta)
  {
    if(doFocal)
      sprintf(buf,"%.2f",f);
    else
      sprintf(buf,"%.0f",f);

    sl = strlen(buf);

    XDrawString(s->dpy, s->cur->act, s->cur->gc, i-(s->fontW*sl)/2,s->cur->plotye+s->fontH,buf,sl);

    if(drawGrid)
    {
      save_color = currentColor;
      setColor(s, DS_GRAY32);
      XDrawLine(  s->dpy, s->cur->act, s->cur->gc, i, s->cur->plotyb, i, s->cur->plotye  );
      setColor(s, save_color);
    }
    else
    {
      XDrawLine(  s->dpy, s->cur->act, s->cur->gc, i, s->cur->plotyb,   i, s->cur->plotyb+5);
      XDrawLine(  s->dpy, s->cur->act, s->cur->gc, i, s->cur->plotye-5, i, s->cur->plotye  );
    }

    f += deltaF;
  }

  return(0);
}




static int drawYticks(s)
struct XGR_ *s;
{
  float f;
  char buf[256];
  int i, delta, sl, save_color;

  if(noYTick)
  {
    return(0);
  }

  delta = (s->cur->plotye-s->cur->plotyb)/(yntick+2);
  if(delta < 1)
  {
    delta = 1;
  }

  f = ytickb;

  xgr_setFont(s, font0);
  for(i=s->cur->plotye;i>=s->cur->plotyb;i-=delta)
  {
    if( yinterval < 0.01)
    {
      sprintf(buf,"%4.3f",f);
    }
    else
    if( yinterval < 0.1)
    {
      sprintf(buf,"%4.2f",f);
    }
    else
    if( yinterval < 1.0)
    {
      sprintf(buf,"%4.1f",f);
    }
    else
    if( yinterval > 50.0)
    {
      sprintf(buf,"%.0f",f);
    }
    else
    {
      sprintf(buf,"%4.1f",f);
    }

    sl = strlen(buf);

    XDrawString(s->dpy,s->cur->act,s->cur->gc,s->cur->plotxb-(sl*s->fontW)-2,i+s->fontH/2-2,buf,sl);

    if(drawGrid)
    {
      save_color = currentColor;
      setColor(s, DS_GRAY32);
      XDrawLine(s->dpy, s->cur->act, s->cur->gc, s->cur->plotxb,   i, s->cur->plotxe, i);
      setColor(s, save_color);
    }
    else
    {
      XDrawLine(s->dpy, s->cur->act, s->cur->gc, s->cur->plotxb,   i, s->cur->plotxb+5,i);
      XDrawLine(s->dpy, s->cur->act, s->cur->gc, s->cur->plotxe-5, i, s->cur->plotxe,  i);
    }

    f += yinterval;
  }

  return(0);
}



int xgr_drawBin( xdata, ydata, n, color)
float    *xdata;
float    *ydata;
int      n, color;
{
  int i,j,k,l=0, m;
  static float *x=NULL,*y=NULL;

  if(debug)
  {
    log_msg("xgr_drawBin()");
  }

  if(n>100)
    k=2;
  else
    k=10;

  m = n*k;
  globalScale = k;
  globalStart = xdata[0];
  globalStop  = xdata[n-1];

  if(local_malloc("xgr_drawBin", &x, &y, m))
    return(0);

  for(i=0;i<n;i++)
  {
    for(j=0;j<k;j++)
    {
      if(!(i != 0 && j == 0))
	l++;
      x[j+(i*k)] = (float)l;
      y[j+(i*k)] = ydata[i];
    }
  }

  xgr_drawLine(x, y, m, color);

  free((char *)x);
  free((char *)y);

  return(0);
}





int xgr_drawGrid(f1b, f1e, f2b, f2e, n1, n2, dx, dy, tick, label, clr1, clr2,format)
float f1b,f1e,f2b,f2e;
int n1, n2, dx, dy, tick, label, clr1, clr2, format;
{
  struct XGR_HEAD *p;
  struct XGR_     *s;
  float i_delta, j, deltaF,f;
  char buf[256];
  int n, i, k, sl, ddx, ddy;
  int segIndx;
  XSegment *segments = NULL;
 
  if(tick >=0)
  {
    if(n1>n2)
      n = n1 * 2 + 10;
    else
      n = n2 * 2 + 10;

    if((segments = (XSegment *)malloc(n * sizeof(XSegment))) == NULL){
      log_msg("xgr_drawGrid: Error in (malloc(%d))", n * sizeof(XSegment));
      return(1);
    }
  }

  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;

    if(!s->cur->invalid) 
      continue;
 
    xgr_setFont(s, font0);
							       /* draw x axis */
    i_delta = ((float)s->cur->plotxe - (float)s->cur->plotxb)/(float)n1;
    j = (float)s->cur->plotxb;
    i = (int)j;
    segIndx = 0;
    switch(tick)
    {
      case 0:
        for(k=0;k<=n1;k++)
	{
          segments[segIndx].x1   = (short)i;
          segments[segIndx].y1   = (short)s->cur->plotyb;
          segments[segIndx].x2   = (short)i;
          segments[segIndx++].y2 = (short)s->cur->plotye;
          j += i_delta;
          i = (int)j;
	}
        break;

      case 1:
        for(k=0;k<=n1;k++)
	{
          segments[segIndx].x1   = (short)i;
          segments[segIndx].y1   = (short)s->cur->plotyb;
          segments[segIndx].x2   = (short)i;
          segments[segIndx++].y2 = (short)s->cur->plotyb+5;
          segments[segIndx].x1   = (short)i;
          segments[segIndx].y1   = (short)s->cur->plotye-5;
          segments[segIndx].x2   = (short)i;
          segments[segIndx++].y2 = (short)s->cur->plotye;
          j += i_delta;
          i = (int)j;
	}
        break;
    }
    if(segIndx)
    {
      setColor(s, clr1);
      XDrawSegments(s->dpy, s->cur->act, s->cur->gc, segments, segIndx);
    }

    if(label&1)				  /* draw x axis labels if so desired */
    {
      f = f1b;
      deltaF = (f1e - f1b) / (float)n1;
      ddx = n1/dx;
      j = (float)s->cur->plotxb;
      i = (int)j;
      setColor(s, clr2);

      for(k=0;k<=dx;k++)
      {
        if(tick==1)
	{
          XDrawLine(s->dpy, s->cur->act, s->cur->gc, i, s->cur->plotyb,
						     i, s->cur->plotyb+10);
          XDrawLine(s->dpy, s->cur->act, s->cur->gc, i, s->cur->plotye-10,
						     i, s->cur->plotye);
	}
        else
        if(tick==0)
	{
          XDrawLine(s->dpy, s->cur->act, s->cur->gc, i, s->cur->plotyb,
						     i, s->cur->plotye);
	}

        if(format == XGR_PLOT_DEGREES)
        {
          if(fabs(deltaF) < 0.100)
            sprintf(buf+4," %.3f ",f);
          else
            sprintf(buf+4," %.1f ",f);
	}
        else
        if(format == XGR_PLOT_SEX)
          sexagesimal((double)f, buf+4, DD_MM_SS);
        else
          sprintf(buf+4,"  ");

        sl = strlen(buf+4);
        XDrawImageString(s->dpy, s->cur->act, s->cur->gc,
			i-(s->fontW*sl)/2, s->cur->plotye+s->fontH+1, buf+4,sl);

        f += deltaF*(float)ddx;
        j += i_delta*ddx;
        i = (int)j;
      }
    }
							       /* draw y axis */
    i_delta = ((float)s->cur->plotye - (float)s->cur->plotyb)/(float)n2;
    j = (float)s->cur->plotyb;
    i = (int)j;
    setColor(s, clr1);
    segIndx = 0;
    switch(tick)
    {
      case 0:
        for(k=0;k<=n2;k++)
        {
          segments[segIndx].x1   = (short)s->cur->plotxb;
          segments[segIndx].y1   = (short)i;
          segments[segIndx].x2   = (short)s->cur->plotxe;
          segments[segIndx++].y2 = (short)i;
          j += i_delta;
          i = (int)j;
        }
        break;

      case 1:
        for(k=0;k<=n2;k++)
        {
          segments[segIndx].x1   = (short)s->cur->plotxb;
          segments[segIndx].y1   = (short)i;
          segments[segIndx].x2   = (short)s->cur->plotxb+5;
          segments[segIndx++].y2 = (short)i;
          segments[segIndx].x1   = (short)s->cur->plotxe-5;
          segments[segIndx].y1   = (short)i;
          segments[segIndx].x2   = (short)s->cur->plotxe;
          segments[segIndx++].y2 = (short)i;
          j += i_delta;
          i = (int)j;
        }
        break;
    }
    if(segIndx)
      XDrawSegments(s->dpy, s->cur->act, s->cur->gc, segments, segIndx);

    if(label&2)
    {
      f = f2b;
      deltaF = (f2e - f2b) / (float)n2;
      ddy = n2/dy;
      j = (float)s->cur->plotyb;
      i = (int)j;
      setColor(s, clr2);

      for(k=0;k<=dy;k++)
      {
        if(tick==1)
        {
          XDrawLine(s->dpy, s->cur->act, s->cur->gc, s->cur->plotxb,   i,
                                                     s->cur->plotxb+10, i);
          XDrawLine(s->dpy, s->cur->act, s->cur->gc, s->cur->plotxe-10, i,
                                                     s->cur->plotxe,   i);
        }
        else
        if(tick==0)
        {
          XDrawLine(s->dpy, s->cur->act, s->cur->gc, s->cur->plotxb, i,
                                                     s->cur->plotxe, i);
        }

        if(format == XGR_PLOT_DEGREES)
        {
          if(fabs(deltaF) < 0.100)
            sprintf(buf+4," %.3f ",f);
          else
            sprintf(buf+4," %.1f ",f);
	}
        else
        if(format == XGR_PLOT_SEX)
          sexagesimal((double)f, buf+4, DD_MM_SS);
        else
          sprintf(buf+4,"  ");

        sl = strlen(buf+4);
        XDrawImageString(s->dpy, s->cur->act, s->cur->gc,
		s->cur->plotxb-s->fontW*sl,i+s->fontH/2, buf+4,sl);

        f += deltaF*(float)ddy;
        j += i_delta*ddy;
        i = (int)j;
      }
    }
  }

  if(segments)
    free(segments);

  return(0);
}



static int local_malloc(who,xdata,ydata,dl)
char *who;
float **xdata, **ydata;
int dl;
{
  if( (*xdata = (float *)malloc((unsigned)(dl*sizeof(float))) ) == NULL) {
    fprintf(stderr,"%s: Error in %s( malloc() )\n", prgName, who);
    return(1);
  }
  if( (*ydata = (float *)malloc((unsigned)(dl*sizeof(float))) ) == NULL) {
    fprintf(stderr,"%s: Error in %s( malloc() )\n", prgName, who);
    return(1);
  }

  return(0);
}



int xgr_drawBox(x, y, x2, y2, color)
float x, y, x2, y2;
int color;
{
  struct XGR_HEAD *p;
  struct XGR_     *s;
  int xb, xe, yb, ye;
  unsigned w, h;

//  log_msg("xgr_drawBox(): x = %f, y = %f, dx = %f, dy = %f", x, y, x2, y2);
 
  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;

    if(!s->cur->invalid) 
    {
      continue;
    }
 
    setColor(s, color);

    s->cur->plotxb = xb = (int)( (float)s->cur->canw * x );
    s->cur->plotxe = xe = (int)( (float)s->cur->canw * x2);

    /* Y is upside down; so, switch them here */
    s->cur->plotyb = yb = s->cur->canh - (int)( (float)s->cur->canh * y2);
    s->cur->plotye = ye = s->cur->canh - (int)( (float)s->cur->canh * y );

    w = (unsigned int)abs(xe-xb);
    h = (unsigned int)abs(ye-yb);

//    log_msg("xgr_drawBox(): canw = %d, canh = %d, x = %d, y = %d, w = %u, h = %u", s->cur->canw, s->cur->canh, xb, yb, w, h);

    XDrawRectangle(s->dpy, s->cur->act, s->cur->gc, xb, yb, w, h);
  }

  return(0);
}



int xgr_setBox(x,y,dx,dy)
float x,y,dx,dy;
{
  struct XGR_HEAD *p;
  struct XGR_     *s;
 
  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;
    s->cur->plotxb = (int)( (float)s->cur->canw * x);
    s->cur->plotxe = (int)( (float)s->cur->canw * dx);
    s->cur->plotyb =  s->cur->canh - (int)( (float)s->cur->canh * dy);
    s->cur->plotye =  s->cur->canh - (int)( (float)s->cur->canh * y);
  }

  return(0);
}



int xgr_drawFillBox( xmin, xmax, ymin, ymax, xl, yt, xr, yb, color)
float  xmin, xmax, ymin, ymax;
float xl, yt, xr, yb;
int color;
{
  struct XGR_HEAD *p;
  struct XGR_     *s;
  int x1, x2, yy1, y2;
  float MaxY, MinY, MaxX, MinX;
  unsigned int dx, dy;

  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;

    if(!s->cur->invalid || !s->cur->act) 
      continue;
 
    setColor(s, color);
    XSetFillStyle(s->dpy, s->cur->gc, FillSolid);
    MaxY = (float)s->cur->plotye;
    MinY = (float)s->cur->plotyb;
    MaxX = (float)s->cur->plotxe;
    MinX = (float)s->cur->plotxb;
    x1   = (int)(MinX+((MaxX-MinX)/(xmax-xmin))*(xl-xmin));
    x2   = (int)(MinX+((MaxX-MinX)/(xmax-xmin))*(xr-xmin));
    dx   = (unsigned int)(x2 - x1);
    yy1  = (int)(MaxY-((MaxY-MinY)/(ymax-ymin))*(yb-ymin));
    y2   = (int)(MaxY-((MaxY-MinY)/(ymax-ymin))*(yt-ymin));
    dy   = (unsigned int)(y2 - yy1);

//    log_msg("xgr_drawFillBox(): Drawing on Window: %p", s->cur->act);

    XFillRectangle(s->dpy, s->cur->act, s->cur->gc, x1, yy1, (unsigned int)dx, (unsigned int)dy);
  }

  return(0);
}


int xgr_drawRectangle( xmin, xmax, ymin, ymax, xl, yt, xr, yb, color, bold)
float  xmin, xmax, ymin, ymax;
float xl, yt, xr, yb;
int color, bold;
{
  struct XGR_HEAD *p;
  struct XGR_     *s;
  int x1, x2, yy1, y2;
  float MaxY, MinY, MaxX, MinX;
  unsigned int dx, dy;

  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;

    if(!s->cur->invalid || !s->cur->act)
    {
      continue;
    }

    setColor(s, color);

    MaxY = (float)s->cur->plotye;
    MinY = (float)s->cur->plotyb;
    MaxX = (float)s->cur->plotxe;
    MinX = (float)s->cur->plotxb;

    x1   = (int)(MinX+((MaxX-MinX)/(xmax-xmin))*(xl-xmin));
    x2   = (int)(MinX+((MaxX-MinX)/(xmax-xmin))*(xr-xmin));
    dx   = (unsigned int)(x2 - x1);
    yy1  = (int)(MaxY-((MaxY-MinY)/(ymax-ymin))*(yb-ymin));
    y2   = (int)(MaxY-((MaxY-MinY)/(ymax-ymin))*(yt-ymin));
    dy   = (unsigned int)(y2 - yy1);

    if(debug)
    {
      log_msg("xgr_drawRectangle(): %d %d %d %d", x1, yy1, dx, dy);
    }

    XDrawRectangle(s->dpy, s->cur->act, s->cur->gc, x1, yy1, dx, dy);

    if(bold)
    {
      XDrawRectangle(s->dpy, s->cur->act, s->cur->gc, x1+1, yy1+1, dx, dy);
    }
  }

  return(0);
}


int xgr_drawTextInGrid( xmin, xmax, ymin, ymax, x, y, msg, color, f)
float  xmin, xmax, ymin, ymax;
float x, y;
char *msg;
int color, f;
{
  struct XGR_HEAD *p;
  struct XGR_     *s;
  int x1, yy1;
  float MaxY, MinY, MaxX, MinX;

  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;

    if(!s->cur->invalid || !s->cur->act)
    {
      continue;
    }

    setColor(s, color);
    xgr_setFont(s, f);


    MaxY = (float)s->cur->plotye;
    MinY = (float)s->cur->plotyb;
    MaxX = (float)s->cur->plotxe;
    MinX = (float)s->cur->plotxb;

    x1   = (int)(MinX+((MaxX-MinX)/(xmax-xmin))*(x-xmin));
    yy1  = (int)(MaxY-((MaxY-MinY)/(ymax-ymin))*(y-ymin));

    if(debug)
    {
      log_msg("xgr_drawTextInGrid(): %d %d %s", x1, yy1, msg);
    }

    XDrawString(s->dpy, s->cur->act, s->cur->gc, x1, yy1, msg, strlen(msg));
  }

  return(0);
}



int xgr_drawCircle( xmin, xmax, ymin, ymax, x, y, w, h, color, size)
float  xmin, xmax, ymin, ymax;
float x, y, w, h;
int color, size;
{
  struct XGR_HEAD *p;
  struct XGR_     *s;
  int x1, x2, y1, y2;
  float MaxY, MinY, MaxX, MinX;
  unsigned int dx, dy;

  if(debug)
  {
    log_msg("xgr_drawCircle(%.4f, %.4f, %.4f, %.4f, %.4f, %.4f, %.4f, %.4f)", xmin, xmax, ymin, ymax, x, y, w, h);
  }

  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;

    if(!s->cur->invalid || !s->cur->act)
    {
      continue;
    }

    setColor(s, color);

    MaxY = (float)s->cur->plotye;
    MinY = (float)s->cur->plotyb;
    MaxX = (float)s->cur->plotxe;
    MinX = (float)s->cur->plotxb;

    x1   = (int)(MinX+((MaxX-MinX)/(xmax-xmin))*(x-xmin));
    x2   = (int)(MinX+((MaxX-MinX)/(xmax-xmin))*(w-xmin));
    dx   = (unsigned int)abs(x2 - x1);

    y1   = (int)(MaxY-((MaxY-MinY)/(ymax-ymin))*(y-ymin));
    y2   = (int)(MaxY-((MaxY-MinY)/(ymax-ymin))*(h-ymin));
    dy   = (unsigned int)abs(y2 - y1);

    if(debug)
    {
      log_msg("xgr_drawCircle(): x1 = %d, x2 = %d, y1 =  %d, y2 =  %d, dx = %d, dy = %d", x1, x2, y1, y2, dx, dy);
    }

    XSetLineAttributes(s->dpy, s->cur->gc, size, LineSolid, CapRound, JoinRound);
    
    XDrawArc(s->dpy, s->cur->act, s->cur->gc, x1, y1, dx, dy, 0*64, 360*64);

    XSetLineAttributes(s->dpy, s->cur->gc, 1, LineSolid, CapRound, JoinRound);
  }

  return(0);
}



int xgr_drawPosText(x,y,msg, clr, f)
float x,y;
char *msg;
int  clr, f;
{
  struct XGR_HEAD *p;
  struct XGR_     *s;
  int x1, yy1, canw, canh;
 
  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;

    if(!s->cur->invalid) 
      continue;
 
    setColor(s, clr);
    xgr_setFont(s, f);

    canw = s->cur->plotxe - s->cur->plotxb;
    canh = s->cur->plotye - s->cur->plotyb;
    x1 = (int)(x * (float)canw)+s->cur->plotxb; 
    yy1 = (int)(y * (float)canh)+s->cur->plotyb; 

    XDrawString(s->dpy, s->cur->act, s->cur->gc, x1, yy1, msg, strlen(msg));
  }

  return(0);
}




int xgr_drawText(x,y,msg, clr, f)
float x,y;
char *msg;
int  clr, f;
{
  struct XGR_HEAD *p;
  struct XGR_     *s;
 
  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;

    if(!s->cur->invalid) 
      continue;
 
    setColor(s, clr);
    xgr_setFont(s, f);
    XDrawString(s->dpy, s->cur->act, s->cur->gc, 
		(int)(x * (float)s->cur->canw), 
		s->cur->canh - (int)(y * (float)s->cur->canh), 
		msg, strlen(msg));
  }

  return(0);
}


int xgr_drawTextClear(x,y,msg, clr, f)
float x,y;
char *msg;
int  clr, f;
{
  struct XGR_HEAD *p;
  struct XGR_     *s;

  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;

    if(!s->cur->invalid) 
      continue;
 
    setColor(s, clr);
    xgr_setFont(s, f);
    XDrawImageString(s->dpy, s->cur->act, s->cur->gc, 
		(int)(x * (float)s->cur->canw), 
		s->cur->canh - (int)(y * (float)s->cur->canh), 
		msg, strlen(msg));
  }

  return(0);
}



int xgr_drawPoint(xdata, ydata, n, xmin, xmax, msg, color, f)
float *xdata;
float *ydata;
int   n, color;
float xmin, xmax;
char  *msg;
int f;
{
  struct XGR_HEAD *p;
  struct XGR_     *s;
  XPoint  temp;
  int i;
 
  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;
    if(!s->cur->invalid) 
      continue;
 
    setColor(s, color);
    xgr_setFont(s, f);
   
    tickGet(xdata,n, &xtickb, &xticke, &xntick, &xinterval, &xmin, &xmax);
    for(i=0;i<n;i++)
    {
      temp.x = (int)((float)s->cur->plotxb + (((float)s->cur->plotxe 
		- (float)s->cur->plotxb) 
		/ (xmax - xmin)) 
		* (xdata[i] - xmin));
      temp.y = (int)((float)s->cur->plotye - (((float)s->cur->plotye 
		- (float)s->cur->plotyb) 
		/ (yticke - ytickb)) 
		* (ydata[i] - ytickb));
      temp.x -= (s->fontW / 2);
      temp.y += (s->fontH / 2) -2;
      XDrawString(s->dpy, s->cur->act, s->cur->gc, temp.x, temp.y, msg, strlen(msg));
    }
  }

  return(0);
}


int xgr_drawOn(x, y, n, xmin, xmax, msg, color, f)
float x,y;
int n, color;
float xmin, xmax;
char *msg;
int f;
{
  struct XGR_HEAD *p;
  struct XGR_     *s;
  XPoint  temp;
  float MaxY, MinY, MaxX, MinX;
  int cw;
 
  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;
    if(!s->cur->invalid) 
      continue;
 
    setColor(s, color);
    xgr_setFont(s, f);
    MaxY = (float)s->cur->plotye;
    MinY = (float)s->cur->plotyb;
    cw = s->cur->canw / n;
    MaxX = (float)s->cur->plotxe-cw/2;
    MinX = (float)s->cur->plotxb+cw/2;
    temp.x = (int)(MinX+((MaxX-MinX)/(xmax-xmin))*(x-xmin));
    temp.y = (int)(MaxY-((MaxY-MinY)/(yticke-ytickb))*(y-ytickb));
    temp.x -= (s->fontW / 2);
    temp.y += (s->fontH / 2) -2;
    XDrawString(s->dpy, s->cur->act, s->cur->gc, temp.x, temp.y, msg, strlen(msg));
  }

  return(0);
}



int xgr_drawLabel(b1, b2, b3, color)
char *b1,*b2,*b3;
int color;
{
  struct XGR_HEAD *p;
  struct XGR_     *s;
  int l;
  float f, ff;
  int x1, yy1;

  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;
    if(!s->cur->invalid) 
      continue;
 
    setColor(s, color);
    xgr_setFont(s, font2);
    l   = strlen(b1);						       /* top */
    ff  = (float)s->fontW / (float)s->cur->canw;
    f   = 0.5 - ((float)l * ff) / 2.0;
    x1  = (int)(f * (float)s->cur->canw);
    yy1 = s->cur->canh - (int)(0.95 * (float)s->cur->canh);
    XDrawString(s->dpy, s->cur->act, s->cur->gc, x1, yy1, b1, strlen(b1));

    xgr_setFont(s, font1);					    /* bottom */
    l   = strlen(b2);
    ff  = (float)s->fontW / (float)s->cur->canw;
    f   = 0.5 - ((float)l * ff) / 2.0;
    x1  = (int)(f * (float)s->cur->canw);
    yy1 = s->cur->canh - (int)(0.025 * (float)s->cur->canh);
    XDrawString(s->dpy, s->cur->act, s->cur->gc, x1, yy1, b2, strlen(b2));
  }

  return(0);
}



static int tickGet(x,n, start, end, tks, interval, min, max)
float *x;
int n, *tks;
float *start, *end, *interval, *min, *max;
{
  *tks = find_ticks(x, n, start, end, interval ,min, max);

  return(0);
}

static int find_ticks(f, n, pstart, pend, ptickint, mn, mx)
float *f;
int n;
float *pstart, *pend, *ptickint, *mn, *mx;
{
  double min, max, tickint, diff;
  int i, ticks;
  int order;

  max = -1.0e127; /* there are probably standard macros, but this works */
  min = 9.99e127;

  for( i=0; i< n; i++ ) {			       /* compute min and max */
    if( f[i] > max )
      max = f[i];
    if( f[i] < min )
      min = f[i];
  }
  *mn = min;
  *mx = max;

  diff = max-min;
  if(diff > 0.0)
    order = (int)rint(log10(diff))-1;
  else
  {
    *pend = max;
    *pstart =  min;
    *ptickint = diff;
    return(1);
  }

  tickint = pow(10, (double)order);
  ticks = (int)rint(diff/tickint);

  if( ticks >= 10 )
  {
    if( ticks >= 20 ) 
    {
      ticks /= 5;
      tickint *= 5.0;
    }
    else
    {
      ticks /= 2;
      tickint *= 2.0;
    }
  }
  else
  if( ticks < 3 )
  {
    ticks *= 5;
    tickint /= 5.0;
  }

  if(fixedZero)
  {
    float maxval;

    if(fabs(min) > fabs(max))
      maxval = fabs(min);
    else
      maxval = fabs(max);

    *pend = maxval;
    *pstart = -maxval;
    *ptickint = maxval*2 / 6.0;
    return(7);
  }
  else
  {
    *pend = ((int)floor((max + tickint) / tickint)) * tickint;

    *pstart = *pend - (ticks +2) * tickint;
    *ptickint = tickint;
    return(ticks);
  }

}


int xgr_setFont(s, f)
struct XGR_ *s;
int f;
{
  int fx;
  float d;

  if(xgr_hardFont == -1)
  {
    fx = f;
  }
  else
  if(xgr_hardFont == 1)
  {
    fx = f;
  }
  else
  {
    d=(float)s->cur->canw/(float)WidthOfScreen(XDefaultScreenOfDisplay(s->dpy));
    fx = f + (int)rint( (float)s->numFonts * d) - 1;
    if(fx >= s->numFonts)
      fx = s->numFonts-1;
    if(fx < 0)
      fx = 0;
  }
  if( s->fontInfo[fx] == NULL)
    if(!loadFont(s, fx))
      return(1);

  s->font_info = s->fontInfo[fx];

  XSetFont(s->dpy, s->cur->gc, s->font_info->fid);
  s->fontW = s->font_info->max_bounds.width;
  s->fontH = s->font_info->max_bounds.ascent+s->font_info->max_bounds.descent;

  return(0);
}


int setInValid(w)
int w;
{
  struct XGR_HEAD *p;
  struct XGR_     *s;
 
  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;
    s->cur->invalid = w;
    s->cur->foreground = -999;
  }

  return(0);
}



char **findFixedFonts(dpy, num)
Display *dpy;
int *num;
{
  int i=0, j=0, k=0, n=0, nn=0, found=0;
  static char **fontNames;
  static char **fN;
  static char tempNames[40][40];
  int charcompare();
 
  for(i=0;i<40;i++)
    tempNames[i][0] = '\0';

  if(*num==0) 
  {
    log_msg("findFixedFonts(): Num of Fixed Fonts == Zero");

    return((char **)0);
  }

  n = 0;
  fontNames = XListFonts(dpy, "?x?bold", 100, &n);
  if(n)
  {
    nn = n;
    for(i=0,k=0;i<nn;i++)
    {
      found = 0;
      for(j=0;j<k;j++)
      {
        if(!strncmp(tempNames[j], fontNames[i], 2))
          found = 1;
      }
      if(!found && strlen(fontNames[i]) < 16)
        strcpy(tempNames[k++], fontNames[i]);
    }
    nn = k;
  }

  n = 0;
  fontNames = XListFonts(dpy, "?x??bold", 100, &n);
  if(n)
  {
    nn += n;
    for(i=0;i<n;i++)
    {
      found = 0;
      for(j=0;j<k;j++)
      {
        if(!strncmp(tempNames[j], fontNames[i], 2))
          found = 1;
      }
      if(!found && strlen(fontNames[i]) < 16)
        strcpy(tempNames[k++], fontNames[i]);
    }
    nn = k;
  }
 
  n = 0;
  fontNames = XListFonts(dpy, "??x??bold", 100, &n);
  if(n)
  {
    nn += n;
    for(i=0;i<n;i++)
    {
      found = 0;
      for(j=0;j<k;j++)
      {
        if(!strncmp(tempNames[j], fontNames[i], 2))
          found = 1;
      }
      if(!strstr(fontNames[i], "fixed") && !found && strlen(fontNames[i]) < 16)
        strcpy(tempNames[k++], fontNames[i]);

    } 
    nn = k;
  }
  if(nn==0) 
  {
    log_msg("findFixedFonts(): No Fixed Fonts Found");
    *num=0;

    return((char **)0);
  }

  if((fN = (char **)malloc((unsigned)(nn*sizeof(char *)))) == NULL)
  {
    log_msg("findFixedFonts(): Error in malloc() of Fixed Fonts");
    *num=0;

    return((char **)0);
  }

  for(i=0;i<nn;i++)
    fN[i] = (char *)tempNames[i];

#ifndef __DARWIN__
  qsort((char *)&fN[0], nn, sizeof(char *), (__compar_fn_t)charcompare);
#else
  qsort((char *)&fN[0], nn, sizeof(char *), charcompare);
#endif
  if(nn != *num)
    *num = nn;

  return(fN);
}



int strcmp();

int charcompare(s1,s2)
char **s1, **s2;
{
  int i,j, si, sj;
  char *cp1, *cp2;
  char t1[40], t2[40];

  i = atoi(*s1);
  j = atoi(*s2);
  if(i<j)
    return(-1);
  else
  if(i>j)
    return(1);
  else
  {
    strcpy(t1, *s1);
    strcpy(t2, *s2);

    cp1 = strtok(t1, "xX");
    if(cp1)
    {
      cp1 = strtok((char *)NULL, "xX");
      if(cp1)
        si = atoi(cp1);
      else
        return(strcmp(*s1,*s2));
    }
    else
      return(strcmp(*s1,*s2));

    cp2 = strtok(t2, "xX");
    if(cp2)
    {
      cp2 = strtok((char *)NULL, "xX");
      if(cp2)
        sj = atoi(cp2);
      else
        return(strcmp(*s1,*s2));
    }
    else
      return(strcmp(*s1,*s2));
    
  }
  if(si<sj)
    return(-1);
  else
  if(si>sj)
    return(1);
  else

  return(strcmp(*s1,*s2));
}



int setFrameFooter(msg, lr)
char *msg;
int lr;
{
  struct XGR_HEAD *p;
  struct XGR_     *s;
  int l;
 
  return(0);
 
  if(msg)
    l = strlen(msg);
  else
    return(0);
 
  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;
    if(lr == LEFT)
    {
      XDrawString(s->dpy, s->cur->act, s->cur->gc, 5, s->cur->canh, msg, l);
    }
    else
    {
      XDrawString(s->dpy, s->cur->act, s->cur->gc, s->cur->canw / 2 +5, s->cur->canh, msg, l);
    }
  }

  return(0);
}



int xgr_checkForEvents()
{
  struct XGR_HEAD *p;
  struct XGR_ *s;

  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;
    if(XPending(s->dpy))
    {
      processEvent(s);
    }
  }

  return(0);
}
