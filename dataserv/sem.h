#ifndef SEM_H
#define SEM_H

struct DATASERV_SEMS {
        int   sem;
        int   indx;
        int   sem_id;
        char *name;
};

extern struct DATASERV_SEMS dsems[];

#define RECOMPUTE_BASELINES 0x00
#define BASELINES_RET       0x01
#define COMPUTE_INTEN       0x02
#define INTEN_RET           0x03
#define DISPLAY_SPECTRA     0x04
#define SPECTRA_RET         0x05

#define MAX_DSEMS 16

#endif
