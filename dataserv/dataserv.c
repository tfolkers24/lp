#include "ds_extern.h"
#include "ds_proto.h"

#ifndef __DARWIN__
#include <sys/sysinfo.h>
#endif

int pthread_setname_np(pthread_t thread, const char *name);

float globalStop, globalStart;

#ifdef GALACTIC
#undef GALACTIC
#endif

#ifdef AZEL
#undef AZEL
#endif

#ifdef USERDEF
#undef USERDEF
#endif

#include "dataserv.bmp"
//#include "datagrid.bmp"

#define  TRUE   1
#define  FALSE  0
#define DATASPREAD      0.000001

int (*sock_callback)() = NULL;
char inMsg[16384];

struct SOCK *master, *helper, *xgraphic, *specplot, *specdata;

int   datalen		= 0,
      manualInitials    = 0,
      manualVersion     = 0,
      thisIsaRefresh	= 0,
      sockmode		= 0,
      initialsChanged   = 0,
      printDataLines    = 0,
      useFixData	= 0,
      version           = 1,
      numOfCPUs         = 0;

float datamax, 
      datamin, 
      scanNum		= -1.0;


char  filename[256];
char  gfilename[256];

char  initials[256], source[SRCLEN+1], oldBuf[16384], tempBuf[16384],
      prgName[80], errBuf[512], manInitials[80], localHost[80],
      deadServer[80], saveBuf[16384], manVersion[80], mainFrameTitle[80], 
      tailFileName[80], datadir[128], fixeddir[128], _3ddir[128];


int   found_scans[MAX_BACKENDS*MAX_PARTS];


int scansDone[100];
int scansDoneInx = 0;
int scanDone     = 0;

static int block_for_event();

struct HEADER  onhead01;
/*
, onhead02, onhead03,
               onhead04, onhead05, onhead06,
               onhead07, onhead08, onhead09,
               onhead10, onhead11, onhead12,
               onhead13, onhead14, onhead15,
               onhead16;
 */

float  *ondata01, *ondata02, *ondata03,
       *ondata04, *ondata05, *ondata06,
       *ondata07, *ondata08, *ondata09,
       *ondata10, *ondata11, *ondata12,
       *ondata13, *ondata14, *ondata15,
       *ondata16;

struct HEADER  offhead01;

float  *offdata01, *offdata02, *offdata03,
       *offdata04, *offdata05, *offdata06,
       *offdata07, *offdata08, *offdata09,
       *offhead10, *offdata11, *offdata12,
       *offdata13, *offdata14, *offdata15,
       *offdata16;

struct HEADER  gainshead01;

float  *gainsdata01, *gainsdata02, *gainsdata03,
       *gainsdata04, *gainsdata05, *gainsdata06,
       *gainsdata07, *gainsdata08, *gainsdata09,
       *gainsdata10, *gainsdata11, *gainsdata12,
       *gainsdata13, *gainsdata14, *gainsdata15,
       *gainsdata16;

extern int _Xdebug;

int fitFail = 0;
int doNotSaveConfig = 0;

extern int lockScale, wasOn;
extern int max_gray_scale, newMaxGrayScale, doColor, startNewResultFile;
extern float offScanNum;

extern struct XGR_HEAD xgr_head;
struct XGR_ *server;

int PID;

  /* DO NOT FORGET TO ADD A HANDLER FOR ANY EXTRA MASKS ADDED, processEvent() */
long frame_mask = KeyPressMask | ExposureMask | StructureNotifyMask;

int istty;
int globalUsPixmap = 1;

extern int insque(), remque();


int to_helper(buf)
char *buf;
{
  if(buf && sockmode)
  {
    log_msg("To HELPER: '%s'", buf);

    sock_send(helper, buf);
  }

  return(0);
}


int to_xgraphic(buf)
char *buf;
{
  if(buf && sockmode)
  {
    log_msg("To XGRAPHIC: %s", buf);

    sock_send(xgraphic, buf);
  }

  return(0);
}


int getNProcs()
{
  int n, na;

#ifndef __DARWIN__
  n  = get_nprocs_conf();
  na = get_nprocs();
#else
  n  = 4;
  na = 4;
#endif

  numOfCPUs = na;
  log_msg("%d processors configured, %d processors available.", n, na);

  return(0);
}


int main(argc,argv)
int argc;
char *argv[];
{
  char *cp, tfile[256];
  int cw, ch, bg;
  int xpos, ypos;
  char title[80];

  setCactusEnvironment();

  otfInit();

  do_etimeInit();

  sockmode       = 0;
  manualInitials = 1;
  strcpy(manInitials, "ssk");

  if(argc >= 2)
  {
    if(!strncmp(argv[1], "-sock", 5))
    {
      sockmode = 1;
    }
  }

  istty = isatty(0);

  atexit((void *)exitProc);

#ifndef __DARWIN__
  signal(SIGINT,  (__sighandler_t)signalHandler);
  signal(SIGTERM, (__sighandler_t)signalHandler);
#else
  signal(SIGINT,  signalHandler);
  signal(SIGTERM, signalHandler);
#endif

  strcpy(prgName, "DATASERV");

  if(sockmode)
  {
    cp = getenv("LPSESSION");

    if(cp)
    {
      sprintf(tfile, "LOGDIR=/tmp/%s", cp);

      putenv(tfile);
    }
    else
    {
      strcpy(tailFileName, "databrowse");
    }

    log_open("dataserv", 4);

    log_eventdEnable(LOG_TO_LOGGER);
  }
  else
  {
    smart_log_open("dataserv", 3);
  }

  setpgid( (pid_t)0, (pid_t)0);        /* divorce myself from parents */

  PID = getpid();

  cp = getenv("TOPDIR");
  if(cp)
    sprintf(datadir,"%s",cp);
  else
    sprintf(datadir,"/home/data");

  cp = getenv("FIXDAT");
  if(cp)
    sprintf(fixeddir,"%s",cp);
  else
    sprintf(fixeddir,"/home/data4/fixed");

  cp = getenv("_3DDIR");
  if(cp)
    sprintf(_3ddir,"%s",cp);
  else
    sprintf(_3ddir,"/home/data1/3d");

  cp = getenv("DISPLAY");
  if(cp)
    sprintf(localHost,"%s",cp);

  xgr_head.prev = xgr_head.next = &xgr_head;

  if((server=(struct XGR_ *)calloc(1,sizeof(struct XGR_)))==NULL)
  {
    log_msg("error in calloc for main server");
    return(1);
  }

  insque(server, xgr_head.prev);

  cw = 650;
  ch = 790;
  xpos = 10;
  ypos = 10;

  strcpy(server->serverName, localHost);

  if((server->dpy = XOpenDisplay(localHost)) == (Display *)NULL)
  {
    log_msg("Unable to open display %s", localHost);
    return(0);
  }

  if(!sockmode)
  {
    sock_bind("ANONYMOUS");
  }
  else
  {
    sock_bind("LINUXPOPS_DATASERV");
//    log_eventdEnable(1);                 /* enable loging to the event daemon */
  }

  master   = sock_connect("LINUXPOPS_MASTER");
  helper   = sock_connect("LINUXPOPS_HELPER");
  xgraphic = sock_connect("LINUXPOPS_XGRAPHIC");
  specplot = sock_connect("LINUXPOPS_SPECPLOT");
  specdata = sock_connect("LINUXPOPS_SPECDATA");

  set_sock_callback( (int (*)(void))readMail );

  sock_bufct(sizeof(struct DS_IMAGE_INFO)+256);

  strcpy(title,"Dataserv");
  strcpy(mainFrameTitle,"Dataserv");

  bg = DS_BLACK;

  createWindows(server, title, cw, ch, xpos, ypos, bg, globalUsPixmap);

  XSetErrorHandler(myErrorHandler);
  XSetIOErrorHandler((XIOErrorHandler)myIOErrorHandler);

  xgr_reSizeProc(server->cur->win, server->cur->canw, server->cur->canh);

  main_loop();

  return(0);
}


int myIOErrorHandler(display, myerr)
Display *display;
XErrorEvent *myerr;
{
  if(display)
  {
    log_msg("Server %s caused IO Trap", DisplayString(display));
    strcpy(deadServer, DisplayString(display));
  }
  else
  {
    log_msg("Server: Unknown Server caused IO Trap");
    strcpy(deadServer, "Unknown Server");
  }
  exit(2);
}



int myErrorHandler(display, myerr)
Display *display;
XErrorEvent *myerr;
{
  if(display)
  {
    log_msg("Server %s caused Xlib Error", DisplayString(display));
    strcpy(deadServer, DisplayString(display));
  }
  else
  {
    log_msg("Server: Unknown Server caused Xlib Error");
    strcpy(deadServer, "Unknown Server");
  }
  return(0);
}


void signalHandler()
{
  struct XGR_HEAD *p;
  struct XGR_     *s;

  exit(1);

  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;
    if(s->dpy)
    {
      log_msg("Signal: Shuting Down Server %s", DisplayString(s->dpy));
      XFlush(s->dpy);
      XCloseDisplay(s->dpy);
    }
  }
}


int exitProc(status)
int status;
{
  log_msg("Exiting with status %d", status);

  /* kill the spec viewer */

  sockWriteNoBitch = 1;
  sock_send(specplot, "quit");
  sock_send(specdata, "quit");
  sockWriteNoBitch = 0;

  return(status);
}



int dummieEventProc(event)
XEvent *event;
{
  KeySym   keysym;
  XComposeStatus compose;
  char     buffer[1];
  int      bufsize=1;

  if(event->type == KeyPress)
  {
    XLookupString((XKeyEvent *)event, buffer, bufsize, &keysym, &compose);
    if(keysym == XK_p && event->xkey.state == ControlMask)
    {
      log_msg("Stand-by, Making Copy of Dataserv Window.");
      do_diagHarCopyServ();
      log_msg("Copy of Dataserv Window sent to Printer.");
    }
  }
  return(0);
}


#define WINDOW_PLOT 1
#define WINDOW_OTF  2

/* Create individual window */

struct WINDOW *createWindow(s, title, cw, ch, xpos, ypos, bg, usPixmap, which)
struct XGR_ *s;
char *title;
int cw, ch, xpos, ypos, bg, usPixmap, which;
{
  unsigned long value_mask;
  XSetWindowAttributes attr;
  XSizeHints size_hints;
  struct WINDOW *w;
  XWMHints wm_hints;
  XTextProperty *wi, *ic;

  if((w = (struct WINDOW *)calloc(1, sizeof(struct WINDOW ))) == NULL)
  {
    log_msg("error in calloc for main Plot window");
    exit(1);
  }

  insque(w, s->windows.prev);

  strcpy(w->frameTitle, title);
  w->canw       = cw;
  w->canh       = ch;
  w->background = bg;
  w->first      = 1;
  w->win        = XCreateSimpleWindow(s->dpy, RootWindow(s->dpy,s->scr_num),
                                      xpos, ypos, (unsigned int)w->canw, 
					    (unsigned int)w->canh, 
					1,  s->colors[DS_WHITE],
					    s->colors[DS_BLACK]);
  w->usPixmap   = usPixmap;

  w->gc   = XCreateGC(s->dpy, RootWindow(s->dpy, s->scr_num), (unsigned long)0, (XGCValues *)NULL);

  if(w->usPixmap)
  {
    if(!(w->pixmap = XCreatePixmap(s->dpy, w->win, (unsigned int)w->canw, (unsigned int)w->canh, s->depth)))
    {
      log_msg("%s: Error in create of off screen pixmap\n", prgName);
      exit(1);
    }

    XSetForeground(s->dpy, w->gc, s->colors[DS_BLACK]);
    XFillRectangle(s->dpy, w->pixmap, w->gc, 0, 0, (unsigned int)cw, (unsigned int)ch);
  }

  XSelectInput(s->dpy, w->win, frame_mask | ButtonPressMask | Button1MotionMask);

  size_hints.flags        = PPosition | PSize | PMinSize/* | PAspect*/;
  size_hints.min_width    = cw;
  size_hints.min_height   = ch;
  size_hints.x            = xpos;
  size_hints.y            = ypos;

  w->invalid               = 1;

  w->icon = XCreateBitmapFromData(s->dpy, w->win, dataserv_bmp_bits, dataserv_bmp_width, dataserv_bmp_height); 
 
  XSetStandardProperties(s->dpy, w->win, w->frameTitle, (char *)NULL, w->icon, (char **)NULL, 0, &size_hints);

  wi = NULL;
  ic = NULL;

  wm_hints.initial_state = NormalState;
  wm_hints.input         = True;
  wm_hints.icon_pixmap   = 0;
  wm_hints.flags         = StateHint | InputHint | WindowGroupHint;
  wm_hints.window_group  = w->win;

  XSetWMProperties(s->dpy, w->win, wi, ic, NULL, 0, &size_hints, &wm_hints, NULL);


  if(DoesBackingStore(XDefaultScreenOfDisplay(s->dpy)))
  {
    if(debug)
    {
      log_msg("createWindow(): Setting Backing Store for Plot Window");
    }

    value_mask = CWBackingStore;
    attr.backing_store = WhenMapped;
    XChangeWindowAttributes(s->dpy, w->win, value_mask, &attr);
  }

  XMapWindow(s->dpy, w->win);
  w->mapped       = 1;

  XSetWindowBackground(s->dpy, w->win, s->colors[w->background]);
  XSetBackground(s->dpy, w->gc, s->colors[w->background]);
  XClearWindow(s->dpy, w->win);

  s->cur = w;

  return(w);
}


int createWindows(s, title, cw, ch, xpos, ypos, bg, usPixmap)
struct XGR_ *s;
char *title;
int cw, ch, xpos, ypos, bg, usPixmap;
{
  int x, y, w, h;
  struct WINDOW *win;

  log_msg("DisplayInfo for %s - %s major %d minor %d release %d", 
           DisplayString(s->dpy), ServerVendor(s->dpy), ProtocolVersion(s->dpy),
	   ProtocolRevision(s->dpy), VendorRelease(s->dpy));

  s->xfd          = XConnectionNumber(s->dpy);
  s->scr_num      = DefaultScreen(s->dpy);
  s->numFonts     = XGR_MAX_FONTS;
  s->fontNames    = findFixedFonts(s->dpy, &s->numFonts);
  s->windows.prev = s->windows.next = &s->windows;
  s->depth        = XDefaultDepthOfScreen(XDefaultScreenOfDisplay(s->dpy));
  s->userXimage   = NULL;

  x = 670;
  y = 28;
  w = 860;
  h = 780;
						/* create OTF gridder window */
  win = createWindow(s, "OTF Gridder", w, h, x, y, bg, usPixmap, XGR_OTF_WIN);

  log_msg("OTF Window = %p", win->win);
  log_msg("OTF Pixmap = %p", win->pixmap);

  win->repaint_proc = doOtfGridRePaint;
  win->resize_proc  = doOtfGridReSize;
  win->event_proc   = otfGridDisplaySpec;

  s->otfwin = win;
  s->cur    = s->otfwin;

  xgr_graphInit(s);

  xgr_mapUnmapWindow("OTF Gridder", 1);

  return(0);
}




int argmatch(s1, s2, atleast)
char *s1;
char *s2;
int atleast;
{
  int l1 = strlen(s1);
  int l2 = strlen(s2);

  if(l1 < atleast)
    return 0;

  if(l1 > l2)
    return 0;

  return(strncmp(s1, s2, l1) == 0);
}


int usage(progname)
char *progname;
{
  fprintf(stderr, "\nUsage of '%s' command line arguments: \n", progname);
  fprintf(stderr, "-ws  x y                   Set Window size to x y\n");
  fprintf(stderr, "-anon                      Set dataserv in command-line mode\n");
  fprintf(stderr, "-nopixmap                  Set dataserv in non-pixmap mapping mode\n");
  fprintf(stderr, "-obs initials              Set initials to display;  defaults to current observer\n");
  fprintf(stderr, "-ver n dir                 Set datafile ver to n, located on disk dir;\n");
  fprintf(stderr, "                           defaults to current observers version\n");
  fprintf(stderr, "-maxcolors [16|32|64|128]  Set max colors of image display;  default 64\n");
  fprintf(stderr, "-color [GRAY|PSEUDO|FLAME] Set color mode of image display;  default PSEUDO\n");
  fprintf(stderr, "-usage                     This message\n");
  exit(0);
}


int mainFrameRepaint()
{
  thisIsaRefresh = 1;

  strcpy(tempBuf,oldBuf);

  readMail(tempBuf, strlen(tempBuf)); 

  return(0);
}


int verifyScanDone(scn)
int scn;
{
  int i;

  for(i=0;i<100;i++)
  {
    if(scn == scansDone[i])
    {
      scanDone = 1;
      return(0);
    }
  }
  insertScan(scn);
  scanDone = 0;

  return(1);
}



int insertScan(scn)
int scn;
{
  scansDoneInx++;
  if(scansDoneInx > 99)
    scansDoneInx = 0;

  scansDone[scansDoneInx] = scn;

  return(0);
}



int to_monitor(buf)
char *buf;
{
  log_msg("To Monitor: %s", buf);

  return(0);
}


int to_rambo(buf)
char *buf;
{
  log_msg("To Rambo: %s", buf);

  return(0);
}

int to_exec(buf)
char *buf;
{
  log_msg("To Exec: %s", buf);

  return(0);
}

int do_toggleFixData(buf)
char *buf;
{
  if(buf)
    useFixData = atoi(buf);
  else
    useFixData = 0;

  log_msg("Setting use of fixed data to %d", useFixData);

  return(0);
}


int show_display()
{
  showDisplay();
  freeDataPointers();

  return(0);
}



int showDisplay()
{
  int i, modeid, be, foundOne=0;
  extern float ifNumber;
  char *cp;
  extern float ifnums[];

  bzero((char*)&found_scans, sizeof(found_scans));

  cp = filename;
  for(i=0;i<4;i++)
  {
    ifNumber = ifnums[i];
    if(!peekScan(scanNum + ifNumber, cp, &be, &modeid, &onhead01))
    {
      found_scans[DBEA+i] = 1;
      foundOne++;
    }
  }

  cp = filename;
  for(i=0;i<8;i++)
  {
    ifNumber = ifnums[i];
    if(!peekScan(scanNum + ifNumber, cp, &be, &modeid, &onhead01))
    {
      found_scans[FFBA+i] = 1;
      foundOne++;
    }
  }

  cp = filename;
  for(i=0;i<4;i++)
  {
    ifNumber = ifnums[i];
    if(!peekScan(scanNum + ifNumber, cp, &be, &modeid, &onhead01))
    {
      found_scans[AOSA+i] = 1;
      foundOne++;
    }
  }

  for(i=4;i<5;i++)
  {
    ifNumber = ifnums[i];
    if(!peekScan(scanNum + ifNumber, cp, &be, &modeid, &onhead01))
    {
      found_scans[AOSC] = 1;
      foundOne++;
    }
  }

  cp = filename;
  for(i=0;i<4;i++)
  {
    ifNumber = ifnums[i];
    if(!peekScan(scanNum + ifNumber, cp, &be, &modeid, &onhead01))
    {
      found_scans[CTSA+i] = 1;
      foundOne++;
    }
  }

  if(!foundOne)
  {
    log_msg("No scan found for scan %.0f", scanNum);

    return(0);
  }

  for(i=0;i<20;i++)               /* set the ifNumber to the first scan found */
  {
    if(found_scans[i])
    {
      ifNumber = ifnums[i];
      break;
    }
  }

//  log_msg("Found %d scan(s), ifnum %.2f, BE = %d, modeid = %d", 
//                                       foundOne, ifNumber, be, modeid);

  wasOn = 1;
  switch(be)
  {
    case SPECTRAL_BE:
      switch(modeid) 
      {
	case TP_OFF:
	case TPM_OFF:
//	  do_linetotalmapoff();
	  wasOn = 0;
	  break;
	case OTF:
	  do_lineotf();
	  break;
	default:
//	  do_linedefault();
	  break;
      }
      break;

    default:
      return(0);
      break;
  }

//  xgr_pixmapToWindow("OTF Gridder");
  xgr_mapUnmapWindow("OTF Gridder", 1);

//  xgr_pixmapToWindow(mainFrameTitle);
//  xgr_mapUnmapWindow(mainFrameTitle, 1);

  return(0);
}



#ifdef TOMMIE
int matherr(x)
register struct exception *x;
{
  log_msg("Math Exception");
  switch(x->type)
  {
    case DOMAIN:		/* change sqrt to return sqrt(-arg1), not NaN */
        if(!strcmp(x->name, "sqrt"))
        {
          log_msg("Domain exception in %s", x->name);
          log_msg("Filename scanNum %d", (int)scanNum);
          log_msg("Command %s", oldBuf);
          x->retval = sqrt(-x->arg1);
          return (0);			       /* print message and set errno */
        }						      /* fall through */
    case SING:/* all other domain or sing exceptions, print message and abort */
       	log_msg("domain exception in %s", x->name);
        log_msg("Filename scanNum %d", (int)scanNum);
        log_msg("Command %s", oldBuf);
        abort( );
        break;
    case OVERFLOW:
        log_msg("Overflow exception in %s(%f)", x->name, x->arg1);
        log_msg("Filename scanNum %d", (int)scanNum);
        log_msg("Command %s", oldBuf);
        break;
    case UNDERFLOW:
        log_msg("Underflow exception in %s(%f)", x->name, x->arg1);
        log_msg("Filename scanNum %d", (int)scanNum);
        log_msg("Command %s", oldBuf);
        break;
  }
  return(0);
}
#endif



int main_loop()
{
  struct XGR_HEAD *p, *p2;
  struct XGR_ *s, *f;
  int count, n, xfd;
  int xfds[256];

  while (1)
  {
    count=0;

    for(p=xgr_head.next;p!=&xgr_head;p=p->next)
    {
      s = (struct XGR_ *)p;

      if(XPending(s->dpy))
      {
	processEvent(s);
	count++;
      }
    }

    if(!count) 
    {
      n=0;

      for(p2=xgr_head.next;p2!=&xgr_head;p2=p2->next)
      {
        f = (struct XGR_ *)p2;
        xfds[n] = f->xfd;
	n++;
      }

      xfd = block_for_event(xfds, n);

      for(p2=xgr_head.next;p2!=&xgr_head;p2=p2->next)
      {
        f = (struct XGR_ *)p2;

	if(xfd == f->xfd) 
	{
	  if(XPending(f->dpy) == 0)
	  {
	    if(f->dpy == server->dpy)	/* is it the main one */
	      exit(0);

            log_msg("Removeing %s in destoryFunc", DisplayString(f->dpy));
	    remque(f);
	    xgr_freeWindows(f);

	    break;
	  }
	}
      }
    }
  }

  return(0);
}
 

int processEvent(s)
struct XGR_ *s;
{
  XEvent event;
/*
  struct WINDOW *xgr_getWin();
 */

  while(XPending(s->dpy))
  {
    XNextEvent(s->dpy, &event);

    switch( event.type )
    {
      case Expose:
		/* Filter down to the last Expose event */
        while (XCheckTypedWindowEvent(s->dpy, event.xany.window, Expose, &event))
        {
	  ;
        }
 
        xgr_rePaintProc(event.xexpose.window);
 
        break;

      case ConfigureNotify:

		/* Filter down to the last ConfigureNotify event */
        while (XCheckTypedWindowEvent(s->dpy, event.xany.window, ConfigureNotify, &event))
        {
	  ;
        }

        if(event.xconfigure.window && !event.xconfigure.send_event)
	{
          xgr_reSizeProc(event.xconfigure.window, event.xconfigure.width, event.xconfigure.height);
	}

        break;

      case ButtonPress:
          xgr_eventProc(&event, event.xbutton.window, ButtonPress);
        break;

      case MotionNotify:
          xgr_eventProc(&event, event.xbutton.window, MotionNotify);
        break;

      case KeyPress:
          xgr_eventProc(&event, event.xbutton.window, KeyPress);
        break;

      case UnmapNotify:
        break;

      case DestroyNotify:
        log_msg("Destroy");

        exit((int)3);

        break;

      default:
        break;
    }
  }

  return(0);
} 




static int block_for_event(xfd, n)
int *xfd;
int n;
{
  int len, i;
  static int sel = 1;

  if(istty && !sockmode && sel == 0)
  {
    fprintf(stderr, "dataserv> ");
    fflush(stderr);
  }

  if(sockmode == 0)
  {
    sel = sock_sel(inMsg, &len, xfd, n, 0, 1);
  }
  else
  if(sockmode == 1)
  {
    sel = sock_sel(inMsg, &len, xfd, n, 10, 0);
  }

  if( sel == -1 )
  {                                                /* timeout */
    return(sel);
  } 

  for(i=0;i<n;i++)
  {
    if(sel == xfd[i])
    {
      return(sel);
    }
  }
 
  if(sock_callback)
  {
    (*sock_callback)(inMsg, len);
  }

  return(sel);
}


int set_sock_callback( call )
int (*call)();
{
  sock_callback = call;

  return(0);
}



int synchronize(v)
int v;
{
  struct XGR_HEAD *p;
  struct XGR_     *s;

  for(p=xgr_head.next;p!=&xgr_head;p=p->next)
  {
    s = (struct XGR_ *)p;

    XSynchronize(s->dpy, v);
  }

  return(0);
}


int do_printData(buf)
char *buf;
{
  if(buf)
    printDataLines = atoi(buf);

  if(printDataLines > 100)
    printDataLines = 0;

  return(0);
}

int determine_min_max(ydata, dl, scan)
float *ydata;
int dl;
float scan;
{
  int i;

  if(ydata == NULL)
     return(1);
                                                   /* redetermine min and max */
  datamax = -9999999999.9;
  datamin = 9999999999.0;

  for(i=0;i<dl;i++)
  {
    if(ydata[i] > datamax)
      datamax = ydata[i];
    if(ydata[i] < datamin)
      datamin = ydata[i];
  }

  if(fabs(datamax - datamin) < DATASPREAD)
  {
    log_msg("determine_min_max(): Scan #%7.2f is all %.4f", scan, datamin);

    datamin -= 1.0;
    datamax += 1.0;

    return(1);
  }
  return(0);
}



int do_filename(buf)
char *buf;
{
  int n, v;

  if(buf)
  {
    strcmprs(buf);

    strcpy(filename, buf);

    log_msg("do_filename() filename set to %s", filename);
  }
  else
  {
    log_msg("do_filename(): Missing arg for filename");

    return(1);
  }

  n = strlen(filename);

  v = atoi(filename+(n-3));

  if(v > 0 && v < 999)
  {
    log_msg("Setting internal version to: %d", v);
    version = v;
  }
  else
  {
    log_msg("Problem parseing version number from %s", filename);
  }

  return(0);
}


int do_gfilename(buf)
char *buf;
{
  if(buf)
  {
    strcmprs(buf);

    strcpy(gfilename, buf);

    log_msg("do_gfilename() gfilename set to %s", gfilename);
  }
  else
  {
    log_msg("do_gfilename(): Missing arg for gfilename");

    return(1);
  }

  return(0);
}


/* ds_DELAY() -- Custom delay routine that uses select to time out. delays in
 *              units of 22 - 44 usec can be had. returns 0 for a good delay.
 *              any key board hit will stop delay and return true.
 *
 *		Checks stdin if we are using interactive mode;
 *		Checks sockets if in sockmode.
 */
int ds_delay(dc)
double dc;
{
  int i, t, sel, len;
  char message[4096];
  struct timeval del;
  fd_set a;

  if(!sockmode)
  {
    del.tv_sec  = (int)dc;
    del.tv_usec = (dc - (int)dc) * 1e6;

    FD_ZERO(&a);
    FD_SET(0, &a);

    if(select(1, &a, NULL, NULL, &del))
    {
    /* ioctl(0,TCFLSH,0);                        clear out keyborad buffer */
      return(1);
    }

    return(0);
  }
  else /* Can only sleep in whole units of seconds */
  {
    t = (int)(dc * 10.0); /* convert to units of 1/10th sec */

    if(t) /* Actual delay */
    {
      for(i=0;i<t;i++)
      {
        if((sel = sock_sel(message, &len, NULL, 0, -1, 1)) > 0) /* Poll for a message */
        { 
          sel = sock_sel(message, &len, NULL, 0, 1, 1); /* Poll for a message */

	  strcmprs(message);

          if(!strncmp(message, "done", 4))
          {
	    return(1);
          }
        }

        usleep(100000);
      }

      return(0);
    }
    else /* t == 0.0, so just poll */
    {
      if((sel = sock_sel(message, &len, NULL, 0, -1, 0)) > 0) /* Poll for a message */
      {
        sel = sock_sel(message, &len, NULL, 0, 1, 1); /* Poll for a message */

	strcmprs(message);

        if(!strncmp(message, "done", 4))
        {
	  return(1);
        }
      }
      else
      {
	return(0);
      }
    }
  }

  return(0);
}
