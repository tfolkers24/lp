#include "ds_extern.h"
#include "ds_proto.h"

#define  TRUE   1
#define  FALSE  0

#define  N_INTEG_CHANS 10

#define ROW0            0.96
#define ROW1            0.92
#define ROW2            0.88
#define ROW3            0.84

static int font0 = FONT_SIZE_SMALL;
//static int font1 = FONT_SIZE_NORMAL;
//static int font2 = FONT_SIZE_BIG;

float startRes;
extern float scanNum, datamax, datamin, globalStart, globalStop,
	     gainsScanNum, *offdata01, *gainsdata01;
extern int datalen, scan, parallel, lockScale,
	   fbScanDone, aosScanDone, thisIsaRefresh;

extern char filenames[16][256], *filename_ctssource[];

extern struct SPECTRA_DISPLAY_PARAMS sdp[];

char *months[] = { "Jan","Feb","Mar","Apr","May","Jun",
                   "Jul","Aug","Sep","Oct","Nov","Dec" };


int doTPprocess    = 0;
int doFsProcess    = 0;
int ignorePos      = 0;
int wasOn          = 0;
int spcIF          = 0;
int domarker       = 0;
int fitChan[2]     = { 1, 2 };

double workingTsys = 0.0;
float workingX[32768];
float workingY[32768];

struct DS_IMAGE_INFO sc_image, *last_image=NULL;

#ifdef TOMMIE
int doingSpecFive = 0;
int auto_locate     = 1;
int off_center_line = 0;

float sfxb[] = { 0.20, 0.20, 0.20, 0.05, 0.35 };
float sfxe[] = { 0.30, 0.30, 0.30, 0.15, 0.45 };
float sfyb[] = { 0.60, 0.13, 0.35, 0.35, 0.35 };
float sfye[] = { 0.85, 0.35, 0.60, 0.60, 0.60 };

struct _SPEC_FIVE {
	int set;
	float a, b, rms;
	double itg_value;
	struct HEADER head;
	float data[MAX_DATA_CHANNELS];
};
struct _SPEC_FIVE specfives[5][20];                 /* 5 scans for 9 backends */

int specFiveChannel = 0;


int docross        = 0;
int dofft          = 0;
/*
rfftw_plan fftplan = NULL;
 */

struct SPEC_FIVE_PARAMS {
	int active;
	int cc;
	int b1;
	int b2;
	int b3;
	int b4;
};

struct SPEC_FIVE_PARAMS sfp;
#endif

int zerodata       = 0;

struct SPEC_SCANS {
        float scan;
        struct SPEC_SCANS *next;
};
struct SPEC_SCANS *spec_scans_head = 0;


int display_spectra(xb, xe, yb, ye, data, head, procSpec)
float xb, xe, yb, ye;
float *data;
struct HEADER *head;
int procSpec;
{
  float *xdata, freqres;
  int i, noint;
  float scale;
  char temp[80];

  if(debug)
  {
    log_msg("display_spectra();");
  }

  noint = (int)head->noint;
  datalen = (int)head->datalen/sizeof(float);

  zerodata = 0;
  if(!lockScale)
  {
    if( determine_min_max(data, noint, head->scan))
    {
      zerodata = 1;
    }
  }

  scale = ye -yb; 
  sprintf(temp, " %7.2f   %8.8s", head->scan, head->backend);
  xgr_drawText(xb, 0.905*scale + yb, temp, DS_RED, font0);
  xgr_drawBox(xb, 0.05*scale + yb, xe,  0.90*scale + yb, DS_GRAY50);

					 /* Initialize the x-axis data points */
  if( (xdata = (float *)malloc((unsigned)(noint*sizeof(float)))) == NULL)
  {
    log_perror("malloc(xdata)");
    return(-1);
  }
						/* Set the x-axis data points */
  freqres = head->freqres;

  startRes = (float)head->refpt*freqres;

  if( head->sideband == 1.0) /* LSB */
  {
    for(i=0;i<noint;i++)
    {
      xdata[i] =  startRes - ((float)i*freqres);
    }
  }
  else
  {
    for(i=0;i<noint;i++) /* USB */
    {
      xdata[i] = -startRes + ((float)i*freqres);
    }
  }

  globalStart = xdata[0];
  globalStop  = xdata[noint-1];
  xgr_drawBin(xdata, data, noint, DS_GREEN);

  if(zerodata)
  {
    xgr_drawText(xb, 0.8*scale + yb, "Data is Zero", DS_RED, font0);
  }

  free((char *)xdata);

  return(1);
}



int do_spectraCombine(buf)
char *buf;
{
  int i, b, e;
  char *cp, tb[80];
  char tmp[256];;

  strcpy(tmp, buf);

  cp = strtok(tmp,"\n\t ");
  if(cp)
  {
    b = atoi(cp);
  }
  else
  {
    log_msg("do_spectraCombine(), no begin scan specified");
    return(1);
  }

  cp = strtok((char *)NULL,"\n\t ");
  if(cp)
  {
    e = atoi(cp);
  }
  else
  {
    log_msg("do_spectraCombine(), no begin scan specified");
    return(1);
  }

  cp = strtok((char *)NULL,"\n\t ");
  if(cp)
  {
    spcIF = atoi(cp);
  }
  else
  {
    log_msg("do_spectraCombine(), no IF specified");
    return(1);
  }

  strcpy(sc_image.magic, "DS_IMAGE_INFO");

  for(i=b;i<=e;i++)
  {
    sprintf(tb,"%d",i);
    fbScanDone = 1;
    aosScanDone = 3;
    do_scanDone(tb);

    if(ds_delay(0.0))
    {
      break;
    }
  }

  do_sendSpecComb();

  spcIF = 0;

  return(0);
}


int do_sendSpecComb()
{
  int i, j, k, n, indx, fac;
  double accumX, accumY;

  if(sc_image.scanct == 0)
  {
    return(1);
  }

  sc_image.doBaseline = getBaselines(sc_image.baselines);
  computeBaseLine(0, workingY, &sc_image.a, &sc_image.b, &sc_image.rms, sc_image.scan_noint, 1);

  if(sc_image.scan_noint > 1024)
  {
    n = sc_image.scan_noint;
    fac = n / 1024;
    j = 0;
    indx = 0;
    
    for(i=0;i<1024;i++)
    {
      accumX = 0.0;
      accumY = 0.0;
      for(k=0;k<fac;k++)
      {
        accumX += workingX[indx];
        accumY += workingY[indx++];
      }

      if(sc_image.doBaseline)
        sc_image.ydata[j++] = accumY / fac -((float)i *sc_image.a +sc_image.b);
      else
        sc_image.ydata[j++] = accumY / fac;

    }
    sc_image.noint = 1024;
  }
  else
  {
    if(sc_image.doBaseline)
    {
      for(i=0;i<sc_image.noint;i++)
        sc_image.ydata[i] = workingY[i] - ((float)i * sc_image.a + sc_image.b);
    }
    else
    {
      for(i=0;i<sc_image.noint;i++)
        sc_image.ydata[i] = workingY[i];
    }

    /*
   bcopy((char *)&workingX,(char *)sc_image.xdata,sc_image.noint*sizeof(float));
    */
  }
  sc_image.tsys = (float)(workingTsys / (double)sc_image.scanct);
  write(1, &sc_image, sizeof(struct DS_IMAGE_INFO));
  return(0);
}



int do_specCombClear()
{
  bzero((char *)&sc_image, sizeof(struct DS_IMAGE_INFO));
  bzero((char *)&workingX, sizeof(workingX));
  bzero((char *)&workingY, sizeof(workingY));
  remSpecScans();
  workingTsys = 0.0;

  return(0);
}


int combineData(xdata, yd, head)
float *xdata, *yd;
struct HEADER *head;
{
  int k, chan;
  float *i, iwt, *o, owt; 

  chan = (int)((head->scan - (int)head->scan) * 100.0 + 0.005) - 1;

  log_msg("CHAN = %d", chan);

/* Be advised that spcIF is one-based IF# */
  if(spcIF && sdp[spcIF-1].head == head)
  {
    if(checkSpecScans(head->scan))
    {
      log_msg("Scan %.2f already in Spectral Accum", head->scan);
      return(1);
    }

    if(sc_image.freqres == 0.0)
      sc_image.freqres = (float)head->freqres;

    if( fabs(sc_image.freqres - (float)head->freqres) > 0.029 )
    {
      log_msg("specCombineData(), Freq Res %.3f, scan #%.2f not the same",
					 head->freqres, head->scan); 
      return(1);
    }
    if(sc_image.ra == 0.0)
      sc_image.ra = (float)head->epocra / 15.0;

    if( fabs(sc_image.ra - (float)head->epocra / 15.0 ) > 0.001  && !ignorePos)
    {
      log_msg("specCombineData(), Ra %.3f, scan #%.2f not the same", head->epocra, head->scan); 
      return(1);
    }
    if(sc_image.dec == 0.0)
      sc_image.dec = (float)head->epocdec;

    if( fabs(sc_image.dec - (float)head->epocdec) > 0.001 && !ignorePos)
    {
      log_msg("specCombineData(), Dec %.3f, scan #%.2f not the same", head->epocdec, head->scan); 
      return(1);
    }

    if(sc_image.noint == 0)
      sc_image.noint = sc_image.scan_noint = (int)head->noint;

    if( sc_image.scan_noint != (int)head->noint )
    {
      log_msg("specCombineData(), Noint %3d, scan #%.2f not the same as accumalator %3d", (int)head->noint, head->scan, sc_image.scan_noint); 
      return(1);
    }
    if(sc_image.sb == 0)
      sc_image.sb = (int)head->sideband;

    if( sc_image.sb != (int)head->sideband )
    {
      log_msg("specCombineData(), Sb %d, scan #%.2f not the same", head->sideband, head->scan); 
      return(1);
    }
    if(sc_image.freq == 0.0)
      sc_image.freq = head->restfreq;

    if( fabs(sc_image.freq - head->restfreq) > 0.0001 )
    {
      log_msg("specCombineData(), Freq %.5f, scan #%.2f not the same", 
				head->restfreq/1000.0, head->scan); 
      return(1);
    }
    if(sc_image.vel == 0.0)
      sc_image.vel = (float)head->refpt_vel;

    if( fabs(sc_image.vel - (float)head->refpt_vel) > 0.0001  && !ignorePos)
    {
      log_msg("specCombineData(), Vel %.3f, scan #%.2f not the same", head->refpt_vel, head->scan); 
      return(1);
    }
    if(sc_image.cal == 0.0)
      sc_image.cal = (float)head->tcal;

    if( fabs(sc_image.cal - (float)head->tcal) > 100.0 )
    {
      log_msg("specCombineData(), Tcal %.1f, scan #%.2f not the same", head->tcal, head->scan); 
      return(1);
    }

    if(sc_image.bscan == 0.0)
      sc_image.bscan = (float)head->scan;

    sc_image.synfreq = head->synfreq;
    sc_image.escan   = (float)head->scan;
    sc_image.date    = (float)head->utdate;
    sc_image.dv      = (float)head->deltax;

    log_msg("specCombineData(), adding scan %.2f", head->scan);
    sc_image.scanct++;
    addSpecScans(head->scan);

    strncpy(sc_image.source, head->object, 16);
    sc_image.source[15] = '\0';
    strncpy(sc_image.coordcd, head->coordcd, 8);
    sc_image.coordcd[7] = '\0';

    sc_image.row   = -1;
    sc_image.col   = -1;
    sc_image.time  += (float)head->inttime;
    workingTsys    += head->stsys;

    iwt            = head->inttime / ( head->stsys * head->stsys );
    owt            = sc_image.wt;

    i = yd;
    o = workingY;

    for(k=0;k<sc_image.scan_noint;k++, i++, o++)
      *o = (*o * owt + *i * iwt) / (owt + iwt);

    sc_image.wt += iwt;

    bcopy((char *)xdata, (char *)&workingX, sc_image.scan_noint*sizeof(float));
  }

  return(0);
}




int remSpecScans()
{
  struct SPEC_SCANS *p;
  struct SPEC_SCANS *q;

  for(p=spec_scans_head; p; p=q)
  {
    q = p->next;
    if(p)
      free((char *)p);
  }
  spec_scans_head = NULL;

  return(0);
}



int addSpecScans(s)
float s;
{
  struct SPEC_SCANS *m;

  if(spec_scans_head == NULL)
  {
    if((spec_scans_head=(struct SPEC_SCANS *)malloc((unsigned)(sizeof(struct SPEC_SCANS))))==NULL)
    {
      log_msg("Error of addSpecScans(malloc(%d))", sizeof(struct SPEC_SCANS));
      fprintf(stderr,"Error of addSpecScans(malloc(%ld))\n", sizeof(struct SPEC_SCANS));
      return(1);
    }
    spec_scans_head->scan = s;
    spec_scans_head->next = NULL;
    return(0);
  }

  for(m=spec_scans_head;m->next; m=m->next)
  {
    ;
  }

  if((m->next=(struct SPEC_SCANS *)malloc((unsigned)(sizeof(struct SPEC_SCANS))))==NULL)
  {
    log_msg("Error of addSpecScans(malloc(%d))", sizeof(struct SPEC_SCANS));
    fprintf(stderr,"Error of addSpecScans(malloc(%ld))\n", sizeof(struct SPEC_SCANS));
    return(1);
  }

  m->next->scan = s;
  m->next->next = NULL;
  return(0);
}



int checkSpecScans(s)
float s;
{
  struct SPEC_SCANS *m;

  if(spec_scans_head == NULL)
    return(0);

  for(m=spec_scans_head;m->next; m=m->next)
    if(m)
      if(m->scan == s)
        return(1);

  if(m)
    if(m->scan == s)
      return(1);

  return(0);
}


int do_ignorePos(buf)
char *buf;
{
  if(buf)
    ignorePos = atoi(buf);

  return(0);
}


int putHeader(image)
struct DS_IMAGE_INFO *image;
{
  int mon, day, yr;
  char raStr[20], decStr[20], timeStr[20], mapStr[60], buf[256];
  extern struct XGR_ *server;

  xgr_setFont(server, font0);

  sexagesimal(image->time / 3600, timeStr, DDD_MM_SS_S100);
  strcmprs(timeStr);

  sexagesimal(image->ra, raStr, DD_MM_SS);
  sexagesimal(image->dec, decStr, DD_MM_SS);

  yr = (int)image->date;
  mon = (int)((image->date - (float)yr) * 100.0);
  day = (int)((image->date - (float)yr) * 10000.0 +0.5)-mon*100.0;

  sprintf(buf, "%16.16s %3d SCANS: %7.2f-%7.2f INT=%s DATE: %2d %s %2.2d",
                        image->source,
                        image->scanct,
                        image->bscan,
                        image->escan,
                        timeStr, day, months[mon-1], yr-2000);

  xgr_drawText(0.05, ROW0, buf, DS_MAGENTA, font0);

  if(image->col >=0)
  {
    sprintf(mapStr,"  Col %3d  Row %3d   ", image->col,image->row);
  }
  else
  {
    sprintf(mapStr, "( %s %s )", raStr, decStr);
  }

  sprintf(buf, "%s=%s %s %s CAL= %5.1f TSYS= %5.0f",
                        image->coordcd,
                        raStr,
                        decStr,
                        mapStr,
                        image->cal,
                        image->tsys);

  xgr_drawText(0.05, ROW1, buf, DS_MAGENTA, font0);

  sprintf(buf, "FREQ= %.2f SYN= %10.8f VEL= %7.1f DV= %6.2f FR= %4.0f SB= %d",
                        image->freq,
                        image->synfreq / 1000.0,
                        image->vel,
                        image->dv,
                        image->freqres * 1000.0,
                        image->sb );
  xgr_drawText(0.05, ROW2, buf, DS_MAGENTA, font0);

  sprintf(buf, "Rms %8.3fK",image->rms);
  xgr_drawText(0.80, ROW3, buf, DS_MAGENTA, font0);

  return(0);
}



int display_image(xb, xe, yb, ye, im)
float xb, xe, yb, ye;
struct DS_IMAGE_INFO *im;
{
  float *xdata;
  int i, noint;
  float scale;
//  char temp[80];
  float *data;

  if(debug)
  {
    log_msg("display_image();");
  }

  data = &im->ydata[0];

  noint = (int)im->noint;

  zerodata = 0;
  if(!lockScale)
  {
    if( determine_min_max(data, noint, im->bscan))
    {
      zerodata = 1;
    }
  }

  scale = ye -yb; 
//  sprintf(temp, " %7.2f   %8.8s", im->scan, im->backend);
//  xgr_drawText(xb, 0.905*scale + yb, temp, DS_RED, font0);
  xgr_drawBox(xb, 0.05*scale + yb, xe,  0.90*scale + yb, DS_GRAY50);

					 /* Initialize the x-axis data points */
  if( (xdata = (float *)malloc((unsigned)(noint*sizeof(float)))) == NULL)
  {
    log_perror("malloc(xdata)");
    return(-1);
  }

#ifdef TOMMIE
						/* Set the x-axis data points */
  startRes = (float)im->refpt*im->freqres;

  if( im->sb == 2.0) /* LSB */
  {
    for(i=0;i<noint;i++)
    {
      xdata[i] = startRes + ((float)i*im->freqres);
    }
  }
  else
  {
    for(i=0;i<noint;i++) /* USB */
    {
      xdata[i] = -startRes - ((float)i*im->freqres);
    }
  }
#else
    for(i=0;i<noint;i++)
    {
      xdata[i] = (float)i;
    }
#endif

  globalStart = xdata[0];
  globalStop  = xdata[noint-1];

  xgr_drawBin(xdata, data, noint, DS_GREEN);

  if(zerodata)
  {
    xgr_drawText(xb, 0.8*scale + yb, "Data is Zero", DS_RED, font0);
  }

  free((char *)xdata);

  return(1);
}


int doSpecPlot(im)
struct DS_IMAGE_INFO *im;
{
  log_msg("doSpecPlot(): depreceiated");

  return(0);
}


int doSpecWinRepaint()
{
  if(debug)
  {
    log_msg("doSpecWinRepaint()");
  }

  if(last_image)
  {
    doSpecPlot(last_image);
  }

  return(0);
}


int doSpecWinReSize()
{
  if(debug)
  {
    log_msg("doSpecWinReSize()");
  }

  return(0);
}


int putChanTemp(c, t)
int c;
float t;
{
  log_msg("putChanTemp(): depreceiated");

  return(0);
}


int doSpecPlotWinEvent(event, mask)
XEvent *event;
unsigned int mask;
{
  int chan;
  struct WINDOW *w;
  float x,y, fchan;
  struct DS_IMAGE_INFO *im = last_image;

  if(debug)
  {
    log_msg("doSpecPlotWinEvent()");
  }

  if(last_image == NULL)
  {
    log_msg("No Spectral Plot");
    return(1);
  }

  switch(event->type)
  {
    case MotionNotify:
    case ButtonPressMask:
      break;

    case KeyPress:
      checkKeyPress(event);
      return(1);
      break;

    default:
      return(1);
      break;
  }

  if( xgr_findWindow(event->xbutton.display, event->xbutton.window, &w))
  {
    log_msg("xgr_findWindow() returned Non Zero");

    return(1);
  }

  x   = ((float)w->plotxe - (float)w->plotxb);
  x   = ((float)event->xbutton.x - (float)w->plotxb) / x - 0.5;

  y   = ((float)w->plotye - (float)w->plotyb);
  y   = ((float)w->plotye - (float)event->xbutton.y) / y - 0.5;

  fchan = (float)im->noint;

  fchan = x * fchan + (fchan / 2.0);

  chan = floor(fchan);

  if(chan < 0)
  {
    chan = 0;
  }
  else
  if(chan > im->noint-1)
  {
    chan = im->noint-1;
  }

  if(debug)
  {
    log_msg("X = %f, Y = %f, Chan = %.2f, %d = %f", x, y, fchan, chan, im->ydata[chan]);
  }

  putChanTemp(chan, im->ydata[chan]);

  return(0);
}
