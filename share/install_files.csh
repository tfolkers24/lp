#!/bin/csh -f
#
if ( "$1" == "" || "$2" == "" ) then
  echo "Usage: $0 host files_to_copy"
  exit
endif
#
set host="$1"
set files="$2 $3 $4 $5 $6 $7 $8 $9 $10 $11 $12 $13 $14 $15 $16 $17 $18 $19 $20"

#echo "host = $host"
#echo "files = $files"
#
ssh root@"$host" "mkdir -p /home/analysis/linuxpops/share /home/analysis/linuxpops64/share"
scp $files root@"$host":/tmp
#
ssh root@"$host" "cd /tmp; install -m 644 "$files" /home/analysis/linuxpops64/share"
ssh root@"$host" "cd /tmp; install -m 644 "$files" /home/analysis/linuxpops/share"

