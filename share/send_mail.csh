#!/bin/csh -f
#
if ( "$1" == "" ) then
  echo "$0 request_file"
  exit
endif
#
cat $1 | mail -s  JOB -c tfolkers@email.arizona.edu horizons@ssd.jpl.nasa.gov
