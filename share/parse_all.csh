#!/bin/csh
#
foreach f ( Mercury Venus Mars Jupiter Saturn Uranus Neptune )
  echo "Processing Planet: $f"
  set inname=MAJOR-BODY-$f.eml
  set outname=$f-Ephem.tab
  echo "Processing $inname"
  echo "cat header.txt $inname >! $outname"
  cat header.txt $inname >! $outname
  dos2unix $outname
end
