#!/bin/csh
#
if ( "$1" == "" ) then
  echo "Usage: $0 horizons file"
  exit
endif
#
awk '{print $1,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14}' $1
