/*
 *  Cactus file @(#)dumpscans.c	1.4
 *        Date 09/27/06
 *
 */
#ifndef lint

static char SccsID[] = "@(#)dumpscans.c	1.4\t09/27/06";

#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <malloc.h>
#include <math.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>

#include "header.h"
#include "sdd.h"
#include "sex.h"

#define MAX_RECORD 0x3fffff

int swapThisSdd = 0;

/*
struct BOOTSTRAP  boot;
struct DIRECTORY  dir;
 */
struct HEADER     head;
float             *data = NULL;
int               bytperrec;

typedef enum {
  OTHER_SCAN=0,
  OFF_SCAN,
  ON_SCAN
} scanType;


struct SCANS {
  int      indx;
  int      entry;
  long     start_rec;
  scanType scantype;
  double   scan;
  double   off;
  char     scanmode[10];
};

int sanity(int fd, struct BOOTSTRAP *, struct DIRECTORY *);

struct SCANS *scanArray=NULL;
int   numOfScans=0, bad_scans=0;

char dataFile[256], source[80], scantype[80];
int first=1, last;
float scan=0.0, firstscan;

static int pdopen();
int findSize(), findScans(), read_scan();


char *header = "Scan#      R.A.     Dec.     Mode       Source      Az      El    Off#   Gains#\n\n";


int argmatch(s1, s2, atleast)
char *s1;
char *s2;
int atleast;
{
  int len1 = strlen(s1);

  if(len1 < atleast)
    return 0;

  return(strncmp(s1, s2, atleast) == 0);
}   


int main(argc, argv)
int argc;
char *argv[];
{
  int i, j=0, k, ret, once = 0, subscan;
  char raStr[32], decStr[32], *flag;
  int this_scan, last_scan = 0;

  if(argc < 2)
  {
    fprintf(stdout, "Usage: %s [-once] datafile\n", argv[0]);
    exit(0);
  }

  for(i=1;i<argc-1;i++)
  {
    if(argmatch(argv[i], "-o", 2))
    {
      once++;
    }
  }

  fprintf(stdout, "\n");

  strcpy(dataFile,argv[argc-1]);

  if(once)
    fprintf(stdout,"DUMPSCANS: Only Printing One Line Per Scan. (no sub scans)\n");

  if( findSize(dataFile))
  {
    fprintf(stdout,"DUMPSCANS: File %s is empty\n", dataFile);
    fprintf(stdout, "\nUsage: %s [-once] datafile\n", argv[0]);
    exit(1);
  }
  if( (numOfScans = findScans(dataFile)) < 0)
  {
    fprintf(stdout,"DUMPSCANS: File %s is empty\n", dataFile);
    fprintf(stdout, "\nUsage: %s [-once] datafile\n", argv[0]);
    exit(1);
  }

  fprintf(stdout, "\n%s", header);
  for(i=0;i<numOfScans;i++)
  {

    strcpy(source, "                    ");
    scan = scanArray[i].scan;

    if(once)
    {
      this_scan = (int)scan;
      if(this_scan == last_scan)
        continue;

    }
    last_scan = this_scan;

    if( (ret = read_scan( scan, dataFile, scanArray[i].start_rec)) > 0.0)
    {

      strncpy(source, head.object, 16);
      scantype[16] = '\0';
      strncpy(scantype, head.obsmode, 8);
      scantype[8] = '\0';
      sexagesimal( head.epocra/15.0,  raStr,  DD_MM_SS);
      sexagesimal( head.epocdec,  decStr,  DD_MM_SS);

      fprintf(stdout,"%7.2f %9.9s %9.9s %s %-12.12s %6.1f %6.1f", 
		head.scan, raStr, decStr, scantype, source, head.az, head.el);

      if(!strcmp(scantype, "LINETPMF") || !strcmp(scantype, "LINEOTF "))
        fprintf(stdout," %7.2f", head.offscan);
      else
	fprintf(stdout,"        ");

      if(strcmp(scantype, "LINEPCAL"))
        fprintf(stdout," %7.2f", head.gains);
      else
	fprintf(stdout,"        ");

/* Check for scan number duplicates */
      flag = "";
      for (k=0; k<i; k++) {
	if (head.scan == scanArray[k].scan) {
	  flag = " duplicate";
	  break;
	}
      }
      fprintf(stdout, "  %5d%s\n", scanArray[i].entry, flag);
    }

    j++;
    if(!(j%24))
      fprintf(stdout, "\n%s", header);
  }

  if (bad_scans)
    fprintf(stderr, "\n%d bad scans ignored\a\n", bad_scans);
  return( (bad_scans > 255) ? 255 : bad_scans );
}


int findSize(filename )
char *filename;
{
  int fd, n;
  struct BOOTSTRAP  boot;

  if( (fd = pdopen(filename))<0)
    return(-1);

  if( read( fd, (char *)&boot, sizeof(struct BOOTSTRAP))<0 ) {
    perror("read boot");
    (void)close(fd);
    return(-1);
  }
  close(fd);

  swapBoot(&boot);

  n = boot.num_entries_used+1;
  if( (scanArray = (struct SCANS *)calloc(n, sizeof(struct SCANS))) == NULL)
  {
    fprintf(stdout,"otffixoffs: Error in findSize( malloc() )\n");
    return(-1);
  }

  return(0);
}



int findScans(filename )
char *filename;
{
  int fd, dirl, bytesinindex, bytesused, num;
  int sizeofDIR;
  struct BOOTSTRAP  boot;
  struct DIRECTORY  dir;
 
  sizeofDIR = sizeof(struct DIRECTORY);
 
  if( (fd = pdopen(filename))<0)
    return(-1);
 
  if( read( fd, (char *)&boot, sizeof(struct BOOTSTRAP))<0 ) {
    perror("read boot");
    (void)close(fd);
    return(-1);
  }
 
  swapBoot(&boot);

  /* check bootstrap block */
 
  if( sizeof(struct DIRECTORY) != boot.bytperent ) {
    (void)fprintf(stdout,"directory sizes dont match\n");
    (void)close(fd);
    return(-1);
  }
 
  bytperrec = boot.bytperrec;
  bytesinindex = boot.num_index_rec * boot.bytperrec;
  bytesused = boot.num_entries_used * boot.bytperent;
 
  if( bytesinindex < bytesused+sizeof(struct DIRECTORY) ) {
    (void)fprintf(stdout,"Directory structure of %s is full.\n", filename );
    (void)close(fd);
    return(-1);
  }
 
  if( boot.typesdd || !boot.version  ) {
    (void)fprintf(stdout, "SDD version or type of %s is invalid.\n", filename );    (void)close(fd);
    return(-1);
  }
 
  if( sizeof(struct DIRECTORY) != boot.bytperent ) {
    (void)fprintf(stdout,"File %s has faulty direcory size\n",filename );
    (void)close(fd);
    return(-1);
  }
 
  if( boot.num_entries_used <= 0 ) {
    (void)fprintf(stdout,"File %s is empty\n", filename);
    (void)close(fd);
    return(-1);
  }

  num = 0; bad_scans = 0;
  while(num+bad_scans < boot.num_entries_used) {
    dirl = (num+bad_scans) * boot.bytperent + boot.bytperrec;
    (void)lseek( fd, (off_t)dirl, SEEK_SET );
    if(read( fd, (char *)&dir, sizeofDIR ) != sizeofDIR ){
      perror("readdir");
      (void)close(fd);
      return(-1);
    }

    if(swapThisSdd)
      swapDir(&dir);

    if (sanity(fd, &boot, &dir)) {
      scanArray[num].scan = head.scan;
      scanArray[num].start_rec = dir.start_rec;
      scanArray[num].entry = num + bad_scans;
      num++;
    } else
      bad_scans++;			/* Count the insane scans */
  }
  fprintf(stdout,"DUMPSCANS: File %s Contains %d scans.\n", filename, num);
  if (bad_scans)
    fprintf(stdout, "(Excluding the %d erroneous scans above)\n", bad_scans);

  (void)close(fd);

  return(num);
}
 



int read_scan(scan, filename, start_rec )
float scan;
char *filename;
long start_rec;
{
  int fd;
  long max;

  if(data) {
    free((char *)data);
    data = NULL;
  }

  if( (fd = pdopen(filename))<0)
    return(-1);

  max = bytperrec * (start_rec-1);
  (void)lseek( fd, (off_t)max, SEEK_SET ); 
  if(read(fd, (char *)&head, sizeof(struct HEADER) )<0) {
    perror("read header");
    (void)close(fd);
    return(-1);
  }

  if(swapThisSdd)
    swapHeader(&head);

  (void)close(fd);

  return((int)head.scan);
}



static int pdopen(name)
char *name;
{
  int fd;

  if( (fd = open(name, O_RDONLY, 0666 ))<0) {
    fprintf(stdout, "Unable to open %s\n", name );
    return(-1);
  }

  return(fd);
}

/* Perform a sanity check on this scan */
/* Return zero if HEADER is inconsistent with DIRECTORY */

int sanity (int fd, struct BOOTSTRAP *boot, struct DIRECTORY *dir) {

  int ret = 1, max, nchan, nchanmax;

  if (dir->start_rec-1 > MAX_RECORD) {
    fprintf(stdout, 
	   "Start record %ld for scan %.2f extends beyond byte 0x7ffffff\n",
	   dir->start_rec, dir->scan);
    return 0;
  }
  if (dir->end_rec-1 > MAX_RECORD) {
    fprintf(stdout, 
	   "End record %ld for scan %.2f extends beyond byte 0x7ffffff\n",
	   dir->end_rec, dir->scan);
    return 0;
  }

  max = boot->bytperrec * (dir->start_rec-1);
  (void)lseek( fd, (off_t)max, SEEK_SET ); 
  if(read(fd, (char *)&head, sizeof(struct HEADER) )<0) {
    perror("read header");
    (void)close(fd);
    fprintf(stdout, "Could not read header for scan %.2f\n", dir->scan);
    return 0;
  }

  if ((float)head.scan != dir->scan) {
    fprintf(stdout, "HEADER scan # %.2f doesn't match DIRECTORY: %.2f\n",
	    head.scan,dir->scan);
    return 0;
  }

  nchan = head.datalen/sizeof(float);
  nchanmax = (boot->bytperrec * (dir->end_rec - dir->start_rec + 1) -
	      			sizeof(struct HEADER))/sizeof(float);
  if (nchanmax-nchan < 0 || 
      			(nchanmax-nchan)*sizeof(float) >= boot->bytperrec) {
    fprintf(stdout, 
          "Scan %.2f no. of channels %d greater than max from record size %d\n",
          dir->scan, nchan, nchanmax);
    ret = 0;
  }

  if (strncmp(head.object, dir->source, 16)) {
    head.object[16] = 0;
    dir->source[16] = 0;
    fprintf(stdout, 
	    "Scan %.2f header object name %s doesn't match directory's: %s\n",
	    dir->scan, head.object, dir->source);
    ret = 0;
  }

  return ret;
}
