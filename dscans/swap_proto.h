/* swapdata.c */
double swapd(double *d);
unsigned int swapl(unsigned int *l);
int swapi(int *l);
unsigned short swaps(unsigned short *s);
int in_swapus(unsigned short *s);
int in_swapf(float *f);
int in_swaps(short *s);
int in_swapd(double *d);
int in_swapl(unsigned int *ll);
int in_swapi(int *ii);
/* swapsdd.c */
int swapBoot(struct BOOTSTRAP *boot);
int force_swapBoot(struct BOOTSTRAP *boot);
int swapDir(struct DIRECTORY *dir);
int swapHeader(struct HEADER *head);
int swapData(float *data, int n);
