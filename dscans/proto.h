/* dumpscans.c */
int argmatch(char *s1, char *s2, int atleast);
int setColor(int c);
int main(int argc, char *argv[]);
int findSize(char *filename);
int findScans(char *filename, int quiet);
int read_scan(double scan, char *filename, long start_rec);
int sanity(int fd, struct BOOTSTRAP *boot, struct DIRECTORY *dir);
