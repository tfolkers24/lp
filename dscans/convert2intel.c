#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#ifndef __DARWIN__
#include <malloc.h>
#endif
#include <math.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>

#include "caclib_proto.h"
#include "sdd.h"
#include "header.h"
#include "sex.h"

#include "swap_proto.h"

#define MAXSCANS 16534

int swapThisSdd = 0;

int data_write(char *of, struct HEADER *Head, char *Data);

/*
struct BOOTSTRAP  boot;
struct DIRECTORY  dir;
 */
struct HEADER     head;
float             *data = NULL;

typedef enum {
  OTHER_SCAN=0,
  OFF_SCAN,
  ON_SCAN
} scanType;


struct SCANS {
  int      indx;
  scanType scantype;
  float    scan;
  float    off;
  char     scanmode[10];
};


struct SCANS *scanArray=NULL;
int   numOfScans=0;

char source[80], scantype[80];
float scan=0.0, firstscan;

static int pdopen();
int findSize(), findScans(), read_scan();


int argmatch(s1, s2, atleast)
char *s1;
char *s2;
int atleast;
{
  int l1 = strlen(s1);
  int l2 = strlen(s2);
                                                                                
  if(l1 < atleast)
    return 0;
                                                                                
  if(l1 > l2)
    return 0;
                                                                                
  return(strncmp(s1, s2, l1) == 0);
}
                                                                                

int usage()
{
  fprintf(stderr, "\nUsage: convert2intel in_file out_file\n\n");
  exit(0);
}
                                                                                



int main(argc, argv)
int argc;
char *argv[];
{
  int i, j, size, ret, entry;
  char infile[256], outfile[256];
  char rmStr[256];
  float *p;

  i = 1;

  if(argc < 3)
  {
    usage();
  }

  strcpy(infile, argv[argc-2]);
  strcpy(outfile, argv[argc-1]);

  if( findSize(infile))
  {
    fprintf(stderr,"convert2intel: File %s is empty\n", infile);
    fprintf(stderr, "\nUsage: %s datafile\n", argv[0]);
    exit(3);
  }
  if( (numOfScans = findScans(infile)) < 0)
  {
    fprintf(stderr,"convert2intel: File %s is empty\n", infile);
    fprintf(stderr, "\nUsage: %s datafile\n", argv[0]);
    exit(3);
  }
  if(!swapThisSdd)
  {
    fprintf(stderr,"convert2intel: File %s already Intel\n", infile);
    exit(3);
  }
                                                                                
  sprintf(rmStr, "rm -f %s", outfile);
  system(rmStr);

  if(new_sdd(outfile))
  {
    fprintf(stderr,"convert2intel: Error opening Out File %s\n", outfile);
    exit(3);
  }

  for(i=0;i<numOfScans;i++)
  {
    scan = scanArray[i].scan;
    fprintf(stderr,"convert2intel: Processing scan %.2f\r", scan);

    if( (ret = read_scan( scan, infile, &entry)) > 0.0)
    {
      size = head.datalen / sizeof(float);
      p = data;

      for(j=0;j<size;j++, p++)
      {
        in_swapf(p);
      }

      /* Check for missing reft_vel in header */
      if(fabs(head.refpt_vel - head.velocity) > 0.5)
      {
        head.refpt_vel = head.velocity;
      }
        
      data_write(outfile, &head, (char *)data);
    }
  }

  fprintf(stderr, "\n");

  return(0);
}


int findSize(filename )
char *filename;
{
  int fd, n;
  struct BOOTSTRAP  boot;

  if( (fd = pdopen(filename))<0)
    return(-1);

  if( read( fd, (char *)&boot, sizeof(struct BOOTSTRAP))<0 ) {
    perror("read boot");
    (void)close(fd);
    return(-1);
  }
  close(fd);

  swapBoot(&boot);

  n = boot.num_entries_used+1;
  if( (scanArray = (struct SCANS *)calloc(n, sizeof(struct SCANS))) == NULL)
  {
    fprintf(stderr,"otffixoffs: Error in findSize( malloc() )\n");
    return(-1);
  }

  return(0);
}



int findScans(filename )
char *filename;
{
  int fd, dirl, bytesinindex, bytesused, num;
  int sizeofDIR;
  struct BOOTSTRAP  boot;
  struct DIRECTORY  dir;
 
  sizeofDIR = sizeof(struct DIRECTORY);
 
  if( (fd = pdopen(filename))<0)
    return(-1);
 
  if( read( fd, (char *)&boot, sizeof(struct BOOTSTRAP))<0 ) {
    perror("read boot");
    (void)close(fd);
    return(-1);
  }
 
  swapBoot(&boot);

  /* check bootstrap block */
 
  if( sizeof(struct DIRECTORY) != boot.bytperent ) {
    (void)fprintf(stderr,"directory sizes dont match\n");
    (void)close(fd);
    return(-1);
  }
 
  bytesinindex = boot.num_index_rec * boot.bytperrec;
  bytesused = boot.num_entries_used * boot.bytperent;
 
  if( bytesinindex < bytesused+sizeof(struct DIRECTORY) ) {
    (void)fprintf(stderr,"Directory structure of %s is full.\n", filename );
    (void)close(fd);
    return(-1);
  }
 
  if( boot.typesdd || !boot.version  ) {
    (void)fprintf(stderr, "SDD version or type of %s is invalid.\n", filename );    (void)close(fd);
    return(-1);
  }
 
  if( sizeof(struct DIRECTORY) != boot.bytperent ) {
    (void)fprintf(stderr,"File %s has faulty direcory size\n",filename );
    (void)close(fd);
    return(-1);
  }
 
  if( boot.num_entries_used <= 0 ) {
    (void)fprintf(stderr,"File %s is empty\n", filename);
    (void)close(fd);
    return(-1);
  }
 
  num = 0;
  while(num < boot.num_entries_used) {
    dirl = num * boot.bytperent + boot.bytperrec;
    (void)lseek( fd, (off_t)dirl, SEEK_SET );
    if(read( fd, (char *)&dir, sizeofDIR ) != sizeofDIR ){
      perror("readdir");
      (void)close(fd);
      return(-1);
    }

    if(swapThisSdd)
      swapDir(&dir);

    scanArray[num].scan = dir.scan;
    num++;
    if(num > MAXSCANS)
    {
      fprintf(stderr, "File %s contains too many scans\n", filename);
      return(num);
    }
  }
  fprintf(stderr,"convert2intel: File %s Contains %d scans.\n", filename, num);
  (void)close(fd);

  return(num);
}
 



int read_scan(scan, filename, entry )
float scan;
char *filename;
int *entry;
{
  int fd, dirl, bytesinindex, bytesused, num, max;
  int sizeofDIR, datalen;
  struct BOOTSTRAP  boot;
  struct DIRECTORY  dir;

  sizeofDIR = sizeof(struct DIRECTORY);
  if(data) {
    free((char *)data);
    data = NULL;
  }

  if( (fd = pdopen(filename))<0)
    return(-1);

  if( read( fd, (char *)&boot, sizeof(struct BOOTSTRAP))<0 ) {
    perror("read boot");
    (void)close(fd);
    return(-1);
  }
  
  swapBoot(&boot);

  /* check bootstrap block */

  if( sizeof(struct DIRECTORY) != boot.bytperent ) {
    (void)fprintf(stderr,"directory sizes dont match\n");
    (void)close(fd);
    return(-1);
  }

  bytesinindex = boot.num_index_rec * boot.bytperrec;
  bytesused = boot.num_entries_used * boot.bytperent;
  
  if( bytesinindex < bytesused+sizeof(struct DIRECTORY) ) {
    (void)fprintf(stderr,"Directory structure of %s is full.\n", filename );
    (void)close(fd);
    return(-1);
  }

  if( boot.typesdd || !boot.version  ) {
    (void)fprintf(stderr, "SDD version or type of %s is invalid.\n", filename );
    (void)close(fd);
    return(-1);
  }

  if( sizeof(struct DIRECTORY) != boot.bytperent ) {
    (void)fprintf(stderr,"File %s has faulty direcory size\n",filename );
    (void)close(fd);
    return(-1);
  }

  if( boot.num_entries_used <= 0 ) {
    (void)fprintf(stderr,"File %s is empty\n", filename);
    (void)close(fd);
    return(-1);
  }
 
  if( scan < 0.0) {
    dirl = boot.bytperrec;
    (void)lseek( fd, (off_t)dirl, SEEK_SET );
    if(read( fd, (char *)&dir, sizeofDIR ) != sizeofDIR ){
      perror("readdir");
      (void)close(fd);
      return(-1);
    }

    if(swapThisSdd)
      swapDir(&dir);

    firstscan = dir.scan;
    dirl = (boot.num_entries_used-1) * boot.bytperent + boot.bytperrec;
    (void)lseek( fd, (off_t)dirl, SEEK_SET );
    if(read( fd, (char *)&dir, sizeofDIR ) != sizeofDIR ) {
      perror("readdir");
      (void)close(fd);
      return(-1);
    }
  }
  else
  if(scan > 0.0)
  {
    num = 0;
    while(num < boot.num_entries_used) {
      dirl = num * boot.bytperent + boot.bytperrec;
      (void)lseek( fd, (off_t)dirl, SEEK_SET );
      if(read( fd, (char *)&dir, sizeofDIR ) != sizeofDIR ){
        perror("readdir");
        (void)close(fd);
        return(-1);
      }

      if(swapThisSdd)
        swapDir(&dir);

      if( scan == dir.scan )
      {
        *entry = num;
        break;
      }
      num++;
    }
    if( num >= boot.num_entries_used)
    {
      (void)fprintf(stderr,"Scan %7.2f NOT Found\n", scan);
      (void)close(fd);
      return(-1);
    }
  }
  
  max = boot.bytperrec * (dir.start_rec-1);
  (void)lseek( fd, (off_t)max, SEEK_SET ); 
  if(read(fd, (char *)&head, sizeof(struct HEADER) )<0) {
    perror("read header");
    (void)close(fd);
    return(-1);
  }

  if(swapThisSdd)
    swapHeader(&head);

  datalen = (int)head.datalen;
  data = (float *)malloc( (unsigned)datalen );
  bzero( (char *)data, datalen );
                                                                                
  if( read( fd, (char *)data, datalen) <0 ) {
    perror("read data");
    (void)fprintf(stderr,"convert2intel: Error reading scan %7.2f data\n", scan);
    (void)close(fd);
    return(-1);
  }

  (void)close(fd);

  return((int)head.scan);
}



static int pdopen(name)
char *name;
{
  int fd;

  if( (fd = open(name, O_RDONLY, 0666 ))<0) {
    fprintf(stderr, "Unable to open %s\n", name );
    return(-1);
  }

  return(fd);
}

int data_write(of, Head, Data)
char *of;
struct HEADER *Head;
char *Data;
{
  char   buf[256];
  int rem;

  if( !Head || !Data ) {
    fprintf(stderr,"convert2intel: bad Data, can't save\n");
    return(-1);
  }

  buf[0] = '\0';
  switch(rem = write_sdd(of, Head, Data)) {
    case -1:
      sprintf(buf,"convert2intel: Error opening sdd file %s\n", of);
      break;
    case -2:
      sprintf(buf,"convert2intel: Error reading sdd file %s\n", of);
      break;
    case -3:
      sprintf(buf,"convert2intel: Error writing sdd file %s\n", of);
      break;
    case -4:
      sprintf(buf,"convert2intel: Directory mismatch in sdd file %s\n", of);
      break;
    case -5:
      sprintf(buf,"convert2intel: Directory structure full, in file %s\n", of);
      break;
    case -6:
      break;
    case -7:
      sprintf(buf,"convert2intel: Error (l)seeking in file %s\n", of);
      break;
    default:
      if( rem <= 0  )
        sprintf(buf,"convert2intel: Directory structure full, in file %s\n", of);
      else if( (rem <= 100 && rem % 10 == 0) || rem < 10 ) {
        sprintf( buf, "convert2intel: Only %d entries left in file %s\n", rem, of );
      }
      break;
  }

  if( buf[0] )
    fprintf(stderr,buf);

  return(rem);
}

