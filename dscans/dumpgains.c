#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#ifndef __DARWIN__
#include <malloc.h>
#endif
#include <math.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>

#include "caclib_proto.h"
#include "header.h"
#include "sdd.h"
#include "sex.h"

#define MAX_RECORD 0x3fffff

struct BOOTSTRAP  boot;
struct DIRECTORY  dir;
struct HEADER     head;
float             *data = NULL;
int               bytperrec;

typedef enum {
  OTHER_SCAN=0,
  OFF_SCAN,
  ON_SCAN
} scanType;


struct SCANS {
  int      indx;
  int      entry;
  long     start_rec;
  scanType scantype;
  double   scan;
  double   off;
  char     scanmode[10];
};

int sanity(int fd, struct BOOTSTRAP *, struct DIRECTORY *);

struct SCANS *scanArray=NULL;
int   numOfScans=0, bad_scans=0;

char dataFile[256], source[80], scantype[80];
int first=1, last;
float scan=0.0, firstscan;

static int pdopen();
int findSize(), findScans(), read_scan();


char *header = "Scan#      R.A.     Dec.     Mode       Source      Az      El    Freq\n\n";


int getFeed(scan)
double scan;
{
  int feed;
  double tmp;

  tmp = scan - floor(scan);
  tmp *= 100.0;
  feed = (int)(tmp + 0.25);

  return(feed);
}


int argmatch(s1, s2, atleast)
char *s1;
char *s2;
int atleast;
{
  int len1 = strlen(s1);

  if(len1 < atleast)
    return 0;

  return(strncmp(s1, s2, atleast) == 0);
}   


int main(argc, argv)
int argc;
char *argv[];
{
  int i, j, ret, dl;
  int feed;
  double total;

  if(argc < 2)
  {
    fprintf(stdout, "Usage: %s datafile\n", argv[0]);
    exit(0);
  }

  fprintf(stdout, "\n");

  strcpy(dataFile,argv[argc-1]);

  if( findSize(dataFile))
  {
    fprintf(stdout,"DUMPSCANS(1): File %s is empty\n", dataFile);
    fprintf(stdout, "\nUsage: %s datafile\n", argv[0]);
//    exit(1);
  }
  if( (numOfScans = findScans(dataFile)) < 0)
  {
    fprintf(stdout,"DUMPSCANS(2): File %s is empty\n", dataFile);
    fprintf(stdout, "\nUsage: %s datafile\n", argv[0]);
    exit(1);
  }

  fprintf(stdout, "\n%s", header);

  for(i=1;i<numOfScans;i++)
  {
    scan = scanArray[i].scan;

    feed = getFeed(scan);

    if(feed < 10)
    {
      if((ret = read_scan(scan, dataFile, scanArray[i].start_rec)) > 0.0)
      {
        printf("%8.2f ", scan);
        dl = head.datalen / sizeof(float);

        total = 0.0;
        for(j=0;j<dl;j++)
        {
          total += data[j];
        }

        total /= (double)dl;

        printf("%10.1f %10.1f %7.4f %10.10s %5.1f %.1f\n", total, head.stsys, total/head.stsys, head.object, head.restfreq, head.tcal);
//        printf("%10.6f\n", total / head.stsys);
      }
    }
  }

  return((bad_scans > 255) ? 255 : bad_scans);
}


int findSize(filename )
char *filename;
{
  int fd, n;
  struct BOOTSTRAP  boot;

  if( (fd = pdopen(filename))<0)
    return(-1);

  if( read( fd, (char *)&boot, sizeof(struct BOOTSTRAP))<0 ) {
    perror("read boot");
    (void)close(fd);
    return(-1);
  }
  close(fd);

  n = boot.num_entries_used+1;
  if( (scanArray = (struct SCANS *)calloc(n, sizeof(struct SCANS))) == NULL)
  {
    fprintf(stdout,"otffixoffs: Error in findSize( malloc() )\n");
    return(-1);
  }

  return(0);
}



int findScans(filename )
char *filename;
{
  int fd, dirl, bytesinindex, bytesused, num;
  int sizeofDIR;
  struct BOOTSTRAP  boot;
  struct DIRECTORY  dir;
 
  sizeofDIR = sizeof(struct DIRECTORY);
 
  if((fd = pdopen(filename))<0)
  {
    return(-1);
  }
 
  if(read( fd, (char *)&boot, sizeof(struct BOOTSTRAP))<0)
  {
    perror("read boot");
    close(fd);

    return(-1);
  }
 
  /* check bootstrap block */
 
  if(sizeof(struct DIRECTORY) != boot.bytperent)
  {
    fprintf(stdout,"directory sizes dont match\n");
    close(fd);

    return(-1);
  }
 
  bytperrec = boot.bytperrec;
  bytesinindex = boot.num_index_rec * boot.bytperrec;
  bytesused = boot.num_entries_used * boot.bytperent;
 
  if(bytesinindex < bytesused+sizeof(struct DIRECTORY))
  {
    fprintf(stdout,"Directory structure of %s is full.\n", filename );
//    close(fd);

//    return(-1);
  }
 
  if(boot.typesdd || !boot.version)
  {
    fprintf(stdout, "SDD version or type of %s is invalid.\n", filename );
    close(fd);

    return(-1);
  }
 
  if(sizeof(struct DIRECTORY) != boot.bytperent)
  {
    fprintf(stdout,"File %s has faulty direcory size\n",filename );
    close(fd);

    return(-1);
  }
 
  if(boot.num_entries_used <= 0 )
  {
    fprintf(stdout,"File %s is empty\n", filename);
    close(fd);

    return(-1);
  }

  num = 0; bad_scans = 0;

  while(num+bad_scans < boot.num_entries_used)
  {
    dirl = (num+bad_scans) * boot.bytperent + boot.bytperrec;

    lseek( fd, (off_t)dirl, SEEK_SET );

    if(read(fd, (char *)&dir, sizeofDIR ) != sizeofDIR)
    {
      perror("readdir");
      close(fd);

      return(-1);
    }

    if(sanity(fd, &boot, &dir))
    {
      scanArray[num].scan = head.scan;
      scanArray[num].start_rec = dir.start_rec;
      scanArray[num].entry = num + bad_scans;
      num++;
    }
    else
    {
      bad_scans++;			/* Count the insane scans */
    }
  }

  fprintf(stdout,"DUMPSCANS: File %s Contains %d scans.\n", filename, num);

  if(bad_scans)
  {
    fprintf(stdout, "(Excluding the %d erroneous scans above)\n", bad_scans);
  }

  close(fd);

  return(num);
}
 



int read_scan(scan, filename, start_rec)
float scan;
char *filename;
long start_rec;
{
  int fd, l, i, datalen;
  long max;

  if(data)
  {
    free((char *)data);
    data = NULL;
  }

  if((fd = pdopen(filename))<0)
  {
    return(-1);
  }

  max = bytperrec * (start_rec-1);

  lseek(fd, (off_t)max, SEEK_SET ); 

  if(read(fd, (char *)&head, sizeof(struct HEADER)) < 0)
  {
    perror("read header");
    close(fd);

    return(-1);
  }

  datalen = (int)head.datalen;
  data = (float *)malloc( (unsigned)datalen );
  bzero( (char *)data, datalen );

  if(read( fd, (char *)data, datalen) <0)
  {
    perror("read data");
    fprintf(stderr, "Error reading scan %7.2f data\n", scan);
    close(fd);

    return(-1);
  }

                                /* now check for abnormal float in data array */
  l = datalen / sizeof(float);
  for(i=0;i<l;i++)
  {
    if(isnan( (double)data[i]))
    {
      data[i] = 0.0;
    }
  }

  close(fd);

  return((int)head.scan);
}



static int pdopen(name)
char *name;
{
  int fd;

  if( (fd = open(name, O_RDONLY, 0666 ))<0) {
    fprintf(stdout, "Unable to open %s\n", name );
    return(-1);
  }

  return(fd);
}

/* Perform a sanity check on this scan */
/* Return zero if HEADER is inconsistent with DIRECTORY */

int sanity (int fd, struct BOOTSTRAP *boot, struct DIRECTORY *dir) {

  int ret = 1, max, nchan, nchanmax;

  if (dir->start_rec-1 > MAX_RECORD) {
    fprintf(stdout, 
	   "Start record %u for scan %.2f extends beyond byte 0x7ffffff\n", dir->start_rec, dir->scan);
    return 0;
  }
  if (dir->end_rec-1 > MAX_RECORD) {
    fprintf(stdout, 
	   "End record %u for scan %.2f extends beyond byte 0x7ffffff\n", dir->end_rec, dir->scan);
    return 0;
  }

  max = boot->bytperrec * (dir->start_rec-1);
  (void)lseek( fd, (off_t)max, SEEK_SET ); 
  if(read(fd, (char *)&head, sizeof(struct HEADER) )<0) {
    perror("read header");
    (void)close(fd);
    fprintf(stdout, "Could not read header for scan %.2f\n", dir->scan);
    return 0;
  }

  if ((float)head.scan != dir->scan) {
    fprintf(stdout, "HEADER scan # %.2f doesn't match DIRECTORY: %.2f\n",
	    head.scan,dir->scan);
    return 0;
  }

  nchan = head.datalen/sizeof(float);
  nchanmax = (boot->bytperrec * (dir->end_rec - dir->start_rec + 1) -
	      			sizeof(struct HEADER))/sizeof(float);
  if (nchanmax-nchan < 0 || 
      			(nchanmax-nchan)*sizeof(float) >= boot->bytperrec) {
    fprintf(stdout, 
          "Scan %.2f no. of channels %d greater than max from record size %d\n",
          dir->scan, nchan, nchanmax);
    ret = 0;
  }

  if (strncmp(head.object, dir->source, 16)) {
    head.object[16] = 0;
    dir->source[16] = 0;
    fprintf(stdout, 
	    "Scan %.2f header object name %s doesn't match directory's: %s\n",
	    dir->scan, head.object, dir->source);
    ret = 0;
  }

  return ret;
}


/* write the data out with normal error and informative messages */

int data_write(of, Head, Data)
char *of;
struct HEADER *Head;
float *Data;
{
  char   buf[256];
  int    rem;

  if( !Head || !Data )
  {
    fprintf(stderr, "COPYSCANS: Bad Data, Can't Save\n");
    return(-1);
  }

  buf[0] = '\0';

  switch(rem = write_sdd(of, Head, (char *)Data))
  {
    case -1:
      sprintf(buf,"FIXSB Error opening sdd file %s\n",of);
      break;

    case -2:
      sprintf(buf,"FIXSB Error reading sdd file %s\n",of);
      break;

    case -3:
      sprintf(buf,"FIXSB Error writing sdd file %s\n",of);
      break;

    case -4:
      sprintf(buf,"FIXSB Directory mismatch in sdd file %s\n",of);
      break;

    case -5:
      sprintf(buf,"FIXSB Directory structure full, in file %s\n",of);
      break;

    case -6:
      break;

    case -7:
      sprintf(buf,"FIXSB Error (l)seeking in file %s\n",of);
      break;

    default:
      if( rem <= 0  )
      {
        sprintf(buf,"FIXSB Directory structure full, in file %s\n", of);
      }
      else
      if( (rem <= 100 && rem % 10 == 0) || rem < 10 )
      {
        sprintf( buf, "FIXSB Only %d entries left in file %s\n", rem, of );
      }
      break;
  }

  if( buf[0] )
  {
    fprintf(stderr, buf);
  }

  return(rem);
}
