#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#ifndef __DARWIN__
#include <malloc.h>
#endif
#include <math.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>

#include "caclib_proto.h"
#include "sdd.h"
#include "header.h"
#include "sex.h"

#include "swap_proto.h"

int swapThisSdd = 0;

static int pdopen();
int findSize();

/* Return:
   0:  File is already IEEE
   1:  File needs converting
   2:  No File given
 */

int main(argc, argv)
int argc;
char *argv[];
{
  char infile[256];

  if(argc < 2)
  {
    fprintf(stderr, "\nUsage: isIEEE in_file\n\n");
    exit(0);
  }

  strcpy(infile, argv[argc-1]);

  if( findSize(infile))
  {
    fprintf(stderr,"isIEEE: File %s is empty\n", infile);
    fprintf(stderr, "\nUsage: %s datafile\n", argv[0]);
    exit(2);
  }

  if(!swapThisSdd)
  {
    fprintf(stderr,"isIEEE: File %s already Intel\n", infile);
    exit(0);
  }
                                                                                
  fprintf(stderr,"isIEEE: File %s Needs Convertion to Intel\n", infile);

  exit(1);
}


int findSize(filename )
char *filename;
{
  int fd;
  struct BOOTSTRAP  boot;

  if( (fd = pdopen(filename))<0)
  {
    return(-1);
  }

  if( read( fd, (char *)&boot, sizeof(struct BOOTSTRAP))<0 ) {
    perror("read boot");
    (void)close(fd);
    return(-1);
  }
  close(fd);

  swapBoot(&boot);

  return(0);
}

static int pdopen(name)
char *name;
{
  int fd;

  if( (fd = open(name, O_RDONLY, 0666 ))<0) {
    fprintf(stderr, "Unable to open %s\n", name );
    return(-1);
  }

  return(fd);
}
