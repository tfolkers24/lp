#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>
#include <math.h>

#include "parse_renders_proto.h"
#include "linuxlib_proto.h"
#include "caclib_proto.h"

int main(argc, argv)
int argc;
char *argv[];
{
  int   i, n, nn;
  int   ports[32];
  char *lph, *cp, inBuf[256], pname[256];
  FILE *fp;
   
  log_open("get_socket_ports", 4);

  if(argc < 2)
  {
    log_msg("Usage: %s number_of_ports", argv[0]);

    fprintf(stdout, "-1\n");

    exit(1);
  }

  n = atoi(argv[1]);

  if(n < 1 || n > 31)
  {
    log_msg("Usage: %s number_of_ports must be > 0 < 32", argv[0]);

    fprintf(stdout, "-1\n");

    exit(1);
  }

  lph = getenv("LINUXPOPS_HOME");

  if(!lph)
  {
    log_msg("%s LINUXPOPS_HOME not set", argv[0]);

    fprintf(stdout, "-1\n");

    exit(1);
  }

  log_msg("%s: creating %d socket ports", argv[0], n);

  sprintf(pname, "%s/bin/find_socket.py", lph);

  for(i=0;i<n;i++)
  {
    fp = popen(pname, "r");

    if(!fp)
    {
      log_msg("%s: Unable to open pipe: %s", argv[0], pname);

      fprintf(stdout, "-1\n");

      exit(1);
    }

    while((cp = fgets(inBuf, sizeof(inBuf), fp)) != NULL)
    {
      nn = sscanf(inBuf, "%d", &ports[i]);

      if(nn != 1)
      {
        log_msg("%s: Error parsing port number: %s", argv[0], inBuf);

        fprintf(stdout, "-1\n");

        exit(1);
      }
    }

    pclose(fp);
  }

  /* printout all n ports */
  printf("PORTS: ");
  for(i=0;i<n;i++)
  {
    printf("%d ", ports[i]);
  }

  printf("\n");
  

  return(0);
}
