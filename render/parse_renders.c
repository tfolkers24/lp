#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>
#include <math.h>

#include "parse_renders_proto.h"
#include "linuxlib_proto.h"
#include "caclib_proto.h"

#define MAX_HOSTNAME_LEN  32
#define MAX_HOSTS        128		/* max allowed hosts in render farm; yeah, I wish... */
#define MAX_PORTS         32		/* allowed number of gridders and render threads */

struct HOSTS {
	char host[MAX_HOSTNAME_LEN];

	int  ngridders;
	int  good;

	int  ports[MAX_PORTS];
};

struct HOSTS hosts[MAX_HOSTS];

char tok[64][64];

int get_tok(edit)
char *edit;
{
  int c = 0;
  char *token;

  bzero((char *)&tok, sizeof(tok));

  token = strtok(edit, " \t\n");
  while(token != (char *)NULL)
  {
    sprintf(tok[c++], "%s", token);
    token = strtok((char *)NULL, " \t\n");
  }

  return(c);
}


int main(argc, argv)
int argc;
char *argv[];
{
  int   i, j, n, nn, ret, tp, nhosts=0, found_one;
  char *cp, *lph, sysBuf[256], inBuf[256], saveBuf[256];
  FILE *fp;
  struct HOSTS *p;

  log_open("parse_renders", 4);

  if(argc < 2)
  {
    log_msg("Usage: %s render_farm.lost", argv[0]);
    exit(1);
  }

  lph = getenv("LINUXPOPS_HOME");

  if(!lph)
  {
    log_msg("%s LINUXPOPS_HOME not set", argv[0]);

    exit(1);
  }

  log_msg("%s Started", argv[0]);

  fp = fopen(argv[1], "r");

  if(!fp)
  {
    log_msg("%s: Unable to open file: %s", argv[0], argv[1]);
    exit(1);
  }
  
  while((cp = fgets(inBuf, sizeof(inBuf), fp)) != NULL)
  {
    deNewLineIt(inBuf);
    strcmprs(inBuf);

    log_msg("Read: %s", inBuf);

    if(!strncmp(inBuf, "#", 1))
    {
      log_msg("Skipping comment");
      continue;
    }

    if(strlen(inBuf) < 4)
    {
      log_msg("Skipping empty");
      continue;
    }

    strcpy(saveBuf, inBuf);

    n = get_tok(inBuf);

    if(n == 2)
    {
      p = &hosts[nhosts];

      strcpy(p->host, tok[0]);

      p->ngridders = atoi(tok[1]);

      nhosts++;
    }
    else
    {
      log_msg("%s: Strange input line: %s", argv[0], saveBuf);
    }
  }

  fclose(fp);

  log_msg("Loaded %d render hosts", nhosts);

  p = &hosts[0];
  for(i=0;i<nhosts;i++,p++)
  {
    sprintf(sysBuf, "ping -c 1 -W 0.333 %s > /dev/null", p->host);
    ret = system(sysBuf);

    if(ret)
    {
      log_msg("%s, not responding", p->host);
      p->good = 0;
    }
    else
    {
      sprintf(sysBuf, "ssh %s \"%s/bin/get_socket_ports %d\"", p->host, lph, p->ngridders);

      log_msg("executing: %s", sysBuf);

      fp = popen(sysBuf, "r");

      if(!fp)
      {
        log_msg("%s: Unable to open pipe: %s", argv[0], sysBuf);
        exit(1);
      }

      while((cp = fgets(inBuf, sizeof(inBuf), fp)) != NULL)
      {
        if(!strncmp(inBuf, "PORTS: ", 7))
        {
          nn = get_tok(inBuf);
	  nn--;				/* remove 'PORTS: ' */

          if(nn != p->ngridders)
          {
            log_msg("%s: Expected ports: %d, received: %d", argv[0], p->ngridders, nn);

            exit(1);
          }

          for(j=0;j<nn;j++)
          {
            p->ports[j] = atoi(tok[j+1]);	/* remobe, skipping 'PORTS: ' */
          }

          p->good = 1;
        }
        else
        {
	  log_msg("Skipping return line: %s", inBuf);
        }
      }

      pclose(fp);
    }
  }

  /* Now, here comes the fun part...
     In order to not lump the gridders in host order, 
     we need to 'randomize' them a bit;  This is because
     the 'gridder_load' script just assigns each new tile
     to the next 'socket' port listed in the mailboxdefs
     file.  So, don't lump ports on the same host in sequence.
  */
  tp = 0;

  while(1)
  {
    found_one = 0;

    p = &hosts[0];
    for(i=0;i<nhosts;i++,p++)
    {
      if(p->good)
      {
        for(j=0;j<MAX_PORTS;j++)
        {
          if(p->ports[j])
          {
            printf("%5d %s GRIDDER_%d\n", p->ports[j], p->host, tp);
            tp++;

            p->ports[j] = 0;			/* zero that one out */

	    found_one++;

            break;
          }
        }

      }
    }

    if(!found_one)
    {
      break;
    }
  }

  return(0);
}
