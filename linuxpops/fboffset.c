/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

float fboffsets[MAX_FILTER_SEGS];


int printFBOffsets()
{
  int seg;

  for(seg=0;seg<MAX_FILTER_SEGS;seg++)
  {
    st_print("Segment %3d: Offset = %12.6f\n", seg, fboffsets[seg]);

    if(((seg+1) * env->markers) >= h0->head.noint)
    {
      return(0);
    }
  }

  return(0);
}


int autoLevel()
{
  int i, seg, m, k, j=0, noint;
  double sum, gsum;
  struct LP_IMAGE_INFO *image = h0;

  if(env->markers > 0)
  {
    noint = (int)image->head.noint;
    k = 0;

    for(i=0;i<noint;i++)
    {
      if(isInBaseLineRegion(i))
      {
        gsum += image->yplot1[i];
        j++;
      }
    }

    if(j > 10)
    {
      gsum /= (double)j;

      st_report("autoLevel(): Grand Avg of %d Baseline Channels: %f", j, gsum);

      for(seg=0;seg<MAX_FILTER_SEGS;seg++)
      {
        sum = 0.0;

        if(k >= noint) /* Stop then */
        {
          return(0);
        }

        for(m=0;m<env->markers;m++)
        {
          sum += image->yplot1[k];
          k++;
        }

        sum /= (double)env->markers;

        st_report("autoLevel(): Segment %3d Avg = %10.6f, diff = %10.6f", seg, sum, sum-gsum);

        fboffsets[seg] = sum-gsum;
      }
    }
    else
    {
      st_fail("autoLevel(): Little or no baseline channels defined");
      return(1);
    }
  }
  else
  {
    st_fail("autoLevel(): Can NOT perform Auto Level without variable: 'markers', being set");
    return(1);
  }

  return(0);
}


int autoMatch(seg)
int seg;
{
  int noint, m;
  int     g1,  b1,  g2,  b2; /* Indx into array */
  double db1, db2, dg1, dg2; /* Array values */
  double diff1, diff2, avg;
  struct LP_IMAGE_INFO *image = h0;

  noint = image->head.noint;
  m     = env->markers;

  if(seg == 0) /* Can only use right edge for matching */
  {
    b2 = (seg+1) * m -1;
    g2 = b2+1;

    db2 = image->yplot1[b2];
    dg2 = image->yplot1[g2];

    diff2 = db2 - dg2;

    if(isSet(PF_V_FLAG))
    {
      st_report("Bad  Channel Right Edge %3d", b2);
      st_report("Good Channel Left  Edge %3d", g2);
      st_report("Bad  Channel Value is %5.2f", db2);
      st_report("Good Channel Value is %5.2f", dg2);
      st_report("Diff2 = %f", diff2);
    }

    st_report("Applying a %.3f offset to segment %d", diff2, seg);

    fboffsets[seg] = diff2;
  }
  else
  if((seg * m + m) >= noint) /* Can only use left edge for matching */
  {
    b1 = seg * m;
    g1 = b1-1;

    db1 = image->yplot1[b1];
    dg1 = image->yplot1[g1];

    diff1 = db1 - dg1;

    if(isSet(PF_V_FLAG))
    {
      st_report("Bad  Channel Left  Edge %3d", b1);
      st_report("Good Channel Right Edge %3d", g1);
      st_report("Bad  Channel Value is %5.2f", db1);
      st_report("Good Channel Value is %5.2f", dg1);
      st_report("Diff1 = %f", diff1);
    }

    st_report("Applying a %.3f offset to segment %d", diff1, seg);

    fboffsets[seg] = diff1;
  }
  else
  {
    b1 = seg * m;
    b2 = (seg+1) * m -1;

    g1 = b1-1;
    g2 = b2+1;


    db1 = image->yplot1[b1];
    db2 = image->yplot1[b2];
    dg1 = image->yplot1[g1];
    dg2 = image->yplot1[g2];


    diff1 = db1 - dg1;
    diff2 = db2 - dg2;


    avg = (diff1 + diff2) / 2.0;

    if(isSet(PF_V_FLAG))
    {
      st_report("Bad  Channels are %3d - %3d", b1, b2);
      st_report("Good Channels are %3d & %3d", g1, g2);
      st_report("Bad  Channels Values are %5.2f - %5.2f", db1, db2);
      st_report("Good Channels Values are %5.2f & %5.2f", dg1, dg2);
      st_report("Diff1 = %f", diff1);
      st_report("Diff2 = %f", diff2);
      st_report("Avg   = %f", avg);
    }

    st_report("Applying a %.3f offset to segment %d", avg, seg);

    fboffsets[seg] = avg;
  }


  return(0);
}



int doFBOffset()
{
  int    seg, seg2, old_mshow;
  double from, to, diff;

  if(isSet(PF_C_FLAG))
  {
    clearFBOffets();

    return(0);
  }

  if(!env->markers)
  {
    st_fail("'markers' must be set to a valid value");
    return(1);
  }

  if(isSet(PF_L_FLAG))
  {
    printFBOffsets();

    return(0);
  }

  if(isSet(PF_A_FLAG))
  {
    /* Do Auto level segments */
    autoLevel();
    applyOffsets(h0);

    return(0);
  }

  old_mshow = env->mshow;
  env->mshow=1;
  xx();

  if(strlen(args->secondArg))
  {
    if(!strncmp(args->secondArg, "match", strlen(args->secondArg)))
    {
      if(strlen(args->thirdArg))
      {
        seg = atoi(args->thirdArg);

        autoMatch(seg);

        applyOffsets(h0);
  
        env->mshow = old_mshow;

        return(0);
      }
      else
      {
        st_fail("Third arg missing for 'fboffset match seq'");
        env->mshow = old_mshow;

        return(1);
      }

      return(0);
    }

    if(!strncmp(args->secondArg, "bmatch", strlen(args->secondArg)))
    {
      if(!valid_bl_averages)
      {
        st_fail("Must do a 'avg /b /s' before using 'bmatch' function");
	return(1);
      }

      if(strlen(args->thirdArg) && strlen(args->fourthArg))
      {
        seg = atoi(args->thirdArg);

        from = bl_averages[seg];

        seg2 = atof(args->fourthArg);

        to   = bl_averages[seg2];

        diff = from - to;

        st_report("Applying a %.3f offset to segment %d", diff, seg);

        fboffsets[seg] =  diff;

        applyOffsets(h0);

        env->mshow = old_mshow;

        return(0);
      }
      else
      {
        printH_FlagHelp(args->firstArg);
        env->mshow = old_mshow;
      }

    return(1);
    }

    seg = atoi(args->secondArg);

    if(seg >= 0 && seg < MAX_FILTER_SEGS)
    {
      if(args->ntokens == 4)  /* User supplied offsets */
      {
        if(strlen(args->thirdArg) && strlen(args->fourthArg))
        {
          from = atof(args->thirdArg);
          to   = atof(args->fourthArg);

          diff = from - to;

          st_report("Applying a %.3f offset to segment %d", diff, seg);

          fboffsets[seg] =  diff;

          applyOffsets(h0);

          env->mshow = old_mshow;

          return(0);
        }
      }
      else
      if(args->ntokens == 2)  /* User will set offsets with mouse */
      {
        _doTCur("Click the Left Mouse Button on Current Value");
        from = env->tcur;

        _doTCur("Click the Left Mouse Button on Desired Value");
        to = env->tcur;

        diff = from - to;

        st_report("Applying a %.3f offset to segment %d", diff, seg);

        fboffsets[seg] =  diff;

        applyOffsets(h0);

        env->mshow = old_mshow;

        return(0);
      }
    }
  }
  else
  {
    printH_FlagHelp(args->firstArg);
    env->mshow = old_mshow;

    return(1);
  }

  env->mshow = old_mshow;

  return(0);
}


int applyOffsets(image)
struct LP_IMAGE_INFO *image;
{
  int m, seg, k;

  st_report("Removing FB offset in %s", image->name);

  if(env->markers > 0)
  {
    k = 0;

    for(seg=0;seg<MAX_FILTER_SEGS;seg++)
    {
      for(m=0;m<env->markers;m++)
      {
        image->yplot1[k] -= fboffsets[seg];
        k++;

        if(k >= image->head.noint)
        {
//          fboffsets[seg] = 0.0; /* Don't apply again */

          return(0);
        }
      }

//      fboffsets[seg] = 0.0; /* Don't apply again */
    }
  }
  else
  {
    st_fail("Markers must be non-zero if using filter offsets");
  }

  return(1);
}


int clearFBOffets()
{
  int i;
  double total = 0.0;

  for(i=0;i<MAX_FILTER_SEGS; i++)
  {
    total += fabs(fboffsets[i]);
  }

  if(total > 0.0) /* Was there any offset before ?? */
  {
    st_report("Clear out all FB offsets");
  }

  bzero((char *)&fboffsets, sizeof(fboffsets));

  return(0);
}


