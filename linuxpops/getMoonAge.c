/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */

/*  Returns the time since the last new moon             */
/*                                                       */
/*  The file NEWMOONS contains new-moon times from       */
/*  page A1 of the Almanac.  When updating the file,     */
/*  be sure to retain at least the entry for the most    */
/*  recent, previous new moon.                           */
/*                                                       */
 
#include "extern.h"

#define NEWMOONS  "newmoons"

double getMoonAge(int yr, int mon, int day, int hr, int min)
{
  int count;
  int nm_yr, nm_mon, nm_day, nm_hr, nm_min;
  int last_nm_yr, last_nm_mon, last_nm_day, last_nm_hr, last_nm_min;
  double mjd, nm_mjd, last_nm_mjd;
  char nmfile[256];

  FILE *fp;

  sprintf(nmfile, "%s/share/%s", pkg_home, NEWMOONS);

  if((fp = fopen(nmfile, "r")) == NULL)
  {
    st_fail("getMoonAge(): Can't open %s", nmfile);

    return(0);
  }

  count = 0;
  nm_mjd = 0.0;
  mjd = getmjd(yr, mon, day, hr, min);

  while(fscanf(fp, "%d %d %d %d %d", &nm_yr, &nm_mon, &nm_day, &nm_hr, &nm_min) != EOF)
  {
    if((nm_mjd = getmjd(nm_yr, nm_mon, nm_day, nm_hr, nm_min)) > mjd)
    {
      break;
    }
    else
    {
      last_nm_yr  = nm_yr;
      last_nm_mon = nm_mon;
      last_nm_day = nm_day;
      last_nm_hr  = nm_hr;
      last_nm_min = nm_min;
      last_nm_mjd = nm_mjd;
      count++;
    }
  }

  fclose(fp);

  if(count == 0)
  {
    st_fail("getMoonAge(): no prior new moons listed in %s", nmfile);
    return(0);
  }

  last_nm_mjd = getmjd(last_nm_yr, last_nm_mon, last_nm_day, last_nm_hr, last_nm_min);

  if (nm_mjd == last_nm_mjd) 
  {
    setColor(ANSI_YELLOW);
    st_report("getMoonAge(): %s is out of date", nmfile);
    setColor(ANSI_BLACK);
  }

  return(mjd - last_nm_mjd);
}
