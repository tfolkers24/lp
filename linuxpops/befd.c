/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"


int deCommaBuf(buf)
char *buf;
{
  char *cp;

  if(buf)
  {
    for(cp=buf;*cp;cp++)
    {
      if(*cp == ',')
      {
	*cp = ' ';
      }
    }
  }

  return(0);
}


int fillScans(bscan, s, f, buf)
int bscan;
char *s;
double f;
char *buf;
{
  int n = 0;
  int i;
  struct HEADER *p;
  double freq, tsys, tcal;
  char *sbname[] = { "   ", "LSB", "USB", "DSB" };
  char tbuf[256];

  p = &sa[0];
  for(i=0;i<env->numofscans;i++,p++)
  {
    if(!strncmp(p->obsmode, "CONTSEQ ",  8))
    {
      if((int)p->scan < bscan)
      {
        continue;
      }

      freq = restFreq(p) / 1000.0;

      if(f && fabs(f-freq) > 0.001) 
      {
        continue;
      }

      /* Eliminate bogus scans */
      if(isnan(p->stsys) 		|| 
         p->stsys < 0.0 		|| 
         (p->stsys > env->tsyslimit)	|| 

         isnan(p->tcal) 		|| 
         p->tcal < 0.0 			|| 
         p->tcal > env->tcallimit)
      {
        continue;
      }

      if(s)
      {
        if(strncmp(s, p->object, strlen(s)))
        {
          continue;
        }
      }

      if(manual_tcal > 0.0 && fabs(p->tcal-400.0) < 1.0)
      {
        tsys = p->stsys * manual_tcal / p->tcal;
        tcal = manual_tcal;
      }
      else
      {
        tsys = p->stsys;
        tcal = p->tcal;
      }

      sprintf(tbuf, "' %10.10s' '%10.6f' '%10.4f' '%6.2f' '%7.2f' '%4.0f'  '%4.0f' '%s' ",
        p->object, freq, p->utdate, p->ut, p->scan, tsys, tcal, sbname[(int)p->sideband]);

      strcat(buf, tbuf);

      n++;

      if(strlen(buf) > (10*4000))
      {
        return(n);
      }
    }
  }

  return(n);
}


/*  Changes:
 *
 */

int doBefd()
{
  int n, ret, sbs;
  char rst[4096];
  static char cmd[12*4096], bigBuf[10*4096], src[256];

  if(args->ntokens > 1)
  {
    strcpy(src, args->secondArg);
  }
  else
  {
    printH_FlagHelp(args->firstArg);

    return(0);
  }

  saveEnv();

  bzero(bigBuf, sizeof(bigBuf));
  bzero(rst,    sizeof(rst));

  st_report("doBefd(): Entered looking for source: %s", src);

  n = fillScans(0, src, 0.0, bigBuf);

  st_report("doBefd(): Filled First with %d scans", n);

  if(n || strlen(bigBuf))
  {
    sprintf(cmd, "--list --height=1080 --width=600 --column='Source' --column='Freq' --column='Date' --column='UT' --column='Scan' --column='Tsys'  --column='Tsys' --column='SB' --print-column=5 --text='Select First Scan to Reduce' %s", bigBuf);

    if(isSet(PF_V_FLAG))
    {
      st_print("pickSourceFreq(): %s\n", cmd);
    }

    st_report("Standby, Finding all Seq scans");

    ret = doZenity(cmd, rst);

    if(!ret && strlen(rst))
    {
      st_report("Setting Bscan to %s", rst);

      env->bscan = atof(rst);
    }
    else
    {
      restoreEnv();
      return(1);
    }

    bzero(bigBuf, sizeof(bigBuf));
    bzero(rst,    sizeof(rst));

    subArgs(2, "%.2f", env->bscan);

    getScan(1);

    n = fillScans((int)env->bscan, src, restFreq(&h0->head) / 1000.0, bigBuf);

    sprintf(cmd, "--list --height=600 --width=600 --column='Source' --column='Freq' --column='Date' --column='UT' --column='Scan' --column='Tsys'  --column='Tsys' --column='SB' --print-column=5 --text='Select Last Scan to Reduce' %s", bigBuf);

    st_report("Standby, Finding Ending Seq scans");

    ret = doZenity(cmd, rst);

    if(!ret && strlen(rst))
    {
      st_report("Setting Escan to %s", rst);

      env->escan = atoi(rst);
    }
    else
    {
      restoreEnv();
      return(1);
    }

    bzero(rst,    sizeof(rst));
    sprintf(cmd, "--list --height=400 --width=500 --column='Index' --column='Sideband' --column='Notes' --print-column=1 --separator=',' --text='Select IFs to Use' \
	'1' 'LSB'     'Scans xx.01 and xx.03' \
	'2' 'USB'     'Scans xx.02 and xx.04' \
	'3' 'Both'    'Scans xx.01, xx.02, xx.03 and xx.04' \
	'4' '1-2 USB' 'Scans xx.01 and xx.02' \
	'5' '1-2 LSB' 'Scans xx.01 and xx.02' \
	");

    if(isSet(PF_V_FLAG))
    {
      st_print("pickIF(): %s\n", cmd);
    }

    st_report("Select, IF's to use");

    ret = doZenity(cmd, rst);

    if(!ret && strlen(rst))
    {
      st_report("Using Index %s", rst);
    }
    else
    {
      restoreEnv();
      return(1);
    }

    sbs = atoi(rst);

    if(sbs < 1 || sbs > 5)
    {
      st_fail("Unknown Response %s", rst);

      restoreEnv();

      return(1);
    }
    
    if(sbs == 1)
    {
      sprintf(cmd, "--forms --title='Beam Efficiencies' --text='Enter Rejections.' --separator=',' --add-entry='H-LSB Rejection' --add-entry='V-LSB Rejection' ");
    }
    else
    if(sbs == 2)
    {
      sprintf(cmd, "--forms --title='Beam Efficiencies' --text='Enter Rejections.' --separator=',' --add-entry='H-USB Rejection' --add-entry='V-USB Rejection'");
    }
    else
    if(sbs == 3)
    {
      sprintf(cmd, "--forms --title='Beam Efficiencies' --text='Enter Rejections.' --separator=',' --add-entry='H-LSB Rejection' --add-entry='H-USB Rejection' --add-entry='V-LSB Rejection' --add-entry='V-USB Rejection'");
    }
    else
    if(sbs == 4)
    {
      sprintf(cmd, "--forms --title='Beam Efficiencies' --text='Enter Rejections.' --separator=',' --add-entry='H-USB Rejection' --add-entry='V-USB Rejection' ");
    }
    else
    if(sbs == 5)
    {
      sprintf(cmd, "--forms --title='Beam Efficiencies' --text='Enter Rejections.' --separator=',' --add-entry='H-LSB Rejection' --add-entry='V-LSB Rejection'");
    }

    bzero(rst,    sizeof(rst));

    st_report("Enter Rejections");

    ret = doZenity(cmd, rst);

    if(!ret && strlen(rst))
    {
      st_report("Setting Rejections to %s", rst);
    }
    else
    {
      restoreEnv();
      return(1);
    }

    deCommaBuf(rst);

    if(sbs == 1)
    {
      sprintf(cmd, "beffl %d %d %s", (int)env->bscan, (int)env->escan, rst);
    }
    else
    if(sbs == 2)
    {
      sprintf(cmd, "beffu %d %d %s", (int)env->bscan, (int)env->escan, rst);
    }
    else
    if(sbs == 3)
    {
      sprintf(cmd, "beff4r %d %d %s", (int)env->bscan, (int)env->escan, rst);
    }
    else
    if(sbs == 4)
    {
      sprintf(cmd, "beff12 %d %d %s", (int)env->bscan, (int)env->escan, rst);
    }
    else
    if(sbs == 5)
    {
      sprintf(cmd, "beff12 %d %d %s", (int)env->bscan, (int)env->escan, rst);
    }

    restoreEnv(); /* I needed to wait until I had scan numbers out of the env before restoring */

    st_report(" ");
    st_report("Executing: %s", cmd);
    st_report(" ");

    parseCmd(cmd);

    return(0);
  }

  restoreEnv();

  return(0);
}
