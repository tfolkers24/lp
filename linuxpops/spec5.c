/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

struct SPEC_5_RESULTS {
	double scan;
	double az;
	double el;

	double gpeaks[5];
	double gsums[5];

	int    used;
	int    good_fit;
};

struct SPEC_5_RESULTS spec5_results[4];

char *spec5Strings[] = { "North ", 
			 "South ", 
			 "Center", 
			 "East  ", 
			 "West  " 
};


int listPointingResults()
{
  int    i, j, k;
  double azAvg, elAvg;
  struct SPEC_5_RESULTS *p;

  st_report(" ");
  st_report("Spec 5 Results");
  st_report(" ");
  st_report("  Scan          Az      El    GPeak     GSum   Fit ");

  azAvg = elAvg = 0.0;
  k     = j     = 0;

  p = &spec5_results[0];

  for(i=0;i<4;i++,p++)
  {
    st_report("%8.2f   %7.2f   %7.2f  %7.2f  %7.2f  %d", p->scan, p->az, p->el, p->gpeaks[2], p->gsums[2], p->good_fit);

    if(p->az != 0.0)
    {
      azAvg += p->az;
      j++;
    }

    if(p->el != 0.0)
    {
      elAvg += p->el;
      k++;
    }
  }

  if(j)
  {
    azAvg /= (double)j;
  }

  if(k)
  {
    elAvg /= (double)k;
  }

  if(j && k)
  {
    st_report("Average    %7.2f   %7.2f", azAvg, elAvg);
  }

  return(0);
}



int doSpecFive()
{
  int    i, indx, x1mode, x2mode;
  double scan, tscan;
  double bw;
  double x1, x2, x3;                                         /* X coordinates   */
  float  a, b, c;                                            /* curve constants */
  char   tbuf[256];
  struct SPEC_5_RESULTS *p;

  if(isSet(PF_C_FLAG))
  {
    st_report("doSpecFive(): Clearing out Pointing Results Array");

    bzero((char *)&spec5_results[0], sizeof(spec5_results));

    return(0);
  }

  if(isSet(PF_L_FLAG))
  {
    listPointingResults();
    return(0);
  }

  if(args->ntokens < 2)
  {
    return(1);
  }

  scan = atof(args->secondArg);

  if(scan <= 0.0)
  {
    return(1);
  }

  indx = getFeed(scan);

  if(indx > 4) /* Shift IF 5-8 down to 1-4 */
  {
    indx -= 4;
  }

  if(indx < 1 || indx > 4)
  {
    st_fail("Scan # %.2f produced a bogus index of: %f: Must be 1 - 4", scan, indx);
    return(1);
  }

  indx--;

  p = &spec5_results[indx];

  p->scan = scan;

  /* Save axis modes */
  x1mode = env->x1mode;
  x2mode = env->x2mode;

  env->x1mode = XMODE_FREQ;	/* Place FREQ on bottom axis so Gaussian fits are in Freq */
  env->x2mode = XMODE_VEL;

  /* Load all 5 sccans into H1-H5, remove baseline; fit a gaussian; record Peak */

  for(i=0;i<5;i++)
  {
    tscan = scan + ((double)i - 4.0 + 2.0);
    st_report("Fetching %s Scan: %.2f in H%d", spec5Strings[i], tscan, i+1);

    sprintf(tbuf, "h%d", i+1);
    parseCmd(tbuf);

    sprintf(tbuf, "get %.2f", tscan);
    parseCmd(tbuf);

    st_report("Scan %.2f: openpar[8]: %.1f", tscan, h0->head.openpar[8]);

    if(i<4)
    {
      if(fabs(h0->head.openpar[8]-9998.5) > 0.5)
      {
        st_fail("Scan %.2f is not part of a Spec 5", tscan);

        env->x1mode = x1mode;
        env->x2mode = x2mode;

        return(1);
      }
    }
    else
    {
      if(fabs(h0->head.openpar[8]-9999.5) > 0.5)
      {
        st_fail("Scan %.2f is not the last of a Spec 5", tscan);

        env->x1mode = x1mode;
        env->x2mode = x2mode;

        return(1);
      }
    }

    sprintf(tbuf, "show");
    parseCmd(tbuf);
    usleep(100000);

    sprintf(tbuf, "baseline");
    parseCmd(tbuf);
    usleep(100000);

    sprintf(tbuf, "gauss /a");
    parseCmd(tbuf);
    usleep(100000);

    p->gpeaks[i] = env->gaussPeak;
    p->gsums[i]  = env->gaussSum;
  }

  sprintf(tbuf, "h3");		/* Point back to center scan */
  parseCmd(tbuf);

  sprintf(tbuf, "xx");
  parseCmd(tbuf);

  st_print("\n");
  st_print("   Az Offset: %7.2f, El Offset: %7.2f\n", h0->head.uxpnt, h0->head.uypnt);
  st_print("\n");

  st_print("                     %s\n", spec5Strings[0]);
  st_print("                   %7.2f\n", p->gpeaks[0]);
  st_print("                   %7.2f\n", p->gsums[0]);
  st_print(" \n");
  st_print(" \n");
  st_print(" \n");
  st_print(" \n");
  st_print("       %s       %s           %s\n",   spec5Strings[3], spec5Strings[2], spec5Strings[4]);
  st_print("    %7.2f        %7.2f        %7.2f\n", p->gpeaks[3],       p->gpeaks[2],       p->gpeaks[4]);
  st_print("    %7.2f        %7.2f        %7.2f\n", p->gsums[3],        p->gsums[2],        p->gsums[4]);
  st_print(" \n");
  st_print(" \n");
  st_print(" \n");
  st_print("                     %s\n", spec5Strings[1]);
  st_print("                   %7.2f\n", p->gpeaks[1]);
  st_print("                   %7.2f\n", p->gsums[1]);
  st_print(" \n");

  x1 = -h0->head.bfwhm;
  x2 = 0.0;
  x3 =  h0->head.bfwhm;

	/* Steal the continuum gaussian fit routine, as it was written for this */

	/* Determine the Gaussian curve that fits the Az data */
  if( cont_gauss(p->gsums[3], p->gsums[2], p->gsums[4], x1, x2, x3, &a, &b, &c) )
  {
    if( (1.0 / b) > 0.0)
    {
      bw = sqrt( 1.0 / b) * 1.664;
    }

    st_print("AZ: Peak = %7.2f, b = %f, Off: = %5.2f, BW = %7.2f, Corrected: %7.2f\n", a, b, c, bw, h0->head.uxpnt-c);
    p->az = h0->head.uxpnt-c;

    p->good_fit++;
  }
  else
  {
    st_print("AZ: Fit Failed\n");
  }
  
	/* Determine the Gaussian curve that fits the EL data */
  if( cont_gauss(p->gsums[1], p->gsums[2], p->gsums[0], x1, x2, x3, &a, &b, &c) )
  {
    if( (1.0 / b) > 0.0)
    {
      bw = sqrt( 1.0 / b) * 1.664;
    }

    st_print("EL: Peak = %7.2f, b = %f, Off: = %5.2f, BW = %7.2f, Corrected: %7.2f\n", a, b, c, bw, h0->head.uypnt-c);
    p->el = h0->head.uypnt-c;

    p->good_fit++;
  }
  else
  {
    st_print("EL: Fit Failed\n");
  }

  listPointingResults();

  env->x1mode = x1mode;
  env->x2mode = x2mode;

  return(0);
}
