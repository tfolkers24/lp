/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

struct OBJECT_GLOBAL_MEMORY *OGM;

char *knownObjects[] = {
	"Mercury",
	"Venus",
	"Mars",
	"Jupiter",
	"Saturn",
	"Neptune",
	"Uranus",
	"3C84",
	"3C273",
	"3C279",
	"3C345",
	"3C454.3"
};

#define MAX_KNOWN_OBJECTS 12

struct ACCUM_RESULTS_SEND local_ars;


/* Check that the object arrays has been initialized */
int checkObjArray()
{
  if(OGM->objects[0].used == 0)
  {
    initObjectsArray();
  }

  return(0);
}


int initObjectsArray()
{
  int i;

  st_report("initObjectsArray(): Initializing Object Structures");

  for(i=0;i<MAX_OBJECTS;i++)
  {
    bzero((char *)&OGM->objects[i], sizeof(struct OBJECT));
  }

  OGM->nobjIndx = 0;
  OGM->nobsIndx = 0;

  for(i=0;i<MAX_KNOWN_OBJECTS;i++)
  {
    strcpy(OGM->objects[i].magic, "OBJECT_MAGIC");
    OGM->objects[i].used = 1;
    strcpy(OGM->objects[i].source, knownObjects[i]);
  }

  return(0);
}


int listAllObjects()
{
  int i, j, first = 1;

  st_report("listAllObjects(): Listing All Set Objects");

  for(i=0;i<MAX_OBJECTS;i++)
  {
    if(OGM->objects[i].used)
    {
      if(first)
      {
        printf("NOBJ     SOURCE\n\n");
        first = 0;
      }

      printf("%2d: %16.16s: \n", i, OGM->objects[i].source);

      for(j=0;j<MAX_MEASUREMENTS;j++)
      {
	if(OGM->objects[i].measurements[j].used)
        {
	  printf("-----------> Meas: %2d, %2d Scans\n", j, OGM->objects[i].measurements[j].nscans);
        }
      }

      printf("\n");
    }
  }

  return(0);
}


int displayAllObservations(xmode, ymode, lines)
int xmode, ymode, lines;
{
  int i, n;

  if(xgraphic)
  {
    st_report("displayAllObservations(): Displaying All Observations of Object %s", OGM->objects[OGM->nobjIndx].source);

    n = 0;

    for(i=0;i<MAX_MEASUREMENTS;i++)
    {
      if(OGM->objects[OGM->nobjIndx].measurements[i].nscans > 0)
      {
        n++;
      }
    }

    OGM->objects[OGM->nobjIndx].numMeas         = n;
    OGM->objects[OGM->nobjIndx].xmode           = xmode;
    OGM->objects[OGM->nobjIndx].ymode           = ymode;
    OGM->objects[OGM->nobjIndx].lines           = lines;
    OGM->objects[OGM->nobjIndx].plotwin         = env->plotwin;
    OGM->objects[OGM->nobjIndx].paperw          = env->paperw;
    OGM->objects[OGM->nobjIndx].paperh          = env->paperh;
    OGM->objects[OGM->nobjIndx].xmin            = env->xplotmin;
    OGM->objects[OGM->nobjIndx].xmax            = env->xplotmax;

    strcpy(OGM->objects[OGM->nobjIndx].magic, "OBJECT_MAGIC");

    sock_write(xgraphic, (char *)&OGM->objects[OGM->nobjIndx], sizeof(struct OBJECT));

    last_plot = LAST_PLOT_CONT_OBJECT;
  }
  else
  {
    st_fail("displayAllObservations(): No graphics package to send struct to");
  }

  return(0);
}


int clearObsEntry(o)
char *o;
{
  int i;

  for(i=0;i<MAX_OBJECTS;i++)
  {
    if(!strncasecmp(OGM->objects[i].source, o, strlen(o)))
    {
      if(i < MAX_KNOWN_OBJECTS)
      {
        st_report("clearObsEntry(): Clearing Object %s", OGM->objects[i].source);

        bzero((char *)&OGM->objects[i], sizeof(struct OBJECT));

        strcpy(OGM->objects[i].magic, "OBJECT_MAGIC");
        OGM->objects[i].used = 1;
        strcpy(OGM->objects[i].source, knownObjects[i]);
      }
      else /* Clear out the entire object */
      {
        bzero((char *)&OGM->objects[i], sizeof(struct OBJECT));
        st_report("clearObsEntry(): Clearing Complete Object %s", OGM->objects[i].source);
      }

      return(0);
    }
  }

  st_fail("clearObsEntry(): Object %s, not found in Table");

  return(1);
}


int findObject(ars)
struct ACCUM_RESULTS_SEND *ars;
{
  int i;
  char obj[64];

  strncpy(obj, ars->head[env->feed].object, 16);
  obj[16] = '\0';

  st_report("findObject(): Searching for Object: %s", obj);

  strcmprs(obj);

  for(i=0;i<MAX_OBJECTS;i++)
  {
    if(strcasestr(OGM->objects[i].source, obj) || strcasestr(obj, OGM->objects[i].source))
    {
      st_report("findObject(): Using Object Index %d", i);

      return(i);
    }
  }

  /* Nothing found, so return the next available */
  for(i=0;i<MAX_OBJECTS;i++)
  {
    if(!OGM->objects[i].used)
    {
      st_report("findObject(): Creating New Object Index %d", i);
      OGM->objects[i].used = 1;

      return(i);
    }
  }

  return(0);
}


int findNextObs(ars, nobj)
struct ACCUM_RESULTS_SEND *ars;
int nobj;
{
  int i=0;
  struct MEASUREMENT *m;

  m = &OGM->objects[nobj].measurements[0];

  for(i=0;i<MAX_MEASUREMENTS;i++,m++)
  {
    /* If I find an unused one first; return that on */
    if(!m->used)
    {
      st_report("findNextObs(): Creating New Observation Index %d", i);
      return(i);
    }
    else /* OK, this on isn't used; check if it's the same scans */
    {
      if(fabs(m->bscan-ars->bscan) < 1.0 && fabs(m->escan-ars->escan) < 1.0)
      {
        st_report("findNextObs(): Using Previous Observation Index %d", i);
        return(i);
      }
    }
  }

  return(0);
}


int  saveObjectAccum(ars)
struct ACCUM_RESULTS_SEND *ars;
{
  st_report("saveObjectAccum(): Result saved to Local Copy");

  bcopy((char *)ars, (char *)&local_ars, sizeof(local_ars));

  return(0);
}


int  saveLocalObjectAccum(ars)
struct ACCUM_RESULTS_SEND *ars;
{
  int i, nobj, nobs;
  struct RESULT *r;
  struct OBJECT *o;
  struct MEASUREMENT *m;

  nobj = findObject (ars);
  nobs = findNextObs(ars, nobj);

  OGM->nobjIndx = nobj;
  OGM->nobsIndx = nobs;

  o = &OGM->objects[nobj];
  m = &OGM->objects[nobj].measurements[nobs];

  m->bscan  = ars->bscan;
  m->escan  = ars->escan;
  m->nscans = ars->scanct;

  st_report("saveLocalObjectAccum(): Saving Expected Values for Feed: %d, %.3f, %.3f", env->feed, env->expectedtmb, env->expectedflux);

  if(env->feed == 4)
  {
    for(i=0;i<MAX_IFS;i++)
    {
      m->expectedTmb[ i] = env->expectedtmb;
      m->expectedFlux[i] = env->expectedflux;
    }
  }
  else
  if(env->feed == 0 || env->feed == 2)
  {
    m->expectedTmb[ 0] = env->expectedtmb;
    m->expectedFlux[0] = env->expectedflux;

    m->expectedTmb[ 2] = env->expectedtmb;
    m->expectedFlux[2] = env->expectedflux;
  }
  else
  if(env->feed == 1 || env->feed == 3)
  {
    m->expectedTmb[ 1] = env->expectedtmb;
    m->expectedFlux[1] = env->expectedflux;

    m->expectedTmb[ 3] = env->expectedtmb;
    m->expectedFlux[3] = env->expectedflux;
  }

  m->averageEl               = env->avgEl;

  r = &OGM->objects[nobj].measurements[nobs].results[0];
  for(i=0;i<MAX_IFS;i++,r++)
  {
    if(ars->avgTaStar[i] > 0.0)
    {
      if(nobj >= MAX_KNOWN_OBJECTS)
      {
        strncpy(o->source, ars->head[i].object, 16);
        o->source[16] = '\0';
        strcmprs(o->source);
      }

      o->used              = 1;

      r->measuredTmb       = ars->avgTaStar[i];
      r->measuredTmbSigma  = ars->taSigma[i];

      r->measuredTr        = ars->avgTr[i];
      r->measuredTrSigma   = ars->trSigma[i];

      r->measuredFlux      = ars->avgJanskys[i];
      r->measuredFluxSigma = ars->jkSigma[i];

      if(m->expectedTmb[i] > 0.0)
      {
        r->beam_eff        = ars->avgTr[i] / m->expectedTmb[i];
      }

      if(m->expectedFlux[i] > 0.0)
      {
        r->app_eff         = ars->avgJanskys[i] / m->expectedFlux[i];
      }

      m->ut     = ars->head[i].ut;
      m->utdate = ars->head[i].utdate;
      m->tsys   = ars->head[i].stsys;
      m->tcal   = ars->head[i].tcal;
      m->burn   = ars->head[i].burn_time;

      m->used   = 1;
    }
  }

  return(0);
}


/*
  extern FILE *errPrint_fp;

 */

int showObjectStatus()
{
  int i, j, k, send2printer=0;
  char fname[256], sysBuf[512];
  struct OBJECT *o;
  struct MEASUREMENT *m;
  double rejs[4], beameff[4], besigma[4];
  FILE *fp, *saveFp;

  /* The PRINT flag indicates if the user wants
     to print out the results too;

     Hijack the library printing function to log the
     output for the duration of this function */
  if(isSet(PF_P_FLAG))
  {
    st_report("showObjectStatus(): Now with Printing");

    sprintf(fname, "/tmp/%s/object_print", session);

    if((fp = fopen(fname, "w") ) <= (FILE *)0)
    {
      st_fail("Unable to open %s", fname);
      send2printer = 0;
    }
    else
    {
      saveFp = errPrint_fp;
      errPrint_fp = fp;
      send2printer = 1;
    }
  }

  st_report("showObjectStatus(): Current Object Entry Set to: %d", OGM->nobjIndx);
  st_report("showObjectStatus(): Current Observations Entry Set to: %d", OGM->nobsIndx);

  o = &OGM->objects[OGM->nobjIndx];
  m = &OGM->objects[OGM->nobjIndx].measurements[0];

  if(env->nbeff == 0) /* check for zero */
  {
    env->nbeff = 4;
  }

  for(i=0;i<env->nbeff;i++)
  {
    rejs[i] = 1.0 / (1.0 + pow(10.0, -1.0 * env->rejections[i] / 10.0));
  }

  if(o->used)
  {
    for(j=0;j<MAX_MEASUREMENTS;j++,m++)
    {
      if(m->used)
      {
        for(k=0;k<env->nbeff;k++)
        {
          beameff[k] = m->results[k].beam_eff * 100.0 * rejs[k];

          besigma[k] = m->results[k].measuredTrSigma * beameff[k] / m->results[k].measuredTr;

          if(gsl_isnan(besigma[k]))
          {
            besigma[k] = 0.0;
          }
        }

        st_report("Object %s", o->source);

        st_report("Set[%2d]: (%2d) Scans: (%.2f-%.2f); Date: %.4f, %.2f UT; El = %.1f", 
				j, m->nscans, m->bscan, m->escan, m->utdate, m->ut, m->averageEl);

        if(env->nbeff == 4)
        {
	  st_report("                   H-LSB      H-USB      V-LSB      V-USB");
	}
	else
	{
	  if(h0->head.sideband == 2.0)
	  {
	    st_report("                   H-USB      V-USB");
	  }
	  else
	  {
	    st_report("                   H-USB      V-USB");
	  }
	}

        if(env->nbeff == 4)
        {
          st_report("     Rejections: %7.3f    %7.3f    %7.3f    %7.3f", 
		env->rejections[0],
		env->rejections[1],
		env->rejections[2],
		env->rejections[3]);
	}
	else
	{
          st_report("     Rejections: %7.3f    %7.3f", env->rejections[0], env->rejections[1]);
	}

        if(env->nbeff == 4)
        {
          st_report("     Rejs Coeff: %7.3f    %7.3f    %7.3f    %7.3f", 
		rejs[0]*100.0, 
		rejs[1]*100.0, 
		rejs[2]*100.0, 
		rejs[3]*100.0);
	}
	else
	{
          st_report("     Rejs Coeff: %7.3f    %7.3f", rejs[0]*100.0, rejs[1]*100.0);
	}

        if(env->nbeff == 4)
        {
          st_report("            Tmb: %7.3f    %7.3f    %7.3f    %7.3f", 
		m->results[0].measuredTr,
		m->results[1].measuredTr,
		m->results[2].measuredTr,
		m->results[3].measuredTr);
	}
	else
	{
          st_report("            Tmb: %7.3f    %7.3f", m->results[0].measuredTr, m->results[1].measuredTr);
	}

        if(env->nbeff == 4)
        {
          st_report("   Expected Tmb: %7.3f    %7.3f    %7.3f    %7.3f", 
		m->expectedTmb[0],
		m->expectedTmb[1],
		m->expectedTmb[2],
		m->expectedTmb[3]);
	}
	else
	{
          st_report("   Expected Tmb: %7.3f    %7.3f", m->expectedTmb[0], m->expectedTmb[1]);
	}

        if(env->nbeff == 4)
        {
          st_report("       TmbSigma: %7.3f    %7.3f    %7.3f    %7.3f", 
		m->results[0].measuredTrSigma,
		m->results[1].measuredTrSigma,
		m->results[2].measuredTrSigma,
		m->results[3].measuredTrSigma);
	}
	else
	{
          st_report("       TmbSigma: %7.3f    %7.3f", m->results[0].measuredTrSigma, m->results[1].measuredTrSigma);
	}

        if(env->nbeff == 4)
        {
          st_report("       Beam Eff: %7.3f    %7.3f    %7.3f    %7.3f", 
		beameff[0],
		beameff[1],
		beameff[2],
		beameff[3]);
	}
	else
	{
          st_report("       Beam Eff: %7.3f    %7.3f", beameff[0], beameff[1]);
	}

        if(env->nbeff == 4)
        {
          st_report(" Beam Eff Sigma: %7.3f    %7.3f    %7.3f    %7.3f", 
		besigma[0],
		besigma[1],
		besigma[2],
		besigma[3]);
	}
	else
	{
          st_report(" Beam Eff Sigma: %7.3f    %7.3f", besigma[0], besigma[1]);
	}

        if(env->nbeff == 4)
        {
	  st_report("------------------Ignore Below Here----------------------");
	}
	else
	{
	  st_report("-------Ignore Below Here-----------");
	}

        if(env->nbeff == 4)
        {
          st_report("           Flux: %7.2f    %7.2f    %7.2f    %7.2f", 
		m->results[0].measuredFlux,
		m->results[1].measuredFlux,
		m->results[2].measuredFlux,
		m->results[3].measuredFlux);
	}
	else
	{
          st_report("           Flux: %7.2f    %7.2f", m->results[0].measuredFlux, m->results[1].measuredFlux);
	}

        if(env->nbeff == 4)
        {
          st_report("  Expected Flux: %7.2f    %7.2f    %7.2f    %7.2f", 
		m->expectedFlux[0],
		m->expectedFlux[1],
		m->expectedFlux[2],
		m->expectedFlux[3]);
	}
	else
	{
          st_report("  Expected Flux: %7.2f    %7.2f", m->expectedFlux[0], m->expectedFlux[1]);
	}

        if(env->nbeff == 4)
        {
          st_report("      FluxSigma: %7.2f    %7.2f    %7.2f    %7.2f", 
		m->results[0].measuredFluxSigma,
		m->results[1].measuredFluxSigma,
		m->results[2].measuredFluxSigma,
		m->results[3].measuredFluxSigma);
	}
	else
	{
          st_report("      FluxSigma: %7.2f    %7.2f", m->results[0].measuredFluxSigma, m->results[1].measuredFluxSigma);
	}

        if(env->nbeff == 4)
        {
          st_report("       Beam Eff: %7.2f    %7.2f    %7.2f    %7.2f", 
		m->results[0].app_eff*100.0 * rejs[0],
		m->results[1].app_eff*100.0 * rejs[1],
		m->results[2].app_eff*100.0 * rejs[2],
		m->results[3].app_eff*100.0 * rejs[3]);
	}
	else
	{
          st_report("       Beam Eff: %7.2f    %7.2f", m->results[0].app_eff*100.0 * rejs[0], m->results[1].app_eff*100.0 * rejs[1]);
	}

        st_report(" ");
      }
    }
  }

  if(isSet(PF_P_FLAG) && send2printer)
  {
    fclose(fp);
    errPrint_fp = saveFp;

    sprintf(sysBuf, "lpr -o landscape %s", fname);

    system(sysBuf);

    st_report("# Job sent to printer");
  }

  return(0);
}


int setExpected()
{
  double t, f;

  if(args->ntokens < 2)
  {
    st_report("setExpected(): Usage: expected tmb [flux]");
    st_report("setExpected(): Current Expected Tmb %.3f", env->expectedtmb);

    return(1);
  }
  
  if(strlen(args->secondArg))
  {
    t = atof(args->secondArg);

    if(t > 0.0 && t < 1000.0)
    {
      env->expectedtmb = t;
      st_report("setExpected(): Setting Expected Tmb to %.3f", env->expectedtmb);
    }
    else
    {
      st_fail("setExpected(): Expected Tmb %f, Out Of Range [0.0 - 1000.0]", t);
    }
  }

  if(strlen(args->thirdArg))
  {
    f = atof(args->thirdArg);

    if(f >= 0.0 && f < 100000.0)
    {
      env->expectedflux = f;
      st_report("setExpected(): Setting Expected Flux to %.3f", env->expectedflux);
    }
    else
    {
      st_fail("setExpected(): Expected Flux %f, Out Of Range [0.0 - 100000.0]", f);
    }
  }

  return(0);
}


int objectDisplay()
{
  int xmode, ymode, lines;

    /* set to defaults */

  lines = OGM->objects[OGM->nobjIndx].lines;
  xmode = OGM->objects[OGM->nobjIndx].xmode;
  ymode = OGM->objects[OGM->nobjIndx].ymode;

  if(strlen(args->secondArg) > 0)
  {
    if(!strncmp(args->secondArg, "num", strlen(args->secondArg)))
    {
      xmode = MODE_NUM;
    }
    else
    if(!strncmp(args->secondArg, "el", strlen(args->secondArg)))
    {
      xmode = MODE_EL;
    }
    else
    {
      xmode = MODE_NUM;
    }
  }

  if(strlen(args->thirdArg) > 0)
  {
    if(!strncmp(args->thirdArg, "flux", strlen(args->thirdArg)))
    {
      ymode = MODE_JANSKYS;
    }
    else
    if(!strncmp(args->thirdArg, "janskys", strlen(args->thirdArg)))
    {
      ymode = MODE_JANSKYS;
    }
    else
    if(!strncmp(args->thirdArg, "tmb", strlen(args->thirdArg)))
    {
      ymode = MODE_TMB;
    }
    else
    if(!strncmp(args->thirdArg, "eff", strlen(args->thirdArg)))
    {
      ymode = MODE_EFF;
    }
    else
    if(!strncmp(args->thirdArg, "app", strlen(args->thirdArg)))
    {
      ymode = MODE_APP;
    }
    else
    {
      ymode = MODE_JANSKYS;
    }
  }

  if(strlen(args->fourthArg) > 0)
  {
    if(!strncmp(args->fourthArg, "lines", strlen(args->fourthArg)))
    {
      lines = MODE_LINES;
    }
    else
    if(!strncmp(args->fourthArg, "nolines", strlen(args->fourthArg)))
    {
      lines = MODE_NOLINES;
    }
    else
    {
      lines = MODE_LINES;
    }
  }

  displayAllObservations(xmode, ymode, lines);

  return(0);
}


int getLabelPos(msg)
char *msg;
{
  if(xgraphic)
  {
    if(strlen(msg) > 3)
    {
      st_report(msg);
    }

    sock_send(xgraphic, "ccur");

    if(!getResponse(WAIT_FOR_MOUSE))
    {
      st_report("getLabelPos(): X = %.2f Y = %.2f", last_mouse_x, last_mouse_y);
    }
    else
    {
      return(1);
    }
  }

  return(0);
}


/* Entry point for the Object command set */
/* Used for handling multiple observation sets on objects */
int doObject()
{
  int  n;
  char obj[80];

  if(isSet(PF_I_FLAG))
  {
    initObjectsArray();
    return(0);
  }

  if(isSet(PF_L_FLAG))
  {
    listAllObjects();
    return(0);
  }

  if(isSet(PF_M_FLAG))
  {
    objectDisplay();			/* Force a repaint first */

    getLabelPos("Click Mouse to Place Labels");

    OGM->objects[OGM->nobjIndx].xlabel = last_mouse_x;
    OGM->objects[OGM->nobjIndx].ylabel = last_mouse_y;

    objectDisplay();

    return(0);
  }

  if(isSet(PF_N_FLAG))
  {
    if(strlen(args->secondArg) > 0)
    {
      n = atoi(args->secondArg);

      if(n >-1 && n < MAX_OBJECTS)
      {
        OGM->nobjIndx = n;
        st_report("object(): Setting Working Object Entry to %d", OGM->nobjIndx);
      }
      else
      {
        st_fail("object(): Working Object Entry: %d, Out Of Range [0 - %d]", n, MAX_OBJECTS);
      }
    }
    else
    {
      st_fail("object(): Working Object Entry arg missing");

      return(1);
    }

    return(0);
  }

  if(isSet(PF_D_FLAG))
  {
    objectDisplay();

    return(0);
  }

  if(isSet(PF_C_FLAG))
  {
    if(strlen(args->secondArg))
    {
      strcpy(obj, args->secondArg);
      clearObsEntry(obj);

      env->xplotmin = 0.0;
      env->xplotmax = 0.0;
      env->yplotmin = 0.0;
      env->yplotmax = 0.0;
    }
    else
    {
      st_fail("object(): Missing arg for 'object /c");
      return(1);
    }

    return(0);
  }
 
  if(isSet(PF_R_FLAG))
  {
    saveLocalObjectAccum(&local_ars);
    return(0);
  }
 
  if(isSet(PF_S_FLAG) == 1)
  {
    showObjectStatus();

    return(0);
  }

  if(isSet(PF_S_FLAG) == 2)
  {
    do_objectSort();
  }

  objectDisplay();

  return(0);
}


int do_objectGraceFit()
{
  int    i, j, k, n, foundone = 0;
  float  *f, y;
  double xd[MAX_CHANS], yd[MAX_CHANS], coeff[32], avg, n1;
  struct OBJECT *o;

  st_report("do_objectGraceFit(): Entered");

  bzero((char *)xd,    sizeof(xd));
  bzero((char *)yd,    sizeof(yd));
  bzero((char *)coeff, sizeof(coeff));

  if(env->nfit == 0)
  {
    st_report("do_objectGraceFit(): Using nfit = %d", env->nfit);

    o = &OGM->objects[OGM->nobjIndx];
    n = o->numMeas;

    for(j=0;j<MAX_IFS;j++)
    {
      if(o->measurements[0].results[j].measuredTmb)
      {
        avg  = 0.0;

        for(i=0;i<n;i++)
        {
          switch(o->ymode)
          {
	    case MODE_EFF:     y = o->measurements[i].results[j].beam_eff;     break;
	    case MODE_APP:     y = o->measurements[i].results[j].app_eff;      break;
	    case MODE_TMB:     y = o->measurements[i].results[j].measuredTmb;  break;
	    case MODE_JANSKYS: y = o->measurements[i].results[j].measuredFlux; break;
          }

	  avg += y;

          st_print("%d %2d %.2f %f\n", j, i, o->measurements[i].averageEl, y);
        }

        avg /= (double)n;
	st_report("do_objectGraceFit(): AVERAGE: %f", avg);

        f = &o->fitcurves[j][0];
        for(i=0;i<n;i++,f++)
        {
          *f = avg;
        }

        foundone++;
      }
    }

    if(foundone)
    {
      o->bplot  = 1;
      o->ncoeff = env->nfit;

      sock_write(xgraphic, (char *)&OGM->objects[OGM->nobjIndx], sizeof(struct OBJECT));
    }
  }
  else
  if(env->nfit > 0 && env->nfit < MAX_NFIT)
  {
    st_report("do_objectGraceFit(): Using nfit = %d", env->nfit);

    o = &OGM->objects[OGM->nobjIndx];
    n = o->numMeas;

    for(j=0;j<MAX_IFS;j++)
    {
      if(o->measurements[0].results[j].measuredTmb)
      {
        n1 = o->normalize[j];

        for(i=0;i<n;i++)
        {
          xd[i] = o->measurements[i].averageEl;

          switch(o->ymode)
          {
	    case MODE_EFF:     yd[i] = o->measurements[i].results[j].beam_eff;     break;
	    case MODE_APP:     yd[i] = o->measurements[i].results[j].app_eff;      break;
	    case MODE_TMB:     yd[i] = o->measurements[i].results[j].measuredTmb;  break;
	    case MODE_JANSKYS: yd[i] = o->measurements[i].results[j].measuredFlux; break;
          }

	  if(n1 != 0.0)
	  {
	    yd[i] *= n1;

	    o->fit_scaled = 1;
	  }
	  else
	  {
	    o->fit_scaled = 0;
	  }
        }

        fitcurve(xd, yd, n, env->nfit, coeff);

	st_print("Linuxpops(C) >>  # %s: ", mg_sideband[j]);

        for(k=0;k<env->nfit+1;k++)
        {
          st_print("a%d = %.4e ", k, coeff[k]);

          o->coeff[j][k] = coeff[k];
        }

        st_print("\n");

        f = &o->fitcurves[j][0];
        for(i=0;i<n;i++,f++)
        {
          *f = (float)leasev(coeff, env->nfit, o->measurements[i].averageEl);
        }

        foundone++;
      }
    }

    if(foundone)
    {
      o->bplot  = 1;
      o->ncoeff = env->nfit;

      sock_write(xgraphic, (char *)&OGM->objects[OGM->nobjIndx], sizeof(struct OBJECT));

      if(isSet(PF_S_FLAG))              /* Save coeff to elgain terms?? */
      {
        st_report("Saving %d Elevation Gain Terms", env->nfit+1);

        env->elgain_term1 = 0.0;                /* Zero old ones out first */
        env->elgain_term2 = 0.0;
        env->elgain_term2 = 0.0;
        env->elgain_term4 = 0.0;
        env->elgain_term5 = 0.0;

        for(k=0;k<env->nfit+1;k++)
        {
          switch(k)
          {
            case 0: env->elgain_term1 = (o->coeff[0][k] + o->coeff[1][k] + o->coeff[2][k] + o->coeff[3][k]) / (float)foundone; break;
            case 1: env->elgain_term2 = (o->coeff[0][k] + o->coeff[1][k] + o->coeff[2][k] + o->coeff[3][k]) / (float)foundone; break;
            case 2: env->elgain_term3 = (o->coeff[0][k] + o->coeff[1][k] + o->coeff[2][k] + o->coeff[3][k]) / (float)foundone; break;
            case 3: env->elgain_term4 = (o->coeff[0][k] + o->coeff[1][k] + o->coeff[2][k] + o->coeff[3][k]) / (float)foundone; break;
            case 4: env->elgain_term5 = (o->coeff[0][k] + o->coeff[1][k] + o->coeff[2][k] + o->coeff[3][k]) / (float)foundone; break;
          }
        }
      }

    }
  }
  else
  {
    st_fail("do_objectGraceFit(): Error: nfit out of range: %d, [0 - %d]", env->nfit, MAX_NFIT-1);
    return(1);
  }


  return(0);
}



int objectSort(p1, p2)
struct MEASUREMENT *p1, *p2;
{
  if(p1->averageEl > p2->averageEl)
  {
    return(1);
  }
  else
  if(p1->averageEl < p2->averageEl)
  {
    return(-1);
  }

  return(0);
}


int do_objectSort()
{
  st_report("do_objectSort(): Sorting OBJECT structure");

#ifndef __DARWIN__
  qsort(&OGM->objects[OGM->nobjIndx].measurements[0], OGM->objects[OGM->nobjIndx].numMeas, sizeof(struct MEASUREMENT), (__compar_fn_t)objectSort);
#else
  qsort(&OGM->objects[OGM->nobjIndx].measurements[0], OGM->objects[OGM->nobjIndx].numMeas, sizeof(struct MEASUREMENT), objectSort);
#endif

  return(0);
}
