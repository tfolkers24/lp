/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */

#include "extern.h"

struct FORMULAS formulas;


int executeFormula()
{
  int    n;
  char   cmdBuf[256];
  struct FORMULA *f;

  n = getNumericFlag();

  if(n<0)
  {
    st_fail("Missing numeric flag for formula /x");

    printH_FlagHelp(args->firstArg);

    return(1);
  }

  f = &formulas.formulas[n];

  if(f->used)
  {
    if(isSet(PF_V_FLAG))
    {
      st_report("Executing formula %d: %s", n, f->formula);
    }
    else
    {
      st_report("Executing formula %d", n);
    }


    sprintf(cmdBuf, "eval %s", f->formula);
    parseCmd(cmdBuf);
  }
  else
  {
    st_fail("Formula: %d, not defined", n);
  }

  return(0);
}


int clearFormula()
{
  int n;
  struct FORMULA *f;

  n = getNumericFlag();

  if(n<0)
  {
    st_fail("Missing numeric flag for formula /c");

    printH_FlagHelp(args->firstArg);

    return(1);
  }

  st_report("Clearing formula %d", n);

  f = &formulas.formulas[n];

  strcpy(f->formula, "");
  f->used = 0;

  return(0);
}


int defineFormula()
{
  int  i, n;
  char tbuf[512], scratch[512];
  struct FORMULA *f;

  n = getNumericFlag();

  if(n<0)
  {
    st_fail("Missing numeric flag for formula /d");

    printH_FlagHelp(args->firstArg);

    return(1);
  }

  f = &formulas.formulas[n];

  tbuf[0] = '\0';
  for(i=1;i<args->ntokens;i++)
  {
    sprintf(scratch, "%s ", args->tok[i]);
    strcat(tbuf, scratch);
  }

  strcmprs(tbuf);

  if(strlen(tbuf) < MAX_FORMULA_LEN)
  {
    strcpy(f->formula, tbuf);
    f->used = 1;
    st_report("Defined Formula: %d as %s", n, f->formula);
  }
  else
  {
    st_fail("Formula to long for buffers");
    return(1);
  }

  return(0);
}


int readFormulas()
{
  FILE  *fp;
  char  *string, line[256];

  if(strlen(args->secondArg))
  {
    if((fp = fopen(args->secondArg, "r")) == NULL)
    {
      if(isSet(PF_I_FLAG)) /* Ignore any errors */
      {
        st_report("No %s formula to load");
	return(0);
      }

      st_fail("Unable to open %s", args->secondArg);
      return(1);
    }

    st_report("Reading Formulas from %s", args->secondArg);
  }
  else
  {
    st_fail("Missing filename args for 'formula /r'");
    return(1);
  }

  while((string = fgets(line, sizeof(line), fp)) != NULL)
  {
    if(line[0] == '#')
    {
      continue;
    }

    parseCmd(line);
  }

  fclose(fp);


  return(0);
}

int writeFormulas()
{
  int    i, j = 0;
  FILE  *fp;
  struct FORMULA *f;

  if(strlen(args->secondArg))
  {
    if((fp = fopen(args->secondArg, "w")) == NULL)
    {
      st_fail("Unable to open %s", args->secondArg);
      return(1);
    }

    st_report("Saving Formulas to %s", args->secondArg);
  }
  else
  {
    st_fail("Missing filename args for 'formula /w'");
    return(1);
  }

  f = &formulas.formulas[0];

  for(i=0;i<MAX_FORMULAS;i++,f++)
  {
    if(f->used)
    {
      fprintf(fp, "formula /q /d /%d %s\n", i, f->formula);
      j++;
    }
  }

  fclose(fp);

  st_report("Wrote %d formulas to %s", j, args->secondArg);

  return(0);
}


int listFormulas()
{
  int i, first = 1;
  struct FORMULA *f;

  f = &formulas.formulas[0];

  for(i=0;i<MAX_FORMULAS;i++,f++)
  {
    if(f->used)
    {

      if(first)
      {
        st_print("\n");
        st_print(" #      Formula\n");
        st_print("\n");

        first = 0;
      }

      st_print("%2d:   %s\n", i, f->formula);
    }
  }

  if(first)
  {
    st_report("No Formulas defined");
  }
  else
  {
    st_print("\n");
  }

  return(0);
}


int eraseFormulas()
{
  st_report("Erasing all formulas");

  bzero((char *)&formulas, sizeof(formulas));

  return(0);
}



int doFormula()
{
  if(isSet(PF_E_FLAG))
  {
    return(eraseFormulas());
  }

  if(isSet(PF_C_FLAG))
  {
    return(clearFormula());
  }

  if(isSet(PF_D_FLAG))
  {
    return(defineFormula());
  }

  if(isSet(PF_R_FLAG))
  {
    return(readFormulas());
  }

  if(isSet(PF_W_FLAG))
  {
    return(writeFormulas());
  }

  if(isSet(PF_L_FLAG))
  {
    return(listFormulas());
  }

  if(isSet(PF_X_FLAG))
  {
    return(executeFormula());
  }

  return(0);
}
