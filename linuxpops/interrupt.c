/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

int control_c_hit           = 0;

extern tcflag_t old_lflag;
extern cc_t     old_vtime;
extern struct termios term;
extern int history_already_written;
extern int lp_ppid;
extern int logging;



/**
  Exit func whether from user command or SIGNAL. \n
  Tell 'xgraphic' and 'helper' tasks to exit. \n
  reset terminal back to normal. \n
  \returns void
 */
void exit_proc()
{
  char sysbuf[256];

  if(!doNotRemoveLockFile)
  {
    removeLockFile();
  }
  else
  {
    st_report("Not Removing Ignored Lockfile: %s", lockFileName);
  }

  if(xgraphic)
  {
    st_report("Killing Graphics Program");
    sock_send(xgraphic, "exit");

    usleep(250000);
  }

  if(helper)
  {
    st_report("Killing Helper Program");
    sock_send(helper, "exit");

    usleep(250000);
  }

  if(!no_terminal)
  {
  /* reset the old terminal setting before exiting */
    term.c_lflag     = old_lflag;
    term.c_cc[VTIME] = old_vtime;

    if( tcsetattr(fileno(stdin), TCSANOW, &term) < 0)
    {
      perror("tcsetattr");
    }
  }

  printTimeStamp();

  /* Because this will kill this process, set a flag so I know to ignore */
  history_already_written = 1;

  sprintf(sysbuf, "kill `ps ax | grep -- '-tn %d' | grep -v grep | awk '{print $1}'` 1>> /dev/null 2>&1", lp_ppid);
  system(sysbuf);

  st_report("Killing any Remaining Processes belonging to PPID: %d", lp_ppid);
  sprintf(sysbuf, "pkill -g %d", lp_ppid);
  system(sysbuf);

  if(logging)
  {
    st_report("Closing logging file: %s", logfilename);
  }
}




/* Any loop in lp that might take a long time needs to 
 * call this function first, and the call the isInterrupt()
 * on a regular bassis to see if it time to break out of a loop.
 */
int clearInterrupt()
{
  control_c_hit = 0;

  return(0);
}


int isInterrupt()
{
  if(control_c_hit)
  {
    st_print("Interrupt Detected\n");
    return(1);
  }

  return(0);
}

/**
  Signal catcher for HUP, KILL, INIT & TERM
  \returns 1
 */
void sig_handler(int sig)
{
  time_t        now;
  static time_t old_time = 0;
  char cmdBuf[256];

  if(sig == SIGTERM) /* External kill */
  {
    if(!history_already_written)
    {
      st_report("Saving History");
      rwHistory(WRITE_HISTORY);
      st_report("Saving Formulas");

      sprintf(cmdBuf, "formula /w .lp.history");
      parseCmd(cmdBuf);

      history_already_written = 1;
    }

    if(doingOTF)
    {
      st_report("Sending quit to dataserv");
      sock_send(dataserv, "q");
      doingOTF = 0;
    }

    exit(1);
  }

  now = time(NULL);

  if(now-old_time > 2)
  {
    st_print("Caught Signal %d\n", sig);
    st_print("Hit <Control-C> within 2 Seconds to Exit\n");

    old_time = now;

    control_c_hit = 1;

    return;
  }

  st_print("Caught 2nd signal: %d\n", sig);

  if(!history_already_written)
  {
    st_report("Saving History");
    rwHistory(WRITE_HISTORY);

    sprintf(cmdBuf, "formula /w .lp.history");
    parseCmd(cmdBuf);

    history_already_written = 1;
  }

  if(doingOTF)
  {
    st_report("Sending quit to dataserv");
    sock_send(dataserv, "q");
    doingOTF = 0;
  }

  exit(1);
}



int initSignals()
{
  struct sigaction new_action, old_action;

 /* SIGNALS */

  /* Register exit_proc to remove lock-file */
  atexit(exit_proc);

  /* Set up the structure to specify the new action. */
  new_action.sa_handler = sig_handler;
  sigemptyset (&new_action.sa_mask);
  new_action.sa_flags = 0;

  sigaction (SIGINT, NULL, &old_action);
  if (old_action.sa_handler != SIG_IGN)
  {
    sigaction (SIGINT, &new_action, NULL);
  }
  else
  {
    st_fail("main(): Unable to setup signal(INT)");
  }

  sigaction (SIGHUP, NULL, &old_action);
  if (old_action.sa_handler != SIG_IGN)
  {
    sigaction (SIGHUP, &new_action, NULL);
  }
  else
  {
    st_fail("main(): Unable to setup signal(HUP)");
  }

  sigaction (SIGTERM, NULL, &old_action);
  if (old_action.sa_handler != SIG_IGN)
  {
    sigaction (SIGTERM, &new_action, NULL);
  }
  else
  {
    st_fail("main(): Unable to setup signal(TERM)");
  }

  sigaction (SIGKILL, NULL, &old_action);
  if (old_action.sa_handler != SIG_IGN)
  {
    sigaction (SIGKILL, &new_action, NULL);
  }
  else
  {
    st_fail("main(): Unable to setup signal(KILL)");
  }

  sigaction (SIGQUIT, NULL, &old_action);
  if (old_action.sa_handler != SIG_IGN)
  {
    sigaction (SIGQUIT, &new_action, NULL);
  }
  else
  {
    st_fail("main(): Unable to setup signal(QUIT)");
  }

  return(0);
}
