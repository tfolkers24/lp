/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"


/* Here is the deal; Every time we send a Image to xgraphics
   we will push that image into the stack;  Thus everytime
   you make a change to an image and plot it, it gets saved;

   Then you can undo a change which is just the act of popping
   the top image off the stack and copying it to the working
   holder h0.  Correction, the top image is the current image
   so poping that one doesn't gain you anything;  pop the top
   one into the redo array, move everybody up 1 and pops that
   one.

   Redo takes the current holder and pushes it on the stack
   and pops one off the redo statck.

   At least in theory...

   Oh, and I need to push and pop the environment too...

 */

#define UNDO_STACK_SIZE 32

struct LP_IMAGE_INFO undo_array[UNDO_STACK_SIZE];
struct LP_IMAGE_INFO redo_array[UNDO_STACK_SIZE];

struct ENVIRONMENT undo_env_array[UNDO_STACK_SIZE];
struct ENVIRONMENT redo_env_array[UNDO_STACK_SIZE];


/* Called when ever one changes the environment */
int zeroOutUndos()
{
  if(verboseLevel & VERBOSE_STACK)
  {
    st_report("zeroOutUndos: Clearing all Undo/Redo Buffers");
  }

  bzero((char *)undo_array, sizeof(undo_array));
  bzero((char *)redo_array, sizeof(redo_array));
  bzero((char *)undo_env_array, sizeof(undo_env_array));
  bzero((char *)undo_env_array, sizeof(undo_env_array));

  return(0);
}

/* Push the latest image onto the stack;  
 * First, check if it differs from the top
 * one;  if so, then push all the current 
 * stack images down one and place this new
 * one on top
 */
int pushUndoStack()
{
  int i;

  /* Check if the current image is the same as top one;
     if so, return without pushing */

  if(bcmp((char *)h0, (char *)&undo_array[0], sizeof(struct LP_IMAGE_INFO)) == 0)
  {
    if(verboseLevel & VERBOSE_STACK)
    {
      st_report("pushUndoStack: Images are the same; returning");
    }

    return(0);
  }

  /* Move UNDO_STACK_SIZE-2 down to UNDO_STACK_SIZE-1,
   * then UNDO_STACK_SIZE-3 down to UNDO_STACK_SIZE-2,
   * and so on until all have been push down one.
   * then copy the current one on the top
   */

  if(verboseLevel & VERBOSE_STACK)
  {
    st_report("pushUndoStack: Pushing Image Stack");
  }

  for(i=UNDO_STACK_SIZE;i>1;i--)
  {
    if(verboseLevel & VERBOSE_STACK)
    {
      st_print("pushUndoStack: Moving %d to %d\n", i-2, i-1);
    }

    bcopy((char *)&undo_array[i-2],     (char *)&undo_array[i-1],     sizeof(struct LP_IMAGE_INFO));
    bcopy((char *)&undo_env_array[i-2], (char *)&undo_env_array[i-1], sizeof(struct ENVIRONMENT));
  }

  bcopy((char *)h0,  (char *)&undo_array[0],     sizeof(struct LP_IMAGE_INFO));
  bcopy((char *)env, (char *)&undo_env_array[0], sizeof(struct ENVIRONMENT));

  return(0);
}


int popUndoStack()
{
  int i;

  /* First, is there anybody home in the undo stack ?? */
  /* We don't care about the top one as that is the current
   * one; is there anything starting at number 2 ??
   */

  if(strlen(undo_array[1].magic) && !strcmp(undo_array[1].magic, "LP_IMAGE_INFO"))
  {
    if(verboseLevel & VERBOSE_STACK)
    {
      st_report("popUndoStack(): Poping Top Stack Image into h0");
    }

    /* Copy the older image to the current ho holder */
    bcopy((char *)&undo_array[1],     (char *)h0,  sizeof(struct LP_IMAGE_INFO));
    bcopy((char *)&undo_env_array[1], (char *)env, sizeof(struct ENVIRONMENT));

    /* Now move everybody up one */
    for(i=1;i<UNDO_STACK_SIZE-1;i++)
    {
      if(verboseLevel & VERBOSE_STACK)
      {
        st_print("popUndoStack: Moving %d to %d\n", i, i-1);
      }

      bcopy((char *)&undo_array[i],     (char *)&undo_array[i-1],     sizeof(struct LP_IMAGE_INFO));
      bcopy((char *)&undo_env_array[i], (char *)&undo_env_array[i-1], sizeof(struct ENVIRONMENT));
    }

    /* Clear out the last one. */
    bzero((char *)&undo_array[UNDO_STACK_SIZE-1],     sizeof(struct LP_IMAGE_INFO));
    bzero((char *)&undo_env_array[UNDO_STACK_SIZE-1], sizeof(struct ENVIRONMENT));
  }
  else
  {
    st_report("popUndoStack(): Nothing to Undo");
  }

  return(0);
}


int pushRedoStack()
{
  int i;

  /* Check if the current image is the same as top one;
     if so, return without pushing */

  if(bcmp((char *)h0, (char *)&redo_array[0], sizeof(struct LP_IMAGE_INFO)) == 0)
  {
    if(verboseLevel & VERBOSE_STACK)
    {
      st_report("pushRedoStack: Images are the same; returning");
    }

    return(0);
  }

  /* Move UNDO_STACK_SIZE-2 down to UNDO_STACK_SIZE-1,
   * then UNDO_STACK_SIZE-3 down to UNDO_STACK_SIZE-2,
   * and so on until all have been push down one.
   * then copy the current one on the top
   */

  if(verboseLevel & VERBOSE_STACK)
  {
    st_report("pushRedoStack: Pushing Image Stack");
  }

  for(i=UNDO_STACK_SIZE;i>1;i--)
  {
    if(verboseLevel & VERBOSE_STACK)
    {
      st_print("pushRedoStack: Copying %d to %d\n", i-2, i-1);
    }

    bcopy((char *)&redo_array[i-2],     (char *)&redo_array[i-1],     sizeof(struct LP_IMAGE_INFO));
    bcopy((char *)&redo_env_array[i-2], (char *)&redo_env_array[i-1], sizeof(struct ENVIRONMENT));
  }

  bcopy((char *)h0,  (char *)&redo_array[0],     sizeof(struct LP_IMAGE_INFO));
  bcopy((char *)env, (char *)&redo_env_array[0], sizeof(struct ENVIRONMENT));

  return(0);
}


int popRedoStack()
{
  int i;

  /* First, is there anybody home in the undo stack ?? */
  /* We don't care about the top one as that is the current
   * one; is there anything starting at number 2 ??
   */

  if(strlen(redo_array[1].magic) && !strcmp(redo_array[1].magic, "LP_IMAGE_INFO"))
  {
    if(verboseLevel & VERBOSE_STACK)
    {
      st_report("popRedoStack(): Poping Top Stack Image into h0");
    }

    /* Copy the older image to the current ho holder */
    bcopy((char *)&redo_array[1],     (char *)h0,  sizeof(struct LP_IMAGE_INFO));
    bcopy((char *)&redo_env_array[1], (char *)env, sizeof(struct ENVIRONMENT));

    /* Now move everybody up one */
    for(i=1;i<UNDO_STACK_SIZE-1;i++)
    {
      if(verboseLevel & VERBOSE_STACK)
      {
        st_print("popRedoStack(): Moving %d to %d\n", i, i-1);
      }

      bcopy((char *)&redo_array[i],     (char *)&redo_array[i-1],     sizeof(struct LP_IMAGE_INFO));
      bcopy((char *)&redo_env_array[i], (char *)&redo_env_array[i-1], sizeof(struct ENVIRONMENT));
    }

    /* Clear out the last one. */
    bzero((char *)&redo_array[UNDO_STACK_SIZE-1],     sizeof(struct LP_IMAGE_INFO));
    bzero((char *)&redo_env_array[UNDO_STACK_SIZE-1], sizeof(struct ENVIRONMENT));
  }
  else
  {
    st_report("popRedoStack(): Nothing to Redo");
  }

  return(0);
}


int doUndo()
{
  pushRedoStack(); /* push the current image to the Redo stack */

  popUndoStack();

  return(0);
}


int doRedo()
{
  pushUndoStack(); /* push the current image to the Undo stack */

  popRedoStack();

  return(0);
}
