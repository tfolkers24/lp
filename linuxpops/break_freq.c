#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <math.h>

#define PARALLEL 1

int main(argc, argv)
int argc;
char *argv[];
{
  int    i, j, parts;
  double fmin, fmax, delta;
  double fs[256];

  if(argc < 4)
  {
    fprintf(stderr, "Usage: %s fmin fmax parts\n", argv[0]);
    exit(1);
  }

  bzero((char *)&fs[0], sizeof(fs));

  fmin  = atof(argv[1]);
  fmax  = atof(argv[2]);
  parts = atoi(argv[3]);

  delta = (fmax - fmin) / (double)parts;

  j = 0;
  for(i=1;i<(parts+1);i++)
  {
    fs[j++] = fmin;
    fs[j++] = fmin + delta;

    fmin += delta;
  }
  
#ifdef PARALLEL

  printf("::: ");

  for(i=0;i<(parts*2);i+=2)
  {
    printf("%.2f ", fs[i]);
  }

  printf("::: ");

  for(i=1;i<(parts*2);i+=2)
  {
    printf("%.2f ", fs[i]);
  }
#else
  for(i=0;i<(parts*2);i++)
  {
    printf("%.2f ", fs[i]);
  }
#endif

  printf("\n");

  return(0);
}
