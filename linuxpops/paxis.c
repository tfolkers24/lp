/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

char peakResultsStr1[256];
char peakResultsStr2[256];


int doXRange()
{
  int bd, ed;
  struct LP_IMAGE_INFO *image = h0;

  _doCCur("Click Left Mouse Button on Left Edge of Desired Region");

  bd = (int)round(env->ccur);

  if(bd < 1)
  {
    bd = 1;
  }

  _doCCur("Click Left Mouse Button on Right Edge of Desired Region");

  ed = (int)round(env->ccur);

  if(ed >= image->head.noint)
  {
    ed = image->head.noint -1;
  }

  ed = image->head.noint - ed;

  st_report("Bdrop = %d, Edrop = %d", bd, ed);

  env->bdrop = bd;
  env->edrop = ed;

  return(0);
}


/**
  Using mouse, set the y-axis range
  \returns 0
 */
int doYRange()
{
  double ymin, ymax, tmp;

  _doTCur("Click the Plot at the Minimum Value");

  ymin = env->tcur;

  _doTCur("Click the Plot at the Maximum Value");

  ymax = env->tcur;

  if(ymax < ymin) /* Check for invertion */
  {
    tmp = ymax;
    ymax = ymin;
    ymin = tmp;
  }

  env->ylower = ymin;
  env->yupper = ymax;
  env->yscale = Y_FIXED_SCALE;

  return(0);
}


int doZoom()
{
  int bd, ed, w;
  double left_x, left_y, right_x, right_y, tmp;
  struct WINDOW_ELEMENT *p;

  if(isSet(PF_A_FLAG))
  {
    doAutoScale();

    return(0);
  }

  /* Has the user ask for a predefined window ?? */
  if((w = getWindowFlag()) >= 0)
  {
    p = &env->winfo.windows[w];

    if(!p->used)
    {
      st_fail("Window %d, not set", w);

      return(1);
    }

  /* push args on stack */
    subArgs(2, "%f", p->xmin);
    subArgs(3, "%f", p->xmax);
    setXRange();

  /* push args on stack */
    subArgs(2, "%f", p->ymin);
    subArgs(3, "%f", p->ymax);
    setYRange();

    return(0);
  }

  if(isSet(PF_X_FLAG))
  {
    setFlag(PF_S_FLAG, 1);
    getPlotExtents();
    setFlag(PF_S_FLAG, 0);

    return(0);
  }

  _doCCur("Click Left Mouse Button on Left Edge of Desired Region");

  bd = (int)round(env->ccur);

  left_x = last_mouse_x;
  left_y = last_mouse_y;

  _doCCur("Click Left Mouse Button on Right Edge of Desired Region");

  ed = (int)round(env->ccur);

  right_x = last_mouse_x;
  right_y = last_mouse_y;

  /* Check for X inversion */
  if(bd > ed)
  {
    tmp = left_x;
    left_x = right_x;
    right_x = tmp;
  }

  /* Check for Y inversion */
  if(left_y < right_y)
  {
    tmp = left_y;
    left_y = right_y;
    right_y = tmp;
  }
 
  /* push args on stack */
  subArgs(2, "%f", left_x);
  subArgs(3, "%f", right_x);
  setXRange();

  /* push args on stack */
  subArgs(2, "%f", right_y);
  subArgs(3, "%f", left_y);
  setYRange();

  return(0);
}


int setXRange()
{
  int b, e, ret;
  double x1, x2;

  makeXaxisArray(h0);

  if(strlen(args->secondArg) && strlen(args->thirdArg))
  {
    x1 = atof(args->secondArg);
    x2 = atof(args->thirdArg);

    ret = findXaxisElement(h0, x1, x2, &b, &e);

    if(!ret)
    {
      st_report("findXaxisElement() returned channels %d %d", b, e);

      env->bdrop = b;
      env->edrop = h0->head.noint-e;
    }
    else
    {
      st_fail("setXRange(): Problem with Passed args %.3f %.3f", x1, x2);
      return(1);
    }
  }
  else
  {
    st_fail("Missing args for xrange");

    return(1);
  }

  return(0);
}



int setYRange()
{
  if(strlen(args->secondArg) && strlen(args->thirdArg))
  {
    env->ylower = atof(args->secondArg);
    env->yupper = atof(args->thirdArg);

    env->yscale = Y_FIXED_SCALE;
  }
  else
  {
    st_fail("Missing args for yrange");
    return(1);
  }

  return(0);
}


int showPeaks()
{
  int i;

  for(i=0;i<MAX_IMAGES;i++)
  {
    switch(i)
    {
      case 0: _h0(); break;
      case 1: _h1(); break;
      case 2: _h2(); break;
      case 3: _h3(); break;
      case 4: _h4(); break;
      case 5: _h5(); break;
      case 6: _h6(); break;
      case 7: _h7(); break;
      case 8: _h8(); break;
      case 9: _h9(); break;
    }

    if(h0->peakChan > 0)
    {
      showPeak();
      st_report(" ");
    }
  }

  _h0();

  return(0);
}



int showPeak()
{
  struct LP_IMAGE_INFO *image = h0;

  computeXaxis(image, 0);

  line_determine_min_max(image);

  if(env->x1mode == XMODE_FREQ)
  {
    sprintf(peakResultsStr1, "%.4fK @ %7.1f MHz; %10.6f GHz",
                        image->datamax,
                        image->x1data[image->peakChan],
                        (image->head.restfreq + image->x1data[image->peakChan]) / 1000.0);
  }
  else
  if(env->x1mode == XMODE_VEL)
  {
    sprintf(peakResultsStr1, "%.4fK @ %7.1f Km/s",
                        image->datamax,
                        image->x1data[image->peakChan]);
  }
  else
  if(env->x1mode == XMODE_CHAN)
  {
    sprintf(peakResultsStr1, "%.4fK @ Channel %7.1f",
                        image->datamax,
                        image->x1data[image->peakChan]);
  }

  if(env->x2mode == XMODE_FREQ)
  {
    sprintf(peakResultsStr2, "%.4fK @ %7.1f MHz; %10.6f GHz",
                        image->datamax,
                        image->x2data[image->peakChan],
                        (image->head.restfreq + image->x1data[image->peakChan]) / 1000.0);
  }
  else
  if(env->x2mode == XMODE_VEL)
  {
    sprintf(peakResultsStr2, "%.4fK @ %7.1f Km/s",
                        image->datamax,
                        image->x2data[image->peakChan]);
  }
  else
  if(env->x2mode == XMODE_CHAN)
  {
    sprintf(peakResultsStr2, "%.4fK @ Channel %7.1f",
                        image->datamax,
                        image->x2data[image->peakChan]);
  }

  st_report("Peak = %s", peakResultsStr1);

  strcmprs(peakResultsStr1);

  if(env->x1mode != env->x2mode)
  {
    st_report("Peak = %s", peakResultsStr2);

    strcmprs(peakResultsStr2);
  }

  return(0);
}

int doCC()
{
  st_report("Setting plotting x-axis to Channel-Channel");

  env->x1mode = XMODE_CHAN;
  env->x2mode = XMODE_CHAN;

  return(0);
}


int doCF()
{
  st_report("Setting plotting x-axis to Channel-Freq");

  env->x2mode = XMODE_CHAN;
  env->x1mode = XMODE_FREQ;

  return(0);
}


int doCV()
{
  st_report("Setting plotting x-axis to Channel-Velocity");

  env->x2mode = XMODE_CHAN;
  env->x1mode = XMODE_VEL;

  return(0);
}


int doFC()
{
  st_report("Setting plotting x-axis to Freq-Channel");

  env->x2mode = XMODE_FREQ;
  env->x1mode = XMODE_CHAN;

  return(0);
}


int doFF()
{
  st_report("Setting plotting x-axis to Freq-Freq");

  env->x2mode = XMODE_FREQ;
  env->x1mode = XMODE_FREQ;

  return(0);
}


int doFV()
{
  st_report("Setting plotting x-axis to Freq-Velocity");

  env->x2mode = XMODE_FREQ;
  env->x1mode = XMODE_VEL;

  return(0);
}


int doVC()
{
  st_report("Setting plotting x-axis to Velocity-Channel");

  env->x2mode = XMODE_VEL;
  env->x1mode = XMODE_CHAN;

  return(0);
}


int doVF()
{
  st_report("Setting plotting x-axis to Velocity-Freq");

  env->x2mode = XMODE_VEL;
  env->x1mode = XMODE_FREQ;

  return(0);
}


int doVV()
{
  st_report("Setting plotting x-axis to Velocity-Velocity");

  env->x2mode = XMODE_VEL;
  env->x1mode = XMODE_VEL;

  return(0);
}


int setPoints()
{
  st_report("Setting plotting format to Points");

  env->plotstyle = PLOT_STYLE_POINTS;

  return(0);
}


int setHisto()
{
  st_report("Setting plotting format to Historgraph");

  env->plotstyle = PLOT_STYLE_HISTO;

  return(0);
}


int setLines()
{
  st_report("Setting plotting format to Lines");

  env->plotstyle = PLOT_STYLE_LINES;

  return(0);
}


int setLinePts()
{
  st_report("Setting plotting format to LinePts");

  env->plotstyle = PLOT_STYLE_LINEPTS;

  return(0);
}


int freeX()
{
  env->bdrop = 0;
  env->edrop = 0;

  env->xplotmax = 0.0;
  env->xplotmin = 0.0;

  return(0);
}


int freeY()
{
  env->ylower = -999999999.9;
  env->yupper =  999999999.9;

  env->yscale = Y_AUTO_SCALE;

  env->yplotmax = 0.0;
  env->yplotmin = 0.0;

  return(0);
}


int setGrid()
{
  if(isSet(PF_O_FLAG))
  {
    env->gridmode = GRID_MODE_NONE;
  }
  else
  if(isSet(PF_X_FLAG))
  {
    env->gridmode = GRID_MODE_X_ONLY;
  }
  else
  if(isSet(PF_Y_FLAG))
  {
    env->gridmode = GRID_MODE_Y_ONLY;
  }
  else
  if(isSet(PF_B_FLAG))
  {
    env->gridmode = GRID_MODE_FULL;
  }

  return(0);
}


double mapFtoChan(f)
double f;
{
  int i, n, next;
  double c = 0.0;
  double xtab[MAX_CHANS], ytab[MAX_CHANS];
  struct LP_IMAGE_INFO *image = h0;

  n = image->head.noint;

  if(env->x1mode == XMODE_FREQ)
  {
    for(i=0;i<n;i++)
    {
      xtab[i] = (double)image->x1data[i];
      ytab[i] = (double)i;
    }

    c = interpl8( &xtab[0], &ytab[0], n, f, &next );
  }
  else
  if(env->x2mode == XMODE_FREQ)
  {
    for(i=0;i<n;i++)
    {
      xtab[i] = (double)image->x2data[i];
      ytab[i] = (double)i;
    }

    c = interpl8( &xtab[0], &ytab[0], n, f, &next );
  }
  else
  {
    st_fail("Niether X-Axis is set to Freq");
  }

  return(c);
}


double mapVtoChan(v)
double v;
{
  int i, n, next;
  double c = 0.0;
  double xtab[MAX_CHANS], ytab[MAX_CHANS];
  struct LP_IMAGE_INFO *image = h0;

  n = image->head.noint;

  if(env->x1mode == XMODE_VEL)
  {
    for(i=0;i<n;i++)
    {
      xtab[i] = (double)image->x1data[i];
      ytab[i] = (double)i;
    }

    c = interpl8( &xtab[0], &ytab[0], n, v, &next );
  }
  else
  if(env->x2mode == XMODE_VEL)
  {
    for(i=0;i<n;i++)
    {
      xtab[i] = (double)image->x2data[i];
      ytab[i] = (double)i;
    }

    c = interpl8( &xtab[0], &ytab[0], n, v, &next );
  }
  else
  {
    st_fail("Niether X-Axis is set to Vel");
  }

  return(c);
}



/* Continuum Axis Routines */

int doAccumArgs()
{
  int i, good1=0, good2=0;

  if(strlen(args->secondArg))
  {
    for(i=0;i<NUM_ACCUM_PLOT_X;i++)
    {
      if(!strncmp(args->secondArg, accum_x_strs[i], strlen(args->secondArg)))
      {
        env->acc_plot_select_x = i;
        st_report("Setting Accum Plot X-Axis to %s", accum_x_strs[i]);
        good1 = 1;
        break;
      }
    }
  }

  if(strlen(args->thirdArg))
  {
    for(i=0;i<NUM_ACCUM_PLOT_Y;i++)
    {
      if(!strncmp(args->thirdArg, accum_y_strs[i], strlen(args->thirdArg)))
      {
        env->acc_plot_select_y = i;
        st_report("Setting Accum Plot Y-Axis to %s", accum_y_strs[i]);
        good2 = 1;
        break;
      }
    }
  }

  if(!good1)
  {
    st_print("Possible Options; Xaxis: ");
    for(i=1;i<NUM_ACCUM_PLOT_X;i++)
    {
      st_print("%s ", accum_x_strs[i]);
    }

    st_print("\n");
  }

  if(!good2)
  {
    st_print("Possible Options; Yaxis: ");
    for(i=1;i<NUM_ACCUM_PLOT_Y;i++)
    {
      st_print("%s ", accum_y_strs[i]);
    }

    st_print("\n");
  }

  return(0);
}

