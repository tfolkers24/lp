/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

struct X_AXIS_ARRAY {
	double chans;
	double vel;
	double freq;
};

struct X_AXIS_ARRAY x_axis_array[MAX_CHANS];

/* This version was from xgraphic.c */
int computeXaxis(image, logit)
struct LP_IMAGE_INFO *image;
int logit;
{
  int i, n;
  struct HEADER *h;
  float *p;

  h = &image->head;

  n = h->noint;
  if(image->pinfo.x1mode == XMODE_FREQ)
  {
    if(logit)
    {
      st_report("# Loading X1-Axis with Frequency Values");
    }

    p = &image->x1data[0];
    if( h->sideband == 2.0)
    {
      for(i=0;i<n;i++,p++)
      {
        *p = -(h->refpt * h->freqres) + ((float)i * h->freqres);
      }
    }
    else
    {
      for(i=0;i<n;i++,p++)
      {
        *p = +(h->refpt * h->freqres) - ((float)i * h->freqres);
	*p *= -1.0;
      }
    }
  }
  else
  if(image->pinfo.x1mode == XMODE_VEL)
  {

    if(logit)
    {
      st_report("# Loading X1-Axis with Velocity Values");
    }

    p = &image->x1data[0];
    if( h->sideband == 2.0)
    {
      for(i=0;i<h->noint;i++,p++)
      {
        *p = (float)(h->refpt_vel - (h->refpt * h->deltax) + ((i+1) * h->deltax));
      }
    }
    else
    {
      for(i=0;i<h->noint;i++,p++)
      {
        *p = (float)(h->refpt_vel - (h->refpt * h->deltax) + ((i+1) * h->deltax));
      }
    }
  }
  else
  if(image->pinfo.x1mode == XMODE_CHAN)
  {
    if(logit)
    {
      st_report("# Loading X1-Axis with Channel Numbers");
    }

    p = &image->x1data[0];
    for(i=0;i<h->noint;i++,p++)
    {
      *p = (float)i;
    }
  }

  if(image->pinfo.x2mode == XMODE_FREQ)
  {
    if(logit)
    {
      st_report("# Loading X2-Axis with Frequency Values");
    }

    p = &image->x2data[0];
    if( h->sideband == 2.0)
    {
      for(i=0;i<n;i++,p++)
      {
        *p = -(h->refpt * h->freqres) + ((float)i * h->freqres);
      }
    }
    else
    {
      for(i=0;i<n;i++,p++)
      {
        *p = +(h->refpt * h->freqres) - ((float)i * h->freqres);
	*p *= -1.0;
      }
    }
  }
  else
  if(image->pinfo.x2mode == XMODE_VEL)
  {

    if(logit)
    {
      st_report("# Loading X2-Axis with Velocity Values");
    }

    p = &image->x2data[0];
    if( h->sideband == 2.0)
    {
      for(i=0;i<h->noint;i++,p++)
      {
        *p = (float)(h->refpt_vel - (h->refpt * h->deltax) + ((i+1) * h->deltax));
      }
    }
    else
    {
      for(i=0;i<h->noint;i++,p++)
      {
        *p = (float)(h->refpt_vel - (h->refpt * h->deltax) + ((i+1) * h->deltax));
      }
    }
  }
  else
  if(image->pinfo.x2mode == XMODE_CHAN)
  {
    if(logit)
    {
      st_report("# Loading X2-Axis with Channel Numbers");
    }

    p = &image->x2data[0];
    for(i=0;i<h->noint;i++,p++)
    {
      *p = (float)i;
    }
  }

  return(0);
}


int makeXaxisArray(image)
struct LP_IMAGE_INFO *image;
{
  int i, n;
  struct HEADER *h;
  struct X_AXIS_ARRAY *p;

  h = &image->head;
  n = h->noint;

  p = &x_axis_array[0];
  if( h->sideband == 2.0)
  {
    for(i=0;i<n;i++,p++)
    {
      p->chans = (double)i;
      p->freq = -(h->refpt * h->freqres) + ((float)i * h->freqres);
    }
  }
  else
  {
    for(i=0;i<n;i++,p++)
    {
      p->chans = (double)i;
      p->freq = +(h->refpt * h->freqres) - ((float)i * h->freqres);
      p->freq *= -1.0;
    }
  }

  p = &x_axis_array[0];
  if( h->sideband == 2.0)
  {
    for(i=0;i<h->noint;i++,p++)
    {
      p->vel = (float)(h->refpt_vel - (h->refpt * h->deltax) + ((i+1) * h->deltax));
    }
  }
  else
  {
    for(i=0;i<h->noint;i++,p++)
    {
      p->vel = (float)(h->refpt_vel - (h->refpt * h->deltax) + ((i+1) * h->deltax));
    }
  }

  return(0);
}


int findXaxisElement(image, x1, x2, b, e)
struct LP_IMAGE_INFO *image;
double x1, x2;
int *b, *e;
{
  int i, indx, n;
  double ddiff;
  struct HEADER *h;
  struct X_AXIS_ARRAY *p;

  *b = 0;
  *e = 0;

  h = &image->head;
  n = h->noint;

  if(image->pinfo.x1mode == XMODE_FREQ)
  {
    ddiff = 99999999.9;
    p = &x_axis_array[0];
    for(i=0;i<n;i++,p++)
    {
      if(fabs(p->freq-x1) < ddiff)
      {
        indx = i;
        ddiff = fabs(p->freq-x1);
      }
    }

    *b = indx;

    ddiff = 99999999.9;
    p = &x_axis_array[0];
    for(i=0;i<n;i++,p++)
    {
      if(fabs(p->freq-x2) < ddiff)
      {
        indx = i;
        ddiff = fabs(p->freq-x2);
      }
    }

    *e = indx;

    return(0);
  }
  else
  if(image->pinfo.x1mode == XMODE_VEL)
  {
    ddiff = 99999999.9;
    p = &x_axis_array[0];
    for(i=0;i<n;i++,p++)
    {
      if(fabs(p->vel-x1) < ddiff)
      {
        indx = i;
        ddiff = fabs(p->vel-x1);
      }
    }

    *b = indx;

    ddiff = 99999999.9;
    p = &x_axis_array[0];
    for(i=0;i<n;i++,p++)
    {
      if(fabs(p->vel-x2) < ddiff)
      {
        indx = i;
        ddiff = fabs(p->vel-x2);
      }
    }

    *e = indx;

    return(0);
  }
  else
  if(image->pinfo.x1mode == XMODE_CHAN)
  {
    *b = (int)x1;
    *e = (int)x2;

    return(0);
  }
  else
  {
    return(1);
  }

  return(1);
}
