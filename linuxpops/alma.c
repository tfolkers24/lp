/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */

#include "extern.h"

struct CALIBRATOR calibrators[MAX_ALMA_CALIBRATORS];

int max_calibrators, almaSortField = SORT_BY_DATE, almaBand = 3;
double almaFreq;

char alma_tok[MAX_TOKENS][MAX_TOKENS];

struct FLUX_HISTORY flux_history;

int alma_get_tok(edit)
char *edit;
{
  int c = 0;
  char *token;

  bzero((char *)&alma_tok, sizeof(alma_tok));

  token = strtok(edit, ",\n");
  while(token != (char *)NULL)
  {
    sprintf(alma_tok[c++], "%s", token);
    token = strtok((char *)NULL, ",\n");
  }

  return(c);
}


int almaSort(p1, p2)
struct CALIBRATOR *p1, *p2;
{
  if(almaSortField == SORT_BY_DATE)
  {
    return(strcmp(p1->date, p2->date));
  }
  else
  if(almaSortField == SORT_BY_NAME)
  {
    return(strcmp(p1->name, p2->name));
  }
  else
  if(almaSortField == SORT_BY_FREQ)
  {
    if(p1->freq < p2->freq)
    {
      return(-1);
    }
    else
    if(p1->freq > p2->freq)
    {
      return(1);
    }
    else
    {
      return(0);
    }
  }
  else
  if(almaSortField == SORT_BY_JANSKY)
  {
    if(p1->jk < p2->jk)
    {
      return(-1);
    }
    else
    if(p1->jk > p2->jk)
    {
      return(1);
    }
    else
    {
      return(0);
    }
  }

  return(0);
}


int sortCalibrators(field)
int field;
{
  almaSortField = field;

#ifndef __DARWIN__
  qsort(&calibrators[0], max_calibrators, sizeof(struct CALIBRATOR), (__compar_fn_t)almaSort);
#else
  qsort(&calibrators[0], max_calibrators, sizeof(struct CALIBRATOR), almaSort);
#endif

  return(0);
}


char angle_tok[40][40];

int angle_get_tok(edit)
char *edit;
{
  int c = 0;
  char *token;

  bzero((char *)&angle_tok, sizeof(angle_tok));

  token = strtok(edit, " :\n");
  while(token != (char *)NULL)
  {
    sprintf(angle_tok[c++], "%s", token);
    token = strtok((char *)NULL, " :\n");
  }

  return(c);
}


double angleToDeg(inBuf)
char *inBuf;
{
  int     n, negflag=0;
  char    buf[256];
  double  decdeg, decmin, decsec, d;

  strcpy(buf, inBuf);

  n = angle_get_tok(buf);

  if(n == 3)
  {
    decdeg = atof(angle_tok[0]);
    decmin = atof(angle_tok[1]);
    decsec = atof(angle_tok[2]);

    if(decdeg < 0.0)  /* Look for negitive num */
    {
      negflag = 1;
    }

    decdeg = fabs(decdeg);

    d = decdeg + decmin / 60.0 + decsec / 3600.0;

    if(negflag)
    {
      d *= -1.0;
    }
  }
  else
  {
    d = 0.0;
  }

  return(d);
}



int loadCalibrators(name)
char *name;
{
  int n, j = 0, indx=0;
  char fname[512], line[1024], *string;
  FILE *fp;
  struct CALIBRATOR *p;

  if(name && strlen(name))
  {
    strcpy(fname, name);
  }
  else /* Load defaults calibrators */
  {
    sprintf(fname, "%s/share/alma_calibrators.dat", pkg_home);
  }

  if((fp = fopen(fname, "r") ) <= (FILE *)0)
  {
    st_fail("Unable to open file %s", fname);
    return(0);
  }

  bzero((char *)&calibrators[0], sizeof(calibrators));

  p = &calibrators[0];

  while((string = fgets(line, sizeof(line), fp)) != NULL)
  {
    indx++;

    if(line[0] == '#')
    {
      continue;
    }

    n = alma_get_tok(line);

    if(n == 8)
    {
      p->band    = atoi(alma_tok[0]);

      strncpy(p->date, alma_tok[1], strlen(alma_tok[1]));
      strcmprs(p->date);

      p->ra      = angleToDeg(alma_tok[2]) * 15.0;
      p->dec     = angleToDeg(alma_tok[3]);

      p->jk      = atof(alma_tok[4]);
      p->errBar  = atof(alma_tok[5]);
      p->freq    = atof(alma_tok[6]) / 1e9;

      strncpy(p->name, alma_tok[7], strlen(alma_tok[7]));
      strcmprs(p->name);

      p++;
      j++;

      if(j >= MAX_ALMA_CALIBRATORS)
      {
        st_fail("Maximum number Calibrators Loaded");
        max_calibrators = j;
        fclose(fp);

        return(1);
      }
    }
    else
    {
      st_print("Wrong number of tokens %d, in loadCalibrators(), line %d\n", n, indx);
    }
  }

  fclose(fp);

  st_report("Loaded %d ALMA Calibrators", j);

  max_calibrators = j;

  sortCalibrators(SORT_BY_DATE); /* Sort them by date, then freq */

  return(0);
}



int showCalibrator(p, first)
struct CALIBRATOR *p;
int first;
{
  char rastr[80], decstr[80];


  if(p->band != almaBand)
  {
    return(0);
  }

  sexagesimal(p->ra / 15.0, rastr, DDD_MM_SS_S100);
  sexagesimal(p->dec, decstr, HH_MM_SS);


  if(first)
  {
    st_print("\n");
    st_print(
    "                                  Source                               RA        Dec      Freq   Jy            Date-Obs  AB\n\n");
  }

  st_print("%64.64s %s %s %7.2f %5.2f +/- %4.2f %s  %d\n", 
			p->name, rastr, decstr, p->freq, p->jk, p->errBar, p->date, p->band);

  return(0);
}


int plotAlmaCalibrator()
{
  int i, j, y, m, d, first = 1;
  char src[80], firstDate[80], lastDate[80], tbuf[256];
  struct CALIBRATOR   *p;

  bzero((char *)&flux_history, sizeof(flux_history));

  if(!strncasecmp(env->source, "APS-", 4)) /* Strip off APS- */
  {
    strcpy(src, env->source+4);
  }
  else
  {
    strcpy(src, env->source);
  }

  st_report("Plotting ALMA Calibrator %s", src);

  p = &calibrators[0];
  j = 0;
  for(i=0;i<max_calibrators;i++,p++)
  {
    if(strcasestr(p->name, src) && /*p->band == almaBand &&*/ (fabs(p->freq-almaFreq) < 5.0))
    {
      y = atoi(p->date);
      m = atoi(p->date+5);
      d = atoi(p->date+8);

      makeJulDate(m, d, y-2000, 0.0);

      sprintf(tbuf, "%04d/%02d/%02d", y, m, d);

      strcpy(flux_history.history[j].date, tbuf);
      flux_history.history[j].flux = p->jk;

      if(first)
      {
        strcpy(firstDate, p->date);
        first = 0;
      }
      else
      {
        strcpy(lastDate, p->date);
      }

      j++;
    }
  }

  if(j < 3)
  {
    st_fail("Little or No ALMA calibrators for %s @ %f GHz", src, almaFreq);
    return(1);
  }
  else
  {
    st_report("Plotting %d ALMA calibrator measurements", j);
    st_report("Dated from %s to %s", firstDate, lastDate);
  }

  sprintf(flux_history.datestr, "Dated from %s to %s", firstDate, lastDate);

  if(xgraphic)
  {
    st_report("Sending Flux History to Xgraphic");

    strcpy(flux_history.magic, "FLUX_HISTORY");
    strcpy(flux_history.name, src);

    flux_history.num = j;

    flux_history.plotwin = env->plotwin;

    sock_write(xgraphic, (char *)&flux_history, sizeof(flux_history));

    last_plot = LAST_PLOT_FLUX_HISTORY;
  }

  return(0);
}


int createAlmaCat(band)
int band;
{
  int i, first = 1;
  char raStr[256], decStr[256];
  struct CALIBRATOR *p;
  char old_name[80], catname[80];
  char outBuf[1024], previous[512];
  FILE *fp;

  sprintf(catname, "alma_band_%d_cal.cat", band);

  if((fp = fopen(catname, "w") ) <= (FILE *)0)
  {
    st_fail("Unable to open file %s", catname);
    return(0);
  }
  
  p = &calibrators[0];
  for(i=0;i<max_calibrators;i++,p++)
  {
    if(band == p->band)
    {
      if(first)
      {
        strncpy(old_name, p->name, 10);
        first = 0;
      }

      sexagesimal(p->ra / 15.0, raStr,  DDD_MM_SS_S100);
      sexagesimal(p->dec,       decStr, DDD_MM_SS_S100);

      sprintf(outBuf, "%s %s J2000.0 %10.10s 0.0 LSR RAD # %d %4.1f %s", 
					raStr, decStr, p->name, p->band, p->jk, p->name);

      if(strncmp(old_name, p->name, 10))
      {
	fprintf(fp, "%s\n", previous);
	strcpy(old_name, p->name);
      }

      strcpy(previous, outBuf);
    }
  }

  st_report("Wrote ALMA Calibrator Source Catalog %s", catname);

  fclose(fp);

  return(0);
}


int validateCalibrators()
{
  int i, j=0, b1=0, b2=0, b3=0, b4=0, b5=0, b6=0, b7=0, b8=0, b9=0, b10=0;
  struct CALIBRATOR *p;

  p = &calibrators[0];
  for(i=0;i<max_calibrators;i++,p++)
  {
    if(p->band > 0)
    {
      switch(p->band)
      {
        case 1:  b1++; break;
        case 2:  b2++; break;
        case 3:  b3++; break;
        case 4:  b4++; break;
        case 5:  b5++; break;
        case 6:  b6++; break;
        case 7:  b7++; break;
        case 8:  b8++; break;
        case 9:  b9++; break;
        case 10: b10++; break;
      }

      j++;
    }
  }

  st_report("Validate(): Found %d total calibrators out of %d", j, max_calibrators);

  st_report("Band:    1    2    3    4    5    6    7    8    9   10");
  st_report("Num:  %4d %4d %4d %4d %4d %4d %4d %4d %4d %4d",
		   b1, b2, b3, b4, b5, b6, b7, b8, b9, b10);

  return(0);
}


int listAlmaBands()
{
  st_report("ALMA Band 2: %7.3f - %7.3f", ALMA_BAND_2_LOWER, ALMA_BAND_2_UPPER);
  st_report("ALMA Band 3: %7.3f - %7.3f", ALMA_BAND_3_LOWER, ALMA_BAND_3_UPPER);
  st_report("ALMA Band 4: %7.3f - %7.3f", ALMA_BAND_4_LOWER, ALMA_BAND_4_UPPER);
  st_report("ALMA Band 5: %7.3f - %7.3f", ALMA_BAND_5_LOWER, ALMA_BAND_5_UPPER);
  st_report("ALMA Band 6: %7.3f - %7.3f", ALMA_BAND_6_LOWER, ALMA_BAND_6_UPPER);
  st_report("ALMA Band 7: %7.3f - %7.3f", ALMA_BAND_7_LOWER, ALMA_BAND_7_UPPER);
  st_report("ALMA Band 8: %7.3f - %7.3f", ALMA_BAND_8_LOWER, ALMA_BAND_8_UPPER);
  st_report("ALMA Band 9: %7.3f - %7.3f", ALMA_BAND_9_LOWER, ALMA_BAND_9_UPPER);

  return(0);
}


int almaCalibrator()
{
  int i, show_all = 0, specific_src = 0, first = 1;
  double f;
  char *src = "   ", src2[80];
  struct CALIBRATOR *p;

  if(isSet(PF_L_FLAG))
  {
    listAlmaBands();

    return(0);
  }

  f = env->freq;

  if(f > ALMA_BAND_2_LOWER && f <= ALMA_BAND_2_UPPER)
  {
    almaBand = 2;
  }
  else
  if(f > ALMA_BAND_3_LOWER && f <= ALMA_BAND_3_UPPER)
  {
    almaBand = 3;
  }
  else
  if(f > ALMA_BAND_4_LOWER && f <= ALMA_BAND_4_UPPER)
  {
    almaBand = 4;
  }
  else
  if(f > ALMA_BAND_5_LOWER && f <= ALMA_BAND_5_UPPER)
  {
    almaBand = 5;
  }
  else
  if(f > ALMA_BAND_6_LOWER && f <= ALMA_BAND_6_UPPER)
  {
    almaBand = 6;
  }
  else
  if(f > ALMA_BAND_7_LOWER && f <= ALMA_BAND_7_UPPER)
  {
    almaBand = 7;
  }
  else
  if(f > ALMA_BAND_8_LOWER && f <= ALMA_BAND_8_UPPER)
  {
    almaBand = 8;
  }
  else
  if(f > ALMA_BAND_9_LOWER && f <= ALMA_BAND_9_UPPER)
  {
    almaBand = 9;
  }
  else
  if(f > ALMA_BAND_10_LOWER && f <= ALMA_BAND_10_UPPER)
  {
    almaBand = 10;
  }

  if(strlen(args->secondArg))
  {
    if(!strcmp(args->secondArg, "sort"))
    {
      if(strlen(args->thirdArg))
      {
        if(!strncmp(args->thirdArg, "name", strlen(args->thirdArg)))
        {
	  almaSortField = SORT_BY_NAME;
        }
        else
        if(!strncmp(args->thirdArg, "date", strlen(args->thirdArg)))
        {
	  almaSortField = SORT_BY_DATE;
        }
        else
        if(!strncmp(args->thirdArg, "freq", strlen(args->thirdArg)))
        {
	  almaSortField = SORT_BY_FREQ;
        }
        else
        if(!strncmp(args->thirdArg, "jansky", strlen(args->thirdArg)))
        {
	  almaSortField = SORT_BY_JANSKY;
        }

        sortCalibrators(almaSortField);
      }
      else
      {
        st_fail("Missing arg for 'alma sort'; [name|date|freq|jansky]");

        printH_FlagHelp("alma");
      }

      return(0);
    }
    else
    if(!strcmp(args->secondArg, "plot"))
    {
      if(strlen(args->thirdArg))
      {
        almaFreq = atof(args->thirdArg);
        plotAlmaCalibrator();
      }
      else
      {
        st_fail("Missing arg for 'alma plot freq");

        printH_FlagHelp("alma");
      }
      return(0);
    }
    else
    if(!strcmp(args->secondArg, "validate"))
    {
      validateCalibrators();
      return(0);
    }
    else
    if(!strncmp(args->secondArg, "catalog", strlen(args->secondArg)))
    { 
     /* First sort the catalog by date */
     /* This places the most recent entry for each soure last */
      sortCalibrators(SORT_BY_DATE);

     /* Now sort by name which puts them in RA order */
      sortCalibrators(SORT_BY_NAME);

      createAlmaCat(almaBand);

     /* Put the catalog back in date order */
      sortCalibrators(SORT_BY_DATE);

      return(0);
    }
    else
    if(!strcmp(args->secondArg, "all"))
    {
      show_all = 1;
    }
    else
    if(!strcmp(args->secondArg, "reload"))
    {
      st_report("Reloading ALMA Calibrators");

      loadCalibrators(NULL);

      return(0);
    }
    else /* Assume any remaining arg is a source name */
    {
      src = args->secondArg;
      specific_src = 1;
    }
  }
  else
  {
    src = env->source;
  }


  st_print("Looking for Band-%d: Calibrator: %s\n", almaBand, src);

  if(!strncasecmp(env->source, "APS-", 4)) /* Strip off APS- */
  {
    strcpy(src2, env->source+4);
  }
  else
  {
    strcpy(src2, env->source);
  }

  p = &calibrators[0];
  for(i=0;i<max_calibrators;i++,p++)
  {
    if(show_all) /* Show them all */
    {
      showCalibrator(p, first);
      first = 0;

      continue;
    }

    if(specific_src) /* Show only the passed arg */
    {
//      st_print("Looking for Calibrator %s in %s\n", src, p->name);
      if(strcasestr(p->name, src))
      {
        showCalibrator(p, first);
        first = 0;

        continue;
      }
    }
    else
    {
      if(strcasestr(p->name, src2))
      {
        showCalibrator(p, first);
        first = 0;

        continue;
      }
    }
  }

  if(first)
  {
    st_fail("No Band-%d Calibrator Found Matching %s or %s", almaBand, src, src2);
  }

  return(0);
}
