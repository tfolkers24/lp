/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

#ifndef __DARWIN__
#include <mysql/mysql.h>
#else
#include <mysql/include/mysql.h>
#endif


struct MYSQL_RESPONSE {
	int    SCANID;
	double Scanno;
	char   Fname[120];
	char   Obs[8];
	char   Opr[8];
	double UTdate;
	char   Tele[8];
	double UT;
	double LST;
	char   Source[24];
	char   Epoch[8];
	double RA;
	double Declin;
	double Vel;
	double Az;
	double El;
	double AzOff;
	double ElOff;
	char   Front[8];
	char   Line[16];
	double Rest;
	double Sky;
	char   Back[8];
	double Noint;
	double Freqres;
	double FirstIF;
	double Sb;
	double T_Sys;
	double Tamb;
	double Pres;
	double Hum;
	double mmh20;
	double PBeam;
	double NutRate;
	char   ObsMode[8];
	double IntTime;
};

struct MYSQL_RESPONSE mysql_response;


int first, dbindx;

char dbase_tok[120][120];

/**
 * Split results up into tokens
 */
int dbase_get_tok(edit)
char *edit;
{
  int c = 0;
  char *token;

  bzero((char *)&dbase_tok, sizeof(dbase_tok));

  token = strtok(edit, ",");
  while(token != (char *)NULL)
  {
    sprintf(dbase_tok[c++], "%s", token);
    token = strtok((char *)NULL, ",");
  }

  return(c);
}


/*

36 items;

0000094492  177.01 /home/archive/2017-2018/12m/data/lmz/sdd.lmz_043 sys sck \
2004.1009 NRAO 12M 12.12277222  5.91141887 IRC+10216 1950.0 146.31166667 13.51111111 \
-26  95.24140111  33.6776447 0 0 3mmhi 12CO 115271.204  115287.7146841 			\
FB_2_01 256 1 1500 2 3337 15.86  611.05 27.35 48.24 120.00002  0 LINEBSP 120

 */
int printRow(buf)
char *buf;
{
  int n;

  if(buf)
  {
    n = dbase_get_tok(buf);
    if(n == 36)
    {
      mysql_response.SCANID   = atoi(dbase_tok[ 0]);
      mysql_response.Scanno   = atof(dbase_tok[ 1]);
      strcpy(mysql_response.Fname,   dbase_tok[ 2]);
      strcpy(mysql_response.Obs,     dbase_tok[ 3]);
      strcpy(mysql_response.Opr,     dbase_tok[ 4]);
      mysql_response.UTdate   = atof(dbase_tok[ 5]);
      strcpy(mysql_response.Tele,    dbase_tok[ 6]);
      mysql_response.UT       = atof(dbase_tok[ 7]);
      mysql_response.LST      = atof(dbase_tok[ 8]);
      strcpy(mysql_response.Source,  dbase_tok[ 9]);
      strcpy(mysql_response.Epoch,   dbase_tok[10]);
      mysql_response.RA       = atof(dbase_tok[11]);
      mysql_response.Declin   = atof(dbase_tok[12]);
      mysql_response.Vel      = atof(dbase_tok[13]);
      mysql_response.Az       = atof(dbase_tok[14]);
      mysql_response.El       = atof(dbase_tok[15]);
      mysql_response.AzOff    = atof(dbase_tok[16]);
      mysql_response.ElOff    = atof(dbase_tok[17]);
      strcpy(mysql_response.Front,   dbase_tok[18]);
      strcpy(mysql_response.Line,    dbase_tok[19]);
      mysql_response.Rest     = atof(dbase_tok[20]);
      mysql_response.Sky      = atof(dbase_tok[21]);
      strcpy(mysql_response.Back,    dbase_tok[22]);
      mysql_response.Noint    = atof(dbase_tok[23]);
      mysql_response.Freqres  = atof(dbase_tok[24]);
      mysql_response.FirstIF  = atof(dbase_tok[25]);
      mysql_response.Sb       = atof(dbase_tok[26]);
      mysql_response.T_Sys    = atof(dbase_tok[27]);
      mysql_response.Tamb     = atof(dbase_tok[28]);
      mysql_response.Pres     = atof(dbase_tok[29]);
      mysql_response.Hum      = atof(dbase_tok[30]);
      mysql_response.mmh20    = atof(dbase_tok[31]);
      mysql_response.PBeam    = atof(dbase_tok[32]);
      mysql_response.NutRate  = atof(dbase_tok[33]);
      strcpy(mysql_response.ObsMode, dbase_tok[34]);
      mysql_response.IntTime  = atof(dbase_tok[35]);

      if(first)
      {
        printf("\n  Scan        Source                                  Filename                                 Date       Freq        Mode\n\n");
        first = 0;
      }

      printf("%8.2f  %16.16s  %64s  %9.4f  %10.3f  %8.8s\n", 
			mysql_response.Scanno, 
			mysql_response.Source, 
			mysql_response.Fname, 
			mysql_response.UTdate, 
			mysql_response.Rest, 
			mysql_response.ObsMode);
      dbindx++;
    }
    else
    {
      printf("N = %d\n", n);
    }
  }
  else
  {
  }

  return(0);
}


int doDbase()
{
  int i, num_fields;
  MYSQL *conn;
  MYSQL_RES *result;
  MYSQL_ROW row;
  char qstr[1024], rstr[4096], tbuf[256], gstr[64];

  st_report("Entering Dbase Sub-System");

  if(isSet(PF_R_FLAG))
  {
    st_report("Dbase Query Command");

    first = 1;
    dbindx = 0;

    conn = mysql_init(NULL);

    if(conn == NULL)
    {
      printf("Error %u: %s\n", mysql_errno(conn), mysql_error(conn));
      return(1);
    }

    if(mysql_real_connect(conn, "hamms", "tfolkers", "2bad4you", "ARO_Data_Archive", 0, NULL, 0) == NULL)
    {
      printf("Error %u: %s\n", mysql_errno(conn), mysql_error(conn));
      return(1);
    }

    if(isSet(PF_G_FLAG))
    {
      if(isSet(PF_O_FLAG))
      {
        strcpy(gstr, "GROUP by Source");
      }
      else
      if(isSet(PF_T_FLAG))
      {
        strcpy(gstr, "GROUP by Rest");
      }
      else
      {
        strcpy(gstr, "GROUP by Fname");
      }
    }
    else
    {
      strcpy(gstr, " ");
    }

    if(isSet(PF_M_FLAG)) /* Match Mode */
    {
      if(strlen(args->secondArg))
      {
        sprintf(qstr, "SELECT * FROM ARO_Data_Archive.Scan_Records WHERE ObsMode LIKE '%%%s%%' %s", args->secondArg, gstr);
      }
      else
      {
        printH_FlagHelp("dbase");

        mysql_close(conn); /* Always close the connection */

        return(1);
      }
    }
    else
    if(isSet(PF_S_FLAG) && notSet(PF_F_FLAG)) /* Match Source */
    {
      sprintf(qstr, 
		"SELECT * FROM ARO_Data_Archive.Scan_Records WHERE Source LIKE '%%%s%%' %s", 
							env->source, gstr);
    }
    else
    if(notSet(PF_S_FLAG) && isSet(PF_F_FLAG)) /* Match Freq */
    {
      sprintf(qstr, 
		"SELECT * FROM ARO_Data_Archive.Scan_Records WHERE Rest > '%.3f' AND Rest < '%.3f' %s", 
							(env->freq-env->dfreq) * 1e3, (env->freq+env->dfreq) * 1e3, gstr);
    }
    else
    if(isSet(PF_S_FLAG) && isSet(PF_F_FLAG)) /* Match Both */
    {
      sprintf(qstr, 
	"SELECT * FROM ARO_Data_Archive.Scan_Records WHERE Source LIKE '%%%s%%' AND Rest > '%.3f' AND Rest < '%.3f' %s", 
							env->source, (env->freq-env->dfreq) * 1e3, (env->freq+env->dfreq) * 1e3, gstr);
    }
    else
    {
      printH_FlagHelp("dbase");

      mysql_close(conn); /* Always close the connection */

      return(1);
    }

    printf("Qstr = %s\n", qstr);

    if(mysql_query(conn, qstr))
    {
      printf("Error %u: %s\n", mysql_errno(conn), mysql_error(conn));
      mysql_close(conn); /* Always close the connection */

      return(1);
    }

    result     = mysql_store_result(conn);

    if(result->row_count > 256)
    {
      if(!getYesNoQuit("MySql Returned %d Rows; Continue: y/N ?", result->row_count))
      {
	st_report("Cancelling Request; You might try the /g, grouping-flag, next time");

        mysql_free_result(result);

        mysql_close(conn); /* Always close the connection */

        return(0);
      }
    }

    num_fields = mysql_num_fields(result);
    row        = mysql_fetch_row(result);

    if(row)
    {
      while(row)
      {
        rstr[0] = '\0';

        for(i=0;i<num_fields;i++)
        {
          sprintf(tbuf, "%s ,", row[i]);
          strcat(rstr, tbuf);
        }

        printRow(rstr);

        row = mysql_fetch_row(result);
      }
    }
    else
    {
      st_report("No Records Found");
    }

    mysql_free_result(result);

    mysql_close(conn); /* Always close the connection */

    if(dbindx)
    {
      st_report("Found %d records", dbindx);
    }
  }

  if(isSet(PF_I_FLAG))
  {
    st_report("Dbase Ingest Command Not Implemented Yet");
  }

  return(0);
}
