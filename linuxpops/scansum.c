/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

struct SCAN_SUM_SEND ss;


int recordScanSum(indx, s)
int    indx;
struct SCAN_SUM *s;
{
  int wif;  /* which IF */
  double sq, trms;
  struct LP_IMAGE_INFO *image = h0;


  wif = getFeed(h0->head.scan) -1;

  st_print("Recording Scan Sum, indx %d, IF %d\n", indx, wif);

  /* we are doint the 250's so shift it down. */
  if(env->issmt && h0->head.freqres == 0.250 && wif > 3)
  {
    wif -= 4;
  }

  if(wif > -1 && wif < MAX_IFS)
  {
    s->scan[wif] = h0->head.scan;
    s->ut[wif]   = h0->head.ut;
    s->lst[wif]  = h0->head.lst;
    s->az[wif]   = h0->head.az;
    s->el[wif]   = h0->head.el;
    s->tsys[wif] = h0->head.stsys;
    s->tcal[wif] = h0->head.tcal;

    s->peak[wif] = env->datamax;

  /* Go compute the baseline which computes 
     the rms of the baseline region */
    computeBaseLine(image);

    if(env->hwdc < 0.5)
    {
      setHWDC(image);
    }

    sq = sqrt(fabs(image->head.freqres) * 1e6 * image->head.inttime * env->hwdc);

    if(sq > 0.0)
    {
      trms = image->head.stsys * 2.0 / sq;
    }
    else
    {
      st_fail("sqrt(freqres * inttime * hwdc) = %f", sq);
      trms = 99999.0;
    }

    s->rms[wif] = trms;
  }
  else
  {
    st_fail("Computed IF %d, out of range", wif);
    return(1);
  }

  return(0);
}


int scansum()
{
  int    indx, scan;
  struct SCAN_SUM *s;

  if(isSet(PF_I_FLAG))
  {
    bzero((char *)&ss, sizeof(ss));

    return(0);
  }

  if(isSet(PF_R_FLAG))
  {
    if(isSpecFivePoint(&h0->head))
    {
      st_print("Skipping 5-Point Scan\n");
      return(0);
    }

    scan = (int)h0->head.scan;
    if(ss.last_scan == 0)
    {
      ss.last_scan = scan;
    }

    if(ss.last_scan != scan)
    {
      ss.nscans++;
      ss.last_scan = scan;
    }

    indx = ss.nscans;

    if(indx == 0)
    {
      strcpy(ss.magic, "SCAN_SUM_SEND");

      bcopy((char *)&h0->head, (char *)&ss.head, sizeof(ss.head));
    }

    s = &ss.scans[indx];

    recordScanSum(indx, s);

    return(0);
  }

  if(isSet(PF_S_FLAG))
  {
    /* Set the defaults */
    ss.xaxis = SCAN_SUM_X_SCANS;
    ss.yaxis = SCAN_SUM_Y_PEAK;

    if(isSet(PF_U_FLAG))
    {
      ss.xaxis = SCAN_SUM_X_UT;
    }
    else
    if(isSet(PF_A_FLAG))
    {
      ss.xaxis = SCAN_SUM_X_AZ;
    }
    else
    if(isSet(PF_E_FLAG))
    {
      ss.xaxis = SCAN_SUM_X_EL;
    }
    else
    if(isSet(PF_U_FLAG))
    {
      ss.xaxis = SCAN_SUM_X_UT;
    }
    else
    if(isSet(PF_L_FLAG))
    {
      ss.xaxis = SCAN_SUM_X_LST;
    }
    else
    if(isSet(PF_N_FLAG))
    {
      ss.xaxis = SCAN_SUM_X_NUM;
    }

    if(isSet(PF_M_FLAG))
    {
      ss.yaxis = SCAN_SUM_Y_RMS;
    }
    else
    if(isSet(PF_T_FLAG))
    {
      ss.yaxis = SCAN_SUM_Y_TSYS;
    }
    else
    if(isSet(PF_C_FLAG))
    {
      ss.yaxis = SCAN_SUM_Y_TCAL;
    }

    ss.plotwin = env->plotwin;
    ss.xdelay  = env->xdelay;
    ss.yscale  = env->yscale;
    ss.ylower  = env->ylower;
    ss.yupper  = env->yupper;

    st_report("Sending SCAN_SUM Struct to XGraphics");

    sock_write(xgraphic, (char *)&ss, sizeof(ss));

    return(0);
  }

  return(0);
}
