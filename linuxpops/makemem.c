/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <readline/readline.h>
#include <readline/history.h>

#include "sex.h"
#include "scans.h"
#include "global.h"
#include "object.h"
#include "gridmap.h"
#include "linuxlib_proto.h"
#include "caclib_proto.h"
#include "observing_par.h"

//#include "proto.h"

#define READ_HISTORY  1
#define WRITE_HISTORY 2

#define CWRITE        1
#define CAPPEND       2

#define VERBOSE_PARSE 1

struct GLOBAL_MEMORY *CGM;
struct OBJECT_GLOBAL_MEMORY *OGM;

/* The following are required to link to ../lib/liblinux.a */
int   stquiet, recursiveLevel, verboseLevel, lp_logging;
FILE *errPrint_fp;

/* Environment Variables */
char pkg_home[256], obs_home[256], data_home[256], obs_inits[256],
     printer[256], help_home[256], lockFileName[256], globalName[256],
     onlineDataFile[256];

char *prompt = " ";

char  tok[MAX_TOKENS][MAX_TOKENS], cmd_tok[MAX_TOKENS][MAX_TOKENS];
char *firstArg  = tok[0];
char *secondArg = tok[1];
char *thirdArg  = tok[2];
char *fourthArg = tok[3];
char *fifthArg  = tok[4];
char *sixthArg  = tok[5];


int get_tok(edit)
char *edit;
{
  int c = 0;
  char *token;

  bzero((char *)&tok, sizeof(tok));

  token = strtok(edit, ",\n\t ");
  while(token != (char *)NULL)
  {
    sprintf(tok[c++], "%s", token);
    token = strtok((char *)NULL, ",\n\t ");
  }

  return(c);
} 


int getEnvironment()
{
  char *cp;

  cp = getenv("LINUXPOPS_HOME");
  if(cp)
  {
    strcpy(pkg_home, cp);
  }
  else
  {
    strcpy(pkg_home, "/home/analysis/linuxpops/");
  }

  cp = getenv("OBSDAT");
  if(cp)
  {
    strcpy(data_home, cp);
  }
  else
  {
    strcpy(data_home, "/home/data");
  }

  cp = getenv("HOME");
  if(cp)
  {
    strcpy(obs_home, cp);
  }
  else
  {
    strcpy(obs_home, "./");
  }

  cp = getenv("OBSINIT");
  if(cp)
  {
    strcpy(obs_inits, cp);
  }
  else
  {
    strcpy(obs_inits, "sys");
  }

  cp = getenv("PRINTER");
  if(cp)
  {
    strcpy(printer, cp);
  }
  else
  {
    sprintf(printer, "lp");
  }

  cp = getenv("LINUXPOPS_HELP_DIR");
  if(cp)
  {
    strcpy(help_home, cp);
  }
  else
  {
    strcpy(help_home, "/home/analysis/linuxpops/help");
  }

  st_report("");
  st_report("Package Dir:    %s", pkg_home);
  st_report("Help Dir:       %s", help_home);
  st_report("Obs Dir:        %s", obs_home);
  st_report("Data Dir:       %s", data_home);
  st_report("Obs Inits:      %s", obs_inits);
  st_report("Global Memory:  %s", globalName);
  st_report("");


  return(0);
}


int main(argc, argv)
int argc;
char *argv[];
{
  int opt, manGlobal=0;


  while ((opt = getopt(argc, argv, "g:")) != -1)
  {
    switch (opt)
    {
      case 'g':
	st_report("Using Global Memory %s", optarg);
        strcpy(globalName, optarg);
        manGlobal = 1;
        break;

      default: /* '?' */
        fprintf(stderr, "Usage: %s [-g global_memory_file]\n", argv[0]);
        exit(1);
        break;

    }
  }

  if(!manGlobal)
  {
    sprintf(globalName, "./LPMEMORY-64");
  }

  getEnvironment();

  if(!gm_map(globalName, 1))
  {
    fprintf(stderr, "Unable to map global memory\n");
    exit(1);
  }

  //strcpy(CGM->envrs[0].title, "Online");


  return(0);
}


