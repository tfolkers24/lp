/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

int noenv = 1;

int runFile()
{
  noenv = 0;

  if(readFile())
  {
    noenv = 1;
    return(1);
  }
  else
  {
    noenv = 1;
    return(0);
  }
}


/*
zenity --list --column="Freq" --column="Qty" --multiple --text="Select a Freq" \
          "230.538" 25 "220.500" 20 "115.270" 35
 */


int doZenity(cmd, rst)
char *cmd, *rst;
{
  FILE *fp;
  char *string, line[4096];
  static char cmdBuf[8*4096];

  sprintf(cmdBuf, "zenity %s 2> /dev/null", cmd);

  if( (fp = popen(cmdBuf, "r") ) <= (FILE *)0 )
  {
    st_fail("doZenity(): Unable to open %s", cmdBuf);

    return(1);
  }

  bzero(line, sizeof(line));

  while((string = fgets(line, sizeof(line), fp)) != NULL)
  {
    deNewLineIt(line);
    strcmprs(line);
    strcpy(rst, line);
  }

  pclose(fp);

  return(0);
}


#define MAX_LOAD_LEVELS 10

int incLevel(l)
int l;
{
  l++;

  if(l >= MAX_LOAD_LEVELS)
  {
    l = MAX_LOAD_LEVELS-1;
  }

  return(l);
}



int decLevel(l)
int l;
{
  l--;

  if(l <= 0)
  {
    l = 0;
  }

  return(l);
}


/* this routine needs to be made re-enterant */
int readFile()
{
  int ret;
  int n[MAX_LOAD_LEVELS] = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 };
  double start[MAX_LOAD_LEVELS], end[MAX_LOAD_LEVELS];
  FILE *fp[MAX_LOAD_LEVELS];
  char *string, line[256], fname[MAX_LOAD_LEVELS][256];
  static int lastline[MAX_LOAD_LEVELS] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
  static int level = 0;

  if(strlen(args->secondArg) < 2)
  {
    st_warn("Missing arg for readFile()");
    return(1);
  }

  level = incLevel(level);

  strcpy(fname[level], args->secondArg);

  st_report("readFile(): Opening up %s in run level %d", fname[level], level);

  if((fp[level] = fopen(fname[level], "r") ) <= (FILE *)0)
  {
    st_fail("readFile(): Unable to open file %s", fname[level]);
    level = decLevel(level);

    return(1);
  }

  if(notSet(PF_C_FLAG)) /* continuation flag */
  {
    lastline[level] = 0;
  }

  start[level] = getmJulDate(0);

  clearInterrupt();

  while((string = fgets(line, sizeof(line), fp[level])) != NULL)
  {
    n[level]++;

    if(n[level] < (lastline[level]-1)) /* Alway backup one line in the file */
    {
      continue;
    } 

    lastline[level] = n[level];

    if(strlen(line) < 2)
    {
      continue;
    }

    if(line[0] == '#')
    {
      continue;
    }

    if(isSet(PF_C_FLAG)) /* Check that the first line in contiune is not a delay */
    {
      if(strstr(line, "helperwait"))
      {
        setFlag(PF_C_FLAG, 0); /* Clear it */

        continue;
      }
    }

    deNewLineIt(line); /* Clean it up */

    if(verboseLevel & VERBOSE_PARSE)
    {
      st_report("Parsing: %s", line);
    }

    if(!strcmp(line, "break"))
    {
      st_report("readFile(1): Closing %s in run level %d", fname[level], level);
      fclose(fp[level]);

      end[level] = getmJulDate(0);

      st_report("Total Load Time in Secs: %.2f", (end[level]-start[level]) * 86400.0);

      level = decLevel(level);

      return(0);
    }

    if(isInterrupt()) /* Check for Control C */
    {
      st_report("readFile(2): Closing %s in run level %d", fname[level], level);
      fclose(fp[level]);

      end[level] = getmJulDate(0);

      st_report("Total Load Time in Secs: %.2f", (end[level]-start[level]) * 86400.0);

      level = decLevel(level);

      return(0);
    }

    ret = parseCmd(line);

    if(ret)
    {
      st_fail("Error in batch file: %s; aborting", fname[level]);

      if(isSet(PF_I_FLAG))
      {
        st_report("readFile(): Ignore flag set; attempting to continue");
	continue;
      }

      st_report("readFile(3): Closing %s in run level %d", fname[level], level);
      fclose(fp[level]);

      end[level] = getmJulDate(0);

      st_report("Total Load Time in Secs: %.2f", (end[level]-start[level]) * 86400.0);

      level = decLevel(level);

      return(1);
    }
  }

  st_report("readFile(4): Closing %s in run level %d", fname[level], level);
  fclose(fp[level]);

  end[level] = getmJulDate(0);

  st_report("Total Load Time in Secs: %.2f", (end[level]-start[level]) * 86400.0);

  level = decLevel(level);


  return( reloadFile() );
}


int checkLockFile()
{
  char sysbuf[256], *cp;
  FILE *fp;
  int ret;

  if(access(lockFileName, F_OK)) /* File doesn't exist; create it */
  {
    st_report("Creating LOCKFILE: %s", lockFileName);

    if((fp = fopen(lockFileName, "w") ) <= (FILE *)0)
    {
      st_fail("checkLockFile(): Unable to open lockfile %s", lockFileName);
      return(0);
    }

    fprintf(fp, "Observer Initials %s\n", obs_inits);
    fprintf(fp, "Session:          %s\n", session);
    fprintf(fp, "Host:             %s\n", hostname);
    fprintf(fp, "Date:             %s\n", getcTime(0));

    cp = getenv("REMOTEHOST");

    if(cp)
    {
      fprintf(fp, "RemoteHost        %s\n", cp);
    }

    fclose(fp);

    return(1);
  }

  st_report("LOCKFILE %s Already Exists", lockFileName);

  st_print(" \n");
  sprintf(sysbuf, "cat %s", lockFileName);
  system(sysbuf);

  while(1)
  {
    do
    {
      ret = getNumericResponse("\nTo Exit the program, type: 0\nTo remove the leftover lockfile, type: 1\nTo ignore the lockfile, type: 2\n");
    }
    while(ret < 0);

    if(ret == 0)
    {
      return(0);
    }
    else
    if(ret == 1)
    {
      removeLockFile();
      checkLockFile();
      return(1);
    }
    else
    if(ret == 2)
    {
      st_print("\nIgnoring LOCKFILE: Proceed at your own risk\n\n");
      doNotRemoveLockFile = 1;
      return(1);
    }
    else
    {
      st_print("Unknown Response\n");
    }
  }

  return(0);
}


int removeLockFile()
{
  char sysbuf[256];

  if(strlen(lockFileName))
  {
    st_report("Removing LOCKFILE: %s", lockFileName);
    sprintf(sysbuf, "rm -f %s", lockFileName);

    if(verboseLevel == 2)
    {
      st_report("Executing Command: %s", sysbuf);
    }

    system(sysbuf);
  }

  return(0);
}



int watchFile()
{
  st_warn("watchFile() Not implemented yet");

  return(0);
}


int reloadFile()
{
  subArgs(2, env->filename);
  args->ntokens = 2;

  return( loadFile() );
}


/* Did the data file change size; i.e. online file */
int fileChanged()
{
  FILE *fp;
  struct stat filestat;

  if( (fp = fopen(env->filename, "r") ) <= (FILE *)0 )
  {
    st_fail("fileChanged(): Unable to open %s", env->filename);
    return(1);
  }

  fstat(fileno(fp), &filestat);

  fclose(fp);

  /* Is the 'force' flag set or file larger */
  if(isSet(PF_F_FLAG) || (filestat.st_size != lastFileSize))
  {
    st_report("Reloading Data File %s", env->filename);
    strcpy(args->tok[1], env->filename);
    args->ntokens = 2;

    if(loadFile())
    {
      return(1);
    }

    lastFileSize = filestat.st_size;
  }
  else
  {
    st_report("Data File %s, Unchanged", env->filename);
  }

  return(0);
}


int loadFile()
{
  int ret;
  char results[256], input[256], tmp[256];

  bzero(results, sizeof(results));
  strcpy(tmp, env->filename);

  if(args->ntokens < 2)
  {
    sprintf(input, "--file-selection --title='Select a Datafile' --file-filter='*.save *.sav *.SAV *.KEEP *.keep sdd* *.sdd' --filename='%s/'", dirname(tmp));
    doZenity(input, results);

    st_report("doZenity() returned: '%s'", results);
    strcmprs(results);
    st_report("doZenity() After strcmprs: '%s'", results);

    if(strlen(results) < 4)
    {
      strcpy(results, env->filename);
    }

    subArgs(2, results);
  }

  strcpy(env->filename, args->secondArg);

  env->numofscans = 0;

  st_report("Loading File %s", env->filename);

  ret = findSize(env);

  if(ret < 0)
  {
    return(1);
  }

  env->numofscans = findScans(env);

  st_report("Found %d Scans; %d Sources", env->numofscans, numOfSources);

  sendFilenames();

  return(0);
}


int setGFileName()
{
  char results[256], input[256], tmp[256];

  bzero(results, sizeof(results));
  strcpy(tmp, env->gfilename);

  if(args->ntokens < 2)
  {
    sprintf(input, "--file-selection --title='Select a Gainsfile' --file-filter='gsdd* *.gsdd' --filename='%s/'", dirname(tmp));
    doZenity(input, results);

    strcmprs(results);

    if(strlen(results) < 4)
    {
      strcpy(results, env->gfilename);
    }

    subArgs(2, results);
  }

  strcpy(env->gfilename, args->secondArg);

  sendFilenames();

  return(0);
}


int setUFileName()
{
  char results[256], input[256], tmp[512];

  bzero(results, sizeof(results));
  strcpy(tmp, env->userfile);

  if(args->ntokens < 2)
  {
    sprintf(input, "--file-selection --title='Select a Userfile' --filename='%s/'", dirname(tmp));
    doZenity(input, results);

    strcmprs(results);

    if(strlen(results) < 4)
    {
      strcpy(results, env->userfile);
    }

    subArgs(2, results);
  }

  strcpy(env->userfile, args->secondArg);

  if(strlen(env->userfile))
  {
    if(isSet(PF_E_FLAG))
    {
      sprintf(tmp, "rm -f %s", env->userfile);
      system(tmp);
    }

    if(isSet(PF_C_FLAG))
    {
      st_report("Creating User-File: %s", env->userfile);

      lp_new_sdd(env->userfile);
    }
    else
    {
      st_report("Skipping Creation of User-File: %s", env->userfile);
    }
  }
  else
  {
    st_fail("Missing name for userfile");
  }

  return(0);
}


int setSFileName()
{
  char results[256], input[256], tmp[512];
  int dont_erase = 0;

  bzero(results, sizeof(results));
  strcpy(tmp, env->savefile);

  if(args->ntokens < 2)
  {
    sprintf(input, "--file-selection --title='Select a Savefile' --filename='%s/'", dirname(tmp));
    doZenity(input, results);

    strcmprs(results);

    if(strlen(results) < 4)
    {
      strcpy(results, env->savefile);
    }

    subArgs(2, results);

    dont_erase = 1; /* Existing file; don't erase */
  }

  strcpy(env->savefile, args->secondArg);

  if(strlen(env->savefile))
  {
    if( !dont_erase && isSet(PF_E_FLAG))
    {
      if(getYesNoQuit("\nDelete savefile: %s [y/N]", env->savefile) == 1)
      {
        sprintf(tmp, "rm -f %s", env->savefile);

        st_print("%s\n", tmp);
        system(tmp);
      }
      else
      {
        st_fail("Deletion of savefile: %s, aborted", env->savefile);
	return(1);
      }
    }

    if( !dont_erase && isSet(PF_C_FLAG))
    {
      if(getYesNoQuit("\nCreate savefile: %s [y/N]", env->savefile) == 1)
      {
        st_report("Creating Save-File: %s", env->savefile);

        lp_new_sdd(env->savefile);
      }
      else
      {
        st_fail("Creation of savefile: %s, aborted", env->savefile);
	return(1);
      }
    }
    else
    {
      st_report("Opening Save-File: %s", env->savefile);
    }
  }
  else
  {
    st_fail("Missing name for savefile");
    return(1);
  }

  return(0);
}


int setValidate()
{
  if(!strncmp(h0->head.obsmode, "CONTSEQ ", 8))
  {
    validate(SEQUENCE);
  }
  else
  if(!strncmp(h0->head.obsmode, "CONTMAP ", 8))
  {
    validate(CONT_MAP);
  }
  else
  if(!strncmp(h0->head.obsmode, "CONTFIVE", 8))
  {
    validate(QK5);
  }
  else
  if(!strncmp(h0->head.obsmode, "CONTQK5 ", 8))
  {
    validate(QK5);
  }
  else
  if(!strncmp(h0->head.obsmode, "CONTFOC ", 8))
  {
    validate(FOCALIZE);
  }
  else
  if(!strncmp(h0->head.obsmode, "CONTNSFC", 8))
  {
    validate(NS_FOCALIZE);
  }
  else
  if(!strncmp(h0->head.obsmode, "CONTEWFC", 8))
  {
    validate(EW_FOCALIZE);
  }
  else
  if(!strncmp(h0->head.obsmode, "LINEPCAL", 8))
  {
    validate(PS_CAL);
  }
  else
  if(!strncmp(h0->head.obsmode, "LINEBSP ", 8))
  {
    validate(BSP);
  }
  else
  if(!strncmp(h0->head.obsmode, "LINEPS  ", 8))
  {
    validate(PS);
  }
  else
  if(!strncmp(h0->head.obsmode, "LINEFS  ", 8))
  {
    validate(FS);
  }

  return(0);
}



int recallNSave()
{
  char *cp;
  unsigned int l;
  int uninget(char *fname, int nsave, struct HEADER *head, float *data);

  if(strlen(env->savefile))
  {
    cp = env->savefile;
  }
  else
  {
    st_fail("savefile must be set first");
    return(1);
  }

  st_report("Fetching Nsave %d into %s", env->nsave, h0->name);

  if(env->nsave > 0)
  {
    uninget(cp, env->nsave, &h0->head, h0->yplot1);
  }
  else
  {
    return(1);
  }

  env->curScan  = h0->head.scan;
  env->mapangle = h0->head.scanang;

                                /* now check for abnormal float in data array */
  l = (unsigned)h0->head.datalen / sizeof(float);

  lp_checkBounds(h0->yplot1, l, 1);

  h0->ainfo.scanct = h0->head.nostac;
  h0->ainfo.bscan  = h0->head.fscan;
  h0->ainfo.escan  = h0->head.lscan;

  setValidate();

  return(0);
}


int getNSave()
{
  char *cp;
  unsigned int l;
  int uninget(char *fname, int nsave, struct HEADER *head, float *data);

  if(strlen(env->filename))
  {
    cp = env->filename;
  }
  else
  {
    st_fail("filename must be set first");
    return(1);
  }

  st_report("Fetching Nsave %d into %s", env->nsave, h0->name);

  if(env->nsave > 0)
  {
    uninget(cp, env->nsave, &h0->head, h0->yplot1);
  }
  else
  {
    return(1);
  }

  env->curScan  = h0->head.scan;
  env->mapangle = h0->head.scanang;

                                /* now check for abnormal float in data array */
  l = (unsigned)h0->head.datalen / sizeof(float);

  lp_checkBounds(h0->yplot1, l, 1);

  h0->ainfo.scanct = h0->head.nostac;
  h0->ainfo.bscan  = h0->head.fscan;
  h0->ainfo.escan  = h0->head.lscan;

  setValidate();

  return(0);
}


int p_getScan()
{
  freeX();
  freeY();

  getScan(1);

  if(isSet(PF_R_FLAG))
  {
    removeBadChans();
  }

  return(0);
}


int getScan(verbose)
int verbose;
{
  double fscan, ret;

  if(strlen(args->secondArg) == 0)
  {
    st_warn("Missing Scan# for get");
    return(1);
  }

  hzero(h0); /* Clear out the H0 image array */

  last_valid      = 0;
  env->dobaseline = 0;

  if(!strcmp(args->secondArg, "stackscan"))
  {
    fscan = env->stackscan;
  }
  else
  {
    fscan = atof(args->secondArg);
  }

  if(verbose)
  {
    st_report("Fetching Scan %.2f into %s", fscan, h0->name);
  }

  ret = read_scan(fscan, env->filename, h0->yplot1, &h0->head, env->autobad);
  env->baselineremoved = 0;

  if(verbose)
  {
    st_report("read_scan(): Returned: %.2f", ret);
  }

  env->curScan = fscan;
  env->mapangle = h0->head.scanang;

  if(ret > 0.0)
  {
    setValidate();

    return(0);
  }

  return(1);
}


int ggetScan(buf)
char *buf;
{
  double fscan, ret;

  if(strlen(args->secondArg) == 0)
  {
    st_warn("Missing Scan# for get");
    return(1);
  }

  if(strlen(env->gfilename) == 0)
  {
    st_warn("No Gains File Specified");
    return(1);
  }

  hzero(h0); /* Clear out the H0 image array */

  last_valid      = 0;
  env->dobaseline = 0;

  fscan = atof(args->secondArg);

  if(buf)
  {
    st_report("Fetching %s Array %.2f into %s", buf, fscan, h0->name);
  }
  else
  {
    st_report("Fetching Gain Scan %.2f into %s", fscan, h0->name);
  }

  ret = read_scan(fscan, env->gfilename, h0->yplot1, &h0->head, 0);
  env->baselineremoved = 0;
  env->curScan = fscan;

  st_report("ggetScan() returned %f", ret);

  if(ret > 0.0)
  {
    setValidate();

    return(0);
  }

  return(1);
}



int doFver()
{
  int newver;
  char tbuf[256], fbuf[512];

  if(strlen(args->secondArg) == 0)
  {
    findVersions(DATA_FILE, "");

    if(doingOTF)
    {
      gsetFile();
    }

    return(0);
  }

  newver = atoi(args->secondArg);

  strcpy(tbuf, env->filename);

  tbuf[strlen(tbuf)-3] = '\0';  /* Truncate the versin number */

  sprintf(fbuf, "%s%03d", tbuf, newver);

  st_report("New file version: %s", fbuf);

  strcpy(env->filename, fbuf);

//  sendFilenames(); 

  if(doingOTF)
  {
    gsetFile();
  }

  return( reloadFile() );
}


int doGver()
{
  int newver;
  char tbuf[256], fbuf[512];

  if(strlen(args->secondArg) == 0)
  {
    findVersions(GAINS_FILE, "");
    return(1);
  }

  if(strlen(env->gfilename) < 4)
  {
    st_fail("Gains Filename NOT set");

    return(1);
  }

  newver = atoi(args->secondArg);

  strcpy(tbuf, env->gfilename);

  tbuf[strlen(tbuf)-3] = '\0';  /* Truncate the versin number */

  sprintf(fbuf, "%s%03d", tbuf, newver);

  st_print("New Gains file version: %s\n", fbuf);

  strcpy(env->gfilename, fbuf);

  return(0);
}


/**
 * Set the Gains file to match the data filename
 */
int gsetFile()
{
  char dir[256], base[256], tbuf[1024], tmp[256];

  strcpy(tmp, env->filename);
  strcpy(dir,  dirname(tmp));

  strcpy(tmp, env->filename);
  strcpy(base, basename(tmp));

  sprintf(tbuf, "%s/g%s", dir, base);

  st_report("Dir:  %s", dir);
  st_report("Base: %s", base);

  st_report("New Gains file name: %s", tbuf);

  strcpy(env->gfilename, tbuf);

  sendFilenames();

  return(0);
}


int getTerminalSize(cols, rows)
int *cols, *rows;
{
  struct winsize w;

  ioctl(0, TIOCGWINSZ, &w);

  *cols = w.ws_col;
  *rows = w.ws_row;

  return(0);
}


int showFiles()
{
  char sysBuf[256];

  st_report("Data  file:  %s", env->filename);
  st_report("Gains file:  %s", env->gfilename);
  st_report("Log   file:  %s", logfilename);
  st_report("Lock  file:  %s", lockFileName);

  sprintf(sysBuf, "ls -l /tmp/%s/*", session);

  system(sysBuf);

  return(0);
}


int findSequences()
{
  findVersions(DATA_FILE, "seq");

  return(0);
}


int findVersions(which, seq)
int which;
char *seq;
{
  FILE *fp;
  char *string, line[256];
  static char cmdBuf[4*4096];

  sprintf(cmdBuf, "findVersions %s %s", env->filename, seq);

  if( (fp = popen(cmdBuf, "r") ) <= (FILE *)0 )
  {
    st_fail("findVersions(): Unable to open %s", cmdBuf);

    return(1);
  }

  bzero(line, sizeof(line));

  while((string = fgets(line, sizeof(line), fp)) != NULL)
  {
    deNewLineIt(line);

    strcmprs(line);

    if(!strncmp(line, "LOADING", 7))
    {
      printf(".");
      fflush(stdout);
    }
    else
    if(!strncmp(line, "DONE", 4))
    {
      printf("\n");
      fflush(stdout);
    }
    else
    if(strstr(line, "sdd"))
    {
      st_report("findVersions() returned: '%s'", line);

      if(which == DATA_FILE)
      {
        strcpy(env->filename, line);
      }
      else
      if(which == GAINS_FILE)
      {
        strcpy(env->gfilename, line);
      }

      if(reloadFile())
      {
        return(1);
      }
    }
    else
    {
      printf("findVersions(): %s\n", line);
    }
  }

  pclose(fp);

  return(0);
}


int findVers()
{
  findVersions(DATA_FILE, "");

  if(doingOTF)
  {
    gsetFile();
  }

  return(0);
}


int saveNSave()
{
  int uninsave(char *fname, int nsave, int overwrite, struct HEADER *head, float *data);

  if(env->nsave > 0)
  {
    if(strlen(env->savefile))
    {
      st_report("Saving Image to nsave=%d, in %s", env->nsave, env->savefile);

      if(h0->head.noint > 0)
      {
        h0->head.align3[0] = env->nsave; /* Save nsave in header */

        uninsave(env->savefile, env->nsave, env->overwrite, &h0->head, (float *)&h0->yplot1[0]);
      }
      else
      {
        st_fail("Image Holder: %s, appears to be empty", h0->name);
      }
    }
    else
    {
      st_fail("savefile must be set first");
    }
  }
  else
  {
    st_fail("nsave must be > 0");
  }

  return(0);
}


/* Ingest, Convert and possibly Export old version of SDD files */
int doSDD()
{
  char sysBuf[512];

  st_report("Entering doSDD()");

  if(args->ntokens < 3)
  {
    printH_FlagHelp("sdd");
  }

  if(isSet(PF_C_FLAG))
  {
    sprintf(sysBuf, "convert2intel %s %s", args->secondArg, args->thirdArg);

    system(sysBuf);

    if(isSet(PF_L_FLAG)) /* Use this new file? */
    {
      sprintf(sysBuf, "file %s", args->thirdArg);

      parseCmd(sysBuf);
    }

    return(0);
  }

  return(0);
}

/* Ingest, Convert and possibly Export old version of GILDAS Class files */
int doClass()
{
  char sysBuf[512], cfile[256], cname[128];

  st_report("Entering doClass(%d)", args->ntokens);

  if(isSet(PF_C_FLAG))
  {
    sprintf(sysBuf, "class2sdd %s %s", args->secondArg, args->thirdArg);

    system(sysBuf);

    if(isSet(PF_L_FLAG)) /* Use this new file? */
    {
      sprintf(sysBuf, "file %s", args->thirdArg);

      parseCmd(sysBuf);
    }

    return(0);
  }
  else
  if(isSet(PF_S_FLAG)) /* Save the H0 to a file */
  {
    if(strlen(args->secondArg))
    {
      strcpy(cfile, args->secondArg);

      if(isSet(PF_I_FLAG))
      {
	sprintf(sysBuf, "rm -i %s", cfile);
        system(sysBuf);
      }

      if(class_init(cfile))
      {
        strncpy(cname, h0->head.backend, 8);
        cname[8] = '\0';

        class_write(cfile, &h0->head, &h0->yplot1[0], cname, 1);

        return(0);
      }
    }
    else
    {
      st_fail("doClass(): No filename passed");
      printH_FlagHelp("class");

      return(1);
    }
  }
  else
  {
    printH_FlagHelp("class");
  }

  return(0);
}


/* Ingest, Convert and possibly Export old version of PDFL files */
int doPDFL()
{
  st_report("Entering doPDFL()");

  return(0);
}



int listSaves()
{
  char *cp;
  int unilistsave_(char *fname);

  if(strlen(env->savefile))
  {
    cp = env->savefile;
  }
  else
  {
    st_fail("savefile must be set first");
    return(1);
  }

  st_report("Fetching Nsave %d into %s", env->nsave, h0->name);

  unilistsave_(cp);
  
  return(0);
}


int doUpdate()
{
  char *cp, sysBuf[512], gsysBuf[512];
  char  host[256], initials[80], fname[256], gname[256];

  if(args->ntokens < 2)
  {
    printH_FlagHelp(args->firstArg);

    return(1);
  }

  cp = getenv("HOST");

  if(cp)
  {
    if(strstr(cp, "modelo") || strstr(cp, "smtoast") || strstr(cp, "unipops"))
    {
      st_report("Update not available Mountain computer: %s", cp);
      return(1);
    }
  }
  else
  {
    st_report("Update not available from Mountain computers");
    return(1);
  }

  st_report("Proceeding on host: %s", cp);

  cp = strchr(env->filename, '.');

  if(cp)
  {
    cp++;

    strncpy(initials, cp, 3);

    initials[3] = '\0';

  }
  else
  {
    st_report("Can't locate initials");
    return(0);
  }

  strcpy(fname, basename(env->filename));

  if(!strcmp(args->secondArg, "12m"))
  {
    sprintf(host, "modelo");
  }
  else
  if(!strcmp(args->secondArg, "smt"))
  {
    sprintf(host, "smtoast");
  }
  else
  {
    printH_FlagHelp(args->firstArg);

    return(1);
  }

  sprintf( sysBuf, "scp %s:/home/data/%s/%s %s", host, initials, fname, env->filename);

  if(isSet(PF_G_FLAG))
  {
    if((env->mode == MODE_SPEC || env->mode == MODE_OTF))
    {
      strcpy(gname, basename(env->gfilename));
      sprintf(gsysBuf, "scp %s:/home/data/%s/%s %s", host, initials, gname, env->gfilename);
    }
  }

  if(isSet(PF_F_FLAG))
  {
    st_report("system(%s);", sysBuf);

    system(sysBuf);

    if(isSet(PF_G_FLAG))
    {
      st_report("system(%s);", gsysBuf);

      system(gsysBuf);
    }
  }
  else
  {
    st_report("system(%s);", sysBuf);

    if(isSet(PF_G_FLAG))
    {
      st_report("system(%s);", gsysBuf);
    }
  }

  return(0);
}

