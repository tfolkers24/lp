/* 

Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

*/

#include "extern.h"

#include "ldd.h"

#define MAX_DATA_CHANNELS  8192
#include "ds_image_info.h"

struct DS_IMAGE_INFO ds_image_info;
struct GLOBAL_MEMORY *CGM;

tcflag_t old_lflag;
cc_t     old_vtime;
struct termios term;

FILE               *fp, *errPrint_fp = NULL;
struct ENVIRONMENT *env;
struct HEADER      *sa  = NULL;
struct XGRAPHIC_SEND xsend;

struct SOCK *helper, *monitorSock, *xgraphic, *dataserv;

int logging                 = 0;
int last_valid              = 0;
int onlineversion           = 0;
int verboseLevel            = 0;
int doNotRemoveLockFile     = 0;
int xfd                     = 0;
int hfd                     = 0;
int dfd                     = 0;
int printNsave              = 0;
int site                    = 0;
int lp_logging              = 0;
int lp_ppid                 = 0;
int history_already_written = 0;
int no_terminal             = 0;

char savedCaptureName[256];


/**
  Set the Verbose level of an interactive session.
  \code
  Possible levels:
  Levels: 1 Verbose for Parsing  Info
          2 Verbose for Auto Tau  Calculations
          4 Verbose for Critiria  Slection
          8 Verbose for Xgraphic  Activity
         16 Verbose for FBOFFSET  Routines
         32 Verbose for BASELINE  Routines
         64 Verbose for STATS     Routines
        128 Verbose for RECURSIVE Info
        256 Verbose for Batchfile Info
        512 Verbose for Alias     Info
       1024 Verbose for FileOps   Info
       2048 Verbose for Grigmap   Routines
       4096 Verbose for Flag      Checking
       8192 Verbose for Atm       Routines
      16384 Verbose for Stack     Routines
  \endcode
  \return 0
*/
int setVerbose()
{
  int v;

  if(strlen(args->secondArg))
  {
    v = atoi(args->secondArg);

    if(v >= 0 && v <= MAX_VERBOSE)
    {
      st_report("Setting Verbose Level to %d", v);

      verboseLevel = v;
    }
    else
    {
      st_fail("Verbose Level %d, out of Range: 0 - %d", v, MAX_VERBOSE);
    }
  }
  else
  {
    st_report("Current Verbose Level: %d", verboseLevel);
    st_report("Suggested Levels: 1 Verbose for Parsing  Info");
    st_report("                  2 Verbose for Auto Tau  Calculations");
    st_report("                  4 Verbose for Critiria  Slection");
    st_report("                  8 Verbose for Xgraphic  Activity");
    st_report("                 16 Verbose for FBOFFSET  Routines");
    st_report("                 32 Verbose for BASELINE  Routines");
    st_report("                 64 Verbose for STATS     Routines");
    st_report("                128 Verbose for RECURSIVE Info");
    st_report("                256 Verbose for Batchfile Info");
    st_report("                512 Verbose for Alias     Info");
    st_report("               1024 Verbose for Fileops   Info");
    st_report("               2048 Verbose for Gridmap   Routines");
    st_report("               4096 Verbose for Flag      Checking");
    st_report("               8192 Verbose for Atm       Routines");
    st_report("              16384 Verbose for Stack     Routines");
  }

  return(0);
}


/**
   Need to make this better
   \param which last plotting mode
   \return 0
 */
int validate(which)
int which;
{
  last_valid = which;

  return(0);
}


/**
  replot the last plot

  \param how i.e gui or postscript
  \return 0
 */
int replot(how)
int how;
{
  switch(last_valid)
  {
    case 0:
	if(notSet(PF_I_FLAG))
        {
	  st_fail("No scans Loaded Yet");
	}
	break;
    case CONTMAP:     doMapPlot(how);                              break;
    case CONT_MAP:    cont_display_mapscan( h0, how);              break;
    case SEQUENCE:    cont_display_sequence(h0, how);              break;
    case QK5:        /*makeScanPlot(); */                          break;
    case FOCALIZE:    cont_display_any_focus(h0, how, last_valid); break;
    case EW_FOCALIZE: cont_display_any_focus(h0, how, last_valid); break;
    case NS_FOCALIZE: cont_display_any_focus(h0, how, last_valid); break;
    case SP_TIP:     /*makeScanPlot(); */                          break;
    case VLBI:       /*makeScanPlot(); */                          break;

    case PS:        
    case BSP:        
	linePlot(h0);
	break;

    case ACCUM_SCANS_MODE:
	sendAccumResults();
	break;

    default:
//	st_warn("Strange Last Plot: %d", last_valid);
	break;
  }

  return(0);
}


int doPaperSize()
{
  double w, h;

  if(args->ntokens == 2) /* "*" or "default" */
  {
    if(!strncmp(args->secondArg, "*", strlen(args->secondArg)))
    {
      env->paperw = 10.0;
      env->paperh =  7.5;
    }
    else
    if(!strncmp(args->secondArg, "0", strlen(args->secondArg)))
    {
      env->paperw = 0.0;
      env->paperh = 0.0;
    }
    else
    {
      printH_FlagHelp("paper");

      return(1);
    }
  }
  else
  if(args->ntokens == 3) /* width height */
  {
    w = atof(args->secondArg);
    h = atof(args->thirdArg);

    if(w > 0.0 && h > 0.0)
    {
      env->paperw = w;
      env->paperh = h;
    }
    else
    {
      printH_FlagHelp("paper");

      return(1);
    }
  }
  else
  {
    printH_FlagHelp("paper");

    return(1);
  }

  st_report("Setting Plotter Paper Size to %.2f\" x %.2f\"", env->paperw, env->paperh);

  return(0);
}


int makeFileName(fname, pid)
char *fname;
int pid;
{
  char obj[256], modeStr[256];

      switch(last_plot)
      {
	  case LAST_PLOT_OTF_CUT:
	    sprintf(obj, "%16.16s", h0->head.object);
	    strcmprs(obj);
            sprintf(fname, "OTF-%s-%.2f-%.2f", obj, h0->ainfo.bscan, h0->ainfo.escan);

	    break;
	  case LAST_PLOT_SPEC_ACCUM:
            sprintf(fname, "CF-%.2f-%.2f_%04dKHz_%d", 
			    h0->ainfo.bscan, 
			    h0->ainfo.escan, 
			    (int)fabs(h0->head.freqres*1000.0), 
			    (int)h0->head.noint);
	    break;

	  case LAST_PLOT_SPEC_FFT:
            sprintf(fname, "FFT-%.2f-%.2f_%04dKHz_%d", 
			    h0->ainfo.bscan, 
			    h0->ainfo.escan, 
			    (int)fabs(h0->head.freqres*1000.0), 
			    (int)h0->head.noint);
	    break;

	  case LAST_PLOT_SPEC_HISTO:
            sprintf(fname, "HISTO-%.2f-%.2f_%04dKHz_%d", 
			    h0->ainfo.bscan, 
			    h0->ainfo.escan, 
			    (int)fabs(h0->head.freqres*1000.0), 
			    (int)h0->head.noint);
	    break;

	  case LAST_PLOT_CONT_ACCUM:
	    sprintf(obj, "%16.16s", accum_results_send.head[0].object);
	    strcmprs(obj);
            sprintf(fname, "CONT_ACCUM-%.2f-%.2f-%s", accum_results_send.bscan, 
						          accum_results_send.escan,
						          obj);
	    break;

	  case LAST_PLOT_CONT_PLAIN:
	    sprintf(obj, "%16.16s", cont_plain_send.head.object);
	    strcmprs(obj);

            strncpy(modeStr, cont_plain_send.head.obsmode+4, 4);
	    modeStr[4] = '\0';
	    strcmprs(modeStr);

            sprintf(fname, "%s-%.2f-%s", modeStr, cont_plain_send.head.scan, obj);

	    break;

	  case LAST_PLOT_CONT_SPTIP:
            sprintf(fname, "SPTIP-%.2f-%.2f", cont_sptip_send.head1.scan, 
						  cont_sptip_send.head2.scan);
	    break;

	  case LAST_PLOT_CONT_OBJECT:
            sprintf(fname, "OBJECT-%.2f-%.2f-%s", 
		OGM->objects[OGM->nobjIndx].measurements[0].bscan, 
		OGM->objects[OGM->nobjIndx].measurements[OGM->objects[OGM->nobjIndx].numMeas-1].escan,
		OGM->objects[OGM->nobjIndx].source);
	    break;

	  case LAST_PLOT_FLUX_INFO:
            sprintf(fname, "FLUX-INFO-%.2f-%.2f-%s", flux_info.bfreq, 
							 flux_info.efreq, 
							 flux_info.name);
	    break;

	  case LAST_PLOT_FLUX_HISTORY:
            sprintf(fname, "FLUX-HISTORY-%s", flux_history.name);
		break;

	  default:
            sprintf(fname, "CaptureFile-%d", pid);
		break;
      }

  return(0);
}


int suggestFileName()
{
  int  pid;
  char fname[256];

  pid = rand();
  pid = pid%32767;

  makeFileName(fname, pid);

  st_report("Suggested Filename: %s", fname);

  return(0);
}




/**
  Make a hardcopy of the current plot \n
  possible params: 'capture' for a PDF file
  \return 0
 */
int printPlot()
{
  int         pid, bpaper = 0, dops = 0, jpg=0;
  char        fname_out[600], fname[512], jname[600], pname[256], *prt;
  double      old_pw, old_ph;
  static char sysbuf[2048];

  if(h0->head.scan == 0.0)
  {
    st_fail("printPlot(): No privious scan loaded");
    return(1);
  }

  prt = printer;

  if(isSet(PF_D_FLAG))
  {
    old_pw = env->paperw;
    old_ph = env->paperh;

    env->paperw = 10.7;
    env->paperh =  7.5;
  }

  if(isSet(PF_X_FLAG))
  {
    xx();  /* Force a replot */
  }

  if(isSet(PF_B_FLAG))
  {
    bpaper = 1;
  }

  if(isSet(PF_P_FLAG) && notSet(PF_J_FLAG))
  {
    dops = 1;
    jpg = 0;
  }
  else
  if(notSet(PF_P_FLAG) && isSet(PF_J_FLAG))
  {
    jpg = 1;
    dops = 0;
  }
  else
  if(isSet(PF_P_FLAG) && isSet(PF_J_FLAG))
  {
    st_fail("printPlot(): /j flag AND /p flag not allowed");

    if(isSet(PF_D_FLAG))
    {
      env->paperw = old_pw;
      env->paperh = old_ph;
    }

    return(1);
  }

  if(xgraphic)
  {
    sock_send(xgraphic, "make");

    sleep(1); /* Give xgraphic time to plot */

    if(isSet(PF_C_FLAG))
    {
      pid = rand();
      pid = pid%32767;

      if(h0->ainfo.bscan == 0.0 || h0->ainfo.escan == 0.0)
      {
        h0->ainfo.bscan = h0->head.scan;
        h0->ainfo.escan = h0->head.scan;
      }

      makeFileName(fname, pid);

      sprintf(pname, "%s.png", fname);		/* Just in case */

      strcpy(savedCaptureName, fname);

      if(!dops)
      {
        sprintf(fname_out, "%s.pdf", fname);
        st_report("printPlot(): Converting plotfile /tmp/%s/ccal_plot.ps to %s", session, fname_out);
        sprintf(sysbuf, "cat /tmp/%s/ccal_plot.ps | /usr/bin/epstopdf -f --outfile=%s", session, fname_out);
      }
      else
      {
        sprintf(fname_out, "%s.ps", fname);
        st_report("printPlot(): Coping plotfile /tmp/%s/ccal_plot.ps to %s", session, fname_out);
        sprintf(sysbuf, "mv /tmp/%s/ccal_plot.ps %s", session, fname_out);
      }

      if(verboseLevel & VERBOSE_PARSE)
      {
        st_report("printPlot(): Executing Command: %s", sysbuf);
      }

      system(sysbuf);

      if(jpg)
      {
        sprintf(jname, "%s.jpg", fname);
        st_report("printPlot(): Converting plotfile %s to %s", fname_out, jname);
        sprintf(sysbuf, "/usr/bin/pdftoppm -jpeg %s > /tmp/tmp.jpg", fname_out);
        system(sysbuf);

        sprintf(sysbuf, "cp /tmp/tmp.jpg %s", jname);
        system(sysbuf);

        if(isSet(PF_D_FLAG)) /* Prepare for doxygen publication */
        {
	  st_report("printPlot(): Converting image for Publication");
          sprintf(sysbuf, "/usr/bin/convert -resize 1280x1024 -rotate 90 /tmp/tmp.jpg %s", pname);
        }
        else
        {
          sprintf(sysbuf, "/usr/bin/convert -rotate 90 /tmp/tmp.jpg %s", pname);
        }
        system(sysbuf);
      }

      if(!jpg && isSet(PF_G_FLAG))
      {
        sprintf(pname, "%s.png", fname);
        st_report("printPlot(): Converting plotfile %s to %s", fname_out, pname);
        sprintf(sysbuf, "/usr/bin/pdftoppm -png %s > /tmp/tmp.png", fname_out);
        system(sysbuf);

        if(isSet(PF_D_FLAG)) /* Prepare for doxygen publication */
        {
	  st_report("printPlot(): Converting image for Publication");
          sprintf(sysbuf, "/usr/bin/convert -resize 1280x1024 -rotate 90 /tmp/tmp.png %s", pname);
        }
	else
	{
          sprintf(sysbuf, "/usr/bin/convert -rotate 90 /tmp/tmp.png %s", pname);
	}

        system(sysbuf);
      }

      if(isSet(PF_V_FLAG))
      {
        sprintf(sysbuf, "evince %s 1>> /dev/null 2>&1 &", fname_out);
        system(sysbuf);
      }

      if(args->ntokens > 1)
      {
        if(strlen(args->secondArg))
	{
          st_report("printPlot(): Moving Capture file: %s to %s", fname_out, args->secondArg);

	  if(strstr(args->secondArg, "png"))
	  {
            sprintf(sysbuf, "mv %s %s", pname, args->secondArg);
	  }
	  else
	  {
            sprintf(sysbuf, "mv %s %s", fname_out, args->secondArg);
	  }

          system(sysbuf);
        }
      }

      if(isSet(PF_D_FLAG))
      {
        env->paperw = old_pw;
        env->paperh = old_ph;
      }

      return(0);
    }
    else /* Assume any args are a printer definition */
    {
      if(strlen(args->secondArg))
      {
        prt = args->secondArg;
      }
    }

    st_report("printPlot(): Printing file /tmp/%s/ccal_plot.ps to %s", session, prt);

    if(bpaper)
    {
      sprintf(sysbuf, "lpr -o media=11x17 -o fitplot /tmp/%s/ccal_plot.ps", session);
    }
    else
    {
      sprintf(sysbuf, "lpr -P%s /tmp/%s/ccal_plot.ps", prt, session);
    }

    if(verboseLevel & VERBOSE_PARSE)
    {
      st_report("printPlot(): Executing Command: %s", sysbuf);
    }

    system(sysbuf);
  }

  sleep(1);

  if(isSet(PF_R_FLAG))
  {
    refreshPlot();
  }

  if(isSet(PF_D_FLAG))
  {
    env->paperw = old_pw;
    env->paperh = old_ph;
  }

  return(0);
}


/**
  Shortcut to replot
  \return 0
 */
int xx()
{
  replot(GNUPLOT);

  return(0);
}


/**
  Display all the scans in the data file
  \return
 */
int tellOndata()
{
  int i;
  struct HEADER *p;

  p = &sa[0];
  for(i=0;i<env->numofscans;i++,p++)
  {
    if(!(i%7))
    {
      st_print("\n%4d: ", i+1);
    }
    st_print("%8.2f", p->scan);
  }

  st_print("\n");

  return(0);
}


/**
  Perform an arbitrary system command
  \return results from 'system' command
 */
int doSysCmd()
{
  int ret;
  char sysbuf[256];

  /* ignore the 'system' keyword in the firstArg */
  sprintf(sysbuf, "%s %s %s %s %s", args->secondArg, 
				    args->thirdArg, args->fourthArg, 
				    args->fifthArg, args->sixthArg);

  if(verboseLevel & VERBOSE_PARSE)
  {
    st_report("Executing Command: %s", sysbuf);
  }

  ret = system(sysbuf);

  return(ret);
}



/**
  Exit the program under user control
  \return 0
 */
int quitProg()
{
  char cmdBuf[256];

  if(isSet(PF_Y_FLAG))
  {
    st_report("Exiting Program");

    exit(0);
  }

  if(getYesNoQuit("Exit Program: y/N ?"))
  {
    if(doingOTF)
    {
      st_report("Killing any Gridder Processes belonging");
      sprintf(cmdBuf, "%s/bin/start_stop_gridders stop", pkg_home);
      system(cmdBuf);

      sock_send(dataserv, "q");

      doingOTF = 0;
    }

    if(!history_already_written)
    {
      rwHistory(WRITE_HISTORY);

  /* Save any predefined formulas */
      sprintf(cmdBuf, "formula /w .lp.formulas");
      parseCmd(cmdBuf);

      history_already_written = 1;
    }

    st_report("Exiting Program");

    exit(0);
  }

  return(0);
}


/**
  No longer used function
  \returns 0
 */
int plotAccum()
{
  st_warn("plotAccum() Not implemented yet");

  return(0);
}


/**
  Print a one line recap of the selected scan
  \param p struct HEADER::
  \param nsave flag to use nsave vs scanno;
  \return 0
 */
int printScan(p, nsave)
struct HEADER *p;
int nsave;
{
  int ns, feed, oc = -1;
  double ttime, freq, tsys, tcal, scan_or_nsave;
  double xoff, yoff, roff, doff, az;
  char sbname[80], backend[80];

  if(env->mode == MODE_CONT)
  {
    freq = restFreq(p) / 1000.0;
    ns = (int)(p->noint / p->nophase);
    ttime = p->noint * p->samprat;
  }
  else
  if(env->mode == MODE_SPEC)
  {
    freq = p->restfreq / 1000.0;
    ns = (int)(p->inttime / p->samprat);
    ttime = p->samprat;
  }
  else
  if(env->mode == MODE_OTF)
  {
    freq = p->restfreq / 1000.0;
    ns = (int)(p->inttime / p->samprat);
    ttime = p->inttime;
  }

//      0        1        2        3
// { "H-LSB", "H-USB", "V-LSB", "V-USB" };
  if(env->issmt)				/* SMT */
  {
    feed = 0;
 						/* Is it a continuum scan; determine SB from scan-num */
    if(strstr(p->backend, "DIGITAL") || strstr(p->backend, "D4G"))
    {
      feed = getFeed(p->scan) -1;
    }
    else
    {
      feed = getFeedFromBackend(p->backend);
    }

    strcpy(sbname, mg_sideband[feed]);
  }
  else						/* Kitt Peak */
  {
 						/* Is it a continuum scan; determine SB from scan-num */
    if(strstr(p->backend, "DIGITAL") || strstr(p->backend, "D4G"))
    {
      if(p->utdate < 2018.5)			/* Date we went to 4 IF's @ KP */
      {
        strcpy(sbname, old_kp_sideband[(int)p->sideband]);
      }
      else
      {
        feed = getFeed(p->scan);
        feed--;

        strcpy(sbname, kp_sideband[feed]);
      }
    }
    else
    {
      feed = getFeedFromBackend(p->backend);
      strcpy(sbname, kp_sideband[feed]);
    }
  }

  if(manual_tcal > 0.0 && fabs(p->tcal-400.0) < 1.0)
  {
    tsys = p->stsys * manual_tcal / p->tcal;
    tcal = manual_tcal;
  }
  else
  {
    tsys = p->stsys;
    tcal = p->tcal;
  }

  strncpy(backend, p->backend, 8);
  backend[8] = '\0';
  strcmprs(backend);

  if(nsave)
  {
    scan_or_nsave = p->align3[0]+1;
  }
  else
  {
    scan_or_nsave = p->scan;
  }

//  st_print("printScan(): nsave = %d, scan_or_nsave = %.2f\n", nsave, scan_or_nsave);

  if(p->openpar[5] == 5555.5)
  {
    oc = oldColor;
    setColor(ANSI_GREEN);
  }

  if(p->openpar[8] == 9999.5 || p->openpar[8] == 9998.5)
  {
    oc = oldColor;
    setColor(ANSI_MAGENTA);
  }

  if(p->az < 0.0)
  {
    az = p->az + 360.0;
  }
  else
  if(p->az > 360.0)
  {
    az = p->az - 360.0;
  }
  else
  {
    az = p->az;
  }

  st_print(" %10.10s %10.6f %10.4f %6.2f %8.2f %5d  %4.0f %5.1f %5.1f %8.8s  %5.5s  %8.8s %4.0f  %4.0f ", 
	p->object, freq, p->utdate, p->ut, scan_or_nsave, ns, ttime, az, p->el, p->obsmode, sbname,
        backend, tsys, tcal);

  if(!strncmp(p->obsmode, "CONTMAP", 7))
  {
    st_print(" %3.0f  %3.0f  %3.0f  %3.0f  %3.0f %6.2f", 
			p->noxpts, p->noypts, 
			p->xcell0, p->ycell0, 
			p->scanang, p->focusr);
  }
  else
  if(!strncmp(p->obsmode, "LINEOTF", 7))
  {
    st_print(" %5.2f  %2d    %2d    %2d    %2d", fabs(p->freqres), 
			(int)p->xcell0, (int)p->noxpts, 
			(int)p->ycell0, (int)p->noypts);
  }
  else
  if(!strncmp(p->obsmode, "LINE", 4))
  {
    xoff = p->az_offset * 3600.0;
    yoff = p->el_offset * 3600.0;

    roff = (p->epocra  - p->xsource) * 3600.0 * cos(p->epocdec*CTR);
    doff = (p->epocdec - p->ysource) * 3600.0;

    if(roff || doff)
    {
      st_print(" %5.2f  %5.1f  %5.1f", fabs(p->freqres), roff, doff);
    }
    else
    if(xoff || yoff)
    {
      st_print(" %5.2f  %5.1f  %5.1f", fabs(p->freqres), xoff, yoff);
    }
    else
    {
      st_print(" %5.2f  %5.1f  %5.1f", fabs(p->freqres), 0.0, 0.0);
    }
  }

  if(oc != -1)
  {
    setColor(oc);
  }

  st_print("\n");

  return(0);
}



/**
  Print listing of all scans in the datafile
  \returns 0
 */
int tellAll()
{
  int i, j;
  char source[32], backend[32], mode[32];
  struct HEADER *p;

  p = &sa[0];
  j = 1;

  st_print("%s\n", listHeader);

  for(i=0;i<env->numofscans;i++,p++)
  {
    if(!(j%24))
    {
      st_print("%s\n", listHeader);
    }

    if(args->ntokens > 1)
    {
      sprintf(source,  "%16.16s", p->object);
      sprintf(backend, "%8.8s",   p->backend);
      sprintf(mode,    "%8.8s",   p->obsmode);

      if(strcasestr(source,  args->secondArg) || 
	 strcasestr(backend, args->secondArg) || 
	 strcasestr(mode,    args->secondArg))
      {
        printScan(p, printNsave);
      }
      else
      {
        continue;
      }
    }
    else
    {
      printScan(p, printNsave);
    }

    j++;
  }

  st_warn("Found %d Scans Matching Criteria", j-1);

  return(0);
}


/**
  Print listing of all Spec Focus scans in the datafile
  \returns 0
 */
int tellAllSfocus()
{
  int i, j;
  struct HEADER *p;

  p = &sa[0];
  j = 1;

  st_print("%s\n", listHeader);

  for(i=0;i<env->numofscans;i++,p++)
  {
    if(!(j%24))
    {
      st_print("%s\n", listHeader);
    }

    if(p->openpar[5] == 5555.5)
    {
      printScan(p, printNsave);

      j++;
    }
  }

  st_warn("Found %d Scans Matching Criteria", j-1);

  return(0);
}


/**
  Print listing of all Spec Five scans in the datafile
  \returns 0
 */
int tellAllSpec5()
{
  int i, j;
  struct HEADER *p;

  p = &sa[0];
  j = 1;

  st_print("%s\n", listHeader);

  for(i=0;i<env->numofscans;i++,p++)
  {
    if(!(j%24))
    {
      st_print("%s\n", listHeader);
    }

    if(p->openpar[8] == 9999.5 || p->openpar[8] == 9998.5)
    {
      printScan(p, printNsave);

      j++;
    }
  }

  st_warn("Found %d Scans Matching Criteria", j-1);

  return(0);
}



/**
  Check to see if passed header backend matches the criteria
  \param h struct HEADER::
  \returns 1 if it's a match
  \returns 0 otherwise 
 */
int backendMatch(h)
struct HEADER *h;
{
  int ret, ok = 0;
  char backend[32];

  switch(env->bkmatch)
  {
    case BKMATCH_FEED:
      if(env->if_feed == 0.0)
      {
	return(1);
      }
      else
      if(lp_fcmp((h->scan-(int)h->scan), env->if_feed, 0.0001))
      {
        return(0);
      }
      else
      {
        return(1);
      }
      break;

    case BKMATCH_RESOLUTION:
      if(env->bkres == 0.0)
      {
        return(1);
      }
      else
      if(lp_fcmp(fabs(h->freqres), env->bkres, 0.01))
      {
        return(0);
      }
      else
      {
        return(1);
      }
      break;

    case BKMATCH_NAME:
      strncpy(backend, h->backend, 8);
      backend[8] = '\0';
      strcmprs(backend);

      ret = fnmatch(env->backend, backend, FNM_CASEFOLD);

      return(ret == 0);

      break;

    case BKMATCH_NAMERES:
	/* Fist check the name */
      strncpy(backend, h->backend, 8);
      backend[8] = '\0';
      strcmprs(backend);

	/* Return 0 if a match else 1 */
      ret = fnmatch(env->backend, backend, FNM_CASEFOLD);

      /* Now check resolution */
      if(env->bkres == 0.0)
      {
        ok = 1;
      }
      else
      {
        ok = lp_fcmp(fabs(h->freqres), env->bkres, 0.01); /* Match = 0 */
      }

      if(!ret && !ok)
      {
        return(1);
      }
      else
      {
        return(0);
      }

      break;
  }

  return(1);
}


#define BAD_NEG   0x0001
#define BAD_NAN   0x0002
#define BAD_INF   0x0004
#define BAD_LIMIT 0x0008

/* Test the header for various bogus values */
int doAutoBad()
{
  int    i, cbad, sbad;
  char   tbuf[256];
  struct HEADER *p;

  if(notSet(PF_C_FLAG) && notSet(PF_S_FLAG))
  {
    printH_FlagHelp("autobad");
    return(0);
  }

  p = &sa[0];
  for(i=0;i<env->numofscans;i++,p++)
  {
    cbad = 0;
    sbad = 0;

    if(isSet(PF_S_FLAG))
    {
      if(p->stsys < 0.0)
      {
        sbad |= BAD_NEG;
      }

      if(isnan(p->stsys))
      {
        sbad |= BAD_NAN;
      }

      if(isinf(p->stsys))
      {
        sbad |= BAD_INF;
      }

      if(p->stsys > env->tsyslimit)
      {
        sbad |= BAD_LIMIT;
      }
    }

    if(isSet(PF_C_FLAG))
    {
      if(p->tcal < 0.0)
      {
        cbad |= BAD_NEG;
      }

      if(isnan(p->tcal))
      {
        cbad |= BAD_NAN;
      }

      if(isinf(p->tcal))
      {
        cbad |= BAD_INF;
      }

      if(p->tcal > env->tcallimit)
      {
        cbad |= BAD_LIMIT;
      }
    }

    if(sbad || cbad)
    {
      if(!isScanIgnored(p->scan))
      {
        st_report("doAutoBad(): Auto ignoring scan: %8.2f, tsys: 0x%02x, tcal: 0x%02x", p->scan, sbad, cbad);

        sprintf(tbuf, "ignore %8.2f", p->scan);
        parseCmd(tbuf);
      }
    }
  }

  return(0);
}


/**
  List all scans that match the search criteria
  \returns 0 if all OK
  \returns 1 if there is a problem
 */
int listScans()
{
  int i, len, n=0, tip=0, feed;
  double freq;
  char   source[80];
  struct HEADER *p;

  if(isSet(PF_T_FLAG))
  {
    doSummary();

    return(0);
  }

  if(isSet(PF_S_FLAG))
  {
    listSaves();

    return(0);
  }

  resetRejections(); /* List rejections, not Receiver rejections */

  verifyParams();

  len = strlen(args->secondArg);

  if(isSet(PF_N_FLAG))
  {
    printNsave = 1;
  }
  else
  {
    printNsave = 0;
  }

  if(isSet(PF_E_FLAG) || (len && !strncasecmp(args->secondArg, "envr", len)))
  {
    tellEnv();

    return(0);
  }

/* Can't go beyond here without a data file */
  if(!env->numofscans)
  {
    st_warn("No data file loaded");

    return(1);
  }

  if((len && !strncasecmp(args->secondArg, "sfocus", len)))
  {
    tellAllSfocus();

    return(0);
  }

  if((len && !strncasecmp(args->secondArg, "spec5", len)))
  {
    tellAllSpec5();

    return(0);
  }

  if(isSet(PF_A_FLAG) || (len && !strncasecmp(args->secondArg, "all", len)))
  {
    tellAll();

    return(0);
  }

  if(isSet(PF_O_FLAG) || (len && !strncasecmp(args->secondArg, "ondata", len)))
  {
    tellOndata();

    return(0);
  }

  if(isSet(PF_I_FLAG) || (len && !strncasecmp(args->secondArg, "ignored", len)))
  {
    tellIgnored();

    return(0);
  }

  if(isSet(PF_W_FLAG) || (len && !strncasecmp(args->secondArg, "weather", len)))
  {
    tellWeather();

    return(0);
  }

  if(isSet(PF_S_FLAG) || (len && !strncasecmp(args->secondArg, "sources", len)))
  {
    showSourceList();

    return(0);
  }

  if(isSet(PF_G_FLAG) || (len && !strncasecmp(args->secondArg, "gdata", len)) || 
                         (len && !strncasecmp(args->secondArg, "gains", len)))
  {
    st_report("Dumping Gains");
    doDumpGains();

    return(0);
  }

  if(env->mode == MODE_CONT)
  {
    if(!strcasecmp(args->secondArg, "map"))
    {
      st_print("%s Xpts Ypts   Col  Row\n", listHeader);
    }
    else
    {
      st_print("%s\n", listHeader);
    }
  }
  else
  if(env->mode == MODE_SPEC)
  {
    st_print("%s  Res   Offset Offset\n", listHeader);
  }
  else
  if(env->mode == MODE_OTF)
  {
    st_print("%s  Res   MapY  Cols  MapY  Rows\n", listHeader);
  }
  else
  {
    st_print("%s  Res\n", listHeader);
  }

  if(len && !strncasecmp(args->secondArg, "tips", len))
  {
    tip = 1;
  }


  p = &sa[0];

  for(i=0;i<env->numofscans;i++,p++)
  {
	/* If user requests only planets, then list all reqardless of source */
    if(notSet(PF_P_FLAG))
    {
      if(!tip) /* SPTIP sometimes have erronious source names */
      {
        strncpy(source, p->object, 16);
        source[16] = '\0';
        strcmprs(source);

        if(env->check_source && strcasecmp(source, env->source))
        {
  	  incrementRejSource();
          continue; /* Skip */
        }
      }
    }

    if(env->mode == MODE_CONT)
    {
      freq = restFreq(p) / 1000.0;
    }
    else
    {
      freq = p->restfreq / 1000.0;
    }

    if(env->freq > 0.0 && fabs(freq - env->freq) > env->dfreq) /* Within 1 GHz */
    {
      incrementRejFreq();
      continue; /* No good */
    }

    if(p->scan > (env->escan+0.99) || p->scan < env->bscan)
    {
      incrementRejScans();
      continue;
    }

    if(env->check_date && fabs(p->utdate - env->date) > 0.000098)
    {
      incrementRejDates();
      continue;
    }

    /* Check for matching backend */
    if(!backendMatch(p))
    {
      incrementRejBackend();
      continue;
    }

    /* Has user supplied an upper limit on acceptable tsys? */
    if((env->tsyslimit > 0.0 && fabs(p->stsys) > env->tsyslimit) || gsl_isnan(p->stsys))
    {
      incrementBogusTsys();
      continue;
    }

    if(tip)
    {
      if(strcmp(p->obsmode, "CONTSTIP"))
      {
        incrementRejMode();
        continue;
      }
    }

    if(args->secondArg && !strcasecmp(args->secondArg, "foc"))
    {
      if(strncmp(p->obsmode, "CONTFOC", 7))
      {
        incrementRejMode();
        continue;
      }
    }

    if(args->secondArg && !strcasecmp(args->secondArg, "ewf"))
    {
      if(strncmp(p->obsmode, "CONTEWFC", 8))
      {
        incrementRejMode();
        continue;
      }
    }

    if(args->secondArg && !strcasecmp(args->secondArg, "nsf"))
    {
      if(strncmp(p->obsmode, "CONTNSFC", 8))
      {
        incrementRejMode();
        continue;
      }
    }

    if(args->secondArg && (!strcasecmp(args->secondArg, "seq") || !strcasecmp(args->secondArg, "acc")))
    {
      if(strncmp(p->obsmode, "CONTSEQ", 7))
      {
        incrementRejMode();
        continue;
      }
    }

    if(args->secondArg && !strcasecmp(args->secondArg, "map"))
    {
      if(strncmp(p->obsmode, "CONTMAP", 7))
      {
        incrementRejMode();
        continue;
      }
    }

    if(args->secondArg && !strcasecmp(args->secondArg, "qk5"))
    {
      if(strncmp(p->obsmode, "CONTFIVE", 8) && strncmp(p->obsmode, "CONTQK5", 7))
      {
        incrementRejMode();
        continue;
      }
    }

    if(isScanIgnored(p->scan))
    {
      incrementRejIgnored();
      continue;
    }

    if(env->mode == MODE_SPEC)
    {
      if(env->bkres > 0.0 && fabs(fabs(p->freqres) - env->bkres) > 0.01)
      {
        incrementRejFreqres();
        continue; /* No good; backend resolution doesn't match */
      }
    }

    if(env->issmt && env->strictsb)
    {
      feed = getFeed(p->scan); /* 1, 2, 3, 4 */

      if(p->sideband == LSB)
      {
        if(feed == 2 || feed == 4)
        {
          incrementRejSb();
          continue;
        }
      }
      else
      if((int)p->sideband == USB)
      {
        if(feed == 1 || feed == 3)
        {
          incrementRejSb();
          continue;
        }
      }
    }

    if(isSet(PF_P_FLAG))
    {
      strncpy(source, p->object, 16);
      source[16] = '\0';
      strcmprs(source);

      if(isPlanet(source) == 0)
      {
        continue;
      }
    }

    printScan(p, printNsave);

    n++;
  }

  env->last_scan_count = n;

  if(n)
  {
    if(env->tsyslimit > 0.0)
    {
      st_warn("Found %d Scans Matching Criteria & Tsys < %.1f", n, env->tsyslimit);
    }
    else
    {
      st_warn("Found %d Scans Matching Criteria", n);
    }
  }
  else
  {
    st_fail("No Scans Matched Criteria");

    printRejections();
  }

  return(0);
}



/**
  Print usages and exit();
  \returns 1
 */
int usage(name)
char *name;
{
  st_report(" ");
  st_report("Usage: %s [-hklrsvx] [-i init_file] [-g global_memory_file]", name);
  st_report("Where:  -g  Use named global_memory_file instead of default.");
  st_report("        -h  Show this help and exit.");
  st_report("        -i  Load init_file upon starting.");
  st_report("        -k  Ignore LOCKFILE altogether.");
  st_report("        -l  Turn on logging. Logfile will be named: linuxpops.$(PID).log");
  st_report("        -q  Be quiet upon starting.");
  st_report("        -r  Remove stale LOCKFILE before starting.");
  st_report("        -s  Turn on Socket Communication to Monitor.");
  st_report("        -v  Show version info and exit.");
  st_report("        -x  Run init script and exit.");
  st_report(" ");

  exit(1);
}


/**
  Prints the current system time
  \returns 0
 */
int printTimeStamp()
{
  st_report("Current Date: %s", getcTime(0));

  return(0);
}


/**
  Print the kernal version and version numbers
  \returns 0
 */
int about()
{
  struct LDD_STRUCT *p;

  st_report("**************************************************");
  st_report("*                                                *");
  st_report("* Linuxpops Version:                      %s *", versionStr);
  st_report("*                                                *");
  st_report("* Build Date:   %32.32s *", dateStr);
  st_report("* Build Kernal: %32.32s *", machStr);
  st_report("* GSL Version:  %32.32s *", GSL_VERSION);
  st_report("* GNUPLOT Version:                       %5.2f-%.0f *", gnuplotLevel, gnupatchlevel);
  st_report("*                                                *");

  if(isSet(PF_F_FLAG))
  {
    st_report("* Linuxpops Build Libraries:                     *");

    p = &ldd[0];

    for(;p->lib;p++)
    {
      st_report("* %28.28s      %12.12s *", p->lib, p->version);
    }

    st_report("*                                                *");
  }

  st_report("**************************************************");

  return(0);
}


/**
  Set the prompt to reflect if we are in line, condar or OTF. \n
  If in line, also show the 'holder' number
  \returns 0
 */
int setPrompt()
{
  if(env->mode == MODE_CONT)
  {
    sprintf(prompt, "Linuxpops(C) >> ");
  }
  else
  if(env->mode == MODE_SPEC)
  {
    sprintf(prompt, "Linuxpops(%s) >> ", h0->name);
  }
  else
  if(env->mode == MODE_OTF)
  {
    sprintf(prompt, "Linuxpops(O) >> ");
  }
  else
  {
    sprintf(prompt, "Linuxpops >> ");
  }

  rl_set_prompt(prompt);

  return(0);
}


int loggingOn()
{
  sprintf(logfilename, "/tmp/%s/linuxpops.log", session);

  if((errPrint_fp = fopen(logfilename, "a") ) <= (FILE *)0)
  {
    st_fail("main(): Unable to open logfile %s", logfilename);
    return(1);
  }

  st_report("Turning Loggin Off Using logging file: %s", logfilename);

  logging = 1;

  return(0);
}



int loggingOff()
{
  if(errPrint_fp)
  {
    fclose(errPrint_fp);

    errPrint_fp = (FILE *)NULL;
  }

  st_report("Turning Loggin Off");

  logging = 0;

  return(0);
}


int doLogging()
{
  if(isSet(PF_O_FLAG))
  {
    loggingOn();
  }

  if(isSet(PF_F_FLAG))
  {
    loggingOff();
  }

  return(0);
}



/**
  Main function
  \returns 0 if a good exit
  \returns 1 if there is a problem
 */
int main(argc, argv)
int argc;
char *argv[];
{
  int opt, manGlobal=0, read_config=0, remove_lockfile=0, ignore_lockfile=0, sockets=0, savemem=0, runandexit=0;
  char *cp, configName[256], cmdBuf[256], sbuf[256];
  fd_set fds;
  int sel, len, nfds;
  static char message[4096*16];

#ifdef TOMMIE
  int indx=0;
  time_t now, then;
#endif

  sprintf(prompt, "Linuxpops(C) >> ");

  setCactusEnvironment();

  initParseArgs(); /* initialize the command stack args */

  srand(time(NULL)); /* Initalize the random number generator */

  cp = getenv("TERM");

  if(!cp)
  {
    no_terminal = 1;
  }
  

    /* Adjust the terminal slightly before the handler is installed. Disable
     * canonical mode processing and set the input character time flag to be
     * non-blocking.
     */
  if(!no_terminal)
  {
    if( tcgetattr(fileno(stdin), &term) < 0)
    {
      perror("tcgetattr");
      exit(1);
    }
    else
    {
      old_lflag = term.c_lflag;
      old_vtime = term.c_cc[VTIME];
      term.c_lflag &= ~ICANON;
      term.c_cc[VTIME] = 1;

      /* COMMENT LINE BELOW - see above */
      if( tcsetattr(fileno(stdin), TCSANOW, &term) < 0)
      {
        perror("tcsetattr");
        exit(1);
      }
    }
  }

  rl_catch_signals = 0;  /* Tell the readline library to ignore signals */

  rl_callback_handler_install(prompt, process_line);
/* The above function prints out the prompt, so newline it here */
  st_print("\n"); 

  while ((opt = getopt(argc, argv, "i:hg:krlvqswx")) != -1)
  {
    switch (opt)
    {
      case 'i':
	st_report("Setting Configuration File: %s", optarg);
	strcpy(configName, optarg);
        read_config = 1;
        break;

      case 'g':
	st_report("Using Global Memory %s", optarg);
        strcpy(globalName, optarg);
        sprintf(objGlobalName, "%s-OBJ", globalName);
        manGlobal = 1;
        break;

      case 'h':
        usage(argv[0]);
        break;

      case 'q':
        stquiet = 1;
        break;

      case 'r':
        remove_lockfile = 1;
        break;

      case 'k':
        ignore_lockfile = 1;
	doNotRemoveLockFile = 1;
        break;

      case 'l':
        logging = 1;
        break;

      case 's':
        sockets = 1;
        break;

      case 'v':
        about();
	usage(argv[0]);
	exit(0);

        break;

      case 'w':
        savemem = 1;
        break;

      case 'x':
        runandexit = 1;
        break;

      default: /* '?' */
        usage(argv[0]);
        break;
    }
  }

  if(logging)
  {
    loggingOn();
  }

  initDvars();

  getEnvironment();

  rl_startup_hook = init_readline;

  using_history(); /* Initiate the history library functions */

  stifle_history(256); /* Only remember the last 256 history entries */

  rwHistory(READ_HISTORY); /* Load in the previous session */


  if(!manGlobal)
  {
    sprintf(globalName, "LPMEMORY-64");
    sprintf(objGlobalName, "LPMEMORY-OBJ-64");
  }

  st_report("Using Global Memory File: %s", globalName);

  sprintf(lockFileName, "%s.LOCKFILE", globalName);

  if(remove_lockfile)
  {
    removeLockFile();
  }

  if(!ignore_lockfile)
  {
    if(!checkLockFile())
    {
      st_fail("main(): Unable to lock Global Memory %s", globalName);
      exit(1);
    }
  }
  else
  {
    st_report("Ignoring Lockfile altogether");
  }

  /* Remove that pesky reply for input buffer */
  fflush(stdin);
  rl_set_prompt(prompt);

  if(!gm_map(globalName, 1))
  {
    st_fail("main(): Unable to map global memory: %s", globalName);
    exit(1);
  }

  correctBadChans();

  if(!obj_map(objGlobalName, 1))
  {
    st_fail("main(): Unable to map object memory: %s", objGlobalName);
    exit(1);
  }

  /* Check that object array has already been initialized */
  checkObjArray();

  about();

  initOnlineEnv();

  loadInitFile();

  tellEnv();

  loadCalibrators(NULL); /* Load the default alma calibrators */

  ffb_load();

  if(runandexit)
  {
    sockets = 0;
  }

  if(sockets)
  {
    st_report("Turning on Socket Communication");

    sock_bind("LINUXPOPS_MASTER");
    xgraphic    = sock_connect("LINUXPOPS_XGRAPHIC");
    helper      = sock_connect("LINUXPOPS_HELPER");
    dataserv    = sock_connect("LINUXPOPS_DATASERV");
    monitorSock = sock_connect("MONITOR");

    sock_send(xgraphic, "page");
    sock_send(helper,   "master");

    sock_bufct(sizeof(struct LP_IMAGE_INFO)+256);

    xfd = sock_fd(xgraphic);
    hfd = sock_fd(helper);

    st_report("Slave  Socket fd = %d", xfd);
    st_report("Helper Socket fd = %d", hfd);

    if(xfd < 2)
    {
      st_fail("Slave Task not Present");
      exit(1);
    }

    if(hfd < 2)
    {
      st_fail("Helper Task not Present");
      exit(1);
    }
  }

  strcpy(H0.name, "H0");
  strcpy(H1.name, "H1");
  strcpy(H2.name, "H2");
  strcpy(H3.name, "H3");
  strcpy(H4.name, "H4");
  strcpy(H5.name, "H5");
  strcpy(H6.name, "H6");
  strcpy(H7.name, "H7");
  strcpy(H8.name, "H8");
  strcpy(H9.name, "H9");

  stquiet = 0;

  env->mode = MODE_CONT;

  stackEmpty(); /* reset the stack */

  setPrompt();

  initLetterHelp();

  initSignals();

  initGnuplotBackground();

  loadProto();

  lp_ppid = getppid();
  st_report("LinuxPops PID: %d", lp_ppid);

  if(read_config)
  {
    subArgs(2, configName);
    readConfig();
  }

  /* Quitely Load any predefined formulas */
  sprintf(cmdBuf, "formula /r /i /q .lp.formulas");
  parseCmd(cmdBuf);

  /* Restore previous environment */
  cp = getenv("LINUXPOPS_DEFAULT_ENV");

  if(cp)
  {
    st_report("Switching to default environment: %s", cp);

    sprintf(cmdBuf, "switch %s ", cp);
    parseCmd(cmdBuf);
  }

#ifdef TOMMIE
  now = then = time(NULL);
#endif

  if(savemem)
  {
    sprintf(sbuf, "%s.save", globalName);
    subArgs(2, sbuf);
    writeAllConfig();

    exit(1);
  }

  /* Main loop */
  while(1)
  {
    if(logging && errPrint_fp)
    {
      fflush(errPrint_fp);
    }

    rl_refresh_line(0, 0);

    FD_ZERO(&fds);
    nfds = 0;

    FD_SET(fileno(stdin), &fds);
    nfds++;

    if(xgraphic)
    {
      FD_SET(xfd, &fds);
      nfds++;
    }

    if(helper)
    {
      FD_SET(hfd, &fds);
      nfds++;
    }

    if(select(FD_SETSIZE, &fds, NULL, NULL, NULL) < 0)
    {
      /* Events like window resize; Ignore these for now */
      continue;
    }

    if(FD_ISSET(fileno(stdin), &fds))
    {
      rl_callback_read_char();
    }

#ifdef TOMMIE
	/* Attempt to trap infinite loops cause by broken stdin */

    now = time(NULL);

    if(now != then)
    {
      if(indx > 100)
      {
	st_fail("Too many loops, %d, per second; exiting", indx);

        exit(1);
      }

      then = now;
    }

    indx++;
#endif

    if(xgraphic) /* Check if any messages from xgraphic */
    {
      if(FD_ISSET(xfd, &fds))
      {
	sel = sock_sel(message, &len, NULL, 0, 1, 0);

        if(sel >0)
        {
	  if(verboseLevel & VERBOSE_COMM)
          {
            printf("From Xgraphics Program: %s\n", message);
          }

	  if(!strncmp(message, "XGRAPHIC", 8) && len == sizeof(xsend))
          {
	    bcopy(message, (char *)&xsend, len);

	    if(xsend.plot_used)
            {
	      env->yplotmin = xsend.yplotmin;
	      env->yplotmax = xsend.yplotmax;
	      env->xplotmin = xsend.xplotmin;
	      env->xplotmax = xsend.xplotmax;
            }

	    if(xsend.map_angle_used)
            {
	      env->map_angle[0] = xsend.map_angle_1;
	      env->map_angle[1] = xsend.map_angle_2;
            }
          }
        }
        else
        if(sel == -1)
        {
          st_fail("Timeout waiting on read from xgraphic");
        }
        else
        {
          st_fail("Got strange sock_sel from xgraphic ret: %d", sel);
        }

        FD_CLR(xfd, &fds);
      }
    }

    if(helper) /* Check if any messages from helper */
    {
      if(FD_ISSET(hfd, &fds))
      {
	sel = sock_sel(message, &len, NULL, 0, 1, 0);

        if(sel >0)
        {
          if(strlen(message) > 2)
          {
	    deNewLineIt(message);

	    if(!strstr(message, "/q"))  /* Ignore 'quite' messsages */
            {
              st_report("Attached: %s", message);
            }

            //add_history(message);

            parseCmd(message);
          }
        }
        else
        if(sel == -1)
        {
          st_fail("Timeout waiting on read from Helper");
        }
        else
        {
          st_fail("Got strange sock_sel from Helper ret: %d", sel);
        }

        FD_CLR(hfd, &fds);
      }
    }

#ifdef TOMMIE
    if(dataserv && dfd > 1) /* Check if any messages from dataserv */
    {
      if(FD_ISSET(dfd, &fds))
      {
	sel = sock_sel(message, &len, NULL, 0, 1, 0);

        if(sel > 0)
        {
          st_print("Got message from Dataserv: %d bytes", len);

	  if(verboseLevel & VERBOSE_COMM)
          {
          }

          if(strlen(message) > 2)
          {
            st_print("From Dataser: %s", message);
          }
        }
        else
        if(sel == -1)
        {
          st_fail("Timeout waiting on read from Dataserv");
        }
        else
        {
          st_fail("Got strange sock_sel from Dataserv ret: %d", sel);
        }

        FD_CLR(dfd, &fds);
      }
    }
#endif
  }

  return(0);
}


/**
  Make an artificial data file based on the search criteria
  \returns 0 success
  \returns 1 on missing filname of error
 */
int pruneFile()
{
  int i, len, n=0, remove = 0, iret;
  struct HEADER *p;
  char sysBuf[1024], *outFile;
  double ret;

  outFile = args->secondArg;

  len = strlen(outFile);

  remove = isSet(PF_R_FLAG);

  if(!len)
  {
    st_fail("Usage: prune output_file_name");
    return(1);
  }
  
/* Can't go beyond here without a data file */
  if(!env->numofscans)
  {
    st_warn("No data file loaded");

    return(1);
  }

  if(remove)
  {
    st_report("Removing output file: %s", outFile);

    sprintf(sysBuf, "rm -f %s", outFile);
    system(sysBuf);
  }

  lp_new_sdd(outFile);

  p = &sa[0];

  for(i=0;i<env->numofscans;i++,p++)
  {
    if(!meetsCritiria(p, CHECK_ALL))
    {
      continue;
    }

    ret = read_scan(p->scan, env->filename, h0->yplot1, &h0->head, 0);
    env->baselineremoved = 0;
    env->curScan = p->scan;

    if(ret > 0.0)
    {
      iret = lp_write_sdd(outFile, &h0->head, (char *)&h0->yplot1);

      if(verboseLevel & VERBOSE_FILEOPS)
      {
        st_report("lp_write_sdd() returned: %d", iret);
      }
    }

    printScan(p, printNsave);
    n++;
  }

  if(n)
  {
    st_warn("Wrote %d Scans Matching Criteria to %s", n, outFile);
  }
  else
  {
    st_fail("No Scans Matched Criteria");
  }

  return(0);
}


/**
  Turn sockets on: default condition
  \returns 0
 */
int setSockets()
{
  if(strlen(args->secondArg))
  {
    if(!strcmp(args->secondArg, "1"))
    {
      st_report("Turning ON Socket Communication");

      sock_bind("ANONYMOUS");
      xgraphic = sock_connect("LINUXPOPS_XGRAPHIC");

      sock_send(xgraphic, "Hello");
    }
    else
    {
      st_report("Turning OFF Socket Communication");
      monitorSock = 0;
    }
  }

  return(0);
}


/**
  Take the 4 mouse positions and convert to Freq, Vel, Channel and Temp \n
  Set the ENVIRONMENT:: variables tcur, vcur, fcur & ccur \n
  \returns 0
 */ 
int parseMouseValues(x1, x2, y1, y2)
double x1, x2, y1, y2;
{
  double startVel, startRes;
  struct HEADER *h;
  struct LP_IMAGE_INFO *image = h0;

  last_mouse_x  = x1;
  last_mouse_x2 = x2;
  last_mouse_y  = y1;
  last_mouse_y2 = y2;

  if(env->mode == MODE_CONT)
  {
    return(0);
  }

  env->ccur = 0.0;
  env->fcur = 0.0;
  env->vcur = 0.0;

  h = &image->head;

  /* FIXME: trap unrealistic values here */

  switch(env->x1mode)
  {
    case XMODE_CHAN:
	env->ccur = x1;
	break;

    case XMODE_FREQ:
	env->fcur = x1;
	break;

    case XMODE_VEL:
	env->vcur = x1;
	break;
  }

  switch(env->x2mode)
  {
    case XMODE_CHAN:
	env->ccur = x2;
	break;

    case XMODE_FREQ:
	env->fcur = x2;
	break;

    case XMODE_VEL:
	env->vcur = x2;
	break;
  }

  /* By now, one or more of the 3 X-axis will still be 0.0, fix those */

  /* Compute the current channel so it can be used next */
  if(env->ccur == 0.0)
  {
    if(env->fcur != 0.0) /* One of these two should be non-zero */
    {
      env->ccur = mapFtoChan(env->fcur);
    }
    else
    if(env->vcur != 0.0)
    {
      env->ccur = mapVtoChan(env->vcur);
    }
  }

  /* The following can be directly computed */
  if(env->fcur == 0.0)
  {
    if(env->ccur != 0.0) /* Use the current channel number to computer Freq offset */
    {
      startRes = (float)h->refpt * h->freqres;

      if(h->sideband == 2.0)
      {
        env->fcur =  startRes - (env->ccur * h->freqres);
      }
      else
      {
        env->fcur = -startRes + (env->ccur * h->freqres);
      }
    }
    else
    {
      st_fail("Should not get here: CCUR is still 0.0; Error #1");
    }
  }

  if(env->vcur == 0.0)
  {
    if(env->ccur != 0.0) /* Use the current channel number to computer vel */
    {
      startVel = h->refpt_vel - h->refpt * h->deltax;

      if( h->sideband == 2.0)
      {
        env->vcur = startVel + (env->ccur * h->deltax);
      }
      else
      {
        env->vcur = startVel - (env->ccur * h->deltax);
      }
    }
    else
    {
      st_fail("Should not get here: CCUR is still 0.0; Error #2");
    }
  }

  env->tcur = y1; /* For now all y-plotting uses only a single y axis */

  if(verboseLevel & VERBOSE_COMM)
  {
    st_report("Mouse Variables:    ccur       fcur     vcur   tcur");
    st_report("Set to:            %7.3f, %7.3f, %7.3f, %7.3f", env->ccur, env->fcur, env->vcur, env->tcur);
  }

  return(0);
}



/**
  Get a response from XGRAPHIC \n
  Will wait a bit on response from xgraphic
  \returns 0 on success
  \returns 1 on timeout
 */
int getResponse(which)
int which;
{
  int sel, delay, len, ret;
  char message[4096];

  if(verboseLevel & VERBOSE_COMM)
  {
    st_report("Slave = %d, xfd = %d", xgraphic, xfd);
    st_report("getResponse(%d): Waiting on xgraphic response", which);
  }

  if(which == WAIT_FOR_MOUSE)
  {
    delay = 15;
  }
  else
  {
    delay = 3;
  }

  while(1)
  {
    sel = sock_sel(message, &len, NULL, 0, delay, 1);

    if(verboseLevel & VERBOSE_COMM)
    {
      st_report("sock_sel returned %d", sel);
    }

    if(sel == xfd)
    {
      if(verboseLevel & VERBOSE_COMM)
      {
        printf("%s\n", message);
      }

      if(!strncmp(message, "XGRAPHICS", 9) && len == sizeof(xsend))
      {
        bcopy(message, (char *)&xsend, sizeof(xsend));

        if(which == WAIT_FOR_MOUSE && xsend.mouse_used)
        {
          ret = parseMouseValues(xsend.last_mouse_x, 
			         xsend.last_mouse_x2, 
			         xsend.last_mouse_y, 
			         xsend.last_mouse_y2);

          return(ret);
        }
	else
	if(which == WAIT_FOR_PLOT && xsend.plot_used)
        {
	  env->yplotmin = xsend.yplotmin;
	  env->yplotmax = xsend.yplotmax;
	  env->xplotmin = xsend.xplotmin;
	  env->xplotmax = xsend.xplotmax;

          return(0);
        }
        else
        if(which == WAIT_FOR_MAP_ANGLE && xsend.map_angle_used)
        {
          env->map_angle[0] = xsend.map_angle_1;
          env->map_angle[1] = xsend.map_angle_2;

          return(0);
        }
      }
    }
    else
    if(sel == 0)
    {
      st_report("Keyboard Hit");
      sendCancel();

      return(1);
    }
    else
    if(sel == -1)
    {
      st_fail("Timeout waiting on read from xgraphic");
      sendCancel();

      return(1);
    }
    else
    {
      st_fail("Got strange sock_sel ret: %d", sel);
      sendCancel();

      return(1);
    }
  }

  return(1);
}


int sendCancel()
{
  sock_send(xgraphic, "cancel");

  return(0);
}


/**
  Command line version
  \returns 0
 */
int doCCur()
{
  _doCCur("");

  return(0);
}


/**
  Tell the xgraphics program to send the next mouse click \n
  (internal version)
  \returns 0
 */
int _doCCur(msg)
char *msg;
{
  if(xgraphic)
  {
    env->ccur = 0; /* reset first */

    if(strlen(msg) > 3)
    {
      st_report(msg);
    }

    sock_send(xgraphic, "ccur");

    if(!getResponse(WAIT_FOR_MOUSE))
    {
      st_report("ccur set to %d", (int)round(env->ccur));
    }
    else
    {
      return(1);
    }
  }

  return(0);
}


/**
  Tell the xgraphics program to send the map_angle \n

  \returns 0

  Not all versions of gnuplot have these variables
  Need to resolve how to tell this via gnuplot versions.

 */
int doGetMapAngle()
{
  if(gnuplotLevel < 4.4) /* Must be this level to support query of map angle */
  {
    return(0);
  }

  if(xgraphic)
  {
    sock_send(xgraphic, "show_view");

    if(!getResponse(WAIT_FOR_MAP_ANGLE))
    {
      if(isSet(PF_V_FLAG))
      {
        st_report("doGetMapAngle(): map angle set to %.1f %.1f", env->map_angle[0], env->map_angle[1]);
      }
    }
    else
    {
      return(1);
    }
  }

  return(0);
}


/**
  Command line version
  \returns 0
 */
int doFCur()
{
  _doFCur("");

  return(0);
}


/**
  Tell the xgraphics program to send the next mouse click \n
  (internal version)
  \returns 0
 */
int _doFCur(msg)
char *msg;
{
  if(xgraphic)
  {
    if(strlen(msg) > 3)
    {
      st_report(msg);
    }

    sock_send(xgraphic, "ccur");

    if(!getResponse(WAIT_FOR_MOUSE))
    {
      st_report("fcur set to %e", env->fcur);
    }
    else
    {
      return(1);
    }
  }

  return(0);
}



/**
  Command line version
  \returns 0
 */
int doVCur()
{
  _doVCur("");

  return(0);
}


/**
  Tell the xgraphics program to send the next mouse click \n
  (internal version)
  \returns 0
 */
int _doVCur(msg)
char *msg;
{
  if(xgraphic)
  {
    if(strlen(msg) > 3)
    {
      st_report(msg);
    }

    sock_send(xgraphic, "ccur");

    if(!getResponse(WAIT_FOR_MOUSE))
    {
      st_report("vcur set to %e", env->vcur);
    }
    else
    {
      return(1);
    }
  }

  return(0);
}


/**
  Command line version
  \returns 0
 */
int doTCur()
{
  _doTCur("");

  return(0);
}


/**
  Tell the xgraphics program to send the next mouse click \n
  (internal version)
  \returns 0
 */
int _doTCur(msg)
char *msg;
{
  if(xgraphic)
  {
    if(strlen(msg) > 3)
    {
      st_report(msg);
    }

    sock_send(xgraphic, "tcur");

    if(!getResponse(WAIT_FOR_MOUSE))
    {
      st_report("tcur set to %e", env->tcur);
    }
    else
    {
      return(1);
    }
  }

  return(0);
}



/**
  Tell the xgraphics program to clear the display
  \returns 0
 */
int doPageCmd()
{
  if(xgraphic)
  {
    st_report("Clearing Plot Window");

    sock_send(xgraphic, "page");
  }

  return(0);
}


/**
  Execute the external program 'dumpscans' on data file
  \returns 0
 */
int doDumpScans()
{
  char sysBuf[1024];

  if(strlen(env->filename))
  {
    sprintf(sysBuf, "dumpscans %s %s %s %s %s %s %s", env->filename, 
		args->secondArg, args->thirdArg, args->fourthArg, args->fifthArg, args->sixthArg, args->seventhArg);

//  st_report(sysBuf);

    system(sysBuf);
  }
  else
  {
    st_fail("Data File Name not set");
  }


  return(0);
}


/**
  Execute the external program 'dumpscans' on Gains file
  \returns 0
 */
int doDumpGains()
{
  char sbuf[256], fbuf[256], sysBuf[1024];

  if(isSet(PF_G_FLAG))
  {
    if(env->check_source)
    {
      sprintf(sbuf, " | grep -i %s", env->source);
    }
    else
    {
      strcpy(sbuf, " ");
    }

    if(env->check_freq)
    {
      sprintf(fbuf, " | grep %.6f", env->freq);
    }
    else
    {
      strcpy(fbuf, " ");
    }

    sprintf(sysBuf, "dumpscans %s %s %s", env->gfilename, sbuf, fbuf);
  }
  else
  if(isSet(PF_C_FLAG))
  {
    sprintf(sysBuf, "dumpscans -c %s ", env->gfilename);
  }
  else
  if(strlen(env->gfilename))
  {
    sprintf(sysBuf, "dumpscans %s %s %s %s %s %s %s", env->gfilename, 
		args->secondArg, args->thirdArg, args->fourthArg, args->fifthArg, args->sixthArg, args->seventhArg);
  }
  else
  {
    st_fail("Gains File Name not set");
    return(1);
  }

  st_report(sysBuf);

  if(isSet(PF_L_FLAG))
  {
    strcat(sysBuf, " | less -E -F");
  }

  system(sysBuf);

  return(0);
}


/**
  Execute the external program 'checkfile'
  \returns 0
 */
int doCheckFile()
{
  char sysBuf[256], *cp;

  if(isSet(PF_S_FLAG))
  {
    cp = env->savefile;
  }
  else
  {
    cp = env->filename;
  }

  sprintf(sysBuf, "checkfile %s %s %s %s %s %s %s", cp, 
		args->secondArg, args->thirdArg, args->fourthArg, args->fifthArg, args->sixthArg, args->seventhArg);

  st_report(sysBuf);

  system(sysBuf);

  return(0);
}


/**
  Execute the external program 'check_sdd'
  \returns 0
 */
int doCheckSdd()
{
  char sysBuf[512];

  sprintf(sysBuf, "check_sdd %s %s %s %s %s %s", env->filename, 
		args->secondArg, args->thirdArg, args->fourthArg, args->fifthArg, args->sixthArg);

  system(sysBuf);

  return(0);
}


/**
  Compare two doubles
  \returns 0 if < than delta
  \returns 1 if > than delta
 */
int lp_fcmp(first, second, delta)
double first, second, delta;
{
  double fdiff;

  fdiff = fabs(first-second);

  if(fdiff < delta)
  {
    return(0);
  }

  return(1);
}


/**
  Frees both plotting axis
  \returns 0
 */
int doAutoScale()
{
  st_report("Setting X & Y axis to Autoscale");
  freeX();
  freeY();

  return(0);
}


/**
  Set Y-Axis to 10 * log10(y/peak)
  \returns 0
 */
int setLogPlot()
{
  if(env->mode == MODE_CONT)
  {
    env->yaxis_scale = LOG_PLOT;
  }
  else
  {
    lineConvert2LogPlot();
  }

  return(0);
}


/**
  Set Y-Axis to Linear Scale
  \returns 0
 */
int setLinPlot()
{
  env->yaxis_scale = LINEAR_PLOT;

  return(0);
}


/**
  This function returns the proper freq based on date of recording; \n
  Kitt Peak still has not been corrected Feb 11, 2016: twf: \n
  I corrected the headers at the SMT to properly fill in the \n
  rest and sky frequencies.  Prior to that the rest freq was \n
  located in the obsfreq slot for continuum.
  \returns restfreq
 */
double restFreq(h)
struct HEADER *h;
{
  double freq;

  if(env->issmt)
  {
    if(env->mode == MODE_CONT)
    {
      if(h->burn_time > 1455213600.0)
      {
        freq = (h->restfreq);
      }
      else
      {
        freq = (h->obsfreq);
      }
    }
  }
  else
  {
    if(env->mode == MODE_CONT)
    {
      freq = (h->obsfreq);
    }
    else
    {
      freq = (h->restfreq);
    }
  }

/* Trap the stupid DSF / restfreq BS */
  if(freq == 10000.0)
  {
    freq = h->obsfreq;
  }

  return(freq);
}


/**
  Tell the xgraphics program to replot the display
  \\returns 0
 */
int refreshPlot()
{
  if(xgraphic)
  {
    sock_send(xgraphic, "refresh");
  }

  return(0);
}

/**
  Tell the xgraphics program to return plot extents of the display
  \\returns 0
 */
int getPlotExtents()
{
  if(isSet(PF_C_FLAG))
  {
    env->xplotmin = 0.0;
    env->xplotmax = 0.0;
    env->yplotmin = 0.0;
    env->yplotmax = 0.0;

    return(0);
  }

  if(xgraphic)
  {
    sock_send(xgraphic, "get_range");
  }

  if(isSet(PF_S_FLAG))
  {
    getResponse(WAIT_FOR_PLOT);

  /* push args on stack */      /* Lock the x and y ranges */
    subArgs(2, "%f", env->xplotmin);
    subArgs(3, "%f", env->xplotmax);
    setXRange();

        /* push args on stack */
    subArgs(2, "%f", env->yplotmin);
    subArgs(3, "%f", env->yplotmax);
    setYRange();
  }

  return(0);
}


int sendXgraphic()
{
  int i;
  char bigBuf[512], tbuf[512];

  if(isSet(PF_X_FLAG))
  {
    bigBuf[0] = '\0';
  }
  else
  {
    strcpy(bigBuf, "xgraphic ");
  }

  if(args->ntokens > 1)
  {
    for(i=1;i<args->ntokens;i++)
    {
      sprintf(tbuf, "%s ", args->tok[i]);
      strcat(bigBuf, tbuf);
    }

    if(xgraphic)
    {
      if(verboseLevel & VERBOSE_COMM)
      {
        st_report("Sending: %s to xgraphic", bigBuf);
      }

      sock_send(xgraphic, bigBuf);
    }
  }
  else
  {
    printH_FlagHelp("xgraphic");
  }

  return(0);
}



int helperWait()
{
  int i, t, len;
  char response[256];
  char message[1024];
  double start, end;

  if(args->ntokens < 3)
  {
    printH_FlagHelp("helperWait");

    return(1);
  }

  strcpy(response, args->secondArg);
  t = atoi(args->thirdArg);

  start = getmJulDate(0);

  if(t > 1)
  {
    clearInterrupt();

    st_report("helperWait(): Waiting for '%s', for %d Secs", response, t);

    for(i=0;i<t;i++)
    {
      len = sock_only(helper, message, 1);

      if(len > 0)
      {
        if(!strncmp(message, response, strlen(response)))
        {
          end = getmJulDate(0);

          st_report("Got %s, from helper in %.2f sec(s)", message, (end-start)*86400.0);
  
          return(0);
        }
      }

      if(isInterrupt()) /* Check for Control-C input */
      {
        end = getmJulDate(0);

        st_report("Got Control-C input in %.2f sec(s)", (end-start)*86400.0);

        return(0);
      }
    }
  }

  end = getmJulDate(0);

  st_report("Timeout in  %.2f sec(s)", (end-start)*86400.0);

  return(1);
}


/* Wait on a binary reply */
int helperBinaryWait()
{
  int i, t, len;
  char message[sizeof(struct DS_IMAGE_INFO)+256];

  if(args->ntokens < 2)
  {
    printH_FlagHelp("helperWait");

    return(1);
  }

  t = atoi(args->secondArg);

  if(t > 1)
  {
    clearInterrupt();

    st_report("helperWait(): Waiting for DS_IMAGE_INFO, for %d Secs", t);

    for(i=0;i<t;i++)
    {
      len = sock_only(helper, message, 1);

      if(len == sizeof(struct DS_IMAGE_INFO))
      {
        st_report("Got DS_IMAGE_INFO, from helper in %d sec(s)", i+1);

        bcopy(message, (char *)&ds_image_info, sizeof(ds_image_info));
  
        return(0);
      }

      if(isInterrupt()) /* Check for Control-C input */
      {
        return(0);
      }
    }
  }

  st_fail("Timeout in helperWait(); or 0 timeout");

  return(1);
}

/**
  Sleep for the designated number of seconds
  \returns 0
 */
int doSleep()
{
  long uSec;

  if(strlen(args->secondArg))
  {
    uSec = atol(args->secondArg);

    if(uSec > 0)
    {
      usleep(uSec);

      return(0);
    }
  }

  return(0);
}


/**
  Pause the program until the user hits a key
  \returns key pressed
 */
int doPause()
{
  int ret;

  ret = getYesNoQuit("Press Any Key to Continue");

  return(ret);
}


/**
  Print out some internal configuration info
  \returns 0
 */
int showID()
{
  st_report("Session:  /tmp/%s", session);
  st_report("setenv MAILBOXDEFS %s", getenv("MAILBOXDEFS"));

  return(0);
}


int doAttach()
{
  int ret;
  char sysBuf[256];

  if(strlen(args->secondArg))
  {
    sprintf(sysBuf, "%s %s %s &", args->secondArg, getenv("MAILBOXDEFS"), globalName);
  }
  else
  {
    st_fail("Missing arg for attach");
    return(1);
  }

  ret = system(sysBuf);

  return(ret);
}


int doGReview()
{
  subArgs(2, "greview");

  doAttach();

  return(0);
}


int doPGUI()
{
  subArgs(2, "paramgui");

  doAttach();

  return(0);
}


int writeIRC_Record()
{
  int i, j=0;
  struct SOURCES *p=0;
  FILE *fp;

  if(strlen(args->secondArg) < 2)
  {
    st_fail("Missing arg for writeirc");
    return(0);
  }

  p = findSourceInList("IRC+10216");

  if(!p)
  {

    st_fail("IRC+10216 not found in %s", env->filename);
    return(1);
  }

  if((fp = fopen(args->secondArg, "a") ) <= (FILE *)0)
  {
    st_fail("writeIRC_Record(): Unable to open file %s", args->secondArg);
    return(1);
  }

  fprintf(fp, "%64.64s", env->filename);

  for(i=0;i<MAX_FREQS;i++)
  {
    if(p->freqs[i].used)
    {
      fprintf(fp, "  %7.3f %5d", p->freqs[i].freq / 1000.0, p->freqs[i].count);
      j++;

      if(j > 1 && !(j%10))
      {
        fprintf(fp, "\n                                                                ");
      }
    }
  }

  fprintf(fp, "\n");

  st_report("Wrote (1) record to %s", args->secondArg);

  fclose(fp);

  return(0);
}

int doSay()
{
  int i;
  char outBuf[1024], tbuf[512], *cp;

  outBuf[0] = '\0';
  for(i=1;i<args->ntokens;i++)
  {
    sprintf(tbuf, "%s ", args->tok[i]);
    strcat(outBuf, tbuf);
  }

  for(cp=outBuf;*cp;cp++)
  {
    if(*cp == '\"')
    {
      *cp = ' ';
    }

    if(*cp == '\'')
    {
      *cp = ' ';
    }
  }

  strcmprs(outBuf);

  st_print("%s\n", outBuf);

  return(0);
}


int doSelPrint()
{
  int n;
  char sysbuf[1024], lbuf[256], sbuf[256], cbuf[256];

  bzero(lbuf, sizeof(lbuf));
  bzero(sbuf, sizeof(sbuf));
  bzero(cbuf, sizeof(cbuf));

  if(isSet(PF_L_FLAG))
  {
    sprintf(lbuf, "-o landscape");
  }

  if(isSet(PF_B_FLAG))
  {
    sprintf(sbuf, "-o media=11x17");
  }

  n = strlen(args->secondArg);
  if(n && !strncmp(args->secondArg, "cpi", 3))
  {
    sprintf(cbuf, "-o cpi=%s", args->secondArg+3);
  }

  n = strlen(args->thirdArg);
  if(n && !strncmp(args->thirdArg, "cpi", 3))
  {
    sprintf(cbuf, "-o cpi=%s", args->thirdArg+3);
  }

  n = strlen(args->fourthArg);
  if(n && !strncmp(args->fourthArg, "cpi", 3))
  {
    sprintf(cbuf, "-o cpi=%s", args->fourthArg+3);
  }

  sprintf(sysbuf, "xclip -o | lpr %s %s %s", lbuf, sbuf, cbuf);

  st_report(sysbuf);

  system(sysbuf);

  st_report("Job sent to %s", printer);

  return(0);
}


int doCodeView()
{
  int len;
  char sysBuf[512], file[256];
  struct COMMANDS *p;

  p = &commands[0];
  len = strlen(args->secondArg);

  if(len)
  {
    for(;p->cmd;p++)
    {
      if(!strncasecmp(p->cmd, args->secondArg, len))
      {
        if(!findFunction(p->fctnStr, file))
        {
          sprintf(sysBuf, "vim +/%s %s/linuxpops/%s", p->fctnStr, pkg_home, file);

	  if(isSet(PF_N_FLAG)) /* Just print it */
          {
            st_report("Exec: %s", sysBuf);
          }
          else                 /* Else, Actually edit file */
          {
            system(sysBuf);
          }
        }

        return(0);
      }
    }
  }
  else
  {
    printH_FlagHelp("codeview");
  }

  return(1);
}


int doTCSH()
{
  system("/bin/tcsh");

  return(0);
}


int doXwatch()
{
  char *cp, sysBuf[256], pid[80];

  cp = strchr(session, '-');

  if(cp)
  {
    cp++;

    strcpy(pid, cp);

    st_report("Starting XGraphic log watcher");

    sprintf(sysBuf, "xterm -tn %s -geometry 142x45+1210+22 -e tail -f /tmp/%s/xgraphic.log /tmp/%s/xgraphic.stdout.log &", pid, session, session);

    system(sysBuf);
  }

  return(0);
}


int doLogWatch()
{
  char *cp, sysBuf[256], pid[80];

  cp = strchr(session, '-');

  if(cp)
  {
    cp++;

    strcpy(pid, cp);

    st_report("Starting Logger watcher");

    sprintf(sysBuf, "startlogwatch.csh %s %s &", pid, session);
//        "xterm -tn %s -geometry 165x36+450+530 -e tail -f /tmp/%s/logger.log /tmp/%s/logger.stdout.log /tmp/%s/xgraphic.stdout.log &", 
//	pid, session, session, session);

    system(sysBuf);
  }

  return(0);
}


int doDone()
{

  st_report("Got random 'done' command");

  return(0);
}


int doStop()
{
  if(doingOTF)
  {
    to_dataserv("stop");
    to_dataserv("done");
  }

  return(0);
}


int doVi()
{
  char tbuf[256];

  if(args->ntokens > 1)
  {
    sprintf(tbuf, "vim %s", args->secondArg);

    system(tbuf);
  }
  else
  {
    printH_FlagHelp(args->firstArg);
    return(1);
  }

  return(0);
}


int doMv()
{
  char tbuf[256];

  if(args->ntokens > 2)
  {
    sprintf(tbuf, "mv %s %s %s %s %s",  args->secondArg, 
					args->thirdArg, 
					args->fourthArg, 
					args->fifthArg, 
					args->sixthArg);

    strcmprs(tbuf);

    system(tbuf);
  }
  else
  {
    printH_FlagHelp(args->firstArg);
    return(1);
  }

  return(0);
}


int doCP()
{
  char tbuf[256];

  if(args->ntokens > 2)
  {
    sprintf(tbuf, "cp %s %s %s %s %s",  args->secondArg, 
					args->thirdArg, 
					args->fourthArg, 
					args->fifthArg, 
					args->sixthArg);

    strcmprs(tbuf);

    system(tbuf);
  }
  else
  {
    printH_FlagHelp(args->firstArg);
    return(1);
  }

  return(0);
}


int doCopyRaw()
{
  int  pid;
  char tbuf[256];
  char fname[256];


  if(isSet(PF_A_FLAG))
  {
    pid = rand();
    pid = pid%32767;

    makeFileName(fname, pid);

    sprintf(tbuf, "cp /tmp/%s/scan.dat %s.raw",  session, fname);
  }
  else
  if(args->ntokens > 1)
  {
    sprintf(tbuf, "cp /tmp/%s/scan.dat %s",  session, args->secondArg);
  }
  else
  {
    printH_FlagHelp(args->firstArg);
    return(1);
  }

  strcmprs(tbuf);
  st_report(tbuf);

  system(tbuf);

  return(0);
}


int xdelay(d)
int d;
{
  if(env->xdelay)
  {
    st_report("xdelay(): Delaying: %.0f mSec", (double)env->xdelay / 1e3);
    usleep(env->xdelay);
  }
  else
  {
    st_report("xdelay(): Delaying: %.0f mSec", (double)d / 1e3);
    usleep(d);
  }

  return(0);
}

