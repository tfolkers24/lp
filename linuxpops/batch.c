/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

struct BATCH_COMMAND bcommands[MAX_BATCH_COMMANDS];


char batch_tok[MAX_TOKENS][MAX_TOKENS];

int batch_get_tok(edit)
char *edit;
{
  int c = 0;
  char *token;

  bzero((char *)&batch_tok, sizeof(batch_tok));

  token = strtok(edit, " \r\t\n");
  while(token != (char *)NULL)
  {
    sprintf(batch_tok[c++], "%s", token);
    token = strtok((char *)NULL, " \r\t\n");
  }

  return(c);
}


int doBatch()
{
  int ret;
  char name[256], fname[256], sysBuf[512];

  if(isSet(PF_L_FLAG))
  {
    listBatch();

    return(0);
  }

  if(!strlen(args->secondArg))
  {
    st_fail("Usage: batch batch_file_name");
    return(1);
  }

  if(!strcmp(args->secondArg, "clear"))
  {
    if(strlen(args->thirdArg))
    {
      strcpy(name, args->thirdArg);
      clearBatch(name);

      return(0);
    }
    else
    {
      st_fail("Missing arg for 'batch clear' command");
      return(1);
    }
  }

  if(!strcmp(args->secondArg, "edit"))
  {
    if(strlen(args->thirdArg))
    {
      strcpy(name, args->thirdArg);
      ret = isBatch(name);

      if(ret >= 0)
      {
        strcpy(fname, bcommands[ret].fname);
        sprintf(sysBuf, "vi %s", fname);

        system(sysBuf);

        return(0);
      }
      else
      {
	st_fail("Unable to locate an internal procedure named: %s", name);
        return(0);
      }
    }
    else
    {
      st_fail("Missing arg for 'batch edit' command");
      return(1);
    }
  }

  /* default action */
  loadBatch(args->secondArg);

  return(0);
}


int clearBatch(name)
char *name;
{
  int n;
  struct BATCH_COMMAND *p;

  n = isBatch(name);

  if(n >= 0)
  {
    p = &bcommands[n];

    bzero((char *)p, sizeof(struct BATCH_COMMAND));

    return(0);
  }
  else
  {
    st_fail("Unable to locate batch file %s", name);
  }

  return(1);
}


int loadBatch(bname)
char *bname;
{
  int i;
  char *cp, line[256], *string;
  char *home, *init, *proc;
  char  pname1[256], pname2[256], pname3[256];
  FILE *fp;
  struct BATCH_COMMAND *p;

  /* The batch file may be in any number of places; try them all */

  home = getenv("HOME");
  init = getenv("OBSINIT");
  proc = getenv("LINUXPOPS_HOME");

  st_print("loadBatch(): Looking for batch file: %s\n", bname);

  if(home)
  {
    sprintf(pname1, "%s/%s", home, bname);
  }

  if(init && home)
  {
    sprintf(pname2, "%s/%s/%s", home, init, bname);
  }

  if(proc)
  {
    sprintf(pname3, "%s/procedures/%s", proc, bname);
  }

  st_print("loadBatch(): %s\n", pname1);
  st_print("loadBatch(): %s\n", pname2);
  st_print("loadBatch(): %s\n", pname3);

  /* First try the bare name */
  if((fp = fopen(bname, "r") ) > (FILE *)0)
  {
    st_print("Found batch file ./here\n");
  }
  else /* Try home/batchfile */
  if((fp = fopen(pname1, "r") ) > (FILE *)0)
  {
    st_print("Found batch file in HOME \n");
  }
  else /* Try home/initials/batchfile */
  if((fp = fopen(pname2, "r") ) > (FILE *)0)
  {
    st_print("Found batch file in HOME/INIT \n");
  }
  else /* Try $LINUXPOPS_HOME/procedures/batchfile */
  if((fp = fopen(pname3, "r") ) > (FILE *)0)
  {
    st_print("Found batch file in $LINUXPOPS_HOME/procedures\n");
  }

  if(!fp)
  {
    st_fail("Unable to open file %s", bname);
    return(0);
  }

  st_report("loadBatch(): Loading: %s", bname);

  p = getFreeBatch(bname);

  if(!p)
  {
    st_fail("Unable to allocate free Batch memory");
    return(0);
  }

  /* Clear it out just in case it's reused */
  bzero((char *)p, sizeof(struct BATCH_COMMAND));

  i = 0;

  p->used = 1;
  strcpy(p->fname, args->secondArg);

  while((string = fgets(line, sizeof(line), fp)) != NULL)
  {
    deNewLineIt(line);
    strcmprs(line);
    st_print("%s\n", line);

    if(strlen(line) < MAX_BATCH_LWIDTH)
    {
      strcmprs(line); /* strip excess spaces */

      if((!strncmp(line, "procedure", 9)))
      {
        strcpy(p->name, line+10);
      }
      else
      if((cp = strstr(line, "ntoken? ")))
      {
        p->ntokens = atoi(cp+8);
      }
      else /* Copy into batch */
      {
        strcpy(p->lines[i++], line);

	/* Look for keywords */
        if(!strncmp(line, "#", 1) && (cp = strstr(line, "Usage")))
        {
          strcpy(p->usage, cp);
        }
      }
    }
    else
    {
      st_fail("Batch file line too long");
    }

    if(i >= MAX_BATCH_LINES)
    {
      st_fail("Too many lines, %d, in batch file %s", i, bname);
      fclose(fp);
      p->nlines = i;

      return(1);
    }
  }

  fclose(fp);

  p->nlines = i;

  return(0);
}


int execBatch(name)
char *name;
{
  int i, j, n, ret, lptokens, pause=0, loophead = 0;
  char *cp, tbuf[256], scanBuf[256],
        arg1[256], arg2[256], arg3[256], arg4[256], arg5[256], arg6[256], arg7[256], arg8[256];
  static struct BATCH_COMMAND *p = NULL;

  lptokens = args->ntokens;  /* Remember number of tokens passed to batch file */

  ret = isBatch(name);

  pause = isSet(PF_P_FLAG);

  if(ret >= 0)
  {
    /* remember args; because they are going to get stomped on */
    /* by all the commands we are about to execute.            */
    strcpy(arg1, args->firstArg);
    strcpy(arg2, args->secondArg);
    strcpy(arg3, args->thirdArg);
    strcpy(arg4, args->fourthArg);
    strcpy(arg5, args->fifthArg);
    strcpy(arg6, args->sixthArg);
    strcpy(arg7, args->seventhArg);
    strcpy(arg8, args->eighthArg);

    if(verboseLevel & VERBOSE_PARSE)
    {
      st_print("execBatch(): Arg1   = %s\n", arg1);
      st_print("execBatch(): Arg2   = %s\n", arg2);
      st_print("execBatch(): Arg3   = %s\n", arg3);
      st_print("execBatch(): Arg4   = %s\n", arg4);
      st_print("execBatch(): Arg5   = %s\n", arg5);
      st_print("execBatch(): Arg6   = %s\n", arg6);
      st_print("execBatch(): Arg7   = %s\n", arg7);
      st_print("execBatch(): Arg8   = %s\n", arg8);
      st_print("execBatch(): pToken = %d\n", lptokens);
    }

    p = &bcommands[ret];

    for(i=0;i<MAX_BATCH_LINES;i++)
    {
      if(strlen(p->lines[i]))
      {
	/* Use a local copy of the commands because we will
           be modifing the command substituting values in
           place of args */
        strcpy(tbuf, p->lines[i]);

        if(!strncmp(tbuf, "#", 1)) /* Skip comments */
        {
          continue;
        }

        if(verboseLevel & VERBOSE_PARSE)
        {
//          st_report("execBatch(): Found ntoken: %s", cp);
          st_report("execBatch(): ptokens:      %d", lptokens);
        }

        env->btokens = lptokens;

        if(lptokens < p->ntokens+1)
        {
          st_fail("%s: Wrong number of args: Needs at least: %d", name, p->ntokens);

	  st_print("%s\n", p->usage);

          return(1);
        }

        if(verboseLevel & VERBOSE_BATCH)
        {
          st_report("execBatch(): Before Parsing: %s", tbuf);
        }

        n = batch_get_tok(tbuf);

        if(verboseLevel & VERBOSE_BATCH)
        {
          st_report("execBatch(): Found %d tokens in this line", n);
        }

	bzero(tbuf, sizeof(tbuf));

        for(j=0;j<n;j++)
        {
  	/* Substitue args here */
          if(verboseLevel & VERBOSE_BATCH)
          {
            st_report("execBatch(): Comparing: %s ", batch_tok[j]);
          }

          if(!strcasecmp(batch_tok[j], "secondArg"))
          {
            strcat(tbuf, arg2);
            strcat(tbuf, " ");

            if(verboseLevel & VERBOSE_BATCH)
            {
              st_report("execBatch(): #2 %s", tbuf);
            }
          }
	  else
          if(!strcasecmp(batch_tok[j], "thirdArg"))
          {
            strcat(tbuf, arg3);
            strcat(tbuf, " ");

            if(verboseLevel & VERBOSE_BATCH)
            {
              st_report("execBatch(): #3 %s", tbuf);
            }
          }
	  else
          if(!strcasecmp(batch_tok[j], "fourthArg"))
          {
            strcat(tbuf, arg4);
            strcat(tbuf, " ");

            if(verboseLevel & VERBOSE_BATCH)
            {
              st_report("execBatch(): #4 %s", tbuf);
            }
          }
	  else
          if(!strcasecmp(batch_tok[j], "fifthArg"))
          {
            strcat(tbuf, arg5);
            strcat(tbuf, " ");

            if(verboseLevel & VERBOSE_BATCH)
            {
              st_report("execBatch(): #5 %s", tbuf);
            }
          }
	  else
          if(!strcasecmp(batch_tok[j], "sixthArg"))
          {
            strcat(tbuf, arg6);
            strcat(tbuf, " ");

            if(verboseLevel & VERBOSE_BATCH)
            {
              st_report("execBatch(): #6 %s", tbuf);
            }
          }
	  else
          if(!strcasecmp(batch_tok[j], "seventhArg"))
          {
            strcat(tbuf, arg7);
            strcat(tbuf, " ");

            if(verboseLevel & VERBOSE_BATCH)
            {
              st_report("execBatch(): #7 %s", tbuf);
            }
          }
	  else
          if(!strcasecmp(batch_tok[j], "eightArg"))
          {
            strcat(tbuf, arg8);
            strcat(tbuf, " ");

            if(verboseLevel & VERBOSE_BATCH)
            {
              st_report("execBatch(): #8 %s", tbuf);
            }
          }
	  else
	  {
            strcat(tbuf, batch_tok[j]);
            strcat(tbuf, " ");
          }
        }

        if(verboseLevel & VERBOSE_BATCH)
        {
          st_report("execBatch(): After reassemble: %s", tbuf);
        }

        if((cp = strcasestr(tbuf, "stackscan")))
        {
          sprintf(scanBuf, "%.2f", env->stackscan);

          strcpy(cp, scanBuf);

          if(verboseLevel & VERBOSE_BATCH)
          {
            st_report("execBatch(): %s", tbuf);
          }
        }

        if(strstr(tbuf, "startstack"))
        {
          loophead = i;

          if(verboseLevel & VERBOSE_BATCH)
          {
            st_report("execBatch(): Found Top of Stack Loop: indx %d", loophead);
          }

          continue;
        }

        if(strstr(tbuf, "endstack"))
        {
          if(verboseLevel & VERBOSE_BATCH)
          {
            st_report("execBatch(): Found End of Stack Loop");
          }

          if(env->stackdone) /* Test for end of stack */
          {
	    strcpy(tbuf, "#");  /* Make it a empty command */
          }
          else
          {
            i = loophead;
            continue;
          }
        }

        if(strstr(tbuf, "pause"))
        {
	  if(doPause() == 2)
          {
            return(0);
          }

          continue;
        }

        if(verboseLevel & VERBOSE_BATCH)
        {
          st_report("Parsing: %s", tbuf);
        }

        parseCmd(tbuf);

        if(pause)
        {
	  if(doPause() == 2)
          {
            return(0);
          }
        }
      }
    }
  }
  else
  {
    st_fail("execBatch(): Batch procedure: %s, not found", name);
  }

  return(0);
}


int printBatch(name)
char *name;
{
  int i, ret;
  static struct BATCH_COMMAND *p = NULL;

  ret = isBatch(name);

  if(ret >= 0)
  {
    p = &bcommands[ret];
    for(i=0;i<MAX_BATCH_LINES;i++)
    {
      if(strlen(p->lines[i]))
      {
        st_print("%s\n", p->lines[i]);
      }
    }
  }
  else
  {
    st_fail("printBatch(): Batch procedure: %s, not found", name);
  }

  return(0);
}


int isBatch(name)
char *name;
{
  int i;
  static struct BATCH_COMMAND *p = NULL;

  p = &bcommands[0];
  for(i=0;i<MAX_BATCH_COMMANDS;i++,p++)
  {
    if(p->used && (!strcmp(p->name, name) || !strcmp(p->fname, name)))
    {
      return(i);
    }
  }

  return(-1);
}



int listBatch()
{
  int i;
  static struct BATCH_COMMAND *p = NULL;

  st_print(" Slot:      Filename        Procedure    n-Lines   nTokens    Usage\n\n");

  p = &bcommands[0];
  for(i=0;i<MAX_BATCH_COMMANDS;i++,p++)
  {
    if(p->used)
    {
      st_print("%3d:%16.16s%16.16s      %4d        %d       '%s'\n", 
			i, p->fname, p->name, p->nlines, p->ntokens, p->usage);
    }
  }

  return(0);
}


int findAmbiguousBatch(arg, buf)
char *arg;
char *buf;
{
  int i, len, found=0;
  char tbuf[256];
  static struct BATCH_COMMAND *p = NULL;

  p = &bcommands[0];

  len = strlen(arg);

  for(i=0;i<MAX_BATCH_COMMANDS;i++,p++)
  {
    if(p->used)
    {
      if(!strncasecmp(p->name, arg, len))
      {
        sprintf(tbuf, "%s ", p->name);
        strcat(buf, tbuf);

        found++;
      }
    }
  }

  return(found);
}



struct BATCH_COMMAND *getFreeBatch(name)
char *name;
{
  int i;
  static struct BATCH_COMMAND *p = NULL;

  /* First make sure this procedure isn't already named */
  p = &bcommands[0];
  for(i=0;i<MAX_BATCH_COMMANDS;i++,p++)
  {
    if(p->used && (!strcmp(p->name, name) || !strcmp(p->fname, name)))
    {
      st_report("Re-Loading Procedure %s into Slot %d", name, i);
      return(p);
    }
  }

  /* OK, its a new one */
  p = &bcommands[0];
  for(i=0;i<MAX_BATCH_COMMANDS;i++,p++)
  {
    if(!p->used)
    {
      st_report("Loading New Procedure %s into Slot %d", name, i);
      return(p);
    }
  }

  p = (struct BATCH_COMMAND *)NULL;

  return(p);
}



int doEnd()
{
  st_report("End of procedure reached");

  return(0);
}
