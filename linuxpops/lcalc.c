/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <setjmp.h>
#include <math.h>

//#include "calc/zmath.h"
//#include "calc/calc.h"

/* Define these so you don't have to include calc.h which
   seems to be #include <calc/have_*.h> files that don't
   exist */

#if !defined(TRUE)
#define TRUE    (1)                      /* booleans */
#endif
#if !defined(FALSE)
#define FALSE   (0)
#endif

#define E_FUNC extern

E_FUNC void libcalc_call_me_first(void);
E_FUNC void initialize(void);
E_FUNC int openstring(char *str, size_t num);
E_FUNC void math_divertio(void);
E_FUNC void getcommands(int toplevel);
E_FUNC void closeinput(void);
E_FUNC char *math_getdivertedio(void);

/* End of calc.h defines */

/*
Name	Args	Description

abs            1-2   absolute value within accuracy b
acos           1-2   arccosine of a within accuracy b
acosh          1-2   inverse hyperbolic cosine of a within accuracy b
acot           1-2   arccotangent of a within accuracy b
acoth          1-2   inverse hyperbolic cotangent of a within accuracy b
acsc           1-2   arccosecant of a within accuracy b
acsch          1-2   inverse csch of a within accuracy b
appr           1-3   approximate a by multiple of b using rounding c
asec           1-2   arcsecant of a within accuracy b
asech          1-2   inverse hyperbolic secant of a within accuracy b
asin           1-2   arcsine of a within accuracy b
asinh          1-2   inverse hyperbolic sine of a within accuracy b
atan           1-2   arctangent of a within accuracy b
atan2          2-3   angle to point (b,a) within accuracy c
atanh          1-2   inverse hyperbolic tangent of a within accuracy b
ceil           1     smallest integer greater than or equal to number
cmp            2     compare values returning -1, 0, or 1
cos            1-2   cosine of value a within accuracy b
cosh           1-2   hyperbolic cosine of a within accuracy b
cot            1-2   cotangent of a within accuracy b
coth           1-2   hyperbolic cotangent of a within accuracy b
csc            1-2   cosecant of a within accuracy b
csch           1-2   hyperbolic cosecant of a within accuracy b
exp            1-2   exponential of value a within accuracy b
factor         1-3   lowest prime factor < b of a, return c if error
fcnt           2     count of times one number divides another
fact           1     factorial
floor          1     greatest integer less than or equal to number
frac           1     fractional part of value
hmean          0+    harmonic mean of values
hnrmod         4     v mod h*2^n+r, h>0, n>0, r = -1, 0 or 1
hypot          2-3   hypotenuse of right triangle within accuracy c
ilog           2     integral log of a to integral base b
ilog10         1     integral log of a number base 10
ilog2          1     integral log of a number base 2
int            1     integer part of value
inverse        1     multiplicative inverse of value
iroot          2     integer b'th root of a
isprime        1-2   whether a is a small prime, return b if error
isqrt          1     integer part of square root

jacobi         2     -1 => a is not quadratic residue mod b 1 => b is composite, or a is quad residue of b
join           1+    join one or more lists into one list
lcm            1+    least common multiple
lcmfact        1     lcm of all integers up till number
lfactor        2     lowest prime factor of a in first b primes
links          1     links to number or string value
list           0+    create list of specified values
ln             1-2   natural logarithm of value a within accuracy b
log            1-2   base 10 logarithm of value a within accuracy b
lowbit         1     low bit number in base 2 representation
ltol           1-2   leg-to-leg of unit right triangle (sqrt(1 - a^2))
makelist       1     create a list with a null elements
matdim         1     number of dimensions of matrix
matfill        2-3   fill matrix with value b (value c on diagonal)
matmax         2     maximum index of matrix a dim b
matmin         2     minimum index of matrix a dim b
matsum         1     sum the numeric values in a matrix
mattrace       1     return the trace of a square matrix
mattrans       1     transpose of matrix
max            0+    maximum value
memsize        1     number of octets used by the value, including overhead
meq            3     whether a and b are equal modulo c
min            0+    minimum value
minv           2     inverse of a modulo b
mmin           2     a mod b value with smallest abs value
mne            3     whether a and b are not equal modulo c
mod            2-3   residue of a modulo b, rounding type c
modify         2     modify elements of a list or matrix
name           1     name assigned to block or file
near           2-3   sign of (abs(a-b) - c)
newerror       0-1   create new error type with message a
nextcand       1-5   smallest value == d mod e > a, ptest(a,b,c) true
nextprime      1-2   return next small prime, return b if err
norm           1     norm of a value (square of absolute value)
null           0+    null value
num            1     numerator of fraction
ord            1     integer corresponding to character value
isupper        1     whether character is upper case
islower        1     whether character is lower case
isalnum        1     whether character is alpha-numeric
isalpha        1     whether character is alphabetic
iscntrl        1     whether character is a control character
isdigit        1     whether character is a digit
isgraph        1     whether character is a graphical character
isprint        1     whether character is printable
ispunct        1     whether character is a punctuation
isspace        1     whether character is a space character
isxdigit       1     whether character is a hexadecimal digit
param          1     value of parameter n (or parameter count if n is zero)
perm           2     permutation number a!/(a-b)!
prevcand       1-5   largest value == d mod e < a, ptest(a,b,c) true
prevprime      1-2   return previous small prime, return b if err
pfact          1     product of primes up till number
pi             0-1   value of pi accurate to within epsilon
pix            1-2   number of primes <= a < 2^32, return b if error
places         1-2   places after "decimal" point (-1 if infinite)
pmod           3     mod of a power (a ^ b (mod c))
polar          2-3   complex value of polar coordinate (a * exp(b*1i))
poly           1+    evaluates a polynomial given its coefficients or coefficient-list
pop            1     pop value from front of list
popcnt         1-2   number of bits in a that match b (or 1)
power          2-3   value a raised to the power b within accuracy c
protect        1-3   read or set protection level for variable
ptest          1-3   probabilistic primality test
printf         1+    print formatted output to stdout
prompt         1     prompt for input line using value a
push           1+    push values onto front of list
putenv         1-2   define an environment variable
quo            2-3   integer quotient of a by b, rounding type c
quomod         4-5   set c and d to quotient and remainder of a divided by b
rand           0-2   additive 55 random number [0,2^64), [0,a), or [a,b)
randbit        0-1   additive 55 random number [0,2^a)
random         0-2   Blum-Blum-Shub random number [0,2^64), [0,a), or [a,b)
randombit      0-1   Blum-Blum-Sub random number [0,2^a)
randperm       1     random permutation of a list or matrix
rcin           2     convert normal number a to REDC number mod b
rcmul          3     multiply REDC numbers a and b mod c
rcout          2     convert REDC number a mod b to normal number
rcpow          3     raise REDC number a to power b mod c
rcsq           2     square REDC number a mod b
re             1     real part of complex number
remove         1     remove value from end of list
reverse        1     reverse a copy of a matrix or list
rewind         0+    rewind file(s)
rm             1+    remove file(s), -f turns off no-such-file errors
root           2-3   value a taken to the b'th root within accuracy c
round          1-3   round value a to b number of decimal places
rsearch        2-4   reverse search matrix or list for value b starting at index c
runtime        0     user and kernel mode cpu time in seconds
saveval        1     set flag for saving values
scale          2     scale value up or down by a power of two
scan           1+    scan standard input for assignment to one or more variables
scanf          2+    formatted scan of standard input for assignment to variables
search         2-4   search matrix or list for value b starting at index c
sec            1-2   sec of a within accuracy b
sech           1-2   hyperbolic secant of a within accuracy b
seed           0     return a 64 bit seed for a psuedo-random generator
segment        2-3   specified segment of specified list
select         2     form sublist of selected elements from list
setbit         2-3   set specified bit in string
sgn            1     sign of value (-1, 0, 1)
sha1           0+    Secure Hash Algorithm (SHS-1 FIPS Pub 180-1)
sin            1-2   sine of value a within accuracy b
sinh           1-2   hyperbolic sine of a within accuracy b
size           1     total number of elements in value
sizeof         1     number of octets used to hold the value
sleep          0-1   suspend operation for a seconds
sort           1     sort a copy of a matrix or list
sqrt           1-3   square root of value a within accuracy b
srand          0-1   seed the rand() function
srandom        0-4   seed the random() function
ssq            1+    sum of squares of values
stoponerror    0-1   assign value to stoponerror flag
str            1     simple value converted to string
strtoupper     1     Make string upper case
strtolower     1     Make string lower case
strcat         1+    concatenate strings together
strcmp         2     compare two strings
strcasecmp     2     compare two strings case independent
strcpy         2     copy string to string
strerror       0-1   string describing error type
strlen         1     length of string
strncmp        3     compare strings a, b to c characters
strncasecmp    3     compare strings a, b to c characters case independent
strncpy        3     copy up to c characters from string to string
strpos         2     index of first occurrence of b in a
strprintf      1+    return formatted output as a string
strscan        2+    scan a string for assignments to one or more variables
strscanf       2+    formatted scan of string for assignments to variables
substr         3     substring of a from position b for c chars
sum            0+    sum of list or object sums and/or other terms
swap           2     swap values of variables a and b (can be dangerous)
system         1     call Unix command
systime        0     kernel mode cpu time in seconds
tail           2     retain list of specified number at tail of list
tan            1-2   tangent of a within accuracy b
tanh           1-2   hyperbolic tangent of a within accuracy b
test           1     test that value is nonzero
time           0     number of seconds since 00:00:00 1 Jan 1970 UTC
trunc          1-2   truncate a to b number of decimal places
ungetc         2     unget char read from file
usertime       0     user mode cpu time in seconds
version        0     calc version string
xor            1+    logical xor

 */

char lcalc_tbuf[256];

/* non-zero => calc_scanerr_jmpbuf ready */
extern int calc_use_scanerr_jmpbuf;
/* for scan and parse errors */
extern jmp_buf calc_scanerr_jmpbuf;


char *lcalc(cmdbuf)
char *cmdbuf;
{
  /*int ret; */

  libcalc_call_me_first();

  if(strlen(cmdbuf))
  {
        /*
         * establish error longjump point with initial conditions
         */
     if (setjmp(calc_scanerr_jmpbuf) == 0)
     {
                /*
                 * reset/initialize the computing environment
                 */
       initialize();
       calc_use_scanerr_jmpbuf = 1;
     }

    /*ret = */
    openstring(cmdbuf, strlen(cmdbuf));
//    fprintf(stderr, "openstring() returned %d\n", ret);
  
    math_divertio();

    getcommands(FALSE);

    strcpy(lcalc_tbuf, math_getdivertedio());

//    fprintf(stderr, "lcalc(): %s", lcalc_tbuf);

    closeinput();
  }
  else
  {
    strcpy(lcalc_tbuf, "0.0");
  }

  return(lcalc_tbuf);
}

