
#include "extern.h"


int doEdit()
{
  int    /*i, n,*/ ret;
  int    indx;
  char   fname[256], cmdBuf[512], sysBuf[512], *cp;
  struct PARSE_FLAGS *p;

/* if scans being loaded matters */
#ifdef TOMMIE
  if(h0->head.scan < 1.0)
  {
    st_warn("No Scan loaded; Use 'get scan#'");
    return(0);
  }
#endif

  cp = getenv("EDITOR");

  if(!cp)
  {
    cp = "/usr/bin/vim";
  }

  p = findFirstFlag();

  if(p)
  {
    indx = p->index;    /* Save value */

    shiftFlags();       /* Remove this first flag and decriment is 'set' count */

    switch(indx)
    {
      case PF_F_FLAG:				/* Edit Gaussian Fits */

        return(0);

        break;

      case PF_G_FLAG:				/* Edit Global Fits */

	sprintf(fname, "/tmp/%s/edit.global", session);
	sprintf(cmdBuf, "cwrite %s", fname);
	parseCmd(cmdBuf);

	sprintf(sysBuf, "%s %s", cp, fname);

	system(sysBuf);

        ret = getYesNoQuit("Reload Global Environment: y/N ?");

	if(ret)
	{
	  st_report("Reloading %s", fname);

	  sprintf(cmdBuf, "load %s /i", fname);		/* with 'ignore' errors flag */
	  parseCmd(cmdBuf);
	}

        return(0);

        break;

      case PF_S_FLAG:				/* Edit Shell Fits */

        return(0);

        break;

      case PF_T_FLAG:				/* Edit Strings and Things */

        return(0);

        break;

      case PF_W_FLAG:				/* Edit Windows Array */

	if(env->winfo.nwindows == 0)
	{
	  st_report("No Windows Defined");
	  return(0);
	}

	sprintf(fname, "/tmp/%s/edit.windows", session);
	sprintf(cmdBuf, "window /w  %s", fname);
	parseCmd(cmdBuf);

	sprintf(sysBuf, "%s %s", cp, fname);

	system(sysBuf);

        ret = getYesNoQuit("Reload Windows File: y/N ?");

	if(ret)
	{
	  st_report("Reloading %s", fname);

	  sprintf(cmdBuf, "load %s /i", fname);		/* with 'ignore' errors flag */
	  parseCmd(cmdBuf);
	}

        return(0);

        break;

     default:

        printH_FlagHelp(args->firstArg);

        return(1);

        break;


    }
  }

  /* Do default action */

  return(0);
}
