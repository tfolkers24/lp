/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

/*  Revision History:
 *
 *  Feb 04, 2015:  Added Rejection Count sub-system; twf
 *
 */

struct REJECTIONS {
	int source;
	int date;
	int noint;
	int freq;
	int freqres;
	int bescans;
	int sb;
	int bkend;
	int mode;
	int ignored;
	int offset;
	int ra;
	int dec;
	int vel;
	int bogus_data;
	int bogus_tcal;
	int bogus_tsys;
	int fivept;
};
struct REJECTIONS rejections;


int incrementRejFive()
{
  rejections.fivept++;

  return(0);
}


int incrementRejNoint()
{
  rejections.noint++;

  return(0);
}


int incrementRejOffsets()
{
  rejections.offset++;

  return(0);
}


int incrementRejFreqres()
{
  rejections.freqres++;

  return(0);
}


int incrementRejRa()
{
  rejections.ra++;

  return(0);
}


int incrementRejDec()
{
  rejections.dec++;

  return(0);
}


int incrementRejSb()
{
  rejections.sb++;

  return(0);
}


int incrementRejFreq()
{
  rejections.freq++;

  return(0);
}


int incrementRejVel()
{
  rejections.vel++;

  return(0);
}


int incrementRejMode()
{
  rejections.mode++;

  return(0);
}

int incrementRejIgnored()
{
  rejections.ignored++;

  return(0);
}


int incrementRejSource()
{
  rejections.source++;

  return(0);
}


int incrementRejScans()
{
  rejections.bescans++;

  return(0);
}


int incrementRejDates()
{
  rejections.date++;

  return(0);
}


int incrementRejBackend()
{
  rejections.bkend++;

  return(0);
}


int incrementBogusData()
{
  rejections.bogus_data++;

  return(0);
}


int incrementBogusTsys()
{
  rejections.bogus_tsys++;

  return(0);
}


int incrementBogusTcal()
{
  rejections.bogus_tcal++;

  return(0);
}


int resetRejections()
{
  bzero((char *)&rejections, sizeof(rejections));

  return(0);
}


int setQRcolor(n)
int n;
{
  if(n)
  {
    setColor(ANSI_RED);
  }
  else
  {
    setColor(ANSI_BLACK);
  }

  return(n);
}


int printRejections()
{
  int total;

  if(rejections.source)
  {
    st_report("%5d Scans Rejected for Wrong Source (%s)",  setQRcolor(rejections.source), env->source);
  }

  if(rejections.ra)
  {
    st_report("%5d Scans Rejected for Wrong RA",             setQRcolor(rejections.ra));
  }

  if(rejections.dec)
  {
    st_report("%5d Scans Rejected for Wrong Dec",            setQRcolor(rejections.dec));
  }

  if(rejections.vel)
  {
    st_report("%5d Scans Rejected for Wrong Velocity",       setQRcolor(rejections.vel));
  }

  if(rejections.noint)
  {
    st_report("%5d Scans Rejected for Wrong Noint",          setQRcolor(rejections.noint));
  }

  if(rejections.date)
  {
    st_report("%5d Scans Rejected for Wrong date (%9.4f)", setQRcolor(rejections.date), env->date);
  }

  if(rejections.freq)
  {
    st_report("%5d Scans Rejected for Wrong Freq (%.6f)",  setQRcolor(rejections.freq), env->freq);
  }

  if(rejections.freqres)
  {
    st_report("%5d Scans Rejected for Wrong Freqres (%.3f)", setQRcolor(rejections.freqres), env->bkres);
  }

  if(rejections.sb)
  {
    st_report("%5d Scans Rejected for Wrong Sideband",    setQRcolor(rejections.sb));
  }

  if(rejections.bkend)
  {
    st_report("%5d Scans Rejected for Wrong Backend (%s)",     setQRcolor(rejections.bkend), env->backend);
  }

  if(rejections.mode)
  {
    st_report("%5d Scans Rejected for Wrong Mode",        setQRcolor(rejections.mode));
  }

  if(rejections.offset)
  {
    st_report("%5d Scans Rejected for Wrong Map Offset",  setQRcolor(rejections.offset));
  }

  if(rejections.fivept)
  {
    st_report("%5d Scans Rejected for Being FivePt",      setQRcolor(rejections.fivept));
  }

  if(rejections.bogus_data)
  {
    st_report("%5d Scans Rejected for Bogus Data",        setQRcolor(rejections.bogus_data));
  }

  if(rejections.bogus_tsys)
  {
    st_report("%5d Scans Rejected for Bogus Tsys (> %.1f)",        setQRcolor(rejections.bogus_tsys), env->tsyslimit);
  }

  if(rejections.bogus_tcal)
  {
    st_report("%5d Scans Rejected for Bogus Tcal (> %.1f)",        setQRcolor(rejections.bogus_tcal), env->tcallimit);
  }

  if(rejections.bescans)
  {
    st_report("%5d Scans Rejected for Out of Scan Range (%.2f - %.2f)", 
						setQRcolor(rejections.bescans), env->bscan, env->escan);
  }

  if(rejections.ignored)
  {
    st_report("%5d Scans Rejected for Being Ignored",     setQRcolor(rejections.ignored));
  }

  setColor(ANSI_BLACK);

  total  = rejections.source;
  total += rejections.ra;
  total += rejections.dec;
  total += rejections.vel;
  total += rejections.date;
  total += rejections.noint;
  total += rejections.freq;
  total += rejections.freqres;
  total += rejections.sb;
  total += rejections.bkend;
  total += rejections.mode;
  total += rejections.offset;
  total += rejections.bogus_data;
  total += rejections.bogus_tsys;
  total += rejections.bogus_tcal;

  total += rejections.fivept;

  total += rejections.bescans;
  total += rejections.ignored;

  st_report("----------------------------------------------");

  st_report("%5d Scans out of %d, Rejected", total, env->numofscans);
  st_report("%5d Scans Accumulated", env->last_scan_count);
  st_report("%5d Total Accounted For", total+env->last_scan_count);

  return(0);
}

