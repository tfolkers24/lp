/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

/* Perform the actual shift.  'ashift' contains the number
   of channels to shift +/-; left or right.  ashift must be
   and interger number of channels */

int shift()
{
  int i, n, nn;
  float thold[MAX_CHANS], *f1, *f2;

  if(env->ashift == 0 || env->ashift < -1000 || env->ashift > 1000)
  {
    st_fail("shift(): ASHIFT %d out of range -300 to 300; but not 0", env->ashift);
    return(1);
  }

  st_report("Shifting data %d channels", env->ashift);

  n = h0->head.noint;

  bzero((char *)&thold[0], sizeof(thold));

  if(env->ashift < 0)
  {
    f1 = &thold[0];
    f2 = &h0->yplot1[abs(env->ashift)];

    nn = n - abs(env->ashift);

    st_report("Moving a total of %d channels left", nn);

    for(i=0;i<nn;i++, f1++, f2++)
    {
      *f1 = *f2;
    }

    bzero((char *)h0->yplot1, sizeof(h0->yplot1));
    bcopy((char *)&thold[0], (char *)h0->yplot1, sizeof(h0->yplot1));

    h0->head.restfreq -= (double)env->ashift*h0->head.freqres;
    h0->head.obsfreq  -= (double)env->ashift*h0->head.freqres;

    setColor(ANSI_RED);

    if(isSet(PF_D_FLAG))
    {
      st_report("Dropping %d Channels From End", abs(env->ashift));
      env->edrop = abs(env->ashift);
    }
    else
    {
      st_report("Recommend  'edrop=%d'", abs(env->ashift));
    }

    setColor(ANSI_BLACK);
  }
  else
  {
    f1 = &thold[env->ashift];
    f2 = &h0->yplot1[0];

    nn = n - env->ashift;

    st_report("Moving a total of %d channels Right", nn);

    for(i=0;i<nn;i++, f1++, f2++)
    {
      *f1 = *f2;
    }

    bzero((char *)h0->yplot1, sizeof(h0->yplot1));
    bcopy((char *)&thold[0], (char *)h0->yplot1, sizeof(h0->yplot1));

    h0->head.restfreq -= (double)env->ashift*h0->head.freqres;
    h0->head.obsfreq  -= (double)env->ashift*h0->head.freqres;

    setColor(ANSI_RED);

    if(isSet(PF_D_FLAG))
    {
      st_report("Dropping %d Channels From Start", abs(env->ashift));
      env->bdrop = abs(env->ashift);
    }
    else
    {
      st_report("Recommend  'bdrop=%d'", env->ashift);
    }

    setColor(ANSI_BLACK);
  }

  if(notSet(PF_D_FLAG))
  {
    st_report("NOTE: Be sure to '[e|b]drop' the 'shifted' %d chans", env->ashift);
  }

  h0->shifted = env->ashift;

  return(0);
}


int doShift()
{
  return(shift());
}


int compAutoShift()
{
  int f, idiff, ret=0;
  double f1, f2, fdiff;
  struct LP_IMAGE_INFO *first, *second;

  if(strlen(args->secondArg))
  {
    f = parseHarg(args->secondArg);

    if((f >= 0 && f < MAX_IMAGES))
    {
      first  = findImage(f);
      second = h0;

      if(first == second)
      {
        st_fail("autoShift(): First and Second Holders must be different");
        return(1);
      }

      f1 = first->head.restfreq;
      f2 = second->head.restfreq;

      st_report("%s Freq: = %f, %s Freq: = %f", first->name, f1, second->name, f2);

      fdiff = f2 - f1; /* Diff between current and designated freq */
      st_report("diff = %.2f MHz", fdiff);

      idiff = (int)round(fdiff / first->head.freqres);

      if(isSet(PF_O_FLAG)) /* We are shifting the image's into alignment */
      {
        idiff *= -2;

        st_report("Setting Opposite ashift to %d", idiff);
      }
      else
      {
        st_report("Setting ashift to %d", idiff);
      }

      env->ashift = idiff;

      if(env->ashift)
      {
        ret = doShift();
      }
    }
  }

  return(ret);
}
