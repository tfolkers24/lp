/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

int addIgnore(scan)
double scan;
{
  int i;

  for(i=0;i<MAX_IGNORED;i++)
  {
    if(env->ignoredscans[i] == 0.0)
    {
      st_report("Added %.2f to Ignored Scan Array", scan);
      env->ignoredscans[i] = scan;
      return(0);
    }
  }

  st_fail("Ignore Stack Full");

  return(1);
}




int removeIgnore(scan)
double scan;
{
  int i;

  for(i=0;i<MAX_IGNORED;i++)
  {
    if(fabs(env->ignoredscans[i] - scan) < 0.005)
    {
      st_report("Removing %.2f from Ignored Scan Array", scan);
      env->ignoredscans[i] = 0.0;
      return(0);
    }
  }

  st_fail("Scan %.2f not found in removeIgnore()", scan);

  return(1);
}



int ignoreScans()
{
  int i;
  double scan;

  if(isSet(PF_C_FLAG))
  {
    clearIgnores();

    return(0);
  }

  if(isSet(PF_L_FLAG))
  {
    tellIgnored();
    return(0);
  }

  for(i=1;i<MAX_TOKENS;i++)
  {
    if(strlen(args->tok[i]))
    {
      scan = atof(args->tok[i]);

      if(scan > 0.0)
      {
        addIgnore(scan);
      }
      else
      if(scan < 0.0)
      {
	removeIgnore(scan * -1.0);
      }
      else
      {
        /*st_fail("Can't Add/Remove scan %.2f", scan)*/ ;
      }
    }
    else
    {
      break;
    }
  }

  if(isSet(PF_V_FLAG))
  {
    tellIgnored();
  }

  return(0);
}


int tellIgnored()
{
  int i;

  st_print("%s # Ignored Scans: ", prompt);

  for(i=0;i<MAX_IGNORED;i++)
  {
    if(env->ignoredscans[i] > 0.0)
    {
      st_print("%8.2f ", env->ignoredscans[i]);

      if(i > 0 && !(i%8))
      {
        st_print("\n                 ");
      }
    }
  }

  st_print("\n");

  return(0);
}


int isScanIgnored(scan)
double scan;
{
  int i;

  for(i=0;i<MAX_IGNORED;i++)
  {
    if(fabs(env->ignoredscans[i] - scan) < 0.005)
    {
      return(1);
    }
  }

  return(0);
}


int clearIgnores()
{
  st_report("Clearing out all Ignored Scans");

  bzero((char *)&env->ignoredscans, sizeof(env->ignoredscans));

  return(0);
}


int readIgnoreFile()
{
  FILE *fp;
  char *string, line[256], fname[256];
  double scan;

  if(strlen(args->secondArg) < 2)
  {
    st_warn("Missing arg for readIgnoreFile()");
    return(1);
  }

  strcpy(fname, args->secondArg);

  if((fp = fopen(fname, "r") ) <= (FILE *)0)
  {
    fprintf(stderr, "Unable to open file %s\n", fname);
    return(0);
  }

  while((string = fgets(line, sizeof(line), fp)) != NULL)
  {
    if(line[0] == '#')
    {
      continue;
    }

    deNewLineIt(line); /* Clean it up */

    st_report("Parsing: %s", line);

    scan = atof(line);

    if(scan > 0.0)
    {
      addIgnore(scan);
    }
    else
    if(scan < 0.0)
    {
      removeIgnore(scan * -1.0);
    }
    else
    {
      st_fail("Can't Add/Remove scan %.2f", scan);
    }
  }

  tellIgnored();

  fclose(fp);

  return(0);
}


int reviewIgnored()
{
  int    i, j = 0, ret;
  char   cmdBuf[256];
  double scan;

  st_report("Displaying Ignored Scans");

  for(i=0;i<MAX_IGNORED;i++)
  {
    scan = env->ignoredscans[i];

    if(scan != 0.0)
    {
      sprintf(cmdBuf, "get %.2f", scan);
      parseCmd(cmdBuf);

      sprintf(cmdBuf, "show");
      parseCmd(cmdBuf);

      if(notSet(PF_N_FLAG))
      {
        ret = getYesNoQuit("Restore Scan %.2f: y/N ?", scan);

        if(ret == 1) /* 'y' */
        {
          sprintf(cmdBuf, "ignore %.2f", scan * -1.0);

          parseCmd(cmdBuf);
        }
        else
        if(ret == 2) /* 'q' */
        {
          return(0);
        }
      }
      else
      {
        xdelay(1000000);
      }

      j++;
    }
  }

  st_report("Reviewed %d Ignoreed Scans", j);

  return(0);
}
