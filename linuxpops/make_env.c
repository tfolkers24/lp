/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <string.h>

#include "caclib_proto.h"
#include "defines.h"

char tok[MAX_TOKENS][MAX_TOKENS];

int get_tok(edit)
char *edit;
{
  int c = 0;
  char *token;

  bzero((char *)&tok, sizeof(tok));

  token = strtok(edit, " \t\r\n;");
  while(token != (char *)NULL)
  {
    sprintf(tok[c++], "%s", token);
    token = strtok((char *)NULL, " \t\r\n;");
  }

  return(c);
}


int makePreamble(fout)
FILE *fout;
{
  fprintf(fout, "#ifndef NGS_HELP_H\n");
  fprintf(fout, "#define NGS_HELP_H\n");

  fprintf(fout, "\n\n");

  fprintf(fout, "#include <stdlib.h>\n");
  fprintf(fout, "#include <stdio.h>\n");
  fprintf(fout, "#include <strings.h>\n");
  fprintf(fout, "#include <string.h>\n");

  fprintf(fout, "\n\n");

  fprintf(fout, "struct NGS_HELP_STRINGS {\n");
  fprintf(fout, "        char *type;\n");
  fprintf(fout, "        char *variable;\n");
  fprintf(fout, "        char *help;\n");
  fprintf(fout, "};\n");

  fprintf(fout, "\n\n");

  fprintf(fout, "struct NGS_HELP_STRINGS ngs_help[] = {\n");

  return(0);
}


int makeEnd(fout, j)
FILE *fout;
int j;
{
  fprintf(fout, "};\n");
  fprintf(fout, "#define MAX_NGS_HELP %d\n", j);
  fprintf(fout, "\n\n");
  fprintf(fout, "int fetchNGSHelp(name, buf)\n");
  fprintf(fout, "char *name, *buf;\n");
  fprintf(fout, "{\n");
  fprintf(fout, "  int i;\n");
  fprintf(fout, "\n");
  fprintf(fout, "  for(i=0;i<MAX_NGS_HELP;i++)\n");
  fprintf(fout, "  {\n");
  fprintf(fout, "    if(!strncmp(ngs_help[i].variable, name, strlen(name)))\n");
  fprintf(fout, "    {\n");
  fprintf(fout, "      strcpy(buf, ngs_help[i].help);\n");
  fprintf(fout, "      return(0);\n");
  fprintf(fout, "    }\n");
  fprintf(fout, "  }\n");
  fprintf(fout, "\n");
  fprintf(fout, "  strcpy(buf, \"   \");\n");
  fprintf(fout, "\n");
  fprintf(fout, "  return(1);\n");
  fprintf(fout, "}\n");
  fprintf(fout, "\n\n");
  fprintf(fout, "#endif\n");
  fprintf(fout, "\n\n");

  return(0);
}



int printRest(fout, n)
FILE *fout;
int n;
{
  int i;

  fprintf(fout, ", \t\"");

  for(i=2;i<n;i++)
  {
    if(!strcmp(tok[i], "/*!<") || !strcmp(tok[i], "/*") || !strcmp(tok[i], "*/") || !strcmp(tok[i], "//"))
    {
      continue;
    }
    else
    {
      fprintf(fout, " %s", tok[i]);
    }
  }

  fprintf(fout, "\"\t},\n");

  return(0);
}





int main()
{
  int n, j=0;
  char *string, line[256], fname[256];
  FILE *fp, *fout;

  strcpy(fname, "../include/env.h");

  if((fp = fopen(fname, "r") ) <= (FILE *)0)
  {
    fprintf(stderr, "Unable to open file %s\n", fname);
    return(0);
  }

  if((fout = fopen("env_help.c", "w") ) <= (FILE *)0)
  {
    fprintf(stderr, "Unable to open file 'env_help.c'\n");
    return(0);
  }

  makePreamble(fout);

  while((string = fgets(line, sizeof(line), fp)) != NULL)
  {
    if(line[0] == '#')
    {
      continue;
    }

    n = get_tok(line);

//    printf("N = %d; tok[0] = %s, %s, %s\n", n, tok[0], tok[1], tok[2]);

    strcmprs(tok[0]);

    if(!strcmp(tok[0], "char") || !strcmp(tok[0], "int") || !strcmp(tok[0], "double"))
    {
      fprintf(fout, "\t{ \"%s\",  \t\"%s\"", tok[0], tok[1]);
      if(n > 2)
      {
        printRest(fout, n);
      }
      else
      {
        fprintf(fout, ", \"  \" \t},\n");
      }

      j++;
    } 
    else
    {
//      printf("Skipping %s\n", tok[0]);
    }
  }

  fclose(fp);

  makeEnd(fout, j);

  return(0);
}
  
