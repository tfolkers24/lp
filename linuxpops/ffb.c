/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"


char ffb_lines[80][80];
int  max_ffb_lines;


struct FFB_CHANNELS ffb_chans[MAX_FFB_CHANS];

static char ffb_tok[40][40];

static int ffb_get_tok(edit)
char *edit;
{
  int c = 0;
  char *token;

  bzero((char *)&ffb_tok, sizeof(ffb_tok));

  token = strtok(edit, " :\n");
  while(token != (char *)NULL)
  {
    sprintf(ffb_tok[c++], "%s", token);
    token = strtok((char *)NULL, " :\n");
  }

  return(c);
}


int  printFFBChan(c, p, header)
int c, header;
struct FFB_CHANNELS *p;
{
  struct FFB_CHANNELS *pp;

  if(!p)
  {
    pp = &ffb_chans[c];
  }
  else
  {
    pp = p;
  }

  if(header)
  {
    st_print("Chan#  Chan#   Crate#  Board#   CON2        CON3        CON4\n");
  }


  st_print("%4d  %4d       %d      %2d    %8.8s    %8.8s   %8.8s\n", 
				c, pp->chan, pp->crate, pp->board, pp->con2, pp->con3, pp->con4);

  return(0);
}


int verifyFFBChans()
{
  int i, first = 1;
  struct FFB_CHANNELS *p;

  p = &ffb_chans[0];

  for(i=0;i<MAX_FFB_CHANS;i++,p++)
  {
    if(!strlen(p->con2) || !strlen(p->con3) || !strlen(p->con4))
    {
      printFFBChan(i, p, first);
      first = 0;
    }
  }

  return(0);
}


int ffb_load()
{
  int  i, j, n, bchan, echan, indx=0;
  char fname[256], line[512], *string, *cp;;
  FILE *fp;

  sprintf(fname, "%s/share/ffb_numbering.dat", pkg_home);

  if((fp = fopen(fname, "r") ) <= (FILE *)0)
  {
    st_fail("Unable to open file %s", fname);
    return(0);
  }

  bzero((char *)&ffb_chans[0], sizeof(ffb_chans));

  while((string = fgets(line, sizeof(line), fp)) != NULL)
  {
    if(line[0] == '#')
    {
//      fprintf(stderr, "ffb_load(): Skipping Comment\n");
      continue;
    }

    strcpy(ffb_lines[indx], line);
    indx++;

    n = ffb_get_tok(line);

    if(!n)
    {
//      fprintf(stderr, "ffb_load(): Skipping Enpty Line\n");
      continue;
    }

    if(!strcmp(ffb_tok[0], "CON2"))
    {
      if(n != 7)
      {
//        fprintf(stderr, "ffb_load(): NTok != 7; %d Skipping Enpty Line\n", n);
        continue;
      }
/*
 0        1       2       3         4         5        6
CON2:   1-128  513-640 1025-1152 1537-1664 2049-2176 L-yellow
 */

      for(j=1;j<6;j++)
      {
        bchan = atoi(ffb_tok[j]) -1;
        cp    = strstr(ffb_tok[j], "-");
        echan = atoi(cp+1);

//        fprintf(stderr, "ffb_load(): Loading CON2 Chans: %4d to %4d: %s\n", bchan, echan, ffb_tok[6]);
        for(i=bchan;i<echan;i++)
        {
          strcpy(ffb_chans[i].con2, ffb_tok[6]);
        }
      }
 
    }
    else
    if(!strcmp(ffb_tok[0], "CON3"))
    {
      if(n != 7)
      {
        fprintf(stderr, "ffb_load(): NTok != 7; %d Skipping Enpty Line\n", n);
        continue;
      }
/*
 0        1       2       3         4         5        6
CON3:    1-64  513-576 1025-1088 1537-1600 2049-2112 L-orange
 */

      for(j=1;j<6;j++)
      {
        bchan = atoi(ffb_tok[j]) -1;
        cp    = strstr(ffb_tok[j], "-");
        echan = atoi(cp+1);

//        fprintf(stderr, "ffb_load(): Loading CON3 Chans: %4d to %4d: %s\n", bchan, echan, ffb_tok[6]);
        for(i=bchan;i<echan;i++)
        {
          strcpy(ffb_chans[i].con3, ffb_tok[6]);
        }
      }
    }
    else
    if(!strcmp(ffb_tok[0], "CHAN"))
    {
      if(n != 8)
      {
//        fprintf(stderr, "ffb_load(): NTok != 8; %d Skipping Enpty Line\n", n);
        continue;
      }

/*
 0        1       2       3         4         5        6        7
CHAN:    1-16   513-528 1025-1040 1537-1552 2049-2064 L-brown   0
 */

      for(j=1;j<6;j++)
      {
        bchan = atoi(ffb_tok[j]) -1;
        cp    = strstr(ffb_tok[j], "-");
        echan = atoi(cp+1);

//        fprintf(stderr, "ffb_load(): Loading CHAN Chans: %4d to %4d: CON4: %s, Board: %d\n", 
//								bchan, echan, ffb_tok[6], atoi(ffb_tok[7]));
        for(i=bchan;i<echan;i++)
        {
	  ffb_chans[i].chan  = i;
	  ffb_chans[i].crate = j-1;
	  ffb_chans[i].board = atoi(ffb_tok[7]);
	
          strcpy(ffb_chans[i].con4, ffb_tok[6]);
        }
      }
    }
    else
    {
      fprintf(stderr, "ffb_load(): Skipping Keyword: %s\n", ffb_tok[0]);
      continue;
    }
  }

  fclose(fp);

  max_ffb_lines = indx;

  st_report("FFB_Load(): Loaded %d lines", indx);

  verifyFFBChans();

  return(0);
}


int printExtraChans(chan, first)
int chan, first;
{
  setColor(ANSI_RED);

  if(h0->head.noint == 1024 && chan < 1024)  /* It's possible this is one of 2 halves; 1 mHz */
  {
    printFFBChan(chan+1024, NULL, first);
  }

  if(h0->head.noint == 512 && chan < 512)  /* It's possible this is one of 4 quads; 1 mHz */
  {
    printFFBChan(chan+512,  NULL, first);
    printFFBChan(chan+1024, NULL, first);
    printFFBChan(chan+1536, NULL, first);
  }

  if(h0->head.noint == 256 && chan < 256)  /* It's possible this is one of 2 halves; 250 kHz */
  {
    printFFBChan(chan+256,  NULL, first);
  }

  if(h0->head.noint == 128 && chan < 128)  /* It's possible this is one of 4 quads; 250 kHz */
  {
    printFFBChan(chan+128, NULL, first);
    printFFBChan(chan+256, NULL, first);
    printFFBChan(chan+384, NULL, first);
  }

  setColor(ANSI_BLACK);

  return(0);
}


int doFFB()
{
  int i, c, first=1, chan;
  char hint[256];

  if(!env->issmt)
  {
    st_report("Not SMT data");
    return(0);
  }

  if(args->ntokens > 1)
  {
    c = atoi(args->secondArg);

    if(c >= 0 && c < MAX_FFB_CHANS)
    {
      printFFBChan(c, NULL, 1);
    }
    else
    {
      st_fail("FFB(): Channel %d, out of range: 0 - %d", MAX_FFB_CHANS);
      return(1);
    }

    return(0);
  }

  if(isSet(PF_A_FLAG))
  {
    for(i=0;i<MAX_FFB_CHANS;i++)
    {
      if(!(i%32))
      {
        first = 1;
      }

      printFFBChan(i, NULL, first);

      first = 0;
    }

    return(0);
  }

  if(isSet(PF_B_FLAG))
  {
    for(i=0;i<MAX_BAD_CHANS;i++)
    {
      c = env->badchans[i];

      if(c >= 0)
      {
        printFFBChan(c, NULL, first);
        first = 0;

        printExtraChans(c, first);
      }
    }

    return(0);
  }

  if(isSet(PF_T_FLAG))
  {
    st_print("\n");

    for(i=0;i<max_ffb_lines;i++)
    {
      st_print("%s", ffb_lines[i]);

      if(i == 3 || i == 11)
      {
        st_print("\n");
      }
    }

    st_print("\n");

    return(0);
  }

  if(isSet(PF_R_FLAG))
  {
    ffb_load();

    return(0);
  }

  /* Ask for a channels */

  sprintf(hint, "Click Left Mouse Button to pick Channel");

  if(!_doCCur(hint))
  {
    chan = (int)round(env->ccur);

    printFFBChan(chan, NULL, first);
    first = 0;

    printExtraChans(chan, first);
  }

  return(0);
}

