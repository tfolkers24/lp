/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"


//#define TELLLIST    "acc|env|foc|ignored|qk5|map|ewf|nsf|ondata|seq|tip"

/* I'm going to begin sorting the individual commands, by letter of course, 
   but then by freq of use.  So commands like 'q' parse out as quit even though
   'qrej' comes first in the alphabet.
 */


/* 

  NOTE: Be sure to add an entry to '../help/include/[a-z]_help.c' files for any commands added

 */

struct COMMANDS commands[] = {

/*   user        #    arg_help               function      functionStr  file  mode            help                           post */
/*  command     args                         to Execute                                                   func */
/* nargs = -1: Flag for single letter help */
/* nargs =  0: Can have multiple args or none at all. */
/* nargs =  1: No args allowed */
/* nargs >  1: Required number of args */

  {"a         ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"about     ", 0,  "                   ",         about,          "about", MODE_ALL,  "Show Version info                 ", NULL },
  {"ac        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"accum     ", 0,  "                   ", accumSelected,  "accumSelected", MODE_ALL,  "Accumulate Selected Scans         ", NULL },
  {"accaxis   ", 1,  "X-axis Y-axis      ",   doAccumArgs,    "doAccumArgs", MODE_CONT, "Set Accum Plot X, Y Axis          ", xx   },
  {"airbrush  ", 0,  "                   ",    doAirBrush,     "doAirBrush", MODE_SPEC, "Blend out a feature in Spectra    ", xx   },
  {"al        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"alias     ", 0,  "                   ",       doAlias,        "doAlias", MODE_ALL,  "Alias Sub-System                  ", NULL },
  {"alma      ", 0,  "                   ",almaCalibrator, "almaCalibrator", MODE_ALL,  "Show ALMA Calibrators or Named    ", NULL },
  {"at        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"atm       ", 0,  "                   ",         doATM,          "doATM", MODE_ALL,  "Plot the ATM profiles             ", NULL },
  {"attach    ", 2,  "cmd_file           ",      doAttach,       "doAttach", MODE_ALL,  "Fork an external program          ", NULL },
  {"au        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"aut       ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"auto      ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"autocal   ", 2,  "expect_flux        ",    setAutoCal,     "setAutoCal", MODE_CONT, "Auto Calibrate the data           ", NULL },
  {"autos     ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"autobad   ", 0,  "                   ",     doAutoBad,      "doAutoBad", MODE_ALL,  "Automatically marks bad scans     ", NULL },
  {"autoscale ", 0,  "                   ",   doAutoScale,    "doAutoScale", MODE_ALL,  "Set Both Axis to Autoscale        ", xx   },
  {"autoshift ", 2,  "Hn                 ", compAutoShift,  "compAutoShift", MODE_SPEC, "Compute # channels to shift       ", xx   },
  {"avg       ", 1,  "                   ",cmdShowAverage, "cmdShowAverage", MODE_SPEC, "Print the Average Intensity       ", NULL },

  {"b         ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"ba        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"bac       ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"baccept   ", 1,  "                   ",   doAcceptEff,    "doAcceptEff", MODE_ALL,  "Accept & Use Beam Eff Results     ", NULL },
  {"backend   ", 1,  "                   ",     doBackEnd,      "doBackEnd", MODE_ALL,  "Set Name of the Backend           ", NULL },
  {"badchan   ", 0,  "                   ", ignoreBadChan,  "ignoreBadChan", MODE_SPEC, "Use Mouse to set Bad Chans(s)     ", xx   },
  {"baseline  ", 1,  "                   ",    doBaseline,     "doBaseline", MODE_ALL,  "Remove Baselines from Current Plot", xx   },
  {"batch     ", 0,  "[fname | list]     ",       doBatch,        "doBatch", MODE_ALL,  "Batch / Procedure Function        ", NULL },
  {"beameff   ", 0,  "[Ch1][Ch2 Ch3 Ch4] ",    setBeamEff,     "setBeamEff", MODE_ALL,  "Set the Beam Eff for up to 4 Chans", NULL },
  {"benford   ", 0,  "                   ",     doBenford,      "doBenford", MODE_SPEC, "Perform A Benford Analysis of data", NULL },
  {"befd      ", 0,  "                   ",        doBefd,         "doBefd", MODE_CONT, "Perform A Beam Eff Reduction (FD) ", NULL },
  {"bk        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"bkres     ", 2,  "freq (MHz)         ",    setFreqRes,     "setFreqRes", MODE_SPEC, "Set Backend Resolution            ", NULL },
  {"bknameres ", 1,  "                   ",   doBkNameRes,    "doBkNameRes", MODE_ALL,  "Set Name and Resolution of Backend", NULL },
  {"bloop     ", 1,  "                   ",       doBloop,        "doBloop", MODE_SPEC, "Loop while Moving Baseline segs   ", xx   },
  {"bmove     ", 1,  "                   ",       doBmove,        "doBmove", MODE_SPEC, "Move a Baseline Box Edge          ", xx   },
  {"boxcar    ", 1,  "                   ",      doBoxcar,       "doBoxcar", MODE_SPEC, "Performs a Boxcar Smoothing       ", xx   },
  {"bs        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"bspline   ", 0,  "                   ",     doBspline,      "doBspline", MODE_SPEC, "Fit a BSpline to the Y-Axis data  ", xx   },
  {"bset      ", 0,  "                   ",  setBaselines,   "setBaselines", MODE_SPEC, "Set up to 32 Baselines Regions    ", xx   },
  {"bscan     ", 2,  "Begin-Scan         ",      setBscan,       "setBscan", MODE_ALL,  "Select Staring Scan to Analyze    ", NULL },
  {"bshape    ", 1,  "                   ",      doBshape,       "doBshape", MODE_SPEC, "Show the Boxcar Shape             ", xx   },
  {"burntime  ", 0,  "                   ", printBurnTime,  "printBurnTime", MODE_ALL,  "Print Scan/UT/Burn-Time/Dur       ", NULL },
  {"bzero     ", 2,  "+/-n               ",       doBzero,        "doBzero", MODE_SPEC, "Zero n chans of data from ends    ", xx   },

  {"c         ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"c1        ", 1,  "                   ",        showC1,         "showC1", MODE_ALL,  "Accumulate IF 1                   ", NULL },
  {"c12       ", 1,  "                   ",       showC12,        "showC12", MODE_ALL,  "Accumulate IF 1 & 2               ", NULL },
  {"c13       ", 1,  "                   ",       showC13,        "showC13", MODE_ALL,  "Accumulate IF 1 & 3               ", NULL },
  {"c2        ", 1,  "                   ",        showC2,         "showC2", MODE_ALL,  "Accumulate IF 2                   ", NULL },
  {"c24       ", 1,  "                   ",       showC24,        "showC24", MODE_ALL,  "Accumulate IF 2 & 4               ", NULL },
  {"c3        ", 1,  "                   ",        showC3,         "showC3", MODE_ALL,  "Accumulate IF 3                   ", NULL },
  {"c34       ", 1,  "                   ",       showC34,        "showC34", MODE_ALL,  "Accumulate IF 3 & 4               ", NULL },
  {"c4        ", 1,  "                   ",        showC4,         "showC4", MODE_ALL,  "Accumulate IF 4                   ", NULL },
  {"cb        ", 1,  "                   ",        showCb,         "showCb", MODE_ALL,  "Accumulate IF's 1 & 2             ", NULL },
  {"cc        ", 0,  "                   ",          doCC,           "doCC", MODE_SPEC, "Set Plot Axis to Channel Channel  ", xx   },
  {"cf        ", 0,  "                   ",          doCF,           "doCF", MODE_SPEC, "Set Plot Axis to Channel Freq     ", xx   },
  {"cv        ", 0,  "                   ",          doCV,           "doCV", MODE_SPEC, "Set Plot Axis to Channel Velocity ", xx   },
  {"ca        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"cancel    ", 0,  "                   ",    sendCancel,     "sendCancel", MODE_ALL,  "Send 'cancel' to xgraphics program", NULL },
  {"calmode   ", 1,  "None|Scale|Beam|eff",    setCalMode,     "setCalMode", MODE_CONT, "Set the Calibration Mode          ", NULL },
  {"ccur      ", 1,  "                   ",        doCCur,         "doCCur", MODE_SPEC, "Get Cursor Channel Number         ", NULL },
  {"ce        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"center    ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"centerall ", 1,  "                   ",     centerAll,      "centerAll", MODE_CONT, "Analyzes Lots of Maps Rows        ", NULL },
  {"centerrow ", 1,  "scan#              ",     centerRow,      "centerRow", MODE_CONT, "Analyzes A Map Center Row         ", NULL },
  {"ch        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"changes   ", 1,  "                   ",   showChanges,    "showChanges", MODE_ALL,  "Show Linuxpops Feature/Bug Fixes  ", NULL },
  {"chngres   ", 1,  "                   ",     changeRes,      "changeRes", MODE_SPEC, "Change Spectral Resolution        ", xx   },
  {"che       ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"chec      ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"check     ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"check     ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"checkfile ", 1,  "[ | less]          ",   doCheckFile,    "doCheckFile", MODE_ALL,  "Dump a listing of scans in file   ", NULL },
  {"check_sdd ", 1,  "[scan # | less]    ",    doCheckSdd,     "doCheckSdd", MODE_ALL,  "Dump Raw Info for Scan From File  ", NULL },
  {"cl        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"class     ", 1,  "                   ",       doClass,        "doClass", MODE_SPEC, "Ingest/Export Class type file     ", NULL },
  {"cle       ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"clea      ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"clear     ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"clearall  ", 0,  "                   ",   clearAllVar,    "clearAllVar", MODE_ALL,  "Clear ALL Dynamic Variables       ", NULL },
  {"clearenv  ", 1,  "                   ",      clearEnv,       "clearEnv", MODE_ALL,  "Completely Clear an Environment   ", NULL },
  {"clearvar  ", 1,  "                   ",      clearVar,       "clearVar", MODE_ALL,  "Clear a Dynamic Variable          ", NULL },
  {"co        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"codeview  ", 0,  "keyword            ",    doCodeView,     "doCodeView", MODE_ALL,  "Show the Code Segment for Function", NULL },
  {"color     ", 0,  "color              ",       doColor,        "doColor", MODE_ALL,  "Set Text Color to 'color'         ", NULL },
  {"cop       ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"copy      ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"copyenv   ", 3,  "src dest           ",       copyEnv,        "copyEnv", MODE_ALL,  "Copies Environment: src -> dest   ", NULL },
  {"copyraw   ", 0,  "filename           ",     doCopyRaw,      "doCopyRaw", MODE_ALL,  "Copies Raw data to filename       ", NULL },
  {"cp        ", 0,  "src_file dest_file ",          doCP,           "doCP", MODE_ALL,  "Copy src_file to dest_file        ", NULL },
  {"cr        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"cread     ", 2,  "configfile         ",    readConfig,     "readConfig", MODE_ALL,  "Read Configuration File           ", NULL },
  {"crisscross", 0,  "                   ",    crissCross,     "crissCross", MODE_CONT, "Process Criss Cross Map           ", NULL },
  {"cursor    ", 0,  "                   ",      doCursor,       "doCursor", MODE_SPEC, "Place a Cross-Hair and labels     ", NULL },
  {"cwrite    ", 2,  "configfile         ",   writeConfig,    "writeConfig", MODE_ALL,  "Write Configuration File          ", NULL },

/* Stopped here: 'showPartHelp' repairs */

  {"d         ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"da        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"data      ", 1,  "                   ",      showData,       "showData", MODE_ALL,  "Show Scan's Data Points           ", NULL },
  {"date      ", 0,  "[yyyy.mmdd]        ",       setDate,        "setDate", MODE_ALL,  "Select UT-Date Only               ", NULL },
  {"datestring", 0,  "                   ",  doDateString,   "doDateString", MODE_ALL,  "Place a Date on Plot              ", xx   },
  {"daymode   ", 1,  "                   ",     doDayMode,      "doDayMode", MODE_ALL,  "Switch to Bright Color Display    ", xx   },
  {"dbase     ", 0,  "                   ",       doDbase,        "doDbase", MODE_ALL,  "Entry into Database sub-system    ", NULL },
  {"dcoffset  ", 0,  "[offset]           ",      dcOffset,       "dcOffset", MODE_SPEC, "Subtract a DC offset from plot    ", xx   },
  {"de        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"deconvol  ", 1,  "                   ",    doDeConvol,     "doDeConvol", MODE_CONT, "De-Convolve Planet from Beam      ", NULL },
  {"dedit     ", 1,  "                   ",       doDedit,        "doDedit", MODE_SPEC, "Interactive Data Array Edit       ", NULL },
  {"del       ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"delfreq   ", 2,  "delta-freq         ",      setDfreq,       "setDfreq", MODE_ALL,  "Frequency Matching Tolerance      ", NULL },
  {"delay     ", 1,  "[time_in_uS]       ",       doSleep,        "doSleep", MODE_ALL,  "Pause execution time micro-secs   ", NULL },
  {"demo      ", 1,  "                   ",        doDemo,         "doDemo", MODE_ALL,  "Perform Various 'demo' functions  ", NULL },
  {"dlist     ", 0,  "                   ",   listDynVars,    "listDynVars", MODE_ALL,  "List Dynamic Variables            ", NULL },
  {"do        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"dobeam    ", 1,  "                   ",     doBeamEff,      "doBeamEff", MODE_CONT, "Perform Beam Eff on Selected Scans", NULL },
  {"done      ", 1,  "                   ",        doDone,         "doDone", MODE_ALL,  "Dummie 'done' handler             ", NULL },
  {"double    ", 2,  "double_var_name    ",  createDouble,   "createDouble", MODE_ALL,  "Create a Double Variable          ", NULL },
  {"dumpscans ", 1,  "[ | less]          ",   doDumpScans,    "doDumpScans", MODE_ALL,  "Dump a listing of scans in file   ", NULL },

  {"e         ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"edit      ", 1,  "                   ",        doEdit,         "doEdit", MODE_ALL,  "Edit any internal structure       ", NULL },
  {"else      ", 1,  "                   ",        doElse,         "doElse", MODE_ALL,  "Part of the IF function           ", NULL },
  {"en        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"end       ", 1,  "                   ",         doEnd,          "doEnd", MODE_ALL,  "End of procedure                  ", NULL },
  {"endfor    ", 1,  "                   ",      doEndFor,       "doEndFor", MODE_ALL,  "End of FOR procedure              ", NULL },
  {"endif     ", 1,  "                   ",       doEndIf,        "doEndIf", MODE_ALL,  "End of IF procedure               ", NULL },
  {"env       ", 1,  "                   ",       tellEnv,        "tellEnv", MODE_ALL,  "List The Environments Titles      ", NULL },
  {"escan     ", 2,  "Ending-Scan        ",      setEscan,       "setEscan", MODE_ALL,  "Select Ending Scan to Analyze     ", NULL },
  {"ex        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"exit      ", 1,  "                   ",      quitProg,       "quitProg", MODE_ALL,  "Exit Program                      ", NULL },
  {"expected  ", 1,  "tmb flux           ",   setExpected,    "setExpected", MODE_ALL,  "Set the Expected Tmb & Flux       ", NULL },
  {"eval      ", 0,  "                   ", doCalculation,  "doCalculation", MODE_ALL,  "Evaluate command line math        ", NULL },

  {"f         ", 1,  "Scan_Number        ",  do_F_Command,   "do_F_Command", MODE_ALL,  "Show First IF                     ", NULL },
  {"fboffset  ", 0,  "seg [tstart,tend]  ",    doFBOffset,     "doFBOffset", MODE_SPEC, "Apply T-Offset to block of chans  ", xx   },
  {"fc        ", 0,  "                   ",          doFC,           "doFC", MODE_SPEC, "Set Plot Axis to Freq Channel     ", xx   },
  {"fcur      ", 1,  "                   ",        doFCur,        "doFCCur", MODE_SPEC, "Get Cursor Frequency Value        ", NULL },
  {"feed      ", 2,  "IF-Feed            ",     setIFfeed,      "setIFfeed", MODE_ALL,  "Select IF to Analyze              ", NULL },
  {"ff        ", 0,  "                   ",          doFF,           "doFF", MODE_SPEC, "Set Plot Axis to Freq Freq        ", xx   },
  {"ffb       ", 0,  "                   ",         doFFB,          "doFFB", MODE_SPEC, "FFB Diagnositics                  ", NULL },
  {"fft       ", 0,  "[pick | cut]       ",        lp_fft,         "lp_fft", MODE_SPEC, "FFT Sub-System                    ", NULL },
  {"fi        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"focus     ", 1,  "                   ",       doFocus,        "doFocus", MODE_CONT, "Focus Analysis Routines           ", NULL },
  {"formula   ", 1,  "                   ",     doFormula,      "doFormula", MODE_ALL,  "Define a Formula                  ", NULL },
  {"findseq   ", 1,  "                   ", findSequences,  "findSequences", MODE_CONT, "Display a List of Sequences       ", NULL },
  {"fil       ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"filename  ", 1,  "filename           ",      loadFile,       "loadFile", MODE_ALL,  "Load SDD file                     ", NULL },
  {"files     ", 0,  "                   ",     showFiles,      "showFiles", MODE_ALL,  "Show Current Files                ", NULL },
  {"fillpat   ", 2,  "freq               ",   fillPattern,    "fillPattern", MODE_SPEC, "Generate sin/cos/tan/sqr wave     ", xx   },
  {"flags     ", 0,  "                   ",       doFlags,        "doFlags", MODE_ALL,  "Help on Command Flags             ", NULL  },
  {"for       ", 0,  "                   ",         doFor,          "doFor", MODE_ALL,  "FOR Function                      ", NULL },
  {"fork      ", 0,  "[create|delete]    ",        doFork,         "doFork", MODE_SPEC, "Create or Delete Hyper-Fine Label ", xx   },
  {"fr        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"fre       ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"free      ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"freex     ", 1,  "                   ",         freeX,          "freeX", MODE_ALL,  "Set X-Axis Range to Auto Scale    ", xx   },
  {"freey     ", 1,  "                   ",         freeY,          "freeY", MODE_ALL,  "Set Y-Axis Range to Auto Scale    ", xx   },
  {"freq      ", 0,  "[frequency]        ",       setFreq,        "setFreq", MODE_ALL,  "Select Frequency to Analyze       ", NULL },
  {"fv        ", 0,  "                   ",          doFV,           "doFV", MODE_SPEC, "Set Plot Axis to Freq Velocity    ", xx   },
  {"fver      ", 0,  "[version]          ",        doFver,         "doFver", MODE_ALL,  "Change the Data File Version      ", NULL },
  {"fwhm      ", 0,  "                   ",        doFWHM,         "doFWHM", MODE_ALL,  "Compute and Display FWHM          ", NULL },

  {"g         ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"g1        ", 0,  "                   ",        doCal1,         "doCal1", MODE_SPEC, "Show Latest First Filter Cal      ", NULL },
  {"g11       ", 0,  "                   ",       doCal11,        "doCal11", MODE_SPEC, "Show Latest First MAC Cal         ", NULL },
  {"g2        ", 0,  "                   ",        doCal2,         "doCal2", MODE_SPEC, "Show Latest Second Filter Cal     ", NULL },
  {"g12       ", 0,  "                   ",       doCal12,        "doCal12", MODE_SPEC, "Show Latest Second MAC Cal        ", NULL },
  {"g3        ", 0,  "                   ",        doCal3,         "doCal3", MODE_SPEC, "Show Latest Third Filters Cal     ", NULL },
  {"g4        ", 0,  "                   ",        doCal4,         "doCal4", MODE_SPEC, "Show Latest Fourth Filters Cal    ", NULL },
  {"gauss     ", 0,  "[plot]             ",       doGauss,        "doGauss", MODE_ALL,  "Compute a Gaussian and [plot]     ", NULL },
  {"get       ", 2,  "scan#              ",     p_getScan,      "p_getScan", MODE_ALL,  "Fetch Scan                        ", NULL },
  {"gfilename ", 1,  "gains_filename     ",  setGFileName,   "setGFileName", MODE_SPEC, "Set Gains filename                ", NULL },
  {"gget      ", 2,  "scan#              ",      ggetScan,       "ggetScan", MODE_SPEC, "Fetch Gains Scan                  ", NULL },
  {"gl        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"glabel    ", 0,  "                   ",      doGlabel,       "doGlabel", MODE_SPEC, "Place Gaussian Results on Plot    ", xx   },
  {"glist     ", 1,  "[/g /h /p]         ",   doDumpGains,    "doDumpGains", MODE_SPEC, "Dump a Listing of Gain Scans      ", NULL },
  {"gr        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"gracefit  ", 1,  "                   ",    doGraceFit,     "doGraceFit", MODE_ALL,  "Fit a 'nfit' LSQ-fit to data      ", NULL },
  {"greview   ", 1,  "                   ",     doGReview,      "doGReview", MODE_SPEC, "Display an Array of Scan Plots    ", NULL },
  {"gri       ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"grid      ", 1,  "[x | y | full]     ",       setGrid,        "setGrid", MODE_ALL,  "Set Plot Grid Mode                ", xx   },

  {"gmap      ", 1,  "                   ",        doGmap,         "doGmap", MODE_CONT, "Grid Map Functions                ", NULL },
  {"gmapclear ", 1,  "                   ",   doGmapClear,    "doGmapClear", MODE_CONT, "Clear out internal Gridmap data   ", NULL },
  {"gmapdive  ", 1,  "                   ",   gridMapDive,    "gridMapDive", MODE_CONT, "Plot Grid Map with varing dB's    ", NULL },
  {"gmapg     ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"gmapgauss ", 1,  "                   ",   mapGaussFit,    "mapGaussFit", MODE_CONT, "Fit Gaussian to a Gridded Map     ", NULL },
  {"gmapgr    ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"gmapgrab  ", 1,  "                   ",   gridMapGrab,    "gridMapGrab", MODE_CONT, "Force Gridmap Window to be Active ", NULL },
  {"gmapgrid  ", 1,  "                   ",   gridMapGrid,    "gridMapGrid", MODE_CONT, "Accumulate Selected CONTMAP Scans ", NULL },
  {"gmaplimits", 1,  "                   ", gridMapLimits,  "gridMapLimits", MODE_CONT, "Set / Reset Plotting Limits       ", NULL },
  {"gmapmapbase",0,  "                   ",   mapBaseline,    "mapBaseline", MODE_CONT, "Remove Baseline from Gridded Map  ", NULL },
  {"gmaprotate", 0,  "                   ",    gmapRotate,     "gmapRotate", MODE_CONT, "Rotate Gridmap and Snap images    ", NULL },
  {"gmapp     ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"gmappgauss", 1,  "                   ",  mapGaussPlot,   "mapGaussPlot", MODE_CONT, "2D Gaussian Gridded Map Center Row", NULL },
  {"gmapplot  ", 1,  "                   ",   gridMapPlot,    "gridMapPlot", MODE_CONT, "Plot 3D Gaussian Gridded Map      ", NULL },
  {"gmaps     ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"gmapslice ", 1,  "row | col          ",  gridMapSlice,   "gridMapSlice", MODE_CONT, "Display a slice of Grid Map       ", NULL },
  {"gmapstats ", 1,  "                   ",  gridMapStats,   "gridMapStats", MODE_CONT, "Display Stats of Grid Map         ", NULL },

  {"gnuplot   ", 1,  "                   ",       gnuplot,        "gnuplot", MODE_ALL,  "Print Help for Gnuplot Windows    ", NULL },

  {"gset      ", 1,  "                   ",      gsetFile,       "gsetFile", MODE_SPEC, "Set the Gains File the Same       ", NULL },
  {"gver      ", 1,  "[version]          ",        doGver,         "doGver", MODE_SPEC, "Change the Gains File Version     ", NULL },

  {"h         ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"h0        ", 1,  "                   ",     setHeader,      "setHeader", MODE_ALL,  "Set Active Working Array to H0    ", NULL },
  {"h1        ", 1,  "                   ",     setHeader,      "setHeader", MODE_ALL,  "Set Active Working Array to H1    ", NULL },
  {"h2        ", 1,  "                   ",     setHeader,      "setHeader", MODE_ALL,  "Set Active Working Array to H2    ", NULL },
  {"h3        ", 1,  "                   ",     setHeader,      "setHeader", MODE_ALL,  "Set Active Working Array to H3    ", NULL },
  {"h4        ", 1,  "                   ",     setHeader,      "setHeader", MODE_ALL,  "Set Active Working Array to H4    ", NULL },
  {"h5        ", 1,  "                   ",     setHeader,      "setHeader", MODE_ALL,  "Set Active Working Array to H5    ", NULL },
  {"h6        ", 1,  "                   ",     setHeader,      "setHeader", MODE_ALL,  "Set Active Working Array to H6    ", NULL },
  {"h7        ", 1,  "                   ",     setHeader,      "setHeader", MODE_ALL,  "Set Active Working Array to H7    ", NULL },
  {"h8        ", 1,  "                   ",     setHeader,      "setHeader", MODE_ALL,  "Set Active Working Array to H8    ", NULL },
  {"h9        ", 1,  "                   ",     setHeader,      "setHeader", MODE_ALL,  "Set Active Working Array to H9    ", NULL },
  {"ha        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"hard      ", 1,  "                   ",     printPlot,      "printPlot", MODE_ALL,  "Print PS of Most Recent Plot      ", NULL },
  {"halves    ", 1,  "                   ",      doHalves,       "doHalves", MODE_SPEC, "Sum Parallel Plots together       ", xx   },
  {"hanning   ", 1,  "                   ",     doHanning,      "doHanning", MODE_SPEC, "Perform Hanning Smoothing on data ", xx   },
  {"he        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"header    ", 1,  "                   ",    showHeader,     "showHeader", MODE_ALL,  "Display Header for Last Scan      ", NULL },
  {"help      ", 0,  "[help_topic]       ",      showHelp,       "showHelp", MODE_ALL,  "Show Help                         ", NULL },
  {"helperwait", 0,  "                   ",    helperWait,     "helperWait", MODE_ALL,  "Wait on Helper Response / timeout ", NULL },
  {"hi        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"his       ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"hist      ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"histo     ", 1,  "                   ",      setHisto,       "setHisto", MODE_ALL,  "Set plotting format to Histograph ", xx   },
  {"histogram ", 1,  "                   ",   doHistogram,    "doHistogram", MODE_SPEC, "Plot a Histogram of Spectral Data ", NULL },
  {"history   ", 1,  "                   ",   listHistory,    "listHistory", MODE_ALL,  "Show Command Line History         ", NULL },
  {"hline     ", 0,  "                   ",       doHline,        "doHline", MODE_SPEC, "Add a Horizontal Line to Plit     ", xx   },
  {"hotkeys   ", 0,  "                   ",     doHotKeys,      "doHotKeys", MODE_ALL,  "List Assigned Hot Keys            ", NULL },
  {"hload     ", 2,  "image_file         ",         hLoad,          "hLoad", MODE_ALL,  "Load image_file into H* array     ", NULL },
  {"hsave     ", 2,  "image_file         ",         hSave,          "hLoad", MODE_ALL,  "Save all H* images to image_file  ", NULL },

  {"i         ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"id        ", 1,  "                   ",        showID,         "showID", MODE_ALL,  "Print ID info                     ", NULL },
  {"if        ", 0,  "                   ",          doIf,           "doIf", MODE_ALL,  "IF Function                       ", NULL },
  {"ig        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"ignore    ", 0,  "scan-1 [scan-2 ...]",   ignoreScans,    "ignoreScans", MODE_ALL,  "Add scan(s) to ignored list       ", NULL },
  {"igfile    ", 0,  "ignore_file        ",readIgnoreFile, "readIgnoreFile", MODE_ALL,  "Populate ignored list from file   ", NULL },
  {"image     ", 1,  "                   ",       doImage,        "doImage", MODE_SPEC, "Image Sub-System                  ", NULL },
  {"in        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"int       ", 2,  "integer_var_name   ", createInteger,  "createInteger", MODE_ALL,  "Create a Integer Variable         ", NULL },
  {"invert    ", 0,  "[0 | 1]            ",     setInvert,      "setInvert", MODE_ALL,  "Invert Cont Phases or Spectra     ", xx   },

  {"j         ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },

  {"k         ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },

  {"l         ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"label     ", 0,  "[create|delete]    ",       doLabel,        "doLabel", MODE_SPEC, "Create or Delete a screen Label   ", xx   },
  {"let       ", 0,  "[args]             ",      doLetCmd,       "doLetCmd", MODE_ALL,  "Assign a value to a variable      ", NULL },
  {"li        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"lin       ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"line      ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"linearplot", 1,  "                   ",    setLinPlot,     "setLinPlot", MODE_ALL,  "Set Y-Axis to Linear Scale        ", xx   },
  {"lines     ", 1,  "                   ",      setLines,       "setLines", MODE_ALL,  "Set plotting format to Lines      ", xx   },
  {"linepts   ", 1,  "                   ",    setLinePts,     "setLinePts", MODE_ALL,  "Set plotting format to Line Points", xx   },
  {"list      ", 0,               TELLLIST,     listScans,      "listScans", MODE_ALL,  "List Selected Scans, or of type   ", NULL },
  {"lo        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"load      ", 2,  "filename           ",      readFile,       "readFile", MODE_ALL,  "Read in file of Commands          ", NULL },
  {"logging   ", 0,  "                   ",     doLogging,      "doLogging", MODE_ALL,  "Turn Logging On/Off (/o / /f)     ", NULL },
  {"logplot   ", 1,  "                   ",    setLogPlot,     "setLogPlot", MODE_ALL,  "Set Y-Axis to 10*log10(y/pk)      ", xx   },
  {"logwatch  ", 1,  "                   ",    doLogWatch,     "doLogWatch", MODE_ALL,  "Start log viewer                  ", NULL },

  {"m         ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"ma        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"mak       ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"make      ", 0,  "[capture|printer]  ",     printPlot,      "printPlot", MODE_ALL,  "Print Last Plot                   ", NULL },
  {"makehelp  ", 0,  "                   ",      makeHelp,       "makeHelp", MODE_ALL,  "Make the Help Files (Admin Only)  ", NULL },
  {"map       ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"mapangle  ", 3,  "Angle#1  Angle#2   ",   setMapAngle,    "setMapAngle", MODE_CONT, "Set Display Angle of a Gridded Map", NULL },
  {"marker    ", 0,  "[create|delate|list",      doMarker,       "doMarker", MODE_SPEC, "Place Vertical Markers on Plot    ", xx   },
  {"mbase     ", 2,  "#_of_baseline_samps",    mBaselines,     "mBaselines", MODE_CONT, "Set # of Samps for Map Baselines  ", NULL },
  {"measure   ", 1,  "                   ",     doMeasure,      "doMeasure", MODE_SPEC, "Measure Dist Between Spec Features", NULL },
  {"midrow    ", 2,  "scan#              ",    showMidRow,     "showMidRow", MODE_CONT, "Show the Middle Row of Map        ", NULL },
  {"mo        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"mode      ", 2,  "mode               ",       setMode,        "setMode", MODE_ALL,  "Set the Mode; CONT | SPEC | OTF   ", NULL },
  {"molecule  ", 1,  "                   ",      molecule,       "molecule", MODE_SPEC, "Manually Select a Line for ID     ", NULL },
  {"moment    ", 1,  "                   ",      doMoment,       "doMoment", MODE_SPEC, "Calculate the 'moment' of a region", NULL },
  {"mv        ", 0,  "                   ",          doMv,           "doMv", MODE_ALL,  "Rename or Move a file             ", NULL },

  {"n         ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"name      ", 2,  "name_of_env        ",       nameEnv,        "nameEnv", MODE_ALL,  "Set Title of Current Environment  ", NULL },
  {"nb        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"nbaseset  ", 1,  "[seg start end][/l]",    doNBaseset,     "doNBaseset", MODE_SPEC, "Set All or Some Baselines at Once ", NULL },
  {"nbset     ", 0,  "                   ",setNegBaselines,"setNegBaselines",MODE_SPEC, "Set Baseline Negative Regions     ", xx   },
  {"nget      ", 0,  "                   ",      getNSave,      "getlNSave", MODE_ALL,  "'get' nsave Scan                  ", showScan },
  {"nightmode ", 1,  "                   ",   doNightMode,    "doNightMode", MODE_ALL,  "Switch to Dark Color Display      ", xx   },
  {"normalize ", 1,  "                   ",do_contNormalize,"do_contNormalize",MODE_CONT,"Normalize Accum / Object array   ", NULL },
  {"nsac      ", 1,  "                   ",        doNsac,         "doNsac", MODE_SPEC, "Sum up a number of nsave scans    ", NULL },
  {"nutator   ", 1,  "                   ",     doNutator,      "doNutator", MODE_CONT, "Analyze the Nutator Seperation    ", NULL },
  {"nyquist   ", 1,  "                   ",     doNyquist,      "doNyquist", MODE_ALL,  "Compute nyquist based on restfreq ", NULL },

  {"o         ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"osb       ", 0,  "                   ",         doOSB,          "doOSB", MODE_SPEC, "Change Display to Other Side Band ", xx   },
  {"object    ", 0,  "                   ",      doObject,       "doObject", MODE_CONT, "Object Handling Routine           ", NULL },
  {"ons       ", 0,  "0 | 1              ",       turnOns,        "turnOns", MODE_CONT, "Turn 'ons' off or on              ", xx   },
  {"otf       ", 0,  "                   ",         doOTF,          "doOTF", MODE_OTF,  "OTF Sub-System                    ", NULL },
  {"otfdirect ", 1,  "                   ",      doOTFcmd,       "doOTFcmd", MODE_OTF,  "Send OTF Dataserv a direct command", NULL },
  {"otffix    ", 1,  "                   ",      doOTFFix,       "doOTFFix", MODE_OTF,  "Fix missing OTF Map Cells         ", NULL },
  {"otfhelp   ", 1,  "                   ",     doOTFHelp,      "doOTFHelp", MODE_OTF,  "Print OTF Help                    ", NULL },
  {"otfdisplay", 1,  "[sample_num]       ",  doOTFdisplay,   "doOTFdisplay", MODE_OTF,  "Display an individual 100 mS plot ", NULL },
  {"otfslice  ", 1,  "[col x][row y]     ",    doOTFSlice,     "doOTFSlice", MODE_OTF,  "Display a Row or Col OTF Slice    ", NULL },

  {"pa        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"paramgui  ", 1,  "                   ",        doPGUI,         "doPGUI", MODE_ALL,  "Select Params via interactive GUI ", NULL },
  {"params    ", 1,  "                   ",    showParams,     "showParams", MODE_ALL,  "Show Selected Criteria            ", NULL },
  {"page      ", 1,  "                   ",     doPageCmd,      "doPageCmd", MODE_ALL,  "Clear the plot window             ", NULL },
  {"papersize ", 0,  "[width height | - ]",   doPaperSize,    "doPaperSize", MODE_ALL,  "Set the Plot Printout Page Size   ", NULL },
  {"pause     ", 1,  "                   ",       doPause,        "doPause", MODE_ALL,  "Pause execution until Keyboard hit", NULL },
  {"pboot     ", 1,  "                   ", printLastBoot,  "printLastBoot", MODE_ALL,  "Print Latest Boot Strap Block     ", NULL },
  {"pe        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"pea       ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"peak      ", 1,  "                   ",      showPeak,       "showPeak", MODE_SPEC, "Show Peak Temperature             ", NULL },
  {"peaks     ", 1,  "                   ",     showPeaks,      "showPeaks", MODE_SPEC, "Show All Peak Temperature         ", NULL },
  {"pdfl      ", 1,  "                   ",        doPDFL,         "doPDFL", MODE_SPEC, "Ingest & Convert PDFL type file   ", NULL },
  {"pflux     ", 2,  "planet             ",showPlanetFlux, "showPlanetFlux", MODE_ALL,  "Show a Plot of Planet Flux        ", NULL },
  {"pgauss    ", 1,  "                   ",    printGauss,     "printGauss", MODE_ALL,  "Print Results of Last Gaussian Fit", NULL },
  {"phases    ", 1,  "                   ",    showPhases,     "showPhases", MODE_CONT, "Show Phases of current Scan       ", NULL },
  {"pl        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"planets   ", 1,  "                   ",     getPlanet,      "getPlanet", MODE_ALL,  "List Planet info                  ", NULL },
  {"plot      ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"plotseq   ", 1,  "                   ",     plotAccum,      "plotAccum", MODE_CONT, "Plot the Results of SEQ           ", NULL },
  {"pname     ", 0,  "                   ",suggestFileName,"suggestFileName",MODE_ALL,  "Show Pending Plot File Name       ", NULL },
  {"points    ", 1,  "                   ",     setPoints,      "setPoints", MODE_ALL,  "Set plotting format to Points     ", xx   },
  {"popbase   ", 0,  "                   ",  popBaseLines,   "popBaseLines", MODE_SPEC, "Pop BaseLines off of stack        ", NULL },
  {"pr        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"preview   ", 1,  "                   ",       pReview,        "pReview", MODE_ALL,  "Review Stack Scans in Parallel    ", NULL },
  {"print     ", 1,  "                   ",      printVar,       "printVar", MODE_ALL,  "Print a Dynamic or Header Variable", NULL },
  {"prune     ", 0,  "filename [/r]      ",     pruneFile,      "pruneFile", MODE_ALL,  "Create a Criteria based SDD file  ", NULL },
  {"ps        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"pshow     ", 2,  "scan_num           ",       doPshow,        "doPshow", MODE_SPEC, "Show Parallel Scans side by side  ", NULL },
  {"psc       ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"psc12     ", 1,  "                   ",        do_psc,         "do_psc", MODE_SPEC, "Show Parallel Accum for IF1 & IF2 ", NULL },
  {"psc13     ", 1,  "                   ",        do_psc,         "do_psc", MODE_SPEC, "Show Parallel Accum for IF1 & IF3 ", NULL },
  {"psc24     ", 1,  "                   ",        do_psc,         "do_psc", MODE_SPEC, "Show Parallel Accum for IF2 & IF4 ", NULL },
  {"psc34     ", 1,  "                   ",        do_psc,         "do_psc", MODE_SPEC, "Show Parallel Accum for IF3 & IF4 ", NULL },
  {"pstamp    ", 0,  "                   ",      doPStamp,       "doPStamp", MODE_SPEC, "Place a Postage Stamp of a region ", xx   },
  {"ptime     ", 2,  "Planet Flux UT     ",      setPTime,       "setPTime", MODE_ALL,  "Set Time for Planet Flux Calc     ", NULL },
  {"pushbase  ", 0,  "                   ", pushBaseLines,  "pushBaseLines", MODE_SPEC, "Push the defined BaseLine to stack", NULL },

  /* There is no showPartHelp for 'q' as a plan 'q' is for quit */
  {"quit      ", 1,  "                   ",      quitProg,       "quitProg", MODE_ALL,  "Quit Program                      ", NULL },
  {"q1        ", 0,  "                   ",      doQuick1,       "doQuick1", MODE_ALL,  "Show Latest First Filter IF Scan  ", NULL },
  {"q11       ", 0,  "                   ",     doQuick11,      "doQuick11", MODE_ALL,  "Show Latest First MAC IF Scan     ", NULL },
  {"q2        ", 0,  "                   ",      doQuick2,       "doQuick2", MODE_ALL,  "Show Latest Second Filter IF Scan ", NULL },
  {"q12       ", 0,  "                   ",     doQuick12,      "doQuick12", MODE_ALL,  "Show Latest Second MAC IF Scan    ", NULL },
  {"q3        ", 0,  "                   ",      doQuick3,       "doQuick3", MODE_ALL,  "Show Latest Third Filters IF Scan ", NULL },
  {"q4        ", 0,  "                   ",      doQuick4,       "doQuick4", MODE_ALL,  "Show Latest Fourth Filters IF Scan", NULL },
  {"qr        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"qre       ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"qreject   ", 1,  "                   ",printRejections,"printRejections",MODE_ALL,  "Print Scan Rejection Tally        ", NULL },
  {"qreview   ", 1,  "                   ",       qReview,        "qReview", MODE_ALL,  "Review Stack Scans in Quad mode   ", NULL },
  {"qshow     ", 2,  "scan_num           ",       doQshow,        "doQshow", MODE_SPEC, "Show Quad Array of Scans          ", NULL },

  {"r         ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"rap       ", 0,  "                   ", do_FreqSwitch,  "do_FreqSwitch", MODE_SPEC, "Process Freq-Switched Array       ", xx },
  {"re        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"rea       ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"read      ", 2,  "filename           ",      readFile,       "readFile", MODE_ALL,  "Read in file of Commands          ", NULL },
  {"readbadch ", 2,  "filename           ",readBadChanFile,"readBadChanFile",MODE_SPEC, "Read in bad channel file          ", NULL },
  {"recall    ", 0,  "                   ",   recallNSave,    "recallNSave", MODE_ALL,  "Fetch 'nsave' Scan                ", showScan },
  {"record    ", 0,  "[on|off] filename  ",    recordCmds,     "recordCmds", MODE_ALL,  "Record commands to filename       ", NULL },
  {"redo      ", 0,  "                   ",        doRedo,         "doRedo", MODE_SPEC, "Redo last undo                    ", xx   },
  {"region    ", 0,  "                   ",     regionCmd,      "regionCmd", MODE_ALL,  "Map Region Sub System             ", NULL },
  {"refresh   ", 1,  "                   ",   refreshPlot,    "refreshPlot", MODE_ALL,  "Repaint the Plot Window           ", NULL },
  {"rejection ", 0,  "rej1 rej2          ", setRejections,  "setRejections", MODE_ALL,  "Set Receiver Rejections           ", NULL },
  {"report    ", 0,  "                   ",   printReport,    "printReport", MODE_SPEC, "Print a Scan Report               ", NULL },
  {"res       ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"resetenv  ", 1,  "                   ",   resetParams,    "resetParams", MODE_ALL,  "Reset All Criteria                ", NULL },
  {"restoreenv", 1,  "                   ",    restoreEnv,     "restoreEnv", MODE_ALL,  "Restore a Saved Environment       ", NULL },
  {"review    ", 1,  "                   ",   reviewStack,    "reviewStack", MODE_ALL,  "Review and possibly Ignore Scans  ", NULL },
  {"rm        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"rms       ", 1,  "                   ",      printRMS,       "printRMS", MODE_SPEC, "Print out the current TP & rms    ", NULL },
  {"rmbase    ", 1,  "                   ",      doRmBase,       "doRmBase", MODE_SPEC, "Remove Computed Baseline          ", NULL },
  {"run       ", 2,  "filename           ",       runFile,        "runFile", MODE_ALL,  "Run commands in file              ", NULL },
  {"rungauss  ", 0,  "                   ",   runGaussian,    "runGaussian", MODE_SPEC, "Fit a Gaussian to each valid scan ", NULL },

  {"s         ", 2,  "Scan_Number        ",  do_S_Command,   "do_S_Command", MODE_ALL,  "Show Second IF                    ", NULL },
  {"sa        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"sav       ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"save      ", 1,  "                   ",     saveNSave,      "saveNSave", MODE_SPEC, "Save H? Image to nsave in Savefile", NULL },
  {"saveall   ", 2,  "configfile         ",writeAllConfig, "writeAllConfig", MODE_ALL,  "Write All Configurations to File  ", NULL },
  {"savedata  ", 1,  "                   ",      saveData,       "saveData", MODE_ALL,  "Write Data Table to File          ", NULL },
  {"saveenv   ", 1,  "                   ",       saveEnv,        "saveEnv", MODE_ALL,  "Save the Current Environment      ", NULL },
  {"sat       ", 0,  "                   ",         doSAT,          "doSAT", MODE_SPEC, "Strings And Things sub-system     ", xx   },
  {"say       ", 1,  "[comments...]      ",         doSay,          "doSay", MODE_ALL,  "Say Something in the Terminal     ", NULL },
  {"sc        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"sca       ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"scale     ", 0,  "                   ",      setScale,       "setScale", MODE_ALL,  "Set the Calibration Scale         ", NULL },
  {"scansum   ", 0,  "                   ",       scansum,        "scansum", MODE_SPEC, "Scan Summary Sub-System           ", NULL },
  {"sdd       ", 1,  "                   ",         doSDD,          "doSDD", MODE_ALL,  "Ingest/Export older SDD type file ", NULL },
  {"se        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"sel       ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"selfrej   ", 2,  "ratio              ",       selfRej,        "selfRej", MODE_ALL,  "Auto Reject if 'ratio' > rms/trms ", NULL },
  {"selprint  ", 0,  "[land|bsize|cpixx] ",    doSelPrint,     "doSelPrint", MODE_ALL,  "Print the Current X Selection     ", NULL },
  {"set       ", 0,  "[args]             ",      doLetCmd,       "doLetCmd", MODE_ALL,  "Assign a value to a variable      ", NULL },
  {"sex       ", 0,  "[args]             ",      doSexCmd,       "doSexCmd", MODE_ALL,  "Convert num to/from sexigesimal   ", NULL },
  {"sfocus    ", 0,  "scan               ",      doSfocus,       "doSfocus", MODE_SPEC, "Analylis a Spectral Line Focus    ", NULL },
  {"sfilename ", 1,  "save_filename      ",  setSFileName,   "setSFileName", MODE_ALL,  "Set SAVE filename                 ", NULL },
  {"sh        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"shell     ", 0,  "                   ",       doShell,        "doShell", MODE_SPEC, "Fit Horn-type Circumstellar Shells", NULL },
  {"sho       ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"show      ", 1,  "                   ",      showScan,       "showScan", MODE_ALL,  "Process and Show Selected Scan    ", NULL },
  {"showbad   ", 1,  "                   ",    do_showBad,     "do_showBad", MODE_SPEC, "Show Bad Chans to feed into Rambo ", NULL },
  {"shift     ", 1,  "                   ",       doShift,        "doShift", MODE_SPEC, "Shift Spectra ASHIFT channels     ", xx   },
  {"shortcuts ", 1,  "                   ", showShortCuts,  "showShortCuts", MODE_ALL,  "Show All Available Hot Keys       ", NULL },
  {"sleep     ", 1,  "[time_in_secs]     ",       doSleep,        "doSleep", MODE_ALL,  "Pause execution time secs         ", NULL },
  {"source    ", 0,  "[sourcename]       ",     setSource,      "setSource", MODE_ALL,  "Select Source to Analyze          ", NULL },
  {"sp        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"spec5     ", 1,  "                   ",    doSpecFive,     "doSpecFive", MODE_SPEC, "Analysis a Spectral Five Point    ", NULL },
  {"spike     ", 1,  "                   ",       doSpike,        "doSpike", MODE_SPEC, "Remove Spikes in Data             ", NULL },
  {"spl       ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"stop      ", 1,  "                   ",        doStop,         "doStop", MODE_ALL,  "Stop a loop                       ", NULL },
  {"splat     ", 1,  "                   ",       doSplat,        "doSplat", MODE_SPEC, "Look up Molecular line near FCUR  ", NULL },
  {"splines   ", 1,  "                   ",    setSPLines,     "setSPLines", MODE_ALL,  "List Possible Line Poining Freqs  ", xx   },
  {"split     ", 1,  "[0 | 1]            ",      setSplit,       "setSplit", MODE_CONT, "Set Split Phases Flag             ", NULL },
  {"sptip     ", 2,  "scan_num           ",   cmd_doSpTip,    "cmd_doSpTip", MODE_CONT, "Process SPTIP Data                ", NULL },
  {"stack     ", 0,  "[clear|fill|loop]  ",       doStack,        "doStack", MODE_ALL,  "Stack Routines                    ", NULL },
  {"su        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"subtractimg",1,  "                   ", subtractImage,  "subtractImage", MODE_SPEC, "Subtract Hx from Hy; results in H0", NULL },
  {"summary   ", 1,  "                   ",     doSummary,      "doSummary", MODE_ALL,  "Print a Summary in Online Data    ", NULL },
  {"survey    ", 1,  "filename           ",      doSurvey,       "doSurvey", MODE_SPEC, "Write out a Survey File           ", NULL },
  {"switchenv ", 2,  "env_number         ",        setEnv,         "setEnv", MODE_ALL,  "Switch to Environment n           ", NULL },
  {"system    ", 0,  "[args]             ",      doSysCmd,       "doSysCmd", MODE_ALL,  "Do system command; alias for ':'  ", NULL },

  {"t         ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"ta        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"table     ", 1,  "                   ",     showTable,      "showTable", MODE_ALL,  "Printout Data Phase Values        ", NULL },
  {"taccept   ", 1,  "                   ",        useTau,         "useTau", MODE_CONT, "Use Sptip Result as Current Tau0  ", NULL },
  {"tau0      ", 2,  "manual_tau0        ",       setTau0,        "setTau0", MODE_CONT, "Set the Manual Zenith Tau0        ", NULL },
  {"tc        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"tcsh      ", 1,  "                   ",        doTCSH,         "doTCSH", MODE_ALL,  "Start a shell for system commands ", NULL },
  {"tcur      ", 1,  "                   ",        doTCur,         "doTCur", MODE_SPEC, "Get Cursor Temperature            ", NULL },
  {"tell      ", 0,               TELLLIST,     listScans,      "listScans", MODE_ALL,  "List Selected Scans, or of type   ", NULL },
  {"ti        ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"tips      ", 1,  "                   ",      showTips,       "showTips", MODE_CONT, "Show All SPTIP's                  ", NULL },
  {"titl      ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"title     ", 2,  "name_of_env        ",       nameEnv,        "nameEnv", MODE_ALL,  "Set Title of Current Environment  ", NULL },
  {"titleblock", 0,  "                   ",    titleBlock,     "titleBlock", MODE_SPEC, "Place a Title Block on the Plot   ", xx   },
  {"trec      ", 0,  "                   ",        doTrec,         "doTrec", MODE_SPEC, "Display T-Receiver Plot           ", NULL },
  {"tsys      ", 0,  "                   ",        doTsys,         "doTsys", MODE_ALL,  "Tsys Sub-System                   ", NULL },

  {"u         ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"ufilename ", 1,  "user_filename      ",  setUFileName,   "setUFileName", MODE_ALL,  "Set User filename                 ", NULL },
  {"unalias   ", 1,  "                   ",     doUnalias,      "doUnalias", MODE_ALL,  "Undefine an Alias                 ", NULL },
  {"undo      ", 1,  "                   ",        doUndo,         "doUndo", MODE_SPEC, "Undo last change                  ", xx   },
  {"update    ", 1,  "[12m | smt]        ",      doUpdate,       "doUpdate", MODE_ALL,  "Update the data file from the Src ", NULL },

  {"v         ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"variables ", 0,  "                   ", showVariables,  "showVariables", MODE_ALL,  "Show Useful Variable Info         ", xx   },
  {"vc        ", 0,  "                   ",          doVC,           "doVC", MODE_SPEC, "Set Plot Axis to Velocity Channel ", xx   },
  {"vcur      ", 1,  "                   ",        doVCur,         "doVCur", MODE_SPEC, "Get Cursor Velocity Value         ", NULL },
  {"vf        ", 0,  "                   ",          doVF,           "doVF", MODE_SPEC, "Set Plot Axis to Velocity Freq    ", xx   },
  {"vi        ", 0,  "                   ",          doVi,           "doVi", MODE_ALL,  "Edit a file using 'vim           '", NULL },
  {"vv        ", 0,  "                   ",          doVV,           "doVV", MODE_SPEC, "Set Plot Axis to Velocity Velocity", xx   },
  {"ver       ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"verbose   ", 0,  "[0 - 5]            ",    setVerbose,     "setVerbose", MODE_ALL,  "Set the Verbose Level             ", NULL },
  {"versions  ", 1,  "                   ",      findVers,       "findVers", MODE_ALL,  "Show List of Availble Data Files  ", NULL },

  {"w         ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"which     ", 1,  "                   ",    showParams,     "showParams", MODE_ALL,  "Show Current Environment Params   ", NULL },
  {"window    ", 0,  "                   ",      doWindow,       "doWindow", MODE_SPEC, "Define or List Spectral Windows   ", xx   },
  {"write     ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"writebadch", 2,  "filename           ", writeBadChans,  "writeBadChans", MODE_SPEC, "Save Bad Channels to file         ", NULL },
  {"writebase ", 2,  "filename           ",writeBaseLines, "writeBaseLines", MODE_SPEC, "Save Baseline segments to file    ", NULL },
  {"writeirc  ", 2,  "logfile            ",writeIRC_Record,"writeIRC_Record",MODE_SPEC, "Write a IRC+10216 Record (debug)  ", NULL },

  {"x         ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"xcorrelate", 1,  "                   ",correlateScans, "correlateScans", MODE_SPEC, "Correlate H1 & H2 Arrays          ", NULL },
  {"xgrab     ", 0,  "                   ",getPlotExtents, "getPlotExtents", MODE_ALL,  "Grab the Xgraphic Plot Extents    ", NULL },
  {"xgraphic  ", 0,  "[args]             ",  sendXgraphic,   "sendXgraphic", MODE_ALL,  "Send arbitrary cmd to Xgraphic    ", NULL },
  {"xrange    ", 3,  "x1 x2              ",     setXRange,      "setXRange", MODE_SPEC, "Set the X-Axis Range to x1, x2    ", xx   },
  {"xset      ", 1,  "                   ",      doXRange,       "doXRange", MODE_SPEC, "Use Mouse to Set the X-Range      ", xx   },
  {"xx        ", 1,  "                   ",            xx,             "xx", MODE_ALL,  "Replot Last Graph                 ", NULL },
  {"xwatch    ", 1,  "                   ",      doXwatch,       "doXwatch", MODE_ALL,  "Open term showing XGraphics.log   ", NULL },

  {"y         ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"yfactor   ", 1,  "gain_scan_num      ",     doYFactor,      "doYFactor", MODE_SPEC, "Produce Y-Factor Plot from Gains  ", NULL },
  {"yrange    ", 3,  "y1 y2              ",     setYRange,      "setYRange", MODE_ALL,  "Set the Y-Axis Range to y1, y2    ", xx   },
  {"yset      ", 1,  "                   ",      doYRange,       "doYRange", MODE_ALL,  "Use Mouse to Set the Y-Range      ", xx   },

  {"z         ",-1,  "                   ",  showPartHelp,   "showPartHelp", MODE_ALL,  "                                  ", NULL },
  {"zero      ", 0,  "                   ",        doZero,         "doZero", MODE_SPEC, "Zero a range of data points       ", xx   },
  {"zoom      ", 0,  "                   ",        doZoom,         "doZoom", MODE_SPEC, "Use Mouse to Set both X & Y Range ", xx   },

  {"@         ", 2,  "filename           ",      readFile,       "readFile", MODE_ALL,  "Read in file of Commands          ", NULL },
  {"?         ", 0,  "[help_topic]       ",      showHelp,       "showHelp", MODE_ALL,  "Show Help                         ", NULL },

  {        NULL, 0,                   NULL,             0,             NULL, MODE_ALL,   NULL                               , NULL }
};

