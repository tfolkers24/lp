#!/bin/csh
#
if ( "$1" == "" || "$2" == "" || "$3" == "" || "$4" == "" || "$5" == "" || "$6" == "" || "$7" == "" ) then
  echo "Usage: $0 fmin fmax fstep pwv temp press alt"
  exit
endif
#
set freqs=`break_freq $1 $2 8`
#
/usr/bin/parallel --jobs=8 --link --keep-order /usr/local/bin/absorption --fmin '{1}' --fmax '{2}' --fstep $3 --pwv $4 --gtemp $5 --gpress $6 --altitude $7 $freqs
#
