/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

#define CWRITE        1
#define CAPPEND       2

int cwritemode          = CWRITE;

int readConfig()
{
  FILE *fp;
  char *string, line[256];

  if((fp = fopen(args->secondArg, "r") ) <= (FILE *)0)
  {
    st_fail("Unable to open config file %s", args->secondArg);
    return(0);
  }

  st_report("Reading Configuration File: %s", args->secondArg);

  while((string = fgets(line, sizeof(line), fp)) != NULL)
  {
    if(strlen(line) < 4)
    {
      continue;
    }

    if(line[0] == '#')
    {
      continue;
    }

    deNewLineIt(line); /* Clean it up */

    st_report("%s", line);

    my_add_history(line);

    parseCmd(line);
  }

  fclose(fp);

  return(0);
}


int writeParams(fp)
FILE *fp;
{
  int i, j, ngs;
  struct GSKEY *pgs;
  extern int getSizeOfGS();
  extern struct GSKEY getset[];

  ngs = getSizeOfGS();

  pgs = &getset[0];
  for(i=0;i<ngs;i++,pgs++)
  {
    switch(pgs->type)
    {
      case INTEGER:
	if(pgs->size > 1)
        {
		;
        }
        else
	{
          fprintf(fp, "let %s = %d\n", pgs->name, *pgs->iaddr);
	}
        break;

      case DOUBLE:
	if(pgs->size > 1)
        {
	  for(j=0;j<pgs->size;j++)
          {
	    if(!strncmp(pgs->name, "ignoredscans", 12))
            {
              if(j == 0)
              {
                fprintf(fp, "ignore /c\n"); /* Force a clearing of the ignore array */
                fprintf(fp, "stquiet=1 \n"); /* We don't want to see all this on reload */
              }

              if(pgs->daddr[j] != 0.0 || j == 0)
	      {
                fprintf(fp, "ignore %8.2f\n", pgs->daddr[j]);
              }
            }
          }
        }
        else
	{
          fprintf(fp, "let %s = %20.20f\n", pgs->name, *pgs->daddr);
	}
        break;

      case CHARSTAR:
	if(strlen(pgs->caddr) > 1)
        {
          fprintf(fp, "%s %s\n", pgs->name, pgs->caddr);
        }
        break;

    }
  }

  if(env->mode == MODE_SPEC)
  {
    fprintf(fp, "nbaseset /c\n");

    for(i=0;i<MAX_BASELINES;i++)
    {
      if(env->baselines[i][0] || env->baselines[i][1])
      {
        fprintf(fp, "nbaseset %2d %4d %4d\n", i, env->baselines[i][0], env->baselines[i][1]);
      }
    }

    for(i=0;i<MAX_BAD_CHANS;i++)
    {
      if(env->badchans[i] > -1)
      {
        fprintf(fp, "badch %d\n", env->badchans[i]);
      }
    }

  }

  return(0);
}



int writeConfig()
{
  FILE *fp;
  char *rmode;

  if(cwritemode == CWRITE)
  {
    rmode = "w";
  }
  else
  if(cwritemode == CAPPEND)
  {
    rmode = "a";
  }
  else
  {
    st_fail("Unknown Configuration File Write Mode: %d", cwritemode);
    return(1);
  }


  if(strlen(args->secondArg)< 2)
  {
    st_fail("Missing Args for writeConfig()");
    return(1);
  }

  if((fp = fopen(args->secondArg, rmode) ) <= (FILE *)0)
  {
    st_fail("Unable to open config file %s", args->secondArg);
    return(0);
  }

  fprintf(fp, "\n");
  fprintf(fp, "switch %d\n", envNumber);

  writeParams(fp);

  fprintf(fp, "stquiet=0 \n");
  fprintf(fp, "ignore \n");

  fprintf(fp, "\n");

  st_report("Wrote Environment[%d] Configuration to file %s", envNumber, args->secondArg);

  fclose(fp);

  return(0);
}


int writeAllConfig()
{
  int i, save;

  if(strlen(args->secondArg)< 2)
  {
    st_fail("Missing Args for writeAllConfig()");
    return(1);
  }

  save = envNumber;

  for(i=0;i<MAX_ENV;i++)
  {
    if(i == 0)
    {
      cwritemode = CWRITE;
    }
    else
    {
      cwritemode = CAPPEND;
    }

    if(strlen(CGM->envrs[i].title))
    {
      env = &CGM->envrs[i];
      mapGS2Env();
      envNumber = i;
      writeConfig();
    }
  }

  envNumber = save;
  env = &CGM->envrs[envNumber];
  mapGS2Env();

  cwritemode = CWRITE;

  return(0);
}


int loadInitFile()
{
  char *cp;

  cp = getenv("LPSETUP");

  if(cp)
  {
    st_report("Loading Initfile: %s", cp);

    subArgs(2, cp);
    readFile();
  }

  return(0);
}
