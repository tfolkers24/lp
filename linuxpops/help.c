/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

struct HELP_STRUCT letter_help;

char highlightBuf[32], normalBuf[32];


int initLetterHelp()
{
  int i, l;
  char tbuf[256];
  struct COMMANDS *p;

  sprintf(highlightBuf, "%c[1m%c[33m", 0x1b, 0x1b);
  sprintf(normalBuf, "%c[0m%c[39m", 0x1b, 0x1b);

  bzero((char *)&letter_help, sizeof(letter_help));

  p = &commands[0];

  for(i=0;p->cmd;i++,p++) /* For each command */
  {
    for(l=0;l<26;l++)     /* For each letter possible */
    {
      if(p->ntoken == -1)
      {
        continue;
      }

      if(p->mode == MODE_ALL || p->mode == env->mode)
      {
        strcpy(tbuf, p->cmd);
	strcmprs(tbuf);
        strlwr(tbuf);

        if(tbuf[0] == l+97)
        {
          strcpy(letter_help.commands[l].commands[letter_help.commands[l].n], p->cmd);
          letter_help.commands[l].n++;
        }
      }
    }
  }

  return(0);
}



int letterHelp(h)
char *h;
{
  int i, k, ih;
  char tbuf[256];
  struct HELP_COMMANDS *p;

//  st_report("Parsing help request %s", h);

  p = &letter_help.commands[0];

  strcpy(tbuf, h);
  strlwr(tbuf);
  ih = h[0] - 97;

  for(i=0;i<26;i++,p++)
  {
    if(i == ih)
    {
      if(p->n == 0)
      {
        st_report("No help for the letter %1.1s", h);
        return(1);
      }

      st_report("There are %d commands starting with %1.1s", p->n, h);

      for(k=0;k<p->n;k++)
      {
        st_print("%s ", p->commands[k]);

        if(k == 7 || k == 15 || k == 23 || k == 31) /* Start newline */
        {
          st_print("\n");
        }
      }

      if(k != 8 && k != 16 && k != 24 && k != 32) /* Don't add another newline */
      {
        st_print("\n");
      }

      return(0);
    }
  }
  
  st_report("No help for the letter %1.1s", h);

  return(1);
}



int showPartHelp(help)
char *help;
{
  st_warn("Ambiguous Command: Possible Commands:\n\n %s", help);

  return(0);
}



int showSpecHelp(buf)
char *buf;
{
  char sysbuf[256];

  sprintf(sysbuf, "less -e %s/linuxpops/%s.hlp", help_home, buf);

  system(sysbuf);

  return(0);
}



int showAllHelp(all)
int all;
{
  struct COMMANDS *p;
  int i, j=0, hi, wd;
  char *mStrs[] = { "A", "C", "S", "O", "?" };

  getTerminalSize(&wd, &hi);

  st_print("\n");
  st_print("Command        Args                                         Mode   nArgs  Help\n");
  st_print("\n");

  p = &commands[0];
  for(i=0;p->cmd;i++,p++)
  {
    /* Only show help for current mode */
    if(!all && p->mode && p->mode != env->mode)
    {
      continue;
    }

    if(isSet(PF_P_FLAG) && j > 0 && !(j%hi))
    {
      if(getYesNoQuit("\nHit 'Return' to continue; 'q' to quit help") == 2)
      {
        return(1);
      }
      st_print("\n");
    }

    if(p->ntoken >= 0) /* Ignore all those Partial Help elements */
    {
      if((p->ntoken-1) < 0)
      {
        st_print("%s: %-50.50s %1s     [*] %s\n", p->cmd, p->args, mStrs[p->mode], p->help);
      }
      else
      {
        st_print("%s: %-50.50s %1s     [%d] %s\n", p->cmd, p->args, mStrs[p->mode], p->ntoken-1, p->help);
      }
    }

    j++;
  }

  return(0);
}


int showKhelp()
{
  struct COMMANDS *p;
  int i, j=0, hi, wd, first=1;
  char *mStrs[] = { "A", "C", "S", "O", "?" };
  char hstr[256];

  getTerminalSize(&wd, &hi);

  if(!strlen(args->secondArg))
  {
    st_fail("Not secondArg");

    printH_FlagHelp("help");

    return(1);
  }

  strcpy(hstr, args->secondArg);

  p = &commands[0];
  for(i=0;p->cmd;i++,p++)
  {
    /* Only show help for current mode */
    if(p->mode && p->mode != env->mode)
    {
      continue;
    }

    if(j > 0 && !(j%hi))
    {
      if(getYesNoQuit("\nHit 'Return' to continue; 'q' to quit help") == 2)
      {
        return(1);
      }
      st_print("\n");
    }

    if(strcasestr(p->help, hstr))
    {
      if(first)
      {
        st_print("\n");
        st_print("Command        Args                                         Mode   nArgs  Help\n");
        st_print("\n");

        first = 0;
      }

      if(p->ntoken >= 0) /* Ignore all those Partial Help elements */
      {
        if((p->ntoken-1) < 0)
        {
          st_print("%s: %-50.50s %1s     [*] %s\n", p->cmd, p->args, mStrs[p->mode], p->help);
        }
        else
        {
          st_print("%s: %-50.50s %1s     [%d] %s\n", p->cmd, p->args, mStrs[p->mode], p->ntoken-1, p->help);
        }
      }

      j++;
    }
  }

  if(first)
  {
    st_print("No commands matching %s\n", hstr);
  }

  return(0);
}


int showHelp()
{
  if(isSet(PF_K_FLAG))
  {
    showKhelp();
    return(0);
  }

  if(isSet(PF_A_FLAG) == 1)
  {
    showAllHelp(0);
    return(0);
  }

  if(isSet(PF_A_FLAG) > 1)
  {
    showAllHelp(1);
    return(0);
  }

  if(strlen(args->secondArg)) /* Help for specific topic?? */
  {
    showSpecHelp(args->secondArg);
    return(0);
  }

  printH_FlagHelp("help");

  return(0);
}


int addSupplemental(cmd, fp)
char *cmd;
FILE *fp;
{
  char tbuf[256], fname[5122], *string, line[512];
  FILE *sfp;

  strcpy(tbuf, cmd);
  strcmprs(tbuf);

  sprintf(fname, "../help/supplemental/%s.sup", tbuf);

//  fprintf(stdout, "Looking for Supplemental Helpfile: %s\n", fname);

  if((sfp = fopen(fname, "r") ) <= (FILE *)0)
  {
    return(1);
  }

//  fprintf(stdout, "Found Supplemental Helpfile: %s\n", fname);

  while((string = fgets(line, sizeof(line), sfp)) != NULL)
  {
    fprintf(fp, "%s", line);
  }

  fclose(sfp);

  return(0);
}


int makeHelp()
{
  int saveStq;
  char tbuf[256], fname[512];
  struct COMMANDS *p;
  FILE *fp, *saveFp;

  if(isSet(PF_V_FLAG))
  {
    st_report("makehelp(): Enter");
  }

  saveFp = errPrint_fp;
  saveStq = stquiet;

  stquiet = 1;

  p = &commands[0];
  for(;p->cmd;p++)
  {
    if(p->ntoken == -1)
    {
      continue;
    }

    strcpy(tbuf, p->cmd);
    strcmprs(tbuf);

    sprintf(fname, "../help/linuxpops/%s.hlp", tbuf);

    if((fp = fopen(fname, "w")) == NULL)
    {
      fprintf(stderr, "Unable to open %s\n", fname);
      continue;
    }

    if(isSet(PF_V_FLAG))
    {
      stquiet = 0;
      st_report("makehelp(): Creating file: %s", fname);
      stquiet = 1;
    }

    errPrint_fp = fp;

    printH_FlagHelp(tbuf);

    addSupplemental(p->cmd, fp);

    fclose(fp);

    errPrint_fp = saveFp;
  }

  stquiet = saveStq;

  if(isSet(PF_V_FLAG))
  {
    st_report("makehelp(): Leave");
  }

  return(0);
}


int printH_FlagHelp(cmd)
char *cmd;
{
  int i, len;
  char *cp, tbuf[256];
  struct HELP_H_FLAG *p;

  strcpy(tbuf, cmd);
  strcmprs(tbuf);

  len = strlen(tbuf);

  for(p=&help_h_flag[0]; p->cmd; p++)
  {
    if(!strncmp(p->cmd, tbuf, len))
    {
      st_print("********************************************************************************\n");

      for(i=0;i<MAX_HELP_STRINGS;i++)
      {
        cp = p->help[i];
	if(cp /* && (strlen(cp) > 1) */)
        {
	  if(strstr(cp, "CAUTION"))
	  {
	    st_print("*\t%s%s%s\n", highlightBuf, cp, normalBuf);
	  }
	  else
	  {
	    st_print("*\t%s\n", cp);
	  }
        }
      }
      st_print("********************************************************************************\n");

      return(0);
    }
  }

  st_fail("No /h help for cmd: %s", tbuf);

  return(1);
}


int showChanges()
{
  char sysbuf[256];

  st_report("Starting Document Viewer");

  sprintf(sysbuf, "%s %s/docs/LP_Changes.pdf 1> /dev/null 2>&1 &", doc_viewer, pkg_home);

  system(sysbuf);

  return(0);
}


int doFlags()
{
  printH_FlagHelp("flags");

  return(0);
}
