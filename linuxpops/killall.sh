#!/bin/bash
#
for child in $(ps -o pid,ppid -ax | \
  awk "{ if ( \$2 == $1 ) { print \$1 }}")
do
  echo "Killing child process $child because ppid = $pid"
#  kill $child
done
