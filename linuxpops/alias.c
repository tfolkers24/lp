/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */

#include "extern.h"

struct ALIAS aliases[MAX_ALIAS];

char alias_tok[MAX_TOKENS][MAX_TOKENS];
char aliastbuf[4096];

/**
 * split command up into tokens
 */
int alias_get_tok(edit)
char *edit;
{
  int c = 0;
  char *token;

  bzero((char *)&alias_tok, sizeof(alias_tok));

  token = strtok(edit, "\n\t\r\n ");
  while(token != (char *)NULL)
  {
    sprintf(alias_tok[c++], "%s", token);
    token = strtok((char *)NULL, "\n\t\r\n ");
  }

  return(c);
}


int doAlias()
{
  int i, j, ndx, first = 1;
  char buf[512], tbuf[256];

  if(verboseLevel & VERBOSE_ALIAS)
  {
    st_report("Alias(): ntokens = %d", args->ntokens);
  }

  if(isSet(PF_L_FLAG)) /* Show listing */
  {
    j = 0;
    for(i=0;i<MAX_ALIAS;i++)
    {
      if(aliases[i].used)
      {
        if(first)
        {
          st_print(" Alias      :  Substitute\n\n");
          first = 0;
        }

        st_print("%-12.12s:  %s\n", aliases[i].keyword, aliases[i].alias);

        j++;
      }
    }

    st_report("%d aliases defines out of %d", j, MAX_ALIAS);

    return(0);
  }

  if(args->ntokens > 2)
  {
    bzero(tbuf, sizeof(tbuf));

    for(i=0;i<MAX_ALIAS;i++) /* Find an empty alias slot */
    {
      if(!aliases[i].used)
      {
        ndx = i;
        break;
      }
    }

    if(i == MAX_ALIAS)
    {
      st_fail("Too many aliases defined");
      return(1);
    }
    
    strcpy(aliases[ndx].keyword, args->secondArg);

    for(i=2;i<args->ntokens;i++)
    {

      if(strstr(args->tok[i], "\\/")) /* Look for excaped flags in alias; need to gig it \\ to stop compiler warns */
      {
        sprintf(buf, "%s ", args->tok[i]+1);
      }
      else
      {
        sprintf(buf, "%s ", args->tok[i]);
      }

      strcat(tbuf, buf);
    }

    strcpy(aliases[ndx].alias, tbuf);
    aliases[ndx].used = 1;
  }
  else
  {
    printH_FlagHelp("alias");
    return(1);
  }

  return(0);
}


int doUnalias()
{
  int i;

  if(args->ntokens > 1)
  {
    for(i=0;i<MAX_ALIAS;i++)
    {
      if(aliases[i].used)
      {
        if(!strcmp(args->secondArg, aliases[i].keyword))
        {
          st_report("Alias(): Zapping alias %s", args->secondArg);

          aliases[i].used = 0;
          aliases[i].keyword[0] = '\0';
          aliases[i].alias[0] = '\0';
        }
      }
    }
  }

  return(0);
}


struct ALIAS *isAlias(buf)
char *buf;
{
  int i;

  for(i=0;i<MAX_ALIAS;i++)
  {
    if(aliases[i].used)
    {
      if(!strcmp(aliases[i].keyword, buf))
      {
        return(&aliases[i]);
      }
    }
  }

  return((struct ALIAS *)0);
}


/**
 Here we:
  1) strip a command into indiviual tokens;
  2) substitute any aliases
  3) recreate the new command
  4) Stuff it back into the origirnal buffer
 */       
int substituteAliases(buf)
char *buf;
{
  int i, n;
  char tbuf[512];
  struct ALIAS *a;

  if(verboseLevel & VERBOSE_ALIAS)
  {
    st_report("Alias(): Starting command: %s", buf);
  }

  strcpy(aliastbuf, buf); /* Make a local copy */

  n = alias_get_tok(aliastbuf);

  if(!n)
  {
    if(verboseLevel & VERBOSE_ALIAS)
    {
      st_report("Alias(): No args");
    }
    return(1);
  }

  /* substitute aliases here */
  for(i=0;i<n;i++)
  {
    if((a = isAlias(alias_tok[i])))
    {
      strcpy(alias_tok[i], a->alias);
    }
    else
    {
      if(verboseLevel & VERBOSE_ALIAS)
      {
        st_report("Alias(): Skipping %s", alias_tok[i]);
      }
    }
  }

  /* reconsitute original buffer here */
  buf[0] = '\0';

  for(i=0;i<n;i++)
  {
    sprintf(tbuf, "%s ", alias_tok[i]);

    strcat(buf, tbuf);
  }

  strcmprs(buf);

  if(verboseLevel & VERBOSE_ALIAS)
  {
    st_report("Alias(): Final command: %s", buf);
  }

  return(0);
}
