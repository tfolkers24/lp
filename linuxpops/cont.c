/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

struct ACCUM_RESULTS      accum_results[4];
struct ACCUM_RESULTS_SEND accum_results_send;
struct CONTINUUM_PLAIN    cont_plain_send;
struct CONTINUUM_SPTIP    cont_sptip_send;

int    accumArrayCnt;
int    autoAccumCnt[4];

float  azPtCorr[4], elPtCorr[4];
float  accumArrayTmb[MAX_ACCUM_SCANS];
float  accumArrayTaStar[MAX_ACCUM_SCANS];
float  accumArrayTr[MAX_ACCUM_SCANS];
float  accumArrayFlux[MAX_ACCUM_SCANS];

double autoCalibrate = 0.0;
double autoAccum[4];

#define NPHASE_2             (8)
int two_phase_seq_five[] = { -1,  1,  1, -1,
			      1, -1, -1,  1};

int two_phase_seq_foc[]  = {  1, -1,  1, -1,
			      1, -1,  1, -1};

/* Pre Summer 2017 Kitt Peak has a different phase order */
#define NPHASE_4            (16)
int four_phase_seq_five[] = { -1,  1, -1,  1,  /* Off */
			       1, -1,  1, -1,  /* On  */
			       1, -1,  1, -1,  /* On  */
			      -1,  1, -1,  1}; /* Off */


/* Computes ratio of source strength, to mean total power. */
int accum_v1(data, h, invert, ar)
float *data;
struct HEADER *h;
int invert;
struct ACCUM_RESULTS *ar;
{
  double source, ref, ons, ratio, offs, Tr, taStar, tau0, sinel;
  int i, j, npts, noffs=0, nons=0, nphase, bpts, epts;
  int *p, low_volts = 0, feed;
  double weight, div, ttime, factor, be, tauFactor, tsys;
  double gainEl;

  feed = getFeed(h->scan);

  if(feed >= 0 && feed < 5)
  {
    be = env->bm_eff[feed-1];
  }
  else
  {
    printf("Unknown Feed %d", feed);
    be = 1.0;
    return(1);
  }

  ons = offs = 0.0;
  npts = (int)h->noint;

  /* Validate data first */
  for(i=0;i<npts;i++)
  {
    if(gsl_isinf((double)data[i]))
    {
      printf("Scan %.2f contains inf values\n", h->scan);
      return(1);
    }

    if(gsl_isnan((double)data[i]))
    {
      printf("Scan %.2f contains nan values\n", h->scan);
      incrementBogusData();

      return(1);
    }

    if(data[i] < 1.0e6)
    {
      div = 1.0e3;
    }
    else
    if(data[i] > 1.0e6 || data[i] < 1.0e9)
    {
      div = 1.0e6;
    }
    else
    {
      div = 1.0e9;
    }

    if(data[i] < 1000.0)
    {
      low_volts++;
    }
  }

  /* Check if data is bad; i.e. no IF input */
  if(low_volts == npts)
  {
    printf("Scan %.2f contains near zero values\n", h->scan);
    return(1);
  }

  /* Check if tsys is bad */
  if((env->tsyslimit && (fabs(h->stsys) > env->tsyslimit)) || gsl_isnan(h->stsys))
  {
    printf("Scan %.2f Tsys Greater then %.1f: %.1f\n", h->scan, env->tsyslimit, h->stsys);
    incrementBogusTsys();

    return(1);
  }

  /* Check if tcal is bad */
  if(fabs(h->tcal) > BOGUS_TCAL_VALUE || gsl_isnan(h->tcal))
  {
    printf("Scan %.2f Tcal Bogus: %.1f\n", h->scan, h->tcal);
    incrementBogusTcal();

    return(1);
  }

  if(env->issmt)
  {
    p = &two_phase_seq_five[0];
    nphase = NPHASE_2;
  }
  else
  {
    /* Switched to 2 phase @KP in Summer of 2017 */
    if(h->nophase == 2 || h->burn_time > 1501704800.0)
    {
      p = &two_phase_seq_five[0];
      nphase = NPHASE_2;
    }
    else
    {
      p = &four_phase_seq_five[0];
      nphase = NPHASE_4;
    }
  }

  if(env->doing_fives)
  {
    bpts =  8 * nphase;
    epts = 11 * nphase;
  }
  else
  {
    bpts =    0;
    epts = npts;
  }

  j = 0;
  for(i=bpts;i<epts;i++)
  {
    data[i] /= div;		/* Scale it */

    if(p[j] > 0)
    {
      if(invert)
      {
        offs += data[i]; /* Sum the Offs */
        noffs++;
      }
      else
      {
        ons += data[i]; /* Sum the Ons */
        nons++;
      }
    }
    else
    {
      if(invert)
      {
        ons += data[i]; /* Sum the Ons */
        nons++;
      }
      else
      {
        offs += data[i]; /* Sum the Offs */
        noffs++;
      }
    }

    j++;

	/* check to see if we ran off the end of the phase array */
    if(j >= nphase)
    {
      if(env->issmt)
      {
        p = &two_phase_seq_five[0];
      }
      else
      {
        /* Switched to 2 phase @KP in Summer of 2017 */
        if(h->nophase == 2 && h->burn_time > 1501704800.0)
        {
          p = &two_phase_seq_five[0];
        }
        else
        {
          p = &four_phase_seq_five[0];
        }
      }

      j = 0;
    }
  }

  if(manual_tcal > 0.0 && fabs(h->tcal-400.0) < 1.0)
  {
    tsys = h->stsys * manual_tcal / h->tcal;
  }
  else
  {
    tsys = h->stsys;
  }

  weight = (h->noint * h->samprat) / (tsys * tsys);

  ref    = offs / (double)noffs; /* TP = Avg of offs */
  source = ons  / (double)nons;
  ratio  = (source - ref) / ref;

  /* Correction for scaling factor used in Tsys */
  taStar = tsys * ratio;

  /* assume Tatm = Tspill = 0.96 * Tamb, 
   * eta(warm spillover) = 0.94, Tbg = 3 K, Tamb = 300 */

  switch(env->calmode)
  {
    case CALMODE_NONE:
      factor = 1.0;
      break;

    case CALMODE_CALSCALE:
      factor = 1.0 / env->cal_scale;
      break;

    case CALMODE_BEAMEFF:
      factor = 1.0 / be; 
      break;

    case CALMODE_TELE_EFF:
#ifdef CALC_TAU_FACTOR
      if(fabs(env->mantau0 - 0.0) < 0.01) /* Near Zero */
      {
        tau0 = h->mmh2o / 19.0;
      }
      else
      {
        tau0 = env->mantau0;
      }

      sinel = sin(h->el / 57.296);
      tauFactor = exp(-tau0 * 1.0 / sinel);
#endif
      factor = 1.0/* / tauFactor*/;  /* Dont remove atm as atm_prog did it already */

      if(env->apperatureeff > 0.0)
      {
        factor /= env->apperatureeff;
      }

      break;

    default:
      factor = 1.0;
      break;
  }

#ifndef CALC_TAU_FACTOR
      if(fabs(env->mantau0 - 0.0) < 0.01) /* Near Zero */
      {
        tau0 = h->mmh2o / 19.0;
      }
      else
      {
        tau0 = env->mantau0;
      }

      sinel = sin(h->el / 57.296);
      tauFactor = exp(-tau0 * 1.0 / sinel);
#endif

/* SMT Elevation Gain Curve Derived by Issaoun & Folkers 2016 */
  if(env->elgain) /* Does user want to apply elevation gain curve? */
  {
    gainEl = env->elgain_term5 * pow(h->el, 4.0) + 
             env->elgain_term4 * pow(h->el, 3.0) + 
             env->elgain_term3 * pow(h->el, 2.0) + 
             env->elgain_term2 * h->el           + 
             env->elgain_term1;

    if(gainEl < 0.5)
    {
      gainEl = 1.0;
    }
  }
  else
  {
    gainEl = 1.0;
  }

  if(env->elgain > 0) /* El gain produces gain factors < 1.0; so divide instead */
  {
    gainEl = 1.0 / gainEl;
  }

  taStar *= gainEl;

  Tr = taStar * factor;

  ttime = npts * h->samprat;

  st_print(" %10.10s %6.2f%10.4f%6.2f%8.2f%3d%4.0f%5.1f%7.1f", 
		h->object, restFreq(h) / 1000.0, h->utdate, h->ut, 
                h->scan, (int)(npts / h->nophase), 
		ttime, h->el, source);


  

  st_print("%7.1f%10.7f%5.0f%9.4f%9.4f%5.2f%7.1f", 
		ref, ratio, tsys, taStar, Tr, factor, Tr*env->jfact);


  ar->results[ar->num].scan    = h->scan;
  ar->results[ar->num].ut      = h->ut;
  ar->results[ar->num].el      = h->el;
  ar->results[ar->num].sig     = source;
  ar->results[ar->num].sky     = ref;
  ar->results[ar->num].smror   = ratio;
  ar->results[ar->num].tsys    = tsys;
  ar->results[ar->num].tastar  = taStar;
  ar->results[ar->num].tr      = Tr;
  ar->results[ar->num].factor  = factor;
  ar->results[ar->num].janskys = Tr*env->jfact;

  accumArrayTmb[accumArrayCnt]  = (float)taStar;
  accumArrayTr[accumArrayCnt]   = (float)Tr;
  accumArrayFlux[accumArrayCnt] = (float)(Tr*env->jfact);

  accumArrayCnt++;

  if(accumArrayCnt >= MAX_ACCUM_SCANS)
  {
    st_fail("Accum Count > Max: %d", MAX_ACCUM_SCANS);
    accumArrayCnt = MAX_ACCUM_SCANS;
  }

  if(autoCalibrate > 0.0)
  {
    autoAccum[feed-1] += taStar;
    autoAccumCnt[feed-1]++;
  }

  st_print("  %5.3f  %5.3f  %5.3f\n", env->mantau0, tauFactor, gainEl);

  if(env->accum)
  {
    env->accumtastar += (taStar * weight);
    env->accumtr     += (Tr     * weight);

    env->totalweight += weight;

    env->accumcount++;
  }
  else
  {
    env->accumtastar = taStar;
    env->accumtr     = Tr;
  }

  ar->num++;

  if(ar->num == MAX_ACCUM_SCANS)
  {
    st_fail("Too many scans in accumulator: %d", MAX_ACCUM_SCANS);
    ar->num--;
  }

  return(0);
}



int sendAccumResults()
{
  if(xgraphic)
  {
    strcpy(accum_results_send.magic, "ACCUM_INFO");

    accum_results_send.plot_select1 = env->acc_plot_select_x;
    accum_results_send.plot_select2 = env->acc_plot_select_y;

    accum_results_send.issmt        = env->issmt;
    accum_results_send.plotwin      = env->plotwin;
    accum_results_send.paperw       = env->paperw;
    accum_results_send.paperh       = env->paperh;

    sock_write(xgraphic, (char *)&accum_results_send, sizeof(accum_results_send));

    last_plot = LAST_PLOT_CONT_ACCUM;
  }

  return(0);
}


int printBurnTime()
{
  int    i, j=0, samps, secs, foundmatch = 0;
  struct HEADER *p;
  struct tm *tn;
  time_t iburn;
  double timeburn, diff;
  char durstr[256];

  p = &sa[0];
  for(i=0;i<env->numofscans;i++,p++)
  {                                                   /* If we want to auto reduce tips */
    if(!meetsCritiria(p, CHECK_ALL))
    {
      continue;
    }

    if(args->ntokens > 1 && args->secondArg)
    {
      if(!strcasecmp(args->secondArg, "foc"))
      {
        foundmatch = 1;
        if(strncmp(p->obsmode, "CONTFOC", 7))
        {
          continue;
        }
      }

      if(!strcasecmp(args->secondArg, "ewf"))
      {
        foundmatch = 1;
        if(strncmp(p->obsmode, "CONTEWFC", 8))
        {
          continue;
        }
      }

      if(!strcasecmp(args->secondArg, "nsf"))
      {
        foundmatch = 1;
        if(strncmp(p->obsmode, "CONTNSFC", 8))
        {
          continue;
        }
      }

      if(!strcasecmp(args->secondArg, "seq"))
      {
        foundmatch = 1;
        if(strncmp(p->obsmode, "CONTSEQ", 7))
        {
          continue;
        }
      }

      if(!strcasecmp(args->secondArg, "cal"))
      {
        foundmatch = 1;
        if(strncmp(p->obsmode, "CONTCAL", 7))
        {
          continue;
        }
      }

      if(!strcasecmp(args->secondArg, "map"))
      {
        foundmatch = 1;
        if(strncmp(p->obsmode, "CONTMAP", 7))
        {
          continue;
        }
      }

      if(!strcasecmp(args->secondArg, "qk5"))
      {
        foundmatch = 1;
        if(strncmp(p->obsmode, "CONTFIVE", 8) && strncmp(p->obsmode, "CONTQK5", 7))
        {
          continue;
        }
      }

      if(!foundmatch)
      {
        st_fail("Unknown arg: %s", args->secondArg);
	printH_FlagHelp(args->firstArg);
        return(1);
      }
    }

    iburn = (time_t)p->burn_time;

    tn = localtime(&iburn);

    timeburn = (double)tn->tm_hour
             + (double)tn->tm_min  / 60.0
             + (double)tn->tm_sec  / 3600.0;

    if(timeburn > p->ut) /* Check for roll over */
    {
      diff = (timeburn - p->ut) - 17.0;
    }
    else
    {
      diff = (timeburn - p->ut) + 7.0;
    }

    sexagesimal(diff, durstr, MMMM_SS);

    if(!(j%24))
    {
      st_print("\n");
      st_print("  Source          Mode      Scan       UT     Burn_Time    Samps  Secs  Duration\n");
      st_print("\n");
    }

    secs  = (int)p->samprat;
    samps = (int)(p->noint / p->nophase);

    st_print("%16.16s %8.8s %8.2f   %5.2f   %.0f   %4d   %4d  %s\n", p->object, p->obsmode, p->scan, p->ut, p->burn_time, samps, secs, durstr);

    j++;

  }

  st_print("Found %d matching scans", j);

  return(0);
}


/* Calibration Equations based on:
  "Quasiotical Systems; Gaussian Beam Quasioptical Propagation and Applications"
   by Paul F. Goldsmith; 
   IEEE Press; 1998 
*/
int accumSelected()
{
  int i=0, old, indx, feed, once, old_once = 0, first = 1, iret;
  struct HEADER *p;
  double aTr, aTaStar, ret, newFactor;
  double lambda, v, surface;
  double A, a, Te, Fb, Et, Es, Ea, Er, Eb, Es2, num;
  struct ACCUM_RESULTS *ar1;
  double contWorkingTime;
  double contWorkingTsys;
  float  rmsTmb, rmsTr, rmsFlux;

  env->dobaseline = 0;

  if(env->mode == MODE_SPEC) /* If we accidently get here in SPEC mode */
  {
    line_accumSelected();
    return(0);
  }

  if(env->accumreset)
  {
    st_report("Reseting Accumulators");

    resetRejections();

    env->accumcount  = 0;
    env->accumtastar = 0.0;
    env->accumtr     = 0.0;
    env->totalweight = 0.0;
    env->avgEl       = 0.0;

    if(env->accumreset != 2) /* Dual Beam accumilation; don't zero */
    {
      bzero((char *)&accum_results, sizeof(accum_results));
      bzero((char *)&accum_results_send, sizeof(accum_results_send));
    }

    contWorkingTsys = 0.0;
    contWorkingTime = 0.0;

    accumArrayCnt = 0;

    /* Reset to something useful */
    if(env->acc_plot_select_y == ACCUM_PLOT_Y_NORMAL)
    {
      env->acc_plot_select_y = ACCUM_PLOT_Y_JANSKY;
    }
  }

  if(!env->numofscans)
  {
    st_warn("No Valid Data Files Loaded");

    env->accumreset = 1;
    return(1);
  }

  if(env->accumreset)
  {
    if(env->freq > 0.0)
    {
      st_report("Looking for scans using Freq = %.3f +/- %3.3f MHz", env->freq, env->dfreq*1000.0);
    }
    else
    {
      st_report("Looking for scans using ALL Freqs");
    }

    if(env->check_date)
    {
      st_report("Looking for scans taken on %.4f", env->date);
    }
    else
    {
      st_report("Looking for scans taken on ALL dates");
    }

    if(env->check_source)
    {
      st_report("Looking for scans on %s", env->source);
    }
    else
    {
      st_report("Looking for scans on ALL Sources");
    }

    if(env->check_scans)
    {
      st_report("Looking for scans starting at: %.2f up to %.2f", env->bscan, env->escan);
    }
    else
    {
      st_report("Looking for Entire Range of scan numbers");
    }

    if(env->check_feed)
    {
      st_report("Looking for scans ending in %.2f", env->if_feed);
    }
    else
    {
      st_report("Looking for ALL IF's");
    }

    if(env->invert)
    {
      st_report("Inverting data");
    }
  }

//  if(env->freq > 0.0 && env->check_source)
//  {
    env->accum = 1;

    if(env->accumreset == 1)
    {
      st_report("Accumulating Scans for %s @ %.1f GHz +/- 500 MHz", env->source, env->freq);
    }
//  }

  if(env->issmt)
  {
    A = (M_PI * pow(MG_DIAMETER, 2.0) / 4.0);
  }
  else
  {
    A = (M_PI * pow(KP_DIAMETER, 2.0) / 4.0);
  }

  st_report("Telescope Area        = %.3f", A);

  env->jfact = ((2.0 * boltzmann / 1.0e-26) / A);

  if(env->kj_factor > 1.0)
  {
    env->jfact = env->kj_factor;
    setColor(ANSI_YELLOW);
    st_report("User Supplied Jk Fact = %.2f Jk / Kelvin", env->jfact);
    setColor(ANSI_BLACK);
  }
  else
  {
    st_report("Using Jansky Factor  = %.2f Jk / Kelvin", env->jfact);
  }

  switch(env->calmode)
  {
    case CALMODE_TELE_EFF:
      st_report("Using Atmospheric Conditions & Telescope Eff for Calibration");

      if(env->autotau)
      {
        st_report("Using Automatically Computed Tau0");
      }
      else
      {
        st_report("Using Fixed Tau0:    = %.3f", env->mantau0);
      }

      if(env->issmt)
      {
        surface = MG_SURFACE_RMS;
        Fb      = MG_SUB_DIA / MG_DIAMETER;
        Te      = MG_TAPER;
      }
      else
      {
        surface = KP_SURFACE_RMS;
        Fb      = KP_SUB_DIA / KP_DIAMETER;
        Te      = KP_TAPER;
      }

      st_report("Using Te (Taper)     = %3.0fdB", Te);

      a = Te * 0.115;                                          /* Eq. 6.2a  Goldsmith */
      lambda = 1000.0 * SOL / env->freq * 1000.0; /* Convert to microns */

      st_report("Using Alpha          = %5.2f", a);
      st_report("Using Blockage       = %5.2f%%", Fb * 100.0);
      st_report("Using Lambda         = %5.2f mm", lambda / 1000.0);
      st_report("Using Surface        = %5.2f microns", surface);

      Es = 1.0 - exp(-(2.0*a));                                /* Eq. 6.12  Goldsmith */
      st_report("Using Es (Spillover) = %.2f%%", Es*100.0);

      num = exp(-(pow(Fb, 2.0) * a)) - exp(-a);                /* Eq. 6.10  Goldsmith */
      Et = (2.0 / a) * ((num*num) / Es);
      st_report("Using Et (Taper)     = %.2f%%", Et*100.0);

/* Compute Ruze Value */
      v = (4.0 * M_PI * surface) / lambda;
      v = v*v; /* square it */
      Er = exp(-(v));
      st_report("Using Er (Ruze)      = %.2f%%", Er * 100.0);

      Es2 = 1.0 - exp(-(a));                                   /* Eq. 6.16  Goldsmith */
      Eb = pow(num, 2.0) / pow(Es2, 2.0);

      /* Throw in a few percent for feedlegs, panel gaps & holes, optical telescope and shading */
      Eb *= 0.984;
      st_report("Using Eb (Blockage)  = %.2f%%", Eb * 100.0);

      Ea = Et * Es * Er * Eb;
      st_report("Using Ea (App Eff)   = %.2f%%", Ea * 100.0);

      env->apperatureeff = Ea;

      break;

    case CALMODE_BEAMEFF:
      st_report("Using Beam Efficencies of: %.2f %.2f %.2f %.2f for Calibration",
			env->bm_eff[0], env->bm_eff[1], env->bm_eff[2], env->bm_eff[3]);
      break;

    case CALMODE_CALSCALE:
      st_report("Using Calibration Scale of: %.3f for Calibration", env->cal_scale);
      break;

    case CALMODE_NONE:
      st_report("NOT Appling any Calibration");
  }

  if(env->elgain)
  {
    setColor(ANSI_RED);
    st_report("Using Elevation Gain");
    setColor(ANSI_BLACK);
  }

  old = (int)sa[0].scan;

  p = &sa[0];
  indx = 0;

  for(i=0;i<env->numofscans;i++,p++)
  {                                                   /* If we want to auto reduce tips */
    if(env->autotau && !strncmp(p->obsmode, "CONTSTIP", 8) && meetsCritiria(p, CHECK_FREQ | CHECK_DATE))
    {
      once = (int)p->scan;
      if(once != old_once)
      {
        subArgs(2, "%d", once);
        doSpTip();

        old_once = once;
      }

      incrementRejMode();

      continue;
    }

    if(!meetsCritiria(p, CHECK_ALL))
    {
      continue;
    }

    if(env->doing_fives)
    {
      if(strncmp(p->obsmode, "CONTQK5", 7)) /* We only process 5 Points */
      {
        incrementRejMode();
        continue; /* No good */
      }
    }
    else
    {
      if(strncmp(p->obsmode, "CONTSEQ", 7)) /* We only process Seq */
      {
        incrementRejMode();
        continue; /* No good */
      }
    }
    

    if(isScanIgnored(p->scan))
    {
      incrementRejIgnored();
      continue;
    }

    ret = read_scan(p->scan, env->filename, h0->yplot1, &h0->head, 0);
    env->baselineremoved = 0;
    env->curScan = p->scan;

    if(ret > 0.0)
    {
      if(!env->check_feed && (int)p->scan != old)
      {
        st_print("\n");
        old = (int)p->scan;
      }

      if(!(indx%32))
      {
        if(env->check_feed)
        {
          st_print("\n");
        }

        st_print(" Source     Freq    UT-Date   UTC   Scan#  NS Time  El   Sig");
        st_print("    Sky    S-R/R * Tsys  = Ta*       Tr   *Fac   Jy's ManTau0 TauFac ElGain");

        if(env->calmode == CALMODE_TELE_EFF)
        {
          st_print("   Tau   TauEL  GainEl\n\n");
        }
        else
        {
          st_print("\n\n");
        }
      }

      feed = getFeed(h0->head.scan) -1;

      if(!(feed >= 0 && feed < 4))
      {
        feed = 0;
      }



      ar1 = &accum_results[feed];

	/* Go do the actual Accumilation */
      iret = accum_v1(h0->yplot1, &h0->head, env->invert, ar1);

      if(!iret) /* It was a good accum */
      {
        if(first)
	{
	  accum_results_send.bscan = h0->head.scan;
	  first = 0;
	}

	accum_results_send.escan = h0->head.scan;
        contWorkingTime += h0->head.inttime;
        contWorkingTsys += h0->head.stsys;

	env->avgEl += h0->head.el;

        bcopy((char *)&h0->head, (char *)&accum_results_send.head[feed], sizeof(struct HEADER));

        accum_results_send.valid |= 1<<feed; /* Flag which feeds are valid */

        indx++;
      }
    }
  }

  rmsTmb = rmsTr = rmsFlux = 0.0;
  if(accumArrayCnt)
  {
    computeRms(&accumArrayTmb[0],  accumArrayCnt, &rmsTmb);
    computeRms(&accumArrayTr[0],   accumArrayCnt, &rmsTr);
    computeRms(&accumArrayFlux[0], accumArrayCnt, &rmsFlux);

  }

  if(indx == 0)
  {
    st_print("No Scans Found Matching Search Criteria\n");

    env->last_scan_count = 0;

    printRejections();

    env->accumreset = 1;

    return(1);
  }

/* Show results */
  if(env->accum && env->accumcount)
  {
    aTaStar = env->accumtastar / env->totalweight;
    aTr     = env->accumtr     / env->totalweight;

    st_print("\n%51.51s  Weighted Average of %3d Scans: %8.4f%9.4f     %7.1f\n", " ",
                        env->accumcount,
                        aTaStar,
                        aTr,
                        aTr*env->jfact);

    st_print("%76.76s   RMS:%9.4f%9.4f     %7.1f\n", " ", rmsTmb, rmsTr, rmsFlux);

    env->last_scan_count = env->accumcount;

    if(autoCalibrate > 0.0)
    {
      for(i=0;i<4;i++)
      {
        if(autoAccumCnt[i])
        {
          newFactor = ((autoAccum[i] / autoAccumCnt[i]) * env->jfact) / autoCalibrate;
          tmpBeamEff[i] = newFactor * 100.0;
        }
        else
        {
          tmpBeamEff[i] = 1.0;
        }
      }

      if(getYesNoQuit("\n  Use beam eff value of %.3f %.3f. %.3f %.3f: y/N ?",
                        tmpBeamEff[0], tmpBeamEff[1],
                        tmpBeamEff[2], tmpBeamEff[3]))

      {
        doAcceptEff();

        autoCalibrate = 0.0;

        accumSelected();
      }
      else
      {
        autoCalibrate = 0.0;
      }
    }
  }

  env->accumreset = 1;

  if(indx)
  {
    st_report("Sending Results to xgraphic");

    bcopy((char *)&accum_results, (char *)&accum_results_send.ar, sizeof(accum_results));

    accum_results_send.plotExpected = env->plottmb;

    accum_results_send.avgTaStar[feed]       = aTaStar;
    accum_results_send.taSigma[feed]         = rmsTmb;
    accum_results_send.expectedTaStar[feed]  = env->expectedtmb;

    accum_results_send.avgTr[feed]           = aTr;
    accum_results_send.trSigma[feed]         = rmsTr;
    accum_results_send.expectedTr[feed]      = env->expectedtmb;

    accum_results_send.avgJanskys[feed]      = aTr*env->jfact;
    accum_results_send.jkSigma[feed]         = rmsFlux;
    accum_results_send.expectedJanskys[feed] = env->expectedflux;

    accum_results_send.scanct                = indx;
    accum_results_send.head[feed].inttime    = contWorkingTime;
    accum_results_send.head[feed].stsys      = contWorkingTsys / (double)indx;

    sendAccumResults();

    env->avgEl /= (double)indx;

    saveObjectAccum(&accum_results_send);
  }

  validate(ACCUM_SCANS_MODE);

  return(0);
}


int do_objectNormalize()
{
  int i, j, n, foundone = 0;
  float max, *f;
  struct OBJECT *o;

  o = &OGM->objects[OGM->nobjIndx];

  if(!o->bplot)
  {
    st_fail("do_objectNormalize(): Must perform a 'gracefit' first");
    return(1);
  }

  if(isSet(PF_C_FLAG))
  {
    for(j=0;j<MAX_IFS;j++)
    {
      o->normalize[j] = 0.0;
    }

    sock_write(xgraphic, (char *)o, sizeof(struct OBJECT));

    return(0);
  }

  if(!o->bplot)
  {
    st_fail("do_objectNormalize(): Must perform a 'gracefit' first");
    return(1);
  }

  n = o->numMeas;
  for(j=0;j<MAX_IFS;j++)
  {
    if(o->measurements[0].results[j].measuredTmb)
    {
      foundone++;
      max = -999999.9;

      f = &o->fitcurves[j][0];	/* Find max of fitted curve */

      for(i=0;i<n;i++,f++)
      {
        if(*f > max)
        {
          max = *f;
        }
      }


      st_report("do_objectNormalize(): Max value for (%s): %f", mg_sideband[j], max);

      if(max)
      {
        o->normalize[j] = 1.0 / max;
      }
    }
  }

  if(foundone)
  {
    o->bplot = 1;
    sock_write(xgraphic, (char *)o, sizeof(struct OBJECT));
  }


  return(0);
}



int do_contNormalize()
{
  int i, j, n;
  float max = -99999.0, *f;
  struct ACCUM_SCAN_RESULT *p;

  if(isSet(PF_O_FLAG))		/* Perform this on the OBJECT array ?? */
  {
    do_objectNormalize();

    return(0);
  }

  if(!accum_results_send.bplot)
  {
    st_fail("do_contNormalize(): Must perform a 'gracefit' first");
    return(1);
  }

  for(j=0;j<MAX_IFS;j++)
  {
    n = accum_results_send.ar[j].num;

    if(n)
    {
      f = &accum_results_send.fitcurve[j][0];

      for(i=0;i<n;i++,f++)
      {
        if(*f > max)
        {
          max = *f;
        }
      }

      st_report("do_contNormalize(): Max value for (%s): %f", mg_sideband[j], max);

      p = &accum_results_send.ar[j].results[0];
      for(i=0;i<n;i++,p++)
      {
        switch(env->acc_plot_select_y)
        {
          case ACCUM_PLOT_Y_SIG:    p->normalized = p->sig     / max; break;
          case ACCUM_PLOT_Y_SKY:    p->normalized = p->sky     / max; break;
          case ACCUM_PLOT_Y_SMROR:  p->normalized = p->smror   / max; break;
          case ACCUM_PLOT_Y_TSYS:   p->normalized = p->tsys    / max; break;
          case ACCUM_PLOT_Y_TASTAR: p->normalized = p->tastar  / max; break;
          case ACCUM_PLOT_Y_TR:     p->normalized = p->tr      / max; break;
          case ACCUM_PLOT_Y_JANSKY: p->normalized = p->janskys / max; break;
        }
      }
    }
  }

  env->acc_plot_select_y = ACCUM_PLOT_Y_NORMAL;

  doGraceFit();		/* Recompute curve */

  return(0);
}





int cont_determine_min_max(ydata, dl, scan)
float *ydata;
int dl;
float scan;
{
  int i;

  if(ydata == NULL)
  {
    return(1);
  }

  env->peakChan = 0;
                                                   /* redetermine min and max */
  env->datamax = -9999999999.9;
  env->datamin = 9999999999.0;

  for(i=0;i<dl;i++)
  {
    if(ydata[i] > env->datamax)
    {
      env->datamax = ydata[i];
      env->peakChan = i;
    }

    if(ydata[i] < env->datamin)
    {
      env->datamin = ydata[i];
    }
  }

  if(fabs(env->datamax - env->datamin) < DATASPREAD)
  {
    st_print("Scan #%7.2f is all %.4f\n", scan, env->datamin);

    env->datamin -= 1.0;
    env->datamax += 1.0;

    return(1);
  }

  return(0);
}



int kp_bin_and_cal_data(ydata, dl, avg, data, h)
float *ydata;
int dl, avg;
float *data;
struct HEADER *h;
{
  int i, j;
  float ascale, cal;
  double rfil = 0.0;

  /* Shouldn't get here, but: Switched to 2 phase @KP in Summer of 2017 */
  if(h->nophase == 2 || h->burn_time > 1501704800.0)
  {
    smt_bin_and_cal_data( ydata, dl, avg, data, h);

    return(0);
  }

  if(notSet(PF_O_FLAG)) /* Use Old Way Below */
  {
    if(!strncmp(h->obsmode, "CONTSEQ", 7))
    {
      return(seq_bin_and_cal_data(ydata, dl, avg, data, h, 4));
    }
    else
    if(!strncmp(h->obsmode, "CONTQK5", 7))
    {
      return(seq_bin_and_cal_data(ydata, dl, avg, data, h, 4));
    }
    else
    if(!strncmp(h->obsmode, "CONTFOC", 7))
    {
      return(foc_bin_and_cal_data(ydata, dl, avg, data, h, 4));
    }
    else
    if(!strncmp(h->obsmode, "CONTNSFC", 8))
    {
      return(foc_bin_and_cal_data(ydata, dl, avg, data, h, 4));
    }
    else
    if(!strncmp(h->obsmode, "CONTEWFC", 8))
    {
      return(foc_bin_and_cal_data(ydata, dl, avg, data, h, 4));
    }
    else
    if(!strncmp(h->obsmode, "CONTMAP", 7))
    {
      return(map_bin_and_cal_data(ydata, dl, avg, data, h, 4));
    }
  }

  st_report("kp_bin_cal(): Using 4 Phase Switch Pattern");

  ascale = 1.0;

  if(avg == SWITCHED)
  {
    for(i=0,j=0;i<dl;i++)
    {
     if(h->nophase == 2)
     {
        ydata[i] = (data[j] - data[j+1]) *ascale *0.5;
        rfil    += (data[j] + data[j+1])         *0.5;
      }
      else
      {
        ydata[i] = (data[j] - data[j+1] + data[j+2] - data[j+3]) *ascale *0.25;
        rfil    += (data[j] + data[j+1] + data[j+2] + data[j+3])         *0.25;
      }

      j += h->nophase;
    }

    rfil /= dl;

    if(rfil > 0.0)
    {
      cal = h->tcal/rfil;
    }
    else
    {
      cal = h->tcal/1.0;
    }

    if(isSet(PF_V_FLAG))
    {
      st_report("BIN_CAL: cal = %f, rfil = %f", cal, rfil);
    }

    for(i=0; i < dl; i++)
    {
      ydata[i] *= cal;

      if(isSet(PF_V_FLAG))
      {
	st_report("Ydata[%2d] = %6.1f", i, ydata[i]);
      }
    }
  }
  else
  {
    for(i=0,j=0;i<dl;i++)
    {
      if(h->nophase == 2)
      {
        ydata[i] = (data[j] + data[j+1]) / 2.0;
        j += 2;
      }
      else
      {
        ydata[i] = (data[j] + data[j+1] + data[j+2] + data[j+3]) /
                                                (float)h->nophase;
        j += h->nophase;
      }
    }
  }

  return(0);
}



/* All SMT data & 12M data after Summer 2017 */
/* NOTE: Phases always come in as: Sig, Ref, Sig, Ref, Sig, Ref.... */
/*       No matter the dicke switching pattern */
/*   Usec for 2 or 4 phase Seq and Five-points */
int seq_bin_and_cal_data(ydata, dl, avg, data, h, phase)
float *ydata;
int dl, avg, phase;
float *data;
struct HEADER *h;
{
  int i, j, k, *p;
  double sig, ref, ratio, avg2;

  st_report("seq_bin_and_cal_data(%d): Using two Phase Switch Pattern", phase);

  if(avg == SWITCHED)
  {
    j = 0;
    k = 0;

    if(isSet(PF_V_FLAG))
    {
      avg2 = 0.0;
    }

    p = &two_phase_seq_five[0];
    for(i=0;i<dl;i++)
    {
      if(p[k] < 0)
      {
        if(phase == 2)
        {
          sig = data[j+1];
          ref = data[j];
	}
	else
	{
          sig = data[j+1] + data[j+3];
          ref = data[j]   + data[j+2];
	}
      }
      else
      {
        if(phase == 2)
        {
          sig = data[j];
          ref = data[j+1];
	}
	else
	{
          sig = data[j]   + data[j+2];
          ref = data[j+1] + data[j+3];
	}
      }

	/*  tempK =  (Sig - Ref)/ Ref * Tsys */

      ratio = (sig - ref) / ref;

      ydata[i] = h->stsys * ratio * (double)p[k];

      if(isSet(PF_V_FLAG))
      {
        st_report("Ratio: %10.6f: K = %5.1f", ratio, ydata[i] );
        avg2 += fabs(ydata[i]);
      }

      
      j += phase;

      k++;

      if(k >= (NPHASE_2 / 2))
      {
        p = &two_phase_seq_five[0];

        k = 0;
      }
    }

    if(isSet(PF_V_FLAG))
    {
      avg2 /= (double)dl;
      st_report("Average = %f K", avg2);
    }
  }
  else
  {
    for(i=0,j=0;i<dl;i++)
    {
      ydata[i] = (data[j] + data[j+1]) / 2.0;
      j += 2;
    }
  }

  return(0);
}


/* All SMT data & 12M data after Summer 2017 */
/* NOTE: Phases always come in as: Sig, Ref, Sig, Ref, Sig, Ref.... */
/*       No matter the dicke switching pattern */
/*   Usec for 2 or 4 phase any_Focus */
int foc_bin_and_cal_data(ydata, dl, avg, data, h, phase)
float *ydata;
int dl, avg, phase;
float *data;
struct HEADER *h;
{
  int i, j, k, *p;
  double sig, ref, ratio;

  st_report("foc_bin_and_cal_data(%d): Using two Phase Switch Pattern", phase);

  if(avg == SWITCHED)
  {
    j = 0;
    k = 0;

    p = &two_phase_seq_foc[0];
    for(i=0;i<dl;i++)
    {
      if(p[k] < 0)
      {
        if(phase == 2)
        {
          sig = data[j+1];
          ref = data[j];
	}
	else
	{
          sig = data[j+1] + data[j+3];
          ref = data[j]   + data[j+2];
	}
      }
      else
      {
        if(phase == 2)
        {
          sig = data[j];
          ref = data[j+1];
	}
	else
	{
          sig = data[j]   + data[j+2];
          ref = data[j+1] + data[j+3];
	}
      }

	/*  tempK =  (Sig - Ref)/ Ref * Tsys */

      ratio = (sig - ref) / ref;

      ydata[i] = h->stsys * ratio * (double)p[k];

      if(isSet(PF_V_FLAG))
      {
        st_report("Ratio: %10.6f: K = %5.1f", ratio, ydata[i] );
      }

      
      j += phase;

      k++;

      if(k >= (NPHASE_2 / 2))
      {
        p = &two_phase_seq_foc[0];

        k = 0;
      }
    }
  }
  else
  {
    for(i=0,j=0;i<dl;i++)
    {
      ydata[i] = (data[j] + data[j+1]) / 2.0;
      j += 2;
    }
  }

  return(0);
}


/*   All SMT data & 12M data after Summer 2017
 *   NOTE: Phases always come in as: Sig, Ref, Sig, Ref, Sig, Ref....
 *   No matter the dicke switching pattern 
 *   Used for 2 or 4 phase Cont Map
 *   Because the sense of Sig / Ref changes half way through the map
 *   we need to account for that to properly calibrate the ydata.
 */
int map_bin_and_cal_data(ydata, dl, avg, data, h, phase)
float *ydata;
int dl, avg, phase;
float *data;
struct HEADER *h;
{
  int i, j, half;
  double sig, ref, ratio;

  st_report("map_bin_and_cal_data(%d): Using %d Phase Switch Pattern", phase, phase);

  if(avg == SWITCHED)
  {
    j = 0;
    half = dl / 2;
    for(i=0;i<dl;i++)
    {
      if(phase == 2)
      {
        sig = data[j];
        ref = data[j+1];
      }
      else
      {
        sig = data[j]   + data[j+2];
        ref = data[j+1] + data[j+3];
      }

   /* tempK = (Sig - Ref)/ Ref * Tsys */

      if(i < half)
      {
        ratio = (ref - sig) / sig * -1.0;
      }
      else
      {
        ratio = (sig - ref) / ref;
      }

      ydata[i] = h->stsys * ratio;

      if(isSet(PF_V_FLAG))
      {
        st_report("Ratio[%2d]: %10.6f: K = %6.2f", i, ratio, ydata[i] );
      }
      
      j += phase;
    }
  }
  else
  {
    for(i=0,j=0;i<dl;i++)
    {
      ydata[i] = (data[j] + data[j+1]) / 2.0;
      j += 2;
    }
  }

  return(0);
}


/* Most 2 phase continuum switching */
int smt_bin_and_cal_data(ydata, dl, avg, data, h)
float *ydata;
int dl, avg;
float *data;
struct HEADER *h;
{
  int    i, j;
  float  ascale = 1.0, cal;
  double rfil   = 0.0;

  if(notSet(PF_O_FLAG)) /* Use Old Way Below */
  {
    if(!strncmp(h->obsmode, "CONTSEQ", 7))
    {
      return(seq_bin_and_cal_data(ydata, dl, avg, data, h, 2));
    }
    else
    if(!strncmp(h->obsmode, "CONTQK5", 7))
    {
      return(seq_bin_and_cal_data(ydata, dl, avg, data, h, 2));
    }
    else
    if(!strncmp(h->obsmode, "CONTFOC", 7))
    {
      return(foc_bin_and_cal_data(ydata, dl, avg, data, h, 2));
    }
    else
    if(!strncmp(h->obsmode, "CONTNSFC", 8))
    {
      return(foc_bin_and_cal_data(ydata, dl, avg, data, h, 2));
    }
    else
    if(!strncmp(h->obsmode, "CONTEWFC", 8))
    {
      return(foc_bin_and_cal_data(ydata, dl, avg, data, h, 2));
    }
    else
    if(!strncmp(h->obsmode, "CONTMAP", 7))
    {
      return(map_bin_and_cal_data(ydata, dl, avg, data, h, 2));
    }
  }

  st_report("smt_bin_cal(): Using 2 Phase Switch Pattern");

  if(avg == SWITCHED)
  {
    j = 0;
    for(i=0;i<dl;i++)
    {
      ydata[i] = (data[j] - data[j+1]) *ascale * 0.5;
      rfil    += (data[j] + data[j+1])         * 0.25;

      j += 2;
    }

    rfil /= dl;

    if(isSet(PF_V_FLAG))
    {
      st_report("rfil: %f, dl = %d", rfil, dl);
    }

    if(rfil > 0.0)
    {
      cal = h->stsys/rfil;
    }
    else
    {
      cal = h->stsys;
    }

    for(i=0;i<dl;i++)
    {
      ydata[i] *= cal;

      if(isSet(PF_V_FLAG))
      {
        st_report("Ydata[%2d] = %6.1f", i, ydata[i]);
      }
    }
  }
  else
  {
    j = 0;
    for(i=0;i<dl;i++)
    {
      ydata[i] = (data[j] + data[j+1]) / 2.0;
      j += 2;
    }
  }

  return(0);
}


int clearFillContStruct(h, dlen, xl, yl)
struct HEADER *h;
int dlen;
char *xl, *yl;
{
//  st_report("Clearing Cont Send Struct");
  bzero((char *)&cont_plain_send, sizeof(cont_plain_send));

  bcopy((char *)h, (char *)&cont_plain_send.head, sizeof(struct HEADER));

  cont_plain_send.issmt = env->issmt;
  cont_plain_send.ons   = env->ons;
  cont_plain_send.dlen  = dlen;

  strcpy(cont_plain_send.xlabel, xl);
  strcpy(cont_plain_send.ylabel, yl);

  return(0);
}


int cont_show_phases(data, h, plot)
float *data;
struct HEADER *h;
int plot;
{
  float  ydata[MAX_CONT_SAMPLES], *yp;
  int    i, dlen, oldzline;

  dlen = (int)h->noint;
  
  zero_check("Phases", dlen, data);

  if(dlen > MAX_CONT_SAMPLES)
  {
    st_fail("Too many Continuum Samples: %d", dlen);
    return(1);
  }

  yp = &ydata[0];

  oldzline = env->zline; /* Save for later */

  if(isSet(PF_S_FLAG)) /* switched ? */
  {
    if(env->issmt)
    {
      smt_bin_and_cal_data( yp, dlen, SWITCHED, data, h);
    }
    else
    {
      kp_bin_and_cal_data( yp, dlen, SWITCHED, data, h);
    }

    dlen /= h->nophase;
  }
  else
  {
    env->zline = 0;

    for(i=0;i<dlen;i++)
    {
      ydata[i] = data[i];
    }
  }

  if(cont_determine_min_max(ydata, dlen, h->scan))
  {
    env->zline = oldzline;

    return(0);
  }

  if(isSet(PF_S_FLAG))
  {
    clearFillContStruct(h, dlen, "Sample Number", "Intensity");
  }
  else
  {
    clearFillContStruct(h, dlen, "Sample Number", "Counts");
  }

  for(i=0;i<dlen;i++)
  {
    cont_plain_send.xdata[i] = (float)(i) + 1.0;
    cont_plain_send.ydata[i] = ydata[i];
  }

  if(xgraphic)
  {
    sendCont(SEND_PLAIN);
  }

  env->zline = oldzline;

  validate(SEQUENCE);

  return(0);
}


int sendCont(mode)
int mode;
{
  if(xgraphic)
  {
    if(mode == SEND_PLAIN)
    {
      strcpy(cont_plain_send.magic, "CONTINUUM_PLAIN");
      cont_plain_send.plotwin   = env->plotwin;
      cont_plain_send.plotStyle = env->plotstyle;
      cont_plain_send.gridMode  = env->gridmode;
      cont_plain_send.autoscale = env->yscale;
      cont_plain_send.yplotmax  = env->yupper;
      cont_plain_send.yplotmin  = env->ylower;
      cont_plain_send.xstart    = env->xstart;
      cont_plain_send.ystart    = env->ystart;
      cont_plain_send.zline     = env->zline;

      sock_write(xgraphic, (char *)&cont_plain_send, sizeof(cont_plain_send));

      last_plot = LAST_PLOT_CONT_PLAIN;
    }
    else
    if(mode == SEND_SPTIP)
    {
      strcpy(cont_sptip_send.magic, "CONTINUUM_SPTIP");
      cont_sptip_send.plotwin   = env->plotwin;
      cont_plain_send.plotStyle = env->plotstyle;
      cont_plain_send.gridMode  = env->gridmode;
      cont_plain_send.autoscale = env->yscale;
      cont_plain_send.yplotmax  = env->yupper;
      cont_plain_send.yplotmin  = env->ylower;

      sock_write(xgraphic, (char *)&cont_sptip_send, sizeof(cont_sptip_send));

      last_plot = LAST_PLOT_CONT_SPTIP;
    }
  }

  return(0);
}


int cont_display_sequence(image, plot)
struct LP_IMAGE_INFO *image;
int plot;
{
  float  ydata[MAX_CONT_SAMPLES], *yp;
  int    i, dlen;

  if(processContinuum(&image->head, &image->yplot1[0], &ydata[0], &dlen, "Sequence"))
  {
    return(1);
  }

  clearFillContStruct(&image->head, dlen, "Sample Number", "{/Symbol T}_{/Symbol A}* (K)");

  yp = &ydata[0];
  for(i=0;i<dlen;i++,yp++)
  {
    cont_plain_send.xdata[i] = (float)(i) + 1.0;
    cont_plain_send.ydata[i] = *yp;
  }

  if(xgraphic)
  {
    sendCont(SEND_PLAIN);
  }

  validate(SEQUENCE);

  return(0);
}



double findDeltaX(h)
struct HEADER *h;
{
  double deltax;

  if(h->scanang == 0.0)
  {
    deltax = h->deltaxr;
  }
  else
  if(h->scanang == 90.0)
  {
    deltax = h->deltayr;
  }
  else
  {
    deltax = sqrt(h->deltaxr * h->deltaxr + h->deltayr * h->deltayr);
  }

  return(deltax);
}


int cont_display_mapscan(image, plot)
struct LP_IMAGE_INFO *image;
int plot;
{
  float  ydata[MAX_CONT_SAMPLES]/*, *yp */;
  int    i, dlen, size, xb, logplot = 0;
  double deltax, peak, npeak;

  if(processContinuum(&image->head, &image->yplot1[0], &ydata[0], &dlen, "ContMap"))
  {
    return(1);
  }

  if(env->map_w == 0 || env->map_h == 0)
  {
    env->map_w = (int)image->head.noxpts;
    env->map_h = (int)image->head.noypts;
  }

  if(env->map_w != (int)image->head.noxpts)
  {
    st_report(" ");
    st_report("cont_display_mapscan(): Warning: Mis-match between env->map_w and head.noxpts");
    st_report(" ");
  }
  if(env->map_h != (int)image->head.noypts)
  {
    st_report(" ");
    st_report("cont_display_mapscan(): Warning: Mis-match between env->map_h and head.noypts");
    st_report(" ");
  }

  size = env->map_w;

  if(env->map_h > env->map_w)
  {
    size = env->map_h;
  }

  xb = size / 2 - size;
//  xe = size / 2;

  npeak =  9999999999999999.0;
  peak  = -9999999999999999.0;

  for(i=0;i<dlen;i++)
  {
    if(ydata[i] > peak)
    {
      peak = ydata[i];
    }

    if(ydata[i] < npeak)
    {
      npeak = ydata[i];
    }
  }

  st_report("Plot Data: Peak = %f, -Peak = %f", peak, npeak);

  npeak = fabs(npeak) + 0.001;

  if(npeak > peak)
  {
    peak = npeak;
  }

  clearFillContStruct(&image->head, dlen, "Map Offset", "{/Symbol T}_{/Symbol A}* (K)");

  cont_plain_send.yaxis_scale  = env->yaxis_scale;

  if(peak == 0.0)
  {
    st_fail("Scan %.2f Has a Peak of 0.0", image->head.scan);
    return(1);
  }

  logplot = isSet(PF_LOG_FLAG);

  deltax = findDeltaX(&image->head);

  for(i=0;i<dlen;i++)
  {
    cont_plain_send.xdata[i] = (float)(xb+i) * deltax;

    if(logplot)
    {
      cont_plain_send.ydata[i] = log10(fabs(ydata[i]) / peak) * 10.0;

      if(gsl_isnan(cont_plain_send.ydata[i]))
      {
	cont_plain_send.ydata[i] = -100.0;
      }
    }
    else
    {
      cont_plain_send.ydata[i] = ydata[i];
    }
  }

  if(xgraphic)
  {
    sendCont(SEND_PLAIN);
  }

  validate(CONT_MAP);

  return(0);
}


int processContinuum(head, phases, data, dl, mode)
struct HEADER *head;
float *phases, *data;
int   *dl;
char  *mode;
{
  int    i, dlen;
  float *yp;
  double avg;

  yp = &data[0];

  dlen = (int)head->noint;

  zero_check(mode, dlen, phases);

  dlen /= head->nophase;

  if(dlen > MAX_CONT_SAMPLES)
  {
    st_fail("Continuum Observation %s: too many samples: %d", mode, dlen);
    return(1);
  }

  if(env->issmt)
  {
    smt_bin_and_cal_data(yp, dlen, SWITCHED, phases, head);
  }
  else
  {
    kp_bin_and_cal_data(yp, dlen, SWITCHED, phases, head);
  }

  if(!strncmp(head->obsmode, "CONTQK5 ", 8) || !strncmp(head->obsmode, "CONTSEQ ", 8) || !strncmp(head->obsmode, "CONTMAP ", 8))
  {
    if(isSet(PF_B_FLAG)) /* Remove baseline average ?? */
    {
      avg = 0.0;

      for(i=0;i<dlen;i++) /* compute average */
      {
        avg += data[i];
      }

      avg /= (double)dlen;

      for(i=0;i<dlen;i++) /* Subtract average */
      {
        data[i] -= avg;
      }

      st_report("Removing an average baseline of: %.3f Kelvin", avg);

      env->zline = 1;
    }
    else
    {
      env->zline = 0;
    }
  }

  *dl = dlen;

  if(env->dobaseline && !strncmp(head->obsmode, "CONTMAP ", 8))
  {
    st_report("contDisplayMapScan(): Removing Baselines");
    cont_removeBaselines(yp, dlen, head->scan);
  }

  if(cont_determine_min_max(yp, dlen, head->scan))
  {
    return(1);
  }

  return(0);
}


int cont_display_five(image, plot)
struct LP_IMAGE_INFO *image;
int plot;
{
  float  ydata[MAX_CONT_SAMPLES];
  float  splits[2][5], means[5];
  int    i, j, dlen, old_plotstyle;;
  double bw;
  double x1, x2, x3;                                         /* X coordinates   */
  float  a, b, c;                                            /* curve constants */

  if(processContinuum(&image->head, &image->yplot1[0], &ydata[0], &dlen, "Five-Pt"))
  {
    return(1);
  }

  st_print("\n");
  st_print("   Az Offset: %7.2f, El Offset: %7.2f\n\n", image->head.uxpnt, image->head.uypnt);

  for(i=0,j=0;i<dlen;i+=4,j++)
  {
    st_print("       %7.2f %7.2f %7.2f %7.2f %s\n", 
			ydata[0+i], ydata[1+i], ydata[2+i], ydata[3+i], fiveStrs[j]);
  }

  st_print("\n");

  x1 = -image->head.bfwhm;
  x2 = 0.0;
  x3 = image->head.bfwhm;

  if(env->split_phases)
  {
    splits[OFFS][0] = (fabs(ydata[ 0]) + fabs(ydata[ 3])) / 2.0; /* North */
    splits[ONS ][0] = (fabs(ydata[ 1]) + fabs(ydata[ 2])) / 2.0;

    splits[OFFS][1] = (fabs(ydata[ 4]) + fabs(ydata[ 7])) / 2.0; /* South */
    splits[ONS ][1] = (fabs(ydata[ 5]) + fabs(ydata[ 6])) / 2.0;

    splits[OFFS][2] = (fabs(ydata[ 8]) + fabs(ydata[11])) / 2.0; /* Center */
    splits[ONS ][2] = (fabs(ydata[ 9]) + fabs(ydata[10])) / 2.0;

    splits[OFFS][3] = (fabs(ydata[12]) + fabs(ydata[15])) / 2.0; /* East */
    splits[ONS ][3] = (fabs(ydata[13]) + fabs(ydata[14])) / 2.0;

    splits[OFFS][4] = (fabs(ydata[16]) + fabs(ydata[19])) / 2.0; /* West */
    splits[ONS ][4] = (fabs(ydata[17]) + fabs(ydata[18])) / 2.0;

    st_print("                     %s\n", fiveStrs[0]);
    st_print("                   %7.2f\n", splits[OFFS][0]);
    st_print("                   %7.2f\n", splits[ONS ][0]);
    st_print(" \n");
    st_print(" \n");
    st_print(" \n");
    st_print(" \n");
    st_print("       %s         %s           %s\n", fiveStrs[3], fiveStrs[2], fiveStrs[4]);
    st_print("    %7.2f        %7.2f        %7.2f\n", 
				splits[OFFS][3], splits[OFFS][2], splits[OFFS][4]);
    st_print("    %7.2f        %7.2f        %7.2f\n", 
				splits[ONS ][3], splits[ONS ][2], splits[ONS ][4]);
    st_print(" \n");
    st_print(" \n");
    st_print(" \n");
    st_print("                     %s\n", fiveStrs[1]);
    st_print("                   %7.2f\n", splits[OFFS][1]);
    st_print("                   %7.2f\n", splits[ONS ][1]);
    st_print(" \n");

                            /* Determine the Gaussian curve that fits the AZ data */
    if( cont_gauss(splits[OFFS][3], splits[OFFS][2], splits[OFFS][4], x1, x2, x3, &a, &b, &c) )
    {
      if( (1.0 / b) > 0.0)
      {
        bw = sqrt( 1.0 / b) * 1.664;
      }

      st_print("AZ: OFFS: Peak = %7.2f, b = %f, Off: = %5.2f, BW = %7.2f, Corrected: %7.2f\n", 
									a, b, c, bw, c+image->head.uxpnt);
      azPtCorr[0] = c+image->head.uxpnt;
    }
    else
    {
      st_print("AZ: OFFS: Fit Failed\n");
    }

    if( cont_gauss(splits[ONS ][3], splits[ONS ][2], splits[ONS ][4], x1, x2, x3, &a, &b, &c) )
    {
      if( (1.0 / b) > 0.0)
      {
        bw = sqrt( 1.0 / b) * 1.664;
      }

      st_print("AZ:  ONS: Peak = %7.2f, b = %f, Off: = %5.2f, BW = %7.2f, Corrected: %7.2f\n", 
									a, b, c, bw, c+image->head.uxpnt);
      azPtCorr[1] = c+image->head.uxpnt;
    }
    else
    {
      st_print("AZ:  ONS: Fit Failed\n");
    }

                            /* Determine the Gaussian curve that fits the EL data */
    if( cont_gauss(splits[OFFS][1], splits[OFFS][2], splits[OFFS][0], x1, x2, x3, &a, &b, &c) )
    {
      if( (1.0 / b) > 0.0)
      {
        bw = sqrt( 1.0 / b) * 1.664;
      }

      st_print("EL: OFFS: Peak = %7.2f, b = %f, Off: = %5.2f, BW = %7.2f, Corrected: %7.2f\n", 
									a, b, c, bw, c+image->head.uypnt);
      elPtCorr[0] = c+image->head.uypnt;
    }
    else
    {
      st_print("EL: OFFS: Fit Failed\n");
    }

    if( cont_gauss(splits[ONS ][1], splits[ONS ][2], splits[ONS ][0], x1, x2, x3, &a, &b, &c) )
    {
      if( (1.0 / b) > 0.0)
      {
        bw = sqrt( 1.0 / b) * 1.664;
      }

      st_print("EL:  ONS: Peak = %7.2f, b = %f, Off: = %5.2f, BW = %7.2f, Corrected: %7.2f\n", 
									a, b, c, bw, c+image->head.uypnt);
      elPtCorr[1] = c+image->head.uypnt;
    }
    else
    {
      st_print("EL:  ONS: Fit Failed\n");
    }
  }
  else
  {
    means[0] = (-ydata[ 0] + ydata[ 1] + ydata[ 2] - ydata[ 3]) / 4.0; /* North */
    means[1] = (-ydata[ 4] + ydata[ 5] + ydata[ 6] - ydata[ 7]) / 4.0; /* South */
    means[2] = (-ydata[ 8] + ydata[ 9] + ydata[10] - ydata[11]) / 4.0; /* Center */
    means[3] = (-ydata[12] + ydata[13] + ydata[14] - ydata[15]) / 4.0; /* East */
    means[4] = (-ydata[16] + ydata[17] + ydata[18] - ydata[19]) / 4.0; /* West */

    st_print("                     %s\n", fiveStrs[0]);
    st_print("                   %7.2f\n", means[0]);
    st_print(" \n");
    st_print(" \n");
    st_print(" \n");
    st_print("       %s         %s           %s\n", fiveStrs[3], fiveStrs[2], fiveStrs[4]);
    st_print("    %7.2f        %7.2f        %7.2f\n", means[3], means[2], means[4]);
    st_print(" \n");
    st_print(" \n");
    st_print(" \n");
    st_print("                     %s\n", fiveStrs[1]);
    st_print("                   %7.2f\n", means[1]);
    st_print(" \n");

                            /* Determine the Gaussian curve that fits the AZ data */
    if( cont_gauss(means[3], means[2], means[4], x1, x2, x3, &a, &b, &c) )
    {
      if( (1.0 / b) > 0.0)
      {
        bw = sqrt( 1.0 / b) * 1.664;
      }

      st_print("AZ: Peak = %7.2f, b = %f, Off: = %5.2f, BW = %7.2f, Corrected: %7.2f\n", 
								a, b, c, bw, image->head.uxpnt-c);
      azPtCorr[2] = c+image->head.uxpnt;
    }
    else
    {
      st_print("AZ: Fit Failed\n");
    }
                            /* Determine the Gaussian curve that fits the EL data */
    if( cont_gauss(means[1], means[2], means[0], x1, x2, x3, &a, &b, &c) )
    {
      if( (1.0 / b) > 0.0)
      {
        bw = sqrt( 1.0 / b) * 1.664;
      }

      st_print("EL: Peak = %7.2f, b = %f, Off: = %5.2f, BW = %7.2f, Corrected: %7.2f\n", 
								a, b, c, bw, image->head.uypnt-c);
      elPtCorr[2] = c+image->head.uypnt;
    }
    else
    {
      st_print("EL: Fit Failed\n");
    }
  }

  clearFillContStruct(&image->head, dlen, "Sample Number", "{/Symbol T}_{/Symbol A}* (K)");

  for(i=0;i<dlen;i++)
  {
    cont_plain_send.xdata[i] = (float)(i) + 1.0;
    cont_plain_send.ydata[i] = ydata[i];
  }

  if(xgraphic)
  {
    old_plotstyle  = env->plotstyle;
    env->plotstyle = PLOT_STYLE_HISTO;

    sendCont(SEND_PLAIN);

    env->plotstyle = old_plotstyle;
  }

  validate(QK5);

  return(0);
}


int cont_display_any_focus(image, plot, type)
struct LP_IMAGE_INFO *image;
int plot, type;
{
  int    i, j, dlen, steps, save_ps;
  float  ydata[MAX_CONT_SAMPLES], *yp;
  double f0, wl, aa1[4];
  double xd[256], yd[256], s[256];
  char   fstr1[80], fstr2[80];

  switch(type)
  {
     case FOCALIZE:
	f0 = image->head.focusr;
	strcpy(fstr1, "AX-Focus");
	strcpy(fstr2, "AX Focus");
	break;

     case NS_FOCALIZE:
	f0 = image->head.focusv;
	strcpy(fstr1, "NS-Focus");
	strcpy(fstr2, "NS Focus");
	break;

     case EW_FOCALIZE:
	f0 = image->head.focush;
	strcpy(fstr1, "EW-Focus");
	strcpy(fstr2, "EW Focus");
	break;

     default:
	st_fail("Unknown Focus type: %d", type);
	return(1);
  }

  if(processContinuum(&image->head, &image->yplot1[0], &ydata[0], &dlen, fstr1))
  {
    return(1);
  }

  steps = (int)image->head.lamp;
  wl    =      image->head.lwid;
  yp    = &ydata[0];

  if(steps == 0)
  {
    steps = 7;
  }

  if(wl == 0.0)
  {
    wl = image->head.wl;
  }

  f0 -= (wl * (int)(steps/2));

  if(isSet(PF_V_FLAG))
  {
    st_report(" ");
  }

  for(i=0;i<dlen;i+=2)				/* subtract off's from on's */
  {
    ydata[i] -= ydata[i+1];

    if(isSet(PF_V_FLAG))
    {
      st_report("Ydata[%2d] = %6.1f", i, ydata[i]);
    }
  }

  if(isSet(PF_V_FLAG))
  {
    st_report(" ");
  }

  dlen = steps;
  for(i=0,j=0;i<dlen;i++)			/* scale & left justify the array */
  {
    ydata[i] = ydata[j] / 2.0;

    if(isSet(PF_V_FLAG))
    {
      st_report("Ydata[%2d] = %6.1f", i, ydata[i]);
    }

    j+=2;
  }

  if(cont_determine_min_max(yp, dlen, image->head.scan))
  {
    return(0);
  }

  st_report("Max value = %f", env->datamax);

  if(env->yaxis_scale == LOG_PLOT)
  {
    clearFillContStruct(&image->head, dlen, fstr2, "log({/Symbol T}_{R} (K))");

  }
  else
  {
    clearFillContStruct(&image->head, dlen, fstr2, "{/Symbol T}_{/Symbol A}* (K)");
  }

  for(i=0;i<dlen;i++)
  {
    cont_plain_send.xdata[i] = f0 + wl * (float)(i);

    if(env->yaxis_scale == LOG_PLOT && env->datamax != 0.0)
    {
      cont_plain_send.ydata[i] = 10.0 * log10(ydata[i] / env->datamax);
    }
    else
    {
      cont_plain_send.ydata[i] = ydata[i];
    }
  }

  cont_plain_send.yaxis_scale = env->yaxis_scale;

//  if(env->yaxis_scale != LOG_PLOT)
  {
    /* Now fit a gaussian */
    for(i=0;i<dlen;i++)
    {
      xd[i] = f0 + wl * (float)(i);
      yd[i] = ydata[i];
      s[i]  = 1.0;
    }

    aa1[0] = 0.0;
    aa1[1] = env->datamax;
    aa1[2] = xd[env->peakChan];
    aa1[3] = wl * 5.0;

    st_report("Guess: Center = %f @ %f: width = %f, dlen = %d", aa1[1], aa1[2], aa1[3], dlen);

    gauss_fit(&xd[0], &yd[0], &s[0], dlen-1, &aa1[0], isSet(PF_V_FLAG));

    st_report("%d-Point Gaussian Fit: Y = %.2f, X = %.2f, BW = %.2f", dlen, aa1[1], aa1[2], aa1[3]);

    env->glength = dlen;

    save_ps                = env->plotstyle;
    env->plotstyle         = PLOT_STYLE_LINEPTS;

    /* plotGauss sends the plot after filling in lots of other stuff */
    plotGauss(xd, dlen, aa1, &image->head, 2);

    env->plotstyle = save_ps;

  }
//  else
//  {
//    save_ps                = env->plotstyle;
//    env->plotstyle         = PLOT_STYLE_LINEPTS;
//
//    sendCont(SEND_PLAIN);
//
//    env->plotstyle = save_ps;
//  }

  validate(type);

  return(0);
}



int zero_check(who, dl, data)
char *who;
int dl;
float *data;
{
  int i;

  for(i=0;i<dl;i++)
  {
    if(!data[i])
    {
      st_fail("zero_check(): %s data contains ZERO'S, ignoring.", who);

      return(0);
    }
  }

  return(0);
}


int cont_save_data(data, h)
float *data;
struct HEADER *h;
{
  float  ydata[MAX_CONT_SAMPLES], *yp;
  int    i, dlen;
  char fname[512], astr[256];
  FILE *fp;

  if(processContinuum(h, data, &ydata[0], &dlen, "save-Data"))
  {
    return(1);
  }

  yp = &ydata[0];

  if(env->dobaseline)
  {
    st_report("contSaveData(): Removing Baselines");
    cont_removeBaselines(yp, dlen, h->scan);
  }

  if(env->mapangle == 0)
  {
    strcpy(astr, "AZs00");
  }
  else
  if(env->mapangle == 90)
  {
    strcpy(astr, "ELs90");
  }
  else
  if(env->mapangle == 45)
  {
    strcpy(astr, "Pos45");
  }
  else
  if(env->mapangle == -45)
  {
    strcpy(astr, "Neg45");
  }
  else
  {
    sprintf(astr, "%05d", env->mapangle);
  }

  sprintf(fname, "CCMS-%.2f_%.2f_%s.dat", h->scan, h->focusr, astr);

  if((fp = fopen(fname, "w")) == NULL)
  {
    st_fail("Unable to open %s for writting", fname);
    return(1);
  }

  st_report("Saving results to: %s", fname);

  fprintf(fp, "# Scan Number    %8.2f\n", h->scan);
  fprintf(fp, "# AX-Focus       %8.2f\n", h->focusr);
  fprintf(fp, "# Beam #1 Peak:  %8.2f\n", beam_map.gmap_peak1);
  fprintf(fp, "# Beam #1 Width: %8.2f\n", beam_map.gmap_bw1);
  fprintf(fp, "# Deconvolved:   %8.2f\n", decon_beam_size);
  fprintf(fp, "# Beam #2 Peak:  %8.2f\n", beam_map.gmap_peak2);
  fprintf(fp, "# Beam #2 Width: %8.2f\n", beam_map.gmap_bw2);
  fprintf(fp, "# Map Angle:     %8d\n",   env->mapangle);
  fprintf(fp, "# \n");

  /* Don't worry about sqrt(deltaxr^2+deltayr^2) here because Nick
     does that in his analysis program; Just use sample number */
  for(i=0;i<dlen;i++,yp++)
  {
    fprintf(fp, "%3d %10.4f\n", i, *yp);
  }

  fclose(fp);

  return(0);
}


/* Display a table of switched values */
int cont_display_data(data, h)
float *data;
struct HEADER *h;
{
  int    i, dlen, nons=0, noffs=0;
  float  ydata[MAX_CONT_SAMPLES], *yp;
  double ons, offs, ratio, gainEl;
  char   fname[256];
  FILE  *fp;

  if(processContinuum(h, data, &ydata[0], &dlen, "Show-Data"))
  {
    return(1);
  }

  if(isSet(PF_S_FLAG))
  {
    nons = noffs = 0;
    ons  = offs  = 0.0;

    yp = &ydata[0];
    for(i=0;i<dlen;i++,yp++)
    {
      if(*yp > 0.0)
      {
        ons += *yp;
	nons++;
      }
      else
      {
        offs += *yp;
	noffs++;
      }
    }

    if(nons)
    {
      ons /= (double)nons;
    }

    if(noffs)
    {
      offs /= (double)noffs;
    }

    offs = fabs(offs);

    if(env->elgain) /* Does user want to apply elevation gain curve? */
    {

/* SMT Elevation Gain Curve Derived by Issaoun & Folkers 2016 */
      gainEl = env->elgain_term5 * pow(h->el, 4.0) + 
               env->elgain_term4 * pow(h->el, 3.0) + 
               env->elgain_term3 * pow(h->el, 2.0) + 
               env->elgain_term2 * h->el           + 
               env->elgain_term1;

      if(gainEl < 0.5)
      {
        gainEl = 1.0;
      }

      st_report("Scaling (Tmb / %f), based on Elev Gain Curve", gainEl);

      ons  /= gainEl;
      offs /= gainEl;
    }
    else
    {
      gainEl = 1.0;
    }

    ratio = ons / offs;

    st_report("");
    st_report("M-Beam = %+6.1f, Average of %2d ONS:      %f", h->mbeam[0], nons,  ons);
    st_report("P-Beam = %+6.1f, Average of %2d OFFS:     %f", h->pbeam[0], noffs, offs);
    st_report("Total  = %6.1f, Average of %2d ONS+OFFS: %f, Ratio: %5.2f", 
			fabs(h->pbeam[0]) + fabs(h->mbeam[0]), 
			    (nons         + noffs), 
			    (ons          + offs) / 2.0,
			ratio);


    st_report("");

    sprintf(fname, "sequence_splits.csv");
    fp = fopen(fname, "a");

    if(fp)
    {
      fprintf(fp, "%8.2f, %7.3f, %6.4f, %7.1f, %2d, %7.3f, %7.1f, %2d, %7.3f, %6.3f\n", 
			h->scan, 
			h->obsfreq / 1e3,
			gainEl,
			h->mbeam[0], nons,   ons, 
			h->pbeam[0], noffs,  offs,
			ratio);

      fclose(fp);

      st_report("Wrote scan %.2f results to %s", h->scan, fname);
    }
    else
    {
      st_report("Unable to open %s for appending", fname);
    }
  }
  else
  {
    yp = &ydata[0];
    for(i=0;i<dlen;i++,yp++)
    {
      if(!(i%8))
      {
        printf("\n%3d: ", i);
      }

      printf("%10.2f ", *yp);
    }

    printf("\n");
  }

  return(0);
}


int setAutoCal()
{
  if(strlen(args->secondArg))
  {
    autoCalibrate = atof(args->secondArg);

    autoAccum[0]    = autoAccum[1]    = autoAccum[2]    = autoAccum[3]    = 0.0;
    autoAccumCnt[0] = autoAccumCnt[1] = autoAccumCnt[2] = autoAccumCnt[3] = 0;

    accumSelected();
  }

  return(0);
}


int turnOns()
{
  int ons;

  if(strlen(args->secondArg))
  {
    ons = atoi(args->secondArg);

    if(ons == 0)
    {
      env->ons = ons;
      showScan();
    }
    else
    if(ons == 1)
    {
      env->ons = ons;
      showScan();
    }
    else
    {
      st_fail("'Ons' must be 0 or 1");
    }
  }

  if(env->ons == 1)
  {
    st_report("'Ons' are On");
  }
  else
  {
    st_report("'Ons' are Off");
  }

  return(0);
}


int crissCross()
{
  int bl, i, scan, first=1, save_ps, save_bl;
  char cmdBuf[256];
  struct HEADER *p;

  p = &sa[0];

  save_ps = env->plotstyle;
  env->plotstyle = PLOT_STYLE_LINES;

  save_bl = env->dobaseline;

  env->yaxis_scale = LINEAR_PLOT;			/* It won't work otherwise */

  if(isSet(PF_B_FLAG))					// remove baselines ?
  {
    bl = 1;
  }
  else
  {
    bl = 0;
  }

  env->dobaseline = bl;

  clearInterrupt();

  for(i=0;i<env->numofscans;i++,p++)
  {
    if(meetsCritiria(p, CHECK_ALL))
    {
      if(env->mode == MODE_CONT)
      {
        if(strncmp(p->obsmode, "CONTMAP", 7)) /* We only process MAP's */
        {
          continue; /* No good */
        }
      }

      if(isScanIgnored(p->scan))
      {
        continue;
      }

      if(first)
      {
        scan = (int)p->scan;
        first = 0;
      }

      if((int)p->scan != scan)
      {
        scan = (int)p->scan;
      }

      st_report("crissCross(): Processing Criss Cross Scan %.2f", p->scan);

      sprintf(cmdBuf, "get %.2f", p->scan);
      parseCmd(cmdBuf);

      env->dobaseline = bl;				/* Must set this each time; getScan() resets it to 0 */

      sprintf(cmdBuf, "show");
      parseCmd(cmdBuf);

      st_report("crissCross(): Delaying %.1f sec", 0.2);
      usleep(200000);					// give xgraphics time to plot

      env->mapangle = p->scanang;

      if(isSet(PF_G_FLAG)) 				// Gauss it ?
      {
        st_report("crissCross(): Performing a Gaussian fit");

        sprintf(cmdBuf, "gauss /p");
        parseCmd(cmdBuf);

        st_report("crissCross(): Delaying %.1f sec", 0.2);

        usleep(200000);					// give xgraphics time to plot
      }

      env->dobaseline = bl;

      if(isSet(PF_R_FLAG)) 				// Save it ?
      {
        st_report("crissCross(): Saving Data");

        saveData();
      }

      if(isSet(PF_J_FLAG))
      {
        st_report("crissCross(): Saving PNG Plot");

        sprintf(cmdBuf, "make /c /g");
        parseCmd(cmdBuf);

        usleep(1000000);
      }

      if(isSet(PF_D_FLAG))				// Delay ?
      {
        st_report("crissCross(): Delaying %d uSec", env->loopdelay2);
        usleep(env->loopdelay2);
      }
      else
      {
        st_report("crissCross(): Delaying %d uSec", env->loopdelay1);
        usleep(env->loopdelay1);
      }

      st_report(" ");
    }

    if(isInterrupt())
    {
      st_report("crissCross(): stopping");
      env->dobaseline = save_bl;			// Restore a few things
      env->plotstyle = save_ps;

      return(0);
    }
  }

  env->dobaseline = save_bl;				// Restore a few things
  env->plotstyle = save_ps;

  return(0);
}

