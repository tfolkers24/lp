/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

extern struct GSKEY getset[];

#define ZRO ((struct ENVIRONMENT *)0)

#define ENV_NOT_SAVED  0
#define ENV_SAVED      1

struct ENVIRONMENT savedEnv;
int                envSaved = ENV_NOT_SAVED;

int    envNumber     = 0;

/* These number should be protected */
double pi            = M_PI;
double plancK        = 6.62606957e-34;
double boltzmann     = 1.3806488e-23;
double ctr           = M_PI / 180.0; /*  0.017453293 */
double ctd           = 180.0 / M_PI; /* 57.295779513 */
double sol           = 299792458.0;  /* Meters per sec */


double last_mouse_x  = 0.0;
double last_mouse_x2 = 0.0;
double last_mouse_y  = 0.0;
double last_mouse_y2 = 0.0;
double manual_tcal   = 0.0;
double gnuplotLevel  = 0.0;
double gnupatchlevel = 0.0;

int interpl8_debug   = 0;
int stquiet          = 0;
int use_multi_hist   = 0;

int    last_plot     = LAST_PLOT_SPEC_ACCUM;


struct ENV_VARIABLES {
	char *label;
	char *env_name;
	char *variable;
	char *alternitive;
};


struct ENV_VARIABLES env_vars[] = {
	{ "Package Dir",  "LINUXPOPS_HOME",         pkg_home,     "/home/analysis/linuxpops"		},
	{ "Data Dir",     "OBSDAT",                 data_home,    "/home/data"				},
	{ "Obs Dir",      "HOME",                   obs_home,     "./"					},
	{ "Obs Inits",    "OBSINIT",                obs_inits,    "sys"					},
	{ "Printer",      "PRINTER",                printer,      "lp"					},
	{ "Help Dir",     "LINUXPOPS_HELP_DIR",     help_home,    "/home/analysis/linuxpops/help"	},
	{ "Demo Dir",     "DEMO_DIR",               demo_dir,     "/home/analysis/linuxpops/demo"	},
	{ "Hostname",     "HOST",                   hostname,     "Unknown_Host"			},
	{ "Session",      "LPSESSION",              session,      "12345"				},
	{ "Mailbox Defs", "MAILBOXDEFS",            mailboxdefs,  "../mailboxdefs"			},
	{ "Site",         "SITE",                   site_name,    "SITE_NONE"				},
	{ "Gnuplot Dir",  "LINUXPOPS_GNUPLOT",      gnuplot_ver,  "/usr/bin/gnuplot"			},
	{ "Default Env",  "LINUXPOPS_DEFAULT_ENV",  default_env,  "0"					},
	{ "Doc Viewer",   "LINUXPOPS_DOC_VIEWER",   doc_viewer,   "evince"				},
	{ "Xmgr Plotter", "XMGRACE",                xmgr_plotter, "/sur/bin/xmgrace"			},

	{ NULL,            NULL,                    NULL,         NULL 					}
};


struct DVARS_INIT {
	char   *name;
	int     type;
	int    *iaddr;
	double *daddr;
	void   *offset;
	int   (*fctn)();
};


struct DVARS_INIT dvars_init[] = {
	{ "pi",	            DYN_TYPE_DOUBLE, NULL,              &pi,		0,	NULL },
	{ "planck",         DYN_TYPE_DOUBLE, NULL,              &plancK,	0,	NULL },
	{ "boltzmann",      DYN_TYPE_DOUBLE, NULL,              &boltzmann,	0,	NULL },
	{ "ctr",            DYN_TYPE_DOUBLE, NULL,              &ctr,	        0,	NULL },
	{ "ctd",            DYN_TYPE_DOUBLE, NULL,              &ctd,	        0,	NULL },
	{ "sol",            DYN_TYPE_DOUBLE, NULL,              &sol,	        0,	NULL },

/* set DYN_VARIBLE_PROTECTED to be the number of these that user can't touch; currently 6 */

	{ "last_mouse_x",   DYN_TYPE_DOUBLE, NULL,              &last_mouse_x,	0,	NULL },
	{ "last_mouse_x2",  DYN_TYPE_DOUBLE, NULL,              &last_mouse_x2,	0,	NULL },
	{ "last_mouse_y",   DYN_TYPE_DOUBLE, NULL,              &last_mouse_y,	0,	NULL },
	{ "last_mouse_y2",  DYN_TYPE_DOUBLE, NULL,              &last_mouse_y2,	0,	NULL },
	{ "manual_tcal",    DYN_TYPE_DOUBLE, NULL,              &manual_tcal,	0,	NULL },

	{ "interpl8_debug", DYN_TYPE_INT,    &interpl8_debug,   NULL,		0,	NULL },
	{ "stquiet",        DYN_TYPE_INT,    &stquiet,          NULL,		0,	NULL },
	{ "use_multi_hist", DYN_TYPE_INT,    &use_multi_hist,   NULL,		0,	NULL },

	{ NULL,                       0,     NULL,             NULL,		0,	NULL }
};

#define MAX_DVARS_INIT ((sizeof(dvars_init) / sizeof(struct DVARS_INIT))-1)



int clearEnv()
{
  int nenv;

  if(strlen(args->secondArg))
  {
    nenv = atoi(args->secondArg);

    if(nenv < 0 || nenv >= MAX_ENV)
    {
      st_fail("Env Number %d, out of range: 0 - %d", nenv, MAX_ENV);
      return(1);
    }

    st_report("Clearing Environment %d", nenv);

    bzero((char *)&CGM->envrs[nenv], sizeof(struct ENVIRONMENT));
  }

  return(0);
}



int setDVars()
{
  int j;

/* These are constant address variables */
  for(j=0;j< MAX_DVARS_INIT;j++)
  {
    strcpy(dvars[j].name, dvars_init[j].name);
    dvars[j].type = dvars_init[j].type;
    dvars[j].fctn = dvars_init[j].fctn;

    if(dvars_init[j].type == DYN_TYPE_DOUBLE)
    {
      dvars[j].dval = dvars_init[j].daddr;
    }
    else
    {
      dvars[j].ival = dvars_init[j].iaddr;
    }
  }

  CGM->ndvars_used = j;

  return(0);
}


int mapGS2Env()
{
  int i, ngs;
  extern int getSizeOfGS();
  struct GSKEY *p;

  ngs = getSizeOfGS();

  p = &getset[0];
  for(i=0;i<ngs;i++,p++)
  {
    switch(p->type)
    {
      case INTEGER:
        p->iaddr =  (int *)((long)env + (long)p->offset);

//        st_report("Setting %s %d", p->name, *p->iaddr);
	break;

      case DOUBLE:
        p->daddr = (double *)((long)env + (long)p->offset);
//        st_report("Setting %s %f", p->name, *p->daddr);
	break;

      case CHARSTAR:
        p->caddr = (char *)((long)env + (long)p->offset);
//        st_report("Setting %s %s", p->name, p->caddr);
	break;

    }
  }

  return(0);
}

int setEnv()
{
  int tmp;

  if(strlen(args->secondArg))
  {
    tmp = atoi(args->secondArg);

    if(tmp >= 0 && tmp < MAX_ENV)
    {
      if(use_multi_hist) /* Save this env history first */
      {
	rwHistory(WRITE_HISTORY);
      }

      envNumber = tmp;
      env = &CGM->envrs[envNumber];

      CGM->which_env = envNumber;

      if(use_multi_hist) /* Open previous history for this env */
      {
	rwHistory(READ_HISTORY);
      }

      if(strlen(env->title))
      {
        st_report("Switching to Environment %s", env->title);
      }
      else
      {
        st_report("Switching to Environment Index %d", envNumber);
        st_report("Reseting this environment to the defaults");
        resetParams();
        st_report("Be Sure to set a data filename and 'name' this environment");
      }

      if(strlen(env->filename))
      {
        subArgs(2, env->filename);
        args->ntokens = 2;
        loadFile();
      }

      if(currentImage != env->currentImage)
      {
        switch(env->currentImage)
	{
	  case 0: _h0(); break;
	  case 1: _h1(); break;
	  case 2: _h2(); break;
	  case 3: _h3(); break;
	  case 4: _h4(); break;
	  case 5: _h5(); break;
	  case 6: _h6(); break;
	  case 7: _h7(); break;
	  case 8: _h8(); break;
	  case 9: _h9(); break;
	}
      }

      setDVars();

      mapGS2Env();

      env->accumreset = 1;

      freeY();
      freeX();

      setPrompt();

      initLetterHelp();

      if(env->hwdc < 0.5) /* Check for Un-Initialized hardware duty-cycle */
      {
        env->hwdc = 1.0;
      }

      initGnuplotBackground(); /* Check and Set Gnuplot background color */

      zeroOutUndos();
    }
    else
    {
      st_fail("Environment Index %d out of range; Valid Range: 0 to %d", tmp, MAX_ENV-1);
    }

    stackEmpty();
  }
  else
  {
    st_warn("Missing Arg for setEnv()");
  }

  return(0);
}


int nameEnv()
{
  if(strlen(args->secondArg))
  {
    sprintf( env->title, "%s %s %s %s", args->secondArg, args->thirdArg, args->fourthArg, args->fifthArg);
    strcmprs(env->title);

    st_report("Named Environment %d to %s", envNumber, env->title);
  }
  else
  {
    st_warn("Missing Arg for nameEnv()");
  }

  return(0);
}


int tellEnv()
{
  int i;
  char *active;

  printTimeStamp();

  reportEnv();

  st_report("Previously Defined Environments:");
  st_report("Env        Title         Mode    SMT     Source    Freq      Data File");
  st_report("");

  for(i=0;i<MAX_ENV;i++)
  {
    if(i == envNumber)
    {
      active = "*";
    }
    else
    {
      active = " ";
    }

    if(strlen(CGM->envrs[i].title))
    {
      st_report("%2d%s  %16.16s  %9.9s  %d %12.12s  %6.2f %16.16s", i, active,
                        CGM->envrs[i].title,
			modeStrs[CGM->envrs[i].mode],
                        CGM->envrs[i].issmt,
                        CGM->envrs[i].source,
                        CGM->envrs[i].freq,
                        basename(CGM->envrs[i].filename)
                );
    }
    else
    {
      st_report("%2d%s  %16.16s", i, active, "Unused");
    }
  }

  return(0);
}


int copyEnv()
{
  int src = 0, dest = 0, len;

  len = strlen(args->secondArg);
  if(len)
  {
    src = atoi(args->secondArg);
  }

  len = strlen(args->thirdArg);
  if(len)
  {
    dest = atoi(args->thirdArg);
  }

  if(src < 0 || src > (MAX_ENV-1))
  {
    st_fail("Source Environment Index: %d, out of range", src);

    return(1);
  }

  if(dest < 0 || dest > (MAX_ENV-1))
  {
    st_fail("Destination Environment Index: %d, out of range", dest);

    return(1);
  }

  if(src == dest)
  {
    st_fail("Source & Destination are the same: %d %d", src, dest);
    return(1);
  }

  st_report("Coping Environment %d to %d", src, dest);

  bcopy((char *)&CGM->envrs[src], (char *)&CGM->envrs[dest], sizeof(struct ENVIRONMENT));

  if(dest == envNumber)
  {
    st_report("Force Reloading Current Env");

    subArgs(2, "%d", envNumber);

    setEnv();
  }

  return(0);
}


int reportEnv()
{
  struct ENV_VARIABLES *p;

  p = &env_vars[0];

  st_report("");

  for(;p->env_name;p++)
  {
    st_report("%-24s %-14.14s: %s", p->env_name, p->label, p->variable);
  }

  st_report("");

  return(0);
}


int getEnvironment()
{
  char *cp, *pp, tbuf[256], cmdBuf[256];
  struct ENV_VARIABLES *p;
  FILE *fp;

  p = &env_vars[0];

  for(;p->env_name;p++)
  {
    cp = getenv(p->env_name);

    if(cp)
    {
      strcpy(p->variable, cp);
    }
    else
    {
      strcpy(p->variable, p->alternitive);
    }

    if(strstr(p->env_name, "GNUPLOT")) /* Go get version */
    {
      sprintf(cmdBuf, "gnuplot -V");

      if( (fp = popen(cmdBuf, "r") ) <= (FILE *)0 )
      {
        st_fail("getEnvironment(): Unable to open %s", cmdBuf);

        continue;
      }

      bzero(tbuf, sizeof(tbuf));

	/* Expected results: "gnuplot 4.4 patchlevel 0" */
      while((cp = fgets(tbuf, sizeof(tbuf), fp)) != NULL)
      {
        deNewLineIt(tbuf);
        strcat(p->variable, " -V ");
        strcat(p->variable, tbuf+8); /* Skip the name */

        gnuplotLevel = atof(tbuf+8);

	pp = strstr(tbuf, "patchlevel");

	if(pp)
	{
	  gnupatchlevel = atof(pp+10);
	}

	break;
      }

      pclose(fp);
    }
  }

  if(!strcmp(site_name, "KITT_PEAK"))
  {
    site = SITE_KITT_PEAK;
  }
  else
  if(!strcmp(site_name, "MT_GRAHAM"))
  {
    site = SITE_MT_GRAHAM;
  }
  else
  if(!strcmp(site_name, "TUCSON"))
  {
    site = SITE_TUCSON;
  }
  else
  {
    site = SITE_NONE;
  }

  return(0);
}


char *replace_str(char *str, char *orig, char *rep)
{
  static char buffer[4096];
  char *p;

  if(!(p = strstr(str, orig)))  // Is 'orig' even in 'str'?
  {
    return str;
  }

  strncpy(buffer, str, p-str); // Copy characters from 'str' start to 'orig' st$
  buffer[p-str] = '\0';

  sprintf(buffer+(p-str), "%s%s", rep, p+strlen(orig));

  return(buffer);
}


int substituteEnv()
{
  int i;
  char *cp, tbuf[256];
  struct ENV_VARIABLES *p;

  if(verboseLevel & VERBOSE_PARSE)
  {
    st_report("substituteEnv(): Entering with %d tokens", args->ntokens);
  }

  for(i=0;i<args->ntokens;i++)
  {
    p = &env_vars[0];

    for(;p->env_name;p++)
    {
      sprintf(tbuf, "$%s", p->env_name);

      if(verboseLevel & VERBOSE_PARSE)
      {
        st_report("substituteEnv(): Comparing if: %s is in: %s", tbuf, args->tok[i]);
      }

      if(strstr(args->tok[i], tbuf))
      {
        if(verboseLevel & VERBOSE_PARSE)
        {
          st_report("substituteEnv(): Found %s in %s", tbuf, args->tok[i]);
        }

	cp = replace_str(args->tok[i], tbuf, p->variable);

        if(verboseLevel & VERBOSE_PARSE)
        {
          st_report("substituteEnv(): New value: %s ", cp);
        }

        strcpy(args->tok[i], cp);
      }
    }
  }

  if(verboseLevel & VERBOSE_PARSE)
  {
    st_report("substituteEnv(): Leaving");
  }

  return(0);
}


int initOnlineEnv()
{
  int i;
  char tmp[256];

  st_report("Initializing Environments for site: %s", siteNames[site]);

  onlineversion = findDataVersion( obs_inits, tmp );
  sprintf(onlineDataFile, "%s/sdd.%3.3s_%03d", data_home, obs_inits, onlineversion);

  env = &CGM->envrs[0]; /* Point to online env */
  strcpy(env->filename, onlineDataFile); /* Set the online filename */
  strcpy(env->title, "Online DBE"); 
  env->mode = MODE_CONT;
  
  switch(site)
  {
    case SITE_KITT_PEAK:
  	  env = &CGM->envrs[1]; /* Point to online env */
  	  sprintf(tmp, "%s/sdd.%3.3s_%03d", data_home, obs_inits, onlineversion);
	  strcpy(env->filename, tmp); /* Set the online filename */
	  strcpy(env->title, "Online Filters"); 
	  env->mode = MODE_SPEC;

  	  env = &CGM->envrs[2]; /* Point to online env */
  	  sprintf(tmp, "%s/sdd_hc.%3.3s_%03d", data_home, obs_inits, onlineversion);
	  strcpy(env->filename, tmp); /* Set the online filename */
	  strcpy(env->title, "Online MAC"); 
	  env->mode = MODE_SPEC;

  	  env = &CGM->envrs[3]; /* Point to online env */
  	  sprintf(tmp, "%s/sdd_aro.%3.3s_%03d", data_home, obs_inits, onlineversion);
	  strcpy(env->filename, tmp); /* Set the online filename */
	  strcpy(env->title, "Online AROWS"); 
	  env->mode = MODE_SPEC;
	break;

    case SITE_MT_GRAHAM:
  	  env = &CGM->envrs[1]; /* Point to online env */
  	  sprintf(tmp, "%s/sdd_ffb.%3.3s_%03d", data_home, obs_inits, onlineversion);
	  strcpy(env->filename, tmp); /* Set the online filename */
	  strcpy(env->title, "Online Filters"); 
	  env->mode = MODE_SPEC;

  	  env = &CGM->envrs[2]; /* Point to online env */
  	  sprintf(tmp, "%s/sdd_aos.%3.3s_%03d", data_home, obs_inits, onlineversion);
	  strcpy(env->filename, tmp); /* Set the online filename */
	  strcpy(env->title, "Online AOS"); 
	  env->mode = MODE_SPEC;
	break;

    case SITE_TUCSON:
	break;
  }

  for(i=0;i<MAX_ENV;i++)
  {
    if(CGM->envrs[i].tsyslimit == 0.0)
    {
      CGM->envrs[i].tsyslimit  = BOGUS_TSYS_VALUE;
    }

    if(CGM->envrs[i].tcallimit == 0.0)
    {
      CGM->envrs[i].tcallimit  = BOGUS_TCAL_VALUE;
    }

    if(CGM->envrs[i].loopdelay1 == 0)
    {
      CGM->envrs[i].loopdelay1 =  100000; /* 100 mSecs */
    }

    if(CGM->envrs[i].loopdelay2 == 0)
    {
      CGM->envrs[i].loopdelay2 = 2000000; /* 2 Secs */
    }
  }

  subArgs(2, "0");
  setEnv();

  return(0);
}


/**
 * Save a copy of the env for later restoration
 */
int saveEnv()
{
  bcopy((char *)env, (char *)&savedEnv, sizeof(savedEnv));

  envSaved = ENV_SAVED;

  return(0);
}


/**
 * Restore a previously saved env
 */
int restoreEnv()
{
  if(envSaved == ENV_NOT_SAVED)
  {
    st_fail("No Environment Saved");

    return(1);
  }

  bcopy((char *)&savedEnv, (char *)env, sizeof(savedEnv));

  envSaved = ENV_NOT_SAVED;

  return(0);
}
