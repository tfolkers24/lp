int doShell()
{
  int  i, n;
  int    indx;
  struct PARSE_FLAGS *p;

  if(h0->head.scan < 1.0)
  {
    st_warn("No Scan loaded; Use 'get scan#'");
    return(0);
  }

  p = findFirstFlag();

  if(p)
  {
    indx = p->index;    /* Save value */

    shiftFlags();       /* Remove this first flag and decriment is 'set' count */

    switch(indx)
    {
      case PF_D_FLAG:                           /* Automatic find and fit */

        n = getNset();				/* if you expect /n flags */

	if(n < 0)
 	{
	  /* missing n arg */
	}

	/* call some procedure here */

        xx();					/* if needed */

        return(0);

        break;

     default:

        printH_FlagHelp(args->firstArg);

        return(1);

        break;


    }
  }

  /* Do default action */

  printHelp("function");

  return(0);
}
