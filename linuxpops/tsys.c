/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

/* Elev Min & Max */
static double elmin =   10.0;
static double elmax =   90.0;

/* Freq Min & Max */
static double Fmin  =   68.0;
static double Fmax  =  900.0;

/* Tsys Max */
static double tmax  = 2600.0;

static char tsys_tok[40][40];
static int tsys_num = 0;


static struct TSYS_SEND tsys_send;
static struct TSYS_ELEM tsys_freqs[MAX_TSYS_BINS];

/* Sort by freq first; then el */
int tsysSort(p1, p2)
struct TSYS_ELEM *p1, *p2;
{

  if(p1->freq < p2->freq)
  {
    return(-1);
  }
  else
  if(p1->freq > p2->freq)
  {
    return(1);
  }
  else
  {
    return(0);
  }
}


int doTsysSort()
{
  st_report("Sorting %d elements", tsys_num);

#ifndef __DARWIN__
  qsort(&tsys_freqs, tsys_num, sizeof(struct TSYS_ELEM), (__compar_fn_t)tsysSort);
#else
  qsort(&tsys_freqs, tsys_num, sizeof(struct TSYS_ELEM), tsysSort);
#endif

  return(0);
}


int findIndex(f, first)
double f;
int first;
{
  int i, indx = -1;
  int freq;

  freq = (int)round(f);

  for(i=0;i<MAX_TSYS_BINS;i++)
  {
    if(tsys_freqs[i].used)
    {
      if(tsys_freqs[i].freq == freq) /* Found a match */
      {
        return(i);
      }
    }
  }

  /* Mark and Return first unused bin */
  for(i=0;i<MAX_TSYS_BINS;i++)
  {
    if(!tsys_freqs[i].used)
    {
      tsys_freqs[i].used = 1;
      tsys_freqs[i].freq = freq;

      return(i);
    }
  }

  return(indx);
}



/**
 * Split single command up into tokens
 */
int tsys_get_tok(edit)
char *edit;
{
  int c = 0;
  char *token;

  bzero((char *)tsys_tok, sizeof(tsys_tok));

  token = strtok(edit, "\n\t ");
  while(token != (char *)NULL)
  {
    sprintf(tsys_tok[c++], "%s", token);
    token = strtok((char *)NULL, "\n\t ");
  }

  return(c);
}


int doTsysGUI(tsys)
struct TSYS_SEND *tsys;
{
  int i, j, n, freq;
  char tbuf[256], input[1024], results[256];

  if(!tsys_send.num)
  {
    st_fail("No Tsys data to Select From");
    return(1);
  }

  sprintf(input, "--list --width=300 --height=400 --text=\"Select Frequencies to Display\" --title=\"Select Frequencies\" --checklist --multiple --column=\"Select\" --column=\"Frequency\" --separator=\" \" ");

  for(i=0;i<tsys->num;i++)
  {
    sprintf(tbuf, "%s %03d ", tsys->freqs[i].used ? "TRUE" : "FALSE", tsys->freqs[i].freq);

    strcat(input, tbuf);
  }

  bzero(results, sizeof(results));

  doZenity(input, results);

  st_report("You Selected: %s");

  n = tsys_get_tok(results);

  if(n > 0)
  {
    for(j=0;j<tsys->num;j++)
    {
      tsys->freqs[j].used = 0;
    }

    for(i=0;i<n;i++)
    {
      freq = atoi(tsys_tok[i]);

      for(j=0;j<tsys->num;j++)
      {
        if(freq == tsys->freqs[j].freq)
        {
	  tsys->freqs[j].used = 1;
        }
      }
    }
  }
  else
  {
    return(1);
  }

  return(0);
}

int doTsysIngest()
{
  int i, j, indx, el, first = 1;
  struct HEADER *p;
  double freq;

  bzero((char *)&tsys_freqs, sizeof(tsys_freqs));

  j = 0;
  p = &sa[0];
  for(i=0;i<env->numofscans;i++,p++)
  {
    if(!meetsCritiria(p, CHECK_BKEND)) /* Only use matching backends */
    {
      continue;
    }

    freq = p->restfreq / 1000.0;

    if(freq < Fmin || freq > Fmax)
    {
      continue;
    }

    if(p->el < elmin || p->el > elmax)
    {
      continue;
    }

    if(fabs(p->stsys) > tmax)
    {
      continue;
    }

    indx  = findIndex(freq, first);
    first = 0;

    if(indx >=0 && indx < MAX_TSYS_BINS)
    {
//      st_print("Using freq bin %d, %d\n", indx, tsys_freqs[indx].freq);

      el = (int)(round(p->el));

      if(el >= elmin && el <= elmax)
      {
        tsys_freqs[indx].tsys[el] += (int)round(fabs(p->stsys));
        tsys_freqs[indx].cnts[el]++;
      }

      j++;
    }
    else
    {
      st_print("Unable to find freq index for scan %.2f\n", p->scan);
    }
  }

  st_report("Processed %d scans", j);

  tsys_num = 0;
  for(i=0;i<MAX_TSYS_BINS;i++)
  {
    if(tsys_freqs[i].used)
    {
      for(el=elmin;el<elmax;el++)
      {
        if(tsys_freqs[i].cnts[el])
        {
          tsys_freqs[i].tsys[el] /= tsys_freqs[i].cnts[el];
        }
      }

      tsys_num++;
    }
  }

  doTsysSort();
  doTsysPrepare();

  return(0);
}


int doTsys3D()
{
  int i, j, el, first=1, fm=68, fx=900;
  char fname[256], sysBuf[300];
  FILE *fp;

  sprintf(fname, "/tmp/%s/tsys.dat", session);

  if((fp = fopen(fname, "w")) == NULL)
  {
    st_fail("Unable to open %s file", fname);

    return(0);
  }

  j = 0;
  for(i=0;i<tsys_send.num;i++)
  {
    if(tsys_send.freqs[i].used)
    {
      fprintf(fp, "\n");

      if(first)
      {
        fm = tsys_send.freqs[i].freq;
	first = 0;
      }

      fx = tsys_send.freqs[i].freq;

      for(el=elmin;el<elmax;el++)
      {
        if(tsys_send.freqs[i].elev[el])
        {
          fprintf(fp, "%d %d %d\n", tsys_send.freqs[i].freq, el, tsys_send.freqs[i].elev[el]);
        }
      }
      j++;
    }
  }

  fclose(fp);

  if(j < 2)
  {
    st_fail("No Tsys data to plot3d");
    return(1);
  }

  /* Now write out the dem file */
  sprintf(fname, "/tmp/%s/tsys.dem", session);

  if((fp = fopen(fname, "w")) == NULL)
  {
    st_fail("Unable to open %s file", fname);

    return(0);
  }

  fprintf(fp, " \n");
  fprintf(fp, "set title \"Tsys vs. Freq\"\n");
  fprintf(fp, "set xlabel \"Freq\" \n");
  fprintf(fp, "set ylabel \"Elevation\" \n");
  fprintf(fp, "set zlabel \"Tsys\" \n");
  fprintf(fp, "#\n");
  fprintf(fp, "set key off\n");
  fprintf(fp, "set border 4095\n");
//  fprintf(fp, "set hidden3d\n");
  fprintf(fp, "# \n");
  fprintf(fp, "set xrange [%d:%d]\n", fm, fx);
  fprintf(fp, "set yrange [%f:%f]\n", elmin, elmax);
  fprintf(fp, "set zrange [0:]\n");
  fprintf(fp, "set xyplane at 0\n");
  fprintf(fp, "# \n");
  fprintf(fp, "set samples %d*%d \n", fx-fm, (int)round(elmax-elmin));
//  fprintf(fp, "set isosamples 60 \n");
//  fprintf(fp, "# \n");
  fprintf(fp, "# \n");
//  fprintf(fp, "set palette model RGB\n");
//  fprintf(fp, "set palette color\n");
//  fprintf(fp, "set palette defined ( 0 \"green\", 1 \"blue\", 2 \"red\", 3 \"orange\" )\n");
//  fprintf(fp, "set palette gamma 1.0\n");
//  fprintf(fp, "#\n");
  fprintf(fp, "set terminal x11 enhanced size 690, 733 position 2338, 0\n");
  fprintf(fp, "splot \"/tmp/%s/tsys.dat\" every 1::0::1024 with lines\n", session);
  fprintf(fp, "pause -1 \"Hit Return When Finished\"\n");

  fclose(fp);

  sprintf(sysBuf, "gnuplot %s", fname);

  system(sysBuf);

  return(0);
}


/* Prepare data for plotting */
int doTsysPrepare()
{
  int i, el;

  bzero((char *)&tsys_send, sizeof(tsys_send));

  strcpy(tsys_send.magic, "TSYS_SEND");

  tsys_send.plotwin = env->plotwin;
  tsys_send.xdelay  = env->xdelay;

  for(i=0;i<MAX_TSYS_BINS;i++)
  {
    if(tsys_freqs[i].used)
    {
      tsys_send.freqs[tsys_send.num].freq = tsys_freqs[i].freq;
      tsys_send.freqs[tsys_send.num].used = 1;

      for(el=elmin;el<elmax;el++)
      {
        if(tsys_freqs[i].tsys[el])
        {
          tsys_send.freqs[tsys_send.num].elev[el] = tsys_freqs[i].tsys[el];
        }
      }

      tsys_send.num++;
    }
  }

  if(!tsys_send.num)
  {
    st_fail("No Tsys data to Plot");
    return(1);
  }

  return(0);
}


int doTsysPlot()
{
  if(notSet(PF_A_FLAG))
  {
    doTsysGUI(&tsys_send);
  }

  sock_write(xgraphic, (char *)&tsys_send, sizeof(tsys_send));
  st_report("Sent TSYS struct to xgraphics");

  return(0);
}


int doTsysList()
{
  int i, el, total;

  if(!tsys_send.num)
  {
    st_fail("No Tsys data to List");
    return(1);
  }

  st_report("Freq   Used   Total");

  for(i=0;i<tsys_send.num;i++)
  {
    total = 0;
    for(el=elmin;el<elmax;el++)
    {
      if(tsys_send.freqs[i].elev[el] > 0)
      {
	total++;
      }
    }
    st_report(" %3d   %3d    %3d", tsys_send.freqs[i].freq, tsys_send.freqs[i].used, total);
  }

  return(0);
}


int doTsys()
{
  int len, nt;
  char tbuf[256], result[256];

  nt = args->ntokens;

  /* Short cuts */

  if(isSet(PF_I_FLAG))
  {
    doTsysIngest();
  }

  if(isSet(PF_P_FLAG))
  {
    doTsysPlot();
  }

  if(isSet(PF_3_FLAG))
  {
    doTsys3D();
  }

  if(isSet(PF_S_FLAG))
  {
    if(!doTsysGUI(&tsys_send))
    {
      sock_write(xgraphic, (char *)&tsys_send, sizeof(tsys_send));
    }
  }

  if(isSet(PF_L_FLAG))
  {
    doTsysList();
  }

  if(nt > 1) /* If using the short cuts, nt should = 0 */
  {
    len = strlen(args->secondArg);

    if(!strncmp(args->secondArg, "elmin", len))
    {
      if(nt > 2)
      {
        elmin = atof(args->thirdArg);
      }
      else			/* Pop up a slider window to enter */
      {
        sprintf(tbuf, "--scale --min-value=10 --max-value=90 --text='Select Minimum Elevation' --value=10");

        doZenity(tbuf, result);

        if(strlen(result) > 1)
        {
          st_report("Got %s for 'tsys elmin'", result);
          elmin = atof(result);
        }
      }
    }
    else
    if(!strncmp(args->secondArg, "elmax", len))
    {
      if(nt > 2)
      {
        elmax = atof(args->thirdArg);
      }
      else
      {
        st_fail("Missing arg for 'tsys elmax'");
      }
    }
    else
    if(!strncmp(args->secondArg, "fmin", len))
    {
      if(nt > 2)
      {
        Fmin = atof(args->thirdArg);
      }
      else
      {
        st_fail("Missing arg for 'tsys fmin'");
      }
    }
    else
    if(!strncmp(args->secondArg, "fmax", len))
    {
      if(nt > 2)
      {
        Fmax = atof(args->thirdArg);
      }
      else
      {
        st_fail("Missing arg for 'tsys fmax'");
      }
    }
    else
    if(!strncmp(args->secondArg, "tmax", len))
    {
      if(nt > 2)
      {
        tmax = atof(args->thirdArg);
      }
      else
      {
        st_fail("Missing arg for 'tsys tmax'");
      }
    }
    else
    if(!strncmp(args->secondArg, "query", len))
    {
      st_report("TSYS: El Min:   %9.0f", elmin);
      st_report("TSYS: El Max:   %9.0f", elmax);
      st_report("TSYS: Freq Min: %9.0f", Fmin);
      st_report("TSYS: Freq Max: %9.0f", Fmax);
      st_report("TSYS: Tsys Max: %9.0f", tmax);
      st_report("TSYS: Num Freqs:%9d",   tsys_num);
    }
  }

  return(0);
}
