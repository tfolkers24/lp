/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"


int surveyWrite(fname)
char *fname;
{
  int i, dlen;
  FILE *fp;
  struct HEADER *h;
  double f0, f1, f2, fr, refpt, deltax, vel;
  float  *f3;
  struct LP_IMAGE_INFO *image = h0;

  h = &image->head;

  f0     = h->restfreq;
  fr     = h->freqres;
  refpt  = h->refpt;
  deltax = h->deltax;
  vel    = h->velocity;

  dlen = h->noint;

  if((fp = fopen(fname, "w")) == NULL)
  {
    st_fail("surveyWrite(): Unable to open file %s", fname);
    return(0);
  }

  fprintf(fp, "# %s ns %d\n",  basename(env->filename), (int)h->align3[0]);
  fprintf(fp, "# FREQ %.3f\n", f0);
  fprintf(fp, "# TSYS %.2f\n", h->stsys);
  fprintf(fp, "# TIME %.2f\n", h->inttime);
  fprintf(fp, "# MHz, K, s\n");
  fprintf(fp, "# RMS  %.2f\n", env->rms);
  fprintf(fp, "# %16.16s\n",   h->object);
  fprintf(fp, "# \n");

  if(h->sideband == 2)
  {
    f3 = &image->yplot1[0];
    for(i=0;i<dlen;i++,f3++)
    {
      f1 = f0  - ((refpt - (double)(i+1)) * fr);
      f2 = vel - ((refpt - (double)(i+1)) * deltax);

      if(fabs(*f3) < 0.5)
      {
        fprintf(fp, "    %8.1f   %10.4f   %12.9E\n", f1, f2, *f3);
      }
      else
      {
        fprintf(fp, "    %8.1f   %10.4f   %9.6f\n", f1, f2, *f3);
      }
    }
  }
  else
  {
    f3 = &image->yplot1[dlen-1];
    for(i=dlen-1;i>-1;i--,f3--)
    {
      f1 = f0  - ((refpt - (double)(i+1)) * fr);
      f2 = vel - ((refpt - (double)(i+1)) * deltax);

      if(fabs(*f3) < 0.5)
      {
        fprintf(fp, "    %8.1f   %10.4f   %12.9E\n", f1, f2, *f3);
      }
      else
      {
        fprintf(fp, "    %8.1f   %10.4f   %9.6f\n", f1, f2, *f3);
      }
    }
  }

  fclose(fp);

  st_report("Wrote Survey File %s", fname);

  return(0);
}



int doSurvey()
{
  if(strlen(args->secondArg) > 2)
  {
    surveyWrite(args->secondArg);
  }
  else
  {
    st_fail("Usage: survey filename");
    return(1);
  }

  return(0);
}
