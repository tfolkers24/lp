/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include <gsl/gsl_histogram.h>

#include "extern.h"

struct HISTO_INFO last_histo;

int histo_failed = 0;

int dummie_main (int argc, char **argv)
{
  double a, b;
  size_t n;

  if(argc != 4)
  {
    printf ("Usage: gsl-histogram xmin xmax n\n"
    "Computes a histogram of the data "
    "on stdin using n bins from xmin "
    "to xmax\n");

    exit (0);
  }

  a = atof(argv[1]);
  b = atof(argv[2]);
  n = atoi(argv[3]);

  {
    double x;

    gsl_histogram * h = gsl_histogram_alloc (n);
    gsl_histogram_set_ranges_uniform (h, a, b);

    while (fscanf (stdin, "%lg", &x) == 1)
    {
      gsl_histogram_increment (h, x);
    }

    gsl_histogram_fprintf (stdout, h, "%g", "%g");
    gsl_histogram_free (h);
  }

  exit (0);
}


int computeHistoRMS(h)
struct HISTO_INFO *h;
{
  int i, k, n;
  float f[MAX_HISTO_LENGTH], rms;
  double total=0.0;

  n = h->num;

  for(i=0;i<n;i++)
  {
    f[i] = h->ydata[i] - h->gdata[i];
    total += h->gdata[i];
  }

  if(isSet(PF_V_FLAG))
  {
    k = 0;
    for(i=0;i<n;i++,k++)
    {
      if(k > 9)
      {
        st_print("\n");
        k = 0;
      }

      st_print("%9.4f ", f[i]);
    }

    st_print("\n");
  }

  computeRms(f, n, &rms);

  st_report("Histogram rms:           %11.6f", rms);
  st_report("Histogram moment:        %11.6f", total);
  st_report("Histogram Percent:       %11.6f", rms / total * 100.0);

  return(0);
}


void histo_handler (const char * reason, const char * file, int line, int gsl_errno)
{
  st_fail("histogram(): Failed: %s", reason);

  histo_failed = 1;

  return;
}


int doHistogram()
{
  int i, n;
  double min, max;
  struct HISTO_INFO *h;
  gsl_histogram *histo;
  gsl_error_handler_t *old_handler;

  if(args->ntokens < 2)
  {
    printH_FlagHelp("histogram");

    return(1);
  }

  n = atoi(args->secondArg);

  if( n <= 10 || n >= MAX_HISTO_LENGTH)
  {
    printH_FlagHelp("histogram");

    return(1);
  }

  min = h0->datamin /**1.5 */;
  max = h0->datamax /**1.5 */;

  if(!env->baselineremoved)
  {
    st_fail("Please remove a DC offset first");
    return(0);
  }

  histo_failed = 0;
  old_handler = gsl_set_error_handler(&histo_handler);

  histo = gsl_histogram_alloc(n);

  if(histo_failed)
  {
    st_fail("Histogram Allocation Failed");

    gsl_set_error_handler(old_handler);

    return(1);
  }

  gsl_histogram_set_ranges_uniform(histo, min, max);

  if(histo_failed)
  {
    st_fail("Histogram Set Ranges Failed");

    gsl_set_error_handler(old_handler);

    return(1);
  }

  for(i=0;i<h0->head.noint;i++)
  {
    gsl_histogram_increment(histo, (double)h0->yplot1[i]);

    if(histo_failed)
    {
      st_fail("Histogram Increment Failed");

      gsl_set_error_handler(old_handler);

      return(1);
    }
  }

  if(isSet(PF_W_FLAG))
  {
    char fname[256];
    FILE *fp;

    sprintf(fname, "histo.dat");

    if((fp = fopen(fname, "w") ) <= (FILE *)0)
    {
      st_fail("Unable to open %s", fname);

      return(1);
    }

    gsl_histogram_fprintf(fp, histo, "%g", "%g");

    fclose(fp);

    if(histo_failed)
    {
      st_fail("Histogram fprintf Failed");

      gsl_set_error_handler(old_handler);

      return(1);
    }


    st_report("Histogram Written to %s", fname);
  }

  h = &last_histo;

  bzero((char *)h, sizeof(*h));

  strcpy(h->magic, "HISTOGRAM");

  bcopy((char *)&h0->head, (char *)&h->head, sizeof(struct HEADER));
  h->plotwin = env->plotwin;

  for(i=0;i<n;i++)
  {
    h->xdata[i] = histo->range[i];
    h->ydata[i] = histo->bin[i];
  }

  h->num = n;

  st_report("Histogram Max:           %11.6f", gsl_histogram_max_val(histo));
  st_report("Histogram Min:           %11.6f", gsl_histogram_min_val(histo));
  st_report("Histogram Mean:          %11.6f", gsl_histogram_mean(histo));
  st_report("Histogram Sigma:         %11.6f", gsl_histogram_sigma(histo));
  st_report("Histogram Sum:           %11.6f", gsl_histogram_sum(histo));

  if(histo_failed)
  {
    st_fail("Histogram Fetch Failed");

    gsl_set_error_handler(old_handler);

    return(1);
  }

  if(isSet(PF_G_FLAG))
  {
    int    pgauss, h1gauss, h2gauss, ret;
    double xd[MAX_HISTO_LENGTH], yd[MAX_HISTO_LENGTH], sig[MAX_HISTO_LENGTH];
    double a[4], dyda[4], y;

    pgauss = gsl_histogram_max_bin(histo);

    if(histo_failed)
    {
      st_fail("Histogram Fetch max_bin Failed");

      gsl_set_error_handler(old_handler);

      return(1);
    }

    h1gauss = pgauss - (n/8);
    h2gauss = pgauss + (n/8);

    for(i=0;i<n;i++)
    {
       xd[i] = h->xdata[i];
       yd[i] = h->ydata[i];
      sig[i] = 1.0;
    }

    a[0] = 0.0;
    a[1] = yd[pgauss];
    a[2] = xd[pgauss];
    a[3] = fabs(xd[h1gauss]-xd[h2gauss]);

    st_report("Histogram Gauss Input:   %11.6f %11.6f %11.6f", a[1], a[2], a[3]);

    /* computer gaussian using Temp / Channel */
    ret = gauss_fit(&xd[0], &yd[0], &sig[0], n-1, &a[0], isSet(PF_V_FLAG));

    if(ret)
    {
      st_report("Histogram Gauss Results: %11.6f %11.6f %11.6f", a[1], a[2], a[3]);

      for(i=0;i<n;i++)
      {
        fgauss(xd[i], &a[0], &y, &dyda[0]);
        h->gdata[i]  = y;
      }

      h->gset = 1;

      computeHistoRMS(h);
    }
  }

  if(xgraphic)
  {
    sock_write(xgraphic, (char *)h, sizeof(*h));
  }

  last_plot = LAST_PLOT_SPEC_HISTO;

  st_report("Sent Histogram to Xgraphics");

  gsl_histogram_free(histo);

  if(histo_failed)
  {
    st_fail("Histogram Free Failed");

    gsl_set_error_handler(old_handler);

    return(1);
  }

 gsl_set_error_handler(old_handler);

  return(0);
}
