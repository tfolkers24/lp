/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#define INCLUDE_PARSE_FLAGS 1

#define DEFINE_ANSI_COLORS_STRINGS 1

#include "extern.h"

#define DVAR  1
#define GSVAR 2

#define LP_IF_TEST_FALSE  0
#define LP_IF_TEST_TRUE   1
#define LP_IF_TEST_NTRUE  2

#define LP_IF_STATE_IDLE  0
#define LP_IF_STATE_TRUE  1
#define LP_IF_STATE_FALSE 2
#define LP_IF_STATE_ELSE  3

int    lp_if_test_rst   =  LP_IF_TEST_FALSE;
int    lp_if_test_state =  LP_IF_STATE_IDLE;

char   lp_if_else_buf[512];

int    oldColor       =  ANSI_BLACK;
int    recording      =  0;
int    old_quiet      =  0;
int    recursiveLevel =  0;

int    ivars_holders[MAX_DYN_VARS];
double dvars_holders[MAX_DYN_VARS];
struct DYNAMIC_VARIABLES dvars[MAX_DYN_VARS];

char   myline[4096];
char   recordingFname[256];

extern int getSizeOfGS();
extern struct GSKEY getset[];


#define VARIABLE_TYPE_NONE   0
#define VARIABLE_TYPE_DVAR   1
#define VARIABLE_TYPE_GSVAR  2
#define VARIABLE_TYPE_HEADER 3
#define VARIABLE_TYPE_PLANET 4


char last_history[4096];

struct PARSE_STACK parse_stack[MAX_PARSE_STACK];
struct PARSE_STACK *args;


/* like add_history() on;y ignores repeated commands and 'history' */
int my_add_history(buf)
char *buf;
{
  static int first = 1;

  if(first)
  {
    strcpy(last_history, "blah blah");
    first = 0;
  }

  if(!strcmp(buf, last_history) || !strncmp(buf, "history", 7))
  {
//    st_report("Ignoring repeat history");
    return(1);
  }

  add_history(buf);

  strcpy(last_history, buf);
  

  return(0);
}


int setDFormat(buf)
char *buf;
{
  if(isSet(PF_0_FLAG))
  {
    strcpy(buf, "%.0f");
  }
  else
  if(isSet(PF_1_FLAG))
  {
    strcpy(buf, "%.1f");
  }
  else
  if(isSet(PF_2_FLAG))
  {
    strcpy(buf, "%.2f");
  }
  else
  if(isSet(PF_3_FLAG))
  {
    strcpy(buf, "%.3f");
  }
  else
  if(isSet(PF_4_FLAG))
  {
    strcpy(buf, "%.4f");
  }
  else
  if(isSet(PF_5_FLAG))
  {
    strcpy(buf, "%.5f");
  }
  else
  if(isSet(PF_6_FLAG))
  {
    strcpy(buf, "%.6f");
  }
  else
  if(isSet(PF_7_FLAG))
  {
    strcpy(buf, "%.7f");
  }
  else
  if(isSet(PF_8_FLAG))
  {
    strcpy(buf, "%.8f");
  }
  else
  if(isSet(PF_9_FLAG))
  {
    strcpy(buf, "%.9f");
  }
  else
  {
    strcpy(buf, "%.16lf");
  }

  return(0);
}


int getNumericFlag()
{
  int i, f = -1;

  for(i=PF_0_FLAG; i<(PF_24_FLAG+1);i++)
  {
    if(isSet(i))
    {
      f = i-PF_0_FLAG;

      return(f);
    }
  }

  return(f);
}

/* returns the first /w[n] flag */
int getWindowFlag()
{
  int i, f = -1;

  for(i=PF_W0_FLAG; i<(PF_W0_FLAG+MAX_WINDOWS+1);i++)
  {
    if(isSet(i))
    {
      f = i-PF_W0_FLAG;

      return(f);
    }
  }

  return(f);
}


/* return the first [/0-/24] flag found */
int getNset()
{
  int i;
  int ret = -1;
  struct PARSE_FLAGS *p;

  p = &args->pflags[PF_0_FLAG];
  for(i=PF_0_FLAG;i<PF_24_FLAG;i++,p++)
  {
    if(p->set)
    {
      ret = i-PF_0_FLAG;

      break;
    }
  }

  return(ret);
}


/* Keep looking until you find a variable; if it's defined that is */
/* If found, populate the gv struct */
int findAnyVariable(name, qv)
char *name;
struct QUERY_VAR *qv;
{
  int r;
  char dfmt[80];
  struct HEAD_PARAMS *p;

  setDFormat(dfmt);

  bzero((char *)qv, sizeof(struct QUERY_VAR));

  strcpy(qv->name, name);

  /* Look for dynamic variables first */
  r = findVariable(name);
  if(r >= 0)
  {
    if(dvars[r].type == DYN_TYPE_DOUBLE)
    {
      sprintf(qv->buf, dfmt, *dvars[r].dval);
      qv->dval = *dvars[r].dval;
      qv->ntype = DOUBLE;
    }
    else
    {
      st_print(qv->buf, "%d", *dvars[r].ival);
      qv->ival = *dvars[r].ival;
      qv->ntype = INTEGER;
    }

    qv->stype = VARIABLE_TYPE_DVAR;
    qv->indx  = r;

    if(verboseLevel & VERBOSE_PARSE)
    {
      st_print("%s(%s:%d): DVAR: %s = %s\n", __func__, __FILE__, __LINE__, name, qv->buf);
    }

    return(1);
  }

  /* Look for environmental variables next */
  r = findGSVar(name);
  if(r >= 0)
  {
    if(getset[r].type == INTEGER)
    {
      sprintf(qv->buf, "%d", *getset[r].iaddr);
      qv->ival = *getset[r].iaddr;
    }
    else
    if(getset[r].type == DOUBLE)
    {
      sprintf(qv->buf, dfmt, *getset[r].daddr);
      qv->dval = *getset[r].daddr;
    }
    else
    if(getset[r].type == CHARSTAR)
    {
      sprintf(qv->buf, "%s", getset[r].caddr);
      strcpy(qv->cval, getset[r].caddr);
    }

    qv->ntype = getset[r].type;
    qv->stype = VARIABLE_TYPE_GSVAR;
    qv->indx  = r;

    if(verboseLevel & VERBOSE_PARSE)
    {
      st_print("%s(%s:%d): GSVAR: %s = %s\n", __func__, __FILE__, __LINE__, name, qv->buf);
    }

    return(1);
  }

  /* Is it a header variable */
  p = findHeaderVal(name);

  if(p)
  {
    switch(p->type)
    {
      case DOUBLE:
        sprintf(qv->buf, dfmt, *p->dptr);
        qv->dval = *p->dptr;
        break;

      case CHARSTAR:
        strncpy(qv->buf, p->cptr, p->size);
        qv->buf[p->size] = '\0';
        strcpy(qv->cval, qv->buf);

	break;
    }

    qv->ntype = p->type;
    qv->stype = VARIABLE_TYPE_HEADER;
    bcopy((char *)p, (char *)&qv->phead, sizeof(struct HEAD_PARAMS));

    if(verboseLevel & VERBOSE_PARSE)
    {
      st_print("%s(%s:%d): HEADER: %s = %s\n", __func__, __FILE__, __LINE__, name, qv->buf);
    }

    return(1);
  }

  strcpy(qv->buf, name);  /* Just return the token into buf */

  return(0);
}



int initParseArgs()
{
  int i;
  struct PARSE_STACK *p;

  p = &parse_stack[0];
  for(i=0;i<MAX_PARSE_STACK;i++,p++)
  {
    p->firstArg   = p->tok[0];
    p->secondArg  = p->tok[1];
    p->thirdArg   = p->tok[2];
    p->fourthArg  = p->tok[3];
    p->fifthArg   = p->tok[4];
    p->sixthArg   = p->tok[5];
    p->seventhArg = p->tok[6];
    p->eighthArg  = p->tok[7];
    p->ninethArg  = p->tok[8];
    p->tenthArg   = p->tok[9];

    bzero((char *)p->pflags, sizeof(p->pflags));

    /* Copy only 'defined' parse_flags */
    bcopy((char *)parse_flags, (char *)p->pflags, sizeof(parse_flags));
  }

  args = &parse_stack[0];

  return(0);
}


int pushArgs()
{
  recursiveLevel++;

  if(recursiveLevel >= MAX_PARSE_STACK)
  {
    st_fail("recursiveLevel > MAX_PARSE_STACK");

    recursiveLevel--;
  }

  args = &parse_stack[recursiveLevel];

  if(verboseLevel & VERBOSE_PARSE)
  {
    st_report("pushArgs(): recurse level %d", recursiveLevel);
  }

  return(0);
}


int popArgs()
{
  recursiveLevel--;

  if(recursiveLevel < 0)
  {
    st_fail("recursiveLevel < 0");

    recursiveLevel = 0;
  }

  args = &parse_stack[recursiveLevel];

  if(verboseLevel & VERBOSE_PARSE)
  {
    st_report("popArgs(): recurse level %d", recursiveLevel);
  }

  return(0);
}


/* Substitute passed string into args->tok[wt-1] */
/* Some assembly required */

void subArgs(int wt, const char *fmt, ...)
{
  char tbuf[4096];
  va_list targs;

  if(verboseLevel & VERBOSE_PARSE)
  {
    st_report("Entering subArgs()");
  }

// Create the string first
  va_start(targs, fmt);
  vsprintf(tbuf, fmt, targs);
  va_end(targs);

  if(strlen(tbuf) < MAX_TOKENS && (wt-1) >= 0 && (wt-1) < MAX_TOKENS)
  {
    if(verboseLevel & VERBOSE_PARSE)
    {
      st_report("Setting args->tok[%d] to %s", wt-1, tbuf);
    }

    strcpy(args->tok[wt-1], tbuf);
    strcpy(args->currentCmd, args->tok[wt-1]);  /* Hope this doesn't screw up something else */
  }
}


/* I should compress the token package here; remove the
     empty args->tok[]'s and decriment the args->ntokens 
   Take tokens and reassemble then and re-tokenize them */
int compressReassembleTokens()
{
  int i;
  char tbuf[512], scratch[512];

  tbuf[0] = '\0';
  for(i=0;i<args->ntokens;i++)
  {
    sprintf(scratch, "%s ", args->tok[i]);
    strcat(tbuf, scratch);
  }

  /* Re-tokenize them again */
  args->ntokens = get_tok(tbuf);

  return(0);
}


int reassembleCommand(buf)
char *buf;
{
  int i;
  char scratch[512];

  buf[0] = '\0';
  for(i=0;i<args->ntokens;i++)
  {
    sprintf(scratch, "%s ", args->tok[i]);
    strcat(buf, scratch);
  }

  return(0);
}


int parseFlags()
{
  int i, k, found = 1;
  struct PARSE_FLAGS *p;

  if(verboseLevel & VERBOSE_PARSE)
  {
    st_report("parseFlags(): Entering with %d tokens", args->ntokens);
  }

  for(i=0;i<args->ntokens;i++)
  {
    p = &args->pflags[0];
    for(k=0;k<MAX_PARSE_FLAGS;k++,p++)
    {
      if(!strstr(args->tok[i], "/") && !strstr(args->tok[i], "--")) /* It's not a flag if there is no '/' or '-' char */
      {
        continue;
      }

      if(verboseLevel & VERBOSE_PARSE)
      {
        st_report("parseFlags(): Comparing %s to %s", args->tok[i], p->token);
      }

      if(!strcmp(args->tok[i], p->token))
      {
        if(verboseLevel & VERBOSE_PARSE)
        {
          st_report("parseFlags(): Flag: %s Set", p->token);
        }

        p->set++;

	if(!p->order)			/* The order I found them */
	{
	  p->order = found;

          found++;
	}

        if(p->index == PF_Q_FLAG)
        {
          if(verboseLevel & VERBOSE_PARSE)
          {
            st_report("parseFlags(): Setting Quiet to True");
          }

          old_quiet = stquiet;
          stquiet   = 1;
        }

	/* Clear out the parsed flags */
	strcpy(args->tok[i], "  ");

        break;
      }
    }
  }

  compressReassembleTokens();

  if(verboseLevel & VERBOSE_PARSE)
  {
    st_report("parseFlags(): We now have %d tokens", args->ntokens);
  }

  if(verboseLevel & VERBOSE_PARSE)
  {
    p = &args->pflags[0];
    for(k=0;k<MAX_PARSE_FLAGS;k++,p++)
    {
      if(p->set)
      {
        st_print("%16.16s is Set\n", p->name);
      }
    }

    st_report("parseFlags(): Leaving");
  }

  return(0);
}



int clearFlags()
{
  int i;
  struct PARSE_FLAGS *p;

  p = &args->pflags[0];
  for(i=0;i<MAX_PARSE_FLAGS;i++,p++)
  {
    p->set   = 0;
    p->order = 0;
  }

  stquiet   = old_quiet;

  return(0);
}


/* Return the first flag set; else NULL */
struct PARSE_FLAGS *findFirstFlag()
{
  int i;
  struct PARSE_FLAGS *p;

  p = &args->pflags[0];
  for(i=0;i<MAX_PARSE_FLAGS;i++,p++)
  {
    if(p->order == 1)
    {
      return(p);
    }
  }

  return((struct PARSE_FLAGS *)NULL);
}


/* Shift all 'set' flags down one place in their 'order' */
/* Decriment 'set' count for the previous first flag */
int shiftFlags()
{
  int i;
  struct PARSE_FLAGS *p;

  p = &args->pflags[0];
  for(i=0;i<MAX_PARSE_FLAGS;i++,p++)
  {
    if(p->order)
    {
      if(p->order == 1)
      {
        p->set--;
      }

      p->order--;
    }
  }

  return(0);
}


  /* Is the specified flag set ?? */
int isSet(flag)
int flag;
{
  if(verboseLevel & VERBOSE_FLAGS)
  {
    if(flag <= PF_Z_FLAG)
    {
      st_report("Checking if /%c flag set", (char)(flag+97));
    }
  }

  return(args->pflags[flag].set);
}


  /* Is the specified flag the first flag ?? */
int isFirst(flag)
int flag;
{
  return(args->pflags[flag].order == 1);
}

  /* Is the specified flag NOT set ?? */
int notSet(flag)
int flag;
{
  if(verboseLevel & VERBOSE_FLAGS)
  {
    if(flag <= PF_Z_FLAG)
    {
      st_report("Checking if /%c flag NOT set", (char)(flag+97));
    }
  }

  return(!args->pflags[flag].set);
}


  /* Set the specified flag */
int setFlag(flag, state)
int flag, state;
{
  if(flag <= PF_Z_FLAG)
  {
    args->pflags[flag].set = state;
  }
  else
  {
    return(1);
  }

  return(0);
}


int showLetHelp()
{
  int i, ngs;
  struct GSKEY *pgs;
  char hbuf[256];

  ngs = getSizeOfGS();

  pgs = &getset[0];
  for(i=0;i<ngs;i++,pgs++)
  {
    if(!fetchNGSHelp(pgs->name, hbuf))
    {
      st_print("%18.18s: %s \n", pgs->name, hbuf);
    }
    else
    {
      st_print("%18.18s: \n", pgs->name);
    }
  }

  st_print("\n");

  return(0);
}


int listDynVars()
{
  int i, j, ngs, key=0;
  struct DYNAMIC_VARIABLES *p;
  struct GSKEY *pgs;
  char hbuf[256], tbuf[512], fbuf[256], kbuf[256];

  if(isSet(PF_K_FLAG))
  {
    if(strlen(args->secondArg))
    {
      key = 1;
      strcpy(kbuf, args->secondArg);
    }
    else
    {
      printH_FlagHelp("dlist");
      return(0);
    }
  }

  st_print("Defined Dynamic Variables\n");

  p = &dvars[0];
  for(i=0;i<MAX_DYN_VARS;i++,p++)
  {
    if(p->type == DYN_TYPE_INT)
    {
      if(key)
      {
        if(strcasestr(p->name, kbuf))
        {
          st_print("D: %18.18s:      (int)        %10d\n", p->name, *p->ival);
        }
      }
      else
      {
        st_print("D: %18.18s:      (int)        %10d\n", p->name, *p->ival);
      }
    }
    else
    if(p->type == DYN_TYPE_DOUBLE)
    {
      if(fabs(*p->dval) < 0.0001 || fabs(*p->dval) > 9999999.9)
      {
        if(key)
        {
          if(strcasestr(p->name, kbuf))
          {
            st_print("D: %18.18s:      (double) %14.4e\n", p->name, *p->dval);
          }
        }
        else
        {
          st_print("D: %18.18s:      (double) %14.4e\n", p->name, *p->dval);
        }
      }
      else
      {
        if(key)
        {
          if(strcasestr(p->name, kbuf))
          {
            st_print("D: %18.18s:      (double) %14.6f\n", p->name, *p->dval);
          }
        }
        else
        {
          st_print("D: %18.18s:      (double) %14.6f\n", p->name, *p->dval);
        }
      }
    }
  }

  ngs = getSizeOfGS();

  pgs = &getset[0];
  for(i=0;i<ngs;i++,pgs++)
  {
    fetchNGSHelp(pgs->name, hbuf);

    switch(pgs->type)
    {
      case INTEGER:
	if(pgs->size > 1)
        {
	  for(j=0;j<pgs->size;j++)
          {
	    if(!strncmp(pgs->name, "badchans", 8))
            {
              if(pgs->iaddr[j] == -1)
              {
                continue;
              }

              if(pgs->iaddr[j] != 0 || j == 0)
	      {
                if(key)
                {
                  if(strcasestr(pgs->name, kbuf))
                  {
                    st_print("E: %18.18s[%03d]: (int)        %10d\n", pgs->name, j, pgs->iaddr[j]);
                  }
                }
                else
                {
                  st_print("E: %18.18s[%03d]: (int)        %10d\n", pgs->name, j, pgs->iaddr[j]);
                }
              }
            }
          }
        }
        else
	{
          if(key)
          {
            if(strcasestr(pgs->name, kbuf))
            {
              st_print("E: %18.18s:      (int)        %10d     %s\n", pgs->name, *pgs->iaddr, hbuf);
            }
          }
          else
          {
            st_print("E: %18.18s:      (int)        %10d     %s\n", pgs->name, *pgs->iaddr, hbuf);
          }
	}
        break;

      case DOUBLE:
	if(pgs->size > 1)
        {
	  for(j=0;j<pgs->size;j++)
          {
	    if(!strncmp(pgs->name, "ignoredScans", 12))
            {
              if(pgs->daddr[j] != 0.0 || j == 0)
	      {
                if(key)
                {
                  if(strcasestr(pgs->name, kbuf))
                  {
                    st_print("E: %18.18s[%03d]: (double) %14.2f\n", pgs->name, j, pgs->daddr[j]);
		  }
		}
                else
                {
                  st_print("E: %18.18s[%03d]: (double) %14.2f\n", pgs->name, j, pgs->daddr[j]);
                }
              }
            }
          }
        }
        else
	{
          if(fabs(*pgs->daddr) < 0.0001 || fabs(*pgs->daddr) > 9999999.9)
	  {
            if(key)
            {
              if(strcasestr(pgs->name, kbuf))
              {
                st_print("E: %18.18s:      (double) %14.4e     %s\n", pgs->name, *pgs->daddr, hbuf);
              }
            }
            else
            {
              st_print("E: %18.18s:      (double) %14.4e     %s\n", pgs->name, *pgs->daddr, hbuf);
            }
	  }
	  else
	  {
            if(key)
            {
              if(strcasestr(pgs->name, kbuf))
              {
                st_print("E: %18.18s:      (double) %14.4f     %s\n", pgs->name, *pgs->daddr, hbuf);
              }
            }
            else
            {
              st_print("E: %18.18s:      (double) %14.4f     %s\n", pgs->name, *pgs->daddr, hbuf);
            }
	  }

	}
        break;

      case CHARSTAR:
	if(!strcmp(pgs->name, "filename")  || 
           !strcmp(pgs->name, "gfilename") || 
           !strcmp(pgs->name, "savefile")  || 
           !strcmp(pgs->name, "userfile"))
        {
	  strcpy(tbuf, pgs->caddr);
	  strcpy(fbuf, basename(tbuf));
          strcpy(tbuf, fbuf);
        }
        else
        {
	  strcpy(tbuf, pgs->caddr);
        }
        
        if(key)
        {
          if(strcasestr(pgs->name, kbuf))
          {
            st_print("E: %18.18s       (char) %20s %s\n", pgs->name, tbuf, hbuf);
          }
        }
        else
        {
          st_print("E: %18.18s       (char) %20s %s\n", pgs->name, tbuf, hbuf);
        }
        break;
    }
  }

  return(0);
}


int findGSVar(name)
char *name;
{
  int i, ngs;

  ngs = getSizeOfGS();

  for(i=0;i<ngs;i++)
  {
    if(!strcmp(getset[i].name, name))
    {
      return(i);
    }
  }

  return(-1);
}


int initDvars()
{
  int i;
  struct DYNAMIC_VARIABLES *p;

  p = &dvars[0];
  for(i=0;i<MAX_DYN_VARS;i++,p++)
  {
    p->dval = &dvars_holders[i];
    p->ival = &ivars_holders[i];
  }

  return(0);
}



int findVariable(var)
char *var;
{
  int i;

  for(i=0;i<MAX_DYN_VARS;i++)
  {
    if(!strcmp(dvars[i].name, var))
    {
      return(i);
    }
  }

  return(-1);
}


int clearVar()
{
  int i;

  if(strlen(args->secondArg))
  {
    for(i=CGM->ndvars_used;i<MAX_DYN_VARS;i++)
    {
      if(!strcasecmp(dvars[i].name, args->secondArg))
      {
        st_report("Clearing Variable: %s", dvars[i].name);

        strcpy(dvars[i].name, "");
        dvars[i].type = 0;

        return(0);
      }
    }

    st_fail("Variable %s either not found or protected", args->secondArg);
  }

  return(1);
}


int clearAllVar()
{
  int i;

  for(i=CGM->ndvars_used;i<MAX_DYN_VARS;i++)
  {
    if(dvars[i].type)
    {
      st_report("Clearing Variable: %s", dvars[i].name);

      strcpy(dvars[i].name, "");
      dvars[i].type = 0;

      return(0);
    }
  }

  return(1);
}


/* Not previously defined; make a new one */
int makeDynVariable(var, type)
char *var;
int type;
{
  int i;

  /* Check for already defined variable */
  for(i=CGM->ndvars_used;i<MAX_DYN_VARS;i++)
  {
    if(!strcmp(dvars[i].name, var))
    {
    //  st_warn("Variable %s, already defined", var);
      return(i);
    }
  }

/* Find the first unused slot */
  for(i=CGM->ndvars_used;i<MAX_DYN_VARS;i++)
  {
    if(!dvars[i].type)
    {
      strcpy(dvars[i].name, var);

      dvars[i].type = type;

      st_report("New Variable: %s", var);

      return(i);
    }
  }

  FAIL_INFO("Dynamic Variable slot all Used");

  return(-1);
}


int createInteger()
{
  if(strlen(args->secondArg))
  {
    makeDynVariable(args->secondArg, DYN_TYPE_INT);
  }
  else
  {
    FAIL_INFO("Usage: int integer_variable_name");
  }

  return(0);
}


int createDouble()
{
  if(strlen(args->secondArg))
  {
    makeDynVariable(args->secondArg, DYN_TYPE_DOUBLE);
  }
  else
  {
    FAIL_INFO("Usage: double double_variable_name");
  }

  return(0);
}



#define FORMAT_NORMAL 0
#define FORMAT_EXP    1
#define FORMAT_HEX    2

int printVar()
{
  int i, n, ret, format = FORMAT_NORMAL, saveqt;
  char *cp, tstr[256], pbuf[256], tbuf[1024], bigBuf[512];
  struct QUERY_VAR qv;
  FILE *fp;

  /* Check for flags */
  if(isSet(PF_X_FLAG) || isSet(PF_HEX_FLAG))
  {
    format = FORMAT_HEX;
  }

  if(isSet(PF_EXP_FLAG))
  {
    format = FORMAT_EXP;
  }

  if(strlen(args->secondArg))
  {
    if(!strcmp(args->secondArg, "header"))
    {
      printHeader(CLASS_ALL);
      return(0);
    }

    if(!strcmp(args->secondArg, "weather"))
    {
      printHeader(CLASS_WEATHER);
      return(0);
    }

    if(!strcmp(args->secondArg, "source"))
    {
      printHeader(CLASS_SOURCE);
      return(0);
    }

    if(!strcmp(args->secondArg, "telescope"))
    {
      printHeader(CLASS_TELESCOPE);
      return(0);
    }

    if(!strcmp(args->secondArg, "map"))
    {
      printHeader(CLASS_MAPPING);
      return(0);
    }

    if(!strcmp(args->secondArg, "status"))
    {
      printStatus();
      return(0);
    }

  /* Look for a batch file or procedure */
    ret = isBatch(args->secondArg);

    if(ret >= 0)
    {
      printBatch(args->secondArg);
      return(0);
    }

    /* If the planets have not been loaded yet, this call will
       produce a lot of verbose output; Possibly not what the
       user is expecting at this time;  Set the quiet flag */
    saveqt = stquiet;
    stquiet = 1;

    ret = isPlanetVar(args->secondArg);

    stquiet = saveqt;

    if(!ret)
    {
      printPlanetVar(args->secondArg);
      return(0);
    }

    n = args->ntokens;

    bzero(bigBuf, sizeof(bigBuf));

    for(i=1;i<n;i++)
    {
      strcpy(pbuf, args->tok[i]);

      ret = findAnyVariable(pbuf, &qv);

      if(ret)
      {
        if(isSet(PF_N_FLAG))
        {
	  tstr[0] = '\0';
        }
        else
        {
          switch(qv.stype)
          {
            case VARIABLE_TYPE_DVAR:
              sprintf(tstr, "DVar:   ");
	      break;

            case VARIABLE_TYPE_GSVAR:
              sprintf(tstr, "GSVar: ");
	      break;

            case VARIABLE_TYPE_HEADER:
              sprintf(tstr, "HEADER:");
	      break;

            default:
              sprintf(tstr, "NOTDEF:");
	      break;
          }
        }

        switch(qv.stype)
        {
          case VARIABLE_TYPE_DVAR:
          case VARIABLE_TYPE_GSVAR:
          case VARIABLE_TYPE_HEADER:

            if(qv.ntype == DOUBLE)
            {
              if(format == FORMAT_EXP)
              {
                if(isSet(PF_N_FLAG))
                {
                  sprintf(tbuf, "%e ", qv.dval);
		  strcat(bigBuf, tbuf);
                }
		else
		{
                  sprintf(tbuf, "%s %s = %e ", tstr, pbuf, qv.dval);
		  strcat(bigBuf, tbuf);
		}
              }
              else
              if(format == FORMAT_HEX)
              {
                if(isSet(PF_N_FLAG))
                {
                  sprintf(tbuf, "0x%x ", (int)qv.dval);
		  strcat(bigBuf, tbuf);
		}
		else
		{
                  sprintf(tbuf, "%s %s = 0x%x ", tstr, pbuf, (int)qv.dval);
		  strcat(bigBuf, tbuf);
		}
              }
              else
              {
                if(isSet(PF_N_FLAG))
                {
                  sprintf(tbuf, "%s ", qv.buf);
		  strcat(bigBuf, tbuf);
		}
		else
		{
                  sprintf(tbuf, "%s %s = %s ", tstr, pbuf, qv.buf);
		  strcat(bigBuf, tbuf);
		}
              }
            }
            else
            if(qv.ntype == INTEGER)
            {
              if(format == FORMAT_HEX)
              {
                if(isSet(PF_N_FLAG))
                {
                  sprintf(tbuf, "0x%x ", qv.ival);
		  strcat(bigBuf, tbuf);
		}
		else
		{
                  sprintf(tbuf, "%s %s = 0x%x ", tstr, pbuf, qv.ival);
		  strcat(bigBuf, tbuf);
		}
              }
              else
              {
                if(isSet(PF_N_FLAG))
                {
                  sprintf(tbuf, "%d ", qv.ival);
		  strcat(bigBuf, tbuf);
		}
		else
		{
                  sprintf(tbuf, "%s %s = %d ", tstr, pbuf, qv.ival);
		  strcat(bigBuf, tbuf);
		}
              }
            }
	    else
            {
              if(isSet(PF_N_FLAG))
              {
                sprintf(tbuf, "%s ", qv.buf);
		strcat(bigBuf, tbuf);
	      }
	      else
	      {
                sprintf(tbuf, "%s %s = %s ", tstr, pbuf, qv.buf);
		strcat(bigBuf, tbuf);
	      }
            }
	    break;

          default:
	    break;
        }
      }
      else
      {
        if(!strcmp(pbuf, "@Space"))
        {
	  strcat(bigBuf, " ");
	  continue;
        }

        if(!strcmp(pbuf, "@Comma"))
        {
	  strcat(bigBuf, ",");
	  continue;
        }
       
        if(!strcmp(pbuf, "@Quote"))
        {
	  strcat(bigBuf, "\" ");
	  continue;
        }
	
        if(!strcmp(pbuf, "@Tick"))
        {
	  strcat(bigBuf, "\'");
	  continue;
        }

        sprintf(tbuf, "%s ", pbuf); /* Just print it */
	strcat(bigBuf, tbuf);
      }
    }

    if(isSet(PF_S_FLAG))
    {
      cp = FilterChars(bigBuf, " ");
    }
    else
    {
      cp = bigBuf;
    }

    if(isSet(PF_W_FLAG))
    {
      if(strlen(env->userfile))
      {
        if((fp = fopen(env->userfile, "a")))
        {
          fprintf(fp, "%s\n", cp);

          fclose(fp);

          return(0);
        }
        else
        {
          st_fail("Unable to open userfile: %s", env->userfile);
          return(1);
        }
      }
      else
      {
        st_fail("Write called without setting userfile");
        return(1);
      }
    }
    else
    {
      st_print("%s", cp);
    }
  }

  st_print("\n");

  return(0);
}


int isSpecial(cp)
char cp;
{
  if(isalnum(cp) || isspace(cp) || cp == 46 || cp == 95) /* check for '.' & '_' */
  {
    return(0);
  }

  return(1);
}


#define FIX_MATH_REPLACE  1
#define FIX_MATH_COMPRESS 2

struct FIX_OPERATORS {
	char *find;
	char *replace;
};

struct FIX_OPERATORS fixers[] = {
	{ "< =",	"<= "  },
	{ "> =",	">= "  },
	{ "! =",	"!= "  },
	{ "= =",	"== "  },
};

int max_fix_operators = (sizeof(fixers) / sizeof(struct FIX_OPERATORS));


/* the expandCmd can break things like <=, >=, ==, e-3, e+3 etc; fix them here */
/* FIXED: e-3, e+3 fixed now; get around e+3 error by using 1.0e3, not 1.0e+3 */

int fixMathOperators(buf)
char *buf;
{
  int i;
  char *cp;

  if(verboseLevel & VERBOSE_PARSE)
  {
    st_print("%s(%s:%d):\n", __func__, __FILE__, __LINE__);
    st_print("%s(%s:%d): Starting String: %s\n", __func__, __FILE__, __LINE__, buf);
  }

  strcmprs(buf); /* remove any excess spaces */

  if(verboseLevel & VERBOSE_PARSE)
  {
    st_print("%s(%s:%d):\n", __func__, __FILE__, __LINE__);
    st_print("%s(%s:%d): Compressed String: %s\n", __func__, __FILE__, __LINE__, buf);
  }

  for(i=0;i<max_fix_operators;i++)
  {
    if((cp = strstr(buf, fixers[i].find)))
    {
      strncpy(cp, fixers[i].replace, 3);
    }
  }

  strcmprs(buf);

  if(verboseLevel & VERBOSE_PARSE)
  {
    st_print("%s(%s:%d):\n", __func__, __FILE__, __LINE__);
    st_print("%s(%s:%d): Ending String: %s\n", __func__, __FILE__, __LINE__, buf);
  }

  return(0);
}


/* Copy the src string into the dst string placing spaces 
 * into the command so the parser can identify possible 
 * variable named tokens 
 *
 * FIXME: Does NOT handle exponets properly;  1.0e-3 turns into 1.0e - 3 
 *
 */
int expandCmd(src, dst)
char *src, *dst;
{
  char *cp, *ccp;

  if(verboseLevel & VERBOSE_PARSE)
  {
    st_print("%s(%s:%d): Starting Buf: %s\n", __func__, __FILE__, __LINE__, src);
  }

  dst[0] = '\0';

  cp  = src;
  ccp = dst;

  for(;*cp;cp++)
  {
    if(isSpecial(*cp)) /* if it's a non alphanumeric char add extra space */
    {
      *ccp = ' ';
       ccp++;
      *ccp = *cp;
       ccp++;
      *ccp = ' ';
       ccp++;
    }
    else               /* Else just copy the char to the dst */
    {
      *ccp = *cp;
      ccp++;
    }
  }

  if(verboseLevel & VERBOSE_PARSE)
  {
    st_print("%s(%s:%d): Endig Buf: %s\n", __func__, __FILE__, __LINE__, dst);
  }

  fixMathOperators(dst);

  return(0);
}


/**
 * Evaluate the right side of the equation by substituting 
 * know variables into string
 */
int evaluateRightSide(k, n, buf)
int k, n;
char *buf;
{
  int i;
  struct QUERY_VAR qvw;
  char tbuf[512];

  for(i=k;i<n;i++) /* Substitute any variable names here */
  {
    if(verboseLevel & VERBOSE_PARSE)
    {
       st_print("%s(%s:%d):\n", __func__, __FILE__, __LINE__);
       st_print("%s(%s:%d): Working token %s\n", __func__, __FILE__, __LINE__, args->tok[i]);
    }

    if(findAnyVariable(args->tok[i], &qvw))
    {
      if(verboseLevel & VERBOSE_PARSE)
      {
        st_print("%s(%s:%d): %s is a %d %d variable\n", __func__, __FILE__, __LINE__, args->tok[i], qvw.stype, qvw.ntype);
      }

      sprintf(tbuf, "%s ", qvw.buf); /* buf already conatins the ascii representation of the variable */
      strcat(buf, tbuf);
    }
    else
    {
      if(verboseLevel & VERBOSE_PARSE)
      {
        st_print("%s(%s:%d): %s not a variable\n", __func__, __FILE__, __LINE__, args->tok[i]);
      }

      sprintf(tbuf, "%s ", qvw.buf); /* buf already conatins the ascii representation of the variable */
      strcat(buf, tbuf);
    }
  }

  if(verboseLevel & VERBOSE_PARSE)
  {
    st_print("%s(%s:%d): To doCalculation() %s\n", __func__, __FILE__, __LINE__, buf);
  }

  return(0);
}


/** in the form:

    let variable = expression
    now can be called with just variable = expression

\bug   Can not modify a header variable.  If called with a header variable, \n
       then system will create a dynamic variable with the same name. \n
       Probably not what you want. \n
       Need to change this behaviour

 */
int doLetCmd(buf)
char *buf;
{
  int i, n, ret;
  char tbuf[512], var[256], evalBuf[1024];
  struct GSKEY *pgs;
  struct DYNAMIC_VARIABLES *pdv;
  struct QUERY_VAR qvl;

  if(verboseLevel & VERBOSE_PARSE)
  {
    st_print("%s(%s:%d): Before Expansion: %s\n", __func__, __FILE__, __LINE__, buf);
  }

  bzero(tbuf, sizeof(tbuf));

  expandCmd(buf, tbuf);

  if(verboseLevel & VERBOSE_PARSE)
  {
    st_print("%s(%s:%d): After Expansion:  %s\n", __func__, __FILE__, __LINE__, tbuf);
  }

/* First parse out the variable name */

  n = get_tok(tbuf);

  if(verboseLevel & VERBOSE_PARSE)
  {
    st_print("%s(%s:%d): N = %d\n", __func__, __FILE__, __LINE__, n);
  }

  if(verboseLevel & VERBOSE_PARSE)
  {
    st_print("Tokens are: ");
    for(i=0;i<n;i++)
    {
      st_print("%s ", args->tok[i]);
    }
    st_print("\n");
  }

  if(n > 1)
  {
    strcpy(var, args->tok[0]);

    ret = findAnyVariable(var, &qvl);

    if(verboseLevel & VERBOSE_PARSE)
    {
      st_print("%s(%s:%d): After findVariable(%s): Ret = %d\n", __func__, __FILE__, __LINE__, var, ret);
    }

    if(ret) /* We have a defined variable so parse it */
    {
      /* Now find expression */
      if(!strcmp(args->tok[1], "=")) /* Assign the variable */
      {
        if(n > 2)
        {
          evalBuf[0] = '\0';

          if(evaluateRightSide(2, n, evalBuf))
          {
	    st_fail("Failure in expression evaluation");
            return(1);
          }

          if(!doCalculation(evalBuf))
          {
            if(qvl.stype) /* it was a previously defind variable */
            {
              if(qvl.stype == VARIABLE_TYPE_DVAR)
              {
		if(qvl.indx < DYN_VARIBLE_PROTECTED)
                {
	          st_fail("Not allowed to change protected variables");
                  return(1);
		}

                if(verboseLevel & VERBOSE_PARSE)
                {
                  st_print("%s(%s:%d): Setting = DVAR\n", __func__, __FILE__, __LINE__);
                }

	        pdv = &dvars[qvl.indx];
	        if(pdv->type == DYN_TYPE_INT)
	        {
                  *pdv->ival = (int)env->rst;
                  st_report("Setting %s to %d", pdv->name, *pdv->ival);
	        }
	        else
	        {
                  *pdv->dval = env->rst;

	          if(fabs(env->rst) < 0.5)
                  {
                    st_report("Setting %s to %e", pdv->name, *pdv->dval);
                  }
                  else
                  {
                    st_report("Setting %s to %lf", pdv->name, *pdv->dval);
                  }
	        }
              }
	      else
	      if(qvl.stype == VARIABLE_TYPE_GSVAR)
              {
                if(verboseLevel & VERBOSE_PARSE)
                {
                  st_print("%s(%s:%d): Setting = GSVAR\n", __func__, __FILE__, __LINE__);
                }

		pgs = &getset[qvl.indx];

		if(pgs->type == INTEGER)
		{
                  *pgs->iaddr = (int)env->rst;
                  st_report("Setting %s to %d", pgs->name, *pgs->iaddr);
		}
		else
		if(pgs->type == DOUBLE)
		{
                  *pgs->daddr = env->rst;
                  st_report("Setting %s to %f", pgs->name, *pgs->daddr);
		}
              }
	      else
	      if(qvl.stype == VARIABLE_TYPE_HEADER)
	      {
		st_report("CAUTION: ******* Setting HEADER variable: '%s' ********", qvl.name);

		/* We can only change doubles at this time */
		if(qvl.phead.type == DOUBLE)
		{
		  st_report("Setting Header Variable %s to %f", qvl.name, env->rst);

		  /* gvl.phead points to a static local header */
		  *qvl.phead.dptr = env->rst;

		  /* Copy the local header to the working header */
		  updateHeader();
		}
	      }
            }
	    else
	    {
              FAIL_INFO("Assignment variable not found");
	    }
          }
        }
      }
      else
      {
        FAIL_INFO("No '=' sign found in expression");
      }
    }
    else
    {
      FAIL_INFO("You must define a variable before assignment");
    }
  }

  return(0);
}


/* Remove all spaces from command before sending it to calculator engine 
 * This fixed the problem of the expansion function turning 1.0e-3 into 10e - 3 
 * This function turns 1.0e - 3 back into 1.0e-3; the lcalc function doesn't care
   it the passed argument has spaces. */
int removeAllSpaces(in, out)
char *in, *out;
{
  char *cpin, *cpout;

  if(verboseLevel & VERBOSE_PARSE)
  {
    st_print("%s(%s:%d): IN: %s\n", __func__, __FILE__, __LINE__, in);
  }

  cpin = in;
  cpout = out;

  for( ;*cpin;cpin++)
  {
    if(!isspace(*cpin))
    {
      *cpout = *cpin;
       cpout++;
    }
  }

  if(verboseLevel & VERBOSE_PARSE)
  {
    st_print("%s(%s:%d): OUT: %s\n", __func__, __FILE__, __LINE__, out);
  }

  return(0);
}


int doCalculation(buf)
char *buf;
{
  char line[256], tbuf[256];

  bzero(tbuf, sizeof(tbuf));

  removeAllSpaces(buf, tbuf);

  env->rst = 0.0;

  if(verboseLevel & VERBOSE_PARSE)
  {
    st_print("%s(%s:%d): %s\n", __func__, __FILE__, __LINE__, tbuf);
  }

/**

\bug  lcalc exits if there is a math error; 			\n
      must set in place a math_err handler			\n
      Also, the command line args are not evaluated first.	\n
      lcalc doesn't know what to do with the command and exits.	\n

*/
  strcpy(line, lcalc(tbuf));

  strcmprs(line);

  if(strlen(line))
  {
    if(strstr(line, "undefined"))
    {
      st_print("%s: %s", tbuf, line);
      FAIL_INFO("Failure in doCalculation()");
      return(1);
    }

    if(strstr(line, "Error"))
    {
      st_print("%s: %s", tbuf, line);
      FAIL_INFO("Failure in doCalculation()");
      return(1);
    }

    if(strstr(line, "Unable"))
    {
      st_print("%s: %s", tbuf, line);
      FAIL_INFO("Failure in doCalculation()");
      return(1);
    }

    if(strstr(line, "Missing"))
    {
      st_print("%s: %s", tbuf, line);
      FAIL_INFO("Failure in doCalculation()");
      return(1);
    }

    if(strstr(line, "Unknown"))
    {
      st_print("%s: %s", tbuf, line);
      FAIL_INFO("Failure in doCalculation()");
      return(1);
    }

    if(strlen(line))
    {
      if(verboseLevel & VERBOSE_PARSE)
      {
        st_print("doCalculation(): Entering Math Parser with: '%s'\n", line);
      }

      if(!strncmp(line, "~", 1)) /* Check for 'calc' roundoff flag */
      {
        env->rst = strtod(line+1, NULL);
      }
      else
      {
        env->rst = strtod(line, NULL);
      }

      if(verboseLevel & VERBOSE_PARSE)
      {
        st_print("doCalculation(): Results: '%s', %.6e, len = %d\n", line, env->rst, strlen(line));
      }

      return(0);
    }
  }

  st_print("%s: ", tbuf);
  st_print("Failure: doCalc() returned NULL\n");

  return(1);
}


/**
 * Split single command up into tokens
 */
int get_tok(edit)
char *edit;
{
  int c = 0;
  char *token;

  bzero((char *)&args->tok, sizeof(args->tok));

  token = strtok(edit, "\n\t ");
  while(token != (char *)NULL)
  {
    sprintf(args->tok[c++], "%s", token);
    token = strtok((char *)NULL, "\n\t ");
  }

  return(c);
}


/**
 * Split multiple commands up in to seperate commands
 */
int get_cmd_tok(edit)
char *edit;
{
  int c = 0;
  char *token;

  bzero((char *)&args->cmd_tok, sizeof(args->cmd_tok));

  token = strtok(edit, ";");
  while(token != (char *)NULL)
  {
    sprintf(args->cmd_tok[c++], "%s", token);
    token = strtok((char *)NULL, ";");
  }

  return(c);
}


int bang(buf)
char *buf;
{
  int ret;
  char *cp;

  ret = history_expand(buf, &cp);

  if(ret == -1)	/* Not found */
  {
    st_report("Command %s, not found in History", buf);
  }

  if(ret == 1 || ret == 0)	/* Yes, found */
  {
    st_report("%s", cp);
    my_add_history(cp);
    parseCmd(cp);

    free(cp);
  }

  if(ret == 2) /* :p Just print it */
  {
    st_print("%s %s\n", prompt, cp);

    my_add_history(cp);

    free(cp);
  }

  return(0);
}


/**
 * determine of passed token is a previously defined command
 */
int isCommand(buf)
char *buf;
{
  int i;
  struct COMMANDS *p;

  p = &commands[0];
  for(i=0;p->cmd;i++,p++)
  {
    if(!strncasecmp(p->cmd, buf, strlen(buf)))
    {
      return(i);
    }
  }

  return(-1);
}


int recordThisCommand(buf)
char *buf;
{
  FILE *fp;

  if(strlen(recordingFname))
  {
    if((fp = fopen(recordingFname, "a")))
    {
      fprintf(fp, "%s\n", buf);

      fclose(fp);
    }
    else
    {
      st_fail("Unable to open %s", recordingFname);

      return(1);
    }
  }
  else
  {
    st_fail("recordingFname not set");

    return(1);
  }

  return(0);
}



int printAmbiguousCommand(arg, cmds)
char *arg;
struct COMMANDS *cmds;
{
  int len, found=0;
  char tbuf[512], bbuf[2048], batchBuf[2048];
  struct COMMANDS *p;

  if(arg)
  {
    bzero(tbuf, sizeof(tbuf));
    bzero(bbuf, sizeof(bbuf));
    bzero(batchBuf, sizeof(batchBuf));

    len = strlen(arg);

    p = &commands[0];

    for(;p->cmd;p++)
    {
      if(p->ntoken == -1) /* Skip all those help commands */
      {
        continue;
      }

      /* print all commands who's first few letters match */

      if(p->mode == env->mode || p->mode == MODE_ALL)
      {
        if(!strncasecmp(p->cmd, arg, len))
        {
          sprintf(tbuf, "%s ", p->cmd);
          strcat(bbuf, tbuf);

          found++;
        }
      }
    }

    found += findAmbiguousBatch(arg, batchBuf);

    if(found)
    {
      st_report("Ambiguous Command: %s", arg);

      strcmprs(bbuf);

      st_report("Possible  Command: %s %s", bbuf, batchBuf);
    }
  }

  return(0);
}


/* The following commands can contain an '=' sign in the command
   and not trigger the calculation function;  variable = 123.0
   should continue to function.
 */
char *allowedCommands[] = { "label", NULL };
#define MAX_ALLOWED_COMMANDS 1

int equalAllowCommand(cmd)
char *cmd;
{
  int   i;
  char *cp;

  for(i=0;i<MAX_ALLOWED_COMMANDS;i++)
  {
    cp = allowedCommands[i];

    if(verboseLevel & VERBOSE_PARSE)
    {
      st_report("equalAllowCommand(): Comparing: %s %s", cmd, cp);
    }

    if(!strncmp(cmd, cp, strlen(cp)))
    {
      if(verboseLevel & VERBOSE_PARSE)
      {
	st_report("Allowed");
      }

      return(1);
    }
  }

  return(0);
}


/**
 * Main parser
 */
int parseCmd(buf)
char *buf;
{
  int i, j, n, len, commandOK, ret;
  char tbuf[512], ttbuf[512], evalBuf[1024];
  char tmp[256];
  char saveBuf[256];
  struct COMMANDS *p;

  /* First, remove all extra spaces from command; including leading */
  strcmprs(buf);

  if(recording)
  {
    recordThisCommand(buf);
  }

  /* Save the stack */
  pushArgs();

  /* Clear all the '/' flags */
  clearFlags();

  if(!strstr(buf, "alias")) /* Avoid commands with the word alias in them */
  {
    substituteAliases(buf); /* Go substitute any aliases */
  }

  /* Save a copy of the command */
  strcpy(tbuf, buf);

  if(errPrint_fp)
  {
    fprintf(errPrint_fp, "%s %s\n", prompt, buf);
  }

  /* Trap the 'eval' command */
  if(!strncmp(tbuf, "eval ", 5))
  {
    bzero(ttbuf, sizeof(ttbuf));
    expandCmd(tbuf, ttbuf);

    n = get_tok(ttbuf);

    if(n < 3)
    {
      st_fail("Too few args for Eval Function");

      /* pop the stack */
      popArgs();

      return(1);
    }

    evalBuf[0] = '\0';

    if(evaluateRightSide(1, n, evalBuf))
    {
      st_fail("Failure in expression evaluation");

      /* pop the stack */
      popArgs();

      return(1);
    }

    if(!doCalculation(evalBuf))
    {
      if(fabs(env->rst) < 0.01)
      {
        st_print("Results: %.6e\n", env->rst);
      }
      else
      {
        st_print("Results: %f\n", env->rst);
      }
    }

  /* pop the stack */
    popArgs();

    return(0);
  }

  if(!strncmp(tbuf, "let ", 4) || !strncmp(tbuf, "set ", 4))
  {
    doLetCmd(tbuf+4);

  /* pop the stack */
    popArgs();

    return(0);
  }


  /* First break the command into individual commands that are
     seperated by any ';' characters */
  args->ncmds = get_cmd_tok(tbuf);

  if(verboseLevel & VERBOSE_PARSE)
  {
    st_report("parseCmd(): Found %d Sub-Command(s)", args->ncmds);
  }

  /* Now parse each seperate command */
  for(j=0;j<args->ncmds;j++)
  {
    if(verboseLevel & VERBOSE_PARSE)
    {
      st_report("parseCmd(): Parsing sub-command[%d]: %s", j, args->cmd_tok[j]);
    }

    strcpy(saveBuf, args->cmd_tok[j]);

    args->ntokens = get_tok(args->cmd_tok[j]);

    parseFlags();

    substituteEnv();

    if(!strcmp(args->firstArg, "#"))
    {
      if(verboseLevel & VERBOSE_PARSE)
      {
        REPORT_INFO("parseCmd(): Ignoring # Comment");
      }

      continue;
    }

    /* Check for a variable assignment; must be done after parseFlags() */

    /*	Now comes the fun time; what happens when the commands is actually
	something like: "label set 0 210.436 0.088 1 2 '^{13}C^{32}S-V=0'"

	There has to be a better way then strstr'ing a '=' in the whole 
	command;  maybe a list of available commands that may have the '='
	sign in them ??? */
    if(strstr(saveBuf, "=") && !equalAllowCommand(saveBuf))
    {
      /* reassemble command minus the flags */

      reassembleCommand(saveBuf);

      doLetCmd(saveBuf);

      if(isSet(PF_R_FLAG))
      {
        xx();
      }

      continue;
    }

    if(verboseLevel & VERBOSE_PARSE)
    {
      st_report("parseCmd(): Parsing %d tokens", args->ntokens);
    }

    if(verboseLevel & VERBOSE_PARSE)
    {
      for(i=0;i<args->ntokens;i++)
      {
        if(cactus_isnumber(args->tok[i]))
        {
          st_report("parseCmd(): Token %d: %s; is number", i, args->tok[i]);
        }
        else
        if(isCommand(args->tok[i]) >= 0)
        {
          st_report("parseCmd(): Token %d: %s; is Command", i, args->tok[i]);
        }
        else
        {
          st_report("parseCmd(): Token %d: %s; is possible arg", i, args->tok[i]);
        }
      }
    }

    /* Check if user is inputting numbers first */
    /* Only works for first two args */
    if(cactus_isnumber(args->firstArg))  /* RPN ?? */
    {
      if(verboseLevel & VERBOSE_PARSE)
      {
        REPORT_INFO("parseCmd(): Swapping tokens 0 <-> 1");
      }

      strcpy(tmp, args->firstArg); /* Save it */
      strcpy(args->tok[0], args->tok[1]);/* Reverse them */
      strcpy(args->tok[1], tmp);
    }

    p = &commands[0];

    commandOK = 0;

    len = strlen(args->firstArg);

    if(len == 0)
    {
      st_fail("parseCmd(): Missing or mal-formed command: %s", buf);

      /* pop the stack */
      popArgs();

      return(1);
    }

    for(;p->cmd;p++)
    {
      if(!strncasecmp(p->cmd, args->firstArg, len))
      {
        if(isSet(PF_H_FLAG) && isFirst(PF_H_FLAG))
        {
	  printH_FlagHelp(p->cmd);

          popArgs();

          return(0);
        }

        if(p->ntoken == -1)
        {
          printAmbiguousCommand(args->firstArg, p);

          if(verboseLevel & VERBOSE_PARSE)
          {
            st_report("parseCmd(): Returning %d", 0);
          }

          popArgs();

          return(1);
        }

        if(p->ntoken > 0 && args->ntokens < p->ntoken) /* Should have at least ntoken's */
        {
          st_print("parseCmd(): Missing argument(s) to command: [%s]\n", p->cmd);
          st_print("parseCmd(): Usage: %s %s  : %s\n", p->cmd, p->args, p->help);

          if(verboseLevel & VERBOSE_PARSE)
          {
            st_report("parseCmd(): Returning %d", 1);
          }

          popArgs();

          return(1);
        }

        if(verboseLevel & VERBOSE_PARSE)
        {
          st_report("parseCmd(): Excuting command %s", p->cmd);
        }

	/* Check for proper mode now */
        if(p->mode && p->mode != env->mode)
        {
          if(env->mode == MODE_OTF)  /* Check for OTF mode; SPEC commands OK */
          {
	    if(p->mode != MODE_SPEC)
            {
              st_fail("parseCmd(): %s Command not allowed in %s mode", p->cmd, modeStrs[env->mode]);
              popArgs();

              return(1);
            }
          }
          else
          {
            st_fail("parseCmd(): %s Command not allowed in %s mode", p->cmd, modeStrs[env->mode]);
            popArgs();

            return(1);
          }
        }

        if(verboseLevel & VERBOSE_PARSE)
        {
          st_report("parseCmd(): Setting Current Command to %s", args->currentCmd);
        }

        strcpy(args->currentCmd, p->cmd); /* Some functions are overloaded; remember who called */
        strcmprs(args->currentCmd); /* Strip any spaces */

        ret = p->fctn();

        if(!ret && p->post) /* Command executed OK and it has a post command */
        {
          p->post();
        }

        if(ret)
        {
	  st_fail("parseCmd(): Command: %s Failed", p->cmd);

          popArgs();

          return(ret);
        }

        commandOK = 1;

        break;
      }
    }

    /* Keep looking */
    if(!commandOK) /* Maybe its a loaded batch procedure */
    {
      ret = isBatch(args->firstArg);
      if(ret >= 0)
      {
        execBatch(args->firstArg);
	commandOK = 1;
      }
    }

    if(!commandOK)
    {
      st_fail("parseCmd(): Command(%s); Not Found", args->firstArg);

      popArgs();

      return(2);
    }
  } /* Looping through ';' seperated commands */

  popArgs();

  return(0);
}



void process_line(char *line)
{
  static int nulls=0;
  int len, old_stquiet;
  char tbuf[512];

  if(line == NULL)
  {
    nulls++;			/* Check for lost stdin */

    if(nulls > 25)		/* good numer */
    {
      st_report("Infinite Loop detected; exiting");
      exit(1);
    }

    st_print("process_line(): received NULL line\n");
    return;
  }

  nulls = 0;

  strcpy(myline, line);

  free(line);

  len = strlen(myline);

   /* Look for things like 'c?' or 'map?' */
   /* backend command can have wildcards including ? in it */
  if(!strncmp(myline+(len-1), "?", 1) && !strstr(myline, "back")) 
  {
    letterHelp(myline);
  }
  else
  if(!strncmp(myline, "@", 1)) /* Catch the special case of the '@' char */
  {
    sprintf(tbuf, "load %s", myline+1);
    parseCmd(tbuf);
  }
  else
  if(!strncmp(myline, "!", 1))
  {
    bang(myline);
  }
  else
  if(!strncmp(myline, ":", 1))
  {
    if(strlen(myline) > 2)
    {
      system(myline+1); /* Ignore the ':' character */
      my_add_history(myline);
    }
  }
  else
  {
    /* Make sure all commands see the most recent data file listing */
    old_stquiet = stquiet;
    stquiet = 1;           /* Be quiet about it */

    fileChanged();         /* Check if new data present */

    stquiet = old_stquiet; /* Restore old quiet flag */

    parseCmd(myline);

    if(strlen(myline) > 1)
    {
      my_add_history(myline);
    }
  }

  fflush(stdin);
}

/* Shortcut to 'autoscale' */
int ctrl_a_list(int arg1, int arg2)
{
  st_print("\n");

  doAutoScale();
  xx();

  /* redraw the current line - this is an undocumented function. It invokes the
   * redraw-current-line command.
   */
  rl_refresh_line(0, 0);

  return(0);
}


/* Shortcut to 'baseline' */
int ctrl_b_list(int arg1, int arg2)
{
  st_print("\n");

  doBaseline();
  xx();

  /* redraw the current line - this is an undocumented function. It invokes the
   * redraw-current-line command.
   */
  rl_refresh_line(0, 0);

  return(0);
}


/* Shortcut to 'summary' */
int ctrl_t_list(int arg1, int arg2)
{
  st_print("\n");

  doSummary();

  /* redraw the current line - this is an undocumented function. It invokes the
   * redraw-current-line command.
   */
  rl_refresh_line(0, 0);

  return(0);
}

/* Shortcut to 'list' */
int ctrl_l_list(int arg1, int arg2)
{
  st_print("\n");

  listScans();

  /* redraw the current line - this is an undocumented function. It invokes the
   * redraw-current-line command.
   */
  rl_refresh_line(0, 0);

  return(0);
}


/* Shortcut to 'params' */
int ctrl_p_list(int arg1, int arg2)
{
  st_print("\n");

  showParams();

  /* redraw the current line - this is an undocumented function. It invokes the
   * redraw-current-line command.
   */
  rl_refresh_line(0, 0);

  return(0);
}

/* Shortcut to 'tellEnv' */
int ctrl_e_list(int arg1, int arg2)
{
  st_print("\n");

  tellEnv();

  /* redraw the current line - this is an undocumented function. It invokes the
   * redraw-current-line command.
   */
  rl_refresh_line(0, 0);

  return(0);
}


/* Shortcut to 'xgrab' */
int ctrl_x_list(int arg1, int arg2)
{
  st_report("Grabing Plot Extents");

  setFlag(PF_S_FLAG, 1);
  getPlotExtents();
  setFlag(PF_S_FLAG, 0);

  return(0);
}


/* Shortcut to 'redo' */
int ctrl_r_list(int arg1, int arg2)
{
  doRedo();

  xx();

  /* redraw the current line - this is an undocumented function. It invokes the
   * redraw-current-line command.
   */
//  rl_refresh_line(0, 0);

  return(0);
}

/* Shortcut to 'undo' */
int ctrl_u_list(int arg1, int arg2)
{
  doUndo();

  xx();

  return(0);
}


/* Shortcut to 'window /c' */
int ctrl_w_list(int arg1, int arg2)
{
  defineWindowFromXgraphics();

  return(0);
}


int showShortCuts()
{
  printH_FlagHelp(args->firstArg);

  return(0);
}


int ctrl_g_list(int arg1, int arg2)
{
  char tbuf[256];

  sprintf(tbuf, "gauss /a");

  parseCmd(tbuf);

  return(0);
}


int init_readline(void)
{
  rl_add_defun("auto",   &ctrl_a_list, CTRL('a'));
  rl_add_defun("base",   &ctrl_b_list, CTRL('b'));
  rl_add_defun("env",    &ctrl_e_list, CTRL('e'));
  rl_add_defun("list",   &ctrl_l_list, CTRL('l'));
  rl_add_defun("params", &ctrl_p_list, CTRL('p'));
  rl_add_defun("redo",   &ctrl_r_list, CTRL('r'));
  rl_add_defun("summ",   &ctrl_t_list, CTRL('t'));
  rl_add_defun("undo",   &ctrl_u_list, CTRL('u'));
  rl_add_defun("window", &ctrl_w_list, CTRL('w'));
  rl_add_defun("xgrab",  &ctrl_x_list, CTRL('x'));
  rl_add_defun("gauss",  &ctrl_g_list, CTRL('g'));

  return(0);
}


int setColor(c)
int c;
{
  if(c == oldColor)
  {
    return(0);
  }

  switch(c)
  {
    case ANSI_BLACK:   st_print("%c[0m",     0x1b); break;
    case ANSI_RED:     st_print("%c[31m",    0x1b); break;
    case ANSI_GREEN:   st_print("%c[32;1m",  0x1b); break;
    case ANSI_YELLOW:  st_print("%c[33;1m",  0x1b); break;
    case ANSI_BLUE:    st_print("%c[34;1m",  0x1b); break;
    case ANSI_MAGENTA: st_print("%c[35;1m",  0x1b); break;
    case ANSI_CYAN:    st_print("%c[36;1m",  0x1b); break;
    case ANSI_WHITE:   st_print("%c[37;40m", 0x1b); break;
    case ANSI_RESERVE: st_print("%c[0m",     0x1b); break;
    case ANSI_RESET:   st_print("%c[39m",    0x1b); break;
  }

  oldColor = c;

  return(0);
}


int doColor()
{
  int i, color;

  if(isSet(PF_R_FLAG))
  {
    setColor(ANSI_BLACK);

    return(0);
  }

  if(!strlen(args->secondArg))
  {
    color = parseColor();
    setColor(color);

    return(0);
  }

  for(i=0;i<MAX_ANSI_COLORS;i++)
  {
    if(!strcasecmp(args->secondArg, ansi_colors[i]))
    {
      setColor(ANSI_BLACK+i);
      return(0);
    }
  }

  return(1);
}


int findInArgs(parg)
char *parg;
{
  int i;

  for(i=0;i<MAX_TOKENS;i++)
  {
    if(strlen(args->tok[i]))
    {
      if(!strncmp(args->tok[i], parg, strlen(parg)))
      {
        return(1);
      }
    }
  }

  return(0);
}


int recordCmds()
{
  char sysBuf[512];

  if(args->ntokens == 3)
  {
    if(!strncmp(args->secondArg, "on", 2))
    {
      if(strlen(args->thirdArg))
      {
	strcpy(recordingFname, args->thirdArg);
        recording = 1;

        st_report("Recording Commands to %s", recordingFname);

        sprintf(sysBuf, "lp_removefile %s", recordingFname);

        system(sysBuf);
      }
      else
      {
        printH_FlagHelp("record");
      }
    }
    else
    {
      printH_FlagHelp("record");
    }
  }
  else
  if(args->ntokens == 2)
  {
    if(!strncmp(args->secondArg, "off", 3))
    {
      recording = 0;
      bzero(recordingFname, sizeof(recordingFname));

      st_report("Turning Command Recording Off");
    }
    else
    {
      printH_FlagHelp("record");
    }
  }
  else
  {
    printH_FlagHelp("record");
  }

  return(0);
}


int showVariables()
{
  listDynVars();

  return(0);
}


/* Filter out passed charaters from a string; good for stripping out spaces and tabs */
char *FilterChars(String, Filter)
char *String, *Filter;
{
  int a=0,i=0;
  char *Filtered = (char *)malloc(strlen(String) * sizeof(char));

  for(a=0;String[a];a++)
  {
    if(!strchr(Filter, String[a]))
    {
      Filtered[i++]=String[a];
    }
  }

  Filtered[i]=0;

  return(Filtered);
}

char sv_tok[40][40];


/**
 * Split single command up into tokens
 */
int get_sv_tok(edit)
char *edit;
{
  int c = 0;
  char *token;

  bzero((char *)&sv_tok, sizeof(sv_tok));

  token = strtok(edit, "\n\t ");
  while(token != (char *)NULL)
  {
    sprintf(sv_tok[c++], "%s", token);
    token = strtok((char *)NULL, "\n\t ");
  }

  return(c);
}



int substituteVariables(inbuf, outbuf)
char *inbuf, *outbuf;
{
  int i, n;
  struct QUERY_VAR qvw;
  char tbuf[512];

  n = get_sv_tok(inbuf);

  outbuf[0] = '\0';

  for(i=0;i<n;i++) /* Substitute any variable names here */
  {
    if(verboseLevel & VERBOSE_PARSE)
    {
       st_print("%s(%s:%d):\n", __func__, __FILE__, __LINE__);
       st_print("%s(%s:%d): Working token %s\n", __func__, __FILE__, __LINE__, sv_tok[i]);
    }

    if(findAnyVariable(sv_tok[i], &qvw))
    {
      if(verboseLevel & VERBOSE_PARSE)
      {
        st_print("%s(%s:%d): %s is a %d %d variable\n", __func__, __FILE__, __LINE__, sv_tok[i], qvw.stype, qvw.ntype);
      }

      sprintf(tbuf, "%s ", qvw.buf); /* buf already conatins the ascii representation of the variable */
      strcat(outbuf, tbuf);
    }
    else
    {
      if(verboseLevel & VERBOSE_PARSE)
      {
        st_print("%s(%s:%d): %s not a variable\n", __func__, __FILE__, __LINE__, sv_tok[i]);
      }

      sprintf(tbuf, "%s ", qvw.buf); /* buf already conatins the ascii representation of the variable */
      strcat(outbuf, tbuf);
    }
  }

  if(verboseLevel & VERBOSE_PARSE)
  {
    st_print("%s(%s:%d): Result: %s\n", __func__, __FILE__, __LINE__, outbuf);
  }

  return(0);
}


/* Entry point to a generic one-line "if, else endif" function; */
int doIf()
{
  int i, ret;
  char buf[256], tbuf[512], *cp1=NULL, *cp2=NULL;
  char ebuf[256], evalBuf[256], trueCmd[256];

  if(verboseLevel & VERBOSE_PARSE)
  {
    st_print("%s(%s:%d): Enter\n", __func__, __FILE__, __LINE__);
  }

  bzero( buf, sizeof( buf));
  bzero(tbuf, sizeof(tbuf));

  if(strlen(args->secondArg))
  {
    reassembleCommand(buf); /* Take all the tokens and reassemble into a single command */

    if(verboseLevel & VERBOSE_PARSE)
    {
      st_print("%s(%s:%d): Parsing: %s\n", __func__, __FILE__, __LINE__, buf);
    }

    expandCmd(buf, tbuf);		/* Stretch it out so there are spaces between everything */

    if(verboseLevel & VERBOSE_PARSE)
    {
      st_print("%s(%s:%d): Now Parsing: %s\n", __func__, __FILE__, __LINE__, tbuf);
    }

    substituteVariables(tbuf, ebuf);

    /* Find the limits of the if statement */
    cp1 = strchr(ebuf, '(');
    cp2 = strrchr(ebuf, ')');

    if(cp1)
    {
      if(verboseLevel & VERBOSE_PARSE)
      {
        st_print("%s(%s:%d): cp1: %s\n", __func__, __FILE__, __LINE__, cp1);
      }
    }

    cp2++;
    if(cp2)
    {
      if(verboseLevel & VERBOSE_PARSE)
      {
        st_print("%s(%s:%d): cp2: %s\n", __func__, __FILE__, __LINE__, cp2);
      }
    }

    if(!cp1 || !cp2)
    {
      printH_FlagHelp(args->firstArg);

      return(1);
    }

    strcpy(trueCmd, cp2); /* Save the command to do if test evals to true */
    if(verboseLevel & VERBOSE_PARSE)
    {
      st_print("%s(%s:%d): True Cmd: %s\n", __func__, __FILE__, __LINE__, trueCmd);
    }

    strncpy(evalBuf, cp1, cp2-cp1);
    evalBuf[cp2-cp1] = '\0';

    if(verboseLevel & VERBOSE_PARSE)
    {
      st_print("%s(%s:%d): Eval Buf: %s\n", __func__, __FILE__, __LINE__, evalBuf);
    }

    /* Evaluate for true; 0, 1 or -1 will be left in env->rst */
    ret =  doCalculation(evalBuf);

    if(ret)
    {
      st_report("Test evaluations Failed");
      return(1);
    }

    if(verboseLevel & VERBOSE_PARSE)
    {
      st_print("%s(%s:%d): Result: %.1f\n", __func__, __FILE__, __LINE__, env->rst);
    }

    if(env->rst > 0.0)
    {
      lp_if_test_rst = LP_IF_TEST_TRUE;
    }
    else
    if(env->rst < 0.0)
    {
      lp_if_test_rst = LP_IF_TEST_NTRUE;
    }
    else
    {
      lp_if_test_rst = LP_IF_TEST_FALSE;
    }

    if(lp_if_test_rst == LP_IF_TEST_TRUE || lp_if_test_rst == LP_IF_TEST_NTRUE)
    {
      if(verboseLevel & VERBOSE_PARSE)
      {
        st_print("%s(%s:%d): Executing True Cmd: %s\n", __func__, __FILE__, __LINE__, trueCmd);
      }

      lp_if_test_state = LP_IF_STATE_TRUE;
      parseCmd(trueCmd);
    }
    else /* Find and set up 'else' command, if any, for later */
    {
      for(i=0;i<args->ncmds;i++)
      {
        if(verboseLevel & VERBOSE_PARSE)
        {
          st_print("%s(%s:%d): Searching for 'else' command in: %s:\n", __func__, __FILE__, __LINE__, args->cmd_tok[i]);
        }

        if(strstr(args->cmd_tok[i], "else "))
        {
          if(verboseLevel & VERBOSE_PARSE)
          {
            st_print("%s(%s:%d): Found 'else' command: %s:\n", __func__, __FILE__, __LINE__, args->cmd_tok[i]);
          }

          substituteVariables(args->cmd_tok[i]+5, lp_if_else_buf);
	  lp_if_test_state = LP_IF_STATE_ELSE;

	  break;
        }
      }
    }
  }
  else
  {
    printH_FlagHelp(args->firstArg);
    return(1);
  }

  if(verboseLevel & VERBOSE_PARSE)
  {
    st_print("%s(%s:%d): Leaving\n", __func__, __FILE__, __LINE__);
  }

  return(0);
}


int doElse()
{
  if(verboseLevel & VERBOSE_PARSE)
  {
    st_print("%s(%s:%d): Enter\n", __func__, __FILE__, __LINE__);
  }

  if(lp_if_test_state == LP_IF_STATE_ELSE)
  {
    if(*lp_if_else_buf && strlen(lp_if_else_buf))
    {
      parseCmd(lp_if_else_buf);

      if(verboseLevel & VERBOSE_PARSE)
      {
        st_print("%s(%s:%d): Leaving\n", __func__, __FILE__, __LINE__);
      }

      return(0);
    }
  }

  /* Let endif clear flags */

  if(verboseLevel & VERBOSE_PARSE)
  {
    st_print("%s(%s:%d): Leaving\n", __func__, __FILE__, __LINE__);
  }

  return(0);
}


int doEndIf()
{
  if(verboseLevel & VERBOSE_PARSE)
  {
    st_print("%s(%s:%d): Enter\n", __func__, __FILE__, __LINE__);
  }

  lp_if_test_state = LP_IF_STATE_IDLE;

  if(verboseLevel & VERBOSE_PARSE)
  {
    st_print("%s(%s:%d): Leaving\n", __func__, __FILE__, __LINE__);
  }

  return(0);
}


int doFor()
{
  st_report("doFor(): Enter");

  return(0);
}


int doEndFor()
{
  st_report("doEndFor(): Enter");

  return(0);
}


int doSexCmd()
{
  double d;
  char sexStr[256], tbuf[256];

  if(args->ntokens < 2)
  {
    printH_FlagHelp(args->firstArg);

    return(0);
  }

  if(strstr(args->secondArg, ":")) /* it's alread in sex, reverse it */
  {

    strcpy(tbuf, args->secondArg);
    strcmprs(tbuf);

    d = ctod(tbuf);

    if(isSet(PF_T_FLAG))
    {
      d *= 15.0;
    }

    if(isSet(PF_EXP_FLAG))
    {
      st_print("Value = %.8e\n", d);
    }
    else
    {
      st_print("Value = %.8f\n", d);
    }

    env->rst = d;
  }
  else					/* convert to sex */
  {
    d = ctod(args->secondArg);

    if(isSet(PF_T_FLAG))
    {
      d /= 15.0;
    }

    sexagesimal(d, sexStr, DDDD_MM_SS_SS100);

    st_print("Value = %s\n", sexStr);
  }

  return(0);
}
