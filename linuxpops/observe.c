/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"


/* Trap missing parameters */
int verifyParams()
{
  if(env->escan < 1.0)
  {
    st_report("Resetting Ending Scan Number to 10000.0");
    env->escan = 10000.0;
  }

  if(env->dfreq < 0.00001)
  {
    if(env->mode == MODE_SPEC)
    {
      env->dfreq = 0.001;
    }
    else
    {
      env->dfreq = 0.5;
    }

    st_report("Resetting Delta Freq to %f GHz", env->dfreq);

  }

  return(0);
}


int meetsCritiria(p, mask)
struct HEADER *p;
int mask;
{
  int    feed, ret;
  double freq;
  char source[64];

  verifyParams();

  if(env->mode == MODE_NOT_SET)
  {
    st_report("Resetting Mode MODE_CONT");
    env->mode = MODE_CONT;
  }

  if(mask & CHECK_SOURCE)
  {
    strncpy(source, p->object, 16);
    source[16] = '\0';
    strcmprs(source);

    if(env->check_source && strcasecmp(source, env->source)) /* Change to exact name lenght matching */
    {
      if(verboseLevel & VERBOSE_CRITIRIA)
      {
        printf("Reject Name\n");
      }

      incrementRejSource();

      return(0); /* Skip */
    }
  }

  if(mask & CHECK_SCANS)
  {
    if(p->scan > (env->escan+0.99) || p->scan < env->bscan)
    {
      if(verboseLevel & VERBOSE_CRITIRIA)
      {
        printf("Reject Scan Range\n");
      }

      incrementRejScans();

      return(0);
    }
  }

  if(mask & CHECK_DATE)
  {
    if(env->check_date)
    {
      if(verboseLevel & VERBOSE_CRITIRIA)
      {
        printf("Checking %.4f vs %.4f\n", p->utdate, env->date);
      }

      if((ret = lp_fcmp(p->utdate, env->date, 0.00005)))
      {
        if(verboseLevel & VERBOSE_CRITIRIA)
        {
          printf("Reject %d Date: %.4f\n", ret, p->utdate);
        }

        incrementRejDates();

        return(0);
      }
    }
  }

  if(mask & CHECK_BKEND)
  {
    if(!backendMatch(p))
    {
      if(verboseLevel & VERBOSE_CRITIRIA)
      {
        printf("Reject Backend\n");
      }

      incrementRejBackend();

      return(0);
    }
  }

  if(mask & CHECK_FREQ)
  {
    if(env->mode == MODE_CONT)
    {
      if(verboseLevel & VERBOSE_CRITIRIA)
      {
        printf("Reject Freq\n");
      }
      freq = restFreq(p) / 1000.0;
    }
    else
    {
      freq = p->restfreq / 1000.0;
    }

    if(env->freq > 0.0 && lp_fcmp(freq, env->freq, env->dfreq))
    {
      if(verboseLevel & VERBOSE_CRITIRIA)
      {
        printf("Reject Freq\n");
      }

      incrementRejFreq();

      return(0); /* No good */
    }
  }

/* Reject the 'matching freq' IF's that don't match freq selection & SB */
  if(env->issmt && env->strictsb)
  {
    feed = getFeed(p->scan); /* 1, 2, 3, 4 */

    if(p->sideband == LSB)
    {
      if(feed == 2 || feed == 4)
      {
        if(verboseLevel & VERBOSE_CRITIRIA)
        {
          printf("Reject Strict\n");
        }

        incrementRejSb();

        return(0);
      }
    }
    else
    if((int)p->sideband == USB)
    {
      if(feed == 1 || feed == 3)
      {
        if(verboseLevel & VERBOSE_CRITIRIA)
        {
          printf("Reject Strict\n");
        }

        incrementRejSb();

        return(0);
      }
    }
  }

  if(verboseLevel & VERBOSE_CRITIRIA)
  {
    printf("Passed All Preliminary Checks\n");
  }

  return(1);
}


int checkMatch()
{
  if(env->bkmatch != BKMATCH_FEED)
  {
    st_fail("Backend Match NOT set to FEED; Use 'accum'");

    return(1);
  }

  return(0);
}


int setFeed(want)
double want;
{
  if(strstr(env->filename, "_hc.")) /* Check for MAC data */
  {
    env->if_feed = (want + 10.0) / 100.0;
  }
  else
  if(env->issmt && env->bkres == 0.250)
  {
    env->if_feed = (want + 4.0) / 100.0;
  }
  else
  {
    env->if_feed = want / 100.0;
  }

  return(0);
}



/* Show only IF #1 */
int showC1()
{
  float old_feed;
  int old_check;

  if(checkMatch())
  {
    return(1);
  }

  old_check = env->check_feed;

    /* save any selected IF feed */
  if(env->check_feed)
  {
    old_feed = env->if_feed;
  }

  setFeed(1.0);
  env->feed = 0;

  env->check_feed = 1;

  accumSelected();

    /* restore any selected IF feed */
  if(old_check)
  {
    env->if_feed = old_feed;
  }
  else
  {
    env->if_feed = 0.0;
    env->check_feed = 0;
  }

  return(0);
}


int showC2()
{
  float old_feed;
  int old_check;

  if(checkMatch())
  {
    return(1);
  }

  old_check = env->check_feed;

    /* save any selected IF feed */
  if(env->check_feed)
  {
    old_feed = env->if_feed;
  }

  setFeed(2.0);
  env->feed = 1;

  env->check_feed = 1;

  accumSelected();

    /* restore any selected IF feed */
  if(old_check)
  {
    env->if_feed = old_feed;
  }
  else
  {
    env->if_feed = 0.0;
    env->check_feed = 0;
  }

  return(0);
}


/* Show only IF #3 */
int showC3()
{
  float old_feed;
  int old_check;

  if(checkMatch())
  {
    return(1);
  }

  old_check = env->check_feed;

    /* save any selected IF feed */
  if(env->check_feed)
  {
    old_feed = env->if_feed;
  }

  setFeed(3.0);
  env->feed = 2;

  env->check_feed = 1;

  accumSelected();

    /* restore any selected IF feed */
  if(old_check)
  {
    env->if_feed = old_feed;
  }
  else
  {
    env->if_feed = 0.0;
    env->check_feed = 0;
  }

  return(0);
}

/* Show only IF #4 */
int showC4()
{
  float old_feed;
  int old_check;

  if(checkMatch())
  {
    return(1);
  }

  old_check = env->check_feed;

    /* save any selected IF feed */
  if(env->check_feed)
  {
    old_feed = env->if_feed;
  }

  setFeed(4.0);
  env->feed = 3;

  env->check_feed = 1;

  accumSelected();

    /* restore any selected IF feed */
  if(old_check)
  {
    env->if_feed = old_feed;
  }
  else
  {
    env->if_feed = 0.0;
    env->check_feed = 0;
  }

  return(0);
}


int showCb()
{
  float old_feed;
  int old_check;

  if(checkMatch())
  {
    return(1);
  }

  old_check = env->check_feed;

    /* save any selected IF feed */
  if(env->check_feed)
  {
    old_feed = env->if_feed;
  }

  _h1();

  setFeed(1.0);
  env->feed = 0;

  env->check_feed = 1;

  accumSelected();

  _h2();

  setFeed(2.0);
  env->feed = 1;

  accumSelected();

  _h0();

  subArgs(2, "H1");
  subArgs(3, "H2");

  doSum();

  doAutoScale();

  xx();

    /* restore any selected IF feed */
  if(old_check)
  {
    env->if_feed = old_feed;
  }
  else
  {
    env->if_feed = 0.0;
    env->check_feed = 0;
  }

  return(0);
}


int showC12()
{
  float old_feed;
  int old_check;

  if(checkMatch())
  {
    return(1);
  }

  old_check = env->check_feed;

    /* save any selected IF feed */
  if(env->check_feed)
  {
    old_feed = env->if_feed;
  }

  setFeed(1.0);
  env->feed = 0;

  env->check_feed = 1;

  accumSelected();

  setFeed(2.0);
  env->feed = 1;

  env->accumreset = 2; /* Set for Dual Beam Accum */

  accumSelected();

    /* restore any selected IF feed */
  if(old_check)
  {
    env->if_feed = old_feed;
  }
  else
  {
    env->if_feed = 0.0;
    env->check_feed = 0;
  }

  return(0);
}


int showC34()
{
  float old_feed;
  int old_check;

  if(checkMatch())
  {
    return(1);
  }

  old_check = env->check_feed;

    /* save any selected IF feed */
  if(env->check_feed)
  {
    old_feed = env->if_feed;
  }

  setFeed(3.0);
  env->feed = 2;

  env->check_feed = 1;

  accumSelected();

  setFeed(4.0);
  env->feed = 3;

  env->accumreset = 2; /* Set for Dual Beam Accum */

  accumSelected();

    /* restore any selected IF feed */
  if(old_check)
  {
    env->if_feed = old_feed;
  }
  else
  {
    env->if_feed = 0.0;
    env->check_feed = 0;
  }

  return(0);
}

int showC13()
{
  float old_feed;
  int old_check;

  if(checkMatch())
  {
    return(1);
  }

  old_check = env->check_feed;

    /* save any selected IF feed */
  if(env->check_feed)
  {
    old_feed = env->if_feed;
  }

  setFeed(1.0);
  env->feed = 0;
 
  env->check_feed = 1;

  accumSelected();

  setFeed(3.0);
  env->feed = 2;

  env->accumreset = 2; /* Set for Dual Beam Accum */

  accumSelected();

    /* restore any selected IF feed */
  if(old_check)
  {
    env->if_feed = old_feed;
  }
  else
  {
    env->if_feed = 0.0;
    env->check_feed = 0;
  }

  return(0);
}


int showC24()
{
  float old_feed;
  int old_check;

  if(checkMatch())
  {
    return(1);
  }

  old_check = env->check_feed;

    /* save any selected IF feed */
  if(env->check_feed)
  {
    old_feed = env->if_feed;
  }

  setFeed(2.0);
  env->feed = 1;
  
  env->check_feed = 1;

  accumSelected();

  setFeed(4.0);
  env->feed = 3;

  env->accumreset = 2; /* Set for Dual Beam Accum */

  accumSelected();

    /* restore any selected IF feed */
  if(old_check)
  {
    env->if_feed = old_feed;
  }
  else
  {
    env->if_feed = 0.0;
    env->check_feed = 0;
  }

  return(0);
}


int showPhases()
{
  if(h0->head.scan < 1.0)
  {
    st_warn("No Scan Loaded for showPhases; Use 'get scan#'");
    return(0);
  }

  cont_show_phases(h0->yplot1, &h0->head, PSPLOT);

  return(0);
}


int showData()
{
  if(env->mode == MODE_SPEC)
  {
    line_showTable();
    return(0);
  }

  if(h0->head.scan < 1.0)
  {
    st_warn("No Scan loaded; Use 'get scan#'");
    return(0);
  }

  cont_display_data(h0->yplot1, &h0->head);

  return(0);
}


int saveData()
{
  if(h0->head.scan < 1.0)
  {
    st_warn("No Scan loaded; Use 'get scan#'");
    return(0);
  }

  if(env->mode == MODE_CONT)
  {
    cont_save_data(h0->yplot1, &h0->head);
  }
  else
  {
    line_save_data(h0);
  }

  return(0);
}


int changeRes()
{
  int i, ret, noint;
  float *f1, *f2;
  double ratio;
  struct HEADER *h;
  struct LP_IMAGE_INFO *image = h0;

  h = &image->head;
  noint = h->noint;

  if(env->newres > 0.0 && env->newres > h->freqres)
  {
    ratio = fabs(env->newres / h->freqres);

    ret = chgres(&image->yplot1[0], noint, fabs(h->freqres), env->newres);

    if(!ret)
    {
	/* Move the valid results down to the bottom */ 
      f1 = &image->yplot1[0];
      f2 = &image->yplot1[0];

      for(i=0;i<noint;i++,f1++)
      {
        if(gsl_finite(*f1)) /* If it's good copy to next slot starting at 0 */
        {
          *f2 = *f1;
           f2++;
        }
      }

      h->noint    = floor(h->noint / ratio) - 4.0; /* Throw a couple off each end */
      h->refpt   /= ratio;
      h->deltax  *= ratio;
      h->freqres *= ratio;

      env->bmark = 0;  /* turn off baseline boxes */
      env->mshow = 0;  /* turn off markers */
    }
    else
    {
      st_fail("Failure: %d in changeRes() function", ret);
    }
  }
  else
  {
    st_fail("newres value %f is 0.0 or is < current freqres: %f", env->newres, h->freqres);
  }

  return(0);
}


int showTable()
{
  int i, dlen;

  if(env->mode == MODE_SPEC)
  {
    line_showTable();
    return(0);
  }

  if(!strncmp(h0->head.obsmode, "CONTOPT", 7))
  {
    display_optical_table(&h0->yplot1[0], &h0->head);
    return(0);
  }

  if(h0->head.scan < 1.0)
  {
    st_warn("No Scan loaded; Use 'get scan#'");
    return(0);
  }

  dlen = h0->head.datalen / sizeof(float);

   /* ignore telescope positin data on end */
  if(!strncmp(h0->head.obsmode, "CONTMAP", 7))
  {
    dlen -= h0->head.noxpts;
  }

  for(i=0;i<dlen;i++)
  {
    if(!(i%8))
    {
      st_print("\n%5d: ", i);
    }

    st_print("%7.0f ", h0->yplot1[i]);
  }

  st_print("\n");

  return(0);
}



int pReview()
{
  int i, ret;
  char tbuf[256];
  struct HEADER *p;

  p = &sa[0];

  for(i=0;i<env->numofscans;i++,p++)
  {
    if(meetsCritiria(p, CHECK_ALL))
    {
      if(strncmp(p->obsmode, "LINEBSP",  7) && 
         strncmp(p->obsmode, "LINEPS",   6) && 
         strncmp(p->obsmode, "LINEFS",   6) && 
         strncmp(p->obsmode, "LINETPON", 8) && 
         strncmp(p->obsmode, "LINEAPS",  7))
      {
        continue; /* No good */
      }

      if(p->openpar[5] == 5555.5) /* a BSP focal */
      {
        continue; /* Ignore */
      }

      sprintf(tbuf, "%.2f", p->scan);

      /* quad only works with xxxx.01, xxxx.03, xxxx.05, xxxx.11 */
      if(strstr(tbuf, ".01") || strstr(tbuf, ".03") || strstr(tbuf, ".05") || strstr(tbuf, ".11"))
      {
        subArgs(2, tbuf);
        doPshow();

        ret = getYesNoQuit("Hit any key to continue");

        if(ret == 2) /* 'q' */
        {
          return(0);
        }
      }
    }
  }

  return(0);
}


int qReview()
{
  int i, ret;
  char tbuf[256];
  struct HEADER *p;

  p = &sa[0];

  for(i=0;i<env->numofscans;i++,p++)
  {
    if(meetsCritiria(p, CHECK_ALL))
    {
      if(strncmp(p->obsmode, "LINEBSP",  7) && 
	 strncmp(p->obsmode, "LINEPS",   6) && 
	 strncmp(p->obsmode, "LINEFS",   6) && 
	 strncmp(p->obsmode, "LINETOPN", 8) && 
	 strncmp(p->obsmode, "LINEAPS",  7))
      {
        continue; /* No good */
      }

      if(p->openpar[5] == 5555.5) /* a BSP focal */
      {
        continue; /* Ignore */
      }

      sprintf(tbuf, "%.2f", p->scan);

      /* quad only works with xxxx.01, xxxx.05, xxxx.11 */
      if(strstr(tbuf, ".01") || strstr(tbuf, ".05") || strstr(tbuf, ".11"))
      {
        subArgs(2, tbuf);
        doQshow();

        ret = getYesNoQuit("Hit any key to continue");

        if(ret == 2) /* 'q' */
        {
          return(0);
        }
      }
    }
  }

  return(0);
}


int reviewStack()
{
  int i, ret, ver;
  extern int doingOTF;
  char tbuf[256], cmdBuf[512], fname[256];
  struct HEADER *p;
  FILE *fp;

  /* Set the plotting to autoscale unless the /f arg is sent */
  if(notSet(PF_F_FLAG))
  {
    doAutoScale();
  }

  if(isSet(PF_I_FLAG)) /* review Ignored scans? */
  {
    reviewIgnored();

    return(0);
  }

  p = &sa[0];

  if(env->mode == MODE_CONT && isSet(PF_S_FLAG))
  {
    sprintf(fname, "sequence_splits.csv");

    st_report("Removing any previous '%s' file", fname);
    sprintf(cmdBuf, "rm -f %s", fname);
    system(cmdBuf);

    fp = fopen(fname, "a");

    if(fp)
    {
      fprintf(fp, "#\n");
      fprintf(fp, "# Scan,    Freq,     EG,     mbeam, mCnt,  mTmb,   pbeam, pCnt,  pTmb,  Ratio\n");
      fprintf(fp, "#\n");

      fclose(fp);
    }
  }

  clearInterrupt();

  for(i=0;i<env->numofscans;i++,p++)
  {
    if(meetsCritiria(p, CHECK_ALL))
    {
      if(env->mode == MODE_CONT)
      {
        if(strncmp(p->obsmode, "CONTSEQ", 7) && strncmp(p->obsmode, "CONTMAP", 7) && strncmp(p->obsmode, "CONTOPT", 7)) /* We only process Seq */
        {
          continue; /* No good */
        }
      }
      else
      if(env->mode == MODE_OTF)
      {
        if(strncmp(p->obsmode, "LINEOTF", 7)) /* We only process OTF */
        {
          continue; /* No good */
        }
      }
      else
      {                                      /* We only process BSP & PS for now*/
        if(strncmp(p->obsmode, "LINEBSP",  7) && 
           strncmp(p->obsmode, "LINEPS",   6) && 
           strncmp(p->obsmode, "LINEFS",   6) && 
           strncmp(p->obsmode, "LINETPON", 8) && 
           strncmp(p->obsmode, "LINEAPS",  7))
        {
          continue; /* No good */
        }

        if(p->openpar[5] == 5555.5) /* a BSP focal */
        {
          continue; /* Ignore */
        }
      }

      if(env->mode == MODE_OTF)
      {
        if(!doingOTF)
        {
          st_fail("Must start OTF process first");
	  return(1);
        }

        sprintf(tbuf, "scan_done %.0f", p->scan);
        to_dataserv(tbuf);

        if(isSet(PF_N_FLAG))
        {
  	  usleep(env->loopdelay2);
        }
        else
        {
          ret = getYesNoQuit("Ignore Scan %.2f: y/N ?", p->scan);
        }

        if(ret == 1) /* 'y' */
        {
          ver = atoi( env->filename + strlen(env->filename)-3 );

	  sprintf(tbuf, "ignore %d %.2f", ver, p->scan);

          to_dataserv(tbuf);
        }
        else
        if(ret == 2) /* 'q' */
        {
          return(0);
        }

	continue;
      }

      subArgs(2, "%.2f", p->scan);

      getScan(1);

      if(!strncmp(h0->head.obsmode, "CONTOPT", 7))
      {
        setFlag(PF_3_FLAG, 1);
      }

	/* if we are in cont mode, and the user requests a split, and it's NOT a seq */
      if(env->mode == MODE_CONT && isSet(PF_S_FLAG) && strncmp(h0->head.obsmode, "CONTSEQ", 7))
      {
        continue;
      }

      showScan();

	/* if we are in cont mode, and the user requests a split, and it's a seq */
      if(env->mode == MODE_CONT && isSet(PF_S_FLAG) && !strncmp(h0->head.obsmode, "CONTSEQ", 7))
      {
        if(isScanIgnored(p->scan) == 0)
        {
	  sprintf(cmdBuf, "data /s");
          parseCmd(cmdBuf);
        }
      }

      if(isSet(PF_N_FLAG) || (env->mode == MODE_CONT && isSet(PF_S_FLAG)))
      {
	usleep(env->loopdelay1);

        if(isInterrupt()) /* Check for Control-C input; any will cause reading to stop */
        {
          st_print("Control-C hit; Stoping\n");

          return(0);
        }
      }
      else
      {
        ret = getYesNoQuit("Ignore Scan %.2f: y/N ?", p->scan);

	st_report("Got %d", ret);
      }

      if(ret == 1) /* 'y' */
      {
        subArgs(2, "%.2f", p->scan);
        ignoreScans();
      }
      else
      if(ret == 2) /* 'q' */
      {
        return(0);
      }
      else
      if(ret == 3) /* 'b' 'back' */
      {
	if(i != 0)
	{
	  i--;
	  i--;
	  if(i != 0)
          {
	    p--;
	    p--;
	  }
	}
      }
    }
  }

  return(0);
}



int tellWeather()
{
  int i;
  int old_check;
  float old_feed;
  struct HEADER *p;

  old_check = env->check_feed;

    /* save any selected IF feed */
  if(env->check_feed)
  {
    old_feed = env->if_feed;
  }

  setFeed(1.0);
  env->feed = 0;

  env->check_feed = 1;

  p = &sa[0];

  st_print(
"\n  Source       Freq          Date      UT    Scan     El    Mode    Tsys Tcal  \
Tamb   %Hum   BP  Dewpt  mmH2O  tHum  Refrac  Tau0\n\n");

  for(i=0;i<env->numofscans;i++,p++)
  {
    if(meetsCritiria(p, CHECK_ALL))
    {
//      if(strncmp(p->obsmode, "CONTSEQ", 7)) /* We only process Seq */
//      {
//        continue; /* No good */
//      }

      subArgs(2, "%.2f", p->scan);

      getScan(0);

      st_print(
" %10.10s %10.6f %10.4f %6.2f %7.2f %5.1f %8.8s  %3.0f  %3.0f\
  %5.1f   %3.0f   %3.0f  %5.1f  %5.1f  %5.1f  %4.1f  %5.3f\n",
        p->object, p->restfreq, p->utdate, p->ut, p->scan, p->el, p->obsmode, p->stsys, p->tcal, 
        h0->head.tamb, h0->head.humidity, h0->head.pressure, h0->head.dewpt, h0->head.mmh2o, h0->head.tip_humid, h0->head.ref_correct, h0->head.tauh2o);
    }
  }

    /* restore any selected IF feed */
  if(old_check)
  {
    env->if_feed = old_feed;
  }
  else
  {
    env->if_feed = 0.0;
    env->check_feed = 0;
  }

  return(0);
}



int showScan()
{
  double fscan;

  if(h0->head.scan == 0.0)
  {
    st_warn("No scan to plot; Use the 'get or gget' command first");

    return(0);
  }

  env->curScan = fscan = h0->head.scan;

  if(env->mode == MODE_CONT && !strncmp(h0->head.obsmode, "CONT", 4))
  {
    if(!strncmp(h0->head.obsmode, "CONTSEQ ", 8))
    {
      st_report("Processing Continuum Sequence, Scan %.2f", fscan);
      cont_display_sequence(h0, PSPLOT);
    }
    else
    if(!strncmp(h0->head.obsmode, "CONTMAP ", 8))
    {
      st_report("Processing Continuum Az-El Grid, Scan %.2f", fscan);
      cont_display_mapscan(h0, PSPLOT);
    }
    else
    if(!strncmp(h0->head.obsmode, "CONTFIVE", 8))
    {
      st_report("Processing Continuum Five-Point, Scan %.2f", fscan);
      cont_display_five(h0, PSPLOT);
    }
    else
    if(!strncmp(h0->head.obsmode, "CONTQK5 ", 8))
    {
      st_report("Processing Continuum Five-Point, Scan %.2f", fscan);
      cont_display_five(h0, PSPLOT);
    }
    else
    if(!strncmp(h0->head.obsmode, "CONTFOC ", 8))
    {
      st_report("Processing Continuum Focus, Scan %.2f", fscan);
      cont_display_any_focus(h0, PSPLOT, FOCALIZE);
    }
    else
    if(!strncmp(h0->head.obsmode, "CONTNSFC", 8))
    {
      st_report("Processing Continuum NS-Focus, Scan %.2f", fscan);
      cont_display_any_focus(h0, PSPLOT, NS_FOCALIZE);
    }
    else
    if(!strncmp(h0->head.obsmode, "CONTEWFC", 8))
    {
      st_report("Processing Continuum EW-Focus, Scan %.2f", fscan);
      cont_display_any_focus(h0, PSPLOT, EW_FOCALIZE);
    }
    else
    if(!strncmp(h0->head.obsmode, "CONTSTIP", 8))
    {
      st_report("Processing Continuum SPTIP, Scan %.2f", fscan);
      cont_display_sptip(h0, PSPLOT);
    }
    else
    if(!strncmp(h0->head.obsmode, "CONTOPT ", 8))
    {
      st_report("Processing Continuum CONTOPT, Scan %.2f", fscan);
      cont_display_optical(h0, PSPLOT);
    }
    else
    {
      st_fail("Unknown Continuum Observing Mode %8.8s", h0->head.obsmode);
    }
  }
  else
  if(env->mode == MODE_SPEC && !strncmp(h0->head.obsmode, "LINE", 4))
  {
    if(!strncmp(h0->head.obsmode, "LINEAPS", 7))
    {
      st_report("Processing Line APS, Scan %.2f", fscan);
      line_display_spectra(h0, PSPLOT);
    }
    else
    if(!strncmp(h0->head.obsmode, "LINEAPM", 7))
    {
      st_report("Processing Line APM, Scan %.2f", fscan);
      line_display_spectra(h0, PSPLOT);
    }
    else
    if(!strncmp(h0->head.obsmode, "LINEBSP", 7))
    {
      st_report("Processing Line BSP, Scan %.2f", fscan);
      line_display_spectra(h0, PSPLOT);
    }
    else
    if(!strncmp(h0->head.obsmode, "LINEPS", 6))
    {
      st_report("Processing Line PS, Scan %.2f", fscan);
      line_display_spectra(h0, PSPLOT);
    }
    else
    if(!strncmp(h0->head.obsmode, "LINEFS", 6))
    {
      st_report("Processing Line FS, Scan %.2f", fscan);
      line_display_spectra(h0, PSPLOT);
    }
    else
    if(!strncmp(h0->head.obsmode, "LINEPCAL", 8))
    {
      st_report("Processing Line PCAL, Scan %.2f", fscan);
      line_display_spectra(h0, PSPLOT);
    }
    else
    if(!strncmp(h0->head.obsmode, "LINETPON", 8))
    {
      st_report("Processing Line TPON, Scan %.2f", fscan);
      line_display_spectra(h0, PSPLOT);
    }
    else
    if(!strncmp(h0->head.obsmode, "LINETPMF", 8))
    {
      st_report("Processing Line TPMF, Scan %.2f", fscan);
      line_display_spectra(h0, PSPLOT);
    }
    else
    {
      st_fail("Unknown Observing Mode %8.8s", h0->head.obsmode);
    }
  }
  else
  if(env->mode == MODE_OTF && !strncmp(h0->head.obsmode, "LINE", 4))
  {
    if(!strncmp(h0->head.obsmode, "LINEPCAL", 8))
    {
      st_report("Processing Line PCAL, Scan %.2f", fscan);
      line_display_spectra(h0, PSPLOT);
    }
    else
    if(!strncmp(h0->head.obsmode, "LINETPMF", 8))
    {
      st_report("Processing Line TPMF, Scan %.2f", fscan);
      line_display_spectra(h0, PSPLOT);
    }
    else
    if(!strncmp(h0->head.obsmode, "LINEOTF ", 8))
    {
      st_report("Processing Line OTF, Scan %.2f", fscan);
      line_display_spectra(h0, PSPLOT);
    }
  }

  return(0);
}



int doCal(d)
double d;
{
  int ret;
  double extra = 0.0, s;
  char tbuf[256];

  if(!strlen(env->gfilename))
  {
    st_fail("No gainsfile defined");

    return(1);
  }

  /* Go find the last scan in the gains file */
  ret = read_scan(-1.0, env->gfilename, h0->yplot1, &h0->head, 0);
  env->baselineremoved = 0;

  if(ret == -1)
  {
    st_fail("No gain scans found in %s", env->gfilename);

    return(0);
  }

  if(ret > 0)
  {
    if(isSet(PF_V_FLAG))
    {
      extra = 0.20;
      s = (double)ret+d+extra;

      st_report("Fetching Vane Scan: %f", s);
    }
    else
    if(isSet(PF_S_FLAG))
    {
      extra = 0.40;
      s = (double)ret+d+extra;

      st_report("Fetching Sky Scan: %f", s);
    }
    else
    if(isSet(PF_C_FLAG))
    {
      extra = 0.60;
      s = (double)ret+d+extra;

      st_report("Fetching Cold Scan: %f", s);
    }
    else
    {
      extra = 0.0;
      s = (double)ret+d+extra;

      st_report("Fetching Gain Scan: %f", s);
    }

    sprintf(tbuf, "gget %.2f; show", s);
    st_report(tbuf);

    parseCmd(tbuf);
  }
  else
  {
    st_fail("No gain scan found");

    return(1);
  }

  return(0);
}


int doCal1()
{
  return(doCal(0.01));
}

int doCal11()
{
  return(doCal(0.11));
}

int doCal2()
{
  return(doCal(0.02));
}

int doCal12()
{
  return(doCal(0.12));
}

int doCal3()
{
  return(doCal(0.03));
}

int doCal4()
{
  return(doCal(0.04));
}



int doQuick(d)
double d;
{
  int i;
  double last_scan  = 0.0;
  char tbuf[256];
  struct HEADER *p;

  p = &sa[0];

  /* find the last scan */
  for(i=0;i<env->numofscans;i++,p++)
  {
    if(isSet(PF_A_FLAG))
    {
      last_scan = round(p->scan);
    }
    else
    if(meetsCritiria(p, CHECK_ALL))
    {
      last_scan = round(p->scan);
    }
  }

  if(last_scan > 0.0)
  {
    sprintf(tbuf, "get %.2f; show", last_scan+d);
    st_report(tbuf);

    parseCmd(tbuf);
  }
  else
  {
    st_fail("No last scan found matching criteria");
    return(1);
  }

  return(0);
}


int doQuick1()
{
  return(doQuick(0.01));
}

int doQuick11()
{
  return(doQuick(0.11));
}

int doQuick2()
{
  return(doQuick(0.02));
}

int doQuick12()
{
  return(doQuick(0.12));
}

int doQuick3()
{
  return(doQuick(0.03));
}

int doQuick4()
{
  return(doQuick(0.04));
}
