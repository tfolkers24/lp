/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

/*  Changes:
 *
 *  Added nbox function: twf; March 01, 2016
 *  Added hanning function: twf; Jan 04, 2017
 *
 */

int doBoxcar()
{
  float tfloat[MAX_CHANS];
  struct LP_IMAGE_INFO *image = h0;

  /* If nbox != 0 then smooth the data */
  if(env->nbox > 0)
  {
    st_report("Performing boxcar(%d) on array %s", env->nbox, image->name);

    bcopy((char *)&image->yplot1[0], (char *)&tfloat[0], image->head.noint * sizeof(float));

    boxcar(&tfloat[0], &image->yplot1[0], image->head.noint, env->nbox);
  }
  else
  {
    st_fail("nbox <= 0");
    return(1);
  }

  return(0);
}


void boxcar(input, output, len, order)
float *input, *output;
int len, order;
{
  int i, j, left, right;
  float box;

  for(i=0;i<len;i++)
  {
    box = 0.0;
    left = ((i-order) < 0 ? 0 : i-order);
    right = ((i+order) >= len ? len : i+order);

    for (j=left; j<right; j++)
    {
      box += input[j];
    }
    output[i] = box / (float)(right-left);
  }

  return;
}


int doHanning()
{
  float tfloat[MAX_CHANS];
  struct LP_IMAGE_INFO *image = h0;

  st_report("Performing hanning on array %s", image->name);

  bcopy((char *)&image->yplot1[0], (char *)&tfloat[0], image->head.noint * sizeof(float));

  hanning(&tfloat[0], &image->yplot1[0], image->head.noint);

  st_report("Dropping 2 channels off both ends");

  env->bdrop += 2;
  env->edrop += 2;

  return(0);
}


int hanning(input, output, n)
float *input, *output;
int n;
{
  int    i, j, ib, ie, ic, nw;
  double sum, twt;
  float  wgt[4];

  wgt[0] = 0.25;
  wgt[1] = 0.50;
  wgt[2] = 0.25;
  nw     = 3;

  ib = 0;
  ie = n - (nw-1);
  ic = nw / 2;

  st_report("Performing Hanning from %d to %d", ib, ie);

  for(i=ib;i<ie;i++)
  {
    sum = 0.0;
    twt = 0.0;

    for(j=0;j<nw;j++)
    {
      sum += wgt[j] * input[i+j-1];
      twt += wgt[j];
    }

    if(twt > 0.0)
    {
      output[i+ic] = sum / twt;
    }
    else
    {
      output[i+ic] = GSL_POSINF;
    }
  }

  return(0);
}

