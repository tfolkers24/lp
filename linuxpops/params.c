/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

int resetParams()
{
  int mode;
  char execbuf[1024], tbuf[256], fbuf[256], gbuf[256];

  st_report("Reseting env %d parameters", envNumber);

  /* Save these */
  mode = env->mode;
  strcpy(tbuf, env->title);
  strcpy(fbuf, env->filename);
  strcpy(gbuf, env->gfilename);

  sprintf(args->secondArg, "%d", envNumber);
  clearEnv();

  clearBadChan();

  if(strlen(tbuf))
  {
    sprintf(execbuf, "title %s", tbuf);
    parseCmd(execbuf);
  }

  if(strlen(fbuf))
  {
    sprintf(execbuf, "file %s", fbuf);
    parseCmd(execbuf);
  }

  if(strlen(gbuf))
  {
    sprintf(execbuf, "gfile %s", gbuf);
    parseCmd(execbuf);
  }

  env->mode          =         mode;

  env->if_feed       =            0;
  env->bkmatch       =            0;
  env->mapbaselines  =            4;
  env->markers       =           16;
  env->calmode       = CALMODE_NONE;
  env->dobadchan     =            1;
  env->bscan         =          0.0;
  env->escan         =      50000.0;
  env->cal_scale     =          1.0;
  env->map_angle[0]  =        180.0;
  env->map_angle[1]  =         90.0;
  env->bm_eff[0]     =         0.88;
  env->bm_eff[1]     =         0.88;
  env->bm_eff[2]     =         0.88;
  env->bm_eff[3]     =         0.88;
  env->rejections[0] =         20.0;
  env->rejections[1] =         20.0;
  env->rejections[2] =         20.0;
  env->rejections[3] =         20.0;
  env->hwdc          =          1.0;
  env->glength       =           15;

  if(mode == MODE_CONT)
  {
    env->dfreq = 0.5;
  }
  else
  {
    env->dfreq = 0.001;
  }

  return(0);
}


int setFreqRes()
{
  double fr;

  fr = atof(args->secondArg);

  if(lp_fcmp(fr, env->bkres, 0.01))
  {
    clearBadChan();
  }

  env->bkres = fr;

  env->bkmatch = BKMATCH_RESOLUTION;

  return(0);
}

/* zenity --list --column="Freq" --column="Qty" --multiple --text="Select a Freq" \
   "230.538" 25 "220.500" 20 "115.270" 35
  */


int pickSourceFreq()
{
  int n, rev, ret, nscans;
  char rst[256], src[256], *cp;
  static char cmd[8*4096], tbuf[4*4096];
  double f;

  st_report("Starting Interactive Source/Freq Selection");

  rev = isSet(PF_R_FLAG);

  n = doSummaryOfFreqs2Buf(env->source, tbuf, rev);

  if(n)
  {
    sprintf(cmd, "--list --height=400 --width=400 --column='Source' --column='Freq' --column='Qty' --multiple --print-column='ALL' --text='Select a Source and Freq' %s", tbuf);

    if(isSet(PF_V_FLAG))
    {
      st_print("pickSourceFreq(): %s\n", cmd);
    }

    ret = doZenity(cmd, rst);

    if(strlen(rst) < 8) /* Cancelled */
    {
      return(1);
    }

    if(!ret)
    {
      if(isSet(PF_V_FLAG))
      {
        st_print("pickSourceFreq(): Got %s back from doZenity()\n", rst);
      }

      for(cp=rst;*cp;cp++)
      {
        if(*cp == '|')
        {
          *cp = ' ';
        }
      }

      strcmprs(rst);

      if(isSet(PF_V_FLAG))
      {
        st_print("pickSourceFreq(): Results is now: %s\n", rst);
      }

      n = sscanf(rst, "%s %lf %d", src, &f, &nscans);

      if(isSet(PF_V_FLAG))
      {
        st_print("pickSourceFreq(): Parsed %d tokens\n", n);
      }

      if(n == 3)
      {
        if(isSet(PF_V_FLAG))
        {
          st_print("pickSourceFreq(): Good Parse\n");
        }

        subArgs(2, src);
        args->ntokens = 2;
        setSource();

        subArgs(2, "%f", f);
        args->ntokens = 2;
        setFreq();
      }
      else
      {
        st_print("pickSourceFreq(): Bad Parse\n");
      }
    }
  }
  else
  {
    st_fail("pickSourceFreq(): No Source/Freq's in file");
  }

  return(0);
}

int setFreq()
{
  if(args->ntokens < 2)
  {
    pickSourceFreq();
  }
  else
  if(!strncasecmp(args->secondArg, "*", strlen(args->secondArg)))
  {
    env->check_freq = 0;
    env->freq = 0.0;
  }
  else
  {
    env->freq = atof(args->secondArg);
    env->check_freq = 1;
  }

  return(0);
}



int setDfreq()
{
  if(!strncasecmp(args->secondArg, "*", strlen(args->secondArg)))
  {
    if(env->mode == MODE_CONT)
    {
      env->dfreq = 0.5;
    }
    else
    {
      env->dfreq = 0.001;
    }
  }
  else
  {
    env->dfreq = atof(args->secondArg);
  }

  return(0);
}


int setTau0()
{
  if(!strncasecmp(args->secondArg, "*", strlen(args->secondArg)))
  {
    env->mantau0 = 0.0;
  }
  else
  {
    env->mantau0 = atof(args->secondArg);
  }

  return(0);
}


int setPTime()
{
  if(!strncasecmp(args->secondArg, "*", strlen(args->secondArg)))
  {
    env->ptime = 0.0;
  }
  else
  {
    env->ptime = atof(args->secondArg);
  }

  return(0);
}



int setDate()
{
  if(isSet(PF_A_FLAG))
  {
    env->date = h0->head.utdate;

    st_report("Setting date to %.4f from header", env->date);

    return(0);
  }

  if(strlen(args->secondArg))
  {
    if(!strncasecmp(args->secondArg, "*", strlen(args->secondArg)))
    {
      env->check_date = 0;
      env->date = 0.0;
    }
    else
    {
      env->date = atof(args->secondArg);
      env->check_date = 1;
      st_report("Setting date to %.4f", env->date);
    }
  }
  else
  {
    st_report("Date set to: %.4f", env->date);
  }

  return(0);
}


int setIFfeed()
{
  env->if_feed = atof(args->secondArg) / 100.0;
  env->bkmatch = BKMATCH_FEED;
  env->bkres   = 0.0;

  return(0);
}


int setScale()
{
  if(env->mode == MODE_SPEC)
  {
    line_setScale();

    return(0);
  }
  else
  if(!strncasecmp(args->secondArg, "*", strlen(args->secondArg)))
  {
    env->cal_scale = 1.0;
  }
  else
  {
    env->cal_scale = atof(args->secondArg);
  }

  return(0);
}


int setBscan()
{
  if(!strncasecmp(args->secondArg, "*", strlen(args->secondArg)))
  {
    env->check_scans = 0;
    env->bscan = 0.0;
  }
  else
  {
    env->bscan = atof(args->secondArg);
    env->check_scans = 1;
  }

  return(0);
}


int setEscan()
{
  if(!strncasecmp(args->secondArg, "*", strlen(args->secondArg)))
  {
    env->check_scans = 0;
    env->escan = 50000.0;
  }
  else
  {
    env->escan = atof(args->secondArg);
    env->check_scans = 1;
  }

  return(0);
}


int setSource()
{
  int i, nscans=0;
  char source[64];
  struct HEADER *p;

  /* TOMMIE: Not sure why I did this, but it messing up the beff procs */
//  env->bscan = 0;
//  env->escan = 50000;

  if(isSet(PF_A_FLAG))
  {
    strncpy(env->source, h0->head.object, 16);
    env->source[16] = '\0';

    strcmprs(env->source);

    st_report("Setting source to %s from header", env->source);

    return(0);
  }

  if(args->ntokens < 2)
  {
    pickSourceFreq();
  }
  else
  if(!strncasecmp(args->secondArg, "*", strlen(args->secondArg)))
  {
    env->check_source = 0;
    strcpy(env->source, " ");
  }
  else
  {
    strcpy(env->source, args->secondArg);
    env->check_source = 1;

    if(!env->numofscans) /* No file loaded yet */
    {
      st_warn("No Datafile Loaded yet to check for Source %s", env->source);
      return(0);
    }

    p = &sa[0];
    for(i=0;i<env->numofscans;i++,p++)
    {
      strncpy(source, p->object, 16);
      source[16] = '\0';
      strcmprs(source);

      if(!strcasecmp(source, env->source))
      {
        nscans++;
      }
    }

    if(nscans)
    {
      st_report("Source: %s, Found in %d times in %s", env->source, nscans, env->filename);
    }
    else
    {
      st_warn("Source: %s, Not Found in %s", env->source, env->filename);
    }
  }

  return(0);
}


int doBackEnd()
{
  if(strlen(args->secondArg))
  {
    strcpy(env->backend, args->secondArg);

    env->bkmatch   = BKMATCH_NAME;  /* Turn off feed checking */
    env->bkres     = 0.0;

    st_report("Setting Backend Name to %s", env->backend);
  }
  else
  {
    st_report("Backend Name: %s", env->backend);
  }

  return(0);
}

int doBkNameRes()
{
  double fr;

  if(args->ntokens == 3)
  {
    if(strlen(args->secondArg))
    {
      strcpy(env->backend, args->secondArg);


      st_report("Setting Backend Name to %s", env->backend);
    }

    if(strlen(args->thirdArg))
    {
      fr = atof(args->thirdArg);

      if(lp_fcmp(fr, env->bkres, 0.01))
      {
        clearBadChan();
      }

      env->bkres = fr;
      st_report("Setting Backend Resolution to %.2f", env->bkres);
    }

    env->bkmatch = BKMATCH_NAMERES;
  }
  else
  {
    printH_FlagHelp("bknameres");
  }

  return(0);
}




int setCalMode()
{
  int len;

  if((len = strlen(args->secondArg)))
  {
    if(!strncasecmp(args->secondArg, "none", 4))
    {
      env->calmode = CALMODE_NONE;
      st_report("Setting Cal Mode to: %s", calmodeStrs[env->calmode]);
    }
    else
    if(!strncasecmp(args->secondArg, "scale", 5))
    {
      env->calmode = CALMODE_CALSCALE;
      st_report("Setting Cal Mode to: %s", calmodeStrs[env->calmode]);
    }
    else
    if(!strncasecmp(args->secondArg, "beam", 4))
    {
      env->calmode = CALMODE_BEAMEFF;
      st_report("Setting Cal Mode to: %s", calmodeStrs[env->calmode]);
    }
    else
    if(!strncasecmp(args->secondArg, "eff", 3))
    {
      env->calmode = CALMODE_TELE_EFF;
      st_report("Setting Cal Mode to: %s", calmodeStrs[env->calmode]);
    }
    else
    {
      st_warn("Unknown Cal Mode %s", args->secondArg);
    }
  }
  else
  {
    st_report("Current Cal Mode: %s", calmodeStrs[env->calmode]);
  }

  return(0);
}


int setInvert()
{
  if(env->mode == MODE_CONT)
  {
    if(strlen(args->secondArg))
    {
      if(!strcmp(args->secondArg, "1"))
      {
        env->invert = 1;
        st_report("Setting Invert to Inverted");
      }
      else
      {
        env->invert = 0;
        st_report("Setting Invert to Normal");
      }
    }
    else
    {
      if(env->invert)
      {
        st_report("Invert is Inverted");
      }
      else
      {
        st_report("Invert is Normal");
      }
    }
  }
  else
  {
    doSpecInvert();
  }

  return(0);
}



int setSplit()
{
  if(strlen(args->secondArg))
  {
    if(!strcmp(args->secondArg, "1"))
    {
      env->split_phases = 1;
      st_report("Setting Split to Active");
    }
    else
    {
      env->split_phases = 0;
      st_report("Setting Split to Normal");
    }
  }
  else
  {
    if(env->split_phases)
    {
      st_report("Split is Active");
    }
    else
    {
      st_report("Split is Normal");
    }
  }

  return(0);
}


int showParams()
{
  st_report("");

  st_report("Environment Index:               %2d", envNumber);

  if(strlen(env->title) == 0)
  {
    st_report("No Title Set");
  }
  else
  {
    st_report("Title:     %24.24s", env->title);
  }

  st_report("Mode:                          %4.4s", modeStrs[env->mode]);

  if(strlen(env->filename) == 0)
  {
    st_report("No Datafile Selected");
  }
  else
  {
    st_report("Data File:         %16.16s", basename(env->filename));

    if(env->issmt)
    {
      st_report("Datafile is type:               SMT");
    }
    else
    {
      st_report("Datafile is type:               12M");
    }
  }

  if(env->mode != MODE_CONT) /* No gains for continuum */
  {
    if(strlen(env->gfilename) == 0)
    {
      st_report("No Gains Datafile Set");
    }
    else
    {
      st_report("Gains File:        %s", basename(env->gfilename));

      if(env->issmt)
      {
        st_report("Datafile is type:               SMT");
      }
      else
      {
        st_report("Datafile is type:               12M");
      }
    }
  }

  st_report("Data File Contains Scans:    %6d", env->numofscans);

  st_report("Beginning Scan #:          %8.2f", env->bscan);
  st_report("Ending   Scan #:           %8.2f", env->escan);

  if(env->check_freq)
  {
    st_report("Frequency to Analyze:    %10.6f", env->freq);
    st_report("Delta Frequency Tol:     %10.6f", env->dfreq);
  }
  else
  {
    st_report("Frequency to Analyze:           All");
  }

  if(env->check_source)
  {
    st_report("Source to Analyze:    %13.13s", env->source);
  }
  else
  {
    st_report("Source to Analyze:              All");
  }

  if(env->mode == MODE_CONT)
  {
    st_report("Calibration Scale:            %.3f", env->cal_scale);
    st_report("Calibration Mode:         %9.9s", calmodeStrs[env->calmode]);
  }

  if(env->check_date)
  {
    st_report("Show Scans Taken on:      %.4f", env->date);
  }
  else
  {
    st_report("Show scans taken on:            All");
  }

  if(env->tsyslimit > 0.0)
  {
    st_report("Show Scans /w Tsys < :      %7.1f", env->tsyslimit);
  }

  if(env->mode != MODE_CONT)
  {
    switch(env->bkmatch)
    {
      case BKMATCH_FEED:
        st_report("Backend Matching:               IF#");
        st_report("IF to Analyze:                 %.2f", env->if_feed);

        break;

      case BKMATCH_RESOLUTION:
        st_report("Backend Matching:        Resolution");
        st_report("Backend Resolution:          %6.3f", env->bkres);

        break;

      case BKMATCH_NAME:
        st_report("Backend Matching:              Name");
        st_report("Backend:   %24.24s", env->backend);

        break;

      case BKMATCH_NAMERES:
        st_report("Backend Matching:           NameRes");
        st_report("Backend:   %24.24s", env->backend);
        st_report("Backend Resolution:          %6.3f", env->bkres);

        break;
    }
  }

  if(env->mode == MODE_CONT)
  {
    if(env->invert)
    {
      st_report("Invert the Phases:         Inverted");
    }
    else
    {
      st_report("Invert the Phases:           Normal");
    }

    if(env->autotau)
    {
      st_report("Auto Application of SPTIP's:   True");
    }
    else
    {
      st_report("Auto Application of SPTIP's:  False");
    }

    if(env->ons)
    {
      st_report("Display 'Ons':                 True");
    }
    else
    {
      st_report("Display 'Ons':                False");
    }

    if(env->split_phases)
    {
      st_report("Split Phases:                Active");
    }
    else
    {
      st_report("Split Phases:                Normal");
    }

    st_report("Planet Flux time:             %5.2f", env->ptime);
    st_report("Current Manual Tau0:          %5.3f", env->mantau0);
    st_report("Last Fitted Tau0:             %5.3f", env->fittedtau);

    st_report("Map Baseline Samples:             %d", env->mapbaselines);
    st_report("Grid Map Angle:       %6.1f %6.1f", env->map_angle[0], env->map_angle[1]);
  }
  else
  {
    tellBaselines();
  }

  st_report("Beam Eff's:     %.2f %.2f %.2f %.2f", env->bm_eff[0], env->bm_eff[1],
                                                   env->bm_eff[2], env->bm_eff[3]);

  st_report("Current Rej:    %4.1f %4.1f %4.1f %4.1f", env->rejections[0], env->rejections[1],
                                                       env->rejections[2], env->rejections[3]);

  tellIgnored();

  if(env->mode == MODE_SPEC)
  {
    tellBadChan();
  }

  st_report("");

  return(0);
}



int params_writeParams(fp)
FILE *fp;
{
  int i;

  fprintf(fp, "switch %d\n", envNumber);

  if(strlen(env->filename))
  {
    fprintf(fp, "file %s\n", env->filename);
  }

  if(strlen(env->title))
  {
    fprintf(fp, "name %s\n", env->title);
  }

  fprintf(fp, "mode %4.4s\n", modeStrs[env->mode]);

  if(strlen(env->gfilename))
  {
    fprintf(fp, "gfile %s\n", env->gfilename);
  }

  fprintf(fp, "bscan %8.2f\n", env->bscan);
  fprintf(fp, "escan %8.2f\n", env->escan);

  if(env->check_freq)
  {
    fprintf(fp, "freq %10.6f\n", env->freq);
  }
  else
  {
    fprintf(fp, "freq -\n");
  }

  fprintf(fp, "delfreq %10.6f\n", env->dfreq);

  if(env->check_source)
  {
    fprintf(fp, "source %s\n", env->source);
  }
  else
  {
    fprintf(fp, "source -\n");
  }

  fprintf(fp, "scale %.3f\n", env->cal_scale);

  fprintf(fp, "calmode %s\n", calmodeStrs[env->calmode]);

  if(env->check_date)
  {
    fprintf(fp, "date %.4f\n", env->date);
  }
  else
  {
    fprintf(fp, "date -\n");
  }

  fprintf(fp, "invert  %d\n", env->invert);
  fprintf(fp, "split   %d\n", env->split_phases );
  fprintf(fp, "autotau %d\n", env->autotau);

  fprintf(fp, "ptime %.2f\n", env->ptime);
  fprintf(fp, "tau0  %.3f\n", env->mantau0);

  fprintf(fp, "beameff  %.3f %.3f %.3f %.3f\n", env->bm_eff[0], env->bm_eff[1],
                                                env->bm_eff[2], env->bm_eff[3]);

  fprintf(fp, "rejection %.3f %.3f %.3f %.3f\n",
                                env->rejections[0], env->rejections[1],
                                env->rejections[2], env->rejections[3]);

  fprintf(fp, "nbase  %d\n", env->mapbaselines);
  fprintf(fp, "mapangle %f %f\n", env->map_angle[0], env->map_angle[1]);

  switch(env->bkmatch)
  {
    case BKMATCH_FEED:
      fprintf(fp, "feed %d\n", (int)(env->if_feed * 100.0));
      break;

    case BKMATCH_RESOLUTION:
      fprintf(fp, "bkres %f\n", env->bkres);
      break;

    case BKMATCH_NAME:
      fprintf(fp, "backend %s\n", env->backend);
      break;

    case BKMATCH_NAMERES:
      fprintf(fp, "bknameres %s %f\n", env->backend, env->bkres);
      break;
  }

  /* Ignored Scans */
  fprintf(fp, "ignore ");
  for(i=0;i<MAX_IGNORED;i++)
  {
    if(env->ignoredscans[i])
    {
      fprintf(fp, "%.2f ", env->ignoredscans[i]);
    }
  }
  fprintf(fp, "\n");

  if(env->x2mode == XMODE_CHAN && env->x1mode == XMODE_CHAN)
  {
    fprintf(fp, "cc\n");
  }
  else
  if(env->x2mode == XMODE_CHAN && env->x1mode == XMODE_VEL)
  {
    fprintf(fp, "cf\n");
  }
  else
  if(env->x2mode == XMODE_CHAN && env->x1mode == XMODE_FREQ)
  {
    fprintf(fp, "cv\n");
  }
  else
  if(env->x2mode == XMODE_FREQ && env->x1mode == XMODE_CHAN)
  {
    fprintf(fp, "fc\n");
  }
  else
  if(env->x2mode == XMODE_FREQ && env->x1mode == XMODE_VEL)
  {
    fprintf(fp, "fv\n");
  }
  else
  if(env->x2mode == XMODE_FREQ && env->x1mode == XMODE_FREQ)
  {
    fprintf(fp, "ff\n");
  }
  else
  if(env->x2mode == XMODE_VEL  && env->x1mode == XMODE_CHAN)
  {
    fprintf(fp, "vc\n");
  }
  else
  if(env->x2mode == XMODE_VEL  && env->x1mode == XMODE_VEL)
  {
    fprintf(fp, "vv\n");
  }
  else
  if(env->x2mode == XMODE_VEL  && env->x1mode == XMODE_FREQ)
  {
    fprintf(fp, "vf\n");
  }

  if(env->plotstyle == PLOT_STYLE_HISTO)
  {
    fprintf(fp, "histo\n");
  }
  else
  if(env->plotstyle == PLOT_STYLE_LINES)
  {
    fprintf(fp, "lines\n");
  }
  else
  if(env->plotstyle == PLOT_STYLE_POINTS)
  {
    fprintf(fp, "points\n");
  }
  else
  if(env->plotstyle == PLOT_STYLE_LINEPTS)
  {
    fprintf(fp, "linepts\n");
  }

  fprintf(fp, "\n");

  return(0);
}



int setMode()
{
  int len;

  if((len = strlen(args->secondArg)))
  {
    if(!strncasecmp(args->secondArg, "cont", 4) || !strncmp(args->secondArg, "1", 1))
    {
      env->mode = MODE_CONT;
      st_report("Setting Mode to: %s", modeStrs[env->mode]);
    }
    else
    if(!strncasecmp(args->secondArg, "spec", 5) || !strncmp(args->secondArg, "2", 1))
    {
      env->mode = MODE_SPEC;
      st_report("Setting Mode to: %s", modeStrs[env->mode]);
    }
    else
    if(!strncasecmp(args->secondArg, "otf", 4) || !strncmp(args->secondArg, "3", 1))
    {
      env->mode = MODE_OTF;
      st_report("Setting Mode to: %s", modeStrs[env->mode]);
    }
    else
    {
      st_warn("Unknown Mode %s", args->secondArg);
    }
  }
  else
  {
    st_report("Current Mode: %s", modeStrs[env->mode]);
  }

  return( reloadFile() );
}
