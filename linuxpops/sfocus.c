/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

double last_focus_fit      = 0.0;
extern float *otfData;

/* Process a multi spectral line focus; assumes scan is already loaded */
int  specFocusOne(noint, nfoc, feed)
int noint, nfoc, feed;
{
  int    i, j, n, ret, delta, nchan, expectedPos, maxi, save_ps;
  int    start, end, npts, bchans;
  int    save_bdrop, save_edrop, save_x1, save_x2;
  char   cmdBuf[256];
  double focusp[MAX_SPEC_FOCUS], peaks[MAX_SPEC_FOCUS];
  double maxG, aa1[4], sig[MAX_SPEC_FOCUS];

  save_x1 = env->x1mode;
  save_x2 = env->x2mode;
  env->x1mode = 2;	/* Set to chan/chan */
  env->x2mode = 2;

  /* These routines assume the line is in the center; need to allow otherwise */
  if(env->sf_fitcenter != 0)
  {
    expectedPos = env->sf_fitcenter;
    st_report("Using User supplied Center Channel: %d", expectedPos);
  }
  else
  {
    expectedPos = noint / 2; /* halfway through bandpass??? */
  }

  if(env->sf_fitmhz == 0.0)
  {
    env->sf_fitmhz = 20.0;
    st_report("Using Line Width: %f", env->sf_fitmhz);
  }
  else
  {
    st_report("Using User supplied Line Width: %f", env->sf_fitmhz);
  }

  if(env->sf_bchans)
  {
    bchans = env->sf_bchans;
  }
  else
  {
    bchans = 40;
  }

  delta = (int)((env->sf_fitmhz / fabs(h0->head.freqres) ) / 4.0);

  st_report("specFocusOne(): noint = %d, nfoc = %d, expectedPos = %d, delta = %d", 
					noint, nfoc, expectedPos, delta);

  /* Automatically set baselines to 40 channels on the ends of each expected spectra */
  /* Zero out the current baselines */
  bzero((char *)&env->baselines[0], sizeof(env->baselines));

  env->baselines[0][0]   =       1;
  env->baselines[0][1]   =  bchans;
  for(i=1;i<nfoc;i++)
  {
    env->baselines[i][0] = (i * noint) - bchans;
    env->baselines[i][1] = (i * noint) + bchans;
  }

  env->baselines[nfoc][0] = h0->head.noint - bchans;
  env->baselines[nfoc][1] = h0->head.noint -      1;

  env->bmark = 1;

  if(isSet(PF_V_FLAG))
  {
    tellBaselines();
  }

  strcpy(cmdBuf, "show"); /* Replot with baselines visible */
  parseCmd(cmdBuf);
  xdelay(env->xdelay); 		/* Give plot time to show */

  env->markers    =  noint; 	/* Show breaks between spectra */
  env->mshow      =  1;		/* Markers show */
  env->bmark      =  1; 	/* Show Baseline regions */
  env->zline      =  1; 	/* Show Zero Line */
  env->bsmoothing =  1; 	/* poly baseline shape */

  if(!env->nfit)
  {
    env->nfit       =  1; 	/* linear baseline fitting */
  }

  /* Here is where I have to remove baselines from individual spectra.  Can't
     remove from the total of 7-9 spectra; that would suck. */

  /* 1 focus at a time;

     1) Set bdrop, edrop
     2) gracefit
     3) rmb
  */

  save_bdrop = env->bdrop;
  save_edrop = env->edrop;


  for(j=0;j<nfoc;j++)
  {
    env->bdrop = noint * j;
    env->edrop = noint * (nfoc-(j+1));

    if(isSet(PF_V_FLAG))
    {
      st_report("Bdrop: %4d, Edrop: %4d", env->bdrop, env->edrop);
    }

    strcpy(cmdBuf, "gracefit /x /q");
    parseCmd(cmdBuf);
  }

  env->bdrop = save_bdrop;
  env->edrop = save_edrop;

  strcpy(cmdBuf, "rmbase /x");  /* Remove the baselines */
  parseCmd(cmdBuf);
  xdelay(env->xdelay); 		/* Give plot time to show */

  strcpy(cmdBuf, "show"); 	/* Replot with baselines visible */
  parseCmd(cmdBuf);
  xdelay(env->xdelay); 		/* Give plot time to show */

  if(isSet(PF_P_FLAG))
  {
    sprintf(cmdBuf, "make /c /g /d sfocus_data.%.2f.png", h0->head.scan);
    parseCmd(cmdBuf);
  }

  maxG = 0.0;
  maxi = 0;

  npts = 0;

  if(env->sf_start)
  {
    start = env->sf_start;
  }
  else
  {
    start = 0;
  }

  if(env->sf_end)
  {
    end = env->sf_end;
  }
  else
  {
    end = nfoc;
  }

  for(i=start;i<end;i++)
  {
    nchan = expectedPos + (noint * i);

    peaks[i] = 0.0;
    n = 0;
    for(j=(nchan-delta);j<(nchan+delta);j++)  /* Average up the delta*2 channels about the center */
    {
      peaks[i] += h0->yplot1[j];

      n++;
    }

    focusp[i] = h0->head.focusr - (floor((double)nfoc / 2.0) * h0->head.wl) + (double)i * h0->head.wl;
    peaks[i] /= (double)n;

    if(peaks[i] > maxG)
    {
      maxG = peaks[i];
      maxi = i;
    }

    st_report("[%2d]: %f %f", i, focusp[i], peaks[i]);

    sig[i] = 1.0;

    npts++;
  }

  /* Steal the continuum focus structure so xgraphics will plot the results */
  clearFillContStruct(&h0->head, npts, "Focus", "Ta* (K)");

  st_report("Fitting a %d Point Gauss to Peak: %f, @ %f, HP = %f", npts, maxG, focusp[maxi], h0->head.wl * 6.0);
  aa1[0] = 0.0;
  aa1[1] = maxG;
  aa1[2] = h0->head.focusr;                     /* Center Focus */
  aa1[3] = h0->head.wl * 6.0;

  ret = gauss_fit(&focusp[start], &peaks[start], &sig[start], npts-1, aa1, isSet(PF_V_FLAG)); /* Fit a gaussian curve to  peaks */

  if(ret != 1)
  {
    st_report("gauss_fit() returned: %d", ret);
  }

  st_report("Gaussian Peak:   %7.2f K",  aa1[1]);
  st_report("Gaussian Center: %7.2f",    aa1[2]);
  st_report("Gaussian FWHM:   %7.2f mm", aa1[3]);

  last_focus_fit = aa1[2];

  for(i=0;i<npts;i++)
  {
    cont_plain_send.xdata[i] = (float)focusp[start+i];
    cont_plain_send.ydata[i] = (float)peaks[start+i];
  }

  if(feed == 0)
  {
    env->plotwin = 15;
    env->xstart  = 100;
    env->ystart  = 700;
  }
  else
  {
    env->plotwin = 16;
    env->xstart  = 906;
    env->ystart  = 700;
  }

  env->yscale    = Y_AUTO_SCALE;

  save_ps = env->plotstyle;
  env->plotstyle = PLOT_STYLE_LINEPTS;

  env->zline     = 0;

  if(xgraphic)
  {
    sendCont(SEND_PLAIN);
    xdelay(env->xdelay); 		/* Give plot time to show */
  }

  plotGauss(&focusp[start], npts, aa1, &h0->head, 1);
  xdelay(env->xdelay); /* Give plot time to show */

  env->plotstyle = save_ps;

  if(isSet(PF_P_FLAG))
  {
    sprintf(cmdBuf, "make /c /g /d sfocus_gauss.%.2f.png", h0->head.scan);
    parseCmd(cmdBuf);
  }

  env->x1mode = save_x1;	/* Restore axis labels */
  env->x2mode = save_x2;

  return(0);
}

/* SFocus analysis:

   The Spec Focus comes in as an array of bandpasses, each at a different
   focus position.

   openpar[6] contains the number of focus positions. Maximum of 15;

   Procedure:

   1) Fetch the scan;
   
      if the total number of float < MAX_CHANS place in yplot1[]

      else

      place data in otfData; (reduce data size)

   2) for(i=0;i<n;i++)
      {
	set bchan and echan to beginning and end of each segment	

	set baseline to be the first n chans and last n chans

	remove linear baseline;

	compute the 1st moment of the center chan +/- 10 Mhz

        save();
      }

    3) fit Gaussian of the results

    4) Repeat for second IF

*/

int doSfocus()
{
  int    i, j, ret, noint, nfoc;
  int    feed, interval;
  char   monBuf[256], cmdBuf[256];
  float  *fd, *fs;
  double scan, ratio;

  if(args->ntokens < 2)
  {
    printH_FlagHelp(args->firstArg);
  }

  if(strlen(args->secondArg))
  {
    scan = atof(args->secondArg);
  }
  else
  {
    st_fail("Missing Scan Number");
    printH_FlagHelp(args->firstArg);

    return(1);
  }

  saveEnv();

  env->focus_fit_1 = env->focus_fit_2 = 0.0;

  if(env->xdelay == 0)
  {
    env->xdelay = 250000;
  }

  for(i=0;i<2;i++)
  {
    /* Be advised that the data may come back in otfData if too large */
    ret = read_scan(scan, env->filename, h0->yplot1, &h0->head, 0);
    env->baselineremoved = 0;

    noint = (int)h0->head.noint;

    if(ret <= 0)
    {
      st_fail("doSfocus(): Scan %.2f, not found", scan);

      restoreEnv();

      return(1);
    }
	/* If this is attempted on AROWS data, which has 6400 channels
	   we will crash LP due to the read buffer being too small;  Much
	   like OTF, a seperate otfData is used. We need to reduce the data here */

	/* None of this code has been tested as we don't have AROWS SFOCUS data yet */
    if(h0->head.datalen > (double)(MAX_CHANS * sizeof(float)))
    {
      st_report("doSfocus(): Scan %.2f, Contains Large Channel Count: %d", scan, noint);

      ratio = h0->head.datalen / (double) sizeof(h0->yplot1);
      st_report("Smoothing Ratio: %f, %f", ratio, ceil(ratio));

      /* Make sure it's clear */
      bzero((char *)&h0->yplot1[0], sizeof(h0->yplot1));

      fd = &h0->yplot1[0];
      fs = &otfData[0];

	/* For now, just grab every interval data points */
      interval = (int)ceil(ratio);
      for(j=0;j<noint;j++,fd++)
      {
        *fd = *fs;

        fs += interval;
      }

      /* Correct the header */

      h0->head.noint   /= (double)interval;
      h0->head.datalen /= (double)interval;
      h0->head.bw      *= (double)interval;
    }

    nfoc  = (int)h0->head.openpar[6];

    if(i == 0)
    {
      env->plotwin = 10;
      env->xstart  = 100;
      env->ystart  = 0;
    }
    else
    {
      env->plotwin = 11;
      env->xstart  = 906;
      env->ystart  = 0;
    }

    sprintf(cmdBuf, "show /f");
    parseCmd(cmdBuf);

//    h0->head.noint = (double)noint;	/* Put noint back because 'show /f' broke it */

    xdelay(env->xdelay); /* Give plot time to show */

    specFocusOne(noint, nfoc, i);

    feed = getFeed(h0->head.scan);

    if((feed%2)) 				/* odd channel */
    {
      env->focus_fit_1 = last_focus_fit;
    }
    else 					/* even channel */
    {
      env->focus_fit_2 = last_focus_fit;
    }

    if(isSet(PF_2_FLAG))
    {
      scan += 0.02;
    }
    else
    {
      scan += 0.01;
    }
  }

  sprintf(monBuf, "DISPLAY FIT_AX01 %f FIT_AX02 %f FIT_AX0AVG %f", env->focus_fit_1, env->focus_fit_2, (env->focus_fit_1 + env->focus_fit_2) / 2.0);
  st_report("Results: %s", monBuf);

  if(isSet(PF_T_FLAG))
  {
    if(monitorSock)
    {
      st_report("To MONITOR: %s", monBuf);

      sock_send(monitorSock, monBuf);
    }
    else
    {
      st_fail("monitor sockect not defined");
    }
  }

  restoreEnv();

  return(0);
}

