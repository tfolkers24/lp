/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

#define MAX_PROTO_FILES 128
#define MAX_PROTO_FUNCS 128

struct PROTO_LIST {
	int  used;
	int  count;
	char file[256];
	char functions[MAX_PROTO_FUNCS][MAX_PROTO_FUNCS];
};

struct PROTO_LIST protos[MAX_PROTO_FILES];

int loadProto()
{
  int findx, rindx;
  char *cp, *cp1, *cp2, *string, inbuf[256], fname[256];
  FILE *fp;

//  cp = getenv("HOST");
//  if(cp)
//  {
//    if(strcmp(cp, "plas"))
//    {
//      st_report("Not loading proto.h");
//      return(1);
//    }
//  }

  cp = getenv("LINUXPOPS_HOME");
  if(cp)
  {
    sprintf(fname, "%s/linuxpops/proto.h", cp);
  }
  else
  {
    st_fail("loadProto(): LINUXPOPS_HOME not defined");
    return(1);
  }

  if((fp = fopen(fname, "r") ) <= (FILE *)0)
  {
    st_fail("loadProto(): Unable to open file %s", fname);
    return(1);
  }

  findx = -1;
  rindx = 0;

  while((string = fgets(inbuf, sizeof(inbuf), fp)) != NULL)
  {
    if(!strncmp(inbuf, "/*", 2))
    {
      findx++;
      cp = index(inbuf+3, ' ');
      if(cp)
      {
	*cp = '\0';
      }

      strcpy(protos[findx].file, inbuf+3);

      protos[findx].used = 1;

      rindx = 0;
    }
    else /* It's a functions name */
    {
      if(!strncmp(inbuf, "int ", 4)) /* Commands all return int's */
      {
        cp1 = index(inbuf, ' '); /* Find the first space */
        cp2 = index(inbuf, '('); /* Find the end of the function name */
        if(cp2)
        {
  	*cp2 = '\0';
        }

        strcpy(protos[findx].functions[rindx++], cp1+1);

        protos[findx].count = rindx;
      }
    }
  }

  st_report("loadProto(): Found %d source files", findx);

  fclose(fp);

  return(0);
}


/* Return the filename containing the function; Used for "codeview" */
int findFunction(func, file)
char *func, *file;
{
  int i, j;

  for(i=0;i<MAX_PROTO_FILES;i++)
  {
    for(j=0;j<protos[i].count; j++)
    {
      if(!strcmp(func, protos[i].functions[j]))
      {
        strcpy(file, protos[i].file);
        return(0);
      }
    }
  }

  st_fail("Function: %s, not found", func);

  return(1);
}

