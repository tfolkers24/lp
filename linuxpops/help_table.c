﻿/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

/* Dummie function so Gnuplot Help will work */
int gnuplot()
{
  printH_FlagHelp("gnuplot");

  return(0);
}
/* Dummie function so HotKeys Help will work */
int doHotKeys()
{
  printH_FlagHelp("hotkeys");

  return(0);
}

/* These arrays of strings are printed to stdout whenever the
   user types: 'command /h'

   There is no practical limit to the width of each line.

   There is a limit of MAX_HELP_STRINGS lines which is currently
   set to 64.

   Be sure to add a section here for any commands added to 'commands.c'

 */

struct HELP_H_FLAG help_h_flag[] = {

#include "a_help.h"
#include "b_help.h"
#include "c_help.h"
#include "d_help.h"
#include "e_help.h"
#include "f_help.h"
#include "g_help.h"
#include "h_help.h"
#include "i_help.h"
#include "j_help.h"
#include "k_help.h"
#include "l_help.h"
#include "m_help.h"
#include "n_help.h"
#include "o_help.h"
#include "p_help.h"
#include "q_help.h"
#include "r_help.h"
#include "s_help.h"
#include "t_help.h"
#include "u_help.h"
#include "v_help.h"
#include "w_help.h"
#include "x_help.h"
#include "y_help.h"
#include "z_help.h"

/* Misc Help */

  { "@", 
      {
	  "Read in a file of Commands",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: @cmd_file_name [options]",
	  "",
	  "where: cmd_file_name is a file containing valid LinuxPops commands",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

  { "?", 
      {
	  "Show Letter Help",
	  "",
	  "Mode: ALL",
	  "",
	  "Usage: [a-z]? [options]",
	  "",
	  "options are:",
	  "",
	  "/h  help",
	  "",
	  "See Also:",
	  "",
	  NULL
      } 
  },

};
