/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

static float sinEl[] = {2.6, 2.3, 2.0, 1.7, 1.4, 1.1, 1.4, 1.7, 2.0, 2.3, 2.6};
static float sqrarg;

int sptip_bin_data(ydata, dl, data, h)
float *ydata;
int dl;
float *data;
struct HEADER *h;
{
  int i,j;

  if(env->issmt)
  {
    for(i=0,j=0;i<dl;i+=2)
    {
      ydata[j] = (data[i] + data[i+1]) / 2.0;
      j++;
    }
  }
  else
  {
    if(h->nophase == 4)
    {
      for(i=0,j=0;i<dl;i+=4)
      {
        ydata[j] = (data[i] + data[i+1] + data[i+2] + data[i+3]) / 4.0;
        j++;
      }
    }
    else
    {
      for(i=0,j=0;i<dl;i+=2)
      {
        ydata[j] = (data[i] + data[i+1]) / 2.0;
        j++;
      }
    }
  }

  return(j);
}


int sptip_display_sptip(tau0, siga, sigb, data, h, plot, chan)
float *tau0, *siga, *sigb;
float *data;
struct HEADER *h;
int plot, chan;
{
  float  xdata[MAX_CONT_SAMPLES];
  float  ydata[MAX_CONT_SAMPLES];
  float    sig[MAX_CONT_SAMPLES];

  float a1,b1,siga1,sigb1,chi2_1,q1,
        a2,b2,siga2,sigb2,chi2_2,q2;

  int   i, j, k, dlen;
  float sinel[11], delta_airmass, upper_airmass, lower_airmass;

  dlen = (int)h->noint;

  zero_check("Sptip", dlen, data);

  dlen = sptip_bin_data(&ydata[0], dlen, data, h);

  j = 0;
  for(i=0;i<dlen;i+=2)            /* subtract vane-out from vane-in's */
  {
    ydata[j] = ydata[i] - ydata[i+1];
    j++;
  }

  dlen = j;

  if(dlen != 11)
  {
    printf("Problems: datalen != 11: %d\n", dlen);

    *tau0 = 0.0;
    *siga = 0.0;
    *sigb = 0.0;

    return(0);
  }

  for(i=0;i<dlen;i++)
  {
    xdata[i] = (float)(i) + 1.0;
    ydata[i] = log(ydata[i]);
    sig[i]   = 1.0;
  }


  if(sptip_min_max(&ydata[0], dlen, h->scan))
  {
    *tau0 = 0.0;
    *siga = 0.0;
    *sigb = 0.0;

    return(0);
  }
                  /* does the header contain the stop and start airmasses ??? */
                                                           /* if so, use them */
  if(h->sptip_start > 0.0 && h->sptip_stop > 0.0)
  {
    lower_airmass = 1.0 / sin(h->sptip_start * DEG_TO_RAD);
    upper_airmass = 1.0 / sin(h->sptip_stop  * DEG_TO_RAD);

    delta_airmass  = (upper_airmass - lower_airmass) / 5.0;

    sinel[0]  = sinel[10] = lower_airmass;
    sinel[1]  = sinel[ 9] = lower_airmass + delta_airmass * 1.0;
    sinel[2]  = sinel[ 8] = lower_airmass + delta_airmass * 2.0;
    sinel[3]  = sinel[ 7] = lower_airmass + delta_airmass * 3.0;
    sinel[4]  = sinel[ 6] = lower_airmass + delta_airmass * 4.0;
    sinel[5]  =             upper_airmass;

  }
  else                       /* if not, it's an old sptip and use the default */
  {
    for(k=0;k<11;k++)
    {
      sinel[k] = sinEl[k];
    }
  }

  sptip_lsqfit( sinel, &ydata[0], 0,  5, &sig[0], 0, &a1, &b1, &siga1, &sigb1, &chi2_1, &q1);
  sptip_lsqfit( sinel, &ydata[0], 5, 10, &sig[0], 0, &a2, &b2, &siga2, &sigb2, &chi2_2, &q2);

  a1 = (a1 + a2) /  2.0;
  b1 = (b1 + b2) / -2.0;

  *tau0 = b1;
  *siga = (siga1 + siga2) / 2.0;
  *sigb = (sigb1 + sigb2) / 2.0;

  if(!env->autotau)
  {
    st_report("%.2f: %5.4f  %f  %f", h->scan, *tau0, *siga, *sigb);
  }
  else
  {
    return(0);
  }

  if(chan == 0)
  {
    bzero((char *)&cont_sptip_send, sizeof(cont_sptip_send));
    bcopy((char *)h, (char *)&cont_sptip_send.head1, sizeof(struct HEADER));

    cont_sptip_send.issmt   = env->issmt;
    cont_sptip_send.ons     = env->ons;
    cont_sptip_send.plotwin = env->plotwin;
    cont_sptip_send.dlen    = dlen;

    for(i=0;i<dlen;i++)
    {
      cont_sptip_send.xdata[i]  = xdata[i];
      cont_sptip_send.ydata1[i] = ydata[i];
    }
  }
  else
  {
    bcopy((char *)h, (char *)&cont_sptip_send.head2, sizeof(struct HEADER));

    for(i=0;i<dlen;i++)
    {
      cont_sptip_send.ydata2[i] = ydata[i];
    }
  }

  cont_sptip_send.results[chan].tau0   = *tau0;
  cont_sptip_send.results[chan].sigmaA = *siga;
  cont_sptip_send.results[chan].sigmaB = *sigb;

  cont_sptip_send.results[chan].a = a1;
  cont_sptip_send.results[chan].b = b1;

  if(xgraphic && chan == 1)
  {
    sendCont(SEND_SPTIP);
    validate(SP_TIP);

    return(0);
  }

  validate(SP_TIP);

  return(1);
}


int cmd_doSpTip()
{
  int save;

  save = env->autotau;

  env->autotau = 0;

  doSpTip();

  env->autotau = save;

  return(0);
}


int doSpTip()
{
  int ret, i, j, n = 0;
  float tau0, siga, sigb, fscan;
  double t_total = 0.0;

  fscan = atof(args->secondArg);

  if(fscan == 0.0)
  {
    st_warn("No arg for SPTIP");
    return(1);
  }

  j = -1;

  fscan = round(fscan);

  st_report("Looking for all SPTIP scans starting with %.2f", fscan);

  for(i=0;i<4;i++)
  {
    fscan += 0.01;

    if(!env->autotau)
    {
      st_print("%s Looking for: %.4f ", prompt, fscan);
      fflush(stdout);
    }

    ret = read_scan(fscan, env->filename, h0->yplot1, &h0->head, 0);
    env->baselineremoved = 0;
    env->curScan = fscan;

    if(ret > 0)
    {
      st_print("Found\n");

      j++;  /* Found one */
      if(!strncmp(h0->head.obsmode, "CONTSTIP", 8))
      {
        if(j == 0)
        {
          if(!env->autotau)
          {
            st_report(" Scan     Tau0     Siga      Sigb");
          }
        }

        sptip_display_sptip(&tau0, &siga, &sigb, h0->yplot1, &h0->head, PSPLOT, j);

        t_total += tau0;
        n++;
      }
      else
      {
        st_warn("Scan %.2f is NOT an SPTIP", h0->head.scan);
        return(1);
      }
    }
  }

  if(n)
  {
    t_total /= (double)n;

    if(!env->autotau)
    {
      st_report("Average: %.4f", t_total);
    }

    env->fittedtau = t_total;

    if(!env->autotau)
    {
      if(getYesNoQuit("Use tau0 value of %.3f: y/N ?", env->fittedtau))
      {
        useTau();
      }
    }
    else
    {
      useTau();
    }
  }

  return(0);
}


int showTips()
{

  return(0);
}


int useTau()
{
  if(env->fittedtau != 0.0)
  {
    if(!env->autotau)
    {
      st_report("Using %.3f Maunal Tau", env->fittedtau);
    }
    else
    if(verboseLevel & VERBOSE_CALIBRATE)
    {
      st_report("DEBUG: Using Automatic tau0 = %.3f from scan %.0f",
                                                env->fittedtau, env->curScan);
    }

    env->mantau0 = env->fittedtau;
  }
  else
  {
    st_warn("No Current Tau to Use");
  }

  return(0);
}


int cont_display_sptip(image, plot)
struct LP_IMAGE_INFO *image;
int plot;
{
  printf("\nSPTIP Display Not Functional Yet\n\n");

  return(0);
}


#define SQR(a) (sqrarg=(a),sqrarg*sqrarg)

int sptip_lsqfit(x, y, bdata, edata, sig, mwt, a, b, siga, sigb, chi2, q)
float x[], y[], sig[], *a, *b, *siga, *sigb, *chi2, *q;
int bdata, edata, mwt;
{
  int i;
  float wt, t, sxoss, sx=0.0, sy=0.0,
        st2 = 0.0, ss, sigdat;

  *b = 0.0;

  if(mwt)
  {
    ss=0.0;

    for(i=bdata;i<edata;i++)
    {
      wt  =  1.0 / SQR(sig[i]);
      ss += wt;
      sx += x[i] * wt;
      sy += y[i] * wt;
    }
  }
  else
  {
    for(i=bdata;i<edata;i++)
    {
      sx += x[i];
      sy += y[i];
    }

    ss=edata-bdata;
  }

  sxoss=sx/ss;

  if(mwt)
  {
    for(i=bdata;i<edata;i++)
    {
      t=(x[i]-sxoss)/sig[i];
      st2 += t*t;
      *b += t*y[i]/sig[i];
    }
  }
  else
  {
    for(i=bdata;i<edata;i++)
    {
      t = x[i] - sxoss;
      st2 += t*t;
      *b  += t *y[i];
    }
  }

  *b /= st2;
  *a = (sy - sx * (*b)) / ss;

  *siga = sqrt((1.0 + sx*sx / (ss * st2)) / ss);
  *sigb = sqrt( 1.0 / st2);

  *chi2 = 0.0;

  if(mwt == 0)
  {
    for(i=bdata;i<edata;i++)
    {
      *chi2 += SQR(y[i]-(*a)-(*b)*x[i]);
    }

    *q=1.0;

    sigdat=sqrt((*chi2)/(edata-bdata-2));

    *siga *= sigdat;
    *sigb *= sigdat;
  }

  return(0);
}

#undef SQR

int sptip_min_max(ydata, dl, scan)
float *ydata;
int dl;
float scan;
{
  int i;

  if(ydata == NULL)
  {
    return(1);
  }
                                                   /* redetermine min and max */
  env->datamax = -9999999999.9;
  env->datamin = 9999999999.0;

  for(i=0;i<dl;i++)
  {
    if(ydata[i] > env->datamax)
    {
      env->datamax = ydata[i];
    }

    if(ydata[i] < env->datamin)
    {
      env->datamin = ydata[i];
    }
  }

  if(fabs(env->datamax - env->datamin) < DATASPREAD)
  {
    st_fail("Scan #%7.2f is all %.4f", scan, env->datamin);
    env->datamin -= 1.0;
    env->datamax += 1.0;

    return(1);
  }

  return(0);
}
