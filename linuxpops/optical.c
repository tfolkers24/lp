/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"


int optical_determine_min_max(ydata, dl, scan)
float *ydata;
int dl;
float scan;
{
  int i;

  if(ydata == NULL)
  {
    return(1);
  }

  env->peakChan = 0;
                                                   /* redetermine min and max */
  env->datamax = -9999999999.9;
  env->datamin = 9999999999.0;

  for(i=0;i<dl;i++)
  {
    if(ydata[i] > env->datamax)
    {
      env->datamax = ydata[i];
      env->peakChan = i;
    }

    if(ydata[i] < env->datamin)
    {
      env->datamin = ydata[i];
    }
  }

  if(fabs(env->datamax - env->datamin) < DATASPREAD)
  {
    st_print("Scan #%7.2f is all %.4f\n", scan, env->datamin);

    env->datamin -= 1.0;
    env->datamax += 1.0;

    return(1);
  }

  st_report("DataMin %f, dataMax %f", env->datamin, env->datamax);

  return(0);
}



int optical_show_phases(data, h, plot)
float *data;
struct HEADER *h;
int plot;
{
  float  ydata[MAX_CONT_SAMPLES], *yp;
  int    i, dlen, oldzline;

  dlen = (int)h->noint;
  
  zero_check("Phases", dlen, data);

  if(dlen > MAX_CONT_SAMPLES)
  {
    st_fail("Too many Continuum Samples: %d", dlen);
    return(1);
  }

  yp = &ydata[0];

  oldzline = env->zline; /* Save for later */

  if(isSet(PF_S_FLAG)) /* switched ? */
  {
    if(env->issmt)
    {
      smt_bin_and_cal_data( yp, dlen, SWITCHED, data, h);
    }
    else
    {
      kp_bin_and_cal_data( yp, dlen, SWITCHED, data, h);
    }

    dlen /= h->nophase;
  }
  else
  {
    env->zline = 0;

    for(i=0;i<dlen;i++)
    {
      ydata[i] = data[i];
    }
  }

  if(cont_determine_min_max(ydata, dlen, h->scan))
  {
    env->zline = oldzline;

    return(0);
  }

  if(isSet(PF_S_FLAG))
  {
    clearFillContStruct(h, dlen, "Sample Number", "Intensity");
  }
  else
  {
    clearFillContStruct(h, dlen, "Sample Number", "Counts");
  }

  for(i=0;i<dlen;i++)
  {
    cont_plain_send.xdata[i] = (float)(i) + 1.0;
    cont_plain_send.ydata[i] = ydata[i];
  }

  if(xgraphic)
  {
    sendCont(SEND_PLAIN);
  }

  env->zline = oldzline;

  validate(SEQUENCE);

  return(0);
}


int display_3d_optical(image)
struct LP_IMAGE_INFO *image;
{
  int    row, col, count=0;
  char  fname[256];
  double x, y, z, x1, x2, y1, y2;
  FILE  *fp;

  sprintf(fname, "/tmp/%s/opt-3d-plot.dem", session);

  if( (fp = fopen(fname, "w") ) <= (FILE *)0)
  {
    st_fail("display_3d_optical(): Unable to open '%s'", fname);
    return(1);
  }

  fprintf(fp, "reset\n");
  fprintf(fp, "set title \"%16.16s\"\n", image->head.object);

  fprintf(fp, "set xlabel \"Az Offset\"\n");
  fprintf(fp, "set ylabel \"El. Offset\"\n");
  fprintf(fp, "set zlabel \"Intensity\"\n");

  fprintf(fp, "#\n");
  fprintf(fp, "set border 4095\n");
  fprintf(fp, "set hidden3d\n");
  fprintf(fp, "#\n");

  x1 = -(image->head.noxpts / 2.0) * 1.5; /* replace with plate_scale */
  x2 =  (image->head.noxpts / 2.0) * 1.5; /* replace with plate_scale */
  y1 = -(image->head.noypts / 2.0) * 1.5; /* replace with plate_scale */
  y2 =  (image->head.noypts / 2.0) * 1.5; /* replace with plate_scale */

  fprintf(fp, "set xrange  [%f:%f]\n", x1, x2);
  fprintf(fp, "set yrange  [%f:%f]\n", y1, y2);
  fprintf(fp, "set cbrange [%f:%f]\n", env->datamin, env->datamax);
  fprintf(fp, "#\n");

  fprintf(fp, "set samples %d*%d\n", (int)image->head.noxpts, (int)image->head.noypts);
  fprintf(fp, "set isosamples 60\n");
  fprintf(fp, "set ticslevel 0.1\n");
  fprintf(fp, "set pm3d\n");
  fprintf(fp, "set palette color\n");
  fprintf(fp, "set palette model RGB\n");
  fprintf(fp, "set palette defined ( 0 \"blue\", 1 \"green\", 2 \"yellow\", 3 \"red\" )\n");
  fprintf(fp, "set palette gamma 1.0\n");
  fprintf(fp, "#\n");

  fprintf(fp, "set view 60.0, 30.0, 1.0, 1.0\n");
  fprintf(fp, "set terminal x11 background rgb \"black\" enhanced 1 size 1200, 850 position 800, 500\n");
  fprintf(fp, "set contour both\n");
  fprintf(fp, "set cntrparam levels incr %.2f,%.2f,%.2f\n", env->datamin, env->datamax / 11.0, env->datamax);
  fprintf(fp, "splot \"/tmp/%s/opt-3d-plot.dat\" every 1::0::1024 with lines\n", session);

  fclose(fp);

  /* Write the data */
  sprintf(fname, "/tmp/%s/opt-3d-plot.dat", session);

  if( (fp = fopen(fname, "w") ) <= (FILE *)0)
  {
    log_msg("display_3d_optical(): Unable to open '%s'", fname);
    return(1);
  }
  
  count = 0;
  for(row=0;row<(int)image->head.noypts;row++)                       /* Search all cells for valid data */
  {
    for(col=0;col<(int)image->head.noxpts;col++)
    {
      x = (-image->head.noxpts / 2.0 * 1.5) + (double)col * 1.5; /* Replace with plate_scale */
      y = (-image->head.noypts / 2.0 * 1.5) + (double)row * 1.5; /* Replace with plate_scale */
      z = image->yplot1[count];

      fprintf(fp, "%8.2f %8.2f %8.2f\n", x, y, z);

      count++;
    }

    fprintf(fp, " \n");
  }

  fclose(fp);

  st_report("display_3d_optical(); Wrote %d cells to %s", count, fname);

  sprintf(fname, "xgraphic load \"/tmp/%s/opt-3d-plot.dem\"", session);

  sock_send(xgraphic, fname);

  return(0);
}




int cont_display_optical(image, plot)
struct LP_IMAGE_INFO *image;
int plot;
{
  float  *yp;
  int    i, dlen;

  dlen = image->head.noint;

  clearFillContStruct(&image->head, dlen, "Sample Number", "Intensity");

  yp = &image->yplot1[0];
  for(i=0;i<dlen;i++,yp++)
  {
    cont_plain_send.xdata[i] = (float)(i) + 1.0;
    cont_plain_send.ydata[i] = *yp;
  }

  if(xgraphic)
  {
    sendCont(SEND_PLAIN);
  }

  if(isSet(PF_3_FLAG))
  {
    optical_determine_min_max(image->yplot1, dlen, image->head.scan);

    display_3d_optical(image);
  }

  validate(SEQUENCE);

  return(0);
}



/* Display a table of switched values */
int display_optical_table(data, h)
float *data;
struct HEADER *h;
{
  float *yp;
  int    i, dlen;

  yp = data;
  dlen = h->noint;

  for(i=0;i<dlen;i++,yp++)
  {
    if(!(i%10))
    {
      printf("\n%4d: ", i);
    }

    printf("%10.6f ", *yp);
  }

  printf("\n");

  return(0);
}
