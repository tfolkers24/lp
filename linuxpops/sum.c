/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

int doSum()
{
  int f, s, i, alreadyH0 = 0;
  float *f1, *f2, wt1, wt2;
  struct LP_IMAGE_INFO *first, *second;
  struct LP_IMAGE_INFO sfirst, ssecond;

  if(strlen(args->secondArg) && strlen(args->thirdArg))
  {
    f = atoi(args->secondArg+1);
    s = atoi(args->thirdArg+1);

    if((f >= 0 && f < MAX_IMAGES) && (s >=0 && s < MAX_IMAGES))
    {
      if(f == s)
      {
        st_fail("doSum(): First and Second Holders must be different");
        return(1);
      }

      first  = findImage(f);
      second = findImage(s);

      if(first->head.inttime && second->head.inttime)
      {
        bcopy((char *)first,  &sfirst,  sizeof(sfirst));
        bcopy((char *)second, &ssecond, sizeof(ssecond));

        if(!strcmp(first->name, "H0"))
        {
          alreadyH0 = 1;
        }

        if(!strcmp(second->name, "H0"))
        {
          alreadyH0 = 2;

          /* Swap them */
          first  = &ssecond;
          second = &sfirst;
        }

        /* Allow up to 60% of channel spacing */
        if(fabs(first->head.restfreq - second->head.restfreq) > fabs(first->head.freqres * 0.6))
        {
	  st_fail("Rest Freq's diff: %f > %f", fabs(first->head.restfreq-second->head.restfreq), 
					       fabs(first->head.freqres * 0.6));
          return(1);
        }

        if(fabs(first->head.freqres - second->head.freqres) > 0.01)
        {
	  st_fail("Backend Resolutions differ: %.3f %.3f", first->head.freqres, second->head.freqres);
          return(1);
        }

        if(first->head.noint != second->head.noint)
        {
	  st_fail("Number of Channels differ: %.0f %.0f", first->head.noint, second->head.noint);
          return(1);
        }

        if(strncmp(first->head.object, second->head.object, 16))
        {
	  st_fail("Source names differ: %.16s %.16s", first->head.object, second->head.object);
	  return(1);
        }

        st_report("Summing %s & %s", first->name, second->name);

        /* Copy 'first' into H0; maybe */

        if(alreadyH0 == 0) /* first & second args a NOT H0 */
        {
          _copyImage(first, &H0);
        }

        /* From now on the H0 contains the 'first' image */
        wt1 =      H0.head.inttime / (      H0.head.stsys *      H0.head.stsys );
        wt2 = second->head.inttime / ( second->head.stsys * second->head.stsys );

        f1 = H0.yplot1;
        f2 = second->yplot1;

        for(i=0;i<H0.head.noint;i++, f1++, f2++)
        {
          *f1 = (*f1 * wt1 + *f2 * wt2) / (wt1 + wt2);
        }

        H0.ainfo.escan   = second->ainfo.escan;
        H0.ainfo.scanct += second->ainfo.scanct;

        wt1 =  first->head.inttime;
        wt2 = second->head.inttime;

        H0.head.stsys = (first->head.stsys * wt1 + second->head.stsys * wt2) / (wt1 + wt2);
        H0.head.tcal  = (first->head.tcal  * wt1 + second->head.tcal  * wt2) / (wt1 + wt2);

        H0.head.inttime += second->head.inttime;
        H0.head.effint  += second->head.effint;

        _h0();  /* Point to H0 for replot */
      }
      else
      if(!first->head.inttime && second->head.inttime) /* This an the equivalent of averaging nothing with something; just copy s to f */
      {
        subArgs(2, "H1");
        subArgs(3, "H0");

        copyImage();

        return(0);
      }
      else
      {
        st_fail("doSum(): Unable to locate First or Second headers");

	return(1);
      }
    }
  }

  return(0);
}


/* Subtract one array from another */
int doImageDiff()
{
  int f, s, i, alreadyH0 = 0;
  float *f1, *f2;
  struct LP_IMAGE_INFO *first, *second;
  struct LP_IMAGE_INFO sfirst, ssecond;

  if(strlen(args->secondArg) && strlen(args->thirdArg))
  {
    f = atoi(args->secondArg+1);
    s = atoi(args->thirdArg+1);

    if((f >= 0 && f < MAX_IMAGES) && (s >=0 && s < MAX_IMAGES))
    {
      if(f == s)
      {
        st_fail("doImageDiff(): First and Second Holders must be different");
        return(1);
      }

      first  = findImage(f);
      second = findImage(s);

      if(first && second)
      {
        bcopy((char *)first,  &sfirst,  sizeof(sfirst));
        bcopy((char *)second, &ssecond, sizeof(ssecond));

        if(!strcmp(first->name, "H0"))
        {
          alreadyH0 = 1;
        }

        if(!strcmp(second->name, "H0"))
        {
          alreadyH0 = 2;

          /* Swap them */
          first  = &ssecond;
          second = &sfirst;
        }

        /* Allow up to 60% of channel spacing */
        if(fabs(first->head.restfreq - second->head.restfreq) > fabs(first->head.freqres * 0.6))
        {
	  st_fail("doImageDiff(): Rest Freq's diff: %f > %f", fabs(first->head.restfreq-second->head.restfreq), 
					       fabs(first->head.freqres * 0.6));
          return(1);
        }

        if(fabs(first->head.freqres - second->head.freqres) > 0.01)
        {
	  st_fail("doImageDiff(): Backend Resolutions differ: %.3f %.3f", first->head.freqres, second->head.freqres);
          return(1);
        }

        if(first->head.noint != second->head.noint)
        {
	  st_fail("doImageDiff(): Number of Channels differ: %.0f %.0f", first->head.noint, second->head.noint);
          return(1);
        }

        if(strncmp(first->head.object, second->head.object, 16))
        {
	  st_fail("doImageDiff(): Source names differ: %.16s %.16s", first->head.object, second->head.object);
	  return(1);
        }

        st_report("doImageDiff(): Diffing %s & %s", first->name, second->name);

        /* Copy 'first' into H0; maybe */

        if(alreadyH0 == 0) /* first & second args a NOT H0 */
        {
          _copyImage(first, &H0);
        }

        /* From now on the H0 contains the 'first' image */

        f1 = H0.yplot1;
        f2 = second->yplot1;

        for(i=0;i<H0.head.noint;i++, f1++, f2++)
        {
          *f1 = *f1 - *f2;
        }

        H0.ainfo.bscan   = H0.head.scan;
        H0.ainfo.escan   = second->head.scan;
        H0.ainfo.scanct  = 1;

        _h0();  /* Point to H0 for replot */
      }
      else
      {
        st_fail("doImageDiff(): Unable to locate First or Second headers");
      }
    }
  }

  return(0);
}


/* Divide one array into another */
int doImageDiv()
{
  int f, s, i, alreadyH0 = 0;
  float *f1, *f2;
  struct LP_IMAGE_INFO *first, *second;
  struct LP_IMAGE_INFO sfirst, ssecond;

  if(strlen(args->secondArg) && strlen(args->thirdArg))
  {
    f = atoi(args->secondArg+1);
    s = atoi(args->thirdArg+1);

    if((f >= 0 && f < MAX_IMAGES) && (s >=0 && s < MAX_IMAGES))
    {
      if(f == s)
      {
        st_fail("doImageDiv(): First and Second Holders must be different");
        return(1);
      }

      first  = findImage(f);
      second = findImage(s);

      if(first && second)
      {
        bcopy((char *)first,  &sfirst,  sizeof(sfirst));
        bcopy((char *)second, &ssecond, sizeof(ssecond));

        if(!strcmp(first->name, "H0"))
        {
          alreadyH0 = 1;
        }

        if(!strcmp(second->name, "H0"))
        {
          alreadyH0 = 2;

          /* Swap them */
          first  = &ssecond;
          second = &sfirst;
        }

        /* Allow up to 60% of channel spacing */
        if(fabs(first->head.restfreq - second->head.restfreq) > fabs(first->head.freqres * 0.6))
        {
	  st_fail("doImageDiv(): Rest Freq's diff: %f > %f", fabs(first->head.restfreq-second->head.restfreq), 
					       fabs(first->head.freqres * 0.6));
          return(1);
        }

        if(fabs(first->head.freqres - second->head.freqres) > 0.01)
        {
	  st_fail("doImageDiv(): Backend Resolutions differ: %.3f %.3f", first->head.freqres, second->head.freqres);
          return(1);
        }

        if(first->head.noint != second->head.noint)
        {
	  st_fail("doImageDiv(): Number of Channels differ: %.0f %.0f", first->head.noint, second->head.noint);
          return(1);
        }

        if(strncmp(first->head.object, second->head.object, 16))
        {
	  st_fail("doImageDiv(): Source names differ: %.16s %.16s", first->head.object, second->head.object);
	  return(1);
        }

        st_report("doImageDiv(): Dividing %s & %s", first->name, second->name);

        /* Copy 'first' into H0; maybe */

        if(alreadyH0 == 0) /* first & second args a NOT H0 */
        {
          _copyImage(first, &H0);
        }

        /* From now on the H0 contains the 'first' image */

        f1 = H0.yplot1;
        f2 = second->yplot1;

        for(i=0;i<H0.head.noint;i++, f1++, f2++)
        {
          if(*f2)
	  {
            *f1 = *f1 / *f2;
	  }
	  else
	  {
	    st_print("doImageDiv(): Channel %d has zero in the divisor\n", i);
	    *f2 = H0.head.tcal;
	  }
        }

        H0.ainfo.bscan   = H0.head.scan;
        H0.ainfo.escan   = second->head.scan;
        H0.ainfo.scanct  = 1;

        _h0();  /* Point to H0 for replot */
      }
      else
      {
        st_fail("doImageDiv(): Unable to locate First or Second headers");
      }
    }
  }

  return(0);
}


int doHalves()
{
  char cmdBuf[256];

  sprintf(cmdBuf, "image /s h0 h1");
  parseCmd(cmdBuf);

  doAutoScale();

  xx();

  return(0);
}
