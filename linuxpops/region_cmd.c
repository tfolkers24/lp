/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */

#include "extern.h"

double region_ra, region_dec, region_rsize, region_dsize;

char region_fname[256];

int regionCmd()
{
  char tbuf[512], dbuf[512];
  char results[256], input[256];

  if(isSet(PF_L_FLAG)) /* List */
  {
    listRegions();

    to_dataserv("otfregionlist");

    return(0);
  }

  if(isSet(PF_E_FLAG)) /* Erase */
  {
    remRegions();

    to_dataserv("otfregiondelete");

    return(0);
  }

  if(isSet(PF_R_FLAG)) /* Read */
  {
    if(env->otf_size_factor == 0.0)
    {
      env->otf_size_factor = 1.0;
    }

    if(args->ntokens > 1)	/* Use passed fname */
    {
      strcpy(region_fname, args->secondArg);
    }
    else			/* Else select it */
    {
      bzero(results, sizeof(results));

      sprintf(input, "--file-selection --title='Select a Region File' --file-filter='*.reg' --filename='.' 2> /dev/null");

      doZenity(input, results);

      if(strlen(results))
      {
        st_report("doZenity() returned: '%s'", results);
        strcmprs(results);
        st_report("doZenity() After strcmprs: '%s'", results);

        strcpy(region_fname, results);
      }
      else
      {
        return(0);
      }
    }

    if(strlen(region_fname) > 2)
    {

      sprintf(tbuf, "%s %f", region_fname, env->otf_size_factor);

      do_loadRegion(tbuf);

      if(isSet(PF_D_FLAG))
      {
        sprintf(dbuf, "otfregionload %s %f 0", region_fname, env->otf_size_factor);
        to_dataserv(dbuf);
      }

      if(isSet(PF_A_FLAG))
      {

        st_report("Auto Setting map extents to: RA: %f, Dec: %f, Map Width: %f, Map_Height: %f",
					region_ra, region_dec, region_rsize, region_dsize);

        if(region_ra != 0.0)
        {
	  env->otf_map_ra = region_ra;
        }

        if(region_dec != 0.0)
        {
	  env->otf_map_dec = region_dec;
        }

        if(region_rsize != 0.0)
        {
	  env->otf_map_xsize = region_rsize;
        }

        if(region_dsize != 0.0)
        {
	  env->otf_map_ysize = region_dsize;
        }
      }

      return(0);
    }
    else
    {
      printH_FlagHelp(args->firstArg);
      return(1);
    }
  }

  printH_FlagHelp(args->firstArg);

  return(0);
}
