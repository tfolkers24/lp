/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <gsl/gsl_bspline.h>
#include <gsl/gsl_multifit.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_statistics.h>

#include "extern.h"


int doBspline()
{
  struct LP_IMAGE_INFO *image = h0;

  image->pinfo.nspline = env->nspline;

  if(isSet(PF_S_FLAG))
  {
    st_report("Performing a bspline fit on baseline data using: nspline = %d", image->pinfo.nspline);
    bspline_smooth(image);
  }
  else
  {
    st_report("Performing a bspline fit on raw data using: nspline = %d", image->pinfo.nspline);
    bspline_fit(image);
  }

  return(0);
}


int bspline_fit(image)
struct LP_IMAGE_INFO *image;
{
  const size_t n = image->head.noint;
  const size_t ncoeffs = image->pinfo.nspline;
  const size_t nbreak = ncoeffs-2;
  size_t i, j;
  gsl_bspline_workspace *bw;
  gsl_vector *B;
  gsl_rng *r;
  gsl_vector *c, *w;
  gsl_vector *x, *y;
  gsl_matrix *X, *cov;
  gsl_multifit_linear_workspace *mw;
  double chisq, Rsq, dof, tss;

  if(n == 0 || ncoeffs < 4 || nbreak < 2)
  {
    st_fail("Incorrect nspline value: %d %d %d", n, ncoeffs, nbreak);
    return(1);
  }

  if(ncoeffs >= n)
  { 
    st_fail("ncoeffs(%d) > number of channels(%d)", ncoeffs, n);
    return(1);
  }

  gsl_rng_env_setup();

  r = gsl_rng_alloc(gsl_rng_default);

/* allocate a cubic bspline workspace (k = 4) */
  bw  = gsl_bspline_alloc(4, nbreak);
  B   = gsl_vector_alloc(ncoeffs);
  x   = gsl_vector_alloc(n);
  y   = gsl_vector_alloc(n);
  X   = gsl_matrix_alloc(n, ncoeffs);
  c   = gsl_vector_alloc(ncoeffs);
  w   = gsl_vector_alloc(n);
  cov = gsl_matrix_alloc(ncoeffs, ncoeffs);
  mw  = gsl_multifit_linear_alloc(n, ncoeffs);

/* this is the data to be fitted */
  for (i=0;i<n;++i)
  {
    double sigma;

    double xi = (double)i;
    double yi = image->yplot1[i];;

    sigma = 0.1 * yi;

    gsl_vector_set(x, i, xi);
    gsl_vector_set(y, i, yi);
    gsl_vector_set(w, i, 1.0 / (sigma * sigma));
  }

/* use uniform breakpoints on [0, 15] */
  gsl_bspline_knots_uniform(0.0, image->head.noint, bw);

/* construct the fit matrix X */
  for(i=0;i<n;++i)
  {
    double xi = gsl_vector_get(x, i);

/* compute B_j(xi) for all j */
    gsl_bspline_eval(xi, B, bw);

/* fill in row i of X */
    for(j=0;j<ncoeffs;++j)
    {
      double Bj = gsl_vector_get(B, j);

      gsl_matrix_set(X, i, j, Bj);
    }
  }

/* do the fit */
  gsl_multifit_wlinear(X, w, y, c, cov, &chisq, mw);

  dof = n - ncoeffs;

  tss = gsl_stats_wtss(w->data, 1, y->data, 1, y->size);

  Rsq = 1.0 - chisq / tss;

  if(isSet(PF_V_FLAG))
  {
    st_report("chisq/dof = %e, Rsq = %f", chisq / dof, Rsq);
  }

/* output the smoothed curve */
  {
    int i;
    double xi, yi, yerr;

    for(i=0;i<n;i++)
    {
      xi = (double)i;
      gsl_bspline_eval(xi, B, bw);
      gsl_multifit_linear_est(B, c, cov, &yi, &yerr);

      image->bplot[i] = yi; /* Stick results in the gauss fit array for plotting */
    }
  }

  image->pinfo.bplot = 1; /* Flag the xgrapher that there is data here */

  gsl_rng_free(r);
  gsl_bspline_free(bw);
  gsl_vector_free(B);
  gsl_vector_free(x);
  gsl_vector_free(y);
  gsl_matrix_free(X);
  gsl_vector_free(c);
  gsl_vector_free(w);
  gsl_matrix_free(cov);
  gsl_multifit_linear_free(mw);

  return(0);
}


int bspline_smooth(image)
struct LP_IMAGE_INFO *image;
{
  const size_t n = image->head.noint;
  const size_t ncoeffs = image->pinfo.nspline;
  const size_t nbreak = ncoeffs-2;
  size_t i, j;
  gsl_bspline_workspace *bw;
  gsl_vector *B;
  gsl_rng *r;
  gsl_vector *c, *w;
  gsl_vector *x, *y;
  gsl_matrix *X, *cov;
  gsl_multifit_linear_workspace *mw;
  float yplot[MAX_CHANS];
  double chisq, Rsq, dof, tss;

  if(n == 0 || ncoeffs < 4 || nbreak < 2)
  {
    st_fail("Incorrect nspline value: %d %d %d", n, ncoeffs, nbreak);
    return(1);
  }

  if(ncoeffs >= n)
  { 
    st_fail("ncoeffs(%d) > number of channels(%d)", ncoeffs, n);
    return(1);
  }

  if(image->pinfo.bplot == 0)
  {
    st_fail("No Baseline profile to smooth");
    return(1);
  }

  /* Copy the prviously computed bplot into the local array for now */
  bcopy((char *)&image->bplot[0], (char *)&yplot[0], sizeof(yplot));

  gsl_rng_env_setup();

  r = gsl_rng_alloc(gsl_rng_default);

/* allocate a cubic bspline workspace (k = 4) */
  bw  = gsl_bspline_alloc(4, nbreak);
  B   = gsl_vector_alloc(ncoeffs);
  x   = gsl_vector_alloc(n);
  y   = gsl_vector_alloc(n);
  X   = gsl_matrix_alloc(n, ncoeffs);
  c   = gsl_vector_alloc(ncoeffs);
  w   = gsl_vector_alloc(n);
  cov = gsl_matrix_alloc(ncoeffs, ncoeffs);
  mw  = gsl_multifit_linear_alloc(n, ncoeffs);

/* this is the data to be fitted' now smooth the bplot data */
  for (i=0;i<n;++i)
  {
    double sigma;

    double xi = (double)i;
    double yi = yplot[i];;

    sigma = 0.1 * yi;

    gsl_vector_set(x, i, xi);
    gsl_vector_set(y, i, yi);
    gsl_vector_set(w, i, 1.0 / (sigma * sigma));
  }

/* use uniform breakpoints on [0, 15] */
  gsl_bspline_knots_uniform(0.0, image->head.noint, bw);

/* construct the fit matrix X */
  for(i=0;i<n;++i)
  {
    double xi = gsl_vector_get(x, i);

/* compute B_j(xi) for all j */
    gsl_bspline_eval(xi, B, bw);

/* fill in row i of X */
    for(j=0;j<ncoeffs;++j)
    {
      double Bj = gsl_vector_get(B, j);

      gsl_matrix_set(X, i, j, Bj);
    }
  }

/* do the fit */
  gsl_multifit_wlinear(X, w, y, c, cov, &chisq, mw);

  dof = n - ncoeffs;

  tss = gsl_stats_wtss(w->data, 1, y->data, 1, y->size);

  Rsq = 1.0 - chisq / tss;

  if(isSet(PF_V_FLAG))
  {
    st_report("chisq/dof = %e, Rsq = %f", chisq / dof, Rsq);
  }

/* output the smoothed curve */
  {
    int i;
    double xi, yi, yerr;

    for(i=0;i<n;i++)
    {
      xi = (double)i;
      gsl_bspline_eval(xi, B, bw);
      gsl_multifit_linear_est(B, c, cov, &yi, &yerr);

      image->bplot[i] = yi; /* Stick results in the gauss fit array for plotting */
    }
  }

  image->pinfo.bplot = 1; /* Flag the xgrapher that there is data here */

  gsl_rng_free(r);
  gsl_bspline_free(bw);
  gsl_vector_free(B);
  gsl_vector_free(x);
  gsl_vector_free(y);
  gsl_matrix_free(X);
  gsl_vector_free(c);
  gsl_vector_free(w);
  gsl_matrix_free(cov);
  gsl_multifit_linear_free(mw);

  return(0);
}
