/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

struct STRINGS_AND_THINGS sat;

char sat_tok[MAX_TOKENS][MAX_TOKENS];

int sat_get_tok(edit)
char *edit;
{
  int c = 0;
  char *token;

  bzero((char *)&sat_tok, sizeof(sat_tok));

  token = strtok(edit, " ,\t\r\n");
  while(token != (char *)NULL)
  {
    sprintf(sat_tok[c++], "%s", token);
    token = strtok((char *)NULL, " ,\t\r\n");
  }

  return(c);
}


int parseColor()
{
  int color = ANSI_BLACK;

  if(isSet(PF_Crd_FLAG))
  {
    color = ANSI_RED;
  }
  else
  if(isSet(PF_Cgr_FLAG))
  {
    color = ANSI_GREEN;
  }
  else
  if(isSet(PF_Cyl_FLAG))
  {
    color = ANSI_YELLOW;
  }
  else
  if(isSet(PF_Cbl_FLAG))
  {
    color = ANSI_BLUE;
  }
  else
  if(isSet(PF_Cmg_FLAG))
  {
    color = ANSI_MAGENTA;
  }
  else
  if(isSet(PF_Ccy_FLAG))
  {
    color = ANSI_CYAN;
  }
  else
  if(isSet(PF_Cwt_FLAG))
  {
    color = ANSI_WHITE;
  }

  return(color);
}


int sendSat()
{
  if(xgraphic)
  {
    st_report("Sending SAT struct");

    strcpy(sat.magic, "STRINGS");

    sock_write(xgraphic, (char *)&sat, sizeof(sat));
  }
  else
  {
    st_fail("No xgraphic defined");
    return(1);
  }

  return(0);
}


int clearSAT()
{
  st_report("Clearing out Strings And Things");

  bzero((char *)&sat, sizeof(sat));

  env->usesat = 0;

  return(0);
}


int listSAT()
{
  st_report("Lables:");
  labelList(NULL);
  st_report("");

  st_report("Forks:");
  forkList(NULL);
  st_report("");

  st_report("Markers:");
  markerList(NULL);
  st_report("");

  st_report("Hlines:");
  hlineList(NULL);
  st_report("");

  st_report("Postage Stamps:");
  pstampList(NULL);
  st_report("");

  return(0);
}


int doSAT()
{
  if(isSet(PF_C_FLAG)) /* Clear All Strings And Things */
  {
    clearSAT();

    return(0);
  }

  if(isSet(PF_L_FLAG)) /* List All Strings And Things */
  {
    listSAT();

    return(0);
  }

  if(isSet(PF_F_FLAG)) /* Turn OFF Strings And Things */
  {
    env->usesat = 0;

    return(0);
  }

  if(isSet(PF_O_FLAG)) /* Turn ON Strings And Things */
  {
    env->usesat = 1;

    return(0);
  }

  return(0);
}


/********************************* Labels ****************************/

int labelList(fname)
char *fname;
{
  int i;
  FILE *fp;

  if(fname)
  {
    if((fp = fopen(fname, "w")) == NULL)
    {
      st_fail("Unable to open %s", fname);
      return(1);
    }

    st_report("Saving Labels to %s", fname);
  }
  else
  {
    fp = stdout;
  }

  fprintf(fp, "Number  Used    Label_x    Label_y    Color   Direct  Size  Text\n");

  for(i=0;i<MAX_IMAGE_LABELS;i++)
  {
    if(sat.labels[i].used)
    {
      fprintf(fp, " %2d      %2d    %9.3f  %9.3f      %2d       %2d    %d   %s\n", 
			i,
			sat.labels[i].used,
			sat.labels[i].x,
			sat.labels[i].y,
			sat.labels[i].color,
			sat.labels[i].direct,
			sat.labels[i].size,
			sat.labels[i].label);
    }
  }

  if(fname)
  {
    fclose(fp);
  }

  return(0);
}


int getLabelSize(s)
int s;
{
  int size = 12;

  if(s == 1 || isSet(PF_1_FLAG))
  {
    size -= 8;
  }
  else
  if(s == 2 || isSet(PF_2_FLAG))
  {
    size -= 6;
  }
  else
  if(s == 3 || isSet(PF_3_FLAG))
  {
    size -= 4;
  }
  else
  if(s == 4 || isSet(PF_4_FLAG))
  {
    size -= 2;
  }
  else
  if(s == 5 || isSet(PF_5_FLAG))
  {
    ;
  }
  else
  if(s == 6 || isSet(PF_6_FLAG))
  {
    size += 2;
  }
  else
  if(s == 7 || isSet(PF_7_FLAG))
  {
    size += 4;
  }
  else
  if(s == 8 || isSet(PF_8_FLAG))
  {
    size += 6;
  }
  else
  if(s == 9 || isSet(PF_9_FLAG))
  {
    size += 8;
  }

  return(size);
}


int deQuote(buf)
char *buf;
{
  char *cp;

  for(cp=buf;*cp;cp++)
  {
    if(*cp == '"')
    {
       *cp = ' ';
    }
    else
    if(*cp == '!')
    {
       *cp = ' ';
    }
  }

  return(0);
}



int doLabel()
{
  int i, j, k, n, size, sizeSet, color = ANSI_BLACK, direct = LABEL_CENTER;
  char tbuf[512], newlabel[256], fname[256], *string, line[256];
  char ttbuf[512], bbuf[256], *cp;
  struct IMAGE_LABELS *l;
  FILE *fp;

  n = args->ntokens;

  if(isSet(PF_C_FLAG))
  {
    st_report("Clearing out all Labels");

    bzero((char *)&sat.labels, sizeof(sat.labels));

    sendSat(); /* Send update to xgraphics */

    return(0);
  }

  if(isSet(PF_L_FLAG))
  {
    labelList(NULL);

    return(0);
  }

  if(isSet(PF_P_FLAG))
  {
    st_report("Displaying Postscript Label Help");

    cp = getenv("LINUXPOPS_HOME");

    if(cp)
    {
      sprintf(tbuf, "/usr/bin/evince %s/docs/ps_guide.pdf &", cp);

      system(tbuf);
    }

    return(0);
  }

  if(isSet(PF_S_FLAG))
  {
    if(n > 1)
    {
      strcpy(fname, args->secondArg);

      labelList(fname);
    }
    else
    {
      printH_FlagHelp("label");

      return(1);
    }

    return(0);
  }

  if(isSet(PF_R_FLAG))
  {
    if(n > 1)
    {
      strcpy(fname, args->secondArg);

      if((fp = fopen(fname, "r")) != NULL)
      {
        st_report("Clearing out all Labels");
        bzero((char *)&sat.labels, sizeof(sat.labels));
        st_report("Loading Labels from: %s", fname);

        while((string = fgets(line, sizeof(line), fp)) != NULL)
        {
          if(line[0] == '#')
          {
            continue;
          }

	  n = sat_get_tok(line);

          if(n > 6)
          {
	    j = atoi(sat_tok[0]);

            if(j<MAX_IMAGE_LABELS)
            {
	      sat.labels[j].used   = atoi(sat_tok[1]);
	      sat.labels[j].x      = atof(sat_tok[2]);
	      sat.labels[j].y      = atof(sat_tok[3]);
	      sat.labels[j].color  = atoi(sat_tok[4]);
	      sat.labels[j].direct = atoi(sat_tok[5]);
	      sat.labels[j].size   = atoi(sat_tok[6]);

		/* Check is the text already has size info */
	      sizeSet = 0;
              for(k=7;k<n;k++)
	      {
	        if(strstr(sat_tok[k], "Helvet"))
		{
		  sizeSet = 1;
		}
	      }

	      if(sizeSet) /* Just assemble them */
	      {
	        bbuf[0] = '\0';
                for(k=7;k<n;k++)
                {
                  sprintf(ttbuf, "%s ", sat_tok[k]);
                  strcat(bbuf, ttbuf);
                }
	      }
	      else /* Add size info */
	      {
		size = getLabelSize(sat.labels[j].size);
      		sprintf(bbuf, "{/Helvetica=%d ", size);

                for(k=7;k<n;k++)
                {
                  sprintf(ttbuf, "%s ", sat_tok[k]);
                  strcat(bbuf, ttbuf);
                }

        	strcat(bbuf, "}");
	      }

              strcmprs(bbuf);

              strcpy(sat.labels[j].label, bbuf);
            }
          }
	}

        fclose(fp);
  
        labelList(NULL);

	env->usesat = 1; /* Set flag so plotter will use */

        sendSat(); /* Send update to xgraphics */

        return(0); /* Good return */
      }
      else
      {
        st_fail("Unable to open: %s", fname);
	return(1);
      }
    }
    else
    {
      printH_FlagHelp("label");

      return(1);
    }

    return(0);
  }

  /* From now on you need more args */
  if(n < 2)
  {
    printH_FlagHelp("label");

    return(1);
  }

  if(!strncmp(args->secondArg, "create", 3))
  {
    if(n > 3)
    {
      i = atoi(args->thirdArg);

      if(i >=0 && i<MAX_IMAGE_LABELS)
      {
        l = &sat.labels[i];

	l->size = getLabelSize(0);

        sprintf(newlabel, "{/Helvetica=%d ", l->size);
        for(j=3;j<n;j++)
        {
          sprintf(tbuf, "%s ", args->tok[j]);
          strcat(newlabel, tbuf);
        }
        strcat(newlabel, "}");

	deQuote(newlabel);
        strcmprs(newlabel); /* Remove excess spaces */

        if(h0->pinfo.x1mode == XMODE_CHAN)
        {
          _doCCur("Mouse-Click plot to locate label");
          l->x = env->ccur;
        }
        else
        if(h0->pinfo.x1mode == XMODE_VEL)
        {
          _doVCur("Mouse-Click plot to locate label");
          l->x = env->vcur;
        }
        else
        if(h0->pinfo.x1mode == XMODE_FREQ)
        {
          _doFCur("Mouse-Click plot to locate label");
          l->x = env->fcur;
        }

        color = parseColor();

        if(isSet(PF_E_FLAG)) /* East or Right */
	{
	  direct = LABEL_RIGHT;
	}
	else
        if(isSet(PF_W_FLAG)) /* West or Left */
	{
	  direct = LABEL_LEFT;
	}
	else
        if(isSet(PF_D_FLAG)) /* Down */
	{
	  direct = LABEL_DOWN;
	}
	else
	{
	  direct = LABEL_CENTER;
	}

        if(isSet(PF_N_FLAG)) /* No Head */
        {
	  l->nohead = TRUE;
        }
        else
        {
	  l->nohead = FALSE;
        }

        l->used   = 1;
        l->y      = env->tcur;
        l->color  = color;
	l->direct = direct;
        strcpy(l->label, newlabel);

	env->usesat = 1; /* Set flag so plotter will use */

        sendSat(); /* Send to xgraphics */
      }
    }
    else
    {
      printH_FlagHelp("label");

      return(1);
    }
  }
  else
  if(!strncmp(args->secondArg, "move", 3))
  {
    if(n == 3)
    {
      i = atoi(args->thirdArg);

      if(i >=0 && i<MAX_IMAGE_LABELS)
      {
        l = &sat.labels[i];

        if(h0->pinfo.x1mode == XMODE_CHAN)
        {
          _doCCur("Mouse-Click plot to locate label");
          l->x = env->ccur;
        }
        else
        if(h0->pinfo.x1mode == XMODE_VEL)
        {
          _doVCur("Mouse-Click plot to locate label");
          l->x = env->vcur;
        }
        else
        if(h0->pinfo.x1mode == XMODE_FREQ)
        {
          _doFCur("Mouse-Click plot to locate label");
          l->x = env->fcur;
        }

	color = parseColor();

        if(isSet(PF_E_FLAG)) /* East or Right */
	{
	  direct = LABEL_RIGHT;
	}
	else
        if(isSet(PF_W_FLAG)) /* West or Left */
	{
	  direct = LABEL_LEFT;
	}
	else
        if(isSet(PF_D_FLAG)) /* Down */
	{
	  direct = LABEL_DOWN;
	}
	else
	{
	  direct = l->direct; /* Leave it alone */
	}

        l->used   = 1;
        l->y      = env->tcur;
        l->color  = color;
	l->direct = direct;

	env->usesat = 1; /* Set flag so plotter will use */

        sendSat(); /* Send to xgraphics */
      }
    }
    else
    {
      printH_FlagHelp("label");

      return(1);
    }

    return(0);
  }
  else
  if(!strncmp(args->secondArg, "delete", 3))
  {
    if(n == 3)
    {
      i = atoi(args->thirdArg);

      if(i >=0 && i<MAX_IMAGE_LABELS)
      {
        st_report("Removing label #%d", i);
        sat.labels[i].used     = 0;
	sat.labels[i].x        = 0.0;
	sat.labels[i].y        = 0.0;
       	sat.labels[i].color    = 0;
	sat.labels[i].label[0] = '\0';

        sendSat(); /* Send update to xgraphics */
      }
    }
    else
    {
      printH_FlagHelp("label");

      return(1);
    }

    return(0);
  }
  else
  if(!strncmp(args->secondArg, "set", 3))
  {
    if(n > 6)
    {
      j = atoi(args->tok[2]);

      l = &sat.labels[j];

      l->used   = 1;
      l->nohead = 1;
      l->x      = atof(args->tok[3]);
      l->y      = atof(args->tok[4]);
      l->color  = atoi(args->tok[5]);
      l->direct = atoi(args->tok[6]);
      l->size = getLabelSize(0);

      sprintf(bbuf, "{/Helvetica=%d ", l->size);

      for(k=7;k<n;k++)
      {
        sprintf(ttbuf, "%s ", args->tok[k]);
        strcat(bbuf, ttbuf);
      }

      strcat(bbuf, "}");

      strcmprs(bbuf);

      strcpy(l->label, bbuf);
    }
    env->usesat = 1; /* Set flag so plotter will use */

    sendSat(); /* Send update to xgraphics */

    return(0); /* Good return */
  }
  else
  {
    printH_FlagHelp("label");

    return(1);
  }

  return(0);
}


/****************************  Markers **********************************/

int markerList(fname)
char *fname;
{
  int i;
  FILE *fp;

  if(fname)
  {
    if((fp = fopen(fname, "w")) == NULL)
    {
      st_fail("Unable to open %s", fname);
      return(1);
    }

    st_report("Saving Markers to %s", fname);
  }
  else
  {
    fp = stdout;
  }

  fprintf(fp, "#  \n");
  fprintf(fp, "#  Used  Marker_x  Marker_y  Marker_y2  Color\n");
  fprintf(fp, "#  \n");

  for(i=0;i<MAX_IMAGE_MARKERS;i++)
  {
    fprintf(fp, "%2d  %d  %9.3f  %9.3f  %9.3f    %d\n", 
			i,
			sat.markers[i].used,
			sat.markers[i].x,
			sat.markers[i].y,
			sat.markers[i].y2,
			sat.markers[i].color);
  }

  fprintf(fp, "#  \n");

  if(fname)
  {
    fclose(fp);
  }

  return(1);
}


int doMarker()
{
  int i, n, color, chan;
  char fname[256], *string, line[256];
  FILE *fp;
  struct IMAGE_MARKERS *m;
  float y1, y2, delta, ny, cy;

  if(isSet(PF_E_FLAG))
  {
    for(i=0;i<MAX_IMAGE_MARKERS;i++)
    {
      sat.markers[i].used     = 0;
      sat.markers[i].x        = 0.0;
      sat.markers[i].y        = 0.0;
      sat.markers[i].y2       = 0.0;
      sat.markers[i].color    = 0;
    }

    sendSat(); /* Send update to xgraphics */

    return(0);
  }

  n     = args->ntokens;
  color = parseColor();

  if(isSet(PF_C_FLAG))
  {
    if(n > 1)
    {
      i = atoi(args->secondArg);

      if(i >=0 && i<MAX_IMAGE_MARKERS)
      {
        m = &sat.markers[i];

        if(h0->pinfo.x1mode == XMODE_CHAN)
        {
          _doCCur("Mouse-Click plot to locate top of marker");
          m->x = env->ccur;
        }
        else
        if(h0->pinfo.x1mode == XMODE_VEL)
        {
          _doVCur("Mouse-Click plot to locate top of marker");
          m->x = env->vcur;
        }
        else
        if(h0->pinfo.x1mode == XMODE_FREQ)
        {
          _doFCur("Mouse-Click plot to locate top of marker");
          m->x = env->fcur;
        }

        m->used   = 1;
        m->y      = env->tcur;
        m->color  = color;

        _doFCur("Mouse-Click plot to locate bottom of marker");

        m->y2 = env->tcur;

        env->usesat = 1; /* Set flag so plotter will use */

        sendSat(); /* Send to xgraphics */

        return(0);
      }
      else
      {
	st_fail("n: %d, out of range", i);
        printH_FlagHelp("marker");

        return(1);
      }
    }
    else
    {
      printH_FlagHelp("marker");

      return(1);
    }
  }
  else
  if(isSet(PF_P_FLAG))
  {
    if(n > 2)
    {
      i = atoi(args->secondArg);

      if(i >=0 && i<MAX_IMAGE_MARKERS)
      {
        m = &sat.markers[i];
        m->x = atof(args->thirdArg);

        if(isSet(PF_A_FLAG))    /* Auto scale placement */
        {
          delta = fabs((env->yplotmax - env->yplotmin) / 6.0);
          y2 = env->yplotmin + delta * 1.0; /* make the lower extent of line the same for both */
          st_print("ymax = %f, ymin = %f, delta = %f\n", env->yplotmax, env->yplotmin, delta);

          if(isSet(PF_L_FLAG)) /* Make a little higher then line strength at this point */
          {
	    chan = mapFtoChan(m->x);
            st_print("Chan = %d\n", chan);

	    if(chan >= 0 && chan < (int)h0->head.noint)
	    {
	      cy = h0->yplot1[chan];
	      ny = env->yplotmin;

              while(ny < cy) /* Find the next interval above line peak */
	      {
	        if(ny > cy)
		{
		  break;
		}

	        ny += delta;
	      }

              y1 = ny;
	    }
	    else
	    {
	      st_fail("Freq of %f, out of range; using default", m->x);

              y1 = env->yplotmin + delta * 5.0;
	    }
          }
          else
          {
            y1 = env->yplotmin + delta * 5.0;
          }

	  st_print("y1 = %f, y2 = %f\n", y1, y2);

          m->y  = y1;
          m->y2 = y2;
        }
        else
        if(isSet(PF_M_FLAG))    /* Passed args for placement */
        {
          if(n < 4)
          {
            printH_FlagHelp(args->firstArg);
            return(1);
          }
          m->y  = atof(args->fourthArg);
          m->y2 = atof(args->fifthArg);
        }
        else                    /* Use mouse placement */
        {

          _doCCur("Mouse-Click plot to locate top of marker");

          m->y      = env->tcur;

          _doFCur("Mouse-Click plot to locate bottom of marker");

          m->y2 = env->tcur;
	}

        m->used   = 1;
        m->color  = color;

        env->usesat = 1; /* Set flag so plotter will use */

        sendSat(); /* Send to xgraphics */

        return(0);
      }
      else
      {
	st_fail("n: %d, out of range", i);
        printH_FlagHelp("marker");

        return(1);
      }
    }
    else
    {
      st_fail("Wrong number of args: %d", n);
      printH_FlagHelp("marker");

      return(1);
    }
  }
  else
  if(isSet(PF_E_FLAG))
  {
    if(n == 2)
    {
      i = atoi(args->secondArg);

      if(i >=0 && i<MAX_IMAGE_MARKERS)
      {
        st_report("Erasing marker #%d", i);
        sat.markers[i].used     = 0;
	sat.markers[i].x        = 0.0;
	sat.markers[i].y        = 0.0;
	sat.markers[i].y2       = 0.0;
       	sat.markers[i].color    = 0;

        sendSat(); /* Send update to xgraphics */
      }
      else
      {
	st_fail("n: %d, out of range", i);
        printH_FlagHelp("marker");

        return(1);
      }

      return(0);
    }
    else
    {
      printH_FlagHelp("marker");

      return(1);
    }
  }
  else
  if(isSet(PF_D_FLAG))
  {
    st_report("Deleting all Markers");

    bzero((char *)&sat.markers, sizeof(sat.markers));

    sendSat(); /* Send update to xgraphics */

    return(0);
  }
  else
  if(isSet(PF_S_FLAG))
  {
    if(n == 2)
    {
      strcpy(fname, args->secondArg);

      markerList(fname);

      return(0);
    }
    else
    {
      st_fail("Missing Filename");
      printH_FlagHelp("marker");

      return(1);
    }
  }
  else
  if(isSet(PF_R_FLAG))
  {
    if(n == 2)
    {
      strcpy(fname, args->secondArg);

      if((fp = fopen(fname, "r")) != NULL)
      {
        st_report("Clearing out all Markers");
        bzero((char *)&sat.markers, sizeof(sat.markers));
        st_report("Loading Markers from: %s", fname);

        while((string = fgets(line, sizeof(line), fp)) != NULL)
        {
          if(line[0] == '#')
          {
            continue;
          }

	  n = sat_get_tok(line);

          if(n == 6)
          {
	    i = atoi(sat_tok[0]);

            if(i<MAX_IMAGE_MARKERS)
            {
	      sat.markers[i].used   = atoi(sat_tok[1]);
	      sat.markers[i].x      = atof(sat_tok[2]);
	      sat.markers[i].y      = atof(sat_tok[3]);
	      sat.markers[i].y2     = atof(sat_tok[4]);
	      sat.markers[i].color  = atoi(sat_tok[5]);
            }
          }
	}

        fclose(fp);
  
        markerList(NULL);

	env->usesat = 1; /* Set flag so plotter will use */

        sendSat(); /* Send update to xgraphics */
      }

      return(0);
    }
    else
    {
      st_fail("Missing Filename");
      printH_FlagHelp("marker");

      return(1);
    }
  }
  else
  if(isSet(PF_L_FLAG)) /* Make this last as I use the /l above in the /a option */
  {
    markerList(NULL);

    return(0);
  }
  else
  {
    printH_FlagHelp("marker");
  }

  return(0);
}


/********************************* Horizontal Lines *************************/


int findHlineExtents(max, min)
double *max, *min;
{
  int i;
  double mx = -1e6, mn = 1e6;
  struct IMAGE_HLINE *p;

  p = &sat.hlines[0];
  for(i=0;i<MAX_IMAGE_HLINES;i++,p++)
  {
    if(p->used)
    {
      if(p->value > mx)
      {
        mx = p->value;
      }

      if(p->value < mn)
      {
        mn = p->value;
      }
    }
  }

  *max = mx;
  *min = mn;

  return(0);
}


int hlineList(fname)
char *fname;
{
  int i;
  FILE *fp;

  if(fname)
  {
    if((fp = fopen(fname, "w")) == NULL)
    {
      st_fail("Unable to open %s", fname);
      return(1);
    }

    st_report("Saving Hlines to %s", fname);
  }
  else
  {
    fp = stdout;
  }

  fprintf(fp, "#  \n");
  fprintf(fp, "#  Used Color  Value\n");
  fprintf(fp, "#  \n");
  for(i=0;i<MAX_IMAGE_HLINES;i++)
  {
    fprintf(fp, "%2d   %d    %d  %10.6f\n", i, sat.hlines[i].used, sat.hlines[i].color, sat.hlines[i].value);
  }

  if(fname)
  {
    fclose(fp);
  }

  return(0);
}


int doHline()
{
  int i, n, color;
  double val;
  char fname[256], *string, line[256];
  FILE *fp;
  double max, min;

  if(isSet(PF_E_FLAG))
  {
    st_report("Clearing out All Horizontal Lines");

    bzero((char *)&sat.hlines[0], sizeof(sat.hlines));

    sendSat();

    return(0);
  }

  if(isSet(PF_L_FLAG))
  {
    hlineList(NULL);

    return(0);
  }

  color = parseColor();

  if(args->ntokens > 1)
  {
    if(isSet(PF_C_FLAG))
    {
      if(args->ntokens > 2)
      {
        n   = atoi(args->secondArg);
        val = atof(args->thirdArg);

        if(n >= 0 && n < MAX_IMAGE_HLINES)
        {
          sat.hlines[n].value = val; /* set it; and inc counter */
          sat.hlines[n].used  = 1;
          sat.hlines[n].color = color;

	  env->usesat = 1; /* Set flag so plotter will use */

          sendSat(); /* Send update to xgraphics */

	  findHlineExtents(&max, &min);

	  if(env->yplotmax < max || env->yplotmin > min)
          {
	    setColor(ANSI_RED);
	    st_report("Recommend Setting yrange to include Newly Create HLine");
	    setColor(ANSI_BLACK);
          }

          return(0);
        }
      }
      else
      {
        printH_FlagHelp("hline");

        return(1);
      }
    }
    else
    if(isSet(PF_D_FLAG))
    {
      if(args->ntokens > 1)
      {
        n = atoi(args->secondArg);

        if(n >= 0 && n < MAX_IMAGE_HLINES)
        {
	  if(sat.hlines[n].used)
          {
            sat.hlines[n].value = 0.0;
            sat.hlines[n].used  = 0;
            sat.hlines[n].color = 0;
          }

          sendSat(); /* Send update to xgraphics */

          return(0);
        }
      }
      else
      {
        printH_FlagHelp("hline");

        return(1);
      }
    }
    else
    if(isSet(PF_S_FLAG))
    {
      if(args->ntokens > 1)
      {
        strcpy(fname, args->secondArg);

        hlineList(fname);

        return(0);
      }
      else
      {
        printH_FlagHelp("hline");

        return(1);
      }

      return(0);
    }
    else
    if(isSet(PF_L_FLAG))
    {
      if(args->ntokens > 1)
      {
        strcpy(fname, args->secondArg);

        if((fp = fopen(fname, "r")) != NULL)
        {
          st_report("Clearing out all Hlines");
          bzero((char *)&sat.hlines, sizeof(sat.hlines));
          st_report("Loading Hlines from: %s", fname);

          while((string = fgets(line, sizeof(line), fp)) != NULL)
          {
            if(line[0] == '#')
            {
              continue;
            }

	    n = sat_get_tok(line);

            if(n == 4)
            {
	      i = atoi(sat_tok[0]);

              if(i<MAX_IMAGE_HLINES)
              {
	        sat.hlines[i].used   = atoi(sat_tok[1]);
	        sat.hlines[i].color  = atoi(sat_tok[2]);
	        sat.hlines[i].value  = atof(sat_tok[3]);
              }
            }
	  }

          fclose(fp);
  
          hlineList(NULL);

	  env->usesat = 1; /* Set flag so plotter will use */

          sendSat(); /* Send update to xgraphics */

          return(0);
	}
      }
    }
    else
    {
      printH_FlagHelp("hline");

      return(1);
    }
  }
  else /* default is to just list them */
  {
    hlineList(NULL);
    return(0);
  }

  return(0);
}


/****************************  Forks **********************************/

int forkList(fname)
char *fname;
{
  int i, j;
  FILE *fp;

  if(fname)
  {
    if((fp = fopen(fname, "w")) == NULL)
    {
      st_fail("Unable to open %s", fname);
      return(1);
    }

    st_report("Saving Forks to %s", fname);
  }
  else
  {
    fp = stdout;
  }

  fprintf(fp, " # \n");
  fprintf(fp, " # Used Color    CT     TT   nT  ");
  for(j=0;j<MAX_FORK_TINES;j++)
  {
    fprintf(fp, "     X       Y  ");
  }

  fprintf(fp, "\n");

  fprintf(fp, " # \n");

  for(i=0;i<MAX_IMAGE_FORKS;i++)
  {
    fprintf(fp, "%2d   %d    %d  %7.3f %7.3f  %d  ", i,
			sat.forks[i].used,
			sat.forks[i].color,
			sat.forks[i].crossT,
			sat.forks[i].topT,
			sat.forks[i].ntines);

    for(j=0;j<MAX_FORK_TINES;j++)
    {
      fprintf(fp, "%7.3f %7.3f ", sat.forks[i].tines[j].x, sat.forks[i].tines[j].y);
    }

    fprintf(fp, "%s\n", sat.forks[i].label);
  }

  fprintf(fp, " # \n");

  if(fname)
  {
    fclose(fp);
  }

  return(0);
}


int autoCreateForks()
{
  int i, n, w, nt;
  struct LP_IMAGE_INFO *p;
  struct IMAGE_FORKS *f;
  struct GAUSSIAN *g;
  char tbuf[512], newlabel[256];

  p  = h0;
  g  = &p->ginfo;
  nt = g->nfits;

  w = atoi(args->secondArg);

  if(nt)
  {
    if(nt > MAX_FORK_TINES)
    {
      st_fail("Too many Gaussian fits: %d; Max = %d", nt, MAX_FORK_TINES);
    }

    st_report("Creating a Fork Label based on %d Gaussian Fits", nt);

	/* create the label */
    newlabel[0] = '\0';

    n = args->ntokens;
    for(i=2;i<n;i++)
    {
      sprintf(tbuf, "%s ", args->tok[i]);
      strcat(newlabel, tbuf);
    }

    deQuote(newlabel); /* Remove pesky "'s */
    strcmprs(newlabel); /* Remove excess spaces */

    if(w >=0 && w < MAX_IMAGE_FORKS)
    {
      f = &sat.forks[w];

      f->ntines = nt;
      f->crossT = g->fits[0].peak * 1.10; /* Assume this is the hightest one for now */
      f->topT   = g->fits[0].peak * 1.15;

      for(i=0;i<nt;i++)
      {
        f->tines[i].x = g->fits[i].center;
        f->tines[i].y = g->fits[i].peak * 1.05;
      }

      f->color = parseColor();

      strcpy(f->label, newlabel);
      f->used = 1;
    }
    else
    {
      st_fail("Fork indes %d, out of range: 0 to %d", w, MAX_IMAGE_FORKS-1);
    }
  }
  else
  {
    st_fail("No Gaussian Fits Yet");

    return(1);
  }

  return(0);
}


int doFork()
{
  int i, j, k, l, n, nt, color;
  char tbuf[512], newlabel[256], fname[256], *string, line[256];
  char ttbuf[512], bbuf[256];
  struct IMAGE_FORKS *f;
  FILE *fp;


  if(isSet(PF_C_FLAG))	/* clear */
  {
    st_report("Clearing out all Forks");

    bzero((char *)&sat.forks, sizeof(sat.forks));

    sendSat(); /* Send update to xgraphics */

    return(0);
  }

  if(isSet(PF_L_FLAG))	/* list */
  {
    forkList(NULL);

    return(0);
  }

  /* From here on, args are required */

  n = args->ntokens;

  if(n < 2)
  {
    printH_FlagHelp("fork");

    return(1);
  }

  if(isSet(PF_A_FLAG))	/* auto gaussian fork */
  {
    if(n > 2)
    {
      if(!autoCreateForks())
      {
        forkList(NULL);
        env->usesat = 1; /* Set flag so plotter will use */
        sendSat(); /* Send update to xgraphics */

        return(0);
      }
      else
      {
        return(1);
      }
    }
    else
    {
      st_fail("Too few arg for auto create fork: requies > 2");
    }
  }

  if(isSet(PF_I_FLAG)) /* initialize */
  {
    if(n > 2)
    {
      i = atoi(args->secondArg);

      if(i >=0 && i<MAX_IMAGE_FORKS)
      {
        f = &sat.forks[i];

	/* create the label */
        newlabel[0] = '\0';

        for(j=3;j<n;j++)
        {
          sprintf(tbuf, "%s ", args->tok[j]);
          strcat(newlabel, tbuf);
        }

        deQuote(newlabel); /* Remove pesky "'s */
        strcmprs(newlabel); /* Remove excess spaces */

        nt = atoi(args->thirdArg);

        if(nt > MAX_FORK_TINES)
        {
	  st_fail("Too many Tines; Max = %d", MAX_FORK_TINES);

          return(1);
        }

        if(nt < 2)
        {
	  st_fail("Not Enough Tines; Must be > %d", 1);

          return(1);
        }

        for(j=0;j<nt;j++)
        {
          sprintf(tbuf, "Mouse-Click plot to locate bottom of Fork Tine %d", j+1);
          if(h0->pinfo.x1mode == XMODE_CHAN)
          {
            _doCCur(tbuf);
            f->tines[j].x = env->ccur;
          }
          else
          if(h0->pinfo.x1mode == XMODE_VEL)
          {
            _doVCur(tbuf);
            f->tines[j].x = env->vcur;
          }
          else
          if(h0->pinfo.x1mode == XMODE_FREQ)
          {
            _doFCur(tbuf);
            f->tines[j].x = env->fcur;
          }

          f->tines[j].y = env->tcur;
	}

	f->ntines = nt;

        _doTCur("Mouse-Click plot to locate Cross Bar Temp");
        f->crossT = env->tcur;

        _doTCur("Mouse-Click plot to locate Label Placement");
        f->topT = env->tcur;

        color = parseColor();

        f->used   = 1;
        f->color  = color;
        strcpy(f->label, newlabel);

	env->usesat = 1; /* Set flag so plotter will use */

        sendSat(); /* Send to xgraphics */
      }
      else
      {
        printH_FlagHelp("fork");

        return(1);
      }
    }
    else
    {
      printH_FlagHelp("fork");

      return(1);
    }

    return(0);
  }

  if(isSet(PF_D_FLAG))	/* delete */
  {
    if(n == 2)
    {
      i = atoi(args->secondArg);

      if(i >=0 && i < MAX_IMAGE_FORKS)
      {
        st_report("Removing Fork #%d", i);

        bzero((char *)&sat.forks[i], sizeof(sat.forks[0]));

        sendSat(); /* Send update to xgraphics */
      }
    }
    else
    {
      printH_FlagHelp("fork");

      return(1);
    }

    return(0);
  }

  if(isSet(PF_S_FLAG))	/* save */
  {
    if(n == 2)
    {
      strcpy(fname, args->secondArg);

      forkList(fname);
    }
    else
    {
      printH_FlagHelp("fork");

      return(1);
    }

    return(0);
  }

  if(isSet(PF_R_FLAG))	/* read */
  {
    if(n == 2)
    {
      strcpy(fname, args->secondArg);

      if((fp = fopen(fname, "r")) != NULL)
      {
        st_report("Clearing out all Forks");
        bzero((char *)&sat.forks, sizeof(sat.forks));
        st_report("Loading Forks from: %s", fname);

        while((string = fgets(line, sizeof(line), fp)) != NULL)
        {
          if(line[0] == '#')
          {
            continue;
          }

	  n = sat_get_tok(line);

          if(n >= 6+MAX_FORK_TINES*2)
          {
	    j = atoi(sat_tok[0]);

            if(j<MAX_IMAGE_FORKS)
            {
	      sat.forks[j].used   = atoi(sat_tok[1]);
	      sat.forks[j].color  = atoi(sat_tok[2]);
	      sat.forks[j].crossT = atof(sat_tok[3]);
	      sat.forks[j].topT   = atof(sat_tok[4]);
	      sat.forks[j].ntines = atoi(sat_tok[5]);

	      l = 0;
              for(k=0;k<MAX_FORK_TINES;k++)
              {
		sat.forks[j].tines[k].x = atof(sat_tok[6+l++]);
		sat.forks[j].tines[k].y = atof(sat_tok[6+l++]);
              }

	      bbuf[0] = '\0';
              for(k=(6+MAX_FORK_TINES*2);k<n;k++)
              {
                sprintf(ttbuf, "%s ", sat_tok[k]);
                strcat(bbuf, ttbuf);
              }

              strcmprs(bbuf);

              strcpy(sat.forks[j].label, bbuf);
            }
          }
	}

        fclose(fp);
  
        forkList(NULL);

	env->usesat = 1; /* Set flag so plotter will use */

        sendSat(); /* Send update to xgraphics */

        return(0); /* Good return */
      }
      else
      {
        st_fail("Unable to open: %s", fname);
	return(1);
      }
    }
    else
    {
      printH_FlagHelp("fork");

      return(1);
    }

    return(0);
  }

  printH_FlagHelp("fork");

  return(1);

}

/******************************* Date **************************************/

int doDateString()
{
  int n, j, color;
  long sec;
  char tbuf[512], dbuf[256];

  st_report("Inside doDateString()");

  if(isSet(PF_E_FLAG)) /* Clear date */
  {
    bzero((char *)&sat.date, sizeof(sat.date));
    st_report("Erasing Date String");

    sendSat(); /* Send update to xgraphics */

    return(0);
  }
  else
  if(isSet(PF_N_FLAG)) /* Make date using Now */
  {
    strcpy(sat.date.date, getcTime(0));
  }
  else
  if(isSet(PF_S_FLAG)) /* Make date from scan info */
  {
    sec = h0->head.burn_time;

    if(!sec)
    {
      st_fail("Date not set in Header");
      strcpy(sat.date.date, getcTime(0));
    }
    else
    {
      strcpy(sat.date.date, ctime(&sec));
    }
  }
  else			/* Make date from passed string */
  {
    n = args->ntokens;

    if(n < 2)
    {
      printH_FlagHelp("datestring");

      return(1);
    }

    bzero(dbuf, sizeof(dbuf));

    for(j=1;j<n;j++)
    {
      sprintf(tbuf, "%s ", args->tok[j]);
      strcat(dbuf, tbuf);
    }

    strcmprs(dbuf); /* Remove excess spaces */

    strcpy(sat.date.date, dbuf);
  }

  deNewLineIt(sat.date.date);
  strcmprs(sat.date.date);

  st_report("Using Date: %s", sat.date.date);

  color = parseColor();

  sprintf(tbuf, "Mouse-Click plot to locate Date");
  if(h0->pinfo.x1mode == XMODE_CHAN)
  {
    _doCCur(tbuf);
    sat.date.x = env->ccur;
  }
  else
  if(h0->pinfo.x1mode == XMODE_VEL)
  {
    _doVCur(tbuf);
    sat.date.x = env->vcur;
  }
  else
  if(h0->pinfo.x1mode == XMODE_FREQ)
  {
    _doFCur(tbuf);
    sat.date.x = env->fcur;
  }

  sat.date.y = env->tcur;

  sat.date.color = color;
  env->usesat   = 1; /* Set flag so plotter will use */
  sat.date.used = 1;

  sendSat(); /* Send update to xgraphics */

  return(0);
}




/****************************** Title Block ***********************************/

int titleBlock()
{
  int i, n, indx;
  char tbuf[512], buf[512];
  double x, y, deltay;
  struct TITLE_BLOCK *p;

  st_report("Clearing out Title Block"); 

  p = &sat.tblock;

  bzero((char *)p, sizeof(struct TITLE_BLOCK));

  if(isSet(PF_C_FLAG)) /* Just clear andd return */
  {
    sendSat(); /* Send empty tblock to xgraphics */

    return(0);
  }

  sprintf(tbuf, "Mouse-Click plot to locate Block");
  if(h0->pinfo.x1mode == XMODE_CHAN)
  {
    _doCCur(tbuf);
    x = env->ccur;
  }
  else
  if(h0->pinfo.x1mode == XMODE_VEL)
  {
    _doVCur(tbuf);
    x = env->vcur;
  }
  else
  if(h0->pinfo.x1mode == XMODE_FREQ)
  {
    _doFCur(tbuf);
    x = env->fcur;
  }

  y = env->tcur;

  p->color = parseColor();

  if(env->yscale == Y_FIXED_SCALE)
  {
    deltay = (env->yupper - env->ylower) / 26.0;
  }
  else
  {
    deltay = (env->datamax - env->datamin) / 26.0;
  }

  n      = args->ntokens;

  indx = 0;
  if(args->ntokens > 1)
  {
    tbuf[0] = '\0';

    for(i=1;i<n;i++) /* Assemble the title */
    {
      sprintf(buf, "%s ", args->tok[i]);
      strcat(tbuf, buf);
    }
    
    p->labels[indx].used   = 1;
    p->labels[indx].x      = x;
    p->labels[indx].y      = y;
    p->labels[indx].nohead = TRUE,
    p->labels[indx].color  = p->color,
    p->labels[indx].direct = LABEL_RIGHT;
    strcpy(p->labels[indx].label, tbuf);
    
    indx++;

    y -= deltay;
  }

  if(isSet(PF_R_FLAG) || isSet(PF_A_FLAG))
  {
    printRMS();

    sprintf(tbuf, "rms       = %.2f / %.2f mK", env->rms * 1e3, env->trms * 1e3);
    p->labels[indx].used   = 1;
    p->labels[indx].x      = x;
    p->labels[indx].y      = y;
    p->labels[indx].nohead = TRUE,
    p->labels[indx].color  = p->color,
    p->labels[indx].direct = LABEL_RIGHT;
    strcpy(p->labels[indx].label, tbuf);
    
    indx++;

    y -= deltay;
  }

  if(isSet(PF_P_FLAG) || isSet(PF_A_FLAG))
  {
    showPeak();

    sprintf(tbuf, "peak      = %s", peakResultsStr1);
    p->labels[indx].used   = 1;
    p->labels[indx].x      = x;
    p->labels[indx].y      = y;
    p->labels[indx].nohead = TRUE,
    p->labels[indx].color  = p->color,
    p->labels[indx].direct = LABEL_RIGHT;
    strcpy(p->labels[indx].label, tbuf);
    
    indx++;
    y -= deltay;

    if(env->x1mode != env->x2mode)
    {
      sprintf(tbuf, "peak      = %s", peakResultsStr2);
      p->labels[indx].used   = 1;
      p->labels[indx].x      = x;
      p->labels[indx].y      = y;
      p->labels[indx].nohead = TRUE,
      p->labels[indx].color  = p->color,
      p->labels[indx].direct = LABEL_RIGHT;
      strcpy(p->labels[indx].label, tbuf);
    
      indx++;
      y -= deltay;
    }
  }

  if(isSet(PF_B_FLAG) || isSet(PF_A_FLAG))
  {
    if(fabs(env->slope) < 0.1)
    {
      sprintf(tbuf, "slope     = %e", env->slope);
    }
    else
    {
      sprintf(tbuf, "slope     = %.4f", env->slope);
    }

    p->labels[indx].used   = 1;
    p->labels[indx].x      = x;
    p->labels[indx].y      = y;
    p->labels[indx].nohead = TRUE,
    p->labels[indx].color  = p->color,
    p->labels[indx].direct = LABEL_RIGHT;
    strcpy(p->labels[indx].label, tbuf);
    
    indx++;
    y -= deltay;

    if(fabs(env->intercept) < 0.1)
    {
      sprintf(tbuf, "intercept = %e", env->intercept);
    }
    else
    {
      sprintf(tbuf, "intercept = %.4f", env->intercept);
    }

    p->labels[indx].used   = 1;
    p->labels[indx].x      = x;
    p->labels[indx].y      = y;
    p->labels[indx].nohead = TRUE,
    p->labels[indx].color  = p->color,
    p->labels[indx].direct = LABEL_RIGHT;
    strcpy(p->labels[indx].label, tbuf);
    
    indx++;
    y -= deltay;
  }

  p->used    = 1;
  p->nlabels = indx;
  p->drawbox = 1; /* For now */

  env->usesat = 1; /* Set flag so plotter will use */

  sendSat(); /* Send to xgraphics */

  return(0);
}

/************************************ Gaussian Labels *******************************/

int doGlabel()
{
  int i, j;
  char tbuf[512];
  double y1, deltay;
  struct IMAGE_GAUSS *p;
  static int color = ANSI_MAGENTA;
  static double x=0.0, y=0.0;

  p = &sat.gauss;

  if(isSet(PF_C_FLAG)) /* Just clear and return */
  {
    p->used = 0;

    sendSat(); /* Send empty tblock to xgraphics */

    return(0);
  }

  if(x == 0.0 && y == 0.0)
  {
    x = h0->x1data[4];
    y = env->gaussPeak * 0.90;
  }

  if(notSet(PF_A_FLAG))
  {
    sprintf(tbuf, "Mouse-Click Plot to Locate Gauss Info");
    if(h0->pinfo.x1mode == XMODE_CHAN)
    {
      _doCCur(tbuf);
      x = env->ccur;
    }
    else
    if(h0->pinfo.x1mode == XMODE_VEL)
    {
      _doVCur(tbuf);
      x = env->vcur;
    }
    else
    if(h0->pinfo.x1mode == XMODE_FREQ)
    {
      _doFCur(tbuf);
      x = env->fcur;
    }

    y = env->tcur;

    color = parseColor();
  }

  if(color == ANSI_BLACK && env->displaymode == ANSI_BLACK)
  {
    color = ANSI_MAGENTA;
  }
  else
  if(color == ANSI_WHITE && env->displaymode == ANSI_WHITE)
  {
    color = ANSI_MAGENTA;
  }

  p->color = color;

  if(env->yscale == Y_FIXED_SCALE)
  {
    deltay = (env->yupper - env->ylower) / 26.0;
  }
  else
  {
    deltay = (env->datamax - env->datamin) / 26.0;
  }

  y1 = y;

  j = 0;
  for(i=0;i<MAX_IMAGE_GAUSS_RESULTS;i++)
  {
    if(sat.gauss.results[i].used)
    {
      p->results[j].x = x;
      p->results[j].y = y1;

      y1 -= deltay;
      j++;
    }
  }

  p->used = 1;

  env->usesat = 1; /* Set flag so plotter will use */

  sendSat();

  return(0);
}


/*********************************** Postage Stamps *************************************/


int getMouseClick(buf, x, y)
char *buf;
double *x, *y;
{
  int ret;

  if(h0->pinfo.x1mode == XMODE_CHAN)
  {
    ret = _doCCur(buf);
    *x = env->ccur;
  }
  else
  if(h0->pinfo.x1mode == XMODE_VEL)
  {
    ret = _doVCur(buf);
    *x = env->vcur;
  }
  else
  if(h0->pinfo.x1mode == XMODE_FREQ)
  {
    ret = _doFCur(buf);
    *x = env->fcur;
  }

  st_report("getMouseClick(): Returned %d", ret);

  *y = env->tcur;

  return(ret);
}


int pstampList(fname)
char *fname;
{
  int i;
  struct STAMP *p;
  FILE *fp;

  if(fname)
  {
    if((fp = fopen(fname, "w")) == NULL)
    {
      st_fail("Unable to open %s", fname);
      return(1);
    }

    st_report("Saving pStamps to %s", fname);
  }
  else
  {
    fp = stdout;
  }

  fprintf(fp, "#\n");
  fprintf(fp, "# Number Used   Color     X-min     X-max       boxUx        BoxUy      BoxLRx      BoxLRy  Size  Label\n");
  fprintf(fp, "#\n");

  p = &sat.pstamp.stamps[0];
  for(i=0;i<MAX_PSTAMPS;i++,p++)
  {
    fprintf(fp, "    %2d    %d      %d  %11.3f %11.3f %11.3f %11.3f %11.3f %11.3f   %d  %s\n", i, p->used,
				p->color,
				p->linexmin, p->linexmax, 
				p->stampULx, p->stampULy, 
				p->stampLRx, p->stampLRy, 
				p->size, p->label);
  }

  if(fname)
  {
    fclose(fp);
  }

  return(0);
}

int doPStamp()
{
  int    j, k, n, color, size, sizeSet;
  double linexmin, linexmax;
  double x, y;
  double stampULx, stampLRx;
  double stampULy, stampLRy;
  char   ttbuf[512], bbuf[256], tbuf[256], fname[256], *string, line[256];
  FILE  *fp;
  struct STAMP *p;

  if(isSet(PF_L_FLAG))
  {
    pstampList(NULL);

    return(0);
  }

  if(isSet(PF_E_FLAG))
  {
    if(isSet(PF_A_FLAG))
    {
      st_report("Erasing All Postage Stamps");
      bzero((char *)&sat.pstamp, sizeof(sat.pstamp));

      sendSat(); /* Send update to xgraphics */

      return(0);
    }

    if(args->ntokens > 1)
    {
      n = atoi(args->secondArg);

      if(n < 0 || n > MAX_PSTAMPS)
      {
        printH_FlagHelp(args->firstArg);

        return(1);
      }

      st_report("Erasing Postage Stamp: %d", n);

      p = &sat.pstamp.stamps[n];

      bzero((char *)p, sizeof(struct STAMP));

      sendSat(); /* Send update to xgraphics */
    }

    return(0);
  }


  if(isSet(PF_R_FLAG))
  {
    if(args->ntokens > 1)
    {
      strcpy(fname, args->secondArg);

      if((fp = fopen(fname, "r")) != NULL)
      {
        st_report("Clearing out all Postage Stamps");
        bzero((char *)&sat.pstamp, sizeof(sat.pstamp));

        st_report("Loading Postage Stamps from: %s", fname);

        while((string = fgets(line, sizeof(line), fp)) != NULL)
        {
          if(line[0] == '#')
          {
            continue;
          }

          n = sat_get_tok(line);

          if(n > 7)
          {
            j = atoi(sat_tok[0]);

            if(j<MAX_PSTAMPS)
            {
              sat.pstamp.stamps[j].used     = atoi(sat_tok[1]);
              sat.pstamp.stamps[j].color    = atoi(sat_tok[2]);

              sat.pstamp.stamps[j].linexmin = atof(sat_tok[3]);
              sat.pstamp.stamps[j].linexmax = atof(sat_tok[4]);
              sat.pstamp.stamps[j].stampULx = atof(sat_tok[5]);
              sat.pstamp.stamps[j].stampULy = atof(sat_tok[6]);
              sat.pstamp.stamps[j].stampLRx = atof(sat_tok[7]);
              sat.pstamp.stamps[j].stampLRy = atof(sat_tok[8]);

              sat.pstamp.stamps[j].size     = atoi(sat_tok[9]);


                /* Check is the text already has size info */
              sizeSet = 0;
              for(k=10;k<n;k++)
              {
                if(strstr(sat_tok[k], "Helvet"))
                {
                  sizeSet = 1;
                }
              }

              if(sizeSet) /* Just assemble them */
              {
                bbuf[0] = '\0';
                for(k=10;k<n;k++)
                {
                  sprintf(ttbuf, "%s ", sat_tok[k]);
                  strcat(bbuf, ttbuf);
                }
              }
              else /* Add size info */
              {
                size = getLabelSize(sat.pstamp.stamps[j].size);
                strcpy(bbuf, "");
                sprintf(bbuf, "{/Helvetica=%d ", size);

                for(k=10;k<n;k++)
                {
                  sprintf(ttbuf, "%s ", sat_tok[k]);
                  strcat(bbuf, ttbuf);
                }

                strcat(bbuf, "}");
              }

              strcmprs(bbuf);

              strcpy(sat.pstamp.stamps[j].label, bbuf);
            }

            sat.pstamp.nstamps++;
          }
        }

        fclose(fp);

        pstampList(NULL);

        sendSat(); /* Send update to xgraphics */

        return(0); /* Good return */
      }
      else
      {
        st_fail("Unable to open: %s", fname);
        return(1);
      }
    }
    else
    {
      printH_FlagHelp(args->firstArg);

      return(1);
    }
  }

  if(isSet(PF_W_FLAG))
  {
    if(args->ntokens > 1)
    {
      strcpy(fname, args->secondArg);

      pstampList(fname);

      return(0);
    }
    else
    {
      printH_FlagHelp(args->firstArg);

      return(1);
    }

    return(0);
  }

  if(isSet(PF_C_FLAG))
  {
    if(args->ntokens > 1)
    {
      n = atoi(args->secondArg);

      if(n < 0 || n > MAX_PSTAMPS)
      {
        printH_FlagHelp(args->firstArg);

        return(1);
      }

      color = parseColor();

      strcpy(tbuf, "Mouse-Click plot to locate Left Edge of Line");
      if(getMouseClick(tbuf, &x, &y))
      {
        return(1);
      }

      linexmin = x;

      strcpy(tbuf, "Mouse-Click plot to locate Right Edge of Line");
      if(getMouseClick(tbuf, &x, &y))
      {
        return(1);
      }

      linexmax = x;

      strcpy(tbuf, "Mouse-Click plot to locate Upper-Left of Stamp");
      if(getMouseClick(tbuf, &x, &y))
      {
        return(1);
      }

      stampULx = x;
      stampULy = y;

      strcpy(tbuf, "Mouse-Click plot to locate Lower-Right of Stamp");
      if(getMouseClick(tbuf, &x, &y))
      {
        return(1);
      }

      stampLRx = x;
      stampLRy = y;

      p = &sat.pstamp.stamps[n];

      p->linexmin = linexmin;
      p->linexmax = linexmax;
      p->stampLRx = stampLRx;
      p->stampLRy = stampLRy;
      p->stampULx = stampULx;
      p->stampULy = stampULy;
      p->color    = color;
      p->used     = 1;
      p->size     = getLabelSize(0);

      
      sprintf(bbuf, "{/Helvetica=%d ", p->size);
      for(k=2;k<args->ntokens;k++)
      {
        sprintf(ttbuf, "%s ", args->tok[k]);
        strcat(bbuf, ttbuf);
      }

      strcat(bbuf, "}");

      strcmprs(bbuf);
      strcpy(p->label, bbuf);

      sat.pstamp.nstamps++;

      sendSat(); /* Send update to xgraphics */
    }

    return(0);
  }

  /* If you get here; print help */
  printH_FlagHelp(args->firstArg);

  return(0);
}


int cursor_labels[MAX_IMAGE_LABELS];

int findNextLabel()
{
  int i;

  for(i=0;i<MAX_IMAGE_LABELS;i++)
  {
    if(!sat.labels[i].used)
    {
      cursor_labels[i] = 1;

      return(i);
    }
  }

  return(-1);
}


int doCursor()
{
  int    i, n;
  double x, y, x1, xdelta, ydelta;
  char   tbuf[256];

  if(isSet(PF_C_FLAG))
  {
    st_report("Clearing Cursor Labels");

    for(i=0;i<MAX_IMAGE_LABELS;i++)
    {
      if(cursor_labels[i])
      {
        sprintf(tbuf, "label delete %d /q", i); 
        parseCmd(tbuf);

        cursor_labels[i] = 0;
      }
    }

    return(0);
  }

  if(isSet(PF_L_FLAG))
  {
    printf("Number  Used    Label_x    Label_y    Color   Direct  Size  Text\n");

    for(i=0;i<MAX_IMAGE_LABELS;i++)
    {
      if(sat.labels[i].used && cursor_labels[i])
      {
        printf(" %2d      %2d    %9.3f  %9.3f      %2d       %2d    %d   %s\n", 
			i,
			sat.labels[i].used,
			sat.labels[i].x,
			sat.labels[i].y,
			sat.labels[i].color,
			sat.labels[i].direct,
			sat.labels[i].size,
			sat.labels[i].label);
      }
    }

    return(0);
  }

  strcpy(tbuf, "Mouse-Click plot to locate");
  if(getMouseClick(tbuf, &x, &y))
  {
    return(1);
  }

  st_report("Temp: %f", env->tcur);
  st_report("Vel : %f", env->vcur);
  st_report("Freq: %f", env->fcur);
  st_report("Chan: %f", env->ccur);

  xdelta = (env->xplotmax-env->xplotmin) / 100.0;
  ydelta = (env->yplotmax-env->yplotmin) /  50.0;

  if(env->x1mode == XMODE_FREQ)
  {
    x1 = env->fcur+xdelta;
  }
  else
  if(env->x1mode == XMODE_VEL)
  {
    x1 = env->vcur+xdelta;
  }
  else  /* chan */
  {
    x1 = env->ccur+xdelta;
  }

  if((n = findNextLabel()) < 0)
  {
    st_fail("No more free labels");
    return(1);
  }
  sprintf(tbuf, "label set %d %f %f 0 2 + /q",          n, x, y); 
  parseCmd(tbuf);

  if((n = findNextLabel()) < 0)
  {
    st_fail("No more free labels");
    return(1);
  }
  sprintf(tbuf, "label set %d %f %f 0 2 Temp: %.3f /q", n, x1, env->tcur - ydelta*1.0, env->tcur); 
  parseCmd(tbuf);

  if((n = findNextLabel()) < 0)
  {
    st_fail("No more free labels");
    return(1);
  }
  sprintf(tbuf, "label set %d %f %f 0 2 Vel : %.3f /q", n, x1, env->tcur - ydelta*2.0, env->vcur ); 
  parseCmd(tbuf);

  if((n = findNextLabel()) < 0)
  {
    st_fail("No more free labels");
    return(1);
  }
  sprintf(tbuf, "label set %d %f %f 0 2 Freq: %.3f /q", n, x1, env->tcur - ydelta*3.0, h0->head.restfreq+env->fcur); 
  parseCmd(tbuf);

  if((n = findNextLabel()) < 0)
  {
    st_fail("No more free labels");
    return(1);
  }
  sprintf(tbuf, "label set %d %f %f 0 2 Chan: %.1f /q", n, x1, env->tcur - ydelta*4.0, env->ccur); 
  parseCmd(tbuf);

  return(0);
}


/************************************** End of define Functions ******************************/

