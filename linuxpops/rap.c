/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

#ifdef TOMMIE

/* From UNipops Cookbook */
Processing Frequency-Switched Data

If you wish to combine the left half of a frequency-switched scan in Array (0) 
with the right half, then use the verb RAP. On completion, RAP places the result 
in Array (0), and sets the adverb OSHIFT equal to the number of channels between 
Signal and Reference frequencies. (At present, RAP only works correctly if the 
frequency shift is an integral number of channels.)

Be careful when using RAP, as its operation is rather different for Green Bank and 
Tucson data.

Green Bank Data : RAP will compute the number of channels to

   "shift and combine" from the frequencies stored
   in the scan header. Thus to perform the
   combination, just type,

   >RAP

Tucson Data : Here, one must set the two adverbs, 

   FS (Frequency offset in MHz for the Signal data) and 
   FR (Frequency offset in MHz for the Reference data) 
   before invoking RAP.  As of Fall 1993, these two values
   are available from the header.  There are three
   frequency offsets stored in the header:
   FOFFSIG, FOFFREF1, and FOFFREF2.  Usually you will
   want to set FS to the value of the FOFFSIG header
   word and FR to the value of the FOFFREF1 header
   word.

   > FS = H0(FOFFSIG)
   > FR = H0(FOFFREF1)
   > RAP

NOTE : Following the application of RAP, the user must decide which half of the 
spectrum is the signal half, and drop the other half via the adverbs BDROP and 
EDROP before displaying the data. If OSHIFT is > 0, then you should set BDROP 
to at least the value of OSHIFT. If OSHIFT is < 0, then you should set EDROP to 
at least the absolute value of OSHIFT.

The adverb DEFMODE indicates how RAP treats undefined data values. An undefined 
data value is one equal to IEEE Inf (Infinity).

If DEFMODE is FALSE, RAP will place an undefined data value at any location where 
either the signal or reference values are undefined (i.e. if channel 100 is 
undefined, as above, then the channel in the result that depends on channel 100 
will also be undefined if DEFMODE is FALSE).

If DEFMODE is TRUE, RAP will place an undefined data value at any location where 
both the signal and reference values are undefined. However, if only one value is 
undefined, then the result will be as if the undefined value were zero (i.e. if 
channel 100 is the only undefined value, then that value will be treated as if 
it were 0).

Most users will leave DEFMODE set to its default value of FALSE.


C-----------------------------------------------------------------------
C                                               RAP
C----------------------------------------------------------------------
  170 CONTINUE
      if(dtwh(c1tel,iptwh).eq.value12m) then
        goto 1709
      else if(dtwh(c1tel,iptwh).eq.value140 .or.
     1        dtwh(c1tel,iptwh).eq.value300) then
        goto 1719
      else
        call oerror(n283, m2, 'RAP')
      endif
c                                        Tucson Version
c                       input adverbs : fr = signal frequency (MHz)
c                                       fs = referenc freq (MHz)
1709  continue
      OSHFT=(FR-FS)/DTWH(C12FR,IPTWH)
c                               oshift number of channels
      if(oshft.eq.0.0) call oerror(n291, m2, 'RAP')
      IF (OSHFT.le.0.0) then
c                               
         IDF=nint(ABS(OSHFT))
         ISTART=DTWH(C12SPN,IPTWH) + IDATOFF
         ISTOP=DTWH(C12SPN,IPTWH) + DTWH(C12NI,IPTWH)
     .         + IDATOFF -IDF - 1
         DO 172 JJ = ISTART,ISTOP
            if(okreal4(TWH(JJ,IPTWH)) .and.
     .         okreal4(TWH(JJ+IDF,IPTWH))) then
                 TWH(JJ,IPTWH)=(TWH(JJ,IPTWH)-TWH(JJ+IDF,IPTWH))/2.0
            else if (defmode .ge. 0.5 .and. okreal4(TWH(JJ+IDF,IPTWH))) then
                 twh(jj,iptwh)=-TWH(JJ+IDF,IPTWH)
            else if (defmode .lt. 0.5) then
                 twh(jj,iptwh)=rinfinity()
            endif
  172       CONTINUE
         DTWH(C12NI,IPTWH)=DTWH(C12NI,IPTWH)-IDF
      else
c
         IDF=nint(OSHFT+0.5)
         ISTART=DTWH(C12SPN,IPTWH)+IDATOFF+IDF
         ISTOP=DTWH(C12SPN,IPTWH)+DTWH(C12NI,IPTWH)+
     .         IDATOFF - 1
         DO 177 JJ=ISTOP,ISTART,-1
            if(okreal4(TWH(JJ,IPTWH)) .and.
     .         okreal4(TWH(JJ-IDF,IPTWH))) then
                 TWH(JJ,IPTWH)=(TWH(JJ,IPTWH)-TWH(JJ-IDF,IPTWH))/2.0
            else if (defmode .ge. 0.5 .and. okreal4(TWH(JJ-IDF,IPTWH))) then
                 twh(jj,iptwh)=-TWH(JJ-IDF,IPTWH)
            else if (defmode .lt. 0.5) then
                 twh(jj,iptwh)=rinfinity()
            endif
  177       CONTINUE
         DTWH(C12SPN,IPTWH)=DTWH(C12SPN,IPTWH)+IDF
         DTWH(C12NI,IPTWH)=DTWH(C12NI,IPTWH)-IDF
      endif
c
      DF=-(FS)/DTWH(C12FR,IPTWH)
      IDF=nint(DF)
c                               set ref point
      DTWH(C12RP,IPTWH) = DTWH(C12RP,IPTWH) + FLOAT(IDF)
      GO TO 99

#endif


int do_FreqSwitch()
{
  int i, idf, istart, istop;
  double oshift, fs, fr, df;

  if(isSet(PF_A_FLAG))				/* Get offsets from header */
  {
    fs = h0->head.foffsig;
    fr = h0->head.foffref1;
  }
  else						/* User sets them himself */
  {
    fs = env->fs;
    fr = env->fr;
  }

  if(fs == 0.0 && fr == 0.0)
  {
    printH_FlagHelp(args->firstArg);

    return(1);
  }

  oshift = (fr-fs) / h0->head.freqres;

  if(oshift == 0.0)
  {
    st_report("Diff between FS & FR = 0.0");

    return(1);
  }

  if(oshift < 0.0)
  {
    idf    = (int)fabs(round(oshift));
    istart = 0;
    istop  = (int)(h0->head.noint - (double)idf);

    for(i=istart;i<istop;i++)
    {
	/* Check for NAN here */
      h0->yplot1[i] = (h0->yplot1[i] - h0->yplot1[i+idf]) / 2.0;
    }

    h0->head.noint -= (double)idf;
  }
  else
  {
    idf    = (int)round(oshift + 0.5);
    istart = idf;
    istop  = (int)(h0->head.noint - 1.0);

    for(i=istop;i>istart;i--)
    {
	/* Check for NAN here */
      h0->yplot1[i] = (h0->yplot1[i] - h0->yplot1[i+idf]) / 2.0;
    }

    h0->head.spn   += (double)idf;
    h0->head.noint -= (double)idf;
  }

  df  = fs / h0->head.noint;
  idf = (int)round(df);

  h0->head.refpt += (double)idf;

  return(0);
}

