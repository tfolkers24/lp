/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

#include <gsl/gsl_errno.h>
#include <gsl/gsl_fft_real.h>
#include <gsl/gsl_fft_halfcomplex.h>

/* Changes:

     Added last_plot to fft plot; twf; Jan 05, 2016 

     Added the xb and xe axis to fft plot; twf; Jan 05, 2016 

 */

unsigned int powerOfTwo;

gsl_fft_real_wavetable        *fft_real = NULL;
gsl_fft_real_workspace        *fft_work = NULL;
gsl_fft_halfcomplex_wavetable *fft_hc   = NULL;
double                         fft_data[MAX_CHANS];

struct FFT_INFO last_fft;

int lp_fftcomp()
{
  int i, n, ret;
  double fr, f, bw;
  struct LP_IMAGE_INFO *image = h0;

  n = (int)image->head.noint;

  if(n == 0)
  {
    st_fail("No data loaded");

    return(0);
  }

  bzero((char *)&fft_data[0], sizeof(fft_data));

  st_report("FFT: N = %d", n);

  /* Copy floats to double array */
  for(i=0;i<n;i++)
  {
    fft_data[i] = image->yplot1[i];
  }
 
  /* Free up work and real structs if already allocated */
  if(fft_work)
  {
    gsl_fft_real_workspace_free (fft_work);
 
    fft_work = NULL;
  }

  if(fft_real)
  {
    gsl_fft_real_wavetable_free (fft_real);
 
    fft_real = NULL;
  }

  fft_work = gsl_fft_real_workspace_alloc (n);
  fft_real = gsl_fft_real_wavetable_alloc (n);

  ret = gsl_fft_real_transform (fft_data, 1, n, fft_real, fft_work);

  st_report("FFT: Function returned %d", ret);

  if(!ret)
  {
    fr = fabs(image->head.freqres);

	/* Double the n because that's the original BW */
	/* in this version of fft the results are 4 times??? */
    bw = (fr * (double)n * 2.0);
    f = 1.0 / bw;

    st_report("Freqres = %f MHz / Chan, BW = %f MHz", fr, fr * (double)n);

    strcpy(last_fft.magic, "FFT_INFO");

    bcopy((char *)&image->head, (char *)&last_fft.head, sizeof(last_fft.head));

    /* Skip the 0th lag */
    for(i=0;i<n/2;i++)
    {
      last_fft.xdata[i] = (double)i * f;
      last_fft.ydata[i] = fft_data[i] / bw * 2.0;
    }

    last_fft.num = n/2;

    if(env->fft_xb || env->fft_xe)
    {
      st_report("FFT Display Truncated");
      last_fft.xset = 1;
      last_fft.xb   = env->fft_xb;
      last_fft.xe   = env->fft_xe;
    }
    else
    {
      last_fft.xset = 0;
      last_fft.xb   = 0.0;
      last_fft.xe   = 0.0;
    }

    if(xgraphic)
    {
      sock_write(xgraphic, (char *)&last_fft, sizeof(last_fft));

      last_plot = LAST_PLOT_SPEC_FFT;

      st_report("Sent FFT to xgraphic");
    }
  }
  else
  {
    st_fail("FFT Failed");
  }

  return(0);
}


int findExactFFTFreq(mx)
double mx;
{
  int i, n, mindx=0;
  double min = 999999999.9;

  n = last_fft.num;

  st_report("FFT Num of Values           %d", n);
  for(i=0;i<n;i++)
  {
    if(fabs(last_fft.xdata[i] - mx) < min)
    {
      min = fabs(last_fft.xdata[i] - mx);
      mindx = i;

    }
  }

  st_report("FFT Found Index             %d", mindx);

  return(mindx);
}


int lp_fftpick()
{
  int i, n, nc, fft_indx;
  float *f1;
  double f, freeD, wireD, fft_freq;

  do
  {
    nc = getNumericResponse("Enter Number of Freqs [ Up to 8]: ");
  }
  while(nc < 0);

  if(nc > 8)
  {
    st_fail("Count out of range: 0x%x", nc);

    return(1);
  }

  for(i=0;i<nc;i++)
  {
    _doTCur("Click the Left Mouse Button on Desired Freq");

    fft_indx = findExactFFTFreq(last_mouse_x);
    fft_freq = last_fft.xdata[fft_indx];

    f = 1.0 / fft_freq;

    f *= 1e6;  /* Convert to Hz */

    freeD = 1.0 / f * SPEED_OF_LIGHT;
    wireD = 1.0 / f * SPEED_OF_LIGHT_COAX;

    freeD /= 2.0;
    wireD /= 2.0;

    st_report("MHz-1 Selected:        %7.4f uS", fft_freq);
    st_report("Freq Selected:         %7.4f MHz", f /= 1e6);
    st_report("Free Space Distance:   %7.4f Meters", freeD);
    st_report("Coaxial Wire Distance: %7.4f Meters", wireD);

    /* Zero out the bad freq */
    fft_data[fft_indx] = 0.0;
  }

  if(nc)
  {
    n = last_fft.num * 2;

    if(fft_work->n != n)
    {
      st_fail("FFT Elements don't match: %d vs %d", n, fft_work->n);
      return(0);
    }

    /* Convert back to Freq domain */
    fft_hc = gsl_fft_halfcomplex_wavetable_alloc(n);

    gsl_fft_halfcomplex_inverse(fft_data, 1, n, fft_hc, fft_work);

    gsl_fft_halfcomplex_wavetable_free(fft_hc);

    f1 = &h0->yplot1[0];
    for(i=0;i<n;i++,f1++)
    {
      *f1 = (float)fft_data[i];
    }

    xx();
  }

  return(0);
}


int lp_fftcut()
{
  int start, end;
  int i, n;
  float *f1;

  _doTCur("Click the Left Mouse Button on Left Freq");

  start = findExactFFTFreq(last_mouse_x);

  _doTCur("Click the Left Mouse Button on Right Freq");

  end = findExactFFTFreq(last_mouse_x);

  if(end > start)
  {
    for(i=start;i<end;i++)
    {
      fft_data[i] = 0.0;
    }
  }
  else
  {
    st_fail("Ending freq < Starting");
    return(1);
  }

  n = last_fft.num * 2;

  if(fft_work->n != n)
  {
    st_fail("FFT Elements don't match: %d vs %d", n, fft_work->n);
    return(0);
  }

    /* Convert back to Freq domain */
  fft_hc = gsl_fft_halfcomplex_wavetable_alloc(n);

  gsl_fft_halfcomplex_inverse(fft_data, 1, n, fft_hc, fft_work);

  gsl_fft_halfcomplex_wavetable_free(fft_hc);

  f1 = &h0->yplot1[0];
  for(i=0;i<n;i++,f1++)
  {
    *f1 = (float)fft_data[i];
  }

  xx();

  return(0);
}


int lp_fft()
{
  if(isSet(PF_A_FLAG))
  {
    if(strlen(args->secondArg))
    {
      env->fft_xb = atof(args->secondArg);
    }
    else
    {
      st_fail("Missing arg for 'fft /a xx.x xx.x'");
      return(1);
    }

    if(strlen(args->thirdArg))
    {
      env->fft_xe = atof(args->thirdArg);
    }
    else
    {
      st_fail("Missing arg for 'fft /a xx.x xx.x'");
      return(1);
    }

    return(0);
  }

  if(isSet(PF_C_FLAG))
  {
    lp_fftcut();
    return(0);
  }

  if(isSet(PF_P_FLAG))
  {
    lp_fftpick();
    return(0);
  }

  if(isSet(PF_R_FLAG))
  {
    st_report("Reseting fft plot axis to defaults");

    env->fft_xb = 0.0;
    env->fft_xe = 0.0;

    return(0);
  }

	/* else do an actual fft */
  lp_fftcomp();

  return(0);
}
