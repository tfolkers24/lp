/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

#define MAX_DATA_CHANNELS  8192
#include "ds_image_info.h"

int doingOTF = 0;

char *loadChoiceStrings[] = { 	"preview",	/* 0 */
				"fast",		/* 1 */
				"step",		/* 2 */
				 NULL};

char *otfColorChoice[] = { "gray", 	/* 0 */
			   "pseudo", 	/* 1 */
			   "flame1", 	/* 2 */
			   "flame2", 	/* 3 */
			   "flame3", 	/* 4 */
			   "flame4", 	/* 5 */
			   "flame5", 	/* 6 */
			   "flame6", 	/* 7 */
			    NULL };


int to_dataserv(buf)
char *buf;
{
  int ret;

  if(buf && strlen(buf) > 1)
  {
    if(doingOTF)
    {
      st_report("To DATASERV: %s", buf);

      if(dataserv == 0)
      {
        dataserv = sock_connect("LINUXPOPS_DATASERV");
      }

      ret = sock_send(dataserv, buf);

      if(!ret)
      {
        st_fail("No connection to OTF client present");

	doingOTF = 0;

	return(1);
      }
    }
    else
    {
      st_fail("No viable OTF client present");

      return(1);
    }
  }
  else
  {
    st_fail("to_dataserv() called with no buffer");

    return(1);
  }

  return(0);
}


int doOTFHelp()
{
  char tbuf[256];

  sprintf(tbuf, "help");

  to_dataserv(tbuf);

  return(0);
}


int configFromHeader()
{
  double dish;

  if(h0->head.scan != 0.0)
  {
    if(strncmp(h0->head.obsmode, "LINEOTF ", 8))
    {
      st_fail("No OTF Scan Loaded");
      return(1);
    }
  }
  else
  {
    st_fail("No Scan Loaded");
    return(1);
  }

  st_report("Configuring OTF map from Header");

  env->otf_if_choice = getFeed(h0->head.scan) - 1;

  strncpy(env->source, h0->head.object, 16);
  env->source[16] = '\0';
  strcmprs(env->source);

  if(env->issmt)
  {
    dish = 10.0;
  }
  else
  {
    dish = 12.0;
  }

  if(env->dish_size)
  {
    dish = env->dish_size;
    st_report("fwhm(): Using Non Standard Dish Size: %.2f M", dish);
  }
  else
  {
    st_report("fwhm(): Using Dish Size: %.2f M", dish);
  }

  env->otf_nyquistra  = (2.997925e8 / (2.0 * dish * h0->head.restfreq)) * 0.2062648;
  env->otf_nyquistdec = env->otf_nyquistra * 0.90;

  env->otf_map_ra  = h0->head.xsource;
  env->otf_map_dec = h0->head.ysource;

  env->otf_cols      = h0->head.noxpts;
  env->otf_rows      = h0->head.noypts;

  env->otf_map_xcell = h0->head.deltaxr;
  env->otf_map_ycell = h0->head.deltayr;

  env->otf_map_xsize = (env->otf_cols * env->otf_nyquistra)  / 3600.0;
  env->otf_map_ysize = (env->otf_rows * env->otf_nyquistdec) / 3600.0;

  if(env->otf_size_factor == 0.0)
  {
    env->otf_size_factor = 1.0;
  }

  if(env->otf_size_factor > 0.0) 					/* User supplied ?? */
  {
    st_report("Scaling created map by: %.2f", env->otf_size_factor);

    env->otf_map_xsize *= env->otf_size_factor;
    env->otf_map_ysize *= env->otf_size_factor;
  }

  env->otf_bint_chan = (int)(h0->head.noint / 2.0 - 2.0);
  env->otf_eint_chan = (int)(h0->head.noint / 2.0 + 2.0);

  env->otf_3d_bchan  = (int)(h0->head.noint / 2.0 - 25.0);
  env->otf_3d_echan  = (int)(h0->head.noint / 2.0 + 25.0);
  env->otf_3d_interp = 8;

  env->otf_movie_bchan  = (int)(h0->head.noint / 2.0 - 40.0);
  env->otf_movie_echan  = (int)(h0->head.noint / 2.0 + 40.0);
  env->otf_movie_nchan  = 2;
  env->otf_delay        = 0.0;

  env->otf_baselines      = 1; /* True */
  env->otf_color	  = 1; /* Pseudo */
  env->otf_ignore_color	  = 1; /* Ignore for now */
  env->otf_ephem_obj	  = 0; /* Not an Ephem Obj */
  env->otf_chan_map_cent  = (int)(h0->head.noint / 2.0);
  env->otf_chan_map_cols  = 3;
  env->otf_chan_map_rows  = 3;
  env->otf_chan_map_avg   = 2;
  env->otf_single_spec    = (int)(h0->head.noint / 2.0);
  env->otf_noint          = (int)(h0->head.noint);

  env->otf_map_velocity   = h0->head.velocity;
  env->otf_map_restfreq   = h0->head.restfreq;
  env->otf_map_freqres    = h0->head.freqres;

  if(env->otf_single_bin == 0)
  {
     env->otf_single_bin = 2;
  }

  if(env->otf_gamma == 0.0)
  {
     env->otf_gamma = 1.0;
  }

  if(env->otf_grid_scale == 0.0)
  {
     env->otf_grid_scale = 1.0;
  }

  return(0);
}



int sendFilenames()
{
  char tbuf[512];

  if(doingOTF)
  {
    sprintf(tbuf, "filename %s", env->filename);
    to_dataserv(tbuf);

    sprintf(tbuf, "gfilename %s", env->gfilename);
    to_dataserv(tbuf);
  }

  return(0);
}


int sendBaselines()
{
  int i;
  char bbuf[1024], tbuf[256];

  sprintf(bbuf, "baselines ");

  for(i=0;i<4;i++)
  {
    if(env->baselines[i][0] > 0 && env->baselines[i][1] > 0)
    {
      sprintf(tbuf, "%d %d ", env->baselines[i][0], env->baselines[i][1]);
    }
    else
    {
      sprintf(tbuf, "-1 -1 ");
    }

    strcat(bbuf, tbuf);
  }

  to_dataserv(bbuf);

  sprintf(tbuf, "otfbaseline %d", env->otf_baselines);
  to_dataserv(tbuf);

  return(0);
}


int sendParams()
{
  char tbuf[256], execBuf[256];

  if(doingOTF)
  {
    if(notSet(PF_K_FLAG)) /* Quick Flag */
    {
      sendFilenames();

      sendBaselines();
    }

    sprintf(tbuf, "otfcutoff %d", env->otf_cutoff);
    to_dataserv(tbuf);

    sprintf(tbuf, "ig_repaints %d", env->otf_ignore_repaint);
    to_dataserv(tbuf);

    sprintf(tbuf, "otfbeamsize %d %f %f", env->otf_show_beam_size, env->otf_beam_ra * 15.0, env->otf_beam_dec);
    to_dataserv(tbuf);

    sprintf(tbuf, "otfifchoice %d", env->otf_if_choice);
    to_dataserv(tbuf);

    sprintf(tbuf, "otfchans %d %d", env->otf_bint_chan, env->otf_eint_chan);
    to_dataserv(tbuf);
    
    sprintf(tbuf, "otfsinglechan %d", env->otf_single_spec);
    to_dataserv(tbuf);
    
    sprintf(tbuf, "otfsinglebin %d", env->otf_single_bin);
    to_dataserv(tbuf);
    
    sprintf(tbuf, "otfgridscale %f", env->otf_grid_scale);
    to_dataserv(tbuf);
    
    sprintf(tbuf, "otfgamma %f", env->otf_gamma);
    to_dataserv(tbuf);


    if(env->otf_map_velocity == 0.0)
    {
      st_warn("otf_map_velocity is Zero!");
    }

    sprintf(tbuf, "otfgridvelocity %f", env->otf_map_velocity);
    to_dataserv(tbuf);


    if(env->otf_map_freqres == 0.0)
    {
      st_fail("otf_map_freqres is Zero!");
      return(1);
    }
    sprintf(tbuf, "otfgridfreqres %f", env->otf_map_freqres);
    to_dataserv(tbuf);


    if(env->otf_map_restfreq == 0.0)
    {
      st_fail("otf_map_restfreq is Zero!");
      return(1);
    }
    sprintf(tbuf, "otfgridrestfreq %f", env->otf_map_restfreq);
    to_dataserv(tbuf);


    if(env->otf_load_choice >= 0 && env->otf_load_choice < 3)
    {
      sprintf(tbuf, "otfloadchoice %s", loadChoiceStrings[env->otf_load_choice]);
      to_dataserv(tbuf);
    }
    else
    {
      st_fail("otf_load_choice of %d, out of range: 0-2", env->otf_load_choice);
      return(1);
    }

    if(notSet(PF_K_FLAG)) /* Quick Flag */
    {
    	/* Do color last as it takes a few seconds over network */
      if(env->otf_color >= 0 && env->otf_color < 7)
      {
        if(!env->otf_ignore_color)
        {
          sprintf(tbuf, "otfcolorchoice %s", otfColorChoice[env->otf_color]);
          to_dataserv(tbuf);

	/* Because changing colors can take a long time over the net, wait till done */
          sprintf(execBuf, "helperwait done 30");
          parseCmd(execBuf);
        }
        else
        {
	  st_report("Skipping OTF Color Selection; 'otf_ignore_color' Non-Zero");
        }
      }
      else
      {
        st_fail("otf_color of %d, out of range: 0-7", env->otf_color);

        return(1);
      }
    }
  }
  else
  {
    st_fail("You must start the OTF sub-system first");

    return(1);
  }

  return(0);
}


int otfInit(init)
int init;
{
  char tbuf[1024];

  st_report("Initializing OTF Dataserv");

  if(doingOTF)
  {
    to_dataserv("otflockgrid 0"); /* Onlock the gridder */

    if(init)
    {
      to_dataserv("otfgridinit");   /* Initialize it */
    }

    if(sendParams())
    {
      return(1);
    }

    sprintf(tbuf, "otfautoconfig 1");
    to_dataserv(tbuf);
    
    sprintf(tbuf, "otfgridra %f", env->otf_map_ra);
    to_dataserv(tbuf);
    
    sprintf(tbuf, "otfgriddec %f", env->otf_map_dec);
    to_dataserv(tbuf);
    
    sprintf(tbuf, "otfgridsizera %f", env->otf_map_xsize);
    to_dataserv(tbuf);
    
    sprintf(tbuf, "otfgridsizedec %f", env->otf_map_ysize);
    to_dataserv(tbuf);
    
    sprintf(tbuf, "otfgridcellsizera %f", env->otf_nyquistra);
    to_dataserv(tbuf);
    
    sprintf(tbuf, "otfgridcellsizedec %f", env->otf_nyquistdec);
    to_dataserv(tbuf);


    env->otf_cols = env->otf_map_xsize * 3600.0 / env->otf_nyquistra;
    env->otf_rows = env->otf_map_ysize * 3600.0 / env->otf_nyquistdec;

    if(env->otf_size_factor == 0.0)
    {
      env->otf_size_factor = 1.0;
    }

    env->otf_cols = (int)((float)env->otf_cols * env->otf_size_factor);
    env->otf_rows = (int)((float)env->otf_rows * env->otf_size_factor);

    if(env->otf_cols > 0 && env->otf_rows > 0 && env->otf_noint > 0)
    {
      sprintf(tbuf, "otfgridcols %d", env->otf_cols);
      to_dataserv(tbuf);

      sprintf(tbuf, "otfgridrows %d", env->otf_rows);
      to_dataserv(tbuf);

      sprintf(tbuf, "otfgridnoint %d", env->otf_noint);
      to_dataserv(tbuf);
    }
    else
    {
      st_fail("Bad config:\n cols = %d or row = %d or noint = %d", env->otf_cols, env->otf_rows, env->otf_noint);
      return(1);
    }

    to_dataserv("otfgridinit");      /* Init grid based on passed values */

    to_dataserv("otflockgrid 0");    /* Lock the gridder */

  }
  else
  {
    st_fail("You must start the OTF sub-system first");

    return(1);
  }

  return(0);
}



int otfParamList()
{
  char tbuf[256];

  sprintf(tbuf, "dl /k otf");

  parseCmd(tbuf);

  return(0);
}


int otfLoad(norepaint)
int norepaint;
{
  char tbuf[256];

  if(norepaint)
  {
    sprintf(tbuf, "otfgridload %.0f %.0f -1.0", env->bscan, env->escan);
  }
  else
  {
    sprintf(tbuf, "otfgridload %.0f %.0f", env->bscan, env->escan);
  }

  to_dataserv(tbuf);

  return(0);
}


int otfMovie(rec, interp)
int rec, interp;
{
  char tbuf[512], dir[256], *cp1, *cp2;

  if(args->ntokens > 1)
  {
    strcpy(dir, args->secondArg);
  }
  else
  {
    strcpy(dir, "Forw");
  }

  if(env->otf_movie_bchan == 0 || env->otf_movie_echan == 0 || env->otf_movie_nchan == 0)
  {
    st_fail("otfMovie(): otf_movie_* must not be 0");

    otfParamList();

    return(1);
  }

  if((env->otf_movie_bchan >= env->otf_movie_echan) /* || env->otf_movie_echan >= h0->head.noint */)
  {
    st_fail("otfMovie(): otf_movie_bchan must be less that otf_movie_echan");

    otfParamList();

    return(1);
  }

  if(rec)
  {
    cp1 = "record";
  }
  else
  {
    cp1 = " ";
  }

  if(interp)
  {
    cp2 = "interp";
  }
  else
  {
    cp2 = " ";
  }

  sprintf(tbuf, "otfvelrun %d %d %d %s %f %s %s", 
						env->otf_movie_bchan, 
						env->otf_movie_echan, 
						env->otf_movie_nchan, 
						dir,
						env->otf_delay, cp1, cp2);

  to_dataserv(tbuf);

  return(0);
}


int otfChanMap()
{
  char tbuf[256];

  if(env->otf_chan_map_cent == 0 || 
     env->otf_chan_map_rows == 0 || 
     env->otf_chan_map_cols == 0 || 
     env->otf_chan_map_avg  == 0)
  {
    st_fail("otfChanMap(): otf_chan_map_* must not be 0");

    otfParamList();

    return(1);
  }

  sprintf(tbuf, "otfchannelmap %d %d %d %d", env->otf_chan_map_cent, 
					     env->otf_chan_map_cols, 
					     env->otf_chan_map_rows, 
					     env->otf_chan_map_avg);

  to_dataserv(tbuf);

  return(0);
}


int doOTF3Dcube()
{
  double cutoff;
  char tbuf[256];

  st_report("Performing 3d Cube");

  if(env->otf_3d_vrange == 0.0)
  {
    env->otf_3d_vrange = 1.0;
  }

  if(env->otf_3d_urms == 0.0)
  {
    env->otf_3d_urms = 100.0;
  }

  cutoff = (double)env->otf_cutoff / 100.0;

  sprintf(tbuf, "otf3dcube %d %d %d %.3f %.2f %.2f %.3f", 
		env->otf_3d_bchan, env->otf_3d_echan, 
		env->otf_3d_interp, env->otf_3d_vrange, 
		cutoff, env->otf_3d_urms, env->otf_zrange_factor);

  to_dataserv(tbuf);

  return(0);
}



int doOTF3Dplot()
{
  int  interp = 0;
  char tbuf[256];

  if(isSet(PF_2_FLAG)) 		/* Interpolate level ?? */
  {
    interp  = 2;
  }
  else
  if(isSet(PF_3_FLAG))
  {
    interp  = 3;
  }
  else
  if(isSet(PF_4_FLAG))
  {
    interp  = 4;
  }
  else
  if(isSet(PF_5_FLAG))
  {
    interp  = 5;
  }

  st_report("Performing 3d Plot");

  sprintf(tbuf, "otf3dplot %s %d", session, interp);

  to_dataserv(tbuf);

  return(0);
}




int autoSetEBscan()
{
  int i, b = 0, e = 0, first = 1;
  struct HEADER *p;

  st_report("autoSetEBscan(): Locating LINEOTF scans for source: %s", env->source);

  p = &sa[0];
  for(i=0;i<env->numofscans;i++,p++)
  {
    if(!strncmp(p->obsmode, "LINEOTF", 7) && !strncmp(p->object, env->source, strlen(env->source)))
    {
      if(first)
      {
        b = (int)p->scan;
        first = 0;
      }

      e = (int)p->scan;
    }
  }

  if( b && e)
  {
    env->bscan = (double)b;
    env->escan = (double)e;

    st_report("autoSetEBscan(): Setting bscan: %.0f, escan: %.0f", env->bscan, env->escan);
  }
  else
  {
    st_report("autoSetEBscan(): No OTF scans found for %s", env->source);
  }

  return(0);
}


int reassembleArgs(buf)
char *buf;
{
  int i, n;
  char dbuf[1024], tbuf[512];

  n = args->ntokens;

  bzero(dbuf, sizeof(dbuf));

  for(i=1;i<n;i++)
  {
    sprintf(tbuf, "%s ", args->tok[i]);
    strcat(dbuf, tbuf);
  }

  strcpy(buf, dbuf);

  return(0);
}


int doOTFcmd()
{
  char dbuf[1024];

  reassembleArgs(dbuf);  /* Reassemble the args into a command */

  to_dataserv(dbuf);

  return(0);
}


int otfGrabSpectralSum()
{
  int i, n, ret=0;
  char tbuf[256], dbuf[1024];
  struct LP_IMAGE_INFO *im = &H0;
  struct DS_IMAGE_INFO *ds;
  extern struct DS_IMAGE_INFO ds_image_info;

  ds = &ds_image_info;

  reassembleArgs(tbuf);

  if(strlen(tbuf) > 1)
  {
    sprintf(dbuf, "otfgridsum %s", tbuf);
  }
  else
  {
    strcpy(dbuf, "otfgridsum");
  }

  to_dataserv(dbuf);

  subArgs(2, "5"); /* Wait 5 seconds */
  args->ntokens = 2;

  ret = helperBinaryWait();

  st_report("helperBinaryWait() returned %d", ret);

  if(!ret)
  {
    if(ds->noint == 0)
    {
      st_report("Dataserv Image Empty");
      return(1);
    }

    subArgs(2, "H0"); /* Clear out the H0 image */
    args->ntokens = 2;
    zeroImage();

    copyEnv2Image(im); /* Copy in most items */

    im->ainfo.scanct = ds->scanct;
    im->ainfo.bscan  = ds->bscan;
    im->ainfo.escan  = ds->escan;

    im->binfo.a      = ds->a;
    im->binfo.b      = ds->b;
    im->binfo.rms    = ds->rms;

    bcopy((char *)&ds->head, (char *)&im->head, sizeof(struct HEADER));

    strncpy(im->head.object,  ds->source, 16);
    im->head.object[15] = '\0';
    strncpy(im->head.coordcd, ds->coordcd, 8);
    im->head.coordcd[7] = '\0';

    im->head.noint   = (double)ds->noint;
    im->head.datalen = im->head.noint * sizeof(float);
    im->head.stsys   = ds->tsys;

    n = ds->noint;

    for(i=0;i<n;i++)
    {
      im->yplot1[i] = ds->ydata[i];
    }

    im->narray = 1;
    im->nsize  = n * sizeof(float);

    sendImageStruct(im, env->mode);
  }

  validate(PS);

  return(ret);
}


int doOTFSlice()
{
  int    i, n, ret=0, dir = 0;
  char   dbuf[1024], dirStr[256];
  struct LP_IMAGE_INFO *im;
  struct DS_IMAGE_INFO *ds;
  extern struct DS_IMAGE_INFO ds_image_info;

  im = &H0;
  ds = &ds_image_info;

  if(args->ntokens < 2)
  {
    st_print("Missing number arg\n");

    printH_FlagHelp(args->firstArg);

    return(1);
  }

  if(isSet(PF_R_FLAG))
  {
    strcpy(dirStr, "row");
    dir = 1;
  }
  else
  if(isSet(PF_C_FLAG))
  {
    strcpy(dirStr, "col");
    dir = 2;
  }
  else
  {
    st_print("Missing direction flag\n");

    printH_FlagHelp(args->firstArg);

    return(1);
  }

  sprintf(dbuf, "otfslice %s %d", dirStr, atoi(args->secondArg));

  to_dataserv(dbuf);

  subArgs(2, "5");
  args->ntokens = 2;

  ret = helperBinaryWait();

  st_report("helperBinaryWait() returned %d", ret);

  if(!ret)
  {
    if(ds->noint == 0)
    {
      st_report("Dataserv Image Empty");
      return(1);
    }

    doCV(); 						/* Set the plot axis */

    subArgs(2, "H0"); 					/* Clear out the H0 image */
    args->ntokens = 2;
    zeroImage();

    copyEnv2Image(im); 					/* Copy in most items */

    im->ainfo.scanct = ds->scanct;
    im->ainfo.bscan  = ds->bscan;
    im->ainfo.escan  = ds->escan;

    im->binfo.a      = ds->a;
    im->binfo.b      = ds->b;
    im->binfo.rms    = ds->rms;

    bcopy((char *)&ds->head, (char *)&im->head, sizeof(struct HEADER));

    strncpy(im->head.object,  ds->source, 16);
    im->head.object[15] = '\0';
    strncpy(im->head.coordcd, ds->coordcd, 8);
    im->head.coordcd[7] = '\0';

    im->head.noint   = (double)ds->noint;
    im->head.datalen = im->head.noint * sizeof(float);
    im->head.stsys   = ds->tsys;

    n = ds->noint;

    for(i=0;i<n;i++)
    {
      im->yplot1[i] = ds->ydata[i];
    }

    im->narray = 1;
    im->nsize  = n * sizeof(float);

    im->xaxisotfoff = dir;

    computeXaxis(im, 1);

    sendImageStruct(im, env->mode);

    last_plot = LAST_PLOT_OTF_CUT;
  }

  validate(PS);

  return(ret);
}


int otfGrabSpectralPlot()
{
  int i, n, ret=0;
  char tbuf[256], dbuf[1024];
  struct LP_IMAGE_INFO *im;
  struct DS_IMAGE_INFO *ds;
  extern struct DS_IMAGE_INFO ds_image_info;

  im = &H0;
  ds = &ds_image_info;

  reassembleArgs(tbuf);

  if(strlen(tbuf) > 1)
  {
    sprintf(dbuf, "otfsendplot %s", tbuf);
  }
  else
  {
    strcpy(dbuf, "otfsendplot");
  }

  to_dataserv(dbuf);

  subArgs(2, "5");
  args->ntokens = 2;

  ret = helperBinaryWait();

  st_report("helperBinaryWait() returned %d", ret);

  if(!ret)
  {
    if(ds->noint == 0)
    {
      st_report("Dataserv Image Empty");
      return(1);
    }

    subArgs(2, "H0"); /* Clear out the H0 image */
    args->ntokens = 2;
    zeroImage();

    copyEnv2Image(im); /* Copy in most items */

    im->ainfo.scanct = ds->scanct;
    im->ainfo.bscan  = ds->bscan;
    im->ainfo.escan  = ds->escan;

    im->binfo.a      = ds->a;
    im->binfo.b      = ds->b;
    im->binfo.rms    = ds->rms;

    bcopy((char *)&ds->head, (char *)&im->head, sizeof(struct HEADER));

    strncpy(im->head.object,  ds->source, 16);
    im->head.object[15] = '\0';
    strncpy(im->head.coordcd, ds->coordcd, 8);
    im->head.coordcd[7] = '\0';

    im->head.noint   = (double)ds->noint;
    im->head.datalen = im->head.noint * sizeof(float);
    im->head.stsys   = ds->tsys;

    n = ds->noint;

    for(i=0;i<n;i++)
    {
      im->yplot1[i] = ds->ydata[i];
    }

    im->narray = 1;
    im->nsize  = n * sizeof(float);

    sendImageStruct(im, env->mode);
  }

  validate(PS);

  return(ret);
}



int setImage(i)
int i;
{
  switch(i)
  {
    case 0: _h0(); break;
    case 1: _h1(); break;
    case 2: _h2(); break;
    case 3: _h3(); break;
    case 4: _h4(); break;
    case 5: _h5(); break;
    case 6: _h6(); break;
    case 7: _h7(); break;
    case 8: _h8(); break;
    case 9: _h9(); break;

    default: st_fail("Unknown H value %d", i); return(1); break;
  }

  return(0);
}


int doOTFdisplay()
{
  int   i, j, noint, ret, old, snum, navg, numSpec;
  float  *w, *o, *s, *g;
  double gscan, oscan, scan;
  extern float *otfData;

  old = atoi(h0->name+1);

  if(args->ntokens > 1)
  {
    scan = atof(args->secondArg);
  }
  else
  {
    printH_FlagHelp(args->firstArg);

    return(0);
  }

  /* Read OTF into H7 */
  _h7();

  ret = read_scan(scan, env->filename, h0->yplot1, &h0->head, 0);
  env->baselineremoved = 0;

  if(ret == -1)
  {
    st_fail("OTF scan: %.2f found in %s", scan, env->filename);

    setImage(old);  /* Point back to the original image */

    return(0);
  }

  if(strncmp(h0->head.obsmode, "LINEOTF", 7))
  {
    st_fail("Scan: %.2f, Not OTF; it's %8.8s", scan, h0->head.obsmode);

    return(0);
  }

  gscan   = h0->head.gains;
  oscan   = h0->head.offscan;
  noint   = (int)h0->head.noint;
  numSpec = (int)h0->head.datalen / (sizeof(float) * (noint+5));

  st_report("OTF Scan: %.2f has %d channels & %d Spectra", h0->head.scan, noint, numSpec);

   /* Read gains into H8 */
  _h8();

  ret = read_scan(gscan, env->gfilename, h0->yplot1, &h0->head, 0);
  env->baselineremoved = 0;

  if(ret == -1)
  {
    st_fail("Gain scan: %.2f found in %s", gscan, env->gfilename);

    setImage(old);  /* Point back to the original image */

    return(0);
  }

  /* read Offscan into H9 */
  _h9();

  ret = read_scan(oscan, env->filename, h0->yplot1, &h0->head, 0);
  env->baselineremoved = 0;

  if(ret == -1)
  {
    st_fail("Off scan: %.2f found in %s", oscan, env->gfilename);

    setImage(old);  /* Point back to the original image */

    return(0);
  }

  /* Check for requested spectra */
  if(args->ntokens > 2)
  {
    snum = atoi(args->thirdArg);

    if(snum < 0 || snum >= numSpec)
    {
      st_fail("Requected spectra: %d, < 0 or > %d", snum, numSpec);
      setImage(old);

      return(1);
    }
  }
  else /* Else just show first one */
  {
    snum = 0;
  }

  /* Check for requested average */
  if(args->ntokens > 3)
  {
    navg = atoi(args->fourthArg);

    if(navg < 1 || (snum+navg) >= numSpec)
    {
      st_fail("Requected average or start+average, %d, < 1 or > %d", snum+navg, numSpec);
      setImage(old);

      return(1);
    }

    st_report("Averaging %d samples", navg);
  }
  else /* Else no averaging */
  {
    navg = 1;
  }

  /* Do our work in H0 */
  _h0();

  hzero(h0);

  _copyImage(&H7, h0); /* Copy header back to H0 */
  
  /* Doctor up the header */
  h0->head.datalen = h0->head.noint * sizeof(float);
  h0->head.inttime = h0->head.samprat * (double)navg;

  /* We need to correct the RA & Dec here */

  for(j=0;j<navg;j++)
  {
    o = &H9.yplot1[0];	/* Off Scan */
    g = &H8.yplot1[0];	/* Gains */
    w = &h0->yplot1[0];	/* Destination */

  /* set the ON pointer to the start of the requested data */
    s = &otfData[(snum+j)*noint];	/* Raw OTF data */

  /* ( Sig - Ref ) 
       ---------    * Gains
          Ref
   */

    for(i=0;i<noint;i++, w++, s++, o++, g++)
    {
      *w += ( *s - *o) / *o * *g;
    }
  }

  /* Scale it back by navg */
  w = &h0->yplot1[0];	/* Destination */
  for(i=0;i<noint;i++, w++)
  {
    *w /= (float)navg;
  }

  showScan();

  /* Change my mind; must stay in H0 otherwise 
     any further analysis on the data isn't possible
     if the user dosn't do a h0 */
//  setImage(old); /* where ever that may have been */

  return(0);
}


int doOTFFix()
{
  char tbuf[256], *interp;

  
  if(isSet(PF_I_FLAG))
  {
    interp = "interp";
  }
  else
  {
    interp = " ";
  }

  sprintf(tbuf, "otffixbadcells %s", interp);

  to_dataserv(tbuf);

  return(0);
}


int doNyquist()
{
  double dish;

  if(env->freq != 0.0)
  {
    if(env->issmt)
    {
      dish = 10.0;
    }
    else
    {
      dish = 12.0;
    }

    if(env->dish_size)
    {
      dish = env->dish_size;
      st_report("fwhm(): Using Non Standard Dish Size: %.2f M", dish);
    }
    else
    {
      st_report("fwhm(): Using Dish Size: %.2f M", dish);
    }

    env->nyquist = (2.997925e8 / (2.0 * dish * (env->freq * 1e3))) * 0.2062648;

    st_report("Nyquist: %f", env->nyquist);
  }
  else
  {
    st_fail("Env: Frequency not set");
  }

  return(0);
}


/* Entry point for OTF commands */
int doOTF()
{
  int old_preview = 0, record = 0, interp = 0, norepaint = 0;
  char sysBuf[256], tbuf[256];

  int    indx;
  struct PARSE_FLAGS *p;

  p = findFirstFlag();

  if(p)
  {
    indx = p->index;	/* Save value */

    shiftFlags();	/* Remove this first flag and decriment its 'set' count */

    if(indx == PF_S_FLAG)
    {
      if(doingOTF)
      {
        st_fail("OTF Session Already Running");
        return(1);
      }
      else
      {
        st_report("Starting Dataserv OTF Process");

        sprintf(sysBuf, "dataserv -sock &");
        system(sysBuf);

        doingOTF = 1;

        return(0);
      }
    }

    /* Must be doing OTF to parse any more flags */
    if(!doingOTF)
    {
      st_fail("OTF Gridder Not Running; type 'otf /s' to start");

      printH_FlagHelp("otf");

      return(0);
    }

    switch(indx)
    {
      case PF_A_FLAG:					/* Auto Configure internals */
	configFromHeader();
	autoSetEBscan();
	return(0);
	break;

      case PF_B_FLAG: 					/* Send (B)aseline info to dataserv */
	sendBaselines();
	return(0);
	break;

      case PF_C_FLAG:					/* Send (C)ommand */
	doOTFcmd();
	return(0);
	break;

      case PF_D_FLAG:					/* (D)isplay a spectral plot from OTF scan */
	doOTFdisplay();
	return(0);
	break;

      case PF_E_FLAG:					/* (E)nd flag */
	if(isSet(PF_Y_FLAG))				/* Force ?? */
	{
	  to_dataserv("quit");

	  doingOTF = 0;
	  dataserv = 0;
	}
	else
	{
	  if(getYesNoQuit("Exit OTF Gridder: y/N ?"))
	  {
	    to_dataserv("quit");

	    doingOTF = 0;
	    dataserv = 0;
	  }
	}
	return(0);
	break;

      case PF_F_FLAG:					/* Load datafile */
	if(isSet(PF_F_FLAG))				/* we want 'fast' load choice */
	{
	  norepaint = 1;
	}

	if(isSet(PF_A_FLAG))
	{
	  autoSetEBscan();
	}

	if(env->bscan == 0 || env->escan == 50000)
	{
	  st_fail("Scan range %.2f %.2f kind of silly", env->bscan, env->escan);

	  if(isSet(PF_P_FLAG))
	  {
	    sprintf(tbuf, "otfloadchoice %s", loadChoiceStrings[old_preview]);
	    to_dataserv(tbuf);
	  }
	  else
	  if(isSet(PF_F_FLAG) == 2)
	  {
	    sprintf(tbuf, "otfloadchoice %s", loadChoiceStrings[old_preview]);
	    to_dataserv(tbuf);
	  }

	  return(0);
	}

	otfLoad(norepaint);

	if(isSet(PF_L_FLAG))
	{
          to_dataserv("otflockgrid 1");
	}

	if(isSet(PF_A_FLAG)) /* Put these back */
	{
	  env->bscan = 0;
	  env->escan = 50000;
	}
	return(0);
	break;

      case PF_G_FLAG:					/* (G)rab the dataserv spec plot and display */
	if(isSet(PF_G_FLAG))				/* (G)rab the dataserv spec plot SUM and display */
	{
	  otfGrabSpectralSum();
	}
	else
	{
	  otfGrabSpectralPlot();
	}
	return(0);
	break;

     case PF_I_FLAG:					/* Init */
	if(isSet(PF_Y_FLAG))				/* Force */
	{
	  otfInit(1);
	}
	else
	{
	  if(getYesNoQuit("Initialize OTF Gridder: y/N ?"))
	  {
	    otfInit(1);
	  }
	}
	return(0);
	break;

      case PF_L_FLAG:					/* List OTF internals */
	otfParamList();
	return(0);
	break;

      case PF_M_FLAG:					/* (M)osaic */
	otfChanMap();
	return(0);
    	break;

      case PF_O_FLAG:					/* (O)rthganol <sp> */
	doOTF3Dplot();
	return(0);
	break;

      case PF_P_FLAG:					/* Push the parameters to dataserv */
	return(sendParams());
	break;

      case PF_R_FLAG:					/* (R)egular */
	to_dataserv("otfregular");
	return(0);
	break;

      case PF_T_FLAG:
	if(isSet(PF_T_FLAG) == 3)
	{
	  to_dataserv("otfregular");
	  to_dataserv("otfinterp");
	}
	else
	if(isSet(PF_T_FLAG) == 2)
	{
	  to_dataserv("otftime");
	}
	else
	if(isSet(PF_T_FLAG) == 1)
	{
	  to_dataserv("otfrms");
	}
	else
	{
	  to_dataserv("otftsys");
	}
	return(0);
	break;

      case PF_W_FLAG:					/* Whoa! Stop a movie */
	to_dataserv("done");
	return(0);
	break;

      case PF_V_FLAG:					/* (V)elocity Movie */
	record = isSet(PF_R_FLAG);
	interp = isSet(PF_I_FLAG);
	otfMovie(record, interp);
	return(0);
	break;

      case PF_3_FLAG:					/* Write 2D data cube to file */
	doOTF3Dcube();
	return(0);
	break;
    }
  }

  /* Nothing, just print out help */
  printH_FlagHelp("otf");

  return(0);
}
