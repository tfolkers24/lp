/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

struct SHELL_GUESS shell_guess;
float splot[MAX_CHANS];

/* Fit a Circumstellar Shell; use the shell structure to hold results */


char shell_tok[MAX_TOKENS][MAX_TOKENS];

int shell_get_tok(edit)
char *edit;
{
  int c = 0;
  char *token;

  bzero((char *)&shell_tok, sizeof(shell_tok));

  token = strtok(edit, " ,\t\r\n");
  while(token != (char *)NULL)
  {
    sprintf(shell_tok[c++], "%s", token);
    token = strtok((char *)NULL, " ,\t\r\n");
  }

  return(c);
}


int sendShellGuess()
{
  st_report("Sending shell_guess struct");

  strcpy(shell_guess.magic, "SHELL_GUESS");

  sock_write(xgraphic, (char *)&shell_guess, SHELL_GUESS_SIZE);

  return(0);
}


int shellCount()
{
  int i;
  struct SHELL_FIT *p;

  env->sinfo.nshells = 0;

  p = &env->sinfo.fits[0];

  for(i=0;i<MAX_SHELL_FITS;i++,p++)
  {
    if(p->used && p->active)
    {
      env->sinfo.nshells++;
    }
  }

  return(0);
}


int shellClear()
{
  int n;

  n = getNset();

  if(n >= 0)
  {
    st_report("Clearing out shell: %d", n);

    bzero((char *)&env->sinfo.fits[n], sizeof(struct SHELL_FIT));
  }
  else
  if(isSet(PF_G_FLAG))
  {
    st_report("Clearing out shell Guess");

    bzero((char *)&shell_guess, sizeof(shell_guess));

    sendShellGuess();
  }
  else
  {
    st_report("Clearing out all shells");

    bzero((char *)&env->sinfo, sizeof(env->sinfo));
    bzero((char *)&shell_guess,    sizeof(shell_guess));

    sendShellGuess();
  }

  shellCount();

  xx();

  return(0);
}


int turnShellOnOff(offon)
int offon;
{
  int    i, n;
  char   *offonstr[] = { "Off", "On" };
  struct SHELL_FIT *p;

  n = getNset();

  if(n >= 0)
  {
    st_report("Turning shell: %d %s", n, offonstr[offon]);

    env->sinfo.fits[n].active = offon;

  }
  else
  {
    st_report("Turning all shells %s", offonstr[offon]);

    p = &env->sinfo.fits[0];
    for(i=0;i<MAX_SHELL_FITS;i++,p++)
    {
      p->active = offon;
    }

  }

  shellCount();

  xx();

  return(0);
}



int shellList()
{
  int i;
  struct SHELL_FIT *p;

  shellCount();

  p = &env->sinfo.fits[0];

  st_report(" ");
  st_report("Shells Used: ");
  st_report(" # Used Act  Area.    Sig-       Img-        Vexp    Horn /    Temp      Linename   Coeff[0]    Coeff[1]    Coeff[2]");
  st_report("             kmhz     Freq       Freq        km/s    Center");
  st_report(" ");

  for(i=0;i<MAX_SHELL_FITS;i++,p++)
  {
    if(p->used)
    {
      st_report("%2d  %d   %d %7.3f %11.3f %11.3f %8.3f %8.3f %8.3f %12.12s %+9.3e %+9.4e %+9.4e", 
			i,
			p->used,
			p->active,
			p->area_kmhz, 
			p->sig_freq, 
			p->img_freq, 
			p->vel_expansion, 
			p->horn_center_ratio, 
			p->temperature,
			p->linename,
			p->coeff[0], p->coeff[1], p->coeff[2]);
    }
  }

  return(0);
}



int shellWrite()
{
  int i;
  char sname[256];
  FILE *fp;
  struct SHELL_FIT *p;

  if(args->ntokens < 2)
  {
    st_fail("shellWrite(): Missing filename for write");
    return(1);
  }

  strcpy(sname, args->secondArg);

  fp = fopen(sname, "w");

  if(!fp)
  {
    st_fail("shellWrite(): Unable to open %s for writing", sname);
    return(1);
  }

  p = &env->sinfo.fits[0];

  for(i=0;i<MAX_SHELL_FITS;i++,p++)
  {
    fprintf(fp, "%2d %d %d %4d %4d %4d %4d ",
                        i,
      			p->used,
      			p->active,
      			p->peak_left_chan,
      			p->peak_right_chan,
      			p->left_edge_chan,
      			p->right_edge_chan);

    fprintf(fp, "%12.6f %12.6f %10.3f %7.3f %10.3f %7.3f %10.6f %7.3f %10.6f %7.3f %12.6f ",
      			p->area_kmhz,
      			p->area_kmhz_error,
      			p->sig_freq,
      			p->sig_freq_error,
      			p->img_freq,
      			p->img_freq_error,
      			p->vel_expansion,
      			p->vel_expansion_error,
      			p->horn_center_ratio,
      			p->horn_center_ratio_error,
      			p->temperature);

    fprintf(fp, "%+12.6e %+12.6e %+12.6e ", p->coeff[0], p->coeff[1], p->coeff[2]);

    fprintf(fp, "%16.16s\n", p->linename);
  }

  fclose(fp);

  return(0);
}


int shellRead()
{
  int    n, indx;
  char   sname[256], line[512], *string;
  FILE  *fp;
  struct SHELL_FIT *p;

  if(args->ntokens < 2)
  {
    st_fail("shellRead(): Missing filename for write");
    return(1);
  }

  strcpy(sname, args->secondArg);

  fp = fopen(sname, "r");

  if(!fp)
  {
    st_fail("shellRead(): Unable to open %s for reading", sname);
    return(1);
  }

  if((fp = fopen(sname, "r")) != NULL)
  {
    st_report("shellRead(): Clearing out all Shells");
    bzero((char *)&env->sinfo, sizeof(env->sinfo));
    st_report("shellRead(): Loading Shells from: %s", sname);

    while((string = fgets(line, sizeof(line), fp)) != NULL)
    {
      if(line[0] == '#')
      {
        continue;
      }

      n = shell_get_tok(line);

      if(n == 22 || n == 21)
      {
        indx = atoi(shell_tok[0]);

        if(indx >= 0 && indx < MAX_SHELL_FITS)
        {
  	  p = &env->sinfo.fits[indx];

	  p->used			= atoi(shell_tok[1]);
	  p->active			= atoi(shell_tok[2]);
	  p->peak_left_chan		= atoi(shell_tok[3]);
	  p->peak_right_chan		= atoi(shell_tok[4]);
	  p->left_edge_chan		= atoi(shell_tok[5]);
	  p->right_edge_chan		= atoi(shell_tok[6]);

	  p->area_kmhz			= atof(shell_tok[7]);
	  p->area_kmhz_error		= atof(shell_tok[8]);
	  p->sig_freq			= atof(shell_tok[9]);
	  p->sig_freq_error		= atof(shell_tok[10]);
	  p->img_freq			= atof(shell_tok[11]);
	  p->img_freq_error		= atof(shell_tok[12]);
	  p->vel_expansion		= atof(shell_tok[13]);
	  p->vel_expansion_error	= atof(shell_tok[14]);
	  p->horn_center_ratio		= atof(shell_tok[15]);
	  p->horn_center_ratio_error	= atof(shell_tok[16]);
	  p->temperature		= atof(shell_tok[17]);
	  p->coeff[0]			= atof(shell_tok[18]);
	  p->coeff[1]			= atof(shell_tok[19]);
	  p->coeff[2]			= atof(shell_tok[20]);

	  if(n == 22)
          {
	    strcpy(p->linename, shell_tok[21]);
          }
        }
	else
	{
          st_report("shellRead(): Indx out of range: %d", indx);
	}
      }
      else
      {
        st_report("shellRead(): Wrong number of tokens: %d", n);
      }
    }
  }

  fclose(fp);

  shellCount();

  shellList();

  xx();

  return(0);
}



struct SHELL_FIT *findNextShell()
{
  int i;
  struct SHELL_FIT *p;

  p = &env->sinfo.fits[0];

  for(i=0;i<MAX_SHELL_FITS;i++,p++)
  {
    if(!p->used)
    {
      return(p);
    }
  }

  return((struct SHELL_FIT *)NULL);
}



int doFitShell(p, start, stop)
struct SHELL_FIT *p;
int start, stop;
{
  int    j, k, minChan, len;
  int    midshell = 0, shelln, peakChan1, peakChan2;
  double xd[MAX_CHANS], yd[MAX_CHANS], peakAverage;
  double sum, minV, coeff[32], y, momentL, momentR, shellthreshold;
#ifdef USE_PEAKS
  double peak1, peak2;
#endif
  struct HEADER *h;
  float *x1, *y1;

  h     = &h0->head;
  x1    = h0->x1data;
  y1    = h0->yplot1;

  st_report("doFitShell(): Start %4d, Stop %4d", start, stop);

	/* Popuplate a double array */
  len = h->noint;

  for(j=0;j<len;j++)
  {
    xd[j] = (double)x1[j];
    yd[j] = (double)y1[j];
  }

    /* Assume lowwest point is somewhere in between */
  midshell = start + (stop-start) / 2;

  st_report("doFitShell(): Midshell = %4d", midshell);

    /* Compute moments  under the curve */
  momentL = 0.0;
  k   = 0;
  for(j=start;j<midshell;j++)
  {
    momentL += yd[j];

    k++;
  }

  momentL /= (double)k;

  momentR = 0.0;
  k   = 0;
  for(j=midshell;j<stop;j++)
  {
    momentR += yd[j];

    k++;
  }

  momentR /= (double)k;

  st_report("doFitShell(): Moments: Left: %f, Right: %f", momentL, momentR);

  if(env->shellthreshold)	/* user override */
  {
    shellthreshold = env->shellthreshold;
    st_report("doFitShell(): Using the user defined shellthreshold of: %.2f", shellthreshold);
  }
  else
  {
    shellthreshold = 0.80;
    st_report("doFitShell(): Using standard shellthreshold of: %.2f", shellthreshold);
  }

    /* starting on the left edge, count up until the value of the channel is > 'shellthreshold' of momentL */
  for(j=start;j<midshell;j++)
  {
    if(yd[j] > (momentL*shellthreshold))
    {
      peakChan1 = j;
      break;
    }
  }

    /* Starting at the right edge, count down until the temp > 'shellthreshold' of momentR */
  for(j=stop;j>midshell;j--)
  {
    if(yd[j] > (momentR*shellthreshold))
    {
      peakChan2 = j;
      break;
    }
  }

  shelln = peakChan2-peakChan1;

  st_report("doFitShell(): With a initial guess of left  peak: %f @ Chan %d: %f", yd[peakChan1], peakChan1, xd[peakChan1]);
  st_report("doFitShell(): With a initial guess of right peak: %f @ Chan %d: %f", yd[peakChan2], peakChan2, xd[peakChan2]);

  fitcurve(&xd[peakChan1], &yd[peakChan1], shelln, 2, coeff);

  p->peak_left_chan  = peakChan1;
  p->peak_right_chan = peakChan2;

  for(j=0;j<MAX_SHELL_COEFF;j++)		/* Save the coeff to structure */
  {
    p->coeff[j] = coeff[j];
  }

  st_report("doFitShell(): COEFF: %f %f %e", coeff[0], coeff[1], coeff[2]);

//    peakChan1 -= 1;
//    peakChan2 += 1;

  p->left_edge_chan  = peakChan1;
  p->right_edge_chan = peakChan2;

        /* Find the minimum channel in center */
  if(p->coeff[2] > 0.0)		/* horn facing up with min in the middle */
  {
    st_report("doFitShell(): Using minimum point for freq");

    minV = 999999.0;
    for(j=peakChan1;j<peakChan2;j++)
    {
      y = leasev(coeff, 2, x1[j]);

      if(y < minV)
      {
        minV    = y;
        minChan = j;
      }
    }
  }
  else				/* just use the midpoint in fit */
  {
    st_report("doFitShell(): Using Midshell for freq");
    minChan = midshell;
    minV = leasev(coeff, 2, x1[minChan]);
  }

  st_report("doFitShell(): Min Value: %f @ Chan %d: Freq: %f", minV, minChan, h->restfreq+xd[minChan]);

  p->temperature = minV;

  p->sig_freq = h->restfreq+xd[minChan];

  findLineName((p->sig_freq) / 1000.0, fabs(h->freqres) / 1000.0, p->linename);

  if(h->sideband == 1)
  {
    p->img_freq    = h->restfreq+xd[minChan] + (h->firstif * 2.0) - (2.0 * xd[minChan]);
  }
  else
  {
    p->img_freq    = h->restfreq+xd[minChan] - (h->firstif * 2.0) + (2.0 * xd[minChan]);
  }

  peakAverage = (leasev(coeff, 2, xd[peakChan1]) + leasev(coeff, 2, xd[peakChan2]) ) / 2.0;

  if(p->temperature != 0.0)
  {
    p->horn_center_ratio = peakAverage / p->temperature - 1.0;
  }
  else
  {
    p->horn_center_ratio = 1.0;
  }

  st_report("doFitShell(): Peak Average: %f; Horn/Center: %f", peakAverage, p->horn_center_ratio);

    /* Compute sum under the curve */
  sum = 0.0;
  for(j=peakChan1-10;j<peakChan2+10;j++)
  {
    if(j >= (peakChan1) && j <= (peakChan2))
    {
      y = leasev(coeff, 2, xd[j]);
    }
    else
    {
      y = 0.0;
    }

    sum += y;
  }

  st_report("doFitShell(): Sum = %.6f, Area K.MHz = %.6f", sum, fabs(sum * h->freqres));

  p->area_kmhz = fabs(sum * h0->head.freqres);

  st_report("doFitShell(): Vexp start: %f, Vexp stop: %f", x1[p->left_edge_chan ], x1[p->right_edge_chan]);

  p->vel_expansion = fabs(x1[p->left_edge_chan ] - x1[p->right_edge_chan]) / fabs(h->deltax);

  p->used   = 1;
  p->active = 1;

  return(0);
}




/* Strategy:

   1) Verify baselines removed and x-axis is freq
   2) Find the next empty shell slot
   3) populate a double arrays of x & y values
   4) request the left and right edges of the shell area
   5) Compute the moments of the area
   6) Determine the beginning and ending channel based on 80% of moment/nchan value
   7) fit a 2nd order quadratic curve to the area of interest between bchan and echan

 */
int do_line_shell()
{
  int    i, n, ret;
  int    shellstart = 0, shellstop = 0;
#ifdef USE_PEAKS
  double peak1, peak2;
#endif
  struct SHELL_FIT *p;
  float *y1;

  if(h0->head.scan < 1.0)
  {
    st_warn("do_line_shell(): No Scan(s) loaded; Use 'get scan#'");
    return(1);
  }

  n = getNset();

  if(n <= 0)
  {
    n = 1;
  }

  h0->pinfo.splot = 0;

  y1    = h0->yplot1;

  if(env->x1mode != XMODE_FREQ)
  {
    st_fail("do_line_shell(): X-Axis must be in Freq mode for Shell Commands");
    return(0);
  }

  if(!env->baselineremoved)
  {
    st_fail("do_line_shell(): Baselines must be removed first for Shell Commands");
    return(0);
  }

  for(i=0;i<n;i++)
  {
    p = findNextShell();

   if(!p)
   {
     st_fail("do_line_shell(): No free shells structures found");
     return(1);
   }

   ret = _doCCur("Mouse-Click plot on LEFT edge of Shell Region");

   if(ret == 1)
   {
     st_fail("do_line_shell(): Shell fit Cancelled");
     return(1);
   }

   shellstart = (int)round(env->ccur);

   st_report("do_line_shell(): shell left channel: %d, temp = %f", shellstart, y1[shellstart]);

    if(shellstart == 0)
    {
      st_fail("do_line_shell(): Shell fit Cancelled");
      return(1);
    }

    ret = _doCCur("Mouse-Click plot on RIGHT edge of Shell Region");

    shellstop = (int)round(env->ccur);

    st_report("do_line_shell(): shell right channel: %d, temp = %f", shellstop, y1[shellstop]);

    if(ret == 1)
    {
      st_fail("do_line_shell(): Shell fit Cancelled");
      return(1);
    }

    doFitShell(p, shellstart, shellstop);

    shellCount();
  }

  shellList();

  xx();

  return(0);
}



int findCloestPoint(s)
struct SHELL_GUESS *s;
{
  int i;
  int indx   =  -1;
  double diff, min = 1e9;

  for(i=0;i<s->npoints;i++)
  {
    diff = fabs(env->fcur - s->xd[i]);

    st_report("findCloestPoint(): Comparing: %f %f %f", env->fcur, s->xd[i], min);

    if(diff < min)
    {
      indx = i;
      min  = diff;
    }
  }

  return(indx);
}



/* A hightly interactive shell define;  Let the user basically draw the shell */
int interactiveShell()
{
  int    i, j, ret, minChan, len, indx;
  int    midshell = 0, shellstart = 0, shellstop = 0;
  int    shellpoints[100], npoints;
  char   hint[256];
  double xd[MAX_CHANS], peakAverage;
  double xpoints[100], ypoints[100];
  double sum, minV, coeff[32], y, f;
  struct SHELL_FIT *p/*, shell*/;
  struct SHELL_GUESS *s;
  struct HEADER *h;
  float *x1;

  if(h0->head.scan < 1.0)
  {
    st_warn("interactiveShell(): No Scan(s) loaded; Use 'get scan#'");
    return(1);
  }

  h0->pinfo.splot = 0;

  bzero((char *)&splot[0],    sizeof(splot));
  bzero((char *)&shell_guess, sizeof(shell_guess));

  s = &shell_guess;

  s->linetype  = 2;
  s->linecolor = 2;
  s->pointsize = 5;
  s->pointtype = 4;

  h     = &h0->head;
  x1    = h0->x1data;

  if(env->x1mode != XMODE_FREQ)
  {
    st_fail("interactiveShell(): X-Axis must be in Freq mode for Shell Commands");
    return(0);
  }

  if(!env->baselineremoved)
  {
    st_fail("interactiveShell(): Baselines must be removed first for Shell Commands");
    return(0);
  }

  p = findNextShell();

	/* Popuplate a double array */
  len = h->noint;

  for(j=0;j<len;j++)
  {
    xd[j] = (double)x1[j];
  }

  sprintf(hint, "interactiveShell(): Enter Number of shell points to sample [ 5 to 50]: ");
  do
  {
    npoints = getNumericResponse(hint);
  }
  while(npoints < 5 && npoints > 50);

  ret = _doCCur("Mouse-Click plot on LEFT-LOWER edge of Shell");

  if(ret == 1)
  {
    st_fail("interactiveShell(): Shell fit Cancelled");
    return(1);
  }

  shellstart = (int)round(env->ccur);

  st_report("interactiveShell(): shell left channel: %d", shellstart);

  if(shellstart == 0)
  {
    st_fail("interactiveShell(): Shell fit Cancelled");
    return(1);
  }

  s->npoints = 0;

  s->xd[s->npoints] = xd[shellstart];
  s->yd[s->npoints] = 0.0;
  s->npoints++;

  bzero((char *)&xpoints[0], sizeof(xpoints));
  bzero((char *)&ypoints[0], sizeof(ypoints));
  bzero((char *)&shellpoints[0], sizeof(shellpoints));

  /* now collect the sameple points */
  st_report("interactiveShell(): Mouse-Click plot on Shell curve %d times, evently from Left to Right", npoints);
  for(i=0;i<npoints;i++)
  {
    ret = _doCCur("Mouse-Click plot on Shell curve");

    if(ret == 1)
    {
      st_fail("interactiveShell(): Shell fit Cancelled");
      return(1);
    }

    xpoints[i] = env->fcur;
    ypoints[i] = env->tcur;

    s->xd[s->npoints] = env->fcur;
    s->yd[s->npoints] = env->tcur;
    s->npoints++;

    shellpoints[i] = (int)round(env->ccur);
  }

  ret = _doCCur("Mouse-Click plot on RIGHT-LOWER edge of Shell");

  if(ret == 1)
  {
    st_fail("interactiveShell(): Shell fit Cancelled");
    return(1);
  }

  shellstop = (int)round(env->ccur);

  st_report("interactiveShell(): shell right channel: %d", shellstop);

  s->xd[s->npoints] = xd[shellstop];
  s->yd[s->npoints] = 0.0;
  s->npoints++;

  /* Assume lowwest point is somewhere in between */
  midshell = shellstart + (shellstop-shellstart) / 2;

  p->peak_left_chan  = shellpoints[0];
  p->peak_right_chan = shellpoints[npoints-1];

  /* Fit user defined curve */
  fitcurve(&s->xd[1], &s->yd[1], s->npoints-2, 2, coeff);

  for(j=1;j<s->npoints-1;j++)			/* Save the fit to structure */
  {
    f = leasev(coeff, 2, s->xd[j]);
    s->fit[j] = f;
  }

  while(1)
  {
    sendShellGuess();
    xx();

    ret = _doCCur("Select any square to move; 'q' to accept");

    if(ret == 1)
    {
      st_fail("interactiveShell(): Shell refine Cancelled");
      break;
    }

    indx = findCloestPoint(s);

    st_report("interactiveShell(): Moving Index: %d", indx);

    if(indx == -1)
    {
      break;
    }

    ret = _doCCur("Select New location for square; 'q' to accept");

    if(ret == 1)
    {
      st_fail("interactiveShell(): Shell refine Cancelled");
      break;
    }

    s->xd[indx] = env->fcur;
    s->yd[indx] = env->tcur;

    /* Fit user defined curve */
    fitcurve(&s->xd[1], &s->yd[1], s->npoints-2, 2, coeff);

    for(j=1;j<s->npoints-1;j++)			/* Save the fit to structure */
    {
      f = leasev(coeff, 2, s->xd[j]);
      s->fit[j] = f;
    }
  }

  bzero((char *)&shell_guess, sizeof(shell_guess));	/* We are done with it */
  sendShellGuess();

  for(j=0;j<MAX_SHELL_COEFF;j++)		/* Save the coeff to structure */
  {
    p->coeff[j] = coeff[j];
  }

  st_report("interactiveShell(): COEFF: %f %f %e", coeff[0], coeff[1], coeff[2]);

  p->left_edge_chan  = shellpoints[0];
  p->right_edge_chan = shellpoints[npoints-1];


        /* Find the minimum channel in center */
  if(p->coeff[2] > 0.0)		/* horn facing up with min in the middle */
  {
    st_report("interactiveShell(): Using minimum point for freq");

    minV = 999999.0;
    for(j=p->peak_left_chan;j<p->peak_right_chan;j++)
    {
      y = leasev(coeff, 2, x1[j]);

      if(y < minV)
      {
        minV    = y;
        minChan = j;
      }
    }
  }
  else				/* just use the midpoint in fit */
  {
    st_report("interactiveShell(): Using Midshell for freq");
    minChan = midshell;
    minV = leasev(coeff, 2, x1[minChan]);
  }

  st_report("interactiveShell(): Min Value: %f @ Chan %d: Freq: %f", minV, minChan, h->restfreq+xd[minChan]);

  p->temperature = minV;

  p->sig_freq = h->restfreq+xd[minChan];

  findLineName((p->sig_freq) / 1000.0, fabs(h->freqres) / 1000.0, p->linename);

  if(h->sideband == 1)
  {
    p->img_freq    = h->restfreq+xd[minChan] + (h->firstif * 2.0) - (2.0 * xd[minChan]);
  }
  else
  {
    p->img_freq    = h->restfreq+xd[minChan] - (h->firstif * 2.0) + (2.0 * xd[minChan]);
  }

  peakAverage = (leasev(coeff, 2, xd[p->peak_left_chan]) + leasev(coeff, 2, xd[p->peak_right_chan]) ) / 2.0;

  if(p->temperature != 0.0)
  {
    p->horn_center_ratio = peakAverage / p->temperature - 1.0;
  }
  else
  {
    p->horn_center_ratio = 1.0;
  }

  st_report("interactiveShell(): Peak Average: %f; Horn/Center: %f", peakAverage, p->horn_center_ratio);

    /* Compute sum under the curve */
  sum = 0.0;
  for(j=shellstart;j<shellstop;j++)
  {
    sum += leasev(coeff, 2, xd[j]);
  }

  st_report("interactiveShell(): Sum = %.6f, Area K.MHz = %.6f", sum, fabs(sum * h->freqres));

  p->area_kmhz = fabs(sum * h0->head.freqres);

  st_report("interactiveShell(): Vexp start: %f, Vexp stop: %f", x1[p->left_edge_chan ], x1[p->right_edge_chan]);

  p->vel_expansion = fabs(x1[p->left_edge_chan ] - x1[p->right_edge_chan]) / fabs(h->deltax);

  p->used   = 1;
  p->active = 1;

  shellCount();

  shellList();

  xx();

  return(0);
}


int nameShell()
{
  int i, n;
  char buf[256], bbuf[512];

  n = getNset();

  if(n < 0 || n >= MAX_SHELL_FITS)
  {
    st_print("nameShell(): shell /n: numeric flag out of range: /0 to /%d\n", MAX_SHELL_FITS-1);

    printH_FlagHelp(args->firstArg);

    return(1);
  }

  if(args->ntokens > 2)
  {
    bzero(bbuf, sizeof(bbuf));

    for(i=1;i<args->ntokens;i++)
    {
      sprintf(buf, args->tok[i]);

      strcat(bbuf, buf);
    }

    st_report("nameShell(): Setting Shell fit[%d] name to %s", n, bbuf);

    strcpy(env->sinfo.fits[n].linename, bbuf);

    return(0);
  }
  else
  {
    st_print("nameShell(): Missing arg for shell /n\n");

    printH_FlagHelp(args->firstArg);

    return(1);
  }

  return(0);
}


int findChanNumber(x)
double x;
{
  int i;
  int foundi = -1;
  double diff, min_diff = 999999999.9;
  
  st_report("findChanNumber(): Looking for a X-Axis value of: %f", x);

  for(i=0;i<h0->head.noint;i++)
  {
    diff = fabs((double)h0->x1data[i] - x);

    if(diff < min_diff)
    {
      min_diff = diff;
      foundi   = i;
    }
  }

  if(foundi >= 0)
  {
    st_report("findChanNumber(): Found match @ Channel: %4d, %.2f", foundi, h0->x1data[foundi]);
  }

  return(foundi);
}


int createShellsFromGaussians()
{
  int    i, deltax;
  int    start, stop, centerChan;
  struct SHELL_FIT *p/*, shell*/;

  st_report("createShellsFromGaussians(): Creating a Shell fit for each Gaussian Fit");

  if(h0->ginfo.nfits)
  {
    st_report("createShellsFromGaussians(): Erasing defined Shells");

    bzero((char *)&env->sinfo, sizeof(env->sinfo));

    for(i=0;i<h0->ginfo.nfits;i++)
    {
      if(i >= MAX_SHELL_FITS)
      {
        break;
      }


      deltax = (int)round((h0->ginfo.fits[i].hp / 2.0) / fabs(h0->head.freqres)) + 2;

      st_report("createShellsFromGaussians(): FIT %2d: Center = %f, HP = %f", i, h0->ginfo.fits[i].center, h0->ginfo.fits[i].hp);

      centerChan = findChanNumber(h0->ginfo.fits[i].center);

      if(centerChan < 0)
      {
        st_report("createShellsFromGaussians(): Can't locate center channel for fit %2d", i);
	continue;
      }
      else
      {
        st_report("createShellsFromGaussians(): Located center channel %2d", centerChan);
      }

      p = findNextShell();

      start = centerChan - deltax;
      stop  = centerChan + deltax;

      st_report("createShellsFromGaussians(): shell Left  channel: %d, freq = %f", start, h0->x1data[start]);
      st_report("createShellsFromGaussians(): shell Right channel: %d, freq = %f", stop,  h0->x1data[stop]);

      doFitShell(p, start, stop);
    }

    shellCount();

    shellList();
  }
  else
  {
    st_fail("createShellsFromGaussians(): No Gaussian Fit to work with");
    return(1);
  }

  xx();

  return(0);
}


/* If 2 /m /m are passed; turn on, else off */
int markShell()
{
  if(isSet(PF_M_FLAG))
  {
    st_report("Turning Shell Labels ON");
    env->sinfo.label = 1;
  }
  else
  {
    st_report("Turning Shell Labels OFF");
    env->sinfo.label = 0;
  }

  xx();

  return(0);
}


int doShellRun()
{

  return(0);
}


int doShell()
{
  int    indx;
  struct PARSE_FLAGS *p;

  p = findFirstFlag();

  if(p)
  {
    indx = p->index;    /* Save value */

    shiftFlags();       /* Remove this first flag and decriment is 'set' count */

    switch(indx)
    {
      case PF_C_FLAG: return(shellClear()); 				break; /* Clear all Priv shells, or just one */
      case PF_D_FLAG: return(do_line_shell()); 				break; /* Manual fitting; one or more */
      case PF_F_FLAG: return(turnShellOnOff(0)); 			break; /* Toggle Visibility */
      case PF_G_FLAG: return(createShellsFromGaussians()); 		break; /* Create 1 shell for each Gaussian fit */
      case PF_I_FLAG: return(interactiveShell()); 			break; /* interactive Shell Define */
      case PF_M_FLAG: return(markShell()); 				break; /* Mark the shells with labels */
      case PF_N_FLAG: return(nameShell()); 				break; /* Name a shell line */
      case PF_O_FLAG: return(turnShellOnOff(1)); 			break; /* Toggle Visibility */
      case PF_L_FLAG: return(shellList()); 				break; /* List all fitted shells */
      case PF_R_FLAG: return(shellRead()); 				break; /* Read shells from file */
      case PF_W_FLAG: return(shellWrite()); 				break; /* Save shells to file */
             default: printH_FlagHelp(args->firstArg); return(1); 	break;
    }
  }

  do_line_shell();

  return(0);
}
