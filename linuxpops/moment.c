/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"


struct SPECTRAL_MOMENT_STRUCT sms;

static int last_scan = 0;


int sumAndPrint(bchan, echan)
int bchan, echan;
{
  int i, j;

  j   = 0;
  env->moment_sum = 0.0;

  for(i=bchan;i<echan;i++)
  {
    env->moment_sum += h0->yplot1[i];

    j++;
  }

  env->moment_avg  = env->moment_sum / (double)j;
  env->moment_area_kmhz = env->moment_sum * h0->head.freqres;
  env->moment_kkms = env->moment_sum * fabs(h0->head.deltax);

  if(env->x1mode == XMODE_FREQ)
  {
    env->moment_freq  = h0->head.restfreq+h0->x1data[(bchan+echan) / 2];
  }
  else
  {
    env->moment_freq  = h0->head.restfreq+h0->x2data[(bchan+echan) / 2];
  }

  findLineName((env->moment_freq) / 1000.0, fabs(h0->head.freqres) / 1000.0, env->moment_linename);

  st_report("0th moment: Scan: %7.2f, Freq: %.3f, Sum = %.3fK, Avg = %.3fK, Area.Mhz = %.3f, Kkm/s = %.3f: Line: %s", 
			h0->head.scan,
			env->moment_freq, 
			env->moment_sum, 
			env->moment_avg, 
			env->moment_area_kmhz, 
			env->moment_kkms, 
			env->moment_linename);

  return(0);
}


int clearMomentStruct()
{
  st_report("Clearing out all Moment Measurements");

  bzero((char *)&sms, sizeof(sms));

  last_scan = 0;

  return(0);
}



int saveMomentStruct()
{
  int    feed, scan;
  struct SPECTRAL_MOMENT_ELEMENT *p;

  st_report("Saving Moment Measurement");

  feed = getFeed(h0->head.scan);		/* returns 1 - 4 */
  scan = (int)floor(h0->head.scan);

  if(last_scan == 0)
  {
    last_scan = scan;

    /* copy in the first header read */
    bcopy((char *)&h0->head, (char *)&sms.head, sizeof(struct HEADER));

    sms.issmt = env->issmt;
  }

  if(feed < 1 || feed > MAX_MOMENT_IFS)
  {
    st_fail("Feed: %d, Out of range: %d to %d", feed, 1, MAX_MOMENT_IFS);
    return(1);
  }

  feed--;					/* Reduce to 0 - 3 */

  /* determine is it's time to increament the indx */
  if(scan != last_scan)
  {
    sms.n++;

    if(sms.n >= MAX_MOMENT_MEASUREMENTS)
    {
      st_fail("Moment Structure Full @ %d Measurements", MAX_MOMENT_MEASUREMENTS);
      return(1);
    }

    last_scan = scan;
  }

	/* Save it */
  p            = &sms.measurements[sms.n][feed];
  p->scan      = h0->head.scan;
  p->tsys      = h0->head.stsys;
  p->el        = h0->head.el;
  p->freq      = env->moment_freq;
  p->sum       = env->moment_sum;
  p->avg       = env->moment_avg;
  p->area_kmhz = env->moment_area_kmhz;
  p->kkms      = env->moment_kkms;

  return(0);
}


int listMomentStruct()
{
  int i, j;

  st_report("Last Moment Measurement");

  st_report("Scan     = %16.2f",     h0->head.scan);
  st_report("Freq     = %16.3f MHz", env->moment_freq);
  st_report("Sum      = %16.3f K",   env->moment_sum);
  st_report("Avg      = %16.3f K",   env->moment_avg);
  st_report("Area.Mhz = %16.3f",     env->moment_area_kmhz);
  st_report("Kkm/s    = %16.3f",     env->moment_kkms);
  st_report("Line     = %16.16s",    env->moment_linename);
  st_report(" ");

  if(sms.n)
  {
    st_print("\n");
    st_print("  Scan     Kkms      Scan     Kkms     Scan     Kkms     Scan     Kkms \n");
    st_print("\n");

    for(i=0;i<sms.n;i++)
    {
      for(j=0;j<MAX_MOMENT_IFS;j++)
      {
        st_print("%8.2f %8.3f ", sms.measurements[i][j].scan, sms.measurements[i][j].kkms);
      }

      st_print("\n");
    }
  }

  return(0);
}


int manualMoment()
{
  int ret, bchan, echan;

  ret = _doCCur("Mouse-Click Left edge of Area of Interest");

  if(ret == 1)
  {
    st_warn("Moment Cancelled");
    return(1);
  }

  bchan = env->ccur;

  env->moment_bchan = bchan;

  ret = _doCCur("Mouse-Click Right edge of Area of Interest");

  if(ret == 1)
  {
    st_warn("Moment Cancelled");
    return(1);
  }

  echan = env->ccur;

  env->moment_echan = echan;

  if(echan < bchan)
  {
    st_fail("Points should be selected Left to Right");
    return(0);
  }

  if(echan == bchan)
  {
    st_fail("Points should be different Left to Right");
    return(0);
  }

  sumAndPrint(bchan, echan);

  return(0);
}


int autoMoment()
{
  int bchan, echan;

  bchan = atoi(args->secondArg);
  echan = atoi(args->thirdArg);

  if(echan < bchan)
  {
    st_fail("Points should be selected Left to Right");
    return(0);
  }

  if(echan == bchan)
  {
    st_fail("Points should be different Left to Right");
    return(0);
  }

  sumAndPrint(bchan, echan);

  return(0);
}


/* Run through each scan and fit a Moment and save results */
int ingestScans()
{
  int    bchan, echan, tmp;
  int    i, j=0, indx, cols, rows, first = 1;
  char   *qp, tbuf[256], cmdBuf[256];
  double lastScan;
  struct HEADER *p;

  bchan = env->moment_bchan;
  echan = env->moment_echan;

  if(bchan == 0 || echan == 0 || (bchan == echan))
  {
    st_fail("ingestScans(): bchan and echan must be no-zero and different");
    return(1);
  }

  if(echan < bchan)
  {
    tmp = bchan;

    bchan = echan;
    echan = tmp;
  }

  getTerminalSize(&cols, &rows);

  rows -= 2;

  resetRejections();

  freeX();
  freeY();

  if(env->use_scaling)
  {
    st_report("Using Data-Scaling Factors of: %f %f %f %f", env->data_scale_1, env->data_scale_2, env->data_scale_3, env->data_scale_4);
  }

  if(!env->numofscans)
  {
    st_warn("No Valid Data Files Loaded");
    return(1);
  }

  if(notSet(PF_A_FLAG))
  {
    clearMomentStruct();			/* Star with an empty structure */
  }

  sprintf(cmdBuf, "stack /f");		/* fill the stack which will count the number of scans */
  parseCmd(cmdBuf);

  st_report("ingestScans(): Ingesting %d scans", env->acount);

  if(isSet(PF_V_FLAG) > 2)
  {
    qp = " ";
  }
  else
  {
    qp = "/q";
  }

  p    = &sa[0];
  indx = 0;

  clearInterrupt();

  for(i=0;i<env->numofscans;i++,p++)
  {
    if(verboseLevel & VERBOSE_CRITIRIA)
    {
      printf("Passing Scan %.2f to meetsCritiria(): ", p->scan);
    }

    /* Check the most obvious critiria */
    if(!meetsCritiria(p, CHECK_ALL))
    {
      continue;
    }

    /* Check for proper LINE* Mode */
    if(strncmp(p->obsmode, "LINEBSP",  7) && 
       strncmp(p->obsmode, "LINEPS",   6) &&
       strncmp(p->obsmode, "LINEFS",   6) &&
       strncmp(p->obsmode, "LINETPON", 8) &&
       strncmp(p->obsmode, "LINEAPS",  7)) /* We only process BSP, APS & PS so far; treat FS the same */
    {
      incrementRejMode();
      continue; /* Not Spectral */
    }

    if(p->openpar[5] == 5555.5) /* a BSP focal */
    {
      continue; /* Ignore */
    }

    /* Is this scan ignored??? */
    if(isScanIgnored(p->scan))
    {
      incrementRejIgnored();
      continue;
    }

    if(isSpecFivePoint(p))
    {
      if(!env->allow_five_pt)
      {
        incrementRejFive();
        continue;
      }
      else
      if(p->az_offset || p->el_offset)
      {
        incrementRejFive();
        continue;
      }
    }

    /* Made it this far; read it in */

    j++;

    env->curScan = p->scan;

    if(first)
    {
      lastScan = p->scan;
      first = 0;
    }

    sprintf(tbuf, "get %.2f %s", p->scan, qp);
    parseCmd(tbuf);

    sprintf(tbuf, "baseline %s", qp);
    parseCmd(tbuf);

    if((int)p->scan != (int)lastScan)
    {
      indx++; /* Accum went well */

      lastScan = p->scan;
    }

    if(isSet(PF_V_FLAG))
    {
      sprintf(tbuf, "show %s", qp);
      parseCmd(tbuf);

      usleep(50000);
    }
    else
    {
      printf(".");
      fflush(stdout);

      if(!(j%80))
      {
        printf("\n");
        fflush(stdout);
      }
    }

/*
    if(getFeed(h0->head.scan) == 3)
    {
      sprintf(tbuf, "scale 0.934499 /q");
      parseCmd(tbuf);
    }

    if(getFeed(h0->head.scan) == 4)
    {
      sprintf(tbuf, "scale 0.951591 /q");
      parseCmd(tbuf);
    }
 */

    sprintf(tbuf, "moment %d %d %s", bchan, echan, qp);
    parseCmd(tbuf);

    sprintf(tbuf, "moment /s %s", qp);
    parseCmd(tbuf);

    if(isInterrupt()) /* Check for Control-C input */
    {
      sprintf(tbuf, "show %s", qp);
      parseCmd(tbuf);

      return(0);
    }

    if(indx >= MAX_MOMENT_MEASUREMENTS)
    {
      if(isSet(PF_Q_FLAG))
      {
	stquiet = 0;
      }

      st_report("Results Array Full");

      if(isSet(PF_Q_FLAG))
      {
	stquiet = 1;
      }

      break;
    }
  }

  printf("\n");
  fflush(stdout);

  sprintf(tbuf, "show %s", qp);
  parseCmd(tbuf);

  if(indx == 0)
  {
    if(isSet(PF_Q_FLAG))
    {
      stquiet = 0;
    }

    st_print("No Scans Found Matching Search Criteria\n");

    env->last_scan_count = 0;

    printRejections();

    if(isSet(PF_Q_FLAG))
    {
      stquiet = 1;
    }

    return(1);
  }

  plotMomentStruct();

  return(0);
}


int plotMomentStruct()
{
  if(!sms.n)
  {
    st_fail("plotMomentStruct(): Moments Structure Empty");
    return(0);
  }

  st_report("Sending Moments Struct to XGraphic");

  strcpy(sms.magic, "MOMENTS_STRUCT");

  if(isSet(PF_E_FLAG))
  {
    sms.plotx = MOM_X_AXIS_EL;
  }
  else
  {
    sms.plotx = MOM_X_AXIS_SCAN;
  }

  if(isSet(PF_A_FLAG) == 2)
  {
    sms.ploty = MOM_Y_AXIS_AVG;
  }
  else
  if(isSet(PF_A_FLAG) == 1)
  {
    sms.ploty = MOM_Y_AXIS_AREA;
  }
  else
  if(isSet(PF_K_FLAG))
  {
    sms.ploty = MOM_Y_AXIS_KKMS;
  }
  else
  if(isSet(PF_T_FLAG))
  {
    sms.ploty = MOM_Y_AXIS_TSYS;
  }
  else
  {
    sms.ploty = MOM_Y_AXIS_SUM;
  }


  sock_write(xgraphic, (char *)&sms, sizeof(sms));

  return(0);
}


int writeMomentStruct()
{
  int i, j, k;
  char fname[256];
  FILE *fp;
  struct SPECTRAL_MOMENT_ELEMENT *p;

  if(!sms.n)
  {
    st_fail("writeMomentStruct(): Moments Structure Empty");
    return(0);
  }

  if(args->ntokens < 2)
  {
    st_fail("writeMomentStruct(): Missing filename arg");
    return(0);
  }

  strcpy(fname, args->secondArg);

  fp = fopen(fname, "w");

  if(!fp)
  {
    st_fail("writeMomentStruct(): Unable to open filename: %s", fname);
    return(0);
  }

  fprintf(fp, "#\n");
  fprintf(fp, "# Source: %s Bchan: %d Echan: %d\n", env->source, env->moment_bchan, env->moment_echan);
  fprintf(fp, "#\n");
  fprintf(fp, "# Scan   Freq         Sum        Avg      Area.mhz      Kkms   Linename\n");
  fprintf(fp, "#\n");

  k = 0;
  for(i=0;i<sms.n;i++)
  {
    for(j=0;j<MAX_MOMENT_IFS;j++)
    {
      p = &sms.measurements[i][j];

      if(p->scan)
      {
        fprintf(fp, "%7.2f %10.3f %10.3f %10.3f %10.3f %10.3f %s\n", 
			p->scan,
			p->freq, 
			p->sum, 
			p->avg, 
			p->area_kmhz, 
			p->kkms, 
			env->moment_linename);
	k++;
      }
    }
  }

  fclose(fp);

  st_report("writeMomentStruct(): Wrote %d records to %s", k, fname);

  return(0);
}


int doMoment()
{
  if(isSet(PF_I_FLAG))
  {
    return(ingestScans());
  }

  if(h0->head.scan < 1.0)
  {
    st_warn("No Scan loaded; Use 'get scan#'");
    return(0);
  }

  if(env->x1mode != XMODE_FREQ && env->x2mode != XMODE_FREQ)
  {
    st_warn("One of the X-Axis must be in Freq mode for Moment Commands");
    return(0);
  }

  if(isSet(PF_C_FLAG))
  {
    return(clearMomentStruct());
  }

  if(isSet(PF_S_FLAG))
  {
    return(saveMomentStruct());
  }

  if(isSet(PF_L_FLAG))
  {
    return(listMomentStruct());
  }

  if(isSet(PF_P_FLAG))
  {
    return(plotMomentStruct());
  }

  if(isSet(PF_W_FLAG))
  {
    return(writeMomentStruct());
  }

//  if(!env->baselineremoved)
//  {
//    st_warn("Baselines should be removed first for Moment Command");
//    return(0);
//  }

	/* If you got this far, you are defining one */
  if(args->ntokens > 2)
  {
    return(autoMoment());
  }
  else
  {
    return(manualMoment());
  }

  return(0);
}
