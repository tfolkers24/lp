/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */

#include "extern.h"


int countWindows()
{
  int i;
  struct WINDOW_ELEMENT *p;

  env->winfo.nwindows = 0;

  p = &env->winfo.windows[0];
  for(i=0;i<MAX_WINDOWS;i++,p++)
  {
    if(p->used && p->active)
    {
      env->winfo.nwindows++;
    }
  }

  return(0);
}


int listWindows()
{
  int i;
  struct WINDOW_ELEMENT *p;

  countWindows();

  st_report("       #    Used Active    Xmin       Xmax       Ymin       Ymax");
  p = &env->winfo.windows[0];
  for(i=0;i<MAX_WINDOWS;i++,p++)
  {
    st_report("Window(%2d): %2d    %2d %10.3f %10.3f %10.3f %10.3f", i, p->used, p->active, p->xmin, p->xmax, p->ymin, p->ymax);
  }

  return(0);
}


int turnWindowOffOn(p1, active)
struct WINDOW_ELEMENT *p1;
int active;
{
  int i;
  struct WINDOW_ELEMENT *p;

  if(p1)
  {
    p1->active = active;
  }
  else
  {
    p = &env->winfo.windows[0];

    for(i=0;i<MAX_WINDOWS;i++,p++)
    {
      p->active = active;
    }
  }

  return(0);
}


struct WINDOW_ELEMENT *findNextWindow()
{
  int i;
  struct WINDOW_ELEMENT *p;

  p = &env->winfo.windows[0];
  for(i=0;i<MAX_WINDOWS;i++,p++)
  {
    if(!p->used)
    {
      return(p);
    }
  }

  return((struct WINDOW_ELEMENT *)NULL);
}


int defineWindowFromXgraphics()
{
  struct WINDOW_ELEMENT *p;

  st_report("Grabing Window Extens From XGraphic");

  setFlag(PF_S_FLAG, 1);
  getPlotExtents();
  setFlag(PF_S_FLAG, 0);

  p = findNextWindow();

  if(p)
  {

    p->xmin = env->xplotmin;
    p->xmax = env->xplotmax;
    p->ymin = env->yplotmin;
    p->ymax = env->yplotmax;

    p->used   = 1;
    p->active = 1;

    countWindows();
  }
  else
  {
    st_fail("Max number of windows already defined");
    return(1);
  }

  xx();

  return(0);
}


int doMouseSetWindow(p)
struct WINDOW_ELEMENT *p;
{
  int bd, ed;
  double left_x, left_y, right_x, right_y, tmp;

  _doCCur("Click Left Mouse Button on Left Edge of Desired Region");

  bd = (int)round(env->ccur);

  left_x = last_mouse_x;
  left_y = last_mouse_y;

  _doCCur("Click Left Mouse Button on Right Edge of Desired Region");

  ed = (int)round(env->ccur);

  right_x = last_mouse_x;
  right_y = last_mouse_y;

  /* Check for X inversion */
  if(bd > ed)
  {
    tmp = left_x;
    left_x = right_x;
    right_x = tmp;
  }

  /* Check for Y inversion */
  if(left_y < right_y)
  {
    tmp = left_y;
    left_y = right_y;
    right_y = tmp;
  }

  p->xmin = left_x;
  p->xmax = right_x;
  p->ymin = right_y;
  p->ymax = left_y;

  p->used   = 1;
  p->active = 1;

  countWindows();

  return(0);
}



int goCreateWindowBasedOnGaussianFits()
{
  int    i;
  double deltax;
  struct LP_IMAGE_INFO *p;
  struct WINDOW_ELEMENT *p2;

  p = h0;

  if(env->autoGaussXfactor == 0.0)
  {
    env->autoGaussXfactor = 3.0;
  }

  if(env->autoGaussYfactor == 0.0)
  {
    env->autoGaussYfactor = 1.25;
  }

  if(p->ginfo.nfits)
  {
    st_report("Erasing defined Windows");

    bzero((char *)&env->winfo, sizeof(env->winfo));

    countWindows();
    
    for(i=0;i<p->ginfo.nfits;i++)
    {
      if(i>=MAX_WINDOWS)
      {
        break;
      }

      p2 = &env->winfo.windows[i];

      deltax =  p->ginfo.fits[i].hp * env->autoGaussXfactor;

      if(env->x1mode == XMODE_VEL)		/* Vel Offset */
      {
        if(h0->head.sideband == 2)
        {
          p2->xmin = p->ginfo.fits[i].center + deltax;
          p2->xmax = p->ginfo.fits[i].center - deltax;
        }
        else
        {
          p2->xmin = p->ginfo.fits[i].center - deltax;
          p2->xmax = p->ginfo.fits[i].center + deltax;
        }
      }
      else
      if(env->x1mode == XMODE_FREQ)		/* Freq Offset */
      {
        if(h0->head.sideband == 2)
        {
          p2->xmin = p->ginfo.fits[i].center - deltax;
          p2->xmax = p->ginfo.fits[i].center + deltax;
        }
        else
        {
          p2->xmin = p->ginfo.fits[i].center + deltax;
          p2->xmax = p->ginfo.fits[i].center - deltax;
        }
      }
      else					/* Channels */
      {
        p2->xmin = p->ginfo.fits[i].center - deltax;
        p2->xmax = p->ginfo.fits[i].center + deltax;
      }


      p2->ymin = (p->ginfo.fits[i].peak / 20.0) * -1.0;
      p2->ymax = p->ginfo.fits[i].peak * env->autoGaussYfactor;

      p2->used   = 1;
      p2->active = 1;

      countWindows();
    }
  }

  listWindows();

  return(0);
}



int readWindowsFromFile()
{
  char tbuf[256];

  if(args->ntokens < 2)
  {
    st_fail("readWindowsFromFile(): Missing filename");
    return(1);
  }

  sprintf(tbuf, "load %s", args->secondArg);
  parseCmd(tbuf);

  return(0);
}


int writeWindowsToFile()
{
  int i;
  FILE *fp;
  char fname[256];
  struct WINDOW_ELEMENT *p;

  if(args->ntokens < 2)
  {
    st_fail("writeWindowsToFile(): Missing filename");
    return(1);
  }

  strcpy(fname, args->secondArg);

  fp = fopen(fname, "w");

  if(!fp)
  {
    st_fail("writeWindowsToFile(): Unable to open filename: %s", fname);
    return(1);
  }

  fprintf(fp, "#\n");
  fprintf(fp, "#      #    Used Active    Xmin       Xmax       Ymin       Ymax\n");
  fprintf(fp, "#\n");
  fprintf(fp, "x1mode = %d\n", env->x1mode);
  fprintf(fp, "x2mode = %d\n", env->x2mode);
  fprintf(fp, "#\n");

  p = &env->winfo.windows[0];
  for(i=0;i<MAX_WINDOWS;i++,p++)
  {
    fprintf(fp, "window /n %2d %2d  %2d %10.3f %10.3f %10.3f %10.3f\n", i, p->used, p->active, p->xmin, p->xmax, p->ymin, p->ymax);
  }

  fclose(fp);

  st_report("writeWindowsToFile(): Wrote Window defines to %s", fname);

  return(0);
}



int createNewWindow()
{
  int n;
  struct WINDOW_ELEMENT *p;

  if(args->ntokens != 8)
  {
    st_fail("createNewWindow(): Wrong number of tokens: %d, should be 8", args->ntokens);
    return(1);
  }

  n    = atoi(args->tok[1]);

  if(n < 0 || n >= MAX_WINDOWS)
  {
    st_fail("createNewWindow(): Window Number %d, Out of Range: [0-15]", n);
    return(1);
  }

  p = &env->winfo.windows[n];

  p->used   = atoi(args->tok[2]);
  p->active = atoi(args->tok[3]);

  p->xmin   = atof(args->tok[4]);
  p->xmax   = atof(args->tok[5]);
  p->ymin   = atof(args->tok[6]);
  p->ymax   = atof(args->tok[7]);

  countWindows();

  return(0);
}


int doWindow()
{
  int    indx, w, ok=0;
  struct PARSE_FLAGS *pf;
  struct WINDOW_ELEMENT *p;


  pf = findFirstFlag();

  if(pf)
  {
    indx = pf->index;					/* Save value */

    shiftFlags();					/* Remove this first flag and decriment is 'set' count */

    switch(indx)
    {
      case PF_C_FLAG:					/* Clear one or all*/
	if((w = getWindowFlag()) >= 0)
	{
	  st_report("Erasing Window %d", w);

	  p = &env->winfo.windows[w];

	  p->active = 0;
	  p->used   = 0;
	  p->xmin   = 0.0;
	  p->xmax   = 0.0;
	  p->ymin   = 0.0;
	  p->ymax   = 0.0;

          countWindows();
	}
	else
	{
	  st_report("Erasing defined Windows");		/* All */

	  bzero((char *)&env->winfo, sizeof(env->winfo));

          countWindows();
	}

	xx();

	ok = 1;
	break;

      case PF_D_FLAG:					/* Define */
	if(h0->head.scan < 1.0)
	{
	  st_warn("doWindow(): No Scan loaded; Use 'get scan#'");
	  return(0);
	}

	if((w = getWindowFlag()) >= 0)			/* Has the user ask for a select window ?? */
	{
	  p = &env->winfo.windows[w];
	}
	else
	{
	  p = findNextWindow();

	  if(!p)
	  {
	    st_fail("Max number of windows already defined");
	    return(1);
	  }
	}

	doMouseSetWindow(p);

	p->used   = 1;
	p->active = 1;

	xx();

	ok = 1;
	break;

      case PF_F_FLAG:					/* Tuen one or all window outlines off */
	if((w = getWindowFlag()) >= 0)
	{
	  p = &env->winfo.windows[w];
	}
	else
	{
	  p = (struct WINDOW_ELEMENT *)NULL;
	}

	turnWindowOffOn(p, 0);

	xx();

	ok = 1;
	break;

      case PF_G_FLAG:					/* Define windows based on Gaussian Fits */
	if(h0->head.scan < 1.0)
	{
	  st_warn("doWindow(): No Scan loaded; Use 'get scan#'");
	  return(0);
	}

	goCreateWindowBasedOnGaussianFits();

	xx();

	ok = 1;
	break;

      case PF_L_FLAG:					/* List all windows */
	listWindows();

	ok = 1;
	break;

      case PF_N_FLAG:
	if(h0->head.scan < 1.0)
	{
	  st_warn("doWindow(): No Scan loaded; Use 'get scan#'");
	  return(0);
	}

	return(createNewWindow());
	break;

      case PF_O_FLAG:					/* Turn one or all window outlines on */
	if((w = getWindowFlag()) >= 0)
	{
	  p = &env->winfo.windows[w];
	}
	else
	{
	  p = (struct WINDOW_ELEMENT *)NULL;
	}

	turnWindowOffOn(p, 1);

	xx();

	ok = 1;
	break;

      case PF_R_FLAG:					/* Read in window params from fname */
	return(readWindowsFromFile());
	break;

      case PF_W_FLAG:					/* Write out window params to fname */
	return(writeWindowsToFile());
	break;

      case PF_X_FLAG:					/* Set window based on current Gnuplot Zoom */
	if(h0->head.scan < 1.0)
	{
	  st_warn("doWindow(): No Scan loaded; Use 'get scan#'");
	  return(0);
	}

	return(defineWindowFromXgraphics());
	break;
    }
  }

  if(!ok)
  {
    printH_FlagHelp("window");
  }

  return(0);
}
