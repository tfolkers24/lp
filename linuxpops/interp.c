/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

/*
   2d array interpolation routine
   swiped from Darrel, converted to 'C' by Jeff
*/


/*
 version 1.0  mpifr cyber edition  22 MAY 1977.
 see nod 2 manual page m 2.1                                       
 C.G Haslam       mar  1969.                                        
 This routine interpolates the amplitude z at the point y which lies
 between the points b and c. y=0.0 corresponds to b and y=1.0
 corresponds to c. a,b,c,d are the amplitudes at 4 points spaced at
 unit intervals.                                                  
*/



double yintpn(a, b, c, d, y)
double a, b, c, d, y;
{
  double e, f, g;

  if(y == 0.0)
  {
    return((double)b);
  }

  e = b-a;      /* exact copy case */
  f = b-c;
  g = e + f + f + d - c;
  g = g*y- e - f - g;
  g = g*y - a + c;

  return( (double)(0.5*g*y + b));
}

/*
  interp interpolates a 2-d array array into parray.
  uses lots of memory; PC gymnastics deleted
*/
int interp(array, nx, ny, parray, npx, npy )
double *array;
int nx, ny;
double *parray;
int npx, npy;
{
  double *one, *two, *three, *four, *save, *done;
  double *xone, *xtwo, *xthree, *xfour;
  int k, i, iy;
  double y, dy, lasty;
  double oversampy, oversampx, oversamp;

  one   = (double *)malloc( (unsigned)(sizeof(double) * npx));
  two   = (double *)malloc( (unsigned)(sizeof(double) * npx));
  three = (double *)malloc( (unsigned)(sizeof(double) * npx));
  four  = (double *)malloc( (unsigned)(sizeof(double) * npx));

  if(!one || !two || !three || !four)
  {
    st_fail("no memory for interpolation");
    return(1);
  }

  xone   = &array[0];
  xtwo   = &array[nx];
  xthree = &array[nx*2];

  oversampy = (npy-1.0)/(ny-1.0);
  oversampx = (npx-1.0)/(nx-1.0);
  oversamp = oversampx < oversampy ? oversampx:oversampy;

/*
  npx = 1 + (int)rint(oversamp * ((double)nx - 1.0));
  npy = 1 + (int)rint(oversamp * ((double)ny - 1.0));
 */

							/*  check that we need to interpolate */
  if(fabs(oversamp - 1.0) < 0.001 )
  {
     st_report("interpolation not needed - oversamp is %f", oversamp );
     return(0);
  }

  xinterp( xone,    nx, two,   npx );
  xinterp( xtwo,    nx, three, npx );
  xinterp( xthree,  nx, four,  npx );

  lasty = 0;
							/* create first row by extrapolation */
  for( i=0; i< npx; i++ )
  {
    one[i] = two[i] - three[i] + two[i];
  }

  for(k=0;k<npy;k++)					/* for each new row, interpolate in y */
  {
    y  = (double)k/oversampy;
    iy = (int)y;

    if(iy != lasty) 					/* check to see if it is time to move the pointers */
    { 
      save  = one;
      one   = two;
      two   = three;
      three = four;
      four  = save;

      if(iy+2 < ny)					/* interpolate along x */
      {
	xfour = &array[(iy+2)*nx];
        xinterp( xfour, nx, four, npx );
      }
      else						/* otherwize extrapolate */
      {
        for(i=0;i<npx;i++)
        {
          four[i] = three[i] - two[i] + three[i];
	}
      }

      lasty = iy;
    }

    done = &parray[npx*k];
    dy   = y - (double)iy;

    for(i=0;i<npx;i++) 
    {
      done[i] = yintpn( one[i], two[i], three[i], four[i], dy );
    }
  }

  free((char *)one);
  free((char *)two);
  free((char *)three);
  free((char *)four);

  return(0);
}

/*
  subroutine xinterp interpolates the input array x1 onto the output
  array x2, with nx2 points, oversampling the input array by oversamp
*/
int xinterp(x1, nx1, x2, nx2)
double *x1;
int nx1;
double *x2;
int nx2;
{
  double oversamp, x, a, b, c, d, dx;
  int jx, ix, inx;
  
  oversamp = (double)(nx2-1)/(double)(nx1-1);

  for(jx=0;jx<nx2;jx++)					/* jx is address of interpolated array */
  {
    x   = jx/oversamp;
    inx = (int)rint(x);

    if(fabs(x-(double)inx) < 0.0001)
    {
      x2[jx]=x1[inx];
    }
    else
    {
      ix = (int)x;
      dx = x-ix;
      b  = x1[ix];
      c  = x1[ix+1];

      if(ix > 0)
      {
        a = x1[ix-1];
      }
      else
      {
        a = b -c +b;         /* linear extrapolation for first point */
      }

      if(ix+2 < nx1)
      {
        d = x1[ix+2];
      }
      else
      {
        d = c -b +c;         /* linear extrapolation for last point */
      }

      x2[jx] = yintpn(a, b, c, d, dx);
    }
  }

  return(0);
}
