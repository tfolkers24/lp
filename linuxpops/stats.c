/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

#include "channels.h"

#include "proto.h"
#include "caclib_proto.h"

int feed;

struct DATASET dataset;

int mySort(a, b)
struct CHANNELS *a;
struct CHANNELS *b;
{
  return( (*a).x < (*b).x ? 1 : (*a).x > (*b).x ? -1 : 0 );
}


/* Prepare & flag bad channel data */
int prepareData(p1, p2)
struct SCAN_INFO *p1;
struct SCAN_INFO *p2;
{
  int i;

  st_report("PD: Preparing Data");

  p1->len = p1->head.datalen / sizeof(float);
  p2->len = p2->head.datalen / sizeof(float);

/* Assume all data good for the first pass */
  for(i=0;i<p1->len;i++)
  {
    if(fabs(p1->data[i]) > 1.0e-20)
    {
      p1->good[i] = 1;
    }
    else
    {
      p1->good[i] = 0;
      st_report("PD: Flagging X-Chan %3d: Value: %10.6f", i, p1->data[i]);
    }

    if(fabs(p2->data[i]) > 1.0e-20)
    {
      p2->good[i] = 1;
    }
    else
    {
      p2->good[i] = 0;
      st_report("PD: Flagging Y-Chan %3d: Value: %10.6f", i, p2->data[i]);
    }
  }

  return(0);
}


/* Find mean of datasets */
int findMean(p1, p2, pass)
struct SCAN_INFO *p1;
struct SCAN_INFO *p2;
int pass;
{
  int i, j;

  st_report(" ");

  p1->mean = p2->mean = 0.0;

  st_report("CM: Computing Pass: %d Mean", pass);

  for(i=0,j=0;i<p1->len;i++)
  {
    if(p1->good[i] && p2->good[i])
    {
      p1->mean   += p1->data[i];
      p2->mean   += p2->data[i];
      j++;
    }
  }

  p1->mean /= (double)j;
  p2->mean /= (double)j;

  st_report("CM: First-Pass: Mean-X = %10.6f, Mean-Y = %10.6f, %4d Good Channels", 
			p1->mean, p2->mean, j);

  return(j);
}


int remove10X(p1, p2, pass)
struct SCAN_INFO *p1;
struct SCAN_INFO *p2;
int pass;
{
  int i;

  st_report("Removing any Channels 10X Mean: Pass %d", pass);
  /* First cut at removing wild bad channels */
  for(i=0;i<p1->len;i++)
  {
    if(p1->good[i])
    {
     /* If any channel is 10 time worst then mean */
      if((fabs(p1->data[i]) / fabs(p1->mean)) > 10.0)
      {
        st_report("Flagging X-Chan %3d: Value: %10.6f: Factor: %.2f", 
					i, p1->data[i], fabs(p1->data[i]) / fabs(p1->mean));
        p1->good[i] = 0; /* Flag as bad */
      }
    }

    if(p2->good[i])
    {
      if((fabs(p2->data[i]) / fabs(p2->mean)) > 10.0)
      {
        st_report("Flagging Y-Chan %3d: Value: %10.6f: Factor: %.2f", 
					i, p2->data[i], fabs(p2->data[i]) / fabs(p2->mean));
        p2->good[i] = 0; /* Flag as bad */
      }
    }
  }

  return(0);
}


int correlateScans()
{
  int i, j, n, m, good, write_good, g;
  double linear;
  struct SCAN_INFO *p1, *p2;
  struct CHANNELS *chans;


  st_report("Entering correlateScans(): %.2f", h0->head.scan);

  bzero((char *)&dataset, sizeof(dataset));

  p1 = &dataset.info[0];
  p2 = &dataset.info[1];
  chans = &dataset.chans[0];

  /* Load the data into the struct */

  /* Channel #1 */
  bcopy((char *)&h0->head, (char *)&p1->head, sizeof(struct HEADER));
  bcopy((char *)&h0->head, (char *)&p2->head, sizeof(struct HEADER));

  p1->len = h0->head.noint;
  for(i=0;i<p1->len;i++)
  {
    p1->data[i] = h0->yplot1[i];
  }
  p1->rms = (2.0 * p1->head.stsys) / sqrt(p1->head.inttime * fabs(p1->head.freqres * 1e6));

  /* Channel #2 */
  p2->len = h0->head.noint;
  for(i=0;i<p2->len;i++)
  {
    p2->data[i] = h0->yplot2[i];
  }
  p2->rms = (2.0 * p2->head.stsys) / sqrt(p2->head.inttime * fabs(p2->head.freqres * 1e6));

  prepareData(p1, p2);

  good = findMean(p1, p2, 1);

  /* Now loop until no change in mean's or 5 times */
  for(m=0;m<5;m++)
  {
    remove10X(p1, p2, 2+m);

    g = findMean(p1, p2, 2+m);

    if(g == good) /* No change */
    {
      st_report("Converged on %d tries", m+2);
      break;
    }
    good = g;
  }

  st_report(" ");
  st_report("Now Remove any Channels 5X the rms");

  /* If still good, copy to sort array */

  n = p1->len;
  for(i=0,j=0;i<n;i++)
  {
    if(p1->good[i] && p2->good[i]) /* Already Flagged bad? */
    {
      if((p1->data[i] > (p1->mean - p1->rms * 5.0)) && (p1->data[i] < (p1->mean + p1->rms * 5.0)))
      {
        if((p2->data[i] > (p2->mean - p2->rms * 5.0)) && (p2->data[i] < (p2->mean + p2->rms * 5.0)))
        {
          chans[j].x    = (double)p1->data[i];
          chans[j].y    = (double)p2->data[i];
          chans[j].c    = (double)i;
          chans[j].good = 1;
          j++;
        }
        else
        {
	  st_report("Y-Channel %4d: %10.6f > Mean-Y: %10.6f", i, p2->data[i], p2->mean*5.0);
        }
      }
      else
      {
        st_report("X-Channel %4d: %10.6f > Mean-X: %10.6f", i, p1->data[i], p1->mean*5.0);
      }
    }
    else
    {
      st_report("Skipping Already Flagged Channel: %4d, %14.6f %14.6f", 
					i, p1->data[i], p2->data[i]);
    }
  }

  /* From now on only use j channels */

  st_report(" ");
  st_report("Sorting %d channels", j);

#ifndef __DARWIN__
  qsort(chans, j, sizeof(struct CHANNELS), (__compar_fn_t)mySort);
#else
  qsort(chans, j, sizeof(struct CHANNELS), mySort);
#endif

  for(i=0;i<j;i++)
  {
    dataset.xd[i] = chans[i].x;
    dataset.yd[i] = chans[i].y;
    dataset.sig[i] = 1.0;
  }

  st_report(" ");
  st_report("Fitting linear regression using grace_fit()");

  fitcurve(dataset.xd, dataset.yd, j, 1, dataset.coeff);

//  linear_regression(j, dataset.xd, dataset.yd, dataset.coeff);

  st_report("Ignoring Results with vaules 1.5 times rms: %10.6f", p2->rms);

  p2->rms *= 1.5;

  /* Copy results into H3 */

  hzero(&H3);  /* Clear it out */
  _copyImage(h0, &H3);

  write_good = 0;
  for(i=0;i<j;i++)
  {
    if(!chans[i].good)
    {
      st_report("Found a Bad one: %d:", chans[i].c);

      continue;
    }

    linear = dataset.coeff[1] * chans[i].x + dataset.coeff[0];

    if(chans[i].y < linear-p2->rms)
    {
      st_report("Ignoring channel[%4d]: data %10.6f < %10.6f", 
			(int)chans[i].c, chans[i].y, linear-p2->rms);
      continue;
    }

    if(chans[i].y > linear+p2->rms)
    {
      st_report("Ignoring channel[%4d]: data %10.6f > %10.6f", 
			(int)chans[i].c, chans[i].y, linear+p2->rms);
      continue;
    }

    if(verboseLevel & VERBOSE_STATS)
    {
      st_print("# Channel %4d\n", (int)chans[i].c);
      st_print("%10.6f %10.6f %10.6f %10.6f %10.6f\n", 
			chans[i].x,  
			chans[i].y, 
			linear, 
			linear + p2->rms, 
			linear - p2->rms);
    }

    H3.bplot[i]  = chans[i].x; 		/* Put the x-axis in the bplot array */
    H3.yplot1[i] = chans[i].y;
    H3.yplot2[i] = linear;
    H3.yplot3[i] = linear + p2->rms;
    H3.yplot4[i] = linear - p2->rms;

    write_good++;
  }

  H3.mode         = MODE_XCOR;
  H3.plotwin      = 1;
  H3.narray       = 4;
  H3.nsize        = j * sizeof(float);
  H3.pinfo.bplot  = j;

  _copyImage(&H3, &H0); /* Copy it int the default holder */

  _h0();  /* Point to H0 */

  sendImageStruct(&H0, MODE_XCOR);

  st_report("Wrote %d data points", write_good);

  return(0);
}


double benfordProb[] = {
	0.0000,
	0.3010,
	0.1761,
	0.1249,
	0.0969,
	0.0792,
	0.0669,
	0.0580,
	0.0512,
	0.0458
};

int doBenford()
{
  int i, j, k=0, n, len;
  int bins[10] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
  char *cp, vstring[256], fname[256], sysBuf[256], user[256];
  double f;
  FILE *fp;
  gsl_rng *rng;

  if(h0->head.scan == 0.0)
  {
    st_fail("No scan loaded yet");
    return(1);
  }

  if(!env->baselineremoved)
  {
    st_fail("doBenford(): Baselines must be removed first");
    return(0);
  }

  if(isSet(PF_R_FLAG))	/* Use random numbers instead */
  {
    if(args->ntokens < 2)
    {
      st_fail("doBenford(): Missing arg for random number points");
      printH_FlagHelp("benford");

      return(0);
    }

    n = atoi(args->secondArg);

    rng = gsl_rng_alloc(gsl_rng_taus);;

    gsl_rng_set(rng, rand());		/* Seed it with a random number */
  }
  else
  {
    n = h0->head.noint;
  }

  for(i=0;i<n;i++)
  {
    if(isSet(PF_R_FLAG))	/* Use random numbers instead */
    {
      f = gsl_rng_uniform_pos(rng) * 1e3;
    }
    else
    {
      f = (double)h0->yplot1[i];
    }

    if(f == 0.0)
    {
      continue;
    }

    k++;

    sprintf(vstring, "%f", fabs(f * 1e6));

    if(isSet(PF_V_FLAG))
    {
      st_report(vstring);
    }

    len = strlen(vstring);

    cp = &vstring[0];

    for(j=0;j<len;j++,cp++)
    {
      if(*cp == '1')
      {
        bins[1]++;
	break;
      }
      else
      if(*cp == '2')
      {
        bins[2]++;
	break;
      }
      else
      if(*cp == '3')
      {
        bins[3]++;
	break;
      }
      else
      if(*cp == '4')
      {
        bins[4]++;
	break;
      }
      else
      if(*cp == '5')
      {
        bins[5]++;
	break;
      }
      else
      if(*cp == '6')
      {
        bins[6]++;
	break;
      }
      else
      if(*cp == '7')
      {
        bins[7]++;
	break;
      }
      else
      if(*cp == '8')
      {
        bins[8]++;
	break;
      }
      else
      if(*cp == '9')
      {
        bins[9]++;
	break;
      }
    }
  }

  if(isSet(PF_R_FLAG))	/* Use random numbers instead */
  {
    gsl_rng_free(rng);
  }

  st_report(" ");
  st_report("Total Non-zero Elements: %d", k);
  st_report(" ");

  if(k > 0)
  {
    for(i=1;i<10;i++)
    {
      st_report("%d %7.4f %7.4f", i, benfordProb[i], (double)bins[i] / (double)k);
    }

    st_report(" ");
  }
  else
  {
    return(0);
  }

  sprintf(fname, "/tmp/%s/benford.dat", session);

  fp = fopen(fname, "w");
  if(!fp)
  {
    st_fail("doBenford(): Unable to open %s", fname);
    return(1);
  }

  for(i=1;i<10;i++)
  {
    fprintf(fp, "%d %7.4f %7.4f\n", i, benfordProb[i], (double)bins[i] / (double)k);
  }

  fclose(fp);

  cp = getenv("USER");

  if(cp)
  {
    sprintf(user, " --user %s", cp);
  }
  else
  {
    strcpy(user, " ");
  }

  sprintf(sysBuf, "killall -q %s xmgrace; %s -geometry 1920x1080 -p %s/share/.benford_params -nxy %s &", user, xmgr_plotter, pkg_home, fname);

  system(sysBuf);

  return(0);
}
