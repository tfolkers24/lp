/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

float sstack[MAX_STACK];

/* Stack routines; empty, fill, add, delete, list, inc */
int doStack()
{

  if(verboseLevel & VERBOSE_BATCH)
  {
    st_report("Stack Entered");
  }

  if(isSet(PF_E_FLAG))
  {
    stackEmpty();
  }
  else
  if(isSet(PF_F_FLAG))
  {
    stackFill();
  }
  else
  if(isSet(PF_G_FLAG))
  {
    subArgs(2, "%.2f", env->stackscan);
    p_getScan();
  }
  else
  if(isSet(PF_L_FLAG))
  {
    stackList();
  }
  else
  if(isSet(PF_I_FLAG))
  {
    stackInc();
  }
  else
  if(isSet(PF_A_FLAG)) /* Not sure about this one */
  {
    stackAdd();
  }
  else
  if(isSet(PF_R_FLAG))
  {
    stackReset();
  }
  else
  {
    printH_FlagHelp(args->firstArg);
    return(1);
  }

  return(0);
}


int stackFill()
{
  int i, indx=0;
  struct HEADER *p;

  if(verboseLevel & VERBOSE_BATCH)
  {
    st_report("Inside Stack Fill");
  }

  /* First clear out old stack */
  stackEmpty();

  p = &sa[0];

  /* Now find the qualified scans */
  for(i=0;i<env->numofscans;i++,p++)
  {
    /* Check the most obvious critiria */
    if(!meetsCritiria(p, CHECK_ALL))
    {
      continue;
    }

    if(env->mode == MODE_SPEC)
    {
    /* Check for proper LINE* Mode */
      if(strncmp(p->obsmode, "LINEBSP",  7) &&
         strncmp(p->obsmode, "LINEPS",   6) &&
         strncmp(p->obsmode, "LINEFS",   6) &&
         strncmp(p->obsmode, "LINETPON", 8) &&
         strncmp(p->obsmode, "LINEAPS",  7)) /* We only process BSP, APS & PS so far*/
      {
        continue; /* Not Spectral */
      }

      if(p->openpar[5] == 5555.5) /* a BSP focal */
      {
        continue; /* Ignore */
      }
    }
    else
    {
      if(strncmp(p->obsmode, "CONTSEQ", 7)) /* We only process Seq */
      {
        continue; /* No good */
      }
    }

    /* Is this scan ignored??? */
    if(isScanIgnored(p->scan))
    {
      incrementRejIgnored();
      continue;
    }

    /* Made it this far; add it in */

    sstack[indx++] = p->scan;
  }

  env->acount = indx;

  st_report("Added %d scans to stack", env->acount);

  stackReset();

  return(0);
}


int stackReset()
{
  /* Reset stack pointer to zero */
  env->stackindx = 0; /* No it's pointing to the first element */
  env->stackscan = sstack[env->stackindx];
  env->stackdone = 0;

  if(verboseLevel & VERBOSE_BATCH)
  {
    st_report("Stack Pointing reset to Zero");
  }

  return(0);
}


int stackList()
{
  int i;

  if(verboseLevel & VERBOSE_BATCH)
  {
    st_report("Inside Stack List");
  }

  for(i=0;i<env->acount;i++)
  {
    if(!(i%8))
    {
      st_print("\n%4d: ", i+1);
    }

    st_print("%8.2f", sstack[i]);
  }

  if(env->acount)
  {
    st_print("\n");
  }

  return(0);
}


int stackEmpty()
{
  if(verboseLevel & VERBOSE_BATCH)
  {
    st_report("Inside Stack Empty");
  }

  bzero((char *)&sstack, sizeof(sstack));

  env->stackindx = 0;
  env->acount    = 0;
  env->stackdone = 0;

  return(0);
}


int stackAdd()
{
  st_report("Inside Stack Add: Not implemented yet");

  return(0);
}


int stackInc()
{
  if(verboseLevel & VERBOSE_BATCH)
  {
    st_report("Inside Stack Inc");
  }

  env->stackindx++;

  env->stackscan = sstack[env->stackindx];

  if(env->stackindx >= env->acount)
  {
    st_report("stack(): Reached end of stack");
    env->stackdone = 1;
    env->stackindx = env->acount;
  }

  return(0);
}
