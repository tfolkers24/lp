#include <stdio.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_min.h>
#include <gsl/gsl_test.h>
#include <gsl/gsl_ieee_utils.h>

#include "extern.h"

#define MAX_MIN_PAIRS 2048

struct PAIRS {
	double x;
	double y;
};
struct PAIRS min_pairs[MAX_MIN_PAIRS];

int max_min_pairs = 0;
int stop_min      = 0;


void handler (const char * reason, const char * file, int line, int gsl_errno)
{
  st_fail("Minimize(): Failed: %s", reason);

  stop_min = 1;

  return;
}

int populateMinPairs(x, y, rest, n)
float *x, *y;
double rest;
int n;
{
  int i;

  for(i=0;i<n;i++)
  {
    min_pairs[i].x = rest-(double)x[i];		/* convert x-axis to freq */
    min_pairs[i].y = (double)y[i];
  }

  max_min_pairs = n;

  return(0);
}


/* Return the interpolated value in the table of "no" points, (xtab, ytab), 
 * that corresponds to the xtab value, "x".  "xtab" must be stored in increasing
 * order.
 */
double min_interpl8( xtab, ytab, no, x)
double *xtab;
double *ytab;
int no;
double x;
{
  int i;

  if(x >= xtab[0])
  {
    for(i=0;i<no-1 && xtab[i+1]<x;i++)
    {
      continue;
    }

    if(i >= no-1)
    {
      i = no - 2;
    }
  }
  else
  {
    for(i=0;i>0 && xtab[i]>x;i--)
    {
      continue;
    }

    if(i < 0)
    {
      i = 0;
    }
  }

  return(ytab[i] + (ytab[i+1] - ytab[i]) * (x - xtab[i]) /
                   (xtab[i+1] - xtab[i]));
}


double findY(x)
double x;
{
  int i;
  double y=0.0;
  static double xx[MAX_MIN_PAIRS], yy[MAX_MIN_PAIRS];

  for(i=0;i<MAX_MIN_PAIRS;i++)
  {
    xx[i] = min_pairs[i].x;
    yy[i] = min_pairs[i].y;
  }

  y = min_interpl8(xx, yy, MAX_MIN_PAIRS, x);

  return(y);
}


double fn1 (double x, void * params)
{
  double y;

  (void)(params); /* avoid unused parameter warning */

  y = findY(x);

  printf("fn1(%f) = %f\n", x, y);

  return(y);
}


int minimize (m, a, b, m_expected)
double m, a, b, m_expected;
{
  int status;
  int iter = 0, max_iter = 300;
  const gsl_min_fminimizer_type *T;
  gsl_min_fminimizer *s;
  gsl_function F;
  gsl_error_handler_t *old_handler;

  stop_min = 0;

  old_handler = gsl_set_error_handler(&handler);

  F.function = &fn1;
  F.params = 0;

  T = gsl_min_fminimizer_brent;
  s = gsl_min_fminimizer_alloc (T);

  gsl_min_fminimizer_set (s, &F, m, a, b);

  st_report ("using %s method", gsl_min_fminimizer_name (s));
  st_report ("%5s [%9s, %9s] %9s %10s %9s", "iter", "lower", "upper", "min", "err", "err(est)");
  st_report ("%5d [%.7f, %.7f] %.7f %+.7f %.7f", iter, a, b, m, m - m_expected, b - a);

  do
  {
    iter++;
    status = gsl_min_fminimizer_iterate (s);

    m = gsl_min_fminimizer_x_minimum (s);
    a = gsl_min_fminimizer_x_lower (s);
    b = gsl_min_fminimizer_x_upper (s);

    if(stop_min)
    {
      gsl_set_error_handler(old_handler);

      return(1);
    }

    status = gsl_min_test_interval (a, b, 0.01, 0.0);

    if (status == GSL_SUCCESS)
    {
      st_report ("Converged");
    }

    st_report ("%5d [%.7f, %.7f] %.7f %+.7f %.7f", iter, a, b, m, m - m_expected, b - a);
  }
  while (status == GSL_CONTINUE && iter < max_iter);

 gsl_set_error_handler(old_handler);

  gsl_min_fminimizer_free (s);

  return(status);
}
