/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

struct LP_IMAGE_INFO accum_image;

struct LP_IMAGE_INFO H0; /* Place where all data is read into */
struct LP_IMAGE_INFO H1;
struct LP_IMAGE_INFO H2;
struct LP_IMAGE_INFO H3;
struct LP_IMAGE_INFO H4;
struct LP_IMAGE_INFO H5;
struct LP_IMAGE_INFO H6;
struct LP_IMAGE_INFO H7;
struct LP_IMAGE_INFO H8;
struct LP_IMAGE_INFO H9;

struct LP_IMAGE_INFO *h0 = &H0;
int    currentImage   = 0;

#define MAX_SAVE_IMAGE_ARRAYS  10


struct SAVE_IMAGE {
	char magic[16];			/* Ascii keyword */
	int  ssize;			/* Total size of this struct */
	int  hsize;			/* Size of individual IMAGE structs */
	struct LP_IMAGE_INFO images[MAX_SAVE_IMAGE_ARRAYS];
};

struct SAVE_IMAGE save_image;

#define SAVE_IMAGE_PREAMBLE (sizeof(save_image.magic) + sizeof(save_image.ssize) + sizeof(save_image.hsize))


extern struct SHELL_GUESS shell_guess;

/* Internal convienence function */
int _h0()
{
  h0                 = &H0;
  currentImage       = 0;
  env->currentImage  = 0;

  setPrompt();

  return(0);
}

int _h1()
{
  h0                 = &H1;
  currentImage       = 1;
  env->currentImage  = 1;

  setPrompt();

  return(0);
}

int _h2()
{
  h0                 = &H2;
  currentImage       = 2;
  env->currentImage  = 2;

  setPrompt();

  return(0);
}

int _h3()
{
  h0                 = &H3;
  currentImage       = 3;
  env->currentImage  = 3;

  setPrompt();

  return(0);
}

int _h4()
{
  h0                 = &H4;
  currentImage       = 4;
  env->currentImage  = 4;

  setPrompt();

  return(0);
}

int _h5()
{
  h0                 = &H5;
  currentImage       = 5;
  env->currentImage  = 5;

  setPrompt();

  return(0);
}

int _h6()
{
  h0                 = &H6;
  currentImage       = 6;
  env->currentImage  = 6;

  setPrompt();

  return(0);
}

int _h7()
{
  h0                 = &H7;
  currentImage       = 7;
  env->currentImage  = 7;

  setPrompt();

  return(0);
}

int _h8()
{
  h0                 = &H8;
  currentImage       = 8;
  env->currentImage  = 8;

  setPrompt();

  return(0);
}

int _h9()
{
  h0                 = &H9;
  currentImage       = 9;
  env->currentImage  = 9;

  setPrompt();

  return(0);
}

int setHeader()
{
  int i;

  i = atoi(args->currentCmd+1);

  /* Set pointing and make sure the 'name' gets fixed */
  switch(i)
  {
    case 0: _h0(); strcpy(h0->name, "H0"); break;
    case 1: _h1(); strcpy(h0->name, "H1"); break;
    case 2: _h2(); strcpy(h0->name, "H2"); break;
    case 3: _h3(); strcpy(h0->name, "H3"); break;
    case 4: _h4(); strcpy(h0->name, "H4"); break;
    case 5: _h5(); strcpy(h0->name, "H5"); break;
    case 6: _h6(); strcpy(h0->name, "H6"); break;
    case 7: _h7(); strcpy(h0->name, "H7"); break;
    case 8: _h8(); strcpy(h0->name, "H8"); break;
    case 9: _h9(); strcpy(h0->name, "H9"); break;

    default: st_fail("Unknown H value %d", i); return(1); break;
  }

  setPrompt();
  st_report("Setting Holder to %s", h0->name); 

  return(0);
}


struct LP_IMAGE_INFO *findImage(i)
int i;
{
  struct LP_IMAGE_INFO *image=NULL;

  switch(i)
  {
    case 0: image = &H0; break;
    case 1: image = &H1; break;
    case 2: image = &H2; break;
    case 3: image = &H3; break;
    case 4: image = &H4; break;
    case 5: image = &H5; break;
    case 6: image = &H6; break;
    case 7: image = &H7; break;
    case 8: image = &H8; break;
    case 9: image = &H9; break;
  }

  return(image);
}


int parseHarg(arg)
char *arg;
{
  int val;

  if(!strncasecmp(arg, "h", 1)) /* did the user pass 'h1' instead of simply '1' */
  {
    val = atoi(arg+1);
  }
  else
  {
    val = atoi(arg); /* plain '1' */
  }

  return(val);
}
  


int copyImage()
{
  int f, t;
  char tmp[80];
  struct LP_IMAGE_INFO *from, *to;

  if(strlen(args->secondArg) && strlen(args->thirdArg))
  {
    f = parseHarg(args->secondArg);
    t = parseHarg(args->thirdArg);

    if((f >= 0 && f < MAX_IMAGES) && (t >=0 && t < MAX_IMAGES))
    {
      if(f == t)
      {
        st_fail("copyImage(): From and To must be different");
        printH_FlagHelp("image");

        return(1);
      }

      from = findImage(f);
      to   = findImage(t);

      if(from && to)
      {
        st_report("Copying %d to %d; %s to %s", f, t, from->name, to->name);

        strcpy(tmp, to->name); /* Preserve the name */
        bcopy((char *)from, (char *)to, sizeof(struct LP_IMAGE_INFO));
        strcpy(to->name, tmp);
      }
      else
      {
	st_fail("copyImage(): Unable to locate from or to headers");
        printH_FlagHelp("image");

        return(1);
      }
    }
  }
  else
  {
    st_fail("Missing args for image copy");

    printH_FlagHelp("image");

    return(1);
  }

  return(0);
}


int _copyImage(i1, i2)
struct LP_IMAGE_INFO *i1, *i2;
{
  char tmp[80];

  st_report("Copying %s to %s", i1->name, i2->name);

  strcpy(tmp, i2->name); /* Preserve the name */
  bcopy((char *)i1, (char *)i2, sizeof(struct LP_IMAGE_INFO));
  strcpy(i2->name, tmp);

  return(0);
}


int printImageHeader()
{
  int i;

  st_print("struct LP_IMAGE_INFO:\n");
  st_print("{\n");
  st_print("\tmagic:        %s\n", h0->magic);
  st_print("\tname:         %s\n", h0->name);
  st_print("\tmode:         %d\n", h0->mode);
  st_print("\tpeakChan:     %d\n", h0->peakChan);
  st_print("\tplotwin:      %d\n", h0->plotwin);
  st_print("\tnarray:       %d\n", h0->narray);
  st_print("\tnsize:        %d\n", h0->nsize);
  st_print("\thline:        %d\n", h0->satActive);
  st_print("\tdatamin:      %f\n", h0->datamin);
  st_print("\tdatamax:      %f\n", h0->datamax);


  st_print("\n");

  st_print("\tstruct ACCUM_INFO ainfo:\n");
  st_print("\t{\n");
  st_print("\t\tmagic:   %s\n", h0->ainfo.magic);
  st_print("\t\tscanct:  %d\n", h0->ainfo.scanct);
  st_print("\t\tbscan:   %.2f\n", h0->ainfo.bscan);
  st_print("\t\tescan:   %.2f\n", h0->ainfo.escan);
  st_print("\t\twt:      %f\n", h0->ainfo.wt);
  st_print("\t}\n");

  st_print("\n");

  st_print("\tstruct BASELINE_INFO binfo:\n");
  st_print("\t{\n");
  st_print("\t\tmagic:      %s\n", h0->binfo.magic);
  st_print("\t\tdoBaseLine: %d\n", h0->binfo.doBaseLine);

  for(i=0;i<MAX_BASELINES;i++)
  {
    if(i==0)
    {
      st_print("\t\tBaselines: [%d]   %4d %4d\n", 
				i+1, h0->binfo.baselines[i][0], h0->binfo.baselines[i][1]);
    }
    else
    {
      st_print("\t\t           [%d]   %4d %4d\n", 
				i+1, h0->binfo.baselines[i][0], h0->binfo.baselines[i][1]);
    }
  }

  st_print("\t\ta:          %f\n", h0->binfo.a);
  st_print("\t\tb:          %f\n", h0->binfo.b);
  st_print("\t\trms:        %f\n", h0->binfo.rms);
  st_print("\t}\n");

  st_print("\n");

  st_print("\tstruct PLOT_INFO pinfo:\n");
  st_print("\t{\n");
  st_print("\t\tmagic:       %s\n", h0->pinfo.magic);
  st_print("\t\tmarkers:     %d\n", h0->pinfo.markers);
  st_print("\t\tplotStyle:   %d\n", h0->pinfo.plotStyle);
  st_print("\t\tx1mode:      %d\n", h0->pinfo.x1mode);
  st_print("\t\tx2mode:      %d\n", h0->pinfo.x2mode);
  st_print("\t\tgridMode:    %d\n", h0->pinfo.gridMode);
  st_print("\t\tzline:       %d\n", h0->pinfo.zline);
  st_print("\t\tbadchshow:   %d\n", h0->pinfo.badchshow);
  st_print("\t\tbmark:       %d\n", h0->pinfo.bmark);
  st_print("\t\tmshow:       %d\n", h0->pinfo.bshow);
  st_print("\t\tbdrop:       %d\n", h0->pinfo.bdrop);
  st_print("\t\tedrop:       %d\n", h0->pinfo.edrop);
  st_print("\t\tdobadch:     %d\n", h0->pinfo.dobadch);
  st_print("\t\tyscale:      %d\n", h0->pinfo.yscale);
  st_print("\t\tplot_noint:  %d\n", h0->pinfo.plot_noint);
  st_print("\t\tbplot:       %d\n", h0->pinfo.bplot);
  st_print("\t\tnspline:     %d\n", h0->pinfo.nspline);
  st_print("\t\typlotmode:   %d\n", h0->pinfo.yplotmode);
  st_print("\t\tylower:      %f\n", h0->pinfo.ylower);
  st_print("\t\tyupper:      %f\n", h0->pinfo.yupper);
  st_print("\t\tplotmin:     %f\n", h0->pinfo.plotmin);
  st_print("\t\tplotmax:     %f\n", h0->pinfo.plotmax);
  st_print("\t}\n");

  st_print("\n");


  for(i=0;i<MAX_BAD_CHANS;i++)
  {
    if(h0->badChans[i] > 0.0)
    {
      st_print("\tbadChans[%4d]     %4d\n", i, h0->badChans[i]);
    }
  }

  st_print("}\n");


  return(0);
}



int subtractImage()
{
  char execBuf[256];

  sprintf(execBuf, "image /d %s %s", args->secondArg, args->thirdArg);

  parseCmd(execBuf);

  return(0);
}


int doImage()
{
  if(isSet(PF_C_FLAG))
  {
    copyImage();

    return(0);
  }

  if(isSet(PF_L_FLAG))
  {
    imageSummary();

    return(0);
  }

  if(isSet(PF_P_FLAG))
  {
    printImageHeader();

    return(0);
  }

  if(isSet(PF_S_FLAG))
  {
    doSum();

    return(0);
  }

  if(isSet(PF_D_FLAG))
  {
    doImageDiff();

    doAutoScale();

    xx();

    return(0);
  }

  if(isSet(PF_Y_FLAG))
  {
    doImageDiv();

    doAutoScale();

    xx();

    return(0);
  }

  if(isSet(PF_T_FLAG))
  {
    setImageTitle();

    return(0);
  }

  if(isSet(PF_Z_FLAG))
  {
    if(isSet(PF_A_FLAG)) /* Zero them all ?? */
    {
      clearAllImages();
    }
    else
    {
      zeroImage();
    }

    return(0);
  }

  printH_FlagHelp("image");

  return(0);
}



int imageSummary()
{
  int i;
  char source[32], timeStr[256];
  struct LP_IMAGE_INFO *p;

  st_print("\nHx      Freq          Source      Scan    Bscan   Escan      Time    Resolution  Channels  Narrays  Tsys   Tcal Shifted   Title\n");
  st_print(  "        GHz                       Cnt                      HH:MM:SS      MHz                      \n\n");

  for(i=0;i<MAX_IMAGES;i++)
  {
    p = findImage(i);

    strncpy(source, p->head.object, 16);
    source[16] = '\0';
    strcmprs(source);

    sexagesimal(p->head.inttime / 3600.0, timeStr, DD_MM_SS);

    if(p->shifted < 0)
    {
      setColor(ANSI_RED);
    }
    else
    if(p->shifted > 0)
    {
      setColor(ANSI_BLUE);
    }

    st_print("%s   %10.6f %16.16s  %3d  %8.2f %8.2f  %s   %7.3f    %5d        %d  %6.1f  %6.1f   %4d    %s\n", 
		p->name, 
		p->head.restfreq / 1000.0,  source,
		p->ainfo.scanct, p->ainfo.bscan, p->ainfo.escan,
		timeStr,
		p->head.freqres, (int)p->head.noint, 
		p->narray,
		p->head.stsys, p->head.tcal, p->shifted, p->title);

    if(p->shifted)
    {
      setColor(ANSI_BLACK);
    }
  }

  return(0);
}


int setImageTitle()
{
  int i;
  char tbuf[512];

  if(args->ntokens > 1)
  {
    h0->title[0] = '\0';

    for(i=1;i<args->ntokens;i++)
    {
      sprintf(tbuf, "%s ", args->tok[i]);
      strcat(h0->title, tbuf);
    }

    st_report("Setting %s Image Holder Title to: %s", h0->name, h0->title);
  }
  else
  {
    printH_FlagHelp("setimagetitle");

    return(1);
  }

  return(0);
}




int hzero(image)
struct LP_IMAGE_INFO *image;
{
  char tmp[80];

  strcpy(tmp, image->name); /* Preserve the name */
  bzero((char *)image, sizeof(struct LP_IMAGE_INFO));
  strcpy(image->name, tmp);

  return(0);
}


int clearAllImages()
{
  int i;

  for(i=0;i<MAX_IMAGES;i++)
  {
    switch(i)
    {
      case 0: _h0(); break;
      case 1: _h1(); break;
      case 2: _h2(); break;
      case 3: _h3(); break;
      case 4: _h4(); break;
      case 5: _h5(); break;
      case 6: _h6(); break;
      case 7: _h7(); break;
      case 8: _h8(); break;
      case 9: _h9(); break;
    }

    hzero(h0);
  }

  _h0();

  return(0);
}


int hLoad()
{
  int fd, ret;

  if(!strlen(args->secondArg))
  {
    st_fail("Missing arg for hload");
    return(1);
  }

  if((fd = open(args->secondArg, O_RDONLY)) < 0)
  {
    st_fail("hload(): Unable to open file %s", args->secondArg);
    return(1);
  }

  ret = read(fd, &save_image, SAVE_IMAGE_PREAMBLE);

  if(ret != SAVE_IMAGE_PREAMBLE)
  {
    st_fail("hload(): read returned %d; not %d", ret, SAVE_IMAGE_PREAMBLE);
    close(fd);
    return(1);
  }

  if(strncmp(save_image.magic, "SAVE_IMAGE", 10))
  {
    st_fail("hload(): file has wrong magic work %s; not %s", save_image.magic, "SAVE_IMAGE");
    close(fd);
    return(1);
  }

  if(save_image.ssize != sizeof(save_image))
  {
    st_fail("hload(): file wrong size %d; not %d", save_image.ssize, sizeof(save_image));
    close(fd);
    return(1);
  }

  if(save_image.hsize != sizeof(struct LP_IMAGE_INFO))
  {
    st_fail("hload(): image elements wrong size %d; not %d", save_image.hsize, sizeof(struct LP_IMAGE_INFO));
    close(fd);
    return(1);
  }

  /* Rewind the file */
  lseek(fd, 0L, SEEK_SET);

  ret = read(fd, &save_image, sizeof(save_image));

  close(fd);

  if(ret != sizeof(save_image))
  {
    st_fail("hload(): struct read returned %d; not %d", ret, sizeof(save_image));
    return(1);
  }

  st_report("hload(): Read in %d bytes from %s", ret, args->secondArg);

  bcopy((char *)&save_image.images[0], (char *)&H0, sizeof(struct LP_IMAGE_INFO));
  bcopy((char *)&save_image.images[1], (char *)&H1, sizeof(struct LP_IMAGE_INFO));
  bcopy((char *)&save_image.images[2], (char *)&H2, sizeof(struct LP_IMAGE_INFO));
  bcopy((char *)&save_image.images[3], (char *)&H3, sizeof(struct LP_IMAGE_INFO));
  bcopy((char *)&save_image.images[4], (char *)&H4, sizeof(struct LP_IMAGE_INFO));
  bcopy((char *)&save_image.images[5], (char *)&H5, sizeof(struct LP_IMAGE_INFO));
  bcopy((char *)&save_image.images[6], (char *)&H6, sizeof(struct LP_IMAGE_INFO));
  bcopy((char *)&save_image.images[7], (char *)&H7, sizeof(struct LP_IMAGE_INFO));
  bcopy((char *)&save_image.images[8], (char *)&H8, sizeof(struct LP_IMAGE_INFO));
  bcopy((char *)&save_image.images[9], (char *)&H9, sizeof(struct LP_IMAGE_INFO));

  imageSummary();

  return(0);
}


int hSave()
{
  int fd, ret;

  if(!strlen(args->secondArg))
  {
    st_fail("Missing arg for hsave");
    return(1);
  }

  strcpy(save_image.magic, "SAVE_IMAGE");
  save_image.ssize = sizeof(save_image);
  save_image.hsize = sizeof(struct LP_IMAGE_INFO);

  bcopy((char *)&H0, (char *)&save_image.images[0], sizeof(struct LP_IMAGE_INFO));
  bcopy((char *)&H1, (char *)&save_image.images[1], sizeof(struct LP_IMAGE_INFO));
  bcopy((char *)&H2, (char *)&save_image.images[2], sizeof(struct LP_IMAGE_INFO));
  bcopy((char *)&H3, (char *)&save_image.images[3], sizeof(struct LP_IMAGE_INFO));
  bcopy((char *)&H4, (char *)&save_image.images[4], sizeof(struct LP_IMAGE_INFO));
  bcopy((char *)&H5, (char *)&save_image.images[5], sizeof(struct LP_IMAGE_INFO));
  bcopy((char *)&H6, (char *)&save_image.images[6], sizeof(struct LP_IMAGE_INFO));
  bcopy((char *)&H7, (char *)&save_image.images[7], sizeof(struct LP_IMAGE_INFO));
  bcopy((char *)&H8, (char *)&save_image.images[8], sizeof(struct LP_IMAGE_INFO));
  bcopy((char *)&H9, (char *)&save_image.images[9], sizeof(struct LP_IMAGE_INFO));

  if((fd = open(args->secondArg, O_CREAT | O_RDWR, 0664)) < 0)
  {
    st_fail("hsave(): Unable to open file %s", args->secondArg);
    return(1);
  }

  ret = write(fd, &save_image, sizeof(save_image));

  close(fd);

  if(ret != sizeof(save_image))
  {
    st_fail("hsave(): write returned %d; not %d", ret, sizeof(save_image));
    return(1);
  }

  st_report("hsave(): Wrote %d bytes to %s", ret, args->secondArg);

  return(0);
}


/* Copy in any environ variable required to plot this */
int copyEnv2Image(image)
struct LP_IMAGE_INFO *image;
{
  image->mode             = env->mode;

        /* Baseline info */
  image->binfo.doBaseLine = env->dobaseline;
  bcopy((char *)&env->baselines[0], (char *)&image->binfo.baselines, sizeof(image->binfo.baselines));

  image->peakChan         = env->peakChan;

        /* Plot info */
  image->pinfo.plotStyle  = env->plotstyle;
  image->pinfo.x1mode     = env->x1mode;
  image->pinfo.x2mode     = env->x2mode;
  image->pinfo.gridMode   = env->gridmode;
  image->pinfo.markers    = env->markers;
  image->pinfo.zline      = env->zline;
  image->pinfo.bmark      = env->bmark;
  image->pinfo.bshow      = env->bshow;
  image->pinfo.mshow      = env->mshow;
  image->pinfo.bdrop      = env->bdrop;
  image->pinfo.edrop      = env->edrop;
  image->pinfo.yupper     = env->yupper;
  image->pinfo.ylower     = env->ylower;
  image->pinfo.yscale     = env->yscale;
  image->pinfo.dobadch    = env->dobadchan;
  image->pinfo.badchshow  = env->badchanshow;

        /* Bad channel info */
  bcopy((char *)&env->badchans, (char *)&image->badChans, sizeof(image->badChans));

  return(0);
}


int sendImageStruct(image, mode)
struct LP_IMAGE_INFO *image;
int mode;
{
  int i;

  if(verboseLevel & VERBOSE_COMM)
  {
    st_report("Sending plot to XGraphic");
  }

  strcpy(image->magic, "LP_IMAGE_INFO");

  image->mode      = mode;
  image->plotwin   = env->plotwin;
  image->satActive = env->usesat;
  image->issmt     = env->issmt;
  image->xdelay    = env->xdelay;

  image->pinfo.paperw    = (float)env->paperw;
  image->pinfo.paperh    = (float)env->paperh;
  image->pinfo.xstart    = env->xstart;
  image->pinfo.ystart    = env->ystart;
  image->pinfo.nightmode = env->displaymode;

  bcopy((char *)&env->winfo, &image->winfo, sizeof(struct WINDOWS));
  bcopy((char *)&env->sinfo, &image->sinfo, sizeof(struct SHELL));

//  if(shell_guess.npoints)
//  {
//    st_report("Sending shell_guess struct");
//    sock_write(xgraphic, (char *)&shell_guess, SHELL_GUESS_SIZE);
//  
//  }

  sock_write(xgraphic, (char *)image, SHORT_IMAGE_SIZE);

  for(i=0;i<image->narray;i++)
  {
    switch(i)
    {
      case 0:
        sock_write(xgraphic, (char *)image->yplot1, image->nsize);
        break;

      case 1:
        sock_write(xgraphic, (char *)image->yplot2, image->nsize);
        break;

      case 2:
        sock_write(xgraphic, (char *)image->yplot3, image->nsize);
        break;

      case 3:
        sock_write(xgraphic, (char *)image->yplot4, image->nsize);
        break;
    }
  }

  if(image->pinfo.bplot)		/* Baseline profile */
  {
    sock_write(xgraphic, (char *)image->bplot, image->nsize);
  }

  if(last_plot != LAST_PLOT_OTF_CUT)
  {
    last_plot = LAST_PLOT_SPEC_ACCUM;
  }

  pushUndoStack();		/* Save this image for possible 'undo' */

  return(0);
}


int zeroImage()
{
  int i;
  char tmp[256];
  struct LP_IMAGE_INFO *img;

  if(strlen(args->secondArg))
  {
    i = parseHarg(args->secondArg);

    if((i >= 0 && i < MAX_IMAGES))
    {
      img = findImage(i);

      strcpy(tmp, img->name); /* Preserve the name */
      bzero((char *)img, sizeof(struct LP_IMAGE_INFO));
      strcpy(img->name, tmp);
    }
    else
    {
      st_fail("zeroImage(): Arg %d out of range: 0 - %d", MAX_IMAGES);
    }
  }
  else
  {
    st_fail("Missing arg for zeroImage()");
  }

  return(0);
}
