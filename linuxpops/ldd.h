#ifndef LDD_H
#define LDD_H
 
struct LDD_STRUCT { 
	 char *lib; 
	 char *version; 
}; 
 
struct LDD_STRUCT ldd[] = { 
	 { "/lib64/ld-linux-x86-64.so.2", "2.17" },
	 { "libcom_err.so.2", "1.42.9" },
	 { "libcrypto.so.10", "1.0.2k" },
	 { "libcrypt.so.1", "2.17" },
	 { "libc.so.6", "2.17" },
	 { "libdl.so.2", "2.17" },
	 { "libfreebl3.so", "3.67.0" },
	 { "libgcc_s.so.1", "4.8.5" },
	 { "libgfortran.so.3", "4.8.5" },
	 { "libgslcblas.so.0", "1.15" },
	 { "libgsl.so.25", "None" },
	 { "libgssapi_krb5.so.2", "1.15.1" },
	 { "libk5crypto.so.3", "1.15.1" },
	 { "libkeyutils.so.1", "1.5.8" },
	 { "libkrb5.so.3", "1.15.1" },
	 { "libkrb5support.so.0", "1.15.1" },
	 { "libm.so.6", "2.17" },
	 { "libmysqlclient.so.21", "8.0.28" },
	 { "libnsl.so.1", "2.17" },
	 { "libpcre.so.1", "8.32" },
	 { "libpthread.so.0", "2.17" },
	 { "libquadmath.so.0", "4.8.5" },
	 { "libreadline.so.6", "6.2" },
	 { "libresolv.so.2", "2.17" },
	 { "librt.so.1", "2.17" },
	 { "libselinux.so.1", "2.5" },
	 { "libssl.so.10", "1.0.2k" },
	 { "libstdc++.so.6", "4.8.5" },
	 { "libtinfo.so.5", "5.9" },
	 { "libX11.so.6", "1.6.7" },
	 { "libXau.so.6", "1.0.8" },
	 { "libxcb.so.1", "1.13" },
	 { "libz.so.1", "1.2.7" },
	 { "linux-vdso.so.1", "None" },
 
	 { NULL, NULL }
}; 
 
#endif
 
