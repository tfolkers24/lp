/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

static struct HEADER head;

struct HEAD_PARAMS head_params[] =  {

	{ 	"headlen", 	&head.headlen, 		NULL,			DOUBLE, 	1,  0, 			1, 0},
	{ 	"datalen", 	&head.datalen, 		NULL,			DOUBLE, 	1,  0, 			1, 0},
	{ 	"scan", 	&head.scan, 		NULL,			DOUBLE, 	1,  0, 			1, 0},
	{ 	"obsid", 	NULL,			&head.obsid[0],		CHARSTAR, 	8,  CLASS_TELESCOPE, 	1, 0},
	{ 	"observer", 	NULL,			&head.observer[0],	CHARSTAR, 	16, CLASS_TELESCOPE, 	1, 0},
	{ 	"telescop", 	NULL,			&head.telescop[0],	CHARSTAR, 	8,  CLASS_TELESCOPE, 	1, 0},
	{ 	"projid", 	NULL,			&head.projid[0],	CHARSTAR, 	8,  CLASS_TELESCOPE, 	1, 0},
	{ 	"object", 	NULL,			&head.object[0],	CHARSTAR, 	16, CLASS_SOURCE, 	1, 0},
	{ 	"obsmode", 	NULL,			&head.obsmode[0],	CHARSTAR, 	8,  0, 			1, 0},
	{ 	"frontend", 	NULL,			&head.frontend[0],	CHARSTAR, 	8,  CLASS_TELESCOPE, 	1, 0},
	{ 	"backend", 	NULL,			&head.backend[0],	CHARSTAR, 	8,  CLASS_TELESCOPE, 	1, 0},
	{ 	"precis", 	NULL,			&head.precis[0],	CHARSTAR, 	8,  0, 			1, 0},

	{ 	"xpoint", 	&head.xpoint, 		NULL,			DOUBLE, 	1,  0, 			2, 0},
	{ 	"ypoint", 	&head.ypoint, 		NULL,			DOUBLE, 	1,  0, 			2, 0},
	{ 	"uxpnt", 	&head.uxpnt, 		NULL,			DOUBLE, 	1,  CLASS_SOURCE, 	2, 0},
	{ 	"uypnt", 	&head.uypnt, 		NULL,			DOUBLE, 	1,  CLASS_SOURCE, 	2, 0},
	{ 	"ptcon", 	&head.ptcon[0], 	NULL,			DOUBLE, 	4,  0, 			2, 0},
	{ 	"orient", 	&head.orient, 		NULL,			DOUBLE, 	1,  0, 			2, 0},
	{ 	"focusr", 	&head.focusr, 		NULL,			DOUBLE, 	1,  CLASS_TELESCOPE, 	2, 0},
	{ 	"focusv", 	&head.focusv, 		NULL,			DOUBLE, 	1,  CLASS_TELESCOPE, 	2, 0},
	{ 	"focush", 	&head.focush, 		NULL,			DOUBLE, 	1,  CLASS_TELESCOPE, 	2, 0},
	{ 	"pt_model", 	NULL,			&head.pt_model[0],	CHARSTAR, 	8,  0, 			2, 0},

	{ 	"utdate", 	&head.utdate, 		NULL,			DOUBLE, 	1,  0, 			3, 0},
	{ 	"ut", 		&head.ut, 		NULL,			DOUBLE, 	1,  0, 			3, 0},
	{ 	"lst", 		&head.lst, 		NULL,			DOUBLE, 	1,  0, 			3, 0},
	{ 	"norchan", 	&head.norchan, 		NULL,			DOUBLE, 	1,  0, 			3, 0},
	{ 	"noswvar", 	&head.noswvar, 		NULL,			DOUBLE, 	1,  0, 			3, 0},
	{ 	"nophase", 	&head.nophase, 		NULL,			DOUBLE, 	1,  0, 			3, 0},
	{ 	"cycllen", 	&head.cycllen, 		NULL,			DOUBLE, 	1,  0, 			3, 0},
	{ 	"samprat", 	&head.samprat, 		NULL,			DOUBLE, 	1,  0, 			3, 0},
	{ 	"cl11type", 	NULL,			&head.cl11type[0],	CHARSTAR, 	8,  0, 			3, 0},

	{ 	"epoch", 	&head.epoch, 		NULL,			DOUBLE, 	1,  CLASS_SOURCE, 	4, 0},
	{ 	"xsource", 	&head.xsource, 		NULL,			DOUBLE, 	1,  CLASS_SOURCE, 	4, 0},
	{ 	"ysource", 	&head.ysource, 		NULL,			DOUBLE, 	1,  CLASS_SOURCE, 	4, 0},
	{ 	"xref", 	&head.xref, 		NULL,			DOUBLE, 	1,  CLASS_SOURCE, 	4, 0},
	{ 	"yref", 	&head.yref, 		NULL,			DOUBLE, 	1,  CLASS_SOURCE, 	4, 0},
	{ 	"epocra", 	&head.epocra, 		NULL,			DOUBLE, 	1,  CLASS_SOURCE, 	4, 0},
	{ 	"epocdec", 	&head.epocdec, 		NULL,			DOUBLE, 	1,  CLASS_SOURCE, 	4, 0},
	{ 	"gallong", 	&head.gallong, 		NULL,			DOUBLE, 	1,  CLASS_SOURCE, 	4, 0},
	{ 	"gallat", 	&head.gallat, 		NULL,			DOUBLE, 	1,  CLASS_SOURCE, 	4, 0},
	{ 	"az", 		&head.az, 		NULL,			DOUBLE, 	1,  CLASS_SOURCE, 	4, 0},
	{ 	"el", 		&head.el, 		NULL,			DOUBLE, 	1,  CLASS_SOURCE, 	4, 0},
	{ 	"indx", 	&head.indx, 		NULL,			DOUBLE, 	1,  CLASS_SOURCE, 	4, 0},
	{ 	"indy", 	&head.indy, 		NULL,			DOUBLE, 	1,  CLASS_SOURCE, 	4, 0},
	{ 	"desorg", 	&head.desorg[0], 	NULL,			DOUBLE, 	3,  CLASS_SOURCE, 	4, 0},
	{ 	"coordcd", 	NULL,			&head.coordcd[0],	CHARSTAR, 	8,  CLASS_SOURCE, 	4, 0},

	{ 	"tamb", 	&head.tamb, 		NULL,			DOUBLE, 	1,  CLASS_WEATHER, 	5, 0},
	{ 	"pressure", 	&head.pressure, 	NULL,			DOUBLE, 	1,  CLASS_WEATHER, 	5, 0},
	{ 	"humidity", 	&head.humidity, 	NULL,			DOUBLE, 	1,  CLASS_WEATHER, 	5, 0},
	{ 	"refrac", 	&head.refrac, 		NULL,			DOUBLE, 	1,  CLASS_WEATHER, 	5, 0},
	{ 	"dewpt", 	&head.dewpt, 		NULL,			DOUBLE, 	1,  CLASS_WEATHER, 	5, 0},
	{ 	"mmh2o", 	&head.mmh2o, 		NULL,			DOUBLE, 	1,  CLASS_WEATHER, 	5, 0},

	{ 	"scanang", 	&head.scanang, 		NULL,			DOUBLE, 	1,  0, 			6, 0},
	{ 	"xzero", 	&head.xzero, 		NULL,			DOUBLE, 	1,  0, 			6, 0},
	{ 	"yzero", 	&head.yzero, 		NULL,			DOUBLE, 	1,  0, 			6, 0},
	{ 	"deltaxr", 	&head.deltaxr, 		NULL,			DOUBLE, 	1,  0, 			6, 0},
	{ 	"deltayr", 	&head.deltayr, 		NULL,			DOUBLE, 	1,  0, 			6, 0},
	{ 	"nopts", 	&head.nopts, 		NULL,			DOUBLE, 	1,  0, 			6, 0},
	{ 	"noxpts", 	&head.noxpts, 		NULL,			DOUBLE, 	1,  0, 			6, 0},
	{ 	"noypts", 	&head.noypts, 		NULL,			DOUBLE, 	1,  0, 			6, 0},
	{ 	"xcell0", 	&head.xcell0, 		NULL,			DOUBLE, 	1,  0, 			6, 0},
	{ 	"ycell0", 	&head.ycell0, 		NULL,			DOUBLE, 	1,  0, 			6, 0},
	{ 	"frame", 	NULL,			&head.frame[0],		CHARSTAR, 	8,  CLASS_SOURCE, 	6, 0},

	{ 	"bfwhm", 	&head.bfwhm, 		NULL,			DOUBLE, 	1,  0, 			7, 0},
	{ 	"offscan", 	&head.offscan, 		NULL,			DOUBLE, 	1,  0, 			7, 0},
	{ 	"badchv", 	&head.badchv, 		NULL,			DOUBLE, 	1,  0, 			7, 0},
	{ 	"rvsys", 	&head.rvsys, 		NULL,			DOUBLE, 	1,  0, 			7, 0},
	{ 	"velocity", 	&head.velocity, 	NULL,			DOUBLE, 	1,  CLASS_SOURCE, 	7, 0},
	{ 	"veldef", 	NULL,			&head.veldef[0],	CHARSTAR, 	8,  CLASS_SOURCE, 	7, 0},
	{ 	"typecal", 	NULL,			&head.typecal[0],	CHARSTAR, 	8,  0, 			7, 0},

	{ 	"appeff", 	&head.appeff, 		NULL,			DOUBLE, 	1,  0, 			8, 0},
	{ 	"beameff", 	&head.beameff, 		NULL,			DOUBLE, 	1,  0, 			8, 0},
	{ 	"antgain", 	&head.antgain, 		NULL,			DOUBLE, 	1,  0, 			8, 0},
	{ 	"etal", 	&head.etal, 		NULL,			DOUBLE, 	1,  0, 			8, 0},
	{ 	"etafss", 	&head.etafss, 		NULL,			DOUBLE, 	1,  0, 			8, 0},

	{ 	"synfreq", 	&head.synfreq, 		NULL,			DOUBLE, 	1,  0, 			9, 0},
	{ 	"lofact", 	&head.lofact, 		NULL,			DOUBLE, 	1,  0, 			9, 0},
	{ 	"harmonic", 	&head.harmonic, 	NULL,			DOUBLE, 	1,  0, 			9, 0},
	{ 	"loif", 	&head.loif, 		NULL,			DOUBLE, 	1,  0, 			9, 0},
	{ 	"firstif", 	&head.firstif, 		NULL,			DOUBLE, 	1,  0, 			9, 0},
	{ 	"razoff", 	&head.razoff, 		NULL,			DOUBLE, 	1,  0, 			9, 0},
	{ 	"reloff", 	&head.reloff, 		NULL,			DOUBLE, 	1,  0, 			9, 0},
	{ 	"bmthrow", 	&head.bmthrow, 		NULL,			DOUBLE, 	1,  0, 			9, 0},
	{ 	"bmorent", 	&head.bmorent, 		NULL,			DOUBLE, 	1,  0, 			9, 0},
	{ 	"baseoff", 	&head.baseoff, 		NULL,			DOUBLE, 	1,  0, 			9, 0},
	{ 	"obstol", 	&head.obstol, 		NULL,			DOUBLE, 	1,  0, 			9, 0},
	{ 	"sideband", 	&head.sideband, 	NULL,			DOUBLE, 	1,  0, 			9, 0},
	{ 	"wl", 		&head.wl, 		NULL,			DOUBLE, 	1,  0, 			9, 0},
	{ 	"gains", 	&head.gains, 		NULL,			DOUBLE, 	1,  0, 			9, 0},
	{ 	"pbeam", 	&head.pbeam[0], 	NULL,			DOUBLE, 	2,  0, 			9, 0},
	{ 	"mbeam", 	&head.mbeam[0], 	NULL,			DOUBLE, 	2,  0, 			9, 0},
	{ 	"sroff", 	&head.sroff[0], 	NULL,			DOUBLE, 	4,  0, 			9, 0},
	{ 	"foffsig", 	&head.foffsig, 		NULL,			DOUBLE, 	1,  0, 			9, 0},
	{ 	"foffref1", 	&head.foffref1, 	NULL,			DOUBLE, 	1,  0, 			9, 0},
	{ 	"foffref2", 	&head.foffref2, 	NULL,			DOUBLE, 	1,  0, 			9, 0},

	{ 	"openpar", 	&head.openpar[0], 	NULL,			DOUBLE, 	10, 0, 		       10, 0},

	{ 	"current_disk", &head.current_disk, 	NULL,			DOUBLE, 	1,  0, 		       11, 0},
	{ 	"bologain", 	&head.bologain, 	NULL,			DOUBLE, 	1,  0, 		       11, 0},
	{ 	"sptip_start", 	&head.sptip_start, 	NULL,			DOUBLE, 	1,  0, 		       11, 0},
	{ 	"sptip_stop", 	&head.sptip_stop, 	NULL,			DOUBLE, 	1,  0, 		       11, 0},
	{ 	"ramp_up", 	&head.ramp_up, 		NULL,			DOUBLE, 	1,  0, 		       11, 0},
	{ 	"tatms", 	&head.tatms, 		NULL,			DOUBLE, 	1,  0, 		       11, 0},
	{ 	"taus", 	&head.taus, 		NULL,			DOUBLE, 	1,  0, 		       11, 0},
	{ 	"taui", 	&head.taui, 		NULL,			DOUBLE, 	1,  0, 		       11, 0},
	{ 	"tatmi", 	&head.tatmi, 		NULL,			DOUBLE, 	1,  0, 		       11, 0},
	{ 	"tchop", 	&head.tchop, 		NULL,			DOUBLE, 	1,  0, 		       11, 0},
	{ 	"tcold", 	&head.tcold, 		NULL,			DOUBLE, 	1,  0, 		       11, 0},
	{ 	"gaini", 	&head.gaini, 		NULL,			DOUBLE, 	1,  0, 		       11, 0},
	{ 	"count", 	&head.count[0], 	NULL,			DOUBLE, 	3,  0, 		       11, 0},
	{ 	"linename", 	NULL,			&head.linename[0],	CHARSTAR, 	16, 0, 		       11, 0},
	{ 	"refpt_vel", 	&head.refpt_vel, 	NULL,			DOUBLE, 	1,  0, 		       11, 0},
	{ 	"tip_humid", 	&head.tip_humid, 	NULL,			DOUBLE, 	1,  CLASS_WEATHER,     11, 0},
	{ 	"tip_ref_flag", &head.tip_ref_flag, 	NULL,			DOUBLE, 	1,  CLASS_WEATHER,     11, 0},
	{ 	"refract_45", 	&head.refract_45, 	NULL,			DOUBLE, 	1,  CLASS_WEATHER,     11, 0},
	{ 	"ref_correct", 	&head.ref_correct, 	NULL,			DOUBLE, 	1,  CLASS_WEATHER,     11, 0},
	{ 	"beam_num", 	&head.beam_num, 	NULL,			DOUBLE, 	1,  0, 		       11, 0},
	{ 	"burn_time", 	&head.burn_time, 	NULL,			DOUBLE, 	1,  0, 		       11, 0},
	{ 	"parallactic", 	&head.parallactic, 	NULL,			DOUBLE, 	1,  0, 		       11, 0},
	{ 	"az_offset", 	&head.az_offset, 	NULL,			DOUBLE, 	1,  CLASS_SOURCE,      11, 0},
	{ 	"el_offset", 	&head.el_offset, 	NULL,			DOUBLE, 	1,  CLASS_SOURCE,      11, 0},
	{ 	"yfactor", 	&head.yfactor, 		NULL,			DOUBLE, 	1,  0, 		       11, 0},
	{ 	"rejection", 	&head.rejection, 	NULL,			DOUBLE, 	1,  0, 		       11, 0},
	{ 	"nutator_temp", &head.nutator_temp, 	NULL,			DOUBLE, 	1,  CLASS_TELESCOPE,   11, 0},
	{ 	"nutate_rate", 	&head.nutate_rate, 	NULL,			DOUBLE, 	1,  CLASS_TELESCOPE,   11, 0},
	{ 	"spares05", 	&head.spares05[0], 	NULL,			DOUBLE, 	5,  0, 		       11, 0},

	{ 	"obsfreq", 	&head.obsfreq, 		NULL,			DOUBLE, 	1,  0, 		       12, 0},
	{ 	"restfreq", 	&head.restfreq, 	NULL,			DOUBLE, 	1,  0, 		       12, 0},
	{ 	"freqres", 	&head.freqres, 		NULL,			DOUBLE, 	1,  0, 		       12, 0},
	{ 	"bw", 		&head.bw, 		NULL,			DOUBLE, 	1,  0, 		       12, 0},
	{ 	"trx", 		&head.trx, 		NULL,			DOUBLE, 	1,  0, 		       12, 0},
	{ 	"tcal", 	&head.tcal, 		NULL,			DOUBLE, 	1,  0, 		       12, 0},
	{ 	"stsys", 	&head.stsys, 		NULL,			DOUBLE, 	1,  0, 		       12, 0},
	{ 	"rtsys", 	&head.rtsys, 		NULL,			DOUBLE, 	1,  0, 		       12, 0},
	{ 	"tsource", 	&head.tsource, 		NULL,			DOUBLE, 	1,  0, 		       12, 0},
	{ 	"trms", 	&head.trms, 		NULL,			DOUBLE, 	1,  0, 		       12, 0},
	{ 	"refpt", 	&head.refpt, 		NULL,			DOUBLE, 	1,  0, 		       12, 0},
	{ 	"x0", 		&head.x0, 		NULL,			DOUBLE, 	1,  0, 		       12, 0},
	{ 	"deltax", 	&head.deltax, 		NULL,			DOUBLE, 	1,  0, 		       12, 0},
	{ 	"inttime", 	&head.inttime, 		NULL,			DOUBLE, 	1,  0, 		       12, 0},
	{ 	"noint", 	&head.noint, 		NULL,			DOUBLE, 	1,  0, 		       12, 0},
	{ 	"spn", 		&head.spn, 		NULL,			DOUBLE, 	1,  0, 		       12, 0},
	{ 	"tauh2o", 	&head.tauh2o, 		NULL,			DOUBLE, 	1,  CLASS_WEATHER,     12, 0},
	{ 	"th2o", 	&head.th2o, 		NULL,			DOUBLE, 	1,  CLASS_WEATHER,     12, 0},
	{ 	"tauo2", 	&head.tauo2, 		NULL,			DOUBLE, 	1,  CLASS_WEATHER,     12, 0},
	{ 	"to2", 		&head.to2, 		NULL,			DOUBLE, 	1,  CLASS_WEATHER,     12, 0},
	{ 	"polariz", 	0, 			&head.polariz[0],	CHARSTAR, 	8,  0, 		       12, 0},
	{ 	"effint", 	&head.effint, 		NULL,			DOUBLE, 	1,  0, 		       12, 0},
	{ 	"rx_info", 	0, 			&head.rx_info[0],	CHARSTAR, 	16, CLASS_TELESCOPE,   12, 0},

	{ 	"nostac", 	&head.nostac, 		NULL,			DOUBLE, 	1,  0, 		       13, 0},
	{ 	"fscan", 	&head.fscan, 		NULL,			DOUBLE, 	1,  0, 		       13, 0},
	{ 	"lscan", 	&head.lscan, 		NULL,			DOUBLE, 	1,  0, 		       13, 0},
	{ 	"lamp", 	&head.lamp, 		NULL,			DOUBLE, 	1,  0, 		       13, 0},
	{ 	"lwid", 	&head.lwid, 		NULL,			DOUBLE, 	1,  0, 		       13, 0},
	{ 	"ili", 		&head.ili, 		NULL,			DOUBLE, 	1,  0, 		       13, 0},
	{ 	"rms", 		&head.rms, 		NULL,			DOUBLE, 	1,  0, 		       13, 0},
	{ 	"align3", 	&head.align3[0],	NULL,			DOUBLE, 	4,  0, 		       13, 0},

	{ 	  NULL, 	0, 		        NULL,			0, 	        0,  0, 			0, 0}
};

#define MAX_HEAD_PARAMS (sizeof(head_params) / sizeof(struct HEAD_PARAMS))

struct HEAD_PARAMS *findHeaderVal(name)
char *name;
{
  static struct HEAD_PARAMS *p;

  bcopy((char *)&h0->head, (char *)&head, sizeof(head));

  p = &head_params[0];
  for(;p->name;p++)
  {
    if(!strcmp(p->name, name))
    {
      return(p);
    }
  }

  return((struct HEAD_PARAMS *)NULL);
}


/* If the user changes a header variable they are 
   only changing the local copy 'head'.  Copy the
   updated 'head' into the working h0->head.
 */
int updateHeader()
{
  bcopy((char *)&head, (char *)&h0->head, sizeof(head));

  return(0);
}


int printClass(h, n)
struct HEADER *h;
int n;
{
  int i, len;
  char tbuf[512], buf[1024];
  static struct HEAD_PARAMS *p;

  st_print("\nClass %2d\n", n);

  p = &head_params[0];
  for(;p->name;p++)
  {
    if(p->nclass == n)
    {
      bzero(tbuf, sizeof(tbuf));

      if(p->type == DOUBLE)
      {
        for(i=0;i<p->size;i++)
        {
          if(!strcmp(p->name, "burn_time"))
          {
	    st_print("%12.12s:   %14.0f\n", p->name, p->dptr[i]);
          }
          else
          {
            if(i == 0)
            {
	      st_print("%12.12s:   %14.6f\n", p->name, p->dptr[i]);
            }
            else
            {
	      st_print("                %14.6f\n", p->dptr[i]);
            }
          }
        }
      }
      else
      if(p->type == CHARSTAR)
      {
        strncpy(tbuf, p->cptr, p->size);
        strcmprs(tbuf);

        sprintf(buf, "                 %s", tbuf);

        len = strlen(buf);

        st_print("%12.12s: %s\n", p->name, buf+(len - 16)); /* Pad with spaces */
      }
    }
  }

  return(0);
}


int printHeader(type)
int type;
{
  int i, len, nclass=0, foundOne=0;
  char tbuf[512], buf[1024];
  static struct HEAD_PARAMS *p;

  bcopy((char *)&h0->head, (char *)&head, sizeof(head));

  if(isSet(PF_1_FLAG))
  {
    printClass(&head, 1);
    foundOne++;
  }

  if(isSet(PF_2_FLAG))
  {
    printClass(&head, 2);
    foundOne++;
  }

  if(isSet(PF_3_FLAG))
  {
    printClass(&head, 3);
    foundOne++;
  }

  if(isSet(PF_4_FLAG))
  {
    printClass(&head, 4);
    foundOne++;
  }

  if(isSet(PF_5_FLAG))
  {
    printClass(&head, 5);
    foundOne++;
  }

  if(isSet(PF_6_FLAG))
  {
    printClass(&head, 6);
    foundOne++;
  }

  if(isSet(PF_7_FLAG))
  {
    printClass(&head, 7);
    foundOne++;
  }

  if(isSet(PF_8_FLAG))
  {
    printClass(&head, 8);
    foundOne++;
  }

  if(isSet(PF_9_FLAG))
  {
    printClass(&head, 9);
    foundOne++;
  }

  if(isSet(PF_10_FLAG))
  {
    printClass(&head, 10);
    foundOne++;
  }

  if(isSet(PF_11_FLAG))
  {
    printClass(&head, 11);
    foundOne++;
  }

  if(isSet(PF_12_FLAG))
  {
    printClass(&head, 12);
    foundOne++;
  }

  if(isSet(PF_13_FLAG))
  {
    printClass(&head, 13);
    foundOne++;
  }

  if(foundOne)
  {
    return(0);
  }

  p = &head_params[0];
  for(;p->name;p++)
  {
    if(p->nclass > nclass)
    {
      st_print("\nClass %2d\n", p->nclass);
      nclass = p->nclass;
    }

    if(type > 0 && p->class != type)
    {
      continue;
    }

    bzero(tbuf, sizeof(tbuf));

    if(p->type == DOUBLE)
    {
      for(i=0;i<p->size;i++)
      {
        if(!strcmp(p->name, "burn_time"))
        {
	  st_print("%12.12s:   %14.0f\n", p->name, p->dptr[i]);
        }
        else
        {
          if(i == 0)
          {
	    st_print("%12.12s:   %14.6f\n", p->name, p->dptr[i]);
          }
          else
          {
	    st_print("                %14.6f\n", p->dptr[i]);
          }
        }
      }
    }
    else
    if(p->type == CHARSTAR)
    {
      strncpy(tbuf, p->cptr, p->size);
      strcmprs(tbuf);

      sprintf(buf, "                 %s", tbuf);

      len = strlen(buf);

      st_print("%12.12s: %s\n", p->name, buf+(len - 16)); /* Pad with spaces */
    }
  }

  return(0);
}



int cont_header(h)
struct HEADER *h;
{
  char *site;
  char raStr[80], decStr[80], modeStr[80], oprStr[80], epochStr[80];
  char lstStr[80], utStr[80], azStr[80], elStr[80], ttimeStr[80];
  char tolStr[80], azbStr[80], elbStr[80], hpStr[80];
  double ttime;

  if(env->issmt)
  {
    site = "SMT";
  }
  else
  {
    site = "12M";
  }

  sexagesimal(h->epocra / 15.0, raStr, DDD_MM_SS_S100);
  sexagesimal(h->epocdec, decStr, HH_MM_SS);

  sexagesimal(h->lst, lstStr, HH_MM_SS);
  sexagesimal( h->ut,  utStr, HH_MM_SS);

  sexagesimal( h->az,  azStr, HH_MM_SS);
  sexagesimal( h->el,  elStr, HH_MM_SS);

  strncpy(modeStr, h->obsmode+4, 4);
  modeStr[4] = '\0';
  strncpy(oprStr, h->obsid+4, 3);
  oprStr[3] = '\0';

  ttime = h->noint * h->samprat;
  ttime /= 60.0; /* convert to hours */
  sexagesimal( ttime,  ttimeStr, HH_MM);

  sexagesimal( h->obstol   / 60.0, tolStr, HH_MM);
  sexagesimal( h->pbeam[0] / 60.0, azbStr, HH_MM);
  sexagesimal( h->pbeam[1] / 60.0, elbStr, HH_MM);
  sexagesimal( h->pbeam[1] / 60.0, hpStr,  HH_MM);

  if(!strncmp(h->coordcd, "APPRADC", 7))
  {
    strcpy(epochStr, " APP ");
  }
  else
  if(!strncmp(h->coordcd, "2000RADC", 8))
  {
    strcpy(epochStr, "2000 ");
  }
  else
  if(!strncmp(h->coordcd, "1950RADC", 8))
  {
    strcpy(epochStr, "1950 ");
  }
  else
  {
    strcpy(epochStr, "UNKNOWN");
  }

  st_print("--------------------------------------------------------------------\n");
  st_print(" Scan %6.2f         ARO %3s          %16.16s          %3.3s\n",
             h->scan, site, h->object, h->obsid);
  st_print("--------------------------------------------------------------------\n");

  st_print("YYYY-MM-DD            LST             UTC            MODE    OPR\n");
  st_print("%8.4f          %s       %s         %4.4s    %3.3s\n\n",
		h->utdate, lstStr, utStr, modeStr, oprStr);

  st_print("               RA   %s   DEC             AZ           EL\n", epochStr);
  st_print("           %s    %s     %s  %s\n\n",
 		raStr, decStr, azStr, elStr);

  st_print("  TS     TC    ATTN   ATM    T(AMB)  SAMPLES    SEC    TIME   TOL\n");
  st_print("%6.1f %6.1f %6.3f  %4.2f    %5.2f    %3d     %5.1f  %s %s\n\n", 
		h->stsys, h->tcal, h->tauh2o,
		1.0 / sin(h->el * CTR),
		h->tamb, (int)h->noint,
		h->samprat, ttimeStr, tolStr);

  st_print("          AZ      EL      HP      EFF    NT     DSF    F0\n");
  st_print("+BEAM:  %s   %s   %s      %2.0f     %d   %d   %4.1f\n",
		azbStr, elbStr, hpStr, h->appeff*100, 
		(int)h->freqres, (int)h->restfreq, h->focusr);

  sexagesimal( h->mbeam[0] / 60.0, azbStr, HH_MM);
  sexagesimal( h->mbeam[1] / 60.0, elbStr, HH_MM);

  st_print("-BEAM:  %s   %s\n", azbStr, elbStr);

  sexagesimal( h->uxpnt / 60.0, azbStr, HH_MM);
  sexagesimal( h->uypnt / 60.0, elbStr, HH_MM);

  st_print("POINT:  %s   %s\n", azbStr, elbStr);

  st_print("--------------------------------------------------------------------\n");

  return(0);
}


int line_header(h)
struct HEADER *h;
{
  char *site;
  char raStr[80], decStr[80], modeStr[80], oprStr[80], epochStr[80];
  char lstStr[80], utStr[80], ttimeStr[80];
  char tolStr[80], uazStr[80], uelStr[80],
       razoStr[80], reloStr[80],
       pbeamStr[8], mbeamStr[80], xrefStr[80], yrefStr[80];
  double ttime, tol;

  if(env->issmt)
  {
    site = "SMT";
  }
  else
  {
    site = "12M";
  }

  sexagesimal(h->epocra / 15.0, raStr,   DDD_MM_SS_S100);
  sexagesimal(h->epocdec,       decStr,  HH_MM_SS);
  sexagesimal(h->xref   / 15.0, xrefStr, DDD_MM_SS_S100);
  sexagesimal(h->yref,          yrefStr, HH_MM_SS);

  sexagesimal(h->lst, lstStr, HH_MM_SS);
  sexagesimal( h->ut,  utStr, HH_MM_SS);

  strncpy(modeStr, h->obsmode+4, 4);
  modeStr[4] = '\0';
  strncpy(oprStr, h->obsid+4, 3);
  oprStr[3] = '\0';

  ttime = h->inttime;
  ttime /= 60.0; /* convert to hours */
  sexagesimal( ttime,  ttimeStr, HH_MM);

  tol = h->obstol / 60.0;

  if(tol > 60)
  {
    strcpy(tolStr, "60:00");
  }
  else
  {
    sexagesimal( tol, tolStr, HH_MM);
  }

  if(!strncmp(h->coordcd, "APPRADC", 7))
  {
    strcpy(epochStr, " APP ");
  }
  else
  if(!strncmp(h->coordcd, "2000RADC", 8))
  {
    strcpy(epochStr, "2000 ");
  }
  else
  if(!strncmp(h->coordcd, "1950RADC", 8))
  {
    strcpy(epochStr, "1950 ");
  }
  else
  {
    strcpy(epochStr, "UNKNOWN");
  }

  sexagesimal( h->pbeam[0] / 60.0, pbeamStr, HH_MM);
  sexagesimal( h->mbeam[0] / 60.0, mbeamStr, HH_MM);
  sexagesimal( h->uxpnt    / 60.0, uazStr,   HH_MM);
  sexagesimal( h->uypnt    / 60.0, uelStr,   HH_MM);
  sexagesimal( h->razoff   / 60.0, razoStr,   HH_MM);
  sexagesimal( h->reloff   / 60.0, reloStr,   HH_MM);

  st_print("--------------------------------------------------------------------\n");
  st_print(" Scan %6.2f         ARO %3s          %16.16s          %3.3s\n",
             h->scan, site, h->object, h->obsid);
  st_print("--------------------------------------------------------------------\n");

  st_print("YYYY-MM-DD            LST             UTC            MODE    OPR\n");
  st_print("%8.4f          %s       %s         %4.4s    %3.3s\n",
		h->utdate, lstStr, utStr, modeStr, oprStr);

  st_print("                RA         DEC                    OFFSETS\n");
  st_print("                                          POINTING   +BEAM     REF\n");

  st_print(" %s      %s  %s     AZ:  %s    %s   %s\n", epochStr,   raStr,  decStr, uazStr, pbeamStr, razoStr );
  st_print(" %s REF  %s  %s     EL:  %s    %s   %s\n", epochStr, xrefStr, yrefStr, uelStr, mbeamStr, reloStr );

  st_print(" GALACTIC      %8.4f   %8.4f     RA:  00:00            00:00\n", h->gallong, h->gallat);
  st_print(" AZ/EL         %8.4f   %8.4f    DEC:  00:00            00:00\n",      h->az,     h->el);

  st_print("  TSYS     TC    RCVRS   BW/CHAN   REST FREQ   DOP FREQ    IFS    SB\n");
  st_print("  %6.1f  %5.1f    1      %4.0f    %10.3f %10.3f    %3.0f     %d\n",
		h->stsys,  h->tcal, 
		fabs(h->freqres * 1000.0), 
		h->restfreq, h->obsfreq, 
		h->loif, (int)h->sideband);

  st_print("                                  %10.8f              %4.0f\n", h->synfreq/1000.0, h->firstif);

  st_print("   TIME   NS    SEC       VEL     VLSR/VR   VEL/CHAN   TOL     F0\n");
  st_print("  %s   %2.0f   %4.0f     %5.1f    %7.3f    %7.3f  %6.6s  %4.1f\n",
		ttimeStr, 
		h->inttime / h->samprat, 
		h->samprat, 
		h->velocity, h->rvsys, h->deltax, 
		tolStr, h->focusr);

  st_print("--------------------------------------------------------------------\n");

  return(0);
}


int showHeader()
{
  /* Always copy the working header into the static head */
  bcopy((char *)&h0->head, (char *)&head, sizeof(head));

  if(env->mode == MODE_CONT)
  {
    cont_header(&head);
  }
  else
  if(env->mode == MODE_SPEC)
  {
    line_header(&head);
  }

  return(0);
}


int cleanSource(tbuf)
char *tbuf;
{
  char *cp;

  for(cp=tbuf;*cp;cp++)
  {
    if(*cp == '(' || *cp == ')' || *cp == ' ')
    {
      *cp = '-';
    }
  }

  return(0);
}



int printStatus()
{
  int offset, i, len;
  char *site, path[512], fname[1024], source[256], version[80];
  char issmt[256], initials[80];
  char *ext[] = { "txt", "ps" };
  char *string, line[256], *cp, outStr[256];
  FILE *fp;

  if(head.scan == 0.0)
  {
    st_fail("No scan loaded");
    return(1);
  }

  if(strlen(env->filename) < 4)
  {
    st_fail("Filename Not Set");
    return(1);
  }

  offset = strlen(env->filename) - 7;
  strncpy(initials, env->filename+offset, 3);
  initials[3] = '\0';

  offset = strlen(env->filename) - 3;
  strncpy(version, env->filename+offset, 3);
  version[3] = '\0';

  if(env->issmt)
  {
    strcpy(issmt, "smt");
  }
  else
  {
    strcpy(issmt, "12m");
  }

  site = getenv("SITE");
  if(!site)
  {
    st_fail("SITE environmental not set");
    return(1);
  }

  if(!strcmp(site, "TUCSON"))
  {
    sprintf(path, "/home/archive/2013-2014/%s/obs/%s/status", issmt, initials);
  }
  else
  if(!strcmp(site, "KITT_PEAK"))
  {
    sprintf(path, "/home/obs/%s/status", initials);
  }
  else
  if(!strcmp(site, "MT_GRAHAM"))
  {
    sprintf(path, "/home/obs/%s/status", initials);
  }
  else
  {
    st_fail("Unknown SITE: %s", site);
    return(1);
  }

  /* Now the status file could be a postscript or text file */
  /* Try the PS file first */

  for(i=0;i<2;i++)
  {
    /* This marks the epoch I changed the filename for status displays */
    if(head.burn_time > 1461699790.0)
    {
      strncpy(source, head.object, 16);
      source[16] = '\0';
      cleanSource(source);
      strcmprs(source);

      sprintf(fname, "%s/%s.%s.%05d.txt", path, source, version, (int)head.scan);
    }
    else
    {
      sprintf(fname, "%s/%04d.%s", path, (int)head.scan, ext[i]);
    }

    st_print("Looking for: %s\n", fname);

    if((fp = fopen(fname, "r") ) <= (FILE *)0)
    {
      continue;
    }

    st_print("\n");
    while((string = fgets(line, sizeof(line), fp)) != NULL)
    {
      if(i == 0) /* If this is the TXT version, just print it out */
      {
        st_print(" %s", line);
      }
      else
      {
        if(!strncmp(line, "540 ", 4))
        {
          cp = strstr(line, "(");
          cp++;

          len = strlen(cp);
          strncpy(outStr, cp, len-4);
          st_print(" %s\n", outStr);
        }
      }
    }

    fclose(fp);

    st_print("\n");

    /* if we got here means we found one so return */
    return(0);
  }

  sprintf(fname, "%s/%04.0f.*", path, head.scan);
  st_fail("Unable to find any file: %s", fname);

  return(0);
}
