/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

struct BANDS {
        char *filename;
	char *lfilename;
        double min;
        double max;
};

struct BANDS bands[] = {
        { "splat-band2.csv", "lovas-band2.dat", ALMA_BAND_2_LOWER, ALMA_BAND_2_UPPER },
        { "splat-band3.csv", "lovas-band3.dat", ALMA_BAND_3_LOWER, ALMA_BAND_3_UPPER },
        { "splat-band4.csv", "lovas-band4.dat", ALMA_BAND_4_LOWER, ALMA_BAND_4_UPPER },
        { "splat-band5.csv", "lovas-band5.dat", ALMA_BAND_5_LOWER, ALMA_BAND_5_UPPER },
        { "splat-band6.csv", "lovas-band6.dat", ALMA_BAND_6_LOWER, ALMA_BAND_6_UPPER },
        { "splat-band7.csv", "lovas-band7.dat", ALMA_BAND_7_LOWER, ALMA_BAND_7_UPPER },
        { "splat-band8.csv", "lovas-band8.dat", ALMA_BAND_8_LOWER, ALMA_BAND_8_UPPER },
        { "splat-band9.csv", "lovas-band0.dat", ALMA_BAND_9_LOWER, ALMA_BAND_9_UPPER }
};

#define MAX_BANDS 8

char tok[80][80];

int findBand(f)
double f;
{
  int i;
  struct BANDS *p;

  p = &bands[0];
  for(i=0;i<MAX_BANDS;i++,p++)
  {
    if(f >= p->min && f <= p->max)
    {
      return(i);
    }
  }

  return(-1);
}


int doSplat()
{
  int i, b, n;
  double f, d;
  char sysbuf[512], cmd[512], tbuf[512];

  if(isSet(PF_S_FLAG)) /* Search by name */
  {
    if((b = findBand(env->freq)) >= 0)
    {
      n = args->ntokens-1;

      cmd[0] = '\0';

      for(i=0;i<n;i++)
      {
        if(i == 0)
        {
	  if(isSet(PF_L_FLAG)) /* Search Lovas Line Catalog ? */
          {
            sprintf(tbuf, "grep -i %s %s/share/%s", args->tok[i+1], pkg_home, bands[b].lfilename);
	  }
	  else
	  {
            sprintf(tbuf, "grep -i %s %s/share/%s", args->tok[i+1], pkg_home, bands[b].filename);
	  }
        }
        else
        {
          sprintf(tbuf, " | grep -i %s ", args->tok[i+1]);
        }

        strcat(cmd, tbuf);
      }

      strcmprs(cmd);

      st_report("%s", cmd);

      system(cmd);
    }
    else
    {
      st_fail("Freq: %f, out of range of any ALMA bands", env->freq);
    }

    return(0);
  }

  if(strlen(args->secondArg) && strlen(args->thirdArg))
  {
    f = atof(args->secondArg);
    d = atof(args->thirdArg);
  }
  else
  {
    f = h0->head.restfreq / 1000.0 + (env->gaussCenter / 1000.0);
    d = env->gaussHP / 2000.0;
  }

  if(isSet(PF_L_FLAG))
  {
    sprintf(sysbuf, "lovas %f %f", f, d * 2.0); /* Double the search range */
  }
  else
  {
    sprintf(sysbuf, "splat %f %f", f, d * 2.0); /* Double the search range */
  }

  system(sysbuf);

  return(0);
}


int molecule()
{
  double f, d;
  char sysbuf[256];

  _doFCur("Click on Line of Interest");

  f = h0->head.restfreq / 1000.0 + env->fcur / 1000.0;
  d = 4.0 / 1000.0; /* +/- 4 MHz */

  if(isSet(PF_L_FLAG))
  {
    sprintf(sysbuf, "lovas %f %f", f, d);
  }
  else
  {
    sprintf(sysbuf, "splat %f %f", f, d);
  }

  system(sysbuf);


  return(0);
}
