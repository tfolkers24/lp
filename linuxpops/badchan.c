/* 
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of
the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and
research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1. Additional Required Provisions

1.1.  Arbitration. The parties agree that if a dispute arises between them
concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2.  Applicable Law and Venue. This Agreement shall be interpreted pursuant
to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby
submits to venue and jurisdiction in Pima County, ARIZONA.

1.3.  Non-Discrimination. The Parties agree to be bound by state and federal
laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4.   Appropriation of Funds. The Parties recognize that performance by
ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if
ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify
LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5.   Conflict of Interest. This Agreement is subject to the provisions of
A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person
significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or
consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

 */


#include "extern.h"

#define ZERO_CHANNEL -1

/** OK, delema;  The badchannel array consists of an
    array of ints.  There are normally zero, but a bad
    channel number is indicated in the array by it's
    channel number.

    How do you flag channel 0???

    So, without a complete hack job, I decided that any
    element in the array with ZERO_CHANNEL (-1) in it 
    indicated Chan 0.

    OK, now it's a hack job;

    New plan; set the array to -1 when there is nothing
    bad;  That way 0 is a legit number.

 */

int addBadChan(bad)
int bad;
{
  int i;

  /* Already set ? */
  if(isBadChan(bad))
  {
    st_print("Channel %d, already set\n", bad);
    return(0);
  }

  /* Find a slot to put it in */
  for(i=0;i<MAX_BAD_CHANS;i++)
  {
    if(env->badchans[i] == -1)
    {
      st_report("Added %d to BadChan Array", bad);

      env->badchans[i] = bad;

      return(0);
    }
  }

  st_fail("Bad Chan Array Full");

  return(1);
}


/** Remove passed channel from array */
int removeBadChanFromArray(bad)
int bad;
{
  int i;

  for(i=0;i<MAX_BAD_CHANS;i++)
  {
    if(env->badchans[i] == bad)
    {
      st_report("Removing %d from Bad Chan Array", bad);
      env->badchans[i] = -1;

      return(0);
    }
  }

  st_fail("Bad Chan %d not found in Bad Chan Array()", bad);

  return(1);
}


int autoFind(flag)
int flag;
{
  int i;
  float *f;
  double val;

  if(args->ntokens < 2)
  {
    st_fail("autoBad(): Missing args");
    return(1);
  }

  if(strlen(args->secondArg))
  {
    val = atof(args->secondArg);
  }
  else
  {
    return(1);
  }

  f = &h0->yplot1[0];
  if(flag > 0)
  {
    for(i=0;i<h0->head.noint;i++,f++)
    {
      if(*f > val)
      {
        addBadChan(i);
      }
    }
  }
  else
  if(flag < 0)
  {
    for(i=0;i<h0->head.noint;i++,f++)
    {
      if(*f < val)
      {
        addBadChan(i);
      }
    }
  }
  else
  {
    for(i=0;i<h0->head.noint;i++,f++)
    {
      if(fabs(*f - val) < 0.001)
      {
        addBadChan(i);
      }
    }
  }

  showScan();

  return(0);
}


int setBadInterval()
{
  int i, n, s, m;

  if(args->ntokens < 3)
  {
    printH_FlagHelp("badchan");
    
    return(1);
  }

  s = atoi(args->secondArg);
  m = atoi(args->thirdArg);

  n = (int)h0->head.noint;

  if(n == 0)
  {
    st_fail("Looks like no scan has been loaded");
    return(1);
  }

  if(s < 0 || s > n)
  {
    st_fail("Starting channel, %d,  must be >= 0 & < %d", s, n);

    return(1);
  }

  if(m < 0 || m > n)
  {
    st_fail("Modulo number, %d,  must be >= 0 & < %d", m, n);

    return(1);
  }

  i = s;
  while(i<n)
  {
    addBadChan(i);

    i += m;
  }

  return(0);
}


/** Set one or more bad channels by name or mouse 
 */
int ignoreBadChan()
{
  int i, n, bad;
  char hint[256];

  /* Zero out the current ones if requested */
  if(isSet(PF_C_FLAG))
  {
    clearBadChan();

    return(0);
  }

  if(isSet(PF_I_FLAG))
  {
    return(setBadInterval());
  }

  if(isSet(PF_A_FLAG))
  {
    if(isSet(PF_E_FLAG))
    {
      return(autoFind(0));
    }
    else
    if(isSet(PF_L_FLAG))
    {
      return(autoFind(-1));
    }
    else
    if(isSet(PF_G_FLAG))
    {
      return(autoFind(1));
    }

    return(0);
  }

  if(strlen(args->secondArg))
  {
    bad = atoi(args->secondArg);

    if(bad >= 0)
    {
      addBadChan(bad);

      return(0);
    }
    else
    if(bad < 0)
    {
      removeBadChanFromArray(bad * -1);

      return(0);
    }
    else
    {
      st_fail("Bad Chan must be >= 0");

      return(1);
    }
  }

  do
  {
    n = getNumericResponse("Enter Number of Bad Channels [ Up to 9]: ");
  }
  while(n < 0);

  if( n == 0)
  {
    return(0);
  }

  for(i=0;i<n;i++)
  {
    sprintf(hint, "Click Left Mouse Button to pick Bad Channel %d", i+1);

    _doCCur(hint);

    bad = (int)round(env->ccur);

    if(bad >= 0)
    {
      addBadChan(bad);
    }
    else
    {
      st_fail("Can't Add/Remove BadChan %d", bad);
    }
  }

  /* Show the user which channels are flagged */
  tellBadChan();

  /* Tell the system to remove bad channels when encountered */
  env->dobadchan = 1;

  /* Remove them now */
  line_removeBadChans(h0, 1);

  return(0);
}


/** Show the user which channels are flagged 
 */
int tellBadChan()
{
  int i;

  st_print("%s # BadChan: \t", prompt);

  for(i=0;i<MAX_BAD_CHANS;i++)
  {
    if(env->badchans[i] >= 0)
    {
      st_print("%4d ", env->badchans[i]);

      if(i > 0 && !(i%8))
      {
        st_print("\n\t\t\t\t");
      }
    }
  }

  st_print("\n");

  return(0);
}


/* Is candidate channel, 'bad', flagged as bad */
int isBadChan(bad)
int bad;
{
  int i;

  for(i=0;i<MAX_BAD_CHANS;i++)
  {
    if(env->badchans[i] == bad)
    {
      return(1);
    }
  }

  return(0);
}


/** Zero out the bad channel array 
 */
int clearBadChan()
{
  int i;

  st_report("Clearing out all Bad Chans");

  for(i=0;i<MAX_BAD_CHANS;i++)
  {
    env->badchans[i] = -1;
  }

  return(0);
}


int readBadChanFile()
{
  FILE *fp;
  char *string, line[256], fname[256];
  int bad;

  if(strlen(args->secondArg) < 2)
  {
    st_warn("Missing arg for readIgnoreFile()");
    return(1);
  }

  strcpy(fname, args->secondArg);

  if((fp = fopen(fname, "r") ) <= (FILE *)0)
  {
    fprintf(stderr, "Unable to open file %s\n", fname);
    return(0);
  }

  while((string = fgets(line, sizeof(line), fp)) != NULL)
  {
    if(line[0] == '#')
    {
      continue;
    }

    deNewLineIt(line); /* Clean it up */

    st_report("Parsing: %s", line);

    bad = atoi(line);

    if(bad >= 0)
    {
      addBadChan(bad);
    }
    else
    if(bad < 0)
    {
      removeBadChanFromArray(bad * -1);
    }
    else
    {
      st_fail("Can't Add/Remove BadChan %d", bad);
    }
  }

  tellBadChan();

  fclose(fp);

  return(0);
}


int badChanShow()
{
  env->badchanshow = 1;

  xx();

  env->badchanshow = 0;

  return(0);
}


/* Return the data value of the next available good channel */
float findNextGoodChan(image, j)
struct LP_IMAGE_INFO *image;
int j;
{
  int i;
  float f = 0.0;

  for(i=j;i<image->head.noint;i++)
  {
    if(!isBadChan(i))
    {
      return(image->yplot1[i]);
    }
  }

  return(f);
}


/* Return the data value of the previous available good channel */
float findPreGoodChan(image, j)
struct LP_IMAGE_INFO *image;
int j;
{
  int i;
  float f = 0.0;

  for(i=j;i>0;i--)
  {
    if(!isBadChan(i))
    {
      return(image->yplot1[i]);
    }
  }

  return(f);
}


/* First remove any bad channels marked & copy to local yplot array 
 */
int line_removeBadChans(image, print)
struct LP_IMAGE_INFO *image;
int print;
{
  int i, first=1, j=0;

  for(i=0;i<image->head.noint;i++)
  {
    if(env->dobadchan && isBadChan(i))
    {
      if(i > 0 && i < image->head.noint-1)
      {
        if(print && first)
        {
          st_print("%s # Smoothing Bad Chan:  ", prompt);
          first = 0;
        }

	if(print)
        {
          st_print("%4d ", i);
        }

	j++;

	if(print && !(j%8))
        {
	  st_print("\n\t\t\t\t\t ");
        }

        image->yplot1[i] = (findPreGoodChan(image, i) + findNextGoodChan(image, i)) / 2.0;
      }
      else
      if(i == 0) /* Nothing to average; set to next good value */
      {
        if(print && first)
        {
          st_print("%s # Smoothing Bad Chan:  ", prompt);
          first = 0;
        }

	if(print)
        {
          st_print("%4d ", i);
	}

	j++;

	if(print && !(j%8))
        {
	  st_print("\n\t\t\t\t\t ");
        }

        image->yplot1[i] = findNextGoodChan(image, i);
      }
      else
      if(i == image->head.noint-1) /* Nothing to average; set to previous good value */
      {
        if(print && first)
        {
          st_print("%s # Smoothing Bad Chan:  ", prompt);
          first = 0;
        }

	if(print)
        {
          st_print("%4d ", i);
	}

	j++;

	if(print && !(j%8))
        {
	  st_print("\n\t\t\t\t\t ");
        }

        image->yplot1[i] = findPreGoodChan(image, i);
      }
      else /* No ajacent channels to smooth to, so forget it */
      {
        image->yplot1[i] = image->yplot1[i];
      }
    }
    else
    {
      image->yplot1[i] = image->yplot1[i];
    }
  }

  /* Found at least one; finish the print staement */
  if(print && !first)
  {
    st_print("\n");
  }

  return(0);
}



int doSpike()
{
  int i, j, guesses[MAX_CHANS], remove=0, cindx;
  float cval, aval, ratio, max_ratio=0.0, climit=200.0, limit;
  struct LP_IMAGE_INFO *image = h0;

  if(isSet(PF_R_FLAG))
  {
    remove = 1;
  }

  if(args->ntokens > 1)
  {
    limit = atof(args->secondArg);

    if(limit > 0.0 && limit < 1000.0)
    {
      climit = limit;
    }
  }

  bzero((char *)&guesses, sizeof(guesses));
  j=0;

  for(i=0;i<image->head.noint;i++)
  {
    if(i > 2 && i < image->head.noint-2)
    {
      cval = (image->yplot1[i]);
      aval = ((image->yplot1[i-2] + image->yplot1[i-1] + image->yplot1[i-1] + image->yplot1[i+2]) / 4.0);

      if(fabs(aval) > 0.001)
      {
        ratio = fabs(cval) / fabs(aval);

        if(ratio > max_ratio)
        {
	  max_ratio = ratio;
	  cindx     = i;
	}

        if(ratio > climit)
        {
          st_print("C[%4d] = %9.6f, Avg = %9.6f, ratio = %6.1f\n", i, cval, aval, ratio);
          guesses[j] = i;
          j++;
        }
      }
    }
  }

  if(j)
  {
    st_print("%d Possible Bad Channels: ", j);

    for(i=0;i<j;i++)
    {
      st_print("%d ", guesses[i]);
    }

    st_print("\n");

    if(remove)
    {
      for(i=0;i<j;i++)
      {
        subArgs(2, "%d", guesses[i]);
        ignoreBadChan();
      }
    }
  }

  st_report("Max Ratio: %f @ Channel: %d", max_ratio, cindx);

  showScan();

  return(0);
}


/** Routine to fix the bad channel array from using
    0 = empty to -1 = empty element.

    That change allows for channel 0 to be flagged.
 */
int correctBadChans()
{
  int e, i;

  for(e=0;e<MAX_ENV;e++)
  {
    if(CGM->envrs[e].badchanepoch < 1482161359)
    {
      st_report("Upgrading Bad Channel Array[%d]", e);

      for(i=0;i<MAX_BAD_CHANS;i++)
      {
        if(CGM->envrs[e].badchans[i] == 0)
        {
          CGM->envrs[e].badchans[i] = -1;
        }
      }

      CGM->envrs[e].badchanepoch = time(NULL);
    }
  }

  return(0);
}


int do_showBad()
{
  int i, half;

  if(args->ntokens > 1)
  {
    half = atoi(args->secondArg);
  }
  else
  {
    printH_FlagHelp("showbad");

    return(0);
  }

  st_print("%s # BadChan for Rambo: \t", prompt);

  for(i=0;i<MAX_BAD_CHANS;i++)
  {
    if(env->badchans[i] >= 0)
    {
      st_print("%4d ", half + env->badchans[i] + 1);

      if(i > 0 && !(i%8))
      {
        st_print("\n\t\t\t\t");
      }
    }
  }

  st_print("\n");
  
  return(0);
}


int writeBadChans()
{
  int i;
  FILE *fp;
  char fname[256];

  if(strlen(args->secondArg) < 2)
  {
    st_warn("Missing arg for writeBadChans()");
    return(1);
  }

  strcpy(fname, args->secondArg);

  if((fp = fopen(fname, "w") ) <= (FILE *)0)
  {
    fprintf(stderr, "Unable to open file %s\n", fname);
    return(0);
  }

  fprintf(fp, "badch /c /i\n");

  for(i=0;i<MAX_BAD_CHANS;i++)
  {
    if(env->badchans[i] >= 0)
    {
      fprintf(fp, "badch %4d /i\n", env->badchans[i]);
    }
  }

  fclose(fp);

  return(0);
}
