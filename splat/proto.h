/* splat.c */
int findBand(double f);
int get_tok(char *edit);
int loadBand(char *name);
int printHeader(void);
int setColor(int c);
int printFreq(struct SPLAT_ENTRY *p, double f, double delta, int hl);
int findFreq(double f, double d);
int main(int argc, char *argv[]);
