#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <math.h>

#include "alma.h"
#include "splat.h"
#include "ansi_colors.h"

#include "caclib_proto.h"

struct SPLAT_CATALOG sc;

struct BANDS {
	char *filename;
	int pad;
	double min;
	double max;
};

struct BANDS bands[] = {
        { "splat-band2.csv", 0, ALMA_BAND_2_LOWER, ALMA_BAND_2_UPPER },
        { "splat-band3.csv", 0, ALMA_BAND_3_LOWER, ALMA_BAND_3_UPPER },
        { "splat-band4.csv", 0, ALMA_BAND_4_LOWER, ALMA_BAND_4_UPPER },
        { "splat-band5.csv", 0, ALMA_BAND_5_LOWER, ALMA_BAND_5_UPPER },
        { "splat-band6.csv", 0, ALMA_BAND_6_LOWER, ALMA_BAND_6_UPPER },
        { "splat-band7.csv", 0, ALMA_BAND_7_LOWER, ALMA_BAND_7_UPPER },
        { "splat-band8.csv", 0, ALMA_BAND_8_LOWER, ALMA_BAND_8_UPPER },
        { "splat-band9.csv", 0, ALMA_BAND_9_LOWER, ALMA_BAND_9_UPPER }
};

#define MAX_BANDS 8

char tok[80][80];

int findBand(f)
double f;
{
  int i;
  struct BANDS *p;

  p = &bands[0];
  for(i=0;i<MAX_BANDS;i++,p++)
  {
    if(f >= p->min && f <= p->max)
    {
      return(i);
    }
  }

  return(-1);
}



int get_tok(edit)
char *edit;
{
  int c = 0;
  char *token;

  bzero((char *)&tok, sizeof(tok));

  token = strtok(edit, ":");
  while(token != (char *)NULL)
  {
    sprintf(tok[c++], "%s", token);
    token = strtok((char *)NULL, ":");
  }

  return(c);
}




int loadBand(name)
char *name;
{
  int n, j = 0, first = 1;
  char line[512], *string, *cp, fname[256];
  FILE *fp;
  struct SPLAT_ENTRY *p;

  cp = getenv("LINUXPOPS_HOME");

  if(!cp)
  {
    printf("SPLAT(): Unable to load %s\n", name);
  }

  sprintf(fname, "%s/share/%s", cp, name);

  if((fp = fopen(fname, "r") ) <= (FILE *)0)
  {
    printf("SPLAT(): Unable to open file %s\n", fname);
    return(1);
  }

  printf("SPLAT(): Loading file: %s\n", fname);

  bzero((char *)&sc, sizeof(sc));

  strcpy(sc.catalog, name);

  p = &sc.entries[0];
  while((string = fgets(line, sizeof(line), fp)) != NULL)
  {
    if(line[0] == '#')
    {
      continue;
    }

    n = get_tok(line);

    if(n >= 8)
    {
      strcpy(p->species,  tok[0]);
      strcpy(p->name,     tok[1]);
      p->freq      = atof(tok[2]);

      if(first)
      {
        sc.freqmin = p->freq;
        first = 0;
      }

      sc.freqmax = p->freq;

      strcpy(p->quantum,  tok[3]);
      p->intensity = atof(tok[4]);
	/* Skip Lovis Line */
      p->Elcm      = atof(tok[6]);
      p->ElK       = atof(tok[7]);

      p++;
      j++;

      if(j >= MAX_SPLAT_ENTRIES)
      {
        printf("SPLAT(): Maximum number Entries Loaded\n");
        sc.num = j;
        fclose(fp);

        return(0);
      }
    }
//    else
//    {
//      printf("SPLAT(): Wrong number of tokens %d, in loadBand(), require > 8\n", n);
//    }
  }

  fclose(fp);

  printf("SPLAT(): # Loaded %d ALMA Lines\n", j);

  sc.num = j;

  if(j > 1000)
  {
    return(0);
  }
  else
  {
    return(1);
  }
}


int printHeader()
{
  printf("   Freq       Delta           Species                 Name");
  printf("                 Quantum Numbers         Intensity    Elcm-1       ElK\n\n");

  return(0);
}


int setColor(c)
int c;
{
  static int oldColor=-1;

  if(c == oldColor)
  {
    return(0);
  }

  switch(c)
  {
    case ANSI_BLACK:   printf("%c[0m",     0x1b); break;
    case ANSI_RED:     printf("%c[31m",    0x1b); break;
    case ANSI_GREEN:   printf("%c[32;1m",  0x1b); break;
    case ANSI_YELLOW:  printf("%c[33;1m",  0x1b); break;
    case ANSI_BLUE:    printf("%c[34;1m",  0x1b); break;
    case ANSI_MAGENTA: printf("%c[35;1m",  0x1b); break;
    case ANSI_CYAN:    printf("%c[36;1m",  0x1b); break;
    case ANSI_WHITE:   printf("%c[37;40m", 0x1b); break;
    case ANSI_RESERVE: printf("%c[0m",     0x1b); break;
    case ANSI_RESET:   printf("%c[39m",    0x1b); break;
  }

  oldColor = c;

  return(0);
}


int printFreq(p, f, delta, hl)
struct SPLAT_ENTRY *p;
double f, delta;
int hl;
{
  if(hl)
  {
    setColor(ANSI_RED);
  }
  else
  {
    setColor(ANSI_BLACK);
  }

  printf("%10.6f (%9.6f) %20.20s %20.20s %32.32s %10.4f %10.4f %10.4f\n", 
		p->freq, delta, p->species, p->name, p->quantum,
		p->intensity, p->Elcm, p->ElK);

  return(0);
}


int findFreq(f, d)
double f, d;
{
  int i, n, first=1, dindx = -1;
  double flo, fhi;
  double delta, dmin = 9999999999.9;
  struct SPLAT_ENTRY *p;

  flo = f - d;
  fhi = f + d;
  n   = sc.num;

  /* First find the closest entry */
  p = &sc.entries[0];
  for(i=0;i<n;i++,p++)
  {
    if(p->freq > flo && p->freq < fhi)
    {
      delta = p->freq - f;

      if(fabs(delta) < dmin)
      {
        dmin = fabs(delta);
        dindx = i;
      }
    }
  }

  p = &sc.entries[0];
  for(i=0;i<n;i++,p++)
  {
    if(p->freq > flo && p->freq < fhi)
    {
      if(first)
      {
        printHeader();
        first = 0;
      }

      delta = p->freq - f;

      if(i == dindx)
      {
        printFreq(p, f, delta, 1);
      }
      else
      {
        printFreq(p, f, delta, 0);
      }
    }
  }

  return(0);
}


int main(argc, argv)
int argc;
char *argv[];
{
  int ret;
  double freq, delta;

  if(argc < 3)
  {
    printf("Usage: %s freq delta-freq\n", argv[0]);
    exit(1);
  }

  freq  = atof(argv[1]);
  delta = atof(argv[2]);

  log_msg("Got Freq = %f, delta = %f", freq, delta);
  
  if((ret = findBand(freq)) >= 0)
  {
    ret = loadBand(bands[ret].filename);
    if(!ret)
    {
      printf("SPLAT(): Searching bands for freq: %f +/- %f\n", freq, delta);

      findFreq(freq, delta);
    }
    else
    {
      printf("SPLAT(): Failed to load enough lines\n");
    }
  }
  else
  {
    printf("SPLAT(): No catalog covering freq = %f\n", freq);
    printf("SPLAT(): %f Min to %f Max\n", ALMA_BAND_2_LOWER, ALMA_BAND_9_UPPER);
  }

  setColor(ANSI_BLACK);

  return(0);
}
