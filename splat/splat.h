#ifndef SPLAT_H
#define SPLAT_H

#define MAX_SPLAT_ENTRIES  256000

struct SPLAT_ENTRY {
	char   species[80];
	char   name[80];
	char   quantum[80];
	float  freq;
	float  intensity;
	float  Elcm;
	float  ElK;
};

struct SPLAT_CATALOG {
	char   catalog[16];	/* Splatalogue catalog */
	int    band;		/* ALMA Band number */
	int    num;		/* Number of entries loaded */
	double freqmin;		/* Search min freq */
	double freqmax;		/* Search max freq */

	struct SPLAT_ENTRY entries[MAX_SPLAT_ENTRIES];
};

#endif
