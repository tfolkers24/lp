#include "slalib.h"
#include "slamac.h"
void slaCd2tf ( int ndp, float days, int *sign, int ihmsf[4] )
/*
**  - - - - - - - - -
**   s l a C d 2 t f
**  - - - - - - - - -
**
**  Convert an interval in days into hours, minutes, seconds.
**
**  (single precision)
**
**  Given:
**     ndp       int      number of decimal places of seconds
**     days      float    interval in days
**
**  Returned:
**     sign      int*     '+' or '-'
**     ihmsf     int[4]   hours, minutes, seconds, fraction
**
**  P.T.Wallace   Starlink   31 October 1993
*/
{
/* Days to seconds */
   static float d2s = 86400.0f;
 
   float rs, rm, rh, a, ah, am, as, af;
 
/* Handle sign */
   *sign = (days < 0.0f) ? '-' : '+';
 
/* Field units in terms of least significant figure */
   rs = (float) pow ( 10.0, ( ndp > 0 ) ? ndp : 0 );
   rm = rs * 60.0f;
   rh = rm * 60.0f;
 
/* Round interval and express in smallest units required */
   a = anint( rs * d2s * (float) fabs ( (double) days ));
 
/* Separate into fields */
   ah = aint ( a / rh );
   a  = a - ah * rh;
   am = aint ( a / rm );
   a  = a - am * rm;
   as = aint ( a / rs );
   af = a - as * rs;
 
/* Return results */
   ihmsf[0] = (int) ah;
   ihmsf[1] = (int) am;
   ihmsf[2] = (int) as;
   ihmsf[3] = (int) af;
}
