@echo off
rem
rem  - - - - - -
rem   P C . B A T
rem  - - - - - -
rem
rem  Create the SLALIB library, starting either from the release
rem  version copied from the VAX or from the PC version produced
rem  by a prior run of the present procedure.
rem
rem  Environment variable temp must point to a scratch directory.
rem
rem  !!! Non-reversible - it is not possible to recreate !!!
rem  !!! the VAX version from the PC version             !!!
rem
rem  P.T.Wallace   Starlink   17 July 1993
rem
if exist *.pcm copy *.pcm *.for
if exist *.bak del *.bak
if exist *.pcm del *.pcm
if exist *.obj del *.obj
if exist *.exe del *.exe
if exist *.olb del *.olb
if exist *.vax del *.vax
if exist *.ind del *.ind
if exist *.com del *.com
if exist *.new del *.new
if exist *.obs del *.obs
if exist *.mdt del *.mdt
if exist test.c del test.c
if exist makefile del makefile
if exist mk del mk
if exist slaLink del slaLink
if exist a*.c qcl/c a*.c
if exist b*.c qcl/c b*.c
if exist c*.c qcl/c c*.c
if exist d*.c qcl/c d*.c
if exist e*.c qcl/c e*.c
if exist f*.c qcl/c f*.c
if exist g*.c qcl/c g*.c
if exist h*.c qcl/c h*.c
if exist i*.c qcl/c i*.c
if exist j*.c qcl/c j*.c
if exist k*.c qcl/c k*.c
if exist l*.c qcl/c l*.c
if exist m*.c qcl/c m*.c
if exist n*.c qcl/c n*.c
if exist o*.c qcl/c o*.c
if exist p*.c qcl/c p*.c
if exist q*.c qcl/c q*.c
if exist r*.c qcl/c r*.c
if exist s*.c qcl/c s*.c
if exist t*.c qcl/c t*.c
if exist u*.c qcl/c u*.c
if exist v*.c qcl/c v*.c
if exist w*.c qcl/c w*.c
if exist x*.c qcl/c x*.c
if exist y*.c qcl/c y*.c
if exist z*.c qcl/c z*.c
echo Building library SLALIB.LIB ...
@echo slalib > %temp%\slalib.tmp
@echo y >> %temp%\slalib.tmp
for %%f in (*.obj) do echo +%%f& >> %temp%\slalib.tmp
@echo ;. >> %temp%\slalib.tmp
if exist slalib.lib del slalib.lib
lib @%temp%\slalib.tmp
del %temp%\slalib.tmp
del *.obj
echo ... done.
echo:
