#include "slalib.h"
#include "slamac.h"
float slaRverot ( float phi, float ra, float da, float st )
/*
**  - - - - - - - - - -
**   s l a R v e r o t
**  - - - - - - - - - -
**
**  Velocity component in a given direction due to Earth rotation.
**
**  (single precision)
**
**  Given:
**     phi     float    latitude of observing station (geodetic)
**     ra,da   float    apparent RA,Dec
**     st      float    local apparent sidereal time
**
**     phi, ra, dec and st are all in radians.
**
**  Result:
**     Component of Earth rotation in direction ra,da (km/s)
**
**  Sign convention:
**     The result is +ve when the observer is receding from the
**     given point on the sky.
**
**  Accuracy:
**     The simple algorithm used assumes a spherical Earth and
**     an observing station at sea level.  For actual observing
**     sites, the error is unlikely to be greater than 0.0005 km/s.
**     for applications requiring greater precision, see the
**     routine slaPvobs.
**
**  P.T.Wallace   Starlink   31 October 1993
*/
 
/*
**  Sidereal speed of Earth equator, adjusted to compensate for
**  the simple algorithm used.  (The true value is 0.4651.)
*/
#define ESPEED 0.4655
 
{
  return (float) ESPEED * cos ( (double) phi ) *
                          sin ( (double) ( st - ra ) ) *
                          cos ( (double) da );
}
