#include "slalib.h"
#include "slamac.h"
#include <stdlib.h>
#include <limits.h>

#ifdef RAND_MAX
#undef RAND_MAX
#endif

#define RAND_MAX  2147483647 /* 2**31-1 added by jrh 12sept95 */

float slaRandom ( float seed )
/*
**  - - - - - - - - - -
**   s l a R a n d o m
**  - - - - - - - - - -
**
**  Generate pseudo-random real number in the range 0 <= x < 1.
**
**  (single precision)
**
**  Given:
**     seed     float     initializes random number generator: used
**                        first time through only.
**
**  P.T.Wallace   Starlink   31 October 1993
*/
{
   static int ftf = 1;
 
   if ( ftf )
   {
      ftf = 0;
      srand ( gmin ( UINT_MAX, (unsigned int) gmax ( 1.0,
                                 dnint ( fabs ( (double) seed ) ) ) ) );
   }
   return (float) rand() / (float) RAND_MAX;
}
