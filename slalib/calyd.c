#include "slalib.h"
#include "slamac.h"
void slaCalyd ( int iy, int im, int id, int *ny, int *nd, int *j )
/*
**  - - - - - - - - -
**   s l a C a l y d
**  - - - - - - - - -
**
**  Calendar to year and day in year.
**
**  Given:
**     iy,im,id   int    year, month, day in Gregorian calendar
**                       (year may optionally omit the century)
**  returned:
**     *ny        int    year AD
**     *nd        int    day in year (1 = January 1st)
**     *j         int    status:
**                        -1 = OK, but outside range 1900/3/1-2100/2/28
**                         0 = OK, and within above range
**                         2 = bad month  (day not computed)
**                         3 = bad day    (year,day both computed)
**
**  j=0 means that the date is within the range where the Gregorian
**  rule concerning century leap years can be neglected.
**
**  P.T.Wallace   Starlink   31 October 1993
*/
{
   int i;
 
/* Month lengths in days */
   static int mtab[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
 
/* Default century if appropriate */
   if ( ( iy >= 0 ) && ( iy <= 49 ) )
      *ny = iy + 2000;
   else if ( ( iy >= 50 ) && ( iy <= 99 ) )
      *ny = iy + 1900;
   else
      *ny = iy;
 
/* Preset status */
   *j = 0;
 
/* Validate month */
   if ( ( im >= 1 ) && ( im <= 12 ) ) {
 
   /* Allow for leap year */
      if ( ( *ny % 4 ) == 0)
         mtab[1] = 29;
      else
         mtab[1] = 28;
 
      if ( ( ( iy % 100 ) == 0 ) && ( ( iy % 400 ) != 0) )
         mtab[1] = 28;
 
   /* Validate day */
      if ( ( id < 1 ) || ( id > mtab[im-1] ) )
         *j = 3;
 
   /* Day in year */
      *nd = id;
 
      for ( i = 0; i < im-1; i++ )
         *nd = *nd + mtab[i];
   } else {
 
   /* Bad month */
      *j = 2;
   }
 
/* Flag dates outside the range 1900 March 1 to 2100 February 28 */
   if ( ( *j == 0 ) &&
        ( ( *ny < 1900 ) || ( *ny > 2100 ) ||
        ( ( *ny == 1900 ) && ( *nd < 60 ) ) ||
        ( ( *ny == 2100 ) && ( *nd > 59 ) ) ) )
     *j = -1;
}
