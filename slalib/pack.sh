#!/bin/sh
#-----------------------------------------------------------------------
# Pack *.c and *.h files into a shell archive.
# Usage:
#  sh pack.sh output_file_name
#-----------------------------------------------------------------------

output=$1

cat > $output << \END
#!/bin/sh
# This is a shell archive, meaning:
# 1. Remove everything above the #! /bin/sh line.
# 2. Save the resulting text in a file.
# 3. Execute the file with /bin/sh (not csh) to create:
#-----------------------------------------------------------------------
# NOTE: This script will only unpack files that don't already exist
# in your working directory. This means that if, after running this
# script once, you delete one disk file from the archive and then run
# the script a second time, only that file will be extracted.
#-----------------------------------------------------------------------
 
END

for file in *.c *.h
do

cat >> $output << END
if test ! -f $file ; then cat > $file << \EOF
END

cat $file >> $output

cat >> $output << END
EOF
fi

END
done
#-----------------------------------------------------------------------
