#include "slalib.h"
#include "slamac.h"
void slaDjcal ( int ndp, double djm, int iymdf[4], int *j )
/*
**  - - - - - - - - -
**   s l a D j c a l
**  - - - - - - - - -
**
**  Modified Julian Date to Gregorian calendar, expressed
**  in a form convenient for formatting messages (namely
**  rounded to a specified precision, and with the fields
**  stored in a single array).
**
**  Given:
**     ndp      int       number of decimal places of days in fraction
**     djm      double    Modified Julian Date (JD-2400000.5)
**
**  Returned:
**     iymdf    int[4]    year, month, day, fraction in Gregorian calendar
**     *j       long      status:  nonzero = out of range
**
**  Any date after 4701BC March 1 is accepted.
**
**  Large ndp values risk internal overflows.  It is typically safe
**  to use up to ndp=4.
**
**  The algorithm is derived from that of Hatcher 1984 (QJRAS 25, 53-55).
**
**  P.T.Wallace   Starlink   31 October 1993
*/
{
   long nfd;
   double fd, df, f, d;
   long jd, n4, nd10;
 
/* Validate */
   if ( ( djm <= -2395520.0 ) || ( djm >= 1.0e9 ) ) {
      *j = 1;
      return;
   } else {
 
   /* Denominator of fraction */
      nfd = (long) pow ( 10.0, (double) gmax ( ndp, 0 ) );
      fd = (double) nfd;
 
   /* Round date and express in units of fraction */
      df = dnint ( djm * fd );
 
   /* Separate day and fraction */
      f = fmod ( df, fd );
      if ( f < 0.0 ) f += fd;
      d = ( df - f ) / fd;
 
   /* Express day in Gregorian calendar */
      jd = (long) dnint ( d ) + 2400001l;
      n4 = 4l * ( jd + ( ( 2l * ( ( 4l * jd - 17918l ) / 146097l)
                                       * 3l ) / 4l + 1l ) / 2l - 37l );
      nd10 = 10l * ( ( ( n4 - 237l ) % 1461l ) / 4l ) + 5l;
      iymdf[0] = (int) ( ( n4 / 1461l ) - 4712l );
      iymdf[1] = (int) ( ( ( nd10 / 306l + 2l ) % 12l ) + 1l );
      iymdf[2] = (int) ( ( nd10 % 306l ) / 10l + 1l );
      iymdf[3] = (int) dnint ( f );
      *j = 0;
   }
}
