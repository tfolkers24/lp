#include "slalib.h"
#include "slamac.h"
static void atms ( double r, double *dn, double *dndr );
static void atmt ( double r, double *t, double *dn, double *dndr );
 
/*--------------------------------------------------------------------------*/
 
/* Variables shared between slaRefro and the internal routine atms */
double rt;     /* height of tropopause from centre of the Earth (metre) */
double tt;     /* temperature at the tropopause (deg k) */
double dnt;    /* refractive index at the tropopause */
double gamal;  /* constant of the atmospheric model = g*md/r */
 
/* Variables shared between slaRefro and the internal routine atmt */
double r0;     /* height of observer from centre of the Earth (metre) */
double t0;     /* temperature at the observer (deg K) */
double alpha;  /* alpha          |                 */
double gamm2;  /* gamma minus 2  | see HMNAO paper */
double delm2;  /* delta minus 2  |                 */
double c1,c2,c3,c4,c5,c6;  /* various */
 
/*--------------------------------------------------------------------------*/
 
void slaRefro ( double zobs, double hm, double tdk, double pmb,
                double rh, double wl, double phi, double tlr,
                double eps, double *ref )
/*
**  - - - - - - - - -
**   s l a R e f r o
**  - - - - - - - - -
**
**  Atmospheric refraction for radio and optical wavelengths.
**
**  Given:
**    zobs    double  observed zenith distance of the source (radian)
**    hm      double  height of the observer above sea level (metre)
**    tdk     double  ambient temperature at the observer (deg K)
**    pmb     double  pressure at the observer (millibar)
**    rh      double  relative humidity at the observer (range 0-1)
**    wl      double  effective wavelength of the source (micrometre)
**    phi     double  latitude of the observer (radian, astronomical)
**    tlr     double  temperature lapse rate in the troposphere (degK/metre)
**    eps     double  precision required to terminate iteration (radian)
**
**  Returned:
**    ref     double  refraction: in vacuo ZD minus observed ZD (radian)
**
**  Typical values for the tlr and eps arguments are 0.0065
**  and 1e-9 respectively.
**
**  This routine computes the refraction for zenith distances up to
**  and a little beyond 90 deg using the method of Hohenkerk and
**  Sinclair (NAO Technical Notes 59 and 63).  The C code is an
**  adaptation of the Fortran optical refraction subroutine AREF
**  of C.Hohenkerk (HMNAO, September 1984), with extensions to support
**  radio wavelengths.  Most of the modifications to the HMNAO optical
**  algorithm are cosmetic;  in addition the angle arguments have been
**  changed to radians, any value of zobs is allowed, and other values
**  have been limited to safe values.  The radio expressions were
**  devised by A.T.Sinclair (RGO - private communication), based on
**  the Essen & Froome refractivity formula adopted in Resolution 1
**  of the 13th International Geodesy Association General Assembly
**  (Bulletin Geodesique 1963 p390).
**
**  The radio refraction is chosen by specifying wl > 100 micrometres.
**
**  Before use, the value of zobs is expressed in the range +/- Pi.
**  If this ranged zobs is -ve, the result ref is computed from its
**  absolute value before being made -ve to match.  In addition, if
**  it has an absolute value greater than 93 deg, a fixed ref value
**  equal to the result for zobs = 93 deg is returned, appropriately
**  signed.
**
**  Fixed values of the water vapour exponent, height of tropopause, and
**  height at which refraction is negligible are used.
**
**  Called:  slaDrange, atmt, atms
**
**  Defined in slamac.h:  TRUE, FALSE
**
**  Original version by C.Hohenkerk, HMNAO, September 1986.
**  Starlink version by P.T.Wallace, revised 31 October 1993.
*/
{
/* Fixed parameters */
 
   static double d93 = 1.623156204; /* 93 degrees in radians        */
   static double gcr = 8314.36;     /* Universal gas constant       */
   static double dmd = 28.966;      /* Molecular weight of dry air  */
   static double dmw = 18.016;      /* Molecular weight of water
                                                             vapour */
   static double s = 6378120.0;     /* Mean Earth radius (metre)    */
   static double delta = 18.36;     /* Exponent of temperature
                                         dependence of water vapour
                                                           pressure */
   static double ht = 11000.0;      /* Height of tropopause (metre) */
   static double hs = 80000.0;      /* Upper limit for refractive
                                                    effects (metre) */
 
   int in, is, k, istart, i, j, optic;
   double zobs1, zobs2, hmok, pmbok, rhok, wlok, tol, wlsq, gb, a,
          gamma, pw0, w, tempo, dn0, dndr0, sk0, f0, dndrt, zt, ft,
          dnts, dndrts, zts, fts, rs, dns, dndrs, zs, fs, refo,
          fe, fo, h, fb, ff, step, z, r, rg, tg, t, dn, dndr, f,
          refp, reft;
 
/* Restrict number to given range. */
#define grange(LO,X,HI) (gmin(gmax((X),(LO)),(HI)));
 
/* Arc sine */
#define arcsin(A) (atan2((A),sqrt(gmax(1.0-A*A,0.0))));
 
/* The refraction integrand */
#define refi(R,DN,DNDR) ((R)*(DNDR)/(DN+R*DNDR));
 
 
 
/* Transform zobs into the normal range */
   zobs1 = slaDrange ( zobs );
   zobs2 = gmin ( fabs ( zobs1 ), d93 );
 
/* Keep other arguments within safe bounds */
   hmok = grange ( -1000.0, hm, 10000.0 );
   t0 = grange ( 100.0, tdk, 500.0 );
   pmbok = grange ( 0.0, pmb, 10000.0 );
   rhok  = grange ( 0.0, rh, 1.0 );
   wlok  = gmax ( 0.1, wl );
   alpha = grange ( 0.001, fabs ( tlr ), 0.01 );
 
/* Tolerance for iteration */
   tol = gmin ( fabs ( eps ), 0.1 ) / 2.0;
 
/* Decide whether optical or radio case - switch at 100 micron */
   optic = wlok <= 100.0;
 
/* Set up model atmosphere parameters defined at the observer */
   wlsq = wlok * wlok;
   gb = 9.784 * ( 1.0 - 0.0026 * cos ( 2.0 * phi ) - 2.8e-7 * hmok );
   a = ( optic ) ?
         ( ( 287.604 + 1.6288 / wlsq + 0.0136 / ( wlsq * wlsq ) )
                 * 273.15 / 1013.25 ) * 1e-6
       :
         77.624e-6;
   gamal = gb * dmd / gcr;
   gamma = gamal / alpha;
   gamm2 = gamma - 2.0;
   delm2 = delta - 2.0;
   pw0 = rhok * pow ( t0 / 247.1, delta );
   w = pw0 * ( 1.0 - dmw / dmd ) * gamma / ( delta - gamma );
   c1 = a * ( pmbok + w ) / t0;
   c2 = ( a * w + ( optic ? 11.2684e-6 : 12.92e-6 ) * pw0 ) / t0;
   c3 = ( gamma - 1.0 ) * alpha * c1 / t0;
   c4 = ( delta - 1.0 ) * alpha * c2 / t0;
   c5 = optic ? 0.0 : 371897e-6 * pw0 / t0;
   c6 = c5 * delm2 * alpha / ( t0 * t0 );
 
/* At the observer */
   r0 = s + hmok;
   atmt ( r0, &tempo, &dn0, &dndr0 );
   sk0 = dn0 * r0 * sin ( zobs2 );
   f0 = refi ( r0, dn0, dndr0 );
 
/* At the tropopause in the troposphere */
   rt = s + ht;
   atmt ( rt, &tt, &dnt, &dndrt );
   zt = arcsin ( sk0 / ( rt * dnt ) );
   ft = refi ( rt, dnt, dndrt );
 
/* At the tropopause in the stratosphere */
   atms ( rt, &dnts, &dndrts );
   zts = arcsin ( sk0 / ( rt * dnts ) );
   fts = refi ( rt, dnts, dndrts );
 
/* At the stratosphere limit */
   rs = s + hs;
   atms ( rs, &dns, &dndrs );
   zs = arcsin ( sk0 / ( rs * dns ) );
   fs = refi ( rs, dns, dndrs );
 
/*
** Integrate the refraction integral in two parts;  first in the
** troposphere (k=1), then in the stratosphere (k=2).
*/
 
   refo = -999.999;
   is = 16;
   for ( k = 1; k <= 2; k++ ) {
      istart = FALSE;
      fe = 0.0;
      fo = 0.0;
      if ( k == 1 ) {
         h = ( zt - zobs2 ) / (double) is;
         fb = f0;
         ff = ft;
      } else {
         h = ( zs - zts ) / (double) is;
         fb = fts;
         ff = fs;
      }
      in = is - 1;
      is /= 2;
      step = h;
 
   /* Start of iteration loop (terminates at specified precision) */
      for ( ; ; ) {
         for ( i = 1; i <= in; i++ ) {
            if ( i == 1 && k == 1 ) {
               z = zobs2 + h;
               r = r0;
            } else if ( i == 1 && k == 2 ) {
               z = zts + h;
               r = rt;
            } else {
               z += step;
            }
 
         /* Given the zenith distance (z) find r */
            rg = r;
            for ( j = 1; j <= 4; j++ ) {
               if ( k == 1 ) {
                  atmt ( rg, &tg, &dn, &dndr );
               } else {
                  atms ( rg, &dn, &dndr );
               }
               if (z > 1e-20) {
                  rg -= ( rg * dn - sk0 / sin ( z ) ) / ( dn + rg * dndr );
               }
            }
            r = rg;
 
         /* Find refractive index and integrand at r */
            if ( k == 1 ) {
               atmt ( r, &t, &dn, &dndr );
            } else {
               atms ( r, &dn, &dndr );
            }
            f = refi ( r, dn, dndr );
            if ( !istart && i % 2 == 0 ) {
               fe += f;
            } else {
               fo += f;
            }
         }
 
      /* Evaluate the integrand using Simpson's Rule */
         refp = h * ( fb + 4.0 * fo + 2.0 * fe + ff ) / 3.0;
 
      /* Has required precision been reached? */
         if ( fabs ( refp - refo ) > tol ) {
 
         /* No: prepare for next iteration */
            is *= 2;
            in = is;
            step = h;
            h /= 2.0;
            fe += fo;
            fo = 0.0;
            refo = refp;
            if ( !istart ) istart = TRUE;
 
         } else {
 
         /* Yes: save troposphere component and terminate loop */
            if ( k == 1 ) reft = refp;
            break;
         }
      }
   }
 
/* Result */
   *ref = reft + refp;
   if ( zobs1 < 0.0 ) *ref = - ( *ref );
}
 
/*--------------------------------------------------------------------------*/
 
static void atms ( double r, double *dn, double *dndr )
/*
**  - - - -
**   a t m s
**  - - - -
**
**  Internal routine used by slaRefro:
**
**  Refractive index and derivative wrt height for the stratosphere.
**
**  Given (variables shared with slaRefro):
**    rt     double   height of tropopause from centre of the Earth (metre)
**    tt     double   temperature at the tropopause (deg k)
**    dnt    double   refractive index at the tropopause
**    gamal  double   constant of the atmospheric model = g*md/r
**
**  Given (argument):
**    r      double   current distance from the centre of the Earth (metre)
**
**  Returned (arguments):
**    *dn    double   refractive index at r
**    *dndr  double   rate the refractive index is changing at r
**
**  This routine is derived from the ATMOSSTR routine (C.Hohenkerk, HMNAO).
*/
{
   double b, w;
 
   b = gamal / tt;
   w = ( dnt - 1.0 ) * exp ( - b * ( r - rt ) );
   *dn = 1.0 + w;
   *dndr = -b * w;
}
 
/*--------------------------------------------------------------------------*/
 
static void atmt ( double r, double *t, double *dn, double *dndr )
/*
**  - - - -
**   a t m t
**  - - - -
**
**  Internal routine used by slaRefro:
**
**  Refractive index and derivative wrt height for the troposphere.
**
**  Given (variables shared with slaRefro):
**    r0     double   height of observer from centre of the Earth (metre)
**    t0     double   temperature at the observer (deg K)
**    alpha  double   alpha          )
**    gamm2  double   gamma minus 2  ) see HMNAO paper
**    delm2  double   delta minus 2  )
**    c1     double   useful term  )
**    c2     double   useful term  )
**    c3     double   useful term  ) see source
**    c4     double   useful term  ) of slaRefro
**    c5     double   useful term  )
**    c6     double   useful term  )
**
**  Given (argument):
**    r      double   current distance from the centre of the Earth (metre)
**
**  Returned (arguments):
**    *t     double   temperature at r (deg K)
**    *dn    double   refractive index at r
**    *dndr  double   rate the refractive index is changing at r
**
**  This routine is derived from the ATMOSTRO routine (C.Hohenkerk,
**  HMNAO), with enhancements specified by A.T.Sinclair (RGO) to
**  handle the radio case.
**
**  Note that in the optical case c5 and c6 are zero.
*/
{
   double tt0, tt0gm2, tt0dm2;
 
   *t = t0 - alpha * ( r - r0 );
   tt0 = gmax ( *t / t0, 0.0 );
   tt0gm2 = pow ( tt0, gamm2 );
   tt0dm2 = pow ( tt0, delm2 );
   *dn = 1.0 + ( c1 * tt0gm2 - ( c2 - c5 / *t ) * tt0dm2 ) * tt0;
   *dndr = - c3 * tt0gm2 + ( c4 - c6 / tt0 ) * tt0dm2;
}
