#include "slalib.h"
#include "slamac.h"
#include <ctype.h>
double slaEpco ( char k0[], char k[], double e )
/*
**  - - - - - - - -
**   s l a E p c o
**  - - - - - - - -
**
**  Convert an epoch into the appropriate form - 'B' or 'J'.
**
**  Given:
**     k0    *char       form of result:  'B'=Besselian, 'J'=Julian
**     k     *char       form of given epoch:  'B' or 'J'
**     e     double      epoch
**
**  Called:  slaEpb, slaEpj2d, slaEpj, slaEpb2d
**
**  Notes:
**
**     1) The result is always either equal to or very close to
**        the given epoch e.  The routine is required only in
**        applications where punctilious treatment of heterogeneous
**        mixtures of star positions is necessary.
**
**     2) k0 and k are not validated, and only their first characters
**        are used, interpreted as follows:
**
**        o  If k0[0] and k[0] are the same the result is e.
**        o  If k0[0] is 'B' or 'b' and k[0] isn't, the conversion is J to B.
**        o  In all other cases, the conversion is B to J.
**
**        Note that k0[0] and k[0] won't match if their cases differ.
**
**  P.T.Wallace   Starlink   5 September 1993
*/
{
   double result;
   int c;
 
   c = toupper ( k0[0] );
   if ( c == toupper ( k[0] ) ) {
      result = e;
   } else {
      if ( c == 'B' ) {
         result = slaEpb ( slaEpj2d ( e ) );
      } else {
         result = slaEpj ( slaEpb2d ( e ) );
      }
   }
   return ( result );
}
