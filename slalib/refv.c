#include "slalib.h"
#include "slamac.h"
void slaRefv ( double vu[3], double refa, double refb, double vr[3] )
/*
**  - - - - - - - -
**   s l a R e f v
**  - - - - - - - -
**
**  Adjust an unrefracted Cartesian vector to include the effect of
**  atmospheric refraction, using the simple A tan z + B tan**3 z
**  model.
**
**  Given:
**    vu    double    unrefracted position of the source (az/el 3-vector)
**    refa  double    A: tan z coefficient (radian)
**    refb  double    B: tan**3 z coefficient (radian)
**
**  Returned:
**    *vr   double    refracted position of the source (az/el 3-vector)
**
**  Note that this routine applies the adjustment for refraction in
**  the opposite sense to the usual one - it takes an unrefracted
**  (in vacuo) position and produces an observed (refracted)
**  position, whereas the basic A tan z + B tan**3 z model strictly
**  applies to the case where a refracted position is available and
**  must be corrected for refraction.  This requires an inverted form of
**  the refraction expression;  the algorithm used here is based on
**  two iterations of the Newton-Raphson method.
**
**  See also the routine slaRefz, which performs the adjustment to
**  the zenith distance rather than in Cartesian az/el coordinates.
**  The results from slaRefz are slightly more accurate (in a
**  numerical sense) to those produced by the present routine, due to
**  the various approximations used in the latter for simplicity and
**  speed.
**
**  To avoid arithmetic overflow at the horizon, the z-component is
**  artificially constrained to the positive non-zero range.  The
**  vector vu must be of unit length;  No check is made.
**
**  P.T.Wallace   Starlink   31 October 1993
*/
{
   double x, y, z, zsq, rsq, r, wb, wt, d, cd, f;
 
/* Initial estimate = unrefracted vector */
   x = vu[0];
   y = vu[1];
   z = gmax ( fabs ( vu[2]), 1e-10 );
 
/* First Newton-Raphson iteration (to better than 1 mas at ZD 70 deg) */
   zsq = z * z;
   rsq = x * x + y * y;
   r = sqrt ( rsq );
   wb = refb * rsq / zsq;
   wt = ( refa + wb ) / ( 1.0 + ( refa + 3.0 * wb ) / zsq );
   d = wt * r / z;
   cd = 1.0 - d * d / 2.0;
   f = cd * ( 1.0 - wt );
   x *= f;
   y *= f;
   z = cd * ( z + d * r );
 
/* Second Newton-Raphson iteration */
   zsq = z * z;
   r *= f;
   rsq = r * r;
   wb = refb * rsq / zsq;
   d = ( ( refa + wb ) * r / z - d ) / ( 1.0 + ( refa + 3.0 * wb ) / zsq );
   cd = 1.0 - d * d / 2.0;
   f = cd * ( 1.0 - d * z / gmax ( r, 1e-10 ) );
   vr[0] = x * f;
   vr[1] = y * f;
   vr[2] = cd * ( z + d * r );
}
