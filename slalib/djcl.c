#include "slalib.h"
#include "slamac.h"
void slaDjcl ( double djm, int *iy, int *im, int *id, double *fd, int *j)
/*
**  - - - - - - - -
**   s l a D j c l
**  - - - - - - - -
**
**  Modified Julian Date to Gregorian year, month, day,
**  and fraction of a day.
**
**  Given:
**     djm      double     Modified Julian Date (JD-2400000.5)
**
**  Returned:
**     *iy      int        year ad
**     *im      int        month
**     *id      int        day
**     *fd      double     fraction of day
**     *j       int        status:
**                      -1 = unacceptable date (before 4701BC March 1)
**
**  The algorithm is derived from that of Hatcher 1984 (QJRAS 25, 53-55).
**
**  P.T.Wallace   Starlink   30 October 1993
*/
{
  double f, d;
  long jd, n4, nd10;
 
/* Check if date is acceptable */
   if ( ( djm <= -2395522.0 ) || ( djm >= 1e9 ) ) {
      *j = -1;
      return;
   } else {
      *j = 0;
 
   /* Separate day and fraction */
      f = fmod ( djm, 1.0 );
      if (f < 0.0) f += 1.0;
      d = dnint ( djm - f );
 
   /* Express day in Gregorian calendar */
      jd = (long) dnint ( d ) + 2400001;
      n4 = 4l*(jd+((6l*((4l*jd-17918l)/146097l))/4l+1l)/2l-37l);
      nd10 = 10l*(((n4-237l)%1461l)/4l)+5l;
      *iy = (int) (n4/1461l-4712l);
      *im = (int) (((nd10/306l+2l)%12l)+1l);
      *id = (int) ((nd10%306l)/10l+1l);
      *fd = f;
      *j = 0;
   }
}
