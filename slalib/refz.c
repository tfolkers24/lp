#include "slalib.h"
#include "slamac.h"
void slaRefz ( double zu, double refa, double refb, double *zr )
/*
**  - - - - - - - -
**   s l a R e f z
**  - - - - - - - -
**
**  Adjust an unrefracted zenith distance to include the effect of
**  atmospheric refraction, using the simple A tan z + B tan**3 z
**  model.
**
**  Given:
**    zu    double    unrefracted zenith distance of the source (radian)
**    refa  double    A: tan z coefficient (radian)
**    refb  double    B: tan**3 z coefficient (radian)
**
**  Returned:
**    *zr   double    refracted zenith distance (radian)
**
**  Note that this routine applies the adjustment for refraction in
**  the opposite sense to the usual one - it takes an unrefracted
**  (in vacuo) position and produces an observed (refracted)
**  position, whereas the basic A tan z + B tan**3 z model strictly
**  applies to the case where a refracted position is available and
**  must be corrected for refraction.  This requires an inverted form of
**  the refraction expression; The formula used here is based on the
**  Newton/Raphson method.  For numerical consistency with the
**  refracted to unrefracted model, two iterations are used;  The error
**  is less than 1e-11 arcseconds at 80 degrees ZD, and is still under
**  1 milliarcsecond at 88 degrees.
**
**  For the results to be useful, zu should be in the range zero to
**  89 degrees (expressed in radians).  For zu bigger than 89 degrees,
**  the value for zu = 89 degrees is returned.  No check is made for
**  zu very large or negative.
**
**  See also the routine slaRefv, which performs the adjustment in
**  Cartesian az/el coordinates.
**
**  P.T.Wallace   Starlink   15 July 1993
*/
{
   double zl, s, c, t, tsq, tcu;
 
/* Keep ZD within safe range */
   zl = (double) gmin ( fabs ( zu ), 1.553343034 );
 
/* Functions of ZD */
   s = sin ( zl );
   c = cos ( zl );
   t = s / c;
   tsq = t * t;
   tcu = t * tsq;
 
/* Refracted ZD (to better than 1mas at 70 deg) */
   zl -= ( refa * t + refb * tcu )
            / ( 1.0 + ( refa + 3.0 * refb * tsq ) / ( c * c ) );
 
/* Further iteration */
   s = sin ( zl );
   c = cos ( zl );
   t = s / c;
   tsq = t * t;
   tcu = t * tsq;
   *zr = zl - ( zl - zu + refa * t + refb * tcu )
             / ( 1.0 + ( refa + 3.0 * refb * tsq ) / ( c * c ) );
}
