#include "slalib.h"
#include "slamac.h"
double slaDtt ( double utc )
/*
**  - - - - - - -
**   s l a D t t
**  - - - - - - -
**
**  Increment to be applied to Coordinated Universal Time UTC to give
**  Terrestrial Time TT (formerly Ephemeris Time ET).
**
**  (double precision)
**
**  Given:
**     utc    double    UTC date as a modified JD (JD-2400000.5)
**
**  Result:  TT-UTC in seconds
**
**  Pre 1972 January 1 a fixed value of 10 + ET-TAI is returned.
**
**  Called:  slaDat
**
**  P.T.Wallace   Starlink   31 October 1993
*/
{
   return 32.184 + slaDat ( utc );
}
