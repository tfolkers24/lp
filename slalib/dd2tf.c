#include "slalib.h"
#include "slamac.h"
void slaDd2tf ( int ndp, double days, int *sign, int ihmsf[4] )
/*
**  - - - - - - - - -
**   s l a D d 2 t f
**  - - - - - - - - -
**
**  Convert an interval in days into hours, minutes, seconds.
**
**  (double precision)
**
**  Given:
**     ndp       int      number of decimal places of seconds
**     days      double   interval in days
**
**  Returned:
**     *sign     char     '+' or '-'
**     ihmsf     int[4]   hours, minutes, seconds, fraction
**
**  P.T.Wallace   Starlink   31 October 1993
*/

#define D2S 86400.0    /* Days to seconds */

{
   double rs, rm, rh, a, ah, am, as, af;
 
/* Handle sign */
   *sign = ( days < 0.0 ) ? '-' : '+';
 
/* Field units in terms of least significant figure */
   rs = pow ( 10.0, ( ndp > 0l ) ? ndp : 0 );
   rm = rs * 60.0;
   rh = rm * 60.0;
 
/* Round interval and express in smallest units required */
   a = dnint ( rs * D2S * fabs ( days ) );
 
/* Separate into fields */
   ah = dint ( a / rh );
   a  = a - ah * rh;
   am = dint ( a / rm );
   a  = a - am * rm;
   as = dint ( a / rs );
   af = a - as * rs;
 
/* Return results */
   ihmsf[0] = (int) ah;
   ihmsf[1] = (int) am;
   ihmsf[2] = (int) as;
   ihmsf[3] = (int) af;
}
