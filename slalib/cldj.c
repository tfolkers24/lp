#include "slalib.h"
#include "slamac.h"
void slaCldj ( int iy, int im, int id, double *djm, int *j )
/*
**  - - - - - - - -
**   s l a C l d j
**  - - - - - - - -
**
**  Gregorian calendar to Modified Julian Date.
**
**  Given:
**     iy,im,id     int    year, month, day in Gregorian calendar
**
**  Returned:
**     *djm         double Modified Julian Date (JD-2400000.5) for 0 hrs
**     *j           int    status:
**                           0 = OK
**                           1 = bad year   (MJD not computed)
**                           2 = bad month  (MJD not computed)
**                           3 = bad day    (MJD computed)
**
**  The year must be -4699 (i.e. 4700BC) or later.
**
**  The algorithm is derived from that of Hatcher 1984 (QJRAS 25, 53-55).
**
**  P.T.Wallace   Starlink   31 October 1993
*/
{
   long iyL, imL;
 
/* Month lengths in days */
   static int mtab[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
 
 
/* Preset status */
   *j = 0;
 
/* Validate year */
   if ( iy < -4699 ) {
      *j = 1;
      return;
   } else {
 
   /* Validate month */
      if ( ( im >= 1 ) && ( im <= 12 ) ) {
 
      /* Allow for leap year */
         if ( ( iy % 4 ) == 0 )
            mtab[1] = 29;
         else
            mtab[1] = 28;
 
         if ( ( iy % 100 ) == 0 && ( iy % 400 ) != 0 )
            mtab[1] = 28;
 
      /* Validate day */
         if ( id < 1 || id > mtab[im-1] ) {
            *j = 3;
            return;
         }
 
      /* Modified Julian Date */
 
      /* Lengthen year and month numbers to avoid overflow */
         iyL = (long) iy;
         imL = (long) im;
         *djm = (double)
              ( ( 1461l * ( iyL - ( 11L - imL ) / 10L + 4712L ) ) / 4L
              + ( 306L * ( ( imL + 9L ) % 12L ) + 5L ) / 10L
              - ( 3L * ( ( iyL - ( 11L - imL ) / 10L + 4900L ) / 100L ) ) / 4L
              + (long) id - 2399904L );
      } else {
 
      /* Bad month */
         *j = 2;
      }
   }
}
