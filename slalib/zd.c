#include "slalib.h"
#include "slamac.h"
double slaZd ( double ha, double dec, double phi )
/*
**  - - - - - -
**   s l a Z d
**  - - - - - -
**
**  HA, Dec to Zenith Distance.
**
**  (double precision)
**
**  Given:
**     ha     double     Hour Angle in radians
**     dec    double     declination in radians
**     phi    double     observatory latitude in radians
**
**  The result is in the range 0 to pi.
**
**  Notes:
**
**  1)  The latitude must be geodetic.  In critical applications,
**      corrections for polar motion should be applied.
**
**  2)  In some applications it will be important to specify the
**      correct type of hour angle and declination in order to
**      produce the required type of zenith distance.  In particular,
**      it may be important to distinguish between the zenith distance
**      as affected by refraction, which would require the "observed"
**      HA,Dec, and the zenith distance in vacuo, which would require
**      the "topocentric" HA,Dec.  If the effects of diurnal aberration
**      can be neglected, the "apparent" HA,Dec may be used instead of
**      the topocentric HA,Dec.
**
**  3)  No range checking of arguments is done.
**
**  4)  In applications which involve many zenith distance calculations,
**      rather than calling the present routine it will be more efficient
**      to use inline code, having previously computed fixed terms such
**      as sine and cosine of latitude, and perhaps sine and cosine of
**      declination.
**
**  P.T.Wallace   Starlink   15 July 1993
*/
{
   double cosz;
 
   cosz = sin ( dec ) * sin ( phi ) +
          cos ( dec ) * cos ( phi ) * cos ( ha );
   return atan2 ( sqrt ( gmax ( 0.0, 1.0 - cosz * cosz ) ), cosz );
}
