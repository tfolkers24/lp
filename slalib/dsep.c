#include "slalib.h"
#include "slamac.h"
double slaDsep ( double a1, double b1, double a2, double b2 )
/*
**  - - - - - - - -
**   s l a D s e p
**  - - - - - - - -
**
**  Angle between two points on a sphere.
**
**  (double precision)
**
**  Given:
**     a1,b1    double    spherical coordinates of one point
**     a2,b2    double    spherical coordinates of the other point
**
**  (The spherical coordinates are RA,dec, long,lat etc, in radians.)
**
**  The result is the angle, in radians, between the two points.  It
**  is always positive.
**
**  Called:  slaDcs2c
**
**  P.T.Wallace   Starlink   31 October 1993
*/
{
   int i;
   double d, v1[3], v2[3], w;
 
/* Convert coordinates from spherical to Cartesian */
   slaDcs2c ( a1, b1, v1 );
   slaDcs2c ( a2, b2, v2 );
 
/* Modulus squared of half the difference vector */
   w = 0.0;
   for ( i = 0; i < 3; i++ ) {
      d = v1[i] - v2[i];
      w += d * d;
   }
   w /= 4.0;
 
/* Angle between the vectors */
   return 2.0 * atan2 ( sqrt ( w ), sqrt ( gmax ( 0.0, 1.0 - w )));
}
