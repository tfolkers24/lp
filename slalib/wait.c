#include "slalib.h"
#include "slamac.h"
#include <stdio.h> /* added by jrh 12 sept95 */
#include <time.h>
void slaWait ( float delay )
/*
**  - - - - - - - -
**   s l a W a i t
**  - - - - - - - -
**
**  Interval wait.
**
**  Given:
**     delay     float      delay in seconds
**
**  Note that this routine is extremely crude, in that the wait
**  is implemented via a CPU-bound loop.  The hibernate and
**  scheduled wakeup facilities that all operating systems offer
**  and which are required for a proper implementation are
**  unfortunately not available through any standard C library
**  functions.
**
**  P.T.Wallace   Starlink   23 October 1993
*/
{
   time_t then,      /* Calendar time on entry        */
          now;       /* Calendar time during the wait */
 
/* Calendar time at start */
   then = time ( NULL );
 
/* Proceed if OK */
   if ( then != -1 ) {
 
   /* Loop */
      for ( ; ; ) {
 
      /* Calendar time now */
         now = time ( NULL );
 
      /* Break if requested amount of time has elapsed */
         if ( now == -1 ||
              (float) difftime ( now, then ) >= delay ) break;
      }
   }
}
