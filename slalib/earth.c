#include "slalib.h"
#include "slamac.h"
void slaEarth ( int iy, int id, float fd, float posvel[6] )
/*
**  - - - - - - - - -
**   s l a E a r t h
**  - - - - - - - - -
**
**  Approximate heliocentric position and velocity of the Earth.
**
**  (single precision)
**
**  Given:
**     iy       int        year ad
**     id       int        day in year (1 = jan 1st)
**     fd       float      fraction of day
**
**  Returned:
**     posvel   float[6]   Earth position & velocity vector
**
**  The date and time is TDB (loosely ET) in the Gregorian
**  calendar, and is interpreted in a manner which is valid
**  between 1900 March 1 and 2100 February 28.
**
**  The Earth heliocentric 6-vector is mean equator and equinox
**  of date.  Position part, posvel(0-2), is in AU;  velocity part,
**  posvel(3-5), is in AU/sec.
**
**  Max/RMS errors 1950-2050:
**     13/5  e-5 AU = 19200/7600 km in position
**     47/26 e-10 AU/s = 0.0070/0.0039 km/s in speed
**
**  More precise results are obtainable with the routine slaEvp.
**
**  Defined in slamac.h:  D2PI
**
**  P.T.Wallace   Starlink   31 October 1993
*/
 
#define SPEED 1.9913e-7f    /* Mean orbital speed of Earth, AU/s */
#define REMB  3.12e-5f      /* Mean Earth:EMB distance, AU       */
#define SEMB  8.31e-11f     /* Mean Earth:EMB speed, AU/s        */
 
{
   float yi, yf, t, elm, gamma, em, elt, eps0,
         e, esq, v, r, elmm, coselt, sineps, coseps, w1, w2, selmm, celmm;
 
/* Whole years & fraction of year, and years since J1900.0 */
   yi = (float) ( iy - 1900 );
   yf = ( (float) ( 4 * ( id - 1 / ( ( iy % 4 ) + 1 ) )
                    - ( iy % 4 ) - 2 ) + 4.0f * fd ) / 1461.0f;
   t = yi + yf;
 
/* Geometric mean longitude of Sun */
/* (cf 4.881627938+6.283319509911*t mod 2pi) */
   elm = (float) fmod ( ( 4.881628f + D2PI * yf + 0.00013420f * t ),
                        D2PI );
 
/* Mean longitude of perihelion */
   gamma = 4.908230f + 3.0005e-4f * t;
 
/* Mean anomaly */
   em = elm - gamma;
 
/* Mean obliquity */
   eps0 = 0.40931975f - 2.27e-6f * t;
 
/* Eccentricity */
   e = 0.016751f - 4.2e-7f * t;
   esq = (float) ( e * e );
 
/* True anomaly */
   v = em + (float) ( 2.0f * e * (float) sin ( em ) )
          + (float) ( 1.25f * esq * (float) sin ( 2.0f * em ) );
 
/* True ecliptic longitude */
   elt = v + gamma;
 
/* True distance */
   r = (float) ( ( 1.0f - esq ) / ( 1.0f + ( e * (float) cos ( v ) ) ) );
 
/* Moon's mean longitude */
   elmm = (float) fmod( ( 4.72f + 83.9971f * t ), D2PI );
 
/* Useful functions */
   coselt = (float) cos ( elt );
   sineps = (float) sin ( eps0 );
   coseps = (float) cos ( eps0 );
   w1 = -r * (float) sin ( elt );
   w2 = -SPEED * ( coselt + e * (float) cos ( gamma ) );
   selmm = (float) sin ( elmm );
   celmm = (float) cos ( elmm );
 
/* Earth position and velocity */
   posvel[0] = - r * coselt - REMB * celmm;
   posvel[1] = ( w1 - REMB * selmm ) * coseps;
   posvel[2] = ( w1 * sineps );
   posvel[3] = ( SPEED * ( sin ( elt )
                        + e * (float) sin ( gamma ) )
                        + SEMB * selmm );
   posvel[4] = ( w2 - SEMB * celmm ) * coseps;
   posvel[5] = ( w2 * sineps );
}
