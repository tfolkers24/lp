#include "slalib.h"
#include "slamac.h"
double slaEqeqx ( double date )
/*
**  - - - - - - - - -
**   s l a E q e q x
**  - - - - - - - - -
**
**  Equation of the equinoxes.
**
**  (double precision)
**
**  Given:
**     date    double      TDB (loosely ET) as Modified Julian Date
**                                          (JD-2400000.5)
**
**  The result is the equation of the equinoxes (double precision)
**  in radians:
**
**  Greenwich apparent ST = Greenwich mean ST + equation of the equinoxes
**
**  Called:  slaNutc
**
**  P.T.Wallace   Starlink   31 October 1993
*/
{
   double dpsi, deps, eps0;
 
/* Nutation */
   slaNutc ( date, &dpsi, &deps, &eps0 );
 
/* Equation of the equinoxes */
   return dpsi * cos ( eps0 + deps );
}
