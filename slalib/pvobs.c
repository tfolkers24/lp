#include "slalib.h"
#include "slamac.h"
void slaPvobs ( double p, double h, double stl, double pv[6] )
/*
**  - - - - - - - - -
**   s l a P v o b s
**  - - - - - - - - -
**
**  Position and velocity of an observing station.
**
**  (double precision)
**
**  Given:
**     p     double     latitude (geodetic, radians)
**     h     double     height above reference spheroid (geodetic, metres)
**     stl   double     local apparent sidereal time (radians)
**
**  Returned:
**     pv    double[6]  position/velocity 6-vector (au, au/s, geocentric
**                                                               apparent)
**
**  IAU 1976 constants are used.
**
**  Called:  slaGeoc
**
**  Deined in slamac.h:  DS2R
**
**  P.T.Wallace   Starlink   30 October 1993
*/
{
   double r, z, s, c, v;
 
/* Geodetic to geocentric conversion */
   slaGeoc ( p, h, &r, &z );
 
/* Functions of ST */
   s = sin ( stl );
   c = cos ( stl );
 
/* Speed */
   v = DS2R * r;
 
/* Position */
   pv[0] = r * c;
   pv[1] = r * s;
   pv[2] = z;
 
/* Velocity */
   pv[3] = - v * s;
   pv[4] = v * c;
   pv[5] = 0.0;
}
