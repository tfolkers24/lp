@echo off
rem
rem  - - - - - - -
rem   R E P . B A T
rem  - - - - - - -
rem
rem  Update one module in the SLALIB library
rem
rem  Command:
rem
rem     REP module
rem
rem  File SLALIB.BAK is deleted.
rem
rem  P.T.Wallace   Starlink   19 June 1993
rem
qcl /c /Ox /Za /batch %1.c
lib slalib -+%1;
del %1.obj
del slalib.bak
echo:
