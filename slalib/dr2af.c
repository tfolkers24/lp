#include "slalib.h"
#include "slamac.h"
void slaDr2af ( int ndp, double angle, int *sign, int idmsf[4] )
/*
**  - - - - - - - - -
**   s l a D r 2 a f
**  - - - - - - - - -
**
**  Convert an angle in radians to degrees, arcminutes, arcseconds.
**
**  (double precision)
**
**  Given:
**     ndp       int          number of decimal places of arcseconds
**     angle     double       angle in radians
**
**  Returned:
**     sign      int*         '+' or '-'
**     idmsf     long[4]      degrees, arcminutes, arcseconds, fraction
**
**  Called:
**     slaDd2tf
**
**  Defined in slamac.h:  D15B2P
**
**  P.T.Wallace   Starlink   30 October 1993
*/
{
/* Scale then use days to h,m,s routine */
   slaDd2tf ( ndp, angle * D15B2P, sign, idmsf );
}
