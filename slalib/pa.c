#include "slalib.h"
#include "slamac.h"
double slaPa ( double ha, double dec, double phi )
/*+
**  - - - - - -
**   s l a P a
**  - - - - - -
**
**  HA, Dec to Parallactic Angle.
**
**  (double precision)
**
**  Given:
**     ha     d     hour angle in radians (geocentric apparent)
**     dec    d     declination in radians (geocentric apparent)
**     phi    d     observatory latitude in radians (geodetic)
**
**  The result is in the range -pi to +pi
**
**  Notes:
**
**  1)  The parallactic angle at a point in the sky is the position
**      angle of the vertical, i.e. the angle between the direction to
**      the pole and to the zenith.  In precise applications care must
**      be taken only to use geocentric apparent HA,Dec and to consider
**      separately the effects of atmospheric refraction and telescope
**      mount errors.
**
**  2)  At the pole a zero result is returned.
**
**  P.T.Wallace   Starlink   30 October 1993
*/
{
   double clat, slat, cqsz, sqsz, cdc, sdc;
 
   slat = sin ( phi );
   clat = cos ( phi );
   sdc = sin ( dec );
   cdc = cos ( dec );
   if ( fabs ( cdc ) < 1e-30 ) {
      cdc = 1e-30;
   }
   sqsz = clat * sin ( ha );
   cqsz = ( slat - sdc * ( sdc * slat + cdc * clat * cos ( ha ) ) ) / cdc;
   if ( sqsz == 0.0 && cqsz == 0.0 )
      cqsz = 1.0;
   return ( atan2 ( sqsz, cqsz ) );
}
