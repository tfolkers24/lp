#include "slalib.h"
#include "slamac.h"
void slaXy2xy ( double x1, double y1, double coeffs[6],
                double *x2, double *y2 )
/*
**  - - - - - - - - -
**   s l a X y 2 x y
**  - - - - - - - - -
**
**  Transform one [x,y] into another using a linear model of the type
**  produced by the slaFitxy routine.
**
**  Given:
**     x1       double        x-coordinate
**     y1       double        y-coordinate
**
**  Returned:
**     coeffs   double[6]     transformation coefficients (see note)
**     *x2      double        x-coordinate
**     *y2      double        y-coordinate
**
**  The model relates two sets of [x,y] coordinates as follows.
**  Naming the elements of coeffs:
**
**     coeffs[0] = a
**     coeffs[1] = b
**     coeffs[2] = c
**     coeffs[3] = d
**     coeffs[4] = e
**     coeffs[5] = f
**
**  the present routine performs the transformation:
**
**     x2 = a + b*x1 + c*y1
**     y2 = d + e*x1 + f*y1
**
**  See also slaFitxy, slaPxy, slaInvf, slaDcmpf.
**
**  P.T.Wallace   Starlink   31 October 1993
*/
{
   *x2 = coeffs[0] + coeffs[1] * x1 + coeffs[2] * y1;
   *y2 = coeffs[3] + coeffs[4] * x1 + coeffs[5] * y1;
}
