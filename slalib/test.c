#include <stdio.h>
#include <math.h>

double sin();
double cos();
double tan();
double asin();
double slaGmst();
double slaEpj();
double fabs();

#define DEG2RAD (2.0*M_PI/360.0)
#define RAD2DEG (360.0/2/M_PI)
#define HR2RAD  (15.0*DEG2RAD)
#define OBSLAT  (31.9538889*DEG2RAD)
#define OBSLONG (-7.44094722)
#define UT2LST  (366.25/365.25)

struct SLACONST {
  int yr, mon, day;
  double lst, mjd , mjdd;
  double amprms[21];
} slaconst;

struct ANSWER {
  double ra, dec;
} answer[1000];
  

main()
{
  int i, j;
  double az, el, ra, dec;
  long tm;
  char *ut_date = "1995.0914";

/* given az el and ut_date, and millisecs since mignight, get ra dec */

  az = 90.5;
  el = 51.0;
  tm = 22.5*3600*1000;

  for( i=0; i< 100; i++ ) {
    init_radec( ut_date, tm, &slaconst );

    for(j=0; j<720; j++ ) {
      get_radec( az, el, tm, &slaconst, &ra, &dec );
      answer[j].ra = ra;
      answer[j].dec = dec;
      tm += 100; /* 10 seconds to turn around */
    }

    tm += 10000; /* 10 seconds to turn around */
    az += 0.2;
    el = 20.0*cos(az*DEG2RAD-(M_PI/4.0)) + 40.0;
  }

  printf("ra %f   dec %f\n", ra, dec);
}

/*
  precompute constants for row
  date_str is in sdd header format yyyy.mmdd
  tm is millisecs since midnight
*/

init_radec( date_str, tm, p )
char *date_str;
long tm;
struct SLACONST *p;
{
  char date[20];
  int ret;

  bzero( p, sizeof(struct SLACONST));
  strcpy(date, date_str);
  p->yr = atoi(date);
  p->day = atoi(&date[7]);
  date[7] = '\0';
  p->mon = atoi(&date[5]);
  
  slaCaldj( p->yr, p->mon, p->day, &p->mjdd, &ret );
  if( ret )
    sprintf(stderr, "sla: bad mjd calc: %d\n", ret );

  p->mjd = p->mjdd + (double)tm/1000.0/3600.0/24.0;
  p->lst = slaGmst(p->mjd) + OBSLONG*UT2LST*HR2RAD;

  slaMappa( 2000.0, p->mjd, &p->amprms[0]);
  return(ret);
}

/*
  given az, el and tm of 12 meter telescope return ra, dec

  az, el in degrees
  tm in millisecs since midnight
  pconst is a pointer to struct SLACONST computed by init_radec
  returns 
  B1950 ra dec in degrees
*/

get_radec( az, el, tm, p, pra, pdec )
double az;
double el;
long tm;
struct SLACONST *p;
double *pra;
double *pdec;
{
  double ra, dec, h, newra, newdec, pma, pmd;
  double lst;

  az *= DEG2RAD;
  el *= DEG2RAD;

  dec = asin(sin(OBSLAT)*sin(el) + cos(OBSLAT)*cos(el)*cos(az));
  
  h = acos(sin(el)/cos(OBSLAT)/cos(dec) - tan(OBSLAT)*tan(dec));

  if( az >= 0  && az < M_PI )
    h = -fabs(h);
  else
    h = fabs(h);

  ra = lst - h;

  slaAmpqk( ra, dec, &p->amprms[0],  &newra, &newdec );

  slaFk54z( newra, newdec, (double)1950.0, &ra, &dec, &pma, &pmd );

  *pra = ra*RAD2DEG;
  *pdec =dec*RAD2DEG;
}

