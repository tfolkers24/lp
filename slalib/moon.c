#include "slalib.h"
#include "slamac.h"
void slaMoon ( int iy, int id, float fd, float posvel[6] )
/*
**  - - - - - - - -
**   s l a M o o n
**  - - - - - - - -
**
**  Approximate geocentric position and velocity of the Moon.
**
**  Given:
**     iy       int        year AD
**     id       int        day in year (1 = Jan 1st)
**     fd       float      fraction of day
**
**  Returned:
**     posvel   float[6]   Moon position & velocity vector
**
**  Called:
**     slaCs2c6, slaEuler, slaMxv
**
**  The date and time is TDB (loosely ET) in the Gregorian
**  calendar, and is interpreted in a manner which is valid
**  between 1900 March 1 and 2100 February 28.
**
**  The Moon 6-vector is Moon centre relative to Earth centre,
**  mean equator and equinox of date.  Position part, posvel(0-2),
**  is in AU;  velocity part, posvel(3-5), is in AU/sec.
**
**  The position is accurate to better than 0.5 arcminute
**  in direction and 1e-5 AU in distance.  The velocity
**  is accurate to better than 0.5"/hour in direction and
**  4 m/s in distance.  Note that the distance accuracy is
**  comparatively poor because this routine is merely
**  intended for use in computing topocentric direction.
**
**  Reference:
**     Meeus, l'Astronomie, June 1984, p348.
**
**  This routine is only a partial implementation of the original
**  algorithm, which offers somewhat higher accuracy in direction
**  and much higher accuracy in distance (or rather in horizontal
**  parallax) when fully implemented.
**
**  Defined in slamac.h:  DD2R
**
**  P.T.Wallace   Starlink   31 October 1993
*/

#define RATCON 9.652743551e-12f   /* Rate conversion factor:
                                          DD2R**2 / (86400 * 365.25) */
#define ERADAU 4.26352e-5f        /* Earth radius in AU */

{
   int i,m,n;
   float v1[6], v2[6], rmat[3][3];

/*
**  Coefficients for fundamental arguments
**
**  Fixed term (deg), term in t (deg & whole revs + fraction per year)
**
**  Moon's mean longitude
*/
   static float elp0  = 270.4342f;
   static float elp1  = 4812.678831f;
   static float elp1i = 4680.0f;
   static float elp1f = 132.678831f;
 
/* Sun's mean anomaly */
   static float em0  = 358.4758f;
   static float em1  = 359.990498f;
   static float em1i = 0.0f;
   static float em1f = 359.990498f;
 
/* Moon's mean anomaly */
   static float emp0  = 296.1046f;
   static float emp1  = 4771.988491f;
   static float emp1i = 4680.0f;
   static float emp1f = 91.988491f;
 
/* Moon's mean elongation */
   static float d0 = 350.7375f;
   static float d1 = 4452.671142f;
   static float d1i = 4320.0f;
   static float d1f = 132.671142f;
 
/* Mean distance of the Moon from its ascending node */
   static float f0 = 11.2509f;
   static float f1 = 4832.020251f;
   static float f1i = 4680.0f;
   static float f1f = 152.020251f;
 
   static float tl[39] = {
       6.288750f,  1.274018f,  0.658309f,
       0.213616f, -0.185596f, -0.114336f,
       0.058793f,  0.057212f,  0.053320f,
       0.045874f,  0.041024f, -0.034718f,
      -0.030465f,  0.015326f, -0.012528f,
      -0.010980f,  0.010674f,  0.010034f,
       0.008548f, -0.007910f, -0.006783f,
       0.005162f,  0.005000f,  0.004049f,
       0.003996f,  0.003862f,  0.003665f,
       0.002695f,  0.002602f,  0.002396f,
      -0.002349f,  0.002249f, -0.002125f,
      -0.002079f,  0.002059f, -0.001773f,
      -0.001595f,  0.001220f, -0.001110f
   };
 
   static float tb[29] = {
       5.128189f,  0.280606f,  0.277693f,  0.173238f,
       0.055413f,  0.046272f,  0.032573f,  0.017198f,
       0.009267f,  0.008823f,  0.008247f,  0.004323f,
       0.004200f,  0.003372f,  0.002472f,  0.002222f,
       0.002072f,  0.001877f,  0.001828f, -0.001803f,
      -0.001750f,  0.001570f, -0.001487f, -0.001481f,
       0.001417f,  0.001350f,  0.001330f,  0.001106f,
       0.001020f
   };
 
   static float tp[4] = {
       0.051818f,  0.009531f,  0.007843f,  0.002824f
   };
 
   float yi, yf, t, elp, em, emp, d, f, el, eld, coeff, cem, cemp;
   float cd, cf, theta, thetad, b, bd, p, pd, r, rd;
   float eps;
 
/*
**  Coefficients for Moon position
**
**   t(n)       = coefficient of term (deg)
**   it(n,1-4)  = coefficients of m, m', d, f in argument
**
**  Longitude
*/
   static long itl[4][39] = {
/* m */  {  0,  0,  0,  0,  1,  0,  0, -1,  0, -1, -1,  0,  1,  0,  0,
            0,  0,  0,  0,  1,  1,  0,  1, -1,  0,  0,  0, -1,  0, -1,
            0, -2,  1,  2, -2,  0,  0, -1,  0 },
 
/* m' */ {  1, -1,  0,  2,  0,  0, -2, -1,  1,  0,  1,  0,  1,  0,  1,
           -1, -1,  3, -2, -1,  0,  1,  0,  1,  2,  0, -3,  2,  1, -2,
            1,  0,  2,  0, -1,  1,  0, -1,  2 },
 
/* d */  {  0,  2,  2,  0,  0,  0,  2,  2,  2,  2,  0,  1,  0,  2,  0,
            0,  4,  0,  4,  2,  2, -1,  1,  2,  2,  4,  2,  0, -2,  2,
            1,  2,  0,  0,  2,  2,  2,  4,  0 },
 
/* f*/   {  0,  0,  0,  0,  0,  2,  0,  0,  0,  0,  0,  0,  0, -2,  2,
            2,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, -2,  0,
            0,  0,  0,  0,  0, -2,  2,  0,  2 }
   };
 
/* Latitude */
  static long itb[4][29] = {
/* m */  {  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
           -1,  0,  0, -1, -1, -1, -1, -1,  0,  1,
            0, -1,  0,  1, -1, -1,  0,  0,  0 },
 
/* m' */ {  0,  1,  1,  0, -1, -1,  0,  2,  1,  2,
            0, -2,  1,  0, -1,  0, -1,  1, -1,  0,
            0,  1,  0,  1, -1,  0,  0,  3,  0 },
 
/* d */  {  0,  0,  0,  2,  2,  2,  2,  0,  2,  0,
            2,  2,  2, -2,  2,  2,  2,  0,  4,  0,
            0,  0,  1,  0,  0,  0, -1,  0,  4 },
/* f */  {  1,  1, -1, -1,  1, -1,  1,  1, -1, -1,
           -1, -1,  1,  1,  1,  1, -1,  1, -1,  1,
            3, -1,  1,  1,  1,  1,  1,  1, -1 }
   };
 
/* Parallax */
   static long itp[4][4] = {
/* m */  {  0,  0,  0,  0 },
/* m' */ {  1, -1,  0,  2 },
/* d */  {  0,  2,  2,  0 },
/* f */  {  0,  0,  0,  0 }
   };
 
/* Whole years & fraction of year, and years since J1900.0 */
   yi = (float) ( iy - 1900 );
   yf = ( (float) ( 4 * ( id - 1 / ( ( iy % 4 ) + 1 ) )
                - ( iy % 4 ) - 2 ) + ( 4.0f * fd ) ) / 1461.0f;
   t  = yi + yf;
 
/* Moon's mean longitude */
   elp = DD2R * (float) fmod ( elp0 + elp1i * yf + elp1f * t, 360.0f );
 
/* Sun's mean anomaly */
   em = DD2R * (float) fmod ( em0 + em1i + em1f * t, 360.0f );
 
/* Moon's mean anomaly */
   emp = DD2R * (float) fmod ( emp0 + emp1i * yf + emp1f * t, 360.0f );
 
/* Moon's mean elongation */
   d = DD2R * (float) fmod ( d0 + d1i * yf + d1f * t, 360.0f );
 
/* Mean distance of the Moon from its ascending node */
   f = DD2R * (float) fmod ( f0 + f1i * yf + f1f * t, 360.0f );
 
/* Longitude */
   el  = 0.0f;
   eld = 0.0f;
   for ( m = 0; m < 39; m++ ) {
      n = 38 - m;
      coeff = tl[n];
      cem = (float) itl[0][n];
      cemp = (float) itl[1][n];
      cd = (float) itl[2][n];
      cf = (float) itl[3][n];
      theta = cem * em + cemp * emp + cd * d + cf * f;
      thetad = cem * em1 + cemp * emp1 + cd * d1 + cf * f1;
      el = el + coeff * sin ( theta );
      eld = eld + coeff * cos ( theta ) * thetad;
   }
   el = el * DD2R + elp;
   eld = RATCON * ( eld + elp1 / DD2R );
 
/* Latitude */
   b = 0.0f;
   bd = 0.0f;
   for ( m = 0; m < 29; m++ ) {
      n = 28 - m;
      coeff = tb[n];
      cem = (float) itb[0][n];
      cemp = (float) itb[1][n];
      cd = (float) itb[2][n];
      cf = (float) itb[3][n];
      theta = cem * em + cemp * emp + cd * d + cf * f;
      thetad = cem * em1 + cemp * emp1 + cd * d1 + cf * f1;
      b += (float) ( coeff * sin ( theta ) );
      bd += (float) ( coeff * cos ( theta ) * thetad );
   }
   b *= DD2R;
   bd *= RATCON;
 
/* Parallax */
   p = 0.0f;
   pd = 0.0f;
   for ( m = 0; m < 4; m++ ) {
      n = 3 - m;
      coeff = tp[n];
      cem = (float) itp[0][n];
      cemp = (float) itp[1][n];
      cd = (float) itp[2][n];
      cf = (float) itp[3][n];
      theta = cem * em + cemp * emp + cd * d + cf * f;
      thetad = cem * em1 + cemp * emp1 + cd * d1 + cf * f1;
      p += (float) ( coeff * cos ( theta ) );
      pd -= (float) ( coeff * sin ( theta ) * thetad );
   }
   p = ( p + 0.950724f ) * DD2R;
   pd *= RATCON;
 
/* Transform parallax to distance (AU, AU/sec) */
   r = ERADAU / p;
   rd = -r * pd / p;
 
/* Longitude, latitude to x,y,z (AU) */
   slaCs2c6 ( el, b, r, eld, bd, rd, v1 );
 
/* Mean obliquity */
   eps = DD2R * ( 23.45229f - 0.00013f * t );
 
/* Rotation matrix - ecliptic to equatorial */
   slaEuler ( "X", -eps, 0.0f, 0.0f, rmat );
 
/* Rotate Moon position and velocity into equatorial system */
   slaMxv ( rmat, v1, v2 );
   slaMxv (rmat, &v1[3], &v2[3]);
 
/* Return results */
   for ( i = 0; i < 6; i++ ) {
      posvel[i] = v2[i];
   }
}
