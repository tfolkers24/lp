/*
 * This is the Planets page of the LinuxPops reference manual, built using
 * doxygen.
 */

/**

@page lp_planets LinuxPops Planets Commands

@section lp_planets_command Planets

@section lp_planets_synopsis Synopsis

\code

Linuxpops(H0) >> planets /h
********************************************************************************
*	Internal Planet info
*	
*	Mode: ALL
*	
*	Usage: plan [date] [freq] [options]
*	
*	options are:
*	
*	/d  Use the 'date' from the environment
*	
*	/f  User passed 'freq' in GHz
*	    NOTE: If passing both '/u date' & '/f freq', 
*	    'date' must be first and 'freq' must be second
*	
*	/n  Now (use current time for ephemeris)
*	
*	/u  User passed 'date' in the form: yyyy.mmdd
*	    In the range of: 2012.0101 to 2025.1231, for now...
*	
*	/r  Reload (reload planet ephemeris)
*	
*	/h  help
*	
*	NOTE: By default, uses the 'date' from the loaded scan and 'freq' from the
*	      environment and should be set before using this function otherwise 
*	      'now' will be used for date and you will get errors in regards to
*	      un-set frequency
*	
*	      Set variable dish_size or taper to be used instead of defaults in
*	      planet convoluted beam size calculations
*	See Also:  pflux, planets, variable:dish_size, variable:taper
*	
********************************************************************************
Linuxpops(H0) >> 

\endcode

@section lp_planets_usage Usage

\code

Linuxpops(H0) >> planets
Linuxpops(H0) >>  # Loading Planet Brightness Tables
Linuxpops(H0) >>  # Looking for Mars date of: 2019 06 03 07
Linuxpops(H0) >>  # Mars date line  = 82569
Linuxpops(H0) >>  # UT-Date:        = 06/03/2019
Linuxpops(H0) >>  # Mars Ephem line = 2712.33
Linuxpops(H0) >>  # System Freq     = 230.54
Linuxpops(H0) >>  # Lambda:         =   1.30mm
Linuxpops(H0) >>  # Diameter:       =  10.00m
Linuxpops(H0) >>  # Taper:          =  12.00dB
Linuxpops(H0) >>  # Alpha:          =   1.18
Linuxpops(H0) >>  # Theta_mb:       =  31.65 arcsec
Linuxpops(H0) >>  # Dish Area       =  78.54m^2
Linuxpops(H0) >>  # JFact           =  35.16
Linuxpops(H0) >>  # JFact(Real)     =  44.12

 [ Tbright: A = ALMA; S = Shirley; F = Fixed ]

  Name       RA            Dec        Vel  Sun  SAZ  Dist  Major   Minor   Tbright   Freq  FWHM  Csize    Tmb    Flux  Flux2
 Mercury   5:47:21.5    25:21:54.6  -28.3  14.8  1   1.19   5.63"   5.63" 450.00(F) 230.5  31.7" 31.82"   9.65  430.3  425.5
   Venus   3:23:44.4    17:21:06.9    6.6  19.5  1   1.60  10.45"  10.45" 288.37(A) 230.5  31.7" 32.24"  20.58  949.4  907.7
    Mars   6:51:22.7    24:04:29.7    9.1  29.3  1   2.44   3.84"   3.82" 194.82(A) 230.5  31.7" 31.73"   1.91   86.5   84.2
 Jupiter  17:18:26.0   -22:29:36.6   -4.5 172.0  0   4.30  45.90"  42.92" 176.12(A) 230.5  31.7" 41.04" 126.83 11183.3 5595.5
  Saturn  19:24:39.4   -21:38:38.0  -17.5 142.9  0   9.22  18.02"  16.08" 139.77(S) 230.5  31.7" 33.20"  24.36 1367.6 1074.7
  Uranus   2:10:40.3    12:39:04.3  -17.8  37.7  1  20.64   3.42"   3.32"  97.08(A) 230.5  31.7" 31.71"   0.71   34.1   31.5
 Neptune  23:19:46.5    -5:25:11.3  -29.0  83.8  0  30.03   2.27"   2.21"  81.23(A) 230.5  31.7" 31.68"   0.26   12.7   11.6
Linuxpops(H0) >> 

\endcode

*/
