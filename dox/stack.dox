/*
 * This is the stack page of the LinuxPops reference manual, built using
 * doxygen.
 */

/**

@page lp_stack LinuxPops Stack Commands

@section lp_stack_command Stack

The built in search criteria system within Linuxpops pretty much makes the stack obsolete.  
Normally, you make all the search criteria known to LinuxPops and all subsequent actions
of accumulation and averaging are handled automatically.

One advantage of filling and using the stack is for user defined <strong>batch</strong> files.  A rudimentary
looping task can be defined in a <strong>batch</strong> file procedure and then the meat of the procedure can execute
a series of commands on each scan in the stack.

For instance, say you wanted to plot and make hard copies of each scan in the stack.  So all you would need to
do, after manually filling the stack:

\code
procedure plotandprint
startstack
	get stackscan
	show
	make
	delay 1000000
	stack /i
endstack
end

\endcode


<h2>Usage:</h2>

The <strong><em>stack</em></strong> command is just the front-end to the stack sub-system.

The argument to stack are:

- stack /e \n
	- Clear out the stack.
		
- stack /f \n
	- Fill the stack with all scan numbers that meet the current search criteria.
		 
- stack /l \n
	- List out the scans in the stack.

- stack /g \n
	- Fetch the scan pointed to by stackscsan.
				
- stack /i \n
	- Bump the stackindx by one.

- stack /r \n
	- Reset stack pointer back to the first scan in the stack.
								

The stack, once filled, contains all scans that would otherwise meet the search criteria used by accum or list.  Once filled
it can be used in a batch file command sequence.  


<h3>Example:</h3>

\code

 procedure stest
 usage: No args for this command
 #
 stack /f
 stack /l
 #
 startstack
    get stackscan
    show
    base
    pause
    stack /i
 endstack
 #
 print acount
 #
 end

\endcode

Lets break down the example above:

- procedure stest
	- The name of this procedure; can be referenced simply by <strong><em>stest</em></strong>
		
- usage
	- Help for how to use batch procedure; none in this example
		
- stack /f
	- Fill the stack with all scans that meet current search criteria.
		
- stack /l
	- Print out a list of selected scans. (For diagnostic purposes)
		
- startstack
	- Label: showing the top of the loop.
		
- get stackscan
	- Fetch the file with the 'stackscan' scan number; ENVIRONMENT::stackscan points to the entry in the @ref lp_stack pointed to by ENVIRONMENT::stackindx.
	- stack /g performs the same operation.
		
- show
	- Show the scan in the plot window.
		
- base
	- Remove a baseline;
		
- pause
	- Wait for user to hit a key; 'q' to quit batch processing.
		
- stack /i
	- Increment the stack counter and set ENVIRONMENT::stackscan to the next scan number in the @ref lp_stack.
		
- endstack
	- End of the loop; Linuxpops will check the ENVIRONMENT::stackdone flag to determine if we are finished.
		
- print acount
	- Print out the number of scans in the @ref lp_stack.
		
- end
	- End of the procedure.
	
<h2>Variables:</h2>

- @ref lp_stack
	- Statically defined floating point array of [@ref MAX_STACK] size.

- ENVIRONMENT::stackscan
	- Scan number pointed to by ENVIRONMENT::stackindx

- ENVIRONMENT::stackindx
	- Current @ref lp_stack index pointer

- ENVIRONMENT::acount
	- Count of the total members in the @ref lp_stack
	
- ENVIRONMENT::stackdone
	- True when end of @ref lp_stack reached

*/
