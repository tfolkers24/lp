Linuxpops(C) >>  # Object Mars
Linuxpops(C) >>  # Set[ 0]: ( 5) Scans: (563.03-571.03); Date: 2021.0218, 22.25 UT; El = 52.6
Linuxpops(C) >>  #                    H-LSB      H-USB      V-LSB      V-USB
Linuxpops(C) >>  #      Rejections:  22.000     22.000     20.000     20.000
Linuxpops(C) >>  #      Rejs Coeff:  99.373     99.373     99.010     99.010
Linuxpops(C) >>  #             Tmb:   1.135      1.417      1.157      1.446
Linuxpops(C) >>  #    Expected Tmb:   1.240      1.608      1.240      1.608
Linuxpops(C) >>  #        TmbSigma:   0.004      0.004      0.006      0.005
Linuxpops(C) >>  #        Beam Eff:  90.910     87.574     92.373     89.036
Linuxpops(C) >>  #  Beam Eff Sigma:   0.336      0.276      0.503      0.335
Linuxpops(C) >>  # ------------------Ignore Below Here----------------------
Linuxpops(C) >>  #            Flux:   27.70      34.61      28.25      35.31
Linuxpops(C) >>  #   Expected Flux:   38.00      49.28      38.00      49.28
Linuxpops(C) >>  #       FluxSigma:    0.10       0.11       0.15       0.13
Linuxpops(C) >>  #        Beam Eff:   72.44      69.78      73.61      70.95
Linuxpops(C) >>  #  
