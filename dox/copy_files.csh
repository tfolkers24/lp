#!/bin/csh
#
if ( "$1" == "" ) then
  echo "Usages: $0 source_dir destination_dir"
  exit 1
endif
#
rsync -av --no-o --no-g $1 $2 
