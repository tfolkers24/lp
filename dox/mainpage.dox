/*
 * This is the Main page of the LinuxPops reference manual, built using
 * doxygen.
 */

/**
@mainpage LinuxPops Spectral Line / Continuum Analysis Package

@section intro Introduction

This complete, from scratch, rewrite of Unipops was our attempt to implement the functionality of the long since
dead and unsupported Condar and a usable replacement for Line.  We have not endeavored to create a one for one
replacement, but a functional replacement.

Many of the commands are missing as are the advanced parser functions and scripting language. Although, a limited
'stack' looping feature and linear scripting is supported. i.e. scripts that contain known commands.

What remains is basic data analysis necessary to reduce continuum and spectral line data. Such as displaying individual
scans, analyzing sky-tips, focusing and five-points. In addition, it can perform beam efficiency measurements, based on
sequence scans, and analyze dual-beam switched beam maps.

On the spectral line portion most functionality of Unipops Line are implemented.

And new to Linuxpops, a robust OTF gridder and mapping analysis package.

\n


@section lp_overview LinuxPops Overview

NOTE:
-----

Linuxpops uses <strong><em>flags</em></strong> for most commands to alter their behavior. If at any time you are unsure of a command, just type the 
command with a <strong><em>/h</em></strong> and a brief help page will display.

Every attempts has been made to remove conflicting flags, but some may have creep in.  

Please let the authors know of any bugs or misbehavior encountered while using Linuxpops.

Report bugs to mailto:tfolkers@arizona.edu


<h2>Environments:</h2>

Introduced in this version is the concept of environments.  That is, you can be working on analyzing one source with all
the search criteria set-up,and then quickly switch to another entirely different environment with one single command. And
then, switch back to the first.  A total of (16) environments are available.

Think of environments as completely separate instances of an analysis program, although much of the resources are shared.

Each environment contains all the search parameters necessary to single out the data sets, scans, sources, frequencies, 
etc. you are interested in, including the ability to ignore bad scans. You can also specify a completely different input 
data-file. Although, environment #0 is meant to be reserved for the on-line file. (Not strictly enforced at this time)

The compete state of your session is keep in a binary memory file that automatically loads at run-time.  You can even 
specify a named memory file if you like; further adding to the versatility.  Individual environment configurations can be 
saved in a text file that can be edited and reloaded when necessary. 

Your command line history is saved and can be reviewed and re-executed much like a shell command parser.  Each observer 
has their own history file.

\n

<h2>Holders</h2>

Another innovation in LinuxPops are data holders (@ref LP_IMAGE_INFO).  These are snap-shots of reduced data.

There are 10 such holders with @ref H0 being the normally active unit.

Data holders can be used to hold complete or intermediate results that can eventually be summed together to form a final result.

Data Holders are how you 'transfer' reduced data and information between environments.

Coupled with the @ref ENVIRONMENT above one can, say, switch to one environment, analyze the data into holder H1, switch 
to another environment reduce the data into holder H2, then sum them together into holder H0 plotting the results.  See 
the @ref lp_holders_command documentation for further explanation.

\n
 
@section invocation Invocation

NOTE: It is recommended that you set your terminal width to 135 characters to better match the programs output; LinuxPops can be verbose at times.

Once you are logged into the observer account, all you need to do is type <strong><em>startlinuxpops</em></strong> to start. 

Start-up options are:

	Linuxpops(C) >> startlinuxpops -h
	Linuxpops(C) >>  #
	Linuxpops(C) >>  # Usage: startlinuxpops [-vlrhgs] [-i init_file] [-g global_memory_file]
	Linuxpops(C) >>  # Where:  -v  Show version info and exit.
	Linuxpops(C) >>  #         -h  Show this help and exit.
	Linuxpops(C) >>  #         -l  Turn on logging. Logfile will be named: linuxpops.$(PID).log
	Linuxpops(C) >>  #         -r  Remove stale LOCKFILE before starting.
	Linuxpops(C) >>  #         -i  Load init_file upon starting.
	Linuxpops(C) >>  #         -g  Use named global_memory_file instead of default.
	Linuxpops(C) >>  #

\n

@section typical Typical Session

Upon start-up, <strong><em>LinuxPops</em></strong> will set-up your default environment and should be ready to analyze
the on-line data.

Type <strong><em>params</em></strong> to show the current search criteria. You can change each of them by using the various Search Criteria Commands. 
See that section under @ref lp_reference.

The main search criteria commands are: <strong><em>freq</em></strong> & <strong><em>source</em></strong>. You can 
refine the search by selecting a single <strong><em>feed</em></strong>, or <strong><em>date</em></strong>.  The 
<strong><em>freq</em></strong> command tells LinuxPops what frequency to select. The <strong><em>delfreq</em></strong> 
specifies a tolerance for frequency matching; default <strong><em>freq</em></strong> +/- 0.500 GHz for continuum 
and +/- 0.001 GHz for spectral line.

Once you are happy with the search criteria, type <strong><em>list</em></strong> to see a list of matching scans. 
<strong><em>List</em></strong> can further be refined by passing it the <strong><em>seq, qk5, foc, bsp, 
etc</em></strong>. argument to only show specific types of scans.

Type <strong><em>accum</em></strong> to analyse the data.  In Continuum mode, this will show all ON/OFF Sequence scans
that match all the criteria that you have selected.  

In Spectral Line mode this will sum any BSP, PS or TP scans.

Convenience functions of the accum command are:

  + @ref lp_c1_command : Accumulate channel #1 only.
  + @ref lp_c2_command : Accumulate channel #2 only.
  + @ref lp_cb_command : Accumulate both channels #1 & #2.
  + And many more.

\n

@section history History

There is a <strong><em>history</em></strong> function in the command line parser so using the up and down cursors will
allow you  to peruse past commands and edit them in the  command-line, then re-executing them.  You can also use history 
manipulations such as '!get' to execute the last <strong><em>get</em></strong> command, '!102' to execute command 102 
again, or '!? grep' to execute the last command that contained the word grep.

\n

@section display Display

Starting LinuxPops will start a companion program called <strong><em>xgraphic</em></strong> which in turn handles all
graphics functions.  It does this by starting it's own instance of the <strong><em>gnuplot</em></strong> program and
handles all communications back and forth with it. Essentially, <strong><em>gnuplot</em></strong> does all the actual 
plotting with <strong><em>xgraphic</em></strong> generating the plotting commands.  

All printing is handled through <strong><em>gnuplot</em></strong>.

\n

@section reference Reference

@ref lp_reference

\n

@section cookbook Cookbook

@ref lp_cookbook

\n

@section license License

<code>
Copyright 2021 Arizona Board of Regents on behalf of the University of Arizona.

For commercial uses, to obtain a license to sell and or sublicense copies of the Software please contact the University of Arizona at Tech Launch Arizona:
lewish@tla.arizona.edu.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal
in the Software without restriction for non-commercial use educational and research purposes, including without limitation the rights to use, copy,
modify, merge, publish, distribute, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The entire above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

1.0	Additional Required Provisions

1.1	Arbitration. The parties agree that if a dispute arises between them concerning this Agreement, the parties may be required to submit the matter to
arbitration pursuant to Arizona law.

1.2	Applicable Law and Venue. This Agreement shall be interpreted pursuant to the laws of the State of Arizona. Any arbitration or litigation between the
Parties shall be conducted in Pima County, ARIZONA, and LICENSEE hereby submits to venue and jurisdiction in Pima County, ARIZONA.

1.3	Non-Discrimination. The Parties agree to be bound by state and federal laws and regulations governing equal opportunity and non-discrimination and
immigration.

1.4	Appropriation of Funds. The Parties recognize that performance by ARIZONA may depend upon appropriation of funds by the State Legislature of
ARIZONA. If the Legislature fails to appropriate the necessary funds, or if ARIZONA’S appropriation is reduced during the fiscal year, ARIZONA may cancel
this Agreement without further duty or obligation. ARIZONA will notify LICENSEE as soon as reasonably possible after it knows of the loss of funds.

1.5	Conflict of Interest. This Agreement is subject to the provisions of A.R.S. 38-511 and other conflict of interest regulations. Within three years
of the EFFECTIVE DATE, ARIZONA may cancel this Agreement if any person significantly involved in initiating, negotiating, drafting, securing, or
creating this Agreement for or on behalf of ARIZONA becomes an employee or consultant in any capacity of LICENSEE with respect to the subject matter of
this Agreement.

</code>
*/
