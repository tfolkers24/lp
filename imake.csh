#!/bin/csh                                                                                                            
#
set files = `find . -name Imakefile -print`
#
foreach d ( $files )
  echo " "
  set dir=`dirname $d`
#
  if ( "$dir" != "./spectral/dataserv" && "$dir" != "./oldsam/sambusd" && "$dir" != "./oldsam/sambus/samwatch") then
    echo "Found: $dir"
    echo " "
    (cd $dir; xmkmf; make depend)
  else
    echo "Skipping $dir"
  endif
#
  echo " "
  echo " "
end
