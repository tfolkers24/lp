
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/file.h>
#include <sys/fcntl.h>
#include <sys/mman.h>

#include "global.h"
#include "object.h"

#include "linuxlib_proto.h"

extern struct GLOBAL_MEMORY *CGM;

struct GLOBAL_MEMORY *gm_map(name, iswrite)
char *name;
int iswrite;
{
  int fd = -1, sz, isrd, iswr=0;
  char zero = 1;
  struct GLOBAL_MEMORY *map;

  sz = 0;

  /* Try to open the file */

  if(access(name, F_OK))   /* access failed, attempt to create it */
  {
    if(!iswrite)
    {
      st_report("gm_map can't create a file when called with no write access");
      return(0);
    }

    if(( fd = open( name, O_CREAT | O_RDWR, 0664 )) < 0)
    {
      st_report("gm_map(): File creation failed");
      return(0);
    }

    /* fill new file with zeros with unix lseek trick */
    lseek(fd, sizeof(struct GLOBAL_MEMORY)-1, SEEK_SET );
    zero = 0;
    write(fd, &zero, 1 );
  }
  else /* File already exists */
  {
    isrd = access(name, R_OK)==0;
    iswr = iswrite && access(name, W_OK)==0;

    if(iswr && isrd) 
    {
      fd = open(name, O_RDWR,0);
    }
    else
    if(isrd)
    {
      fd = open(name, O_RDONLY,0);
    }

    if(fd < 0)
    {
      st_report("gm_map could not open file %s",name);
      return(0);
    }

    sz = lseek(fd, 0l, SEEK_END );

    /* Sanity check.  The file ought to be the expected size. */
    if( sz > 0 && sz != sizeof(struct GLOBAL_MEMORY))
    {

      char *pwd;

      if( !(pwd = getenv("PWD")))
      {
	pwd = "no pwd";
      }

      st_report("gm_map: %s (pwd %s) has %d bytes but expected %d", 
	name, pwd, sz, sizeof(struct GLOBAL_MEMORY));

      return(0);
    }
  }

  /* Map to the file */

  map = (struct GLOBAL_MEMORY *)mmap(0, sizeof(struct GLOBAL_MEMORY), PROT_READ | (iswr?PROT_WRITE:0), MAP_SHARED, fd, 0);

  close(fd);

  if(map == NULL)
  {
    st_report("gm_map could not map to %s", name);
    perror("mmap");

    return 0;
  }

  /* Result is also saved in a static pointer */

  CGM = map;

  return(map);
}


struct OBJECT_GLOBAL_MEMORY *obj_map(name, iswrite)
char *name;
int iswrite;
{
  int fd = -1, sz, isrd, iswr=0;
  char zero = 1;
  struct OBJECT_GLOBAL_MEMORY *map;

  sz = 0;

  /* Try to open the file */

  if(access(name, F_OK))   /* access failed, attempt to create it */
  {
    if(!iswrite)
    {
      st_report("obj_map() can't create a file when called with no write access");
      return(0);
    }

    if(( fd = open( name, O_CREAT | O_RDWR, 0664 )) < 0)
    {
      st_report("obj_map(): File creation failed");
      return(0);
    }

    /* fill new file with zeros with unix lseek trick */
    lseek(fd, sizeof(struct OBJECT_GLOBAL_MEMORY)-1, SEEK_SET );
    zero = 0;
    write(fd, &zero, 1 );
  }
  else /* File already exists */
  {
    isrd = access(name, R_OK)==0;
    iswr = iswrite && access(name, W_OK)==0;

    if(iswr && isrd) 
    {
      fd = open(name, O_RDWR,0);
    }
    else
    if(isrd)
    {
      fd = open(name, O_RDONLY,0);
    }

    if(fd < 0)
    {
      st_report("obj_map() could not open file %s",name);
      return(0);
    }

    sz = lseek(fd, 0l, SEEK_END );

    /* Sanity check.  The file ought to be the expected size. */
    if( sz > 0 && sz != sizeof(struct OBJECT_GLOBAL_MEMORY))
    {
      char *pwd;

      if( !(pwd = getenv("PWD")))
      {
	pwd = "no pwd";
      }

      st_report("obj_map(): %s (pwd %s) has %d bytes but expected %d", name, pwd, sz, sizeof(struct OBJECT_GLOBAL_MEMORY));

      if(!iswrite)
      {
        st_report("obj_map() abort: Called with no write access");
	return(0);	/* Can't create a file if not allowed write access */
      }

      st_report("obj_map(): Erasing this file to create another with the correct length");
            
      if(( fd = open( name, O_CREAT | O_TRUNC | O_RDWR, 0664 )) < 0)
      {
        st_report("obj_map(): File creation failed");
	return(0);
      }

      /* fill new file with zeros with unix lseek trick */

      lseek(fd, sizeof(struct OBJECT_GLOBAL_MEMORY)-1, SEEK_SET );
      zero = 0;
      write(fd, &zero, 1 );
    }
  }

  /* Map to the file */

  map = (struct OBJECT_GLOBAL_MEMORY *)mmap(0, sizeof(struct OBJECT_GLOBAL_MEMORY), PROT_READ | (iswr?PROT_WRITE:0), MAP_SHARED, fd, 0);

  close(fd);

  if(map == NULL)
  {
    st_report("obj_map() could not map to %s", name);
    perror("mmap");

    return 0;
  }

  /* Result is also saved in a static pointer */

  OGM = map;

  return(map);
}
