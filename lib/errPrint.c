#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>
#include <stdarg.h>
#include <math.h>
#include <errno.h>
#include <sys/time.h>
#include <readline/readline.h>
#include <readline/history.h>

#include "defines.h"
#include "ansi_colors.h"

#include "caclib_proto.h"

extern char *prompt;
extern FILE *errPrint_fp;
extern int   stquiet;

int getNumericResponse(const char *fmt, ...)
{
  int     result;
  char   *cp, prompt[256];
  extern char *rl_prompt;
  va_list args;

  strcpy(prompt, rl_prompt);

  fflush(stdin);

  va_start(args, fmt);

  vfprintf(stdout, fmt, args);
  if(errPrint_fp)
  {
    vfprintf(errPrint_fp, fmt, args);
  }

  va_end(args);

  fflush(stdin);

  cp = readline(NULL);

  if(cp)
  {
    if(errPrint_fp)
    {
      fprintf(errPrint_fp, cp);
    }

    if(errPrint_fp)
    {
      fprintf(errPrint_fp, "\n");
    }

    fprintf(stdout, "\n");

    result = atoi(cp);

    rl_set_prompt(prompt);

    return(result);
  }
  else
  {
    if(errPrint_fp)
    {
      fprintf(errPrint_fp, "\n");
    }

    rl_set_prompt(prompt);

    return(-1);
  }

  return(-1);
}


int getYesNoQuit(const char *fmt, ...)
{
  char *cp, tbuf[256];
  va_list args;

  fflush(stdin);

  va_start(args, fmt);

  vfprintf(stdout, fmt, args);
  if(errPrint_fp)
  {
    vfprintf(errPrint_fp, fmt, args);
  }

  va_end(args);

  fflush(stdin);

  cp = fgets(tbuf, 2, stdin);

  if(cp)
  {
    if(errPrint_fp)
    {
      fprintf(errPrint_fp, cp);
    }

    if(*cp == 'y')
    {
      if(errPrint_fp)
      {
        fprintf(errPrint_fp, "\n");
      }

      fprintf(stdout, "\n");

      return(1);
    }
    else
    if(*cp == 'q')
    {
      if(errPrint_fp)
      {
        fprintf(errPrint_fp, "\n");
      }

      fprintf(stdout, "\n");

      return(2);
    }
    else
    if(*cp == 'b')
    {
      if(errPrint_fp)
      {
        fprintf(errPrint_fp, "\n");
      }

      fprintf(stdout, "\n");

      return(3);
    }
  }

  if(errPrint_fp)
  {
    fprintf(errPrint_fp, "\n");
  }

  fprintf(stdout, "\n");

  return(0);
}


/* Print with msg with no newline */
void st_print(const char *fmt, ...)
{
  va_list args;

  va_start(args, fmt);

  if(!stquiet)
  {
    vfprintf(stdout, fmt, args);
  }

  if(errPrint_fp)
  {
    vfprintf(errPrint_fp, fmt, args);
  }

  va_end(args);
}


/* Print with prompt + msg + (1) newline */
void st_report(const char *fmt, ...)
{
  extern int verboseLevel;
  extern int recursiveLevel;

  va_list args;

  if(!stquiet)
  {
    if(verboseLevel & VERBOSE_RECURSIVE)
    {
      fprintf(stdout, "%s #%d ", prompt, recursiveLevel);
    }
    else
    {
      fprintf(stdout, "%s # ", prompt);
    }
  }

  if(errPrint_fp)
  {
    if(verboseLevel & VERBOSE_RECURSIVE)
    {
      fprintf(errPrint_fp, "%s #%d ", prompt, recursiveLevel);
    }
    else
    {
      fprintf(errPrint_fp, "%s # ", prompt);
    }
  }

  va_start(args, fmt);

  if(!stquiet)
  {
    vfprintf(stdout, fmt, args);
  }

  if(errPrint_fp)
  {
    vfprintf(errPrint_fp, fmt, args);
  }

  va_end(args);

  if(!stquiet)
  {
    fprintf(stdout, "\n");
  }

  if(errPrint_fp)
  {
    fprintf(errPrint_fp, "\n");
  }
}


/* Print with prompt + msg + (1) newline */
void st_report_fp(const char *fmt, ...)
{
  va_list args;

  if(!stquiet)
  {
    fprintf(stdout, "%s ", prompt);
  }

  fprintf(errPrint_fp, "%s ", prompt);

  va_start(args, fmt);

  if(!stquiet)
  {
    vfprintf(stdout, fmt, args);
  }

  vfprintf(errPrint_fp, fmt, args);

  va_end(args);

  if(!stquiet)
  {
    fprintf(stdout, "\n");
  }

  fprintf(errPrint_fp, "\n");
}



/* Print with (1) newline + prompt + msg + (2) newline */
void st_warn(const char *fmt, ...)
{
  va_list args;

  if(!stquiet)
  {
    fprintf(stdout, "\n%s: ", prompt);
  }

  if(errPrint_fp)
  {
    fprintf(errPrint_fp, "\n%s: ", prompt);
  }

  va_start(args, fmt);

  if(!stquiet)
  {
    vfprintf(stdout, fmt, args);
  }

  if(errPrint_fp)
  {
    vfprintf(errPrint_fp, fmt, args);
  }

  va_end(args);

  if(!stquiet)
  {
    fprintf(stdout, "\n\n");
  }

  if(errPrint_fp)
  {
    fprintf(errPrint_fp, "\n\n");
  }
}



/* Print with (1) newline + msg + (2) newline */
void st_fail(const char *fmt, ...)
{
  va_list args;

  fprintf(stdout, "\n");
  if(errPrint_fp)
  {
    fprintf(errPrint_fp, "\n");
  }

  va_start(args, fmt);

  vfprintf(stdout, fmt, args);
  if(errPrint_fp)
  {
    vfprintf(errPrint_fp, fmt, args);
  }

  va_end(args);

  fprintf(stdout, "\n\n");
  if(errPrint_fp)
  {
    fprintf(errPrint_fp, "\n\n");
  }
}


int st_perror(msg)
char *msg;
{
  char err[4096];

  fprintf(stdout, "%s # ", prompt);

  if(errPrint_fp)
  {
    fprintf(errPrint_fp, "%s # ", prompt);
  }

  sprintf( err, "%s, %s", msg, strerror(errno) );

  fprintf(stdout, "%s\n", err);

  if(errPrint_fp)
  {
    fprintf(errPrint_fp, "%s\n", err);
  }

  return(0);
}
