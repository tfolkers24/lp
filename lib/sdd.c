#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <gsl/gsl_math.h>

#include "observing_par.h"

#include "scans.h"
#include "global.h"
#include "ansi_colors.h"

#include "caclib_proto.h"
#include "linuxlib_proto.h"

#define BLOCK 512
#define OFFSET(x,f)  ((short)(((int)&x->f - ((int)x))/8 + 1))
#define MAX_SCANS     16384  /* number of directory entrys, for new file */

int            lastFileSize;
int            numOfSources;
extern int verboseLevel;

struct SOURCES sources[MAX_SOURCE_LIST];
static struct  BOOTSTRAP  boot;

extern struct  HEADER *sa;

static int lp_pdopen(char *name, int mode);
static int lp_findNumOfSources(void);
static int lp_findFreqInSource(struct SOURCES *p, double f);
static int lp_addFreqToSource(struct SOURCES *p, double f);
static int lp_addSourceToList(char *name, int mode, double freq);
//static int lp_RfreqSort(struct SOURCES *a, struct SOURCES *b);
static int lp_freqSort(struct SOURCES *a, struct SOURCES *b);
static float lp_findNextGoodPoint(float *d, int indx, int l);
static float lp_findPrevGoodPoint(float *d, int indx, int l);
//static int lp_checkBounds(float *d, int l, int autobad);
static int lp_blocks(int b);
static int lp_padd(char *s, int n);
static double lp_index_freqres(struct HEADER *h);
static double lp_index_restfreq(struct HEADER *h);
static int lp_obsmode(char *o);
static int lp_pos_code(char *s);
static int lp_write_dir(int fd, struct DIRECTORY *d, int len);
//static int lp_write_boot(int fd, struct BOOTSTRAP *b, int len);
static int lp_close(int *fd);


float *otfData = NULL;


static int lp_close(fd)
int *fd;
{
  if(verboseLevel & VERBOSE_FILEOPS)
  {
    st_report("Closing: fd = %d", *fd);
  }

  close(*fd);

  *fd = 0;

  return(0);
}

int checkConstCal(h)
struct HEADER *h;
{
  /* fix the 99999 tsys when in CONST cal mode */
  if(!strstr(h->obsmode, "CAL"))
  {
    if(!strncmp(h->typecal, "CONST", 5))
    {
      if(h->stsys > 2000.0 || h->stsys < 50.0)
      {
        h->stsys = h->tcal / 2.0;
      }
    }
  }

  return(0);
}


static int lp_pdopen(name, mode)
char *name;
int mode;
{
  int fd;
  struct stat statb;

  if( (fd = open(name, mode, 0666 ))<0)
  {
    st_fail("lp_pdopen(): Error opening: '%s'", name );
    return(-1);
  }

  if(mode == O_RDONLY) /* Is this the online file ?? */
  {
    fstat(fd, &statb);
    lastFileSize = statb.st_size;
  }

  if(verboseLevel & VERBOSE_FILEOPS)
  {
    st_report("Opening: %s, fd = %d", name, fd);
  }

  return(fd);
}


int findSize(env)
struct ENVIRONMENT *env;
{
  int fd, n;

  if( (fd = lp_pdopen(env->filename, O_RDONLY))<0)
  {
    return(-1);
  }

  if( read( fd, (char *)&boot, sizeof(struct BOOTSTRAP))<0)
  {
    st_perror("read boot");
    lp_close(&fd);

    return(-1);
  }

  if(boot.version != 1)
  {
    st_fail("File not of correct vintage; Consider using 'sdd convert'");

    return(-1);
  }

  lp_close(&fd);

  n = boot.num_entries_used+1;

  if(sa)
  {
    free(sa);
  }

  if( (sa = (struct HEADER *)calloc(n, sizeof(struct HEADER))) == NULL)
  {
    fprintf(stderr,"Error in findSize( malloc() )\n");

    return(-1);
  }

  return(0);
}


static int lp_findNumOfSources()
{
  int i;
  static struct SOURCES *p;

  numOfSources = 0;

  p = &sources[0];
  for(i=0;i<MAX_SOURCE_LIST;i++,p++)
  {
    if(p->used)
    {
      numOfSources++;
    }
  }

  return(0);
}


struct SOURCES *findSourceInList(name)
char *name;
{
  int i;
  static struct SOURCES *p;

  p = &sources[0];

  for(i=0;i<MAX_SOURCE_LIST;i++,p++)
  {
    if(p->used && !strcmp(name, p->name))
    {
      return(p);
    }
  }

  return((struct SOURCES *)NULL);
}


static int lp_findFreqInSource(p, f)
struct SOURCES *p;
double f;
{
  int i;

  for(i=1;i<MAX_FREQS;i++)
  {
    if(p->freqs[i].used)
    {
      if(fabs(f - p->freqs[i].freq) < 0.1) /* Compare in GHz */
      {
        return(i);
      }
    }
  }

  return(0);
}


static int lp_addFreqToSource(p, f)
struct SOURCES *p;
double f;
{
  int found, i;

  found = lp_findFreqInSource(p, f);

  if(found)
  {
    p->freqs[found].count++;

    return(0);
  }

  for(i=1;i<MAX_FREQS;i++)
  {
    if(!p->freqs[i].used)
    {
      p->freqs[i].used  = 1;
      p->freqs[i].count = 1;
      p->freqs[i].freq  = f;

      p->nfreqs++;

      return(0);
    }
  }

  return(0);
}



static int lp_addSourceToList(name, mode, freq)
char *name;
int mode;
double freq;
{
  int i;
  struct SOURCES *p;

  p = findSourceInList(name);

  if(p) /* Already have a source; Bump it's count */
  {
    p->num++;

    lp_addFreqToSource(p, freq);

    return(0);
  }

 /* Look for an empty one */
  p = &sources[0];
  for(i=0;i<MAX_SOURCE_LIST;i++,p++)
  {
    if(!p->used)
    {
      strcpy(p->name, name);

      p->num++;
      p->used = 1;
      p->mode = mode;

      lp_addFreqToSource(p, freq);

      return(0);
    }
  }


  return(1);
}


int clearSourceList()
{
  bzero((char *)&sources, sizeof(sources));

  return(0);
}


int showSourceList()
{
  int i, n=0;
  struct SOURCES *p;

  p = &sources[0];

  st_print("  Source      Num-of-Scans\n\n");

  for(i=0;i<MAX_SOURCE_LIST;i++,p++)
  {
    if(p->used)
    {
      st_print("%16.16s  %4d\n", p->name, p->num);
      n += p->num;
    }
  }

  st_print("\n Total ->        %5d\n", n);

  return(0);
}


#ifdef TOMMIE
/* Reverse sort */
static int lp_RfreqSort(a, b)
struct SOURCES *a;
struct SOURCES *b;
{
  if(a->nfreqs > b->nfreqs)
  {
    return(1);
  }
  else
  if(a->nfreqs < b->nfreqs)
  {
    return(-1);
  }

  return(0);
}
#endif


static int lp_freqSort(a, b)
struct SOURCES *a;
struct SOURCES *b;
{
  if(a->nfreqs > b->nfreqs)
  {
    return(-1);
  }
  else
  if(a->nfreqs < b->nfreqs)
  {
    return(1);
  }

  return(0);
}


int doSummary()
{
  int i, n=0, j, k, found /*, max */;
  struct SOURCES *p;

#ifdef __DARWIN__
  qsort(sources, MAX_SOURCE_LIST, sizeof(struct SOURCES), lp_freqSort);
#else
  qsort(sources, MAX_SOURCE_LIST, sizeof(struct SOURCES), (__compar_fn_t)lp_freqSort);
#endif

  p = &sources[0];

//  max = p->nfreqs;

  st_print("       Source    Scans ");

  for(i=0;i<5;i++)
  {
    st_print("    Freq     Scans");
  }

  st_print("\n\n");

  for(i=0;i<MAX_SOURCE_LIST;i++,p++)
  {
    if(p->used)
    {
      found = 0;

      st_print("%16.16s   %4d", p->name, p->num-1); /* -1, I don't know why?? */

      n += p->num;

      j = 0;
      for(k=0;k<MAX_FREQS;k++)
      {
        if(p->freqs[k].used)
        {
          if(j>0 && !(j%5))
          {
            st_print("\n                       ");
          }

          found = 1;
          st_print("  %10.6f  %4d", p->freqs[k].freq / 1000.0, p->freqs[k].count);
          j++;
        }
      }

      if(found)
      {
        st_print("\n");
      }
    }
  }

  st_print("\n        Total ->       %6d\n", n);

  return(0);
}


int doSummaryOfFreqs2Buf(source, buf, rev)
char *source;
char *buf;
int rev;
{
  int i, n=0, k, found = 0;
  struct SOURCES *p;
  char tbuf[256];

#ifdef __DARWIN__
  qsort(sources, MAX_SOURCE_LIST, sizeof(struct SOURCES), lp_freqSort);
#else
  qsort(sources, MAX_SOURCE_LIST, sizeof(struct SOURCES), (__compar_fn_t)lp_freqSort);
#endif

  buf[0] = '\0';

  if(rev) /* Show it in reverse order */
  {
    p = &sources[MAX_SOURCE_LIST-1];

    for(i=0;i<MAX_SOURCE_LIST;i++, p--) /* Same number of interations */
    {
      if(p->used)
      {
        n += p->num;

        for(k=0;k<MAX_FREQS;k++)
        {
          if(p->freqs[k].used)
          {
		/*  Leave the space before name so sources with a '-' in the front of their name
		    don't get interpeted as a switch option by zenity */
            sprintf(tbuf, "' %s' '%10.6f' '%4d' ", p->name, p->freqs[k].freq / 1000.0, p->freqs[k].count);
            strcat(buf, tbuf);
            found++;
          }
        }
      }
    }
  }
  else
  {
    p = &sources[0];

    for(i=0;i<MAX_SOURCE_LIST;i++,p++)
    {
      if(p->used)
      {
        n += p->num;

        for(k=0;k<MAX_FREQS;k++)
        {
          if(p->freqs[k].used)
          {
		/*  Leave the space before name so sources with a '-' in the front of their name
		    don't get interpeted as a switch option by zenity */
            sprintf(tbuf, "' %s' '%10.6f' '%4d' ", p->name, p->freqs[k].freq / 1000.0, p->freqs[k].count);
            strcat(buf, tbuf);
            found++;
          }
        }
      }
    }
  }

  return(found);
}



int findScans(env)
struct ENVIRONMENT *env;
{
  int fd, max, dirl, bytesinindex, bytesused, num, j = 0;
  int sizeofDIR, first = 1;
  struct DIRECTORY  dir;
  struct HEADER     head;
  char object[80];

  clearSourceList();

  sizeofDIR = sizeof(struct DIRECTORY);

  if( (fd = lp_pdopen(env->filename, O_RDONLY))<0)
  {
    return(-1);
  }

  if(read( fd, (char *)&boot, sizeof(struct BOOTSTRAP))<0)
  {
    st_perror("read boot");
    lp_close(&fd);

    return(-1);
  }

  /* check bootstrap block */

  if( sizeof(struct DIRECTORY) != boot.bytperent)
  {
    fprintf(stderr,"directory sizes dont match\n");
    lp_close(&fd);

    return(-1);
  }

  bytesinindex = boot.num_index_rec * boot.bytperrec;
  bytesused = boot.num_entries_used * boot.bytperent;

  if( bytesinindex < bytesused+sizeof(struct DIRECTORY))
  {
    fprintf(stderr,"Directory structure of %s is full.\n", env->filename );
    lp_close(&fd);

    return(-1);
  }

  if( boot.typesdd || !boot.version)
  {
    fprintf(stderr, "SDD version or type of %s is invalid.\n", env->filename );
    lp_close(&fd);

    return(-1);
  }

  if(boot.bytperent != sizeof(struct DIRECTORY))
  {
    fprintf(stderr,"File %s has faulty direcory size\n", env->filename );
    lp_close(&fd);

    return(-1);
  }

  if( boot.num_entries_used <= 0)
  {
    fprintf(stderr,"File %s is empty\n", env->filename);
    lp_close(&fd);

    return(-1);
  }

  num = 0;
  while(num < boot.num_entries_used)
  {
    dirl = num * boot.bytperent + boot.bytperrec;
    lseek( fd, (off_t)dirl, SEEK_SET );

    if(read( fd, (char *)&dir, sizeofDIR ) != sizeofDIR)
    {
      st_perror("readdir");
      lp_close(&fd);

      return(-1);
    }

     /* It is possible that some files have holes in them; i.e. save files.
	Check here */
    if(dir.start_rec == 0 || dir.end_rec == 0)
    {
      num++;
      continue;
    }

    /* Check that this scan is of the correct type before
       wasting anymore time on this one */
    if(env->mode == MODE_CONT)
    {
      if((dir.obsmode & 0x01ff) == OTF)
      {
        num++;
        continue;
      }
    }
    else
    if(env->mode == MODE_SPEC)
    {
      if((dir.obsmode & 0x01ff) == OTF)
      {
        num++;
        continue;
      }
    }
    else
    if(env->mode == MODE_OTF)
    {
    }

    max = boot.bytperrec * (dir.start_rec-1);
    lseek( fd, (off_t)max, SEEK_SET );

    if(read(fd, (char *)&head, sizeof(struct HEADER) )<0)
    {
      st_perror("read header");
      lp_close(&fd);

      return(-1);
    }

    checkConstCal(&head);

    if(env->mode == MODE_CONT)
    {
      if(!strncmp(head.obsmode, "LINE", 4))
      {
        num++;
        continue;
      }
    }
    else
    if(env->mode == MODE_SPEC)
    {
      if(!strncmp(head.obsmode, "CONT", 4))
      {
        num++;
        continue;
      }

	/* Check if this is a TP OFF associated with a OTF Map */
      if(!strncmp(head.obsmode, "LINETPMF", 8))
      {
        if(fabs(head.openpar[5] - 6666.0) < 1.0) /* Check for marker */
        {
          num++;
          continue;
        }
      }
    }
    else
    if(env->mode == MODE_OTF)
    {
      if(!strncmp(head.obsmode, "CONT", 4))
      {
        num++;
        continue;
      }
    }


    strncpy(object, head.object, 16);
    object[16] = '\0';
    strcmprs(object);

    if(!strncmp(head.obsmode, "CONT", 4)) /* Look in obsfreq for continuum */
    {
      if(head.restfreq < 11000.0)
      {
        lp_addSourceToList(object, dir.obsmode, head.obsfreq);
      }
      else
      {
        lp_addSourceToList(object, dir.obsmode, head.restfreq);
      }
    }
    else
    {
      lp_addSourceToList(object, dir.obsmode, head.restfreq);
    }

    if(first)
    {
      if(!strncmp(head.telescop, "ARO-HHT", 7))
      {
        st_report("Data File is SMT Data");
        env->issmt = 1;
      }
      else
      if(!strncmp(head.telescop, "NRAO 12M", 8) || !strncmp(head.telescop, "ARO-12M", 7))
      {
        st_report("Data File is 12M Data");
        env->issmt = 0;
      }
      else
      {
        st_warn("Data File is Unknown Data: %8.8s", head.telescop);
        env->issmt = 0;
      }
      
      first = 0;
    }

    head.align3[0] = (double)num; /* Save the directory entry here */

    bcopy((char *)&head, (char *)&sa[j], sizeof(struct HEADER));

    num++;
    j++;

    if(num > MAX_SCANS)
    {
      fprintf(stderr, "File %s contains too many scans\n", env->filename);

      lp_findNumOfSources();

      return(num);
    }
  }

  lp_close(&fd);

  lp_findNumOfSources();

  return(j);
}



static float lp_findNextGoodPoint(d, indx, l)
float *d;
int indx, l;
{
  int i;
  float good=0.0;

  for(i=indx;i<l;i++)
  {
    if(d[i] > 1e-20 || d[i] < 1e-21)
    {
      good = d[i];
      break;
    }
  }

  return(good);
}

  

static float lp_findPrevGoodPoint(d, indx, l)
float *d;
int indx, l;
{
  int i;
  float good=0.0;

  for(i=indx;i>0;i--)
  {
    if(d[i] > 1e-20 || d[i] < 1e-21)
    {
      good = d[i];
      break;
    }
  }

  return(good);
}


int lp_checkBounds(d, l, autobad)
float *d;
int l, autobad;
{
  int i, j = 0;
  float next, prev;
  double avg = 0.0;
  extern struct ENVIRONMENT *env;

  if(env->oob_val)
  {
    avg = env->oob_val;
  }
  else
  {
    for(i=0;i<l;i++)
    {
      if(gsl_isnan( (double)d[i]))
      {
        continue;
      }

      if(gsl_isinf( (double)d[i]))
      {
        d[i] = 0.0;
        continue;
      }

      if(fabs((double)d[i]) > 1e9)
      {
        continue;
      }

      avg += (double)d[i];
      j++;
    }

    if(j)
    {
      avg /= (double)j;
    }
  }

  for(i=0;i<l;i++)
  {
    if(gsl_isnan( (double)d[i]))
    {
      st_print("Chan %d was NAN\n", i);
      d[i] = avg;
    }

    if(gsl_isinf( (double)d[i]))
    {
      st_print("Chan %d was INF\n", i);
      d[i] = avg;
    }

    if(fabs((double)d[i]) > 1e9)
    {
      st_print("Chan %d was OOB\n", i);
      d[i] = avg;
    }

    /* Automatically remove bad channels */
    /* I readly need a better look up for good channels */
    if(autobad && (d[i] < 1e-20 && d[i] > 1e-21))
    {
      if(autobad > 1) /* Verbose ? */
      {
        st_print("Smoothing Bad Channel %d\n", i);
      }

      if(i == 0) /* First channel ?? */
      {
        next = lp_findNextGoodPoint(d, i, l);

        d[i] = next;
      }
      else
      if(i == l-1)  /* Last channel ?? */
      {
        prev = lp_findPrevGoodPoint(d, i, l);

        d[i] = prev;
      }
      else
      {
        next = lp_findNextGoodPoint(d, i, l);
        prev = lp_findPrevGoodPoint(d, i, l);

	d[i] = (prev + next) / 2.0; /* Average them */
      }
    }

  }

  return(0);
}


int checkForScaling(d, h)
float *d;
struct HEADER *h;
{
  int   i, feed;
  float scale;
  extern int setColor(int);
  extern struct ENVIRONMENT *env;

  if(!env->use_scaling)
  {
    return(0);
  }

  if(!strncmp(h->obsmode, "LINEPCAL", 8))
  {
    return(0);
  }

  feed = getFeed(h->scan);

  switch(feed)
  {
    case 1: scale = env->data_scale_1; break;
    case 2: scale = env->data_scale_2; break;
    case 3: scale = env->data_scale_3; break;
    case 4: scale = env->data_scale_4; break;

    default: return(0); break;
  }

  if(scale == 0.0)
  {
    return(0);
  }

  setColor(ANSI_RED);
  st_report("Scaling Scan %.2f by %f", h->scan, scale);
  setColor(ANSI_RESET);

  h->tcal  /= scale;
  h->stsys /= scale;

  for(i=0;i<h->noint;i++)
  {
    d[i] /= scale;
  }

  return(0);
}




int read_scan(scan, filename, data, head, chk )
float scan;
char *filename;
float *data;
struct HEADER *head;
int chk;
{
  int fd, dirl, bytesinindex, bytesused, num, max;
  int l, sizeofDIR;
  struct DIRECTORY  dir;
  extern struct ENVIRONMENT *env;

  sizeofDIR = sizeof(struct DIRECTORY);

  if( (fd = lp_pdopen(filename, O_RDONLY))<0)
  {
    return(-1);
  }

  if( read( fd, (char *)&boot, sizeof(struct BOOTSTRAP))<0)
  {
    st_perror("read boot");
    lp_close(&fd);

    return(-1);
  }

  /* check bootstrap block */

  if( sizeof(struct DIRECTORY) != boot.bytperent)
  {
    fprintf(stderr,"directory sizes dont match\n");
    lp_close(&fd);

    return(-1);
  }

  bytesinindex = boot.num_index_rec * boot.bytperrec;
  bytesused = boot.num_entries_used * boot.bytperent;

  if( bytesinindex < bytesused+sizeof(struct DIRECTORY))
  {
    fprintf(stderr,"Directory structure of %s is full.\n", filename );
    lp_close(&fd);

    return(-1);
  }

  if( boot.typesdd || !boot.version)
  {
    fprintf(stderr, "SDD version or type of %s is invalid.\n", filename );
    lp_close(&fd);

    return(-1);
  }

  if(boot.bytperent != sizeof(struct DIRECTORY))
  {
    fprintf(stderr,"File %s has faulty direcory size\n", filename );
    lp_close(&fd);

    return(-1);
  }

  if(boot.num_entries_used <= 0)
  {
    fprintf(stderr,"File %s is empty\n", filename);
    lp_close(&fd);

    return(-1);
  }

  if(scan < 0.0)
  {
    dirl = boot.bytperrec;
    lseek( fd, (off_t)dirl, SEEK_SET );

    if(read( fd, (char *)&dir, sizeofDIR ) != sizeofDIR)
    {
      st_perror("readdir");
      lp_close(&fd);

      return(-1);
    }

     /* It is possible that some files have holes in them; i.e. save files.
	Check here */
    if(dir.start_rec == 0 || dir.end_rec == 0)
    {
      st_fail("Directory Structure Empty for scan: %.2f", scan);
      lp_close(&fd);

      return(-1);
    }

    dirl = (boot.num_entries_used-1) * boot.bytperent + boot.bytperrec;
    lseek( fd, (off_t)dirl, SEEK_SET );

    if(read( fd, (char *)&dir, sizeofDIR ) != sizeofDIR)
    {
      st_perror("readdir");
      lp_close(&fd);

      return(-1);
    }
  }
  else
  if(scan > 0.0)
  {
    if(env->read_dir == READ_REVERSE)  /* default way */
    {
	/* CHANGE: Start at the end and read backward looking for scan */
	/*         That way, you find the most recent scan             */
      num = boot.num_entries_used;

      while(num >= 0)
      {
        dirl = num * boot.bytperent + boot.bytperrec;
        lseek( fd, (off_t)dirl, SEEK_SET );

        if(read( fd, (char *)&dir, sizeofDIR ) != sizeofDIR)
        {
          st_perror("readdir");
          lp_close(&fd);

          return(-1);
        }

     /* It is possible that some files have holes in them; i.e. save files.
	Check here */
        if(dir.start_rec == 0 || dir.end_rec == 0)
        {
          num--;
          continue;
        }

        if(fabs(scan-dir.scan) < 0.005)
        {
          break;
        }

        num--;
      }

      if(num == -1)
      {
        fprintf(stderr,"Scan %7.2f NOT Found\n", scan);
        lp_close(&fd);

        return(-1);
      }
    }
    else
    {
      num = 0;

      while(num < boot.num_entries_used)
      {
        dirl = num * boot.bytperent + boot.bytperrec;

        lseek( fd, (off_t)dirl, SEEK_SET );

        if(read( fd, (char *)&dir, sizeofDIR ) != sizeofDIR )
        {
          st_perror("readdir");
          lp_close(&fd);
          return(-1);
        }

     /* It is possible that some files have holes in them; i.e. save files.
	Check here */
        if(dir.start_rec == 0 || dir.end_rec == 0)
        {
          num++;
          continue;
        }

        if(fabs(scan-dir.scan) < 0.005)
        {
          break;
        }

        num++;
      }

      if(num >= boot.num_entries_used)
      {
        fprintf(stderr,"Scan %7.2f NOT Found\n", scan);
        lp_close(&fd);

        return(-1);
      }
    }
  }

  max = boot.bytperrec * (dir.start_rec-1);
  lseek( fd, (off_t)max, SEEK_SET );

  if(read(fd, (char *)head, sizeof(struct HEADER) )<0)
  {
    st_perror("read header");
    lp_close(&fd);

    return(-1);
  }

  head->align3[0] = (double)num; /* Save the directory entry here */

  checkConstCal(head);

  if(scan < 0.0)
  {
    lp_close(&fd);

    return((int)head->scan);
  }

  if(chk == -1) /* caller just wanted headers, no data */
  {
    lp_close(&fd);

    return((int)head->scan);
  }

  /* Do NOT read OTF data into these passed arrays; 
     They are TOO small for OTF data, use malloc() instead.

     Same applies to SFOCUS scans, which are LINEBSP but
     contain multiple spectra; i.e. 9; So an AROWS spec-focus
     will contain 6400 * 9 = 57600 channels; bigger that
     the 32K buffer that yplot1 is. */
  if(!strncmp(head->obsmode, "LINEOTF", 7) || head->datalen > (MAX_CHANS * sizeof(float))) 
  {
    st_report("Scan is of OTF type, or too big, Using Internal Data Array");

    bzero( (char *)data, MAX_CHANS * sizeof(float) );		/* Clear this out anyway */

    if(otfData)
    {
      free(otfData);
    }

    if( (otfData = (float *)malloc(head->datalen)) == NULL)
    {
      fprintf(stderr, "Error in otfData( malloc() )\n");

      lp_close(&fd);

      return(-1);
    }

    bzero( (char *)otfData, head->datalen);

    if( read( fd, (char *)otfData, (unsigned)head->datalen) <0)
    {
      st_perror("read data");
      fprintf(stderr,"Error reading scan %7.2f data\n", scan);

      lp_close(&fd);

      return(-1);
    }
  }
  else
  {
    bzero( (char *)data, MAX_CHANS * sizeof(float) );

    if( read( fd, (char *)data, (unsigned)head->datalen) <0)
    {
      st_perror("read data");
      fprintf(stderr,"Error reading scan %7.2f data\n", scan);
      lp_close(&fd);

      return(-1);
    }
                                /* now check for abnormal float in data array */
    l = (unsigned)head->datalen / sizeof(float);

    lp_checkBounds(data, l, chk);

    checkForScaling(data, head);
  }

  lp_close(&fd);

  return((int)head->scan);
}


int read_nsave(nsave, filename, data, head, chk )
int nsave;
char *filename;
float *data;
struct HEADER *head;
int chk;
{
  int fd, dirl, bytesinindex, bytesused, num, max;
  int l, sizeofDIR;
  struct DIRECTORY  dir;

  sizeofDIR = sizeof(struct DIRECTORY);

  if( (fd = lp_pdopen(filename, O_RDONLY))<0)
  {
    return(-1);
  }

  if( read( fd, (char *)&boot, sizeof(struct BOOTSTRAP))<0)
  {
    st_perror("read boot");
    lp_close(&fd);

    return(-1);
  }

  /* check bootstrap block */

  if( sizeof(struct DIRECTORY) != boot.bytperent)
  {
    fprintf(stderr,"directory sizes dont match\n");
    lp_close(&fd);

    return(-1);
  }

  bytesinindex = boot.num_index_rec * boot.bytperrec;
  bytesused = boot.num_entries_used * boot.bytperent;

  if( bytesinindex < bytesused+sizeof(struct DIRECTORY))
  {
    fprintf(stderr,"Directory structure of %s is full.\n", filename );
    lp_close(&fd);

    return(-1);
  }

  if( boot.typesdd || !boot.version)
  {
    fprintf(stderr, "SDD version or type of %s is invalid.\n", filename );
    lp_close(&fd);

    return(-1);
  }

  if(boot.bytperent != sizeof(struct DIRECTORY))
  {
    fprintf(stderr,"File %s has faulty direcory size\n", filename );
    lp_close(&fd);

    return(-1);
  }

  if(boot.num_entries_used <= 0)
  {
    fprintf(stderr,"File %s is empty\n", filename);
    lp_close(&fd);

    return(-1);
  }

  if(nsave >= 0)
  {
    num = 0;

    while(num < boot.num_entries_used)
    {
      dirl = num * boot.bytperent + boot.bytperrec;
      lseek( fd, (off_t)dirl, SEEK_SET );

      if(read( fd, (char *)&dir, sizeofDIR ) != sizeofDIR)
      {
        st_perror("readdir");
        lp_close(&fd);

        return(-1);
      }

     /* It is possible that some files have holes in them; i.e. save files.
	Check here */
      if(dir.start_rec == 0 || dir.end_rec == 0)
      {
        num++;
        continue;
      }

      if( num == nsave )
      {
        break;
      }

      num++;
    }

    if( num >= boot.num_entries_used)
    {
      st_fail("NSAVE %d NOT Found", nsave);
      lp_close(&fd);

      return(-1);
    }
  }

  max = boot.bytperrec * (dir.start_rec-1);
  lseek( fd, (off_t)max, SEEK_SET );

  if(read(fd, (char *)head, sizeof(struct HEADER) )<0)
  {
    st_perror("read header");
    lp_close(&fd);

    return(-1);
  }

  checkConstCal(head);

  bzero( (char *)data, MAX_CHANS * sizeof(float) );

  if( read( fd, (char *)data, (unsigned)head->datalen) <0)
  {
    st_perror("read data");
    st_fail("Error reading nsave %d data\n", nsave);
    lp_close(&fd);

    return(-1);
  }

  head->align3[0] = (double)num; /* Save the directory entry here */

                                /* now check for abnormal float in data array */
  l = (unsigned)head->datalen / sizeof(float);

  lp_checkBounds(data, l, chk);

  lp_close(&fd);

  return((int)head->scan);
}


int getFeed(scan)
double scan;
{
  int feed;
  double tmp;

  tmp = scan - floor(scan);

  if(tmp > 0.1) /* is this the MAC data; .11 .12 */
  {
    tmp -= 0.1;
  }

  tmp *= 100.0;
  feed = (int)(tmp + 0.25);

  return(feed);
}


int getFeedFromBackend(b)
char *b;
{
  int feed = 0;

  if(strstr(b, "HU"))
  {
    feed = 1;
  }
  else
  if(strstr(b, "VU"))
  {
    feed = 3;
  }
  else
  if(strstr(b, "HL"))
  {
    feed = 0;
  }
  else
  if(strstr(b, "VL"))
  {
    feed = 2;
  }

  return(feed);
}


int lp_write_sdd(name, h, d)
char *name;
struct HEADER *h;
char *d;
{
  int  outFD;
  long headlen, datalen, dirl, start;
  long datab, maxb, bytesinindex, bytesused;
  struct DIRECTORY dir;
  char *z;
  time_t itime;

  if((outFD = lp_pdopen(name, O_RDWR)) < 0)
  {
    return(-1);
  }

  if(verboseLevel & VERBOSE_FILEOPS)
  {
    st_report("FD = %d", outFD);
  }

  bzero(&boot, sizeof(struct BOOTSTRAP));

  if( read( outFD, &boot, sizeof(struct BOOTSTRAP))<0 )
  {
    st_perror("read boot");
    lp_close(&outFD);

    return(-2);
  }

  if(verboseLevel & VERBOSE_FILEOPS)
  {
    printLastBoot();
  }

  /* update bootstrap block */

  if( sizeof(struct DIRECTORY) != boot.bytperent)
  {
    st_report("directory sizes dont match");
    lp_close(&outFD);

    return(-4);
  }

/* There could be holes but I'm not going to check for that */

  bytesinindex = boot.num_index_rec * boot.bytperrec;
  bytesused = boot.num_entries_used * boot.bytperent;

  if( bytesinindex < bytesused+sizeof(struct DIRECTORY))
  {
    st_fail("Directory structure of %s is full.\n", name );
    lp_close(&outFD);

    return(-5);
  }

  if( boot.typesdd || !boot.version)
  {
    st_fail("SDD version or type of %s is invalid.\n", name );
    lp_close(&outFD);

    return(-5);
  }

  headlen            = sizeof(struct HEADER);
  h->headlen         = (double)headlen;
  h->headlen         = h->headlen;
  datalen            = (long)h->datalen;
  datab              =  lp_blocks( headlen + datalen );
  boot.num_data_rec += datab;
  maxb               = boot.num_data_rec + boot.num_index_rec;

  if( maxb < 0)
  {
    st_fail("Data file %s is full.\n", name );
    lp_close(&outFD);

    return(-5);
  }

  if( maxb >= 1717986918 )
  {
    st_fail("Data file is getting full. %d blocks left\n", 2147483647-maxb);
  }

  boot.counter++;
  boot.num_entries_used++;

  if( lseek( outFD, 0l, SEEK_SET)<0)
  {
    st_perror("lseek boot");
    lp_close(&outFD);

    return(-7);
  }

  if( write(outFD, &boot, sizeof(struct BOOTSTRAP)) != sizeof(struct BOOTSTRAP))
  {
    st_perror("write boot");
    lp_close(&outFD);

    return(-3);
  }

  /* update new directory */

  dirl = (boot.num_entries_used-1) * boot.bytperent + boot.bytperrec;

  if( lseek( outFD, dirl, SEEK_SET ) < 0) {
    st_perror("lseek dir");
    lp_close(&outFD);

    return(-7);
  }

  bzero( &dir, sizeof(struct DIRECTORY) );

  dir.start_rec = boot.num_index_rec + boot.num_data_rec - datab + 1;
  dir.end_rec   = boot.num_index_rec + boot.num_data_rec;
  dir.h_coord   = h->xsource;
  dir.v_coord   = h->ysource;

  strncpy( dir.source, h->object, 16 );
  lp_padd(dir.source, 16);

  dir.scan      = h->scan;
  dir.freq_res  = lp_index_freqres(h);
  dir.rest_freq = lp_index_restfreq(h);
  dir.lst       = h->lst;
  dir.ut        = h->utdate;
  dir.obsmode   = lp_obsmode(h->obsmode);
  dir.phase_rec = -1;
  dir.pos_code  = lp_pos_code(h->coordcd);

  if( lp_write_dir( outFD, &dir, boot.bytperent)<0)
  {
    st_perror("write dir");
    lp_close(&outFD);

    return(-3);
  }

  /* set header offsets */

  h->headcls = (short)13;
  h->oneptr =  (short)OFFSET(h, headlen);
  h->twoptr =  (short)OFFSET(h, xpoint);
  h->thrptr =  (short)OFFSET(h, utdate);
  h->fourptr = (short)OFFSET(h, epoch);
  h->fiveptr = (short)OFFSET(h, tamb);
  h->sixptr =  (short)OFFSET(h, scanang);
  h->sevptr =  (short)OFFSET(h, bfwhm);
  h->eigptr =  (short)OFFSET(h, appeff);
  h->nineptr = (short)OFFSET(h, synfreq);
  h->tenptr =  (short)OFFSET(h, openpar[0]);
  h->elvptr =  (short)OFFSET(h, current_disk);
  h->twlptr =  (short)OFFSET(h, obsfreq);
  h->trnptr =  (short)OFFSET(h, nostac);

/* padd strings with spaces */

  lp_padd(h->obsid,     8);
  lp_padd(h->observer, 16);
  lp_padd(h->telescop,  8);
  lp_padd(h->projid,    8);
  lp_padd(h->object,   16);
  lp_padd(h->obsmode,   8);
  lp_padd(h->frontend,  8);
  lp_padd(h->backend,   8);
  lp_padd(h->precis,    8);
  lp_padd(h->pt_model,  8);
  lp_padd(h->cl11type,  8);
  lp_padd(h->rx_info,  16);
  lp_padd(h->coordcd,   8);
  lp_padd(h->frame,     8);
  lp_padd(h->veldef,    8);
  lp_padd(h->typecal,   8);

  /* fill the header stop "burn_time" with the time it was written. */
  itime = time(NULL);
  h->burn_time = (double)itime;

  /* write data */
  start = (dir.start_rec-1) * boot.bytperrec;
  if( lseek(outFD, start, SEEK_SET)<0)
  {
    st_perror("lseek data");
    lp_close(&outFD);

    return(-7);
  }

  if( write( outFD, h, headlen)<0)
  {
    st_perror("write head");
    lp_close(&outFD);

    return(-3);
  }

  if(write( outFD, d, datalen)<0)
  {
    st_perror("write data");
    lp_close(&outFD);

    return(-3);
  }

  /* complete block */
  dirl = (datalen + headlen) % boot.bytperrec;

  if( dirl > 0)
  {
    dirl = boot.bytperrec - dirl;
    z    = (char *)alloca(dirl);

    bzero(z, dirl);

    if( write( outFD, z, dirl)<0)
    {
      st_perror("write block_fill");
      lp_close(&outFD);

      return(-3);
    }
  }

  lp_close(&outFD);

  return((boot.num_index_rec-1) * boot.bytperrec / boot.bytperent - boot.num_entries_used);
}



/* Here is my attempt to write scans to a named directory entry; i.e. nsaves */
int lp_nwrite_sdd(name, nsave, h, d)
char  *name;
int    nsave;
struct HEADER *h;
char  *d;
{
  int  fd;
  long headlen, datalen, dirl, start;
  long datab, maxb, bytesinindex, bytesused;
  struct DIRECTORY dir;
  char *z;
  time_t itime;

  if( (fd = lp_pdopen(name, O_RDWR))<0)
    return(-1);

  bzero(&boot, sizeof(struct BOOTSTRAP));
  if( read( fd, &boot, sizeof(struct BOOTSTRAP))<0 ) {
    st_perror("read boot");
    lp_close(&fd);
    return(-2);
  }

  /* update bootstrap block */

  if( sizeof(struct DIRECTORY) != boot.bytperent ) {
    st_fail("directory sizes dont match\n");
    lp_close(&fd);
    return(-4);
  }

/* There could be holes but I'm not going to check for that */

  bytesinindex = boot.num_index_rec * boot.bytperrec;
  bytesused = boot.num_entries_used * boot.bytperent;

  if( bytesinindex < bytesused+sizeof(struct DIRECTORY) ) {
    st_fail("Directory structure of %s is full.\n", name );
    lp_close(&fd);
    return(-5);
  }

  if( boot.typesdd || !boot.version  ) {
    st_fail("SDD version or type of %s is invalid.\n", name );
    lp_close(&fd);
    return(-5);
  }

  headlen = sizeof(struct HEADER);
  h->headlen = (double)headlen;
  h->headlen = h->headlen;
  datalen = (long)h->datalen;

  datab =  lp_blocks( headlen + datalen );
  boot.num_data_rec += datab;

  maxb = boot.num_data_rec + boot.num_index_rec;

  if( maxb < 0 ) {
    st_fail("Data file %s is full.\n", name );
    lp_close(&fd);
    return(-5);
  }

  if( maxb >= 1717986918 )
    st_fail("Data file is getting full. %d blocks left\n", 2147483647-maxb);

  boot.counter++;
  boot.num_entries_used++;

  if( lseek( fd, 0l, SEEK_SET ) <0 ) {
    st_perror("lseek boot");
    return(-7);
  }

//  if( lp_write_boot( fd, &boot, sizeof(struct BOOTSTRAP)) <0 ) {
  if( write(fd, &boot, sizeof(struct BOOTSTRAP)) != sizeof(struct BOOTSTRAP))
  {
    st_perror("write boot");
    return(-3);
  }

  /* update new directory */
  dirl = (boot.num_entries_used-1) * boot.bytperent + boot.bytperrec;

  if( lseek( fd, dirl, SEEK_SET ) < 0) {
    st_perror("lseek dir");
    return(-7);
  }

  bzero( &dir, sizeof(struct DIRECTORY) );

  dir.start_rec = boot.num_index_rec + boot.num_data_rec - datab + 1;
  dir.end_rec = boot.num_index_rec + boot.num_data_rec;
  dir.h_coord = h->xsource;
  dir.v_coord = h->ysource;
  strncpy( dir.source, h->object, 16 );
  lp_padd(dir.source, 16);
  dir.scan = h->scan;
  dir.freq_res = lp_index_freqres(h);
  dir.rest_freq = lp_index_restfreq(h);
  dir.lst = h->lst;
  dir.ut = h->utdate;
  dir.obsmode = lp_obsmode(h->obsmode);
  dir.phase_rec = -1;
  dir.pos_code = lp_pos_code(h->coordcd);

  if( lp_write_dir( fd, &dir, boot.bytperent ) <0 ) {
    st_perror("write dir");
    return(-3);
  }

  /* set header offsets */

  h->headcls = (short)13;
  h->oneptr =  (short)OFFSET(h, headlen);
  h->twoptr =  (short)OFFSET(h, xpoint);
  h->thrptr =  (short)OFFSET(h, utdate);
  h->fourptr = (short)OFFSET(h, epoch);
  h->fiveptr = (short)OFFSET(h, tamb);
  h->sixptr =  (short)OFFSET(h, scanang);
  h->sevptr =  (short)OFFSET(h, bfwhm);
  h->eigptr =  (short)OFFSET(h, appeff);
  h->nineptr = (short)OFFSET(h, synfreq);
  h->tenptr =  (short)OFFSET(h, openpar[0]);
  h->elvptr =  (short)OFFSET(h, current_disk);
  h->twlptr =  (short)OFFSET(h, obsfreq);
  h->trnptr =  (short)OFFSET(h, nostac);

/* padd strings with spaces */

  lp_padd(h->obsid, 8);
  lp_padd(h->observer, 16);
  lp_padd(h->telescop, 8);
  lp_padd(h->projid, 8);
  lp_padd(h->object, 16);
  lp_padd(h->obsmode, 8);
  lp_padd(h->frontend, 8);
  lp_padd(h->backend, 8);
  lp_padd(h->precis, 8);
  lp_padd(h->pt_model,8);
  lp_padd(h->cl11type,8);
/*
  lp_padd(h->phastb01,32);
  lp_padd(h->phastb02,32);
  lp_padd(h->phastb03,32);
  lp_padd(h->phastb04,32);
  lp_padd(h->phastb05,32);
 */
  lp_padd(h->rx_info,16);

  lp_padd(h->coordcd,8);
  lp_padd(h->frame,8);
  lp_padd(h->veldef,8);
  lp_padd(h->typecal,8);

  /* fill the header stop "burn_time" with the time it was written. */

  itime = time(NULL);
  h->burn_time = (double)itime;

  /* write data */

  start = (dir.start_rec-1) * boot.bytperrec;

  if( lseek(fd, start, SEEK_SET ) <0 ) {
    st_perror("lseek data");
    return(-7);
  }

  if( write( fd, h, headlen) <0 ) {
    st_perror("write head");
    return(-3);
  }

  if( write( fd, d, datalen ) <0 ) {
    st_perror("write data");
    return(-3);
  }

  /* complete block */
  dirl = (datalen + headlen) % boot.bytperrec;
  if( dirl > 0 ) {
    dirl = boot.bytperrec - dirl;
    z = (char *)alloca(dirl);
    bzero(z,dirl);
    if( write( fd, z, dirl ) <0 ) {
      st_perror("write block_fill");
      return(-3);
    }
  }

  lp_close(&fd);

  return((boot.num_index_rec-1) * boot.bytperrec / boot.bytperent - boot.num_entries_used);
}


/* create new sdd file with (MAX_SCANS) 16384 possible scans (formerly 4096) */

int lp_new_sdd(name)
char *name;
{
  int fd;

  if( (fd = open(name, O_CREAT | O_RDWR | O_EXCL, 0644 ))<0)
  {
    st_perror("open");

    return(1);
  }

  if(verboseLevel & VERBOSE_FILEOPS)
  {
    st_report("new_sdd(): Creating data file: %s; FD = %d", name, fd);
  }

  bzero((char *)&boot, sizeof(struct BOOTSTRAP) );

  boot.num_index_rec    = MAX_SCANS * sizeof(struct DIRECTORY)/BLOCK+1;
  boot.num_data_rec     = 0;
  boot.bytperrec        = BLOCK;
  boot.bytperent        = sizeof(struct DIRECTORY);
  boot.num_entries_used = 0;
  boot.counter          = 0;
  boot.typesdd          = 0;
  boot.version          = 1;

//  if( lp_write_boot( fd, &boot, BLOCK)<0)
  if(write(fd, &boot, BLOCK) != BLOCK)
  {
    st_perror("write");
    lp_close(&fd);

    return(1);
  }

  lp_close(&fd);

  return(0);
}


/* return number of stupid fortran blocks given bytes  */
static int lp_blocks(b)
int b;
{
  if( b % BLOCK )
  {
    return(b/BLOCK + 1);
  }
  else
  {
    return(b/BLOCK);
  }
}


/* padd a string with spaces, fortran style */
static int lp_padd(s,n)
char *s;
int n;
{
  int i, flag = 0;

  for( i=0; i<n; i++ )
  {
    if( !flag && s[i] == '\0' )
    {
      flag = 1;
    }

    if( flag )
    {
      s[i] = ' ';
    }
  }

  return(0);
}


static double lp_index_freqres(h)
struct HEADER *h;
{
  double ret, sq, cos(), sqrt();

  if(!strncmp(h->obsmode, "CONT", 4))
  {
    sq  = h->deltaxr * cos(h->ysource * M_PI / 180.0);
    ret = sqrt( sq * sq + h->deltayr * h->deltayr );
  }
  else
  {
    ret = h->freqres;
  }

  return(ret);
}

static double lp_index_restfreq(h)
struct HEADER *h;
{
  double ret, cos();

  if( strncmp(h->obsmode, "CONT", 4 ) == 0 )
  {
    ret = h->samprat;
  }
  else
  {
    ret = h->restfreq;
  }

  return(ret);
}


static int lp_obsmode(o)
char *o;
{
  int mask, mode;

  if( strncasecmp(o, "LINE", 4 )== 0 )
  {
    mask = 512;
  }
  else
  if( strncasecmp(o, "CONT", 4 )== 0 )
  {
    mask = 256;
  }
  else
  {
    mask = 0;
  }

       if( strncasecmp(&o[4], "PS  ", 4)==0) mode = 1;
  else if( strncasecmp(&o[4], "APS ", 4)==0) mode = 2;
  else if( strncasecmp(&o[4], "FS  ", 4)==0) mode = 3;
  else if( strncasecmp(&o[4], "BSP ", 4)==0) mode = 4;
  else if( strncasecmp(&o[4], "TPON", 4)==0) mode = 5;
  else if( strncasecmp(&o[4], "TPOF", 4)==0) mode = 6;
  else if( strncasecmp(&o[4], "ATP ", 4)==0) mode = 7;
  else if( strncasecmp(&o[4], "PSM ", 4)==0) mode = 8;
  else if( strncasecmp(&o[4], "APM ", 4)==0) mode = 9;
  else if( strncasecmp(&o[4], "FSM ", 4)==0) mode = 10;
  else if( strncasecmp(&o[4], "TPMO", 4)==0) mode = 11;
  else if( strncasecmp(&o[4], "TPMF", 4)==0) mode = 12;
  else if( strncasecmp(&o[4], "DRFT", 4)==0) mode = 13;
  else if( strncasecmp(&o[4], "PCAL", 4)==0) mode = 14;
  else if( strncasecmp(&o[4], "BCAL", 4)==0) mode = 15;
  else if( strncasecmp(&o[4], "BLNK", 4)==0) mode = 16;
  else if( strncasecmp(&o[4], "SEQ ", 4)==0) mode = 17;
  else if( strncasecmp(&o[4], "FIVE", 4)==0) mode = 18;
  else if( strncasecmp(&o[4], "MAP ", 4)==0) mode = 19;
  else if( strncasecmp(&o[4], "FOC ", 4)==0) mode = 20;
  else if( strncasecmp(&o[4], "NSFC", 4)==0) mode = 21;
  else if( strncasecmp(&o[4], "TTIP", 4)==0) mode = 22;
  else if( strncasecmp(&o[4], "STIP", 4)==0) mode = 23;
  else if( strncasecmp(&o[4], "D_ON", 4)==0) mode = 24;
  else if( strncasecmp(&o[4], "CAL ", 4)==0) mode = 25;
  else if( strncasecmp(&o[4], "FSPS", 4)==0) mode = 26;
  else if( strncasecmp(&o[4], "EWFC", 4)==0) mode = 27;
  else if( strncasecmp(&o[4], "ZERO", 4)==0) mode = 28;

  else if( strncasecmp(&o[4], "TLPW", 4)==0) mode = 29;
  else if( strncasecmp(&o[4], "FQSW", 4)==0) mode = 30;
  else if( strncasecmp(&o[4], "NOCL", 4)==0) mode = 31;
  else if( strncasecmp(&o[4], "PLCL", 4)==0) mode = 32;
  else if( strncasecmp(&o[4], "ONOF", 4)==0) mode = 33;
  else if( strncasecmp(&o[4], "BMSW", 4)==0) mode = 34;
  else if( strncasecmp(&o[4], "PSSW", 4)==0) mode = 35;
  else if( strncasecmp(&o[4], "DRFT", 4)==0) mode = 36;
  else if( strncasecmp(&o[4], "OTF ", 4)==0) mode = 37;
  else if( strncasecmp(&o[4], "S_ON", 4)==0) mode = 38;
  else if( strncasecmp(&o[4], "S_OF", 4)==0) mode = 39;
  else if( strncasecmp(&o[4], "QK5 ", 4)==0) mode = 40;
  else if( strncasecmp(&o[4], "QK5A", 4)==0) mode = 41;
  else if( strncasecmp(&o[4], "PS-1", 4)==0) mode = 42;
  else if( strncasecmp(&o[4], "VLBI", 4)==0) mode = 43;
  else if( strncasecmp(&o[4], "PZC ", 4)==0) mode = 44;
  else if( strncasecmp(&o[4], "CPZM", 4)==0) mode = 45;
  else if( strncasecmp(&o[4], "PSPZ", 4)==0) mode = 46;
  else if( strncasecmp(&o[4], "CPZ1", 4)==0) mode = 47;
  else if( strncasecmp(&o[4], "CPZ2", 4)==0) mode = 48;
  else if( strncasecmp(&o[4], "CCPZ", 4)==0) mode = 49;
  else if( strncasecmp(&o[4], "CCPM", 4)==0) mode = 50;
  else if( strncasecmp(&o[4], "TPPZ", 4)==0) mode = 51;
  else if( strncasecmp(&o[4], "OPT ", 4)==0) mode = 52;
  else if( strncasecmp(&o[4], "COLD", 4)==0) mode = 53;
  else mode = 0;

  return(mode+mask);
}


static int lp_pos_code(s)
char *s;
{
       if( strncmp( s, "GALACTIC", 8 )==0) return 1;
  else if( strncmp( s, "1950RADC", 8 )==0) return 2;
  else if( strncmp( s, "EPOCRADC", 8 )==0) return 3;
  else if( strncmp( s, "MEANRADC", 8 )==0) return 4;
  else if( strncmp( s, "APPRADC",  7 )==0) return 5;
  else if( strncmp( s, "APPHADC",  7 )==0) return 6;
  else if( strncmp( s, "1950ECL",  7 )==0) return 7;
  else if( strncmp( s, "EPOCECL",  7 )==0) return 8;
  else if( strncmp( s, "MEANECL",  7 )==0) return 9;
  else if( strncmp( s, "APPECL",   6 )==0) return 10;
  else if( strncmp( s, "AZEL",     4 )==0) return 11;
  else if( strncmp( s, "USERDEF",  7 )==0) return 12;
  else if( strncmp( s, "2000RADC", 8 )==0) return 13;
  else if( strncmp( s, "INDRADC",  7 )==0) return 14;
  else return 0;
}


#ifdef TOMMIE
static int lp_write_boot( fd, b, len )
int fd;
struct BOOTSTRAP *b;
int len;
{
  int ret;
  struct BOOTSTRAP bb;

  bcopy( b, &bb, sizeof(struct BOOTSTRAP));

  if(verboseLevel & VERBOSE_FILEOPS)
  {
    st_report("lp_writeboot(): fd = %d, len = %d", fd, len);
  }

  ret = write(fd, &bb, len);

  return(ret);
}
#endif


static int lp_write_dir( fd, d, len )
int fd;
struct DIRECTORY *d;
int len;
{
  int ret;
  struct DIRECTORY dd;

  bcopy( d, &dd, sizeof(struct DIRECTORY));

  ret = write(fd, &dd, len);

  return(ret);
}


int printLastBoot()
{
  st_report("Current BOOT Struct:");
  st_report(" ");
  st_report("num_index_rec    = %d", boot.num_index_rec);
  st_report("num_data_rec     = %d", boot.num_data_rec);
  st_report("bytperrec        = %d", boot.bytperrec);
  st_report("bytperent        = %d", boot.bytperent);
  st_report("num_entries_used = %d", boot.num_entries_used);
  st_report("counter          = %d", boot.counter);
  st_report("typesdd          = %d", boot.typesdd);
  st_report("version          = %d", boot.version);
  st_report(" ");

  return(0);
}
