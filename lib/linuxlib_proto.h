/* sdd.c */
extern int checkConstCal(/*struct HEADER *h*/);
extern int findSize(/*struct ENVIRONMENT *env*/);
extern struct SOURCES *findSourceInList(/*char *name*/);
extern int clearSourceList(/*void*/);
extern int showSourceList(/*void*/);
extern int doSummary(/*void*/);
extern int doSummaryOfFreqs2Buf(/*char *source, char *buf, int rev*/);
extern int findScans(/*struct ENVIRONMENT *env*/);
extern int lp_checkBounds(/*float *d, int l, int autobad*/);
extern int checkForScaling(/*float *d, struct HEADER *h*/);
extern int read_scan(/*double scan, char *filename, float *data, struct HEADER *head, int chk*/);
extern int read_nsave(/*int nsave, char *filename, float *data, struct HEADER *head, int chk*/);
extern int getFeed(/*double scan*/);
extern int getFeedFromBackend(/*char *b*/);
extern int lp_write_sdd(/*char *name, struct HEADER *h, char *d*/);
extern int lp_nwrite_sdd(/*char *name, int nsave, struct HEADER *h, char *d*/);
extern int lp_new_sdd(/*char *name*/);
extern int printLastBoot(/*void*/);
/* swapsdd.c */
extern int swapBoot(/*struct BOOTSTRAP *boot*/);
extern int force_swapBoot(/*struct BOOTSTRAP *boot*/);
extern int swapDir(/*struct DIRECTORY *dir*/);
extern int swapHeader(/*struct HEADER *head*/);
extern int swapData(/*float *data, int n*/);
/* gm_map.c */
extern struct GLOBAL_MEMORY *gm_map(/*char *name, int iswrite*/);
extern struct OBJECT_GLOBAL_MEMORY *obj_map(/*char *name, int iswrite*/);
/* gaussfit.c */
extern int nrerror(/*char *error_text*/);
extern double *vector(/*int nl, int nh*/);
extern double **matrix(/*int nrl, int nrh, int ncl, int nch*/);
extern int free_vector(/*double *v, int nl*/);
extern int free_ivector(/*int *v, int nl*/);
extern int free_matrix(/*double **m, int nrl, int nrh, int ncl*/);
extern int gaussj(/*double a[100 +1][100 +1], int n, double **b, int m*/);
extern int mrqmin(/*double x[], double y[], double sig[], int ndata, double a[], int ma, int lista[], int mfit, double covar[100 +1][100 +1], double alpha[100 +1][100 +1], double *chisq, void (*funcs)(double, double *, double *, double *), double *alamda*/);
extern int mrqcof(/*double x[], double y[], double sig[], int ndata, double a[], int ma, int lista[], int mfit, double alpha[100 +1][100 +1], double beta[], double *chisq, void (*funcs)(double, double *, double *, double *)*/);
extern int covsrt(/*double covar[100 +1][100 +1], int ma, int lista[], int mfit*/);
extern double gammq(/*double a, double x*/);
extern int gser(/*double *gamser, double a, double x, double *gln*/);
extern int gcf(/*double *gammcf, double a, double x, double *gln*/);
extern double gammln(/*double xx*/);
extern void fgauss(/*double x, double *a, double *y, double *dyda*/);
extern int gauss_fit(/*double *x, double *y, double *sig, int npts, double *a, int verbose*/);
/* errPrint.c */
extern int getNumericResponse(/*const char *fmt, ...*/);
extern int getYesNoQuit(/*const char *fmt, ...*/);
extern void st_print(/*const char *fmt, ...*/);
extern void st_report(/*const char *fmt, ...*/);
extern void st_report_fp(/*const char *fmt, ...*/);
extern void st_warn(/*const char *fmt, ...*/);
extern void st_fail(/*const char *fmt, ...*/);
extern int st_perror(/*char *msg*/);
/* class.c */
extern int julda(/*int nyr*/);
extern void datj(/*int id, int im, int iy, int *jour*/);
extern int class_date(/*double pops_date*/);
extern void add_class_section(/*long *info, int num, int type, int length*/);
extern int eqtogal(/*double ra, double dec, double *glong, double *glat*/);
extern int class_write(/*char *file, struct HEADER *headp, float *datap, char *cname, int verbose*/);
extern int class_init(/*char *file*/);
extern int write_index(/*int fd, struct index_ent *ix, int len*/);
extern int write_fileix(/*int fd, struct file_index *ix, int len*/);
extern int write_scan(/*int fd, struct scan_ent *scan, int len*/);
extern int write_general(/*int fd, struct general_descr *gen, int len*/);
extern int write_position(/*int fd, struct position_descr *gen, int len*/);
